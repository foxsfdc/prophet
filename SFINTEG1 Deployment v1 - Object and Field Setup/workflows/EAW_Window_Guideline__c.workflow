<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_name_filed</fullName>
        <field>Name</field>
        <formula>IF( AND(NOT(ISBLANK(Clone_WG_Name__c)),  NOT(ISCHANGED(Window_Guideline_Alias__c)), NOT(ISCHANGED(Product_Type__c))) , Window_Guideline_Alias__c &amp; &quot;-&quot; &amp; Text(Product_Type__c) &amp; &quot; &quot; &amp; Clone_WG_Name__c , Window_Guideline_Alias__c &amp; &quot;-&quot; &amp; Text(Product_Type__c))</formula>
        <name>Update window guideline name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Auto update Window Guideline Name</fullName>
        <actions>
            <name>Update_name_filed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT(ISBLANK(Window_Guideline_Alias__c)) , NOT(ISPICKVAL(Product_Type__c, &apos;&apos;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
