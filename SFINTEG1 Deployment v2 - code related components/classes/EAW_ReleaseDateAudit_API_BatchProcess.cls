//LRCC-1708
global class EAW_ReleaseDateAudit_API_BatchProcess implements Database.AllowsCallouts,Database.Batchable<sObject>{
	public List<Id> releaseDateIdList = new List<Id>();
	
    global EAW_ReleaseDateAudit_API_BatchProcess(List<Id> releaseDateIdList){
    	this.releaseDateIdList = releaseDateIdList;
    }
    global Database.QueryLocator start(Database.BatchableContext bc) {
    	return Database.getQueryLocator(
            'select ID,name, History_Sent_Date__c, Sent_To_Repo__c, '+ 
			'(Select Field, OldValue, NewValue, CreatedBy.Name,CreatedDate From Histories order by CreatedDate desc) '+
			'from Eaw_release_date__c where '+
    		'ID IN : releaseDateIdList AND Soft_Deleted__c <> \'TRUE\''	
        );
    }
    global void execute(Database.BatchableContext bc, List<Eaw_release_date__c> records){
		try{
			EAW_ReleaseDateAudit_Outbound erao = new EAW_ReleaseDateAudit_Outbound();
			erao.sendData(records);
        }catch(Exception e){
            String message = e.getMessage();
    		//System.debug('e.getMessage(): '+e.getMessage());
    		if(message!=null && message.length()>250)message=message.substring(0, 250);
            
        }
    }    
    global void finish(Database.BatchableContext bc){
    	// TO do Call Window API batch process
    	//Id batchJobId = Database.executeBatch(new EAW_Window_API_BatchProcess(), EAW_Outbound_Utility.MAX_API_RECORDS_LIMIT);
		//system.debug('EAW_Window_API_BatchProcess batchJobId: '+batchJobId);
    }    
}