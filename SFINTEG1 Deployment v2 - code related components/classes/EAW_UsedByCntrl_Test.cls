@isTest
public class EAW_UsedByCntrl_Test {
    
    @testSetup static void setupTestData() {
        EAW_TestDataFactory.createEAWCustomer(1, true);
        EAW_Window_Guideline__c newWg = new EAW_Window_Guideline__c ( Status__c = 'Draft', Product_Type__c = 'Compilation Episode', Window_Type__c = 'First-Run', Window_Guideline_Alias__c = 'Test');
        insert newWg; 
        EAW_Window_Guideline_Strand__c windowGuideLineStrand =  new EAW_Window_Guideline_Strand__c(Language__c = 'Tamil',Media__c='Basic TV ',Territory__c='Afghanistan', EAW_Window_Guideline__c = newWg.Id, License_Type__c = 'Exhibition License');
        insert windowGuideLineStrand;
        newWg.Status__c = 'Active';
        update newWg;
        
        EAW_Window_Guideline__c conditionalOperand = new EAW_Window_Guideline__c ( Status__c = 'Draft', Product_Type__c = 'Compilation Episode', Window_Type__c = 'First-Run', Window_Guideline_Alias__c = 'Test wg1');
        insert conditionalOperand;
        EAW_Window_Guideline_Strand__c newwindowGuideLineStrand =  new EAW_Window_Guideline_Strand__c(Language__c = 'Tamil',Media__c='Basic TV  ',Territory__c='Afghanistan', EAW_Window_Guideline__c = conditionalOperand.Id, License_Type__c = 'Exhibition License');
        insert newwindowGuideLineStrand;
        conditionalOperand.Status__c = 'Active';
        update conditionalOperand;
        
        List<EAW_Customer__c> customerList = [SELECT Id, Name FROM EAW_Customer__c LIMIT 1];
        EAW_Release_Date_Guideline__c releaseDateGuideLine = new EAW_Release_Date_Guideline__c( Product_Type__c = 'Compilation Episode', Territory__c = 'United States +', Language__c = 'English', Customers__c = customerList[0].Name, EAW_Release_Date_Type__c = 'Television');
        insert releaseDateGuideLine; 
        String newruleContent = '{"sObjectType":"EAW_Rule__c","Nested__c":true}';
        String newruleDetailContent = '[{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Condition_Timeframe__c":"True_Earliest"},"dateCalcs":[{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Conditional_Operand_Id__c":"'+releaseDateGuideLine.Id+'","Condition_Met_Months__c":"2","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"},{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Conditional_Operand_Id__c":"'+conditionalOperand.Id+'","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"}],"nodeCalcs":[{"ruleDetail":{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":2,"Condition_Timeframe__c":"True_Earliest"},"dateCalcs":[{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":2,"Conditional_Operand_Id__c":"aA73D000000CyJjSAK","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"}],"nodeCalcs":[]}]}]';
        EAW_TestRuleDataFactory.saveRuleAndRuleDetails(newruleContent,newruleDetailContent,false,newWg.Id,'Start Date',null);

    }
    @isTest static void getUsedByRecords_Test(){
        EAW_Release_Date_Guideline__c releaseDateGuideLine = [SELECT Id FROM EAW_Release_Date_Guideline__c LIMIT 1];
        EAW_UsedByController.getUsedByRecords(releaseDateGuideLine.Id,'EAW_Release_Date_Guideline__c');
        EAW_Window_Guideline__c wg = [SELECT Id FROM EAW_Window_Guideline__c WHERE Window_Guideline_Alias__c = 'Test wg1'];
        EAW_UsedByController.getUsedByRecords(wg.Id,'EAW_Window_Guideline__c');    
    }
    @isTest static void multiConditionalOperands_Test(){
        
        EAW_Window_Guideline__c newWg = new EAW_Window_Guideline__c ( Status__c = 'Draft', Product_Type__c = 'Compilation Episode', Window_Type__c = 'First-Run', Window_Guideline_Alias__c = 'New Test');
        insert newWg; 
        EAW_Window_Guideline_Strand__c windowGuideLineStrand =  new EAW_Window_Guideline_Strand__c(Language__c = 'Tamil',Media__c='Basic TV ',Territory__c='Afghanistan', EAW_Window_Guideline__c = newWg.Id, License_Type__c = 'Exhibition License');
        insert windowGuideLineStrand;
        newWg.Status__c = 'Active';
        update newWg;
        
        EAW_Window_Guideline__c conditionalOperand = new EAW_Window_Guideline__c ( Status__c = 'Draft', Product_Type__c = 'Compilation Episode', Window_Type__c = 'First-Run', Window_Guideline_Alias__c = 'New Test wg1');
        insert conditionalOperand;
        EAW_Window_Guideline_Strand__c newwindowGuideLineStrand =  new EAW_Window_Guideline_Strand__c(Language__c = 'Albanian',Media__c='Basic TV   ',Territory__c='Afghanistan', EAW_Window_Guideline__c = conditionalOperand.Id, License_Type__c = 'Exhibition License');
        insert newwindowGuideLineStrand;
        conditionalOperand.Status__c = 'Active';
        update conditionalOperand;
        
        EAW_Window_Guideline__c conditionalOperand1 = new EAW_Window_Guideline__c ( Status__c = 'Draft', Product_Type__c = 'Compilation Episode', Window_Type__c = 'First-Run', Window_Guideline_Alias__c = 'New Test wg2');
        insert conditionalOperand1;
        EAW_Window_Guideline_Strand__c newwindowGuideLineStrand1 =  new EAW_Window_Guideline_Strand__c(Language__c = 'Albanian',Media__c='Basic TV  ',Territory__c='Afghanistan', EAW_Window_Guideline__c = conditionalOperand1.Id, License_Type__c = 'Exhibition License');
        insert newwindowGuideLineStrand1;
        conditionalOperand1.Status__c = 'Active';
        update conditionalOperand1;
        
        List<EAW_Customer__c> customerList = [SELECT Id, Name FROM EAW_Customer__c LIMIT 1];
        EAW_Release_Date_Guideline__c releaseDateGuideLine = new EAW_Release_Date_Guideline__c( Product_Type__c = 'Compilation Episode', Territory__c = 'United States +', Language__c = 'Albanian', Customers__c = customerList[0].Name, EAW_Release_Date_Type__c = 'Television');
        insert releaseDateGuideLine; 
        
        String ruleContent = '{"sObjectType":"EAW_Rule__c","Operator__c":"IN","Condition_Field__c":"Release Date Status","Criteria__c":"Estimated","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature","Conditional_Operand_Id__c":"'+conditionalOperand.Id+';'+conditionalOperand1.Id+'","Multi_conditional_operands__c":true,"Nested__c":true,"Condition_Type__c":"If"}';
        String ruleDetailContent = '[{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Condition_Type__c":"If","Conditional_Operand_Id__c":"'+conditionalOperand.Id+';'+conditionalOperand1.Id+'","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature","Rule_Order__c":1,"Multi_conditional_operands__c":true},"dateCalcs":[],"nodeCalcs":[]},{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Rule_Order__c":2,"Nest_Order__c":1,"Condition_Type__c":"Else/If","Conditional_Operand_Id__c":"aA73D000000CyJjSAK","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"},"dateCalcs":[],"nodeCalcs":[]}]';
        EAW_TestRuleDataFactory.saveRuleAndRuleDetails(ruleContent,ruleDetailContent,false,newWg.Id,'Start Date',null);
        EAW_UsedByController.getUsedByRecords(newWg.Id,'EAW_Window_Guideline__c');    
        EAW_UsedByController.findReferencedWindowGuidelines(new Set<Id>{conditionalOperand.Id,conditionalOperand1.Id});
    }
    
}