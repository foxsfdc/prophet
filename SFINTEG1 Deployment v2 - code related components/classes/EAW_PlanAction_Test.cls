/**
 *  Test class for EAWPlanAction
 * 
 */
@isTest
public class EAW_PlanAction_Test {
    
    @testSetup static void setupTestData() {
    
        EAW_TestDataFactory.createPlanGuideline(2, true);
        
    }
    @isTest static void EAWPlanActionMethod_Test(){
        
        Test.startTest();
            
            List<EAW_Plan_Guideline__c> planGuideLineList = [SELECT Id,Name FROM EAW_Plan_Guideline__c LIMIT 2];
            
            ApexPages.StandardSetController setController = new ApexPages.StandardSetController(planGuideLineList );
           
            setController.setSelected(planGuideLineList);
            
            EAWPlanAction planAction = new EAWPlanAction(setController);
            PageReference planPageRef = planAction.deleteSelectedRecords();
           
        Test.stopTest();
        
        System.assertEquals(True, planPageRef != null);
    }
    @isTest static void cloneSelectedRecords_Test() {
        
        Test.startTest();
        
            List<EAW_Plan_Guideline__c> planGuideLineList = [SELECT Id,Name, Media__c, Product_Type__c, Territory__c  FROM EAW_Plan_Guideline__c LIMIT 2];
            
            ApexPages.StandardSetController setController = new ApexPages.StandardSetController(planGuideLineList );
               
            setController.setSelected(planGuideLineList);
            
            EAWPlanAction planAction = new EAWPlanAction(setController);
            PageReference planPageRef = planAction.cloneSelectedRecords();
        
        Test.stopTest();
        
        List<EAW_Plan_Guideline__c> planGuideLineListObtained = [SELECT Id,Name FROM EAW_Plan_Guideline__c];
        
        System.assertEquals(4, planGuideLineListObtained.size());
    }
    
    @isTest static void deactivatePlans_Test(){
        
        Test.startTest();
        
        List<EAW_Plan_Guideline__c> planGuideLineList = [SELECT Id,Name, Media__c, Product_Type__c, Territory__c  FROM EAW_Plan_Guideline__c LIMIT 2];
        
        ApexPages.StandardSetController setController = new ApexPages.StandardSetController(planGuideLineList );
        
        setController.setSelected(planGuideLineList);
        
        EAWPlanAction planAction = new EAWPlanAction(setController);
        PageReference planPageRef = planAction.deactivatePlans();
        
        Test.stopTest();
        
        System.assertEquals(True, planPageRef != null);
    }
    @isTest static void searchForPlans_Test(){
        
        Test.startTest();
        
        List<EAW_Plan_Guideline__c> planGuideLineList = [SELECT Id,Name, Media__c, Product_Type__c, Territory__c  FROM EAW_Plan_Guideline__c LIMIT 2];
        
        List<EAW_Plan_Guideline__c> planGuideLineListQuereid = EAW_TestDataFactory.createPlanGuideline(1, false);
        planGuideLineListQuereid[0].Territory__c = 'Albania';
        planGuideLineListQuereid[0].Language__c = 'Amharic';
        
        ApexPages.StandardSetController setController = new ApexPages.StandardSetController(planGuideLineList);
        //searchPlan = planGuideLineListQuereid[0];
        
        setController.setSelected(planGuideLineList);    
                
        try {
            EAWPlanAction planAction = new EAWPlanAction(setController);
            PageReference planPageRef = planAction.searchForPlans();
            
        } catch(Exception ex) {
        }
        
        Test.stopTest();
        
    }
}