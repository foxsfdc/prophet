/*
Description : 
Handler - EAW_WindowGuidelineTrigger_Handler
Trigger - EAW_WindowGuidelineTrigger
*/

@isTest
public class EAW_WindowGuidelineTrigger_Test {
   
    @isTest static void validateWindowGuidelineStatus_Test() {
        
        try {
        
            EAW_Window_Guideline__c windowGuideLine = new EAW_Window_Guideline__c ( Status__c = 'Active');
            insert windowGuideLine;
        } catch(Exception ex) {
        }
    }
    @isTest static void preventDeletionOfActiveWindows_Test() {
        
        try {
        
            List<EAW_Window_Guideline__c> windowGuidelineList = EAW_TestDataFactory.createWindowGuideLine(1, false);
            windowGuidelineList[0].Status__c = 'Draft';
            windowGuidelineList[0].Name = 'Test window GuideLine';
            windowGuidelineList[0].Product_Type__c = 'Shorts';
            windowGuidelineList[0].Window_Type__c = 'First-Run';
            insert windowGuidelineList[0];
         
            List<EAW_Window_Guideline_Strand__c> windowGuideLineStrandList = EAW_TestDataFactory.createWindowGuideLineStrand(1, false);
            windowGuideLineStrandList[0].License_Type__c = 'Exhibition License';
            windowGuideLineStrandList[0].EAW_Window_Guideline__c = windowGuideLineList[0].Id;
            windowGuideLineStrandList[0].Media__c = 'Theatrical';
            windowGuideLineStrandList[0].Territory__c = 'Afghanistan'; 
            windowGuideLineStrandList[0].Language__c = 'Afrikaans';
            insert windowGuideLineStrandList; 
        
            List<EAW_Title__c> title = EAW_TestDataFactory.createEAWTitle(1, true);
        
            windowGuidelineList[0].Status__c = 'Active';
            update windowGuidelineList;    
        
           /* EAW_Window__c winowIns = new EAW_Window__c();
            winowIns.EAW_Window_Guideline__c = windowGuideLineList.Id; 
            winowIns.EAW_Title_Attribute__c = title[0].Id;
            insert winowIns;
            
            EAW_Window_Strand__c windowStrandIns = new EAW_Window_Strand__c();
            windowStrandIns.Territory__c = 'Afghanistan';
            windowStrandIns.Language__c = 'Afrikaans';
            windowStrandIns.Media__c = 'Theatrical';
            windowStrandIns.License_Type__c = 'Exhibition License';
            windowStrandIns.Window__c = winowIns.Id;        
            insert windowStrandIns;       
            
            List<EAW_Plan_Guideline__c> planGuideLineList = new List<EAW_Plan_Guideline__c> {
                
                new EAW_Plan_Guideline__c(Status__c = 'Draft', Product_Type__c = 'Shorts')
            };
            insert planGuideLineList ;
             
             List<EAW_Plan_Window_Guideline_Junction__c> planWindowList = new List<EAW_Plan_Window_Guideline_Junction__c> {
                new EAW_Plan_Window_Guideline_Junction__c(Plan__c = planGuideLineList[0].Id , Window_Guideline__c = windowGuideLineList.Id)
            };
            insert planWindowList; */
             
            List<EAW_Window_Guideline__c> obtainedlist = [SELECT Id, Name FROM EAW_Window_Guideline__c LIMIT 1];
            delete obtainedlist;
            
            } catch(Exception ex) {
                
                System.assert(ex.getMessage().contains('You cannot delete an Active window Guideline'));
            }
    }
   @isTest static void preventDeletionOfInActiveWindows_Test() {
        
        try {
        
            List<EAW_Window_Guideline__c> windowGuidelineList = EAW_TestDataFactory.createWindowGuideLine(2, false);
            insert windowGuidelineList[0];
            
            windowGuidelineList[1].Status__c = 'Draft';
            windowGuidelineList[1].Name = 'Test window GuideLine rec';
            windowGuidelineList[1].Product_Type__c = 'Shorts';
            windowGuidelineList[1].Window_Type__c = 'First-Run';
            windowGuidelineList[1].Next_Version__c = windowGuidelineList[0].Id;
            insert windowGuidelineList[1];       
            
            windowGuidelineList[1].Status__c = 'inactive';
            update windowGuidelineList[1];    
            
            List<EAW_Window_Guideline__c> obtainedlist = [SELECT Id, Name FROM EAW_Window_Guideline__c WHERE Name = 'Test window Guideline rec'];
            delete obtainedlist;
            
        } catch(Exception ex) {
            
            System.assert(ex.getMessage().contains('You cannot delete an Inactive Window Guideline with newer versions'));
        }
    }
    
    @isTest static void validateWindowStrands_Test() {
        
        List<EAW_Window_Guideline__c> windowGuidelineList = EAW_TestDataFactory.createWindowGuideLine(2, false);
        windowGuidelineList[0].Status__c = 'Draft';
        windowGuidelineList[0].Name = 'Test window GuideLine';
        windowGuidelineList[0].Product_Type__c = 'Shorts';
        windowGuidelineList[0].Window_Type__c = 'First-Run';
        insert windowGuidelineList[0];
        
        windowGuidelineList[1].Status__c = 'Draft';
        windowGuidelineList[1].Previous_Version__c = windowGuidelineList[0].Id;
        windowGuidelineList[1].Product_Type__c = 'Shorts';
        windowGuidelineList[1].Window_Type__c = 'First-Run';
        insert windowGuidelineList[1];
         
        List<EAW_Window_Guideline_Strand__c> windowGuideLineStrandList = EAW_TestDataFactory.createWindowGuideLineStrand(1, false);
        windowGuideLineStrandList[0].License_Type__c = 'Exhibition License';
        windowGuideLineStrandList[0].EAW_Window_Guideline__c = windowGuideLineList[1].Id;
        windowGuideLineStrandList[0].Media__c = 'Theatrical';
        windowGuideLineStrandList[0].Territory__c = 'Afghanistan'; 
        windowGuideLineStrandList[0].Language__c = 'Afrikaans';
        insert windowGuideLineStrandList; 
        
        windowGuidelineList[1].Status__c = 'Active';        
        update windowGuidelineList[1];
        
        List<EAW_Plan_Guideline__c> planinstanceList = EAW_TestDataFactory.createPlanGuideline(2, false); 
        planinstanceList[0].Name = 'Test plan guideline Next';
        planinstanceList[0].Status__c = 'draft';
        planinstanceList[0].Product_Type__c = 'Shorts';
        insert planinstanceList[0];
        
        planinstanceList[1].Name = 'Test plan guideline';
        planinstanceList[1].Status__c = 'draft';
        planinstanceList[1].Next_Version__c = planinstanceList[0].Id;
        planinstanceList[1].Product_Type__c = 'Shorts';
        insert planinstanceList[1]; 
        
        EAW_Plan_Window_Guideline_Junction__c ruleDetail = new EAW_Plan_Window_Guideline_Junction__c (Plan__c = planinstanceList[1].Id, Window_Guideline__c = windowGuidelineList[1].Id);
        insert ruleDetail;
    }
    
    @isTest static void reconfigureJunctionsPlanGuidelines_Test() {
        
        List<EAW_Window_Guideline__c> windowGuidelineParent = EAW_TestDataFactory.createWindowGuideLine(2, false);
        windowGuidelineParent[0].Status__c = 'Draft';
        windowGuidelineParent[0].Name = 'Test window GuideLine';
        windowGuidelineParent[0].Product_Type__c = 'Shorts';
        windowGuidelineParent[0].Window_Type__c = 'First-Run';
        insert windowGuidelineParent[0]; 
        
        List<EAW_Window_Guideline_Strand__c> windowGuideLineStrandList = EAW_TestDataFactory.createWindowGuideLineStrand(1, false);
        windowGuideLineStrandList[0].License_Type__c = 'Exhibition License';
        windowGuideLineStrandList[0].EAW_Window_Guideline__c = windowGuidelineParent[0].Id;
        windowGuideLineStrandList[0].Media__c = 'Theatrical';
        windowGuideLineStrandList[0].Territory__c = 'Afghanistan'; 
        windowGuideLineStrandList[0].Language__c = 'Afrikaans';
        insert windowGuideLineStrandList[0]; 
        
        List<EAW_Title__c> title = EAW_TestDataFactory.createEAWTitle(1, true);
        
        windowGuidelineParent[0].Status__c = 'Active';
        update windowGuidelineParent[0]; 
        
        List<EAW_Window__c> winowIns = EAW_TestDataFactory.createWindow(1, false);
        winowIns[0].EAW_Window_Guideline__c = windowGuidelineParent[0].Id;  
        winowIns[0].EAW_Title_Attribute__c = title[0].Id;
        insert winowIns;
        
        //LRCC-1653 - -In this ticket once passed QA it with remove the commented code.
        /*List<EAW_Window_Strand__c> windowStrand = EAW_TestDataFactory.createWindowStrand(1, false);
        windowStrand[0].License_Type__c = 'Exhibition License';
        windowStrand[0].Window__c = winowIns[0].Id;
        windowStrand[0].Media__c = 'Theatrical';
        windowStrand[0].Territory__c = 'Afghanistan'; 
        windowStrand[0].Language__c = 'Afrikaans';
        insert windowStrand; */
        
        List<EAW_Plan_Guideline__c> planinstanceList = EAW_TestDataFactory.createPlanGuideline(1, false); 
        planinstanceList[0].Name = 'Test plan guideline';
        planinstanceList[0].Status__c = 'draft';
        planinstanceList[0].Product_Type__c = 'Shorts';
        insert planinstanceList[0];
        
        List<EAW_Plan_Window_Guideline_Junction__c> planWindowList = new List<EAW_Plan_Window_Guideline_Junction__c> {
            new EAW_Plan_Window_Guideline_Junction__c(Plan__c = planinstanceList[0].Id , Window_Guideline__c = windowGuidelineParent[0].Id)
        };
        insert planWindowList;
        
        EAW_Plan_Window_Guideline_Junction__c ruleDetail = new EAW_Plan_Window_Guideline_Junction__c (Plan__c = planinstanceList[0].Id, Window_Guideline__c = windowGuidelineParent[0].Id);
        insert ruleDetail;
        
        planinstanceList[0].Status__c = 'active';
        update planinstanceList;
        
        windowGuidelineParent[1].Status__c = 'Draft';
        windowGuidelineParent[1].Previous_Version__c = windowGuidelineParent[0].Id;
        windowGuidelineParent[1].Product_Type__c = 'Shorts';
        windowGuidelineParent[1].Window_Type__c = 'First-Run';
        insert windowGuidelineParent[1]; 
        
     }
     
   /* @isTest static void preventDeletionOfActiveWindows_Test() {
        
        List<EAW_Window_Guideline__c> windowguildelinenextVersion = EAW_TestDataFactory.createWindowGuideLine(1, true);
        
        List<EAW_Window_Guideline__c> windowguildelineList = EAW_TestDataFactory.createWindowGuideLine(1, true);
        windowguildelineList[0].Status__c = 'inactive';
        windowguildelineList[0].Next_Version__c = windowguildelinenextVersion[0].Id;
        update windowguildelineList;
        try {
            delete windowguildelineList;
        } catch(Exception ex) {
            System.assert(ex.getMessage().contains('You cannot delete an Inactive Window Guideline with newer versions'));
        }
        
        List<EAW_Window_Guideline__c> windowguildelineactive = EAW_TestDataFactory.createWindowGuideLine(1, true);
        windowguildelineactive [0].Status__c = 'inactive';
        update windowguildelineactive;
        
        delete windowguildelineactive;
        
    }*/
    @isTest static void copyWindowsAndWindowGuidelineStrands_Test() {
        
        List<EAW_Customer__c> customerInsList = EAW_TestDataFactory.createEAWCustomer(2, false);
        customerInsList[0].Name='Test customer';
        customerInsList[0].Media__c = 'Free TV';
        customerInsList[0].Customer_Id__c = '123456789';
        insert customerInsList[0];
        
        customerInsList[1].Name='Test customer Ins';
        customerInsList[1].Media__c = 'Basic TV';
        customerInsList[1].Customer_Id__c = '94433';
        insert customerInsList[1];
        
        List<EAW_Window_Guideline__c> windowGuidelineList = EAW_TestDataFactory.createWindowGuideLine(2, false);
        windowGuidelineList[0].Status__c = 'Draft';
        windowGuidelineList[0].Name = 'Test window GuideLine';
        windowGuidelineList[0].Product_Type__c = 'Shorts';
        windowGuidelineList[0].Window_Type__c = 'First-Run';
        insert windowGuidelineList[0];
        
        windowGuidelineList[1].Status__c = 'Draft';
        windowGuidelineList[1].Previous_Version__c = windowGuidelineList[0].Id;
        windowGuidelineList[1].Product_Type__c = 'Shorts';
        windowGuidelineList[1].Window_Type__c = 'First-Run';
        //LRCC-1560 - Replace Customer lookup field with Customer text field 
        //windowGuidelineList[1].Customer__c = customerInsList[0].Id;
        windowGuidelineList[1].Customers__c = customerInsList[0].Name;
        insert windowGuidelineList[1];
        
        List<EAW_Window_Guideline_Strand__c> windowGuideLineStrandList = EAW_TestDataFactory.createWindowGuideLineStrand(1, false);
        windowGuideLineStrandList[0].License_Type__c = 'Exhibition License';
        windowGuideLineStrandList[0].EAW_Window_Guideline__c = windowGuideLineList[1].Id;
        windowGuideLineStrandList[0].Media__c = 'Theatrical';
        windowGuideLineStrandList[0].Territory__c = 'Afghanistan'; 
        windowGuideLineStrandList[0].Language__c = 'Afrikaans';
        insert windowGuideLineStrandList; 
        
        windowGuidelineList[1].Status__c = 'Active';
        //LRCC-1560 - Replace Customer lookup field with Customer text field 
        //windowGuidelineList[1].Customer__c = customerInsList[1].Id;
        windowGuidelineList[1].Customers__c = customerInsList[1].Name;       
        update windowGuidelineList[1];
        
        List<EAW_Plan_Guideline__c> planinstanceList = EAW_TestDataFactory.createPlanGuideline(1, false); 
        planinstanceList[0].Name = 'Test plan guideline';
        planinstanceList[0].Status__c = 'draft';
        planinstanceList[0].Product_Type__c = 'Shorts';
        insert planinstanceList;
        
        EAW_Plan_Window_Guideline_Junction__c planwindowjunction = new EAW_Plan_Window_Guideline_Junction__c (Plan__c = planinstanceList[0].Id, Window_Guideline__c = windowGuidelineList[1].Id);
        insert planwindowjunction;
        
        //List<EAW_Title__c> title = EAW_TestDataFactory.createEAWTitle(1, true);
        
        List<EAW_Window__c> ruleDetail = EAW_TestDataFactory.createWindow(1, false);
        ruleDetail[0].Name='Test window';
        ruleDetail[0].EAW_Window_Guideline__c = windowGuidelineList[1].Id;
        ruleDetail[0].Status__c = 'Estimated';
        List<EAW_Title__c> title = [SELECT Id FROM EAW_Title__c LIMIT 1];
        ruleDetail[0].EAW_Title_Attribute__c = title[0].Id;
        insert ruleDetail; 
        
    }
}