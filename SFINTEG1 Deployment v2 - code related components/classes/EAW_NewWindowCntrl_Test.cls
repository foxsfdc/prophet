@isTest
public class EAW_NewWindowCntrl_Test {

    @isTest static void EAW_NewWindowController_Test(){
        EAW_Window_Guideline__c newWg = new EAW_Window_Guideline__c ( Status__c = 'Draft', Product_Type__c = 'Compilation Episode', Window_Type__c = 'First-Run', Window_Guideline_Alias__c = 'Test');
        insert newWg; 
        EAW_Window_Guideline_Strand__c windowGuideLineStrand =  new EAW_Window_Guideline_Strand__c(Language__c = 'Tamil',Media__c='Basic TV  ',Territory__c='Afghanistan', EAW_Window_Guideline__c = newWg.Id, License_Type__c = 'Exhibition License');
    insert windowGuideLineStrand;
        newWg.Status__c = 'Active';
        update newWg;
        
        EAW_Title__c title = new EAW_Title__c();
        title.Name = 'Test Title';
        insert title;
        
        EAW_Window__c window = new EAW_Window__c (Name='Test E', EAW_Title_Attribute__c = title.Id, Window_Type__c = 'Library',EAW_Window_Guideline__c = newWg.Id);
        insert window;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(window);
        EAW_NewWindowController windowController = new EAW_NewWindowController(sc);
    }
    
}