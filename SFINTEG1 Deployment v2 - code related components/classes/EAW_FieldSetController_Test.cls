@isTest
private class EAW_FieldSetController_Test {
    
    @isTest static void testgetFieldsMethod(){
    
        List<EAW_FieldSetController.DataTableColumns> datacolumn =  EAW_FieldSetController.getFields('EAW_Plan_Guideline__c','Plan_Guideline_Name');
        
        System.assertEquals(true, datacolumn != null);
    }
    
    @isTest static void testgetRecordsMethod(){
        
        List<sObject> obtainedList= EAW_FieldSetController.getRecords('EAW_Plan_Guideline__c','Plan_Guideline_Name', '', new List<String>{''}, new List<String>{''});
        
        System.assertEquals(true, obtainedList != null);
    }      
    
    @isTest static void getLookupRecordsMethod(){
        
        List<sObject> obtainedList = EAW_FieldSetController.getLookupRecords('EAW_Plan_Guideline__c', ' WHERE Name != NULL');
        
        System.assertEquals(true, obtainedList != null);
    }
    
    @isTest static void testgetRecordsCountMethod(){
        
        List<sObject> obtainedList= EAW_FieldSetController.GetRecordsCount('EAW_Plan_Guideline__c','Plan_Guideline_Name', '', new List<String>{''}, new List<String>{''}, new List<String>{''});
        
        System.assertEquals(true, obtainedList != null);
    }
    
    @isTest static void testgetBaseUrlMethod(){
        
        String baseURL = EAW_FieldSetController.getBaseUrl();
        
        System.assertEquals(true, String.isNotBlank(baseURL));
    }
}