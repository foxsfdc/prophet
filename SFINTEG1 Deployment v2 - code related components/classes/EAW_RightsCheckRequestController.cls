public with sharing class EAW_RightsCheckRequestController {


    @AuraEnabled
    public static String sentWindowsRequest(String windowId){
        
            System.debug('windowId:::::'+windowId);
            String returnstring ;
            if(windowId != NULL){
            
                Set<Id> windowIdSet = new Set<Id>();
                windowIdSet.add(windowId);
                System.debug('windowIdSet:::::'+windowIdSet);
                returnstring  = EAW_RightsCheckCalloutController.initiateBtnAction(windowIdSet);
                //System.debug('returned data:::::'+  EAW_RightsCheckCalloutController.sentWindowstoGalilio(windowIdSet));
            }
            return returnstring  ;
    }
    
    @AuraEnabled
    public static List<EAW_Error_Log__c> getErrorLogs(String windowId){
    	
            String returnstring ;
            if(windowId != NULL){
            
                List<EAW_Error_Log__c > existingErrorLogList = [SELECT Id,Window__c,Error_Code__c,Error_Message__c 
                												FROM EAW_Error_Log__c 
                												WHERE Window__c =: windowId 
                												AND CreatedById =: UserInfo.getUserId()
                												ORDER BY CreatedDate DESC NULLS LAST LIMIT 1];
                												
		        System.debug('existingErrorLogList:::::'+existingErrorLogList);
		        if(existingErrorLogList.size() > 0){
		        	  return existingErrorLogList;
		        }else{
		        	return null;
		        }
            }
          return null;
    }
    
}