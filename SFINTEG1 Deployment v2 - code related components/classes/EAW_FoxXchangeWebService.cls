@RestResource(urlMapping='/FoxChange/*')
global with sharing class EAW_FoxXchangeWebService 
{
    @HttpGet
    global static String getAuditHistory() 
    {
        RestRequest request = RestContext.request;
        String promotionDatingID = request.requestURI.substring(
        request.requestURI.lastIndexOf('/')+1);
        system.debug('EAW_FoxXchangeController promotionDatingID: '+promotionDatingID);
        //EAW_Release_Date__c erdc =[ select HEP_Promotion_Dating_Id__c from EAW_Release_Date__c where id=:releaseDateId];
        List<HEP_Promotion_Date_API.AuditHistory> auditHistory = EAW_PromotionDateController.getAuditHistory(promotionDatingID);
        
        String jsonAuditHistory = Json.serialize(auditHistory); 

        return jsonAuditHistory;
    }
}