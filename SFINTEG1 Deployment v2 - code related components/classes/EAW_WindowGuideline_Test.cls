/**
  * Test class for EAW_WindowGuideline
 */
@isTest
private class EAW_WindowGuideline_Test {

    @testSetup static void setupTestData() {
        
        Map<String, Object> dynamicFieldsAndValuesMap; 
        
        // Plan Guideline
        dynamicFieldsAndValuesMap = new Map<String, Object>{ 'Name' => 'Test Plan Guideline', 'Language__c' => 'English;Somali', 'Media__c' => 'Theatrical', 'Release_Type__c' => 'New Release', 'Sales_Region__c' => 'Australia / New Zealand', 'Territory__c' => 'Australia', 'Product_Type__c' => 'Episode', 'Status__c' =>  'Draft'};
        List<EAW_Plan_Guideline__c> planGuideList = (List<EAW_Plan_Guideline__c>)EAW_TestDataFactory.createGenericSameTestRecords( dynamicFieldsAndValuesMap,  'EAW_Plan_Guideline__c', 2, True);
        
        // Window Guideline
        dynamicFieldsAndValuesMap = new Map<String, Object>{ 'Name' => 'Test Window Guideline', 'Window_Type__c' => 'First-Run', 'Media__c' => 'Theatrical', 'Product_Type__c' => 'Episode', 'Status__c' => 'Draft', 'Window_Type__c' => 'Library', 'Window_Guideline_Alias__c' => 'Test Window Guideline Alias'};
        List<EAW_Window_Guideline__c> windowGuideList = (List<EAW_Window_Guideline__c>)EAW_TestDataFactory.createGenericSameTestRecords( dynamicFieldsAndValuesMap,  'EAW_Window_Guideline__c', 1, True);
    }    
    static testMethod void method1() {
        // TO DO: implement unit test 
        EAW_Window_Guideline__c windowGuideline = [SELECT ID FROM EAW_Window_Guideline__c LIMIT 1]; 
        List<EAW_Window_Guideline__c> windowGuideline1 = EAW_TestDataFactory.createWindowGuideLine(1, false);
        EAW_Plan_Guideline__c planGuideline = [SELECT ID FROM EAW_Plan_Guideline__c LIMIT 1];
        EAW_WindowGuideline.insertWindowGuideline(windowGuideline1[0],planGuideline.Id);
        EAW_WindowGuideline.collectPicklistsValues();
        
        List<EAW_Plan_Window_Guideline_Junction__c> planWindowGuidelineList = [SELECT Id, Name FROM EAW_Plan_Window_Guideline_Junction__c];
        System.assertEquals(1, planWindowGuidelineList.size());
    }
}