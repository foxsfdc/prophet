@isTest
public with sharing class EAW_ProphetUpdates_Test {
       
    @isTest
    static void method1(){
    
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI ='/Prophet/*';
        req.httpMethod = 'POST';
        
        RestContext.request = req;
        RestContext.response= res;
        
        List<EAW_ProphetUpdates.windowUpdate> proUpdatewrapperList = new List<EAW_ProphetUpdates.windowUpdate>();
        
        EAW_ProphetUpdates.windowUpdate proUpdatewrapper = new EAW_ProphetUpdates.windowUpdate();
        proUpdatewrapper.windowId = null;
        proUpdatewrapper.startDate = '2018-08-01';
        proUpdatewrapper.endDate = '2018-08-31';
        
        proUpdatewrapperList.add(proUpdatewrapper);
        
        Test.startTest();
        EAW_ProphetUpdates.updateWindows(proUpdatewrapperList);
        Test.stopTest();
        
        List<EAW_Window__c>windows = [SELECT Id, Name FROM EAW_Window__c];
        
        System.assertEquals(True, windows != null);

    } 
    @isTest
    static void method2(){
    
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI ='/Prophet/*'; 
        req.httpMethod = 'POST';
       
        EAW_ProphetUpdates.TitleWrapper prophetwrapper = new EAW_ProphetUpdates.TitleWrapper();
        List<String> planGuidelineIdsList = new List<String>();
        planGuidelineIdsList.add('aA43D0000004CoASAU');
        prophetwrapper.titleId = 'aAD3D000000FfJtWAK';
        prophetwrapper.planGuidelineIds = planGuidelineIdsList;
        
        String JsonMsg = JSON.serialize(prophetwrapper); 
        
        RestContext.request = req;
        req.requestBody = Blob.valueof(JsonMsg);
        RestContext.response= res;
               
        try {
            Test.startTest();
            EAW_ProphetUpdates.buildPlans();
            Test.stopTest();
        }
        catch(Exception ex) {
            System.assert(ex.getmessage().contains('Script-thrown exception'));
        }
    }
}