@RestResource(urlMapping='/contentownerevent')
global with sharing class EAW_ContentOwnerDataController {
	//Collect all modified WG ids with existing Tags
	global Set<Id> wgIds = new Set<Id>();
	//Collect all modified Title ids with existing Tags 
	global Set<Id> rdgIds = new Set<Id>();
	//Collect all modified Title ids with existing Tags
	global Set<id> titleIdSet = new Set<id>();  
  	@HttpPost
    global static String doPost(){
        String status = 'Success';
        EAW_Inbound_Notifications__c eawIN = new EAW_Inbound_Notifications__c(Source_System__c='Content Owner');
        ContentOwnerJson contentOwner;
    	try{
            RestResponse res = RestContext.response;
            String requestBody = RestContext.request.requestBody.toString(); 
            System.debug('EAW_ContentOwnerDataController requestBody'+requestBody);
                  
            String decoded = EncodingUtil.urlDecode(RestContext.request.requestBody.toString(), 'UTF-8');
            contentOwner = (ContentOwnerJson) System.JSON.deserialize(decoded,ContentOwnerJson.class);
            //System.debug('EAW_ContentOwnerDataController requestBody'+system.JSON.serialize(contentOwner));
            //System.debug('EAW_ContentOwnerDataController contentOwner.rstrcnDesc:'+contentOwner.rstrcnDesc+'contentOwner.foxId(): '+contentOwner.foxId);
            if(String.isblank(contentOwner.foxId)){
          	  	status = 'Invalid Request';
          		throw new InvalidRequestException(status);
            }
            eawIN.FOX_ID__c=contentOwner.foxId;
    		eawIN.Content_Owner_Descriptions__c=contentOwner.rstrcnDesc;
            eawIN.Content_Owner_Codes__c=contentOwner.rstrcnCd;
            upsert eawIN;
            //System.debug('EAW_ContentOwnerDataController eawIN'+system.json.serialize(eawIN));
		}catch(exception e){
			system.debug('Exception in EAW_ContentOwnerDataController: '+e.getMessage());
			String message = e.getMessage();
    		if(message!=null && message.length()>10000)message=message.substring(0, 10000);
    		eawIN.Message__c=message;
    		eawIN.Status__c='Failed';
    		if(contentOwner!=null) {
    			eawIN.FOX_ID__c=contentOwner.foxId;
    			eawIN.Content_Owner_Descriptions__c=contentOwner.rstrcnDesc;
    			eawIN.Content_Owner_Codes__c=contentOwner.rstrcnCd;
    		}
    		upsert eawIN;
		}
        return status;
    }
    
    public void processContentOwners(List<EAW_Inbound_Notifications__c> records){
		//system.debug('records: '+system.json.serialize(records));
        if(records!=null && !records.isempty()){
	       Set<String> tagNames = new Set<string>();
	       Set<String> titleIds = new Set<string>();
	       Set<String> titleTagNames = new Set<string>();
	       
	       //Collect TagIds which are existing
           Set<Id> insertTitleTagIdSet = new Set<Id>();
           Set<Id> deleteTitleTagIdSet = new Set<Id>();
           //Collect inserted/deleted Title ids with existing Tags
           Set<id> insertTitleIdSet = new Set<id>();
           Set<id> deleteTitleIdSet = new Set<id>();
          
	       for(EAW_Inbound_Notifications__c eawIN : records){
		       eawIN.Status__c = 'Processing';
		       if(String.isnotblank(eawIN.FOX_ID__c)){
		       		titleIds.add(eawIN.FOX_ID__c);
		       }else{
		       		break;
		       }
		       if(String.isnotblank(eawIN.Content_Owner_Descriptions__c)){
		       		String[] tags = eawIN.Content_Owner_Descriptions__c.split(';');
		       		if(tags!=null && !tags.isEmpty()){
		       			tagNames.addAll(tags);
		       			for(String s: tags)titleTagNames.add(eawIN.FOX_ID__c+'~'+s);
		       		}else{
		       			titleTagNames.add(eawIN.FOX_ID__c);
		       		}
		       }else{
		       		titleTagNames.add(eawIN.FOX_ID__c);
		       }
	       }
	       //system.debug('titleTagNames: '+system.json.serialize(titleTagNames));
	       if(tagNames!=null && !tagNames.isempty() && titleIds!=null && !titleIds.isempty()){
	       		EAW_Tag__c[] tagList = [Select id,Active__c,Sub_Type__c,Name,Tag_Type__c from EAW_Tag__c where name in :tagNames 
        						and Tag_Type__c='Title Tag' and Sub_Type__c='Content Owner'];
        		//System.debug('tagList: '+System.JSON.serialize(tagList));
        		Map<String,Id> existingTagsMap = new Map<String,Id>();
        		if(tagList!=null && !tagList.isEmpty()){
 					for(EAW_Tag__c tag:tagList){
     					existingTagsMap.put(tag.Name,tag.Id);//modify name to existingTagsMap
         			}
         		}
         		//System.debug('existingTagsMap: '+System.JSON.serialize(existingTagsMap));
         		
     			List<EAW_Tag__c> newTags = new List<EAW_Tag__c>();
	    		for(String name:tagNames){
		            EAW_Tag__c tag = null;
		            if(!existingTagsMap.containskey(name)){
		                tag = new EAW_Tag__c();
		                tag.Active__c=true;
		                tag.Name=name;//Code or description
		                tag.Tag_Type__c='Title Tag';
		                tag.Sub_Type__c='Content Owner';
		                newTags.add(tag);
		            }
	    		}
	    		Map<String,Id> latestTagsMap = new Map<String,Id>();
	    		if(!newTags.isempty()){
	    			insert newTags;
	    			for(EAW_Tag__c tag:newTags){
	         			latestTagsMap.put(tag.Name,tag.Id);//create new map for latestTagsMap
	         		}
	         		//System.debug('latestTagsMap: '+System.JSON.serialize(latestTagsMap));
	    		}
	    		
	    		EAW_Title_Tag_Junction__c[] eawTitleTagJunctionList = [SELECT
																			Id, 
																			Tag__c, 
																			Tag__r.name, 
																			Title_Attribute__c,
																			Title_Attribute__r.FOX_ID__c
																		FROM
																			EAW_Title_Tag_Junction__c
																		WHERE
																			Title_Attribute__r.FOX_ID__c  in: titleIds  
																			AND  Tag__r.Tag_Type__c  =  'Title Tag'  
																			AND  Tag__r.Sub_Type__c  =  'Content Owner'];
	    		
	            //System.debug('eawTitleTagJunctionList: '+System.JSON.serialize(eawTitleTagJunctionList));

	    		List<EAW_Title_Tag_Junction__c> newTagTitleJuncList = new List<EAW_Title_Tag_Junction__c>();
	    		List<EAW_Title_Tag_Junction__c> deletedTagTitleJuncList = new List<EAW_Title_Tag_Junction__c>();
	            
	            EAW_Title__c[] titleAttributelist = [Select id,FOX_ID__c from EAW_Title__c where FOX_ID__c in :titleIds];
	            Map<String,EAW_Title__c> titleMap ;
	            if(titleAttributelist!=null && !titleAttributelist.isEmpty()){
	            	titleMap = new Map<String,EAW_Title__c>();
	            	for(EAW_Title__c etc: titleAttributelist)titleMap.put(etc.FOX_ID__c,etc);
	            }
	            //System.debug('titleAttributelist: '+System.JSON.serialize(titleAttributelist));
	            //System.debug('titleMap: '+System.JSON.serialize(titleMap));
	             if(titleMap!=null && !titleMap.isempty()){
					Map<String,EAW_Title_Tag_Junction__c> existingTTJMap = new Map<String,EAW_Title_Tag_Junction__c>();
					if(eawTitleTagJunctionList!=null && !eawTitleTagJunctionList.isEmpty()){	            				
		            	for(EAW_Title_Tag_Junction__c ttj: eawTitleTagJunctionList){
		            		existingTTJMap.put(ttj.Title_Attribute__r.FOX_ID__c+'~'+ttj.Tag__r.name,ttj);
		            	}
	             	}
	             	
	             	//System.debug('existingTTJMap: '+System.JSON.serialize(existingTTJMap));			
	            	for(String titleTag: titleTagNames){
	            		if(existingTTJMap!=null && existingTTJMap.containskey(titleTag)){
	            			existingTTJMap.remove(titleTag);
	            		}else if(titleTag!=null && titleTag.split('~')!=null && titleTag.split('~').size()>1 && titleTag.split('~')[1]!=null && titleTag.split('~')[0]!=null){
	            			if(titleMap.get(titleTag.split('~')[0])!=null){
	            			EAW_Title_Tag_Junction__c eawTitleTagJunction = new EAW_Title_Tag_Junction__c();
		            			if(existingTagsMap.containsKey(titleTag.split('~')[1])){
				                    eawTitleTagJunction.Tag__c=existingTagsMap.get(titleTag.split('~')[1]);
				                    eawTitleTagJunction.Title_Attribute__c=titleMap.get(titleTag.split('~')[0]).id;
		            				
                                //titleTagIdSet.add(existingTagsMap.get(titleTag.split('~')[1]));
                                insertTitleTagIdSet.add(existingTagsMap.get(titleTag.split('~')[1]));
                                //titleIdSet.add(titleMap.get(titleTag.split('~')[0]).id);
                                insertTitleIdSet.add(titleMap.get(titleTag.split('~')[0]).id);
		            			}else if(latestTagsMap.containsKey(titleTag.split('~')[1])){
				                    eawTitleTagJunction.Tag__c=latestTagsMap.get(titleTag.split('~')[1]);
				                    eawTitleTagJunction.Title_Attribute__c=titleMap.get(titleTag.split('~')[0]).id;
		            			}
			                    newTagTitleJuncList.add(eawTitleTagJunction);
	            			}
	            		}
	            	}
	            	//System.debug('newTagTitleJuncList: '+System.JSON.serialize(newTagTitleJuncList));
	            	//System.debug('existingTTJMap: '+System.JSON.serialize(existingTTJMap));
	            	if(existingTTJMap!=null && !existingTTJMap.isEmpty()){
	            		for(String key: existingTTJMap.keySet()){
	            			deletedTagTitleJuncList.add(existingTTJMap.get(key));
	            			
                            //titleTagIdSet.add(existingTTJMap.get(key).Tag__c);
                            deleteTitleTagIdSet.add(existingTTJMap.get(key).Tag__c);
                            //titleIdSet.add(existingTTJMap.get(key).Title_Attribute__c);
                            deleteTitleIdSet.add(existingTTJMap.get(key).Title_Attribute__c);
	            		}
	            	}
	            	//System.debug('deletedTagTitleJuncList: '+System.JSON.serialize(deletedTagTitleJuncList));
		            if(newTagTitleJuncList!=null && !newTagTitleJuncList.isempty()){
		            	//System.debug(' newTagTitleJuncList: '+System.JSON.serialize(newTagTitleJuncList));
		            	insert newTagTitleJuncList;
	            	}
		            if(deletedTagTitleJuncList!=null && !deletedTagTitleJuncList.isempty()){
		            	//System.debug(' deletedTagTitleJuncList: '+System.JSON.serialize(deletedTagTitleJuncList));
		            	delete deletedTagTitleJuncList;
		            }
	            }
			}
			//System.debug('titleTagIdSet: '+System.JSON.serialize(titleTagIdSet));
			//System.debug('titleIdSet: '+System.JSON.serialize(titleIdSet));
            if(insertTitleTagIdSet!=null && !insertTitleTagIdSet.isEmpty() && insertTitleIdSet!=null && !insertTitleIdSet.isEmpty()){
                
                EAW_TitleTagJunctionTriggerHandler ttjHandler = new EAW_TitleTagJunctionTriggerHandler();
                
                ttjHandler.processTitleTagToCheckForPlanQualification(insertTitleTagIdSet,null,insertTitleIdSet,false);
                if(EAW_PlanGuidelineQualifierController.wgIds!=null && !EAW_PlanGuidelineQualifierController.wgIds.isempty()){
                    wgIds.addAll(EAW_PlanGuidelineQualifierController.wgIds);
                }
                
                ttjHandler.filterAndProcessRulesForReQualification(insertTitleTagIdSet,insertTitleIdSet,false);
                if(ttjHandler.wgIds!=null && !ttjHandler.wgIds.isempty()){
                    wgIds.addAll(ttjHandler.wgIds);
                }
                if(ttjHandler.rdgIds!=null && !ttjHandler.rdgIds.isempty()){
                    rdgIds.addAll(ttjHandler.rdgIds);
                }
                titleIdSet.addall(insertTitleIdSet);
                //System.debug('wgIds: '+System.JSON.serialize(wgIds));
                //System.debug('rdgIds: '+System.JSON.serialize(rdgIds));
            }
            if(deleteTitleTagIdSet!=null && !deleteTitleTagIdSet.isEmpty() && deleteTitleIdSet!=null && !deleteTitleIdSet.isEmpty()){
			    
			    EAW_TitleTagJunctionTriggerHandler ttjHandler = new EAW_TitleTagJunctionTriggerHandler();
			    
                ttjHandler.processTitleTagToCheckForPlanQualification(deleteTitleTagIdSet,null,deleteTitleIdSet,true);
				if(EAW_PlanGuidelineQualifierController.wgIds!=null && !EAW_PlanGuidelineQualifierController.wgIds.isempty()){
					wgIds.addAll(EAW_PlanGuidelineQualifierController.wgIds);
				}
				
                ttjHandler.filterAndProcessRulesForReQualification(deleteTitleTagIdSet,deleteTitleIdSet,true);
				if(ttjHandler.wgIds!=null && !ttjHandler.wgIds.isempty()){
					wgIds.addAll(ttjHandler.wgIds);
				}
				if(ttjHandler.rdgIds!=null && !ttjHandler.rdgIds.isempty()){
					rdgIds.addAll(ttjHandler.rdgIds);
				}
                titleIdSet.addall(deleteTitleIdSet);
				//System.debug('wgIds: '+System.JSON.serialize(wgIds));
				//System.debug('rdgIds: '+System.JSON.serialize(rdgIds));
			}
			//System.debug('wgIds: '+System.JSON.serialize(wgIds));
			//System.debug('rdgIds: '+System.JSON.serialize(rdgIds));
		}
	}
    
	public class ContentOwnerJson{
        public String foxId;
		public Integer businessUnitId;
		public String rstrcnCdId;
		public String rstrcnCd;
		public String rstrcnDesc;
		
    }
    
    public class InvalidRequestException extends Exception {

    }
}