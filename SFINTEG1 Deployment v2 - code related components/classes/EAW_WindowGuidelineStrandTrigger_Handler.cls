public with sharing class EAW_WindowGuidelineStrandTrigger_Handler {
	
	/*
	Please note, if you are creating any new method in this handler, make sure add following flag at starting of the method
	to skip the trigger logic.
	if(!checkOuboundRecursiveData.bypassTriggerFire) 
	*/
    
    // This method was implemented to share only specific records to Window User in SFINTEG for successful UAT testing. This was commented here and need to be removed in future
    /*public void setWindowGuidelineTemp(List<EAW_Window_Guideline_Strand__c> windowGLStrands){
    
        Map<Id, String> windowGlIdWglTemp = new Map<Id, String>();
        List<EAW_Window_Guideline__c> windowGuidelines = new List<EAW_Window_Guideline__c>();
        
        Set<String> territorySet = new Set<String>{ 
            'South Africa', 'Australia', 'Belgium', 'Luxembourg', 'Netherlands', 'Brazil', 'Canada +', 'Chile', 
            'Colombia', 'Panama', 'Anguilla', 'Antigua', 'Argentina', 'Aruba', 'Bahamas, The +', 'Barbados', 'Barbuda', 'Belize', 
            'Bolivia', 'Bonaire', 'British Virgin Islands +', 'Cayman Islands ^', 'Costa Rica', 'Curacao', 'Dominica', 
            'Dominican Republic', 'Ecuador', 'El Salvador', 'Grenada', 'Grenadines', 'Guadeloupe', 'Guatemala', 'Guyana', 'Haiti', 
            'Honduras', 'Jamaica', 'Martinique', 'Mexico', 'Montserrat', 'Nevis', 'Nicaragua', 'Paraguay', 'Peru', 'Saba', 
            'Saint Eustatius', 'Saint Kitts', 'Saint Lucia', 'Saint Maarten', 'Saint Vincent', 'Suriname', 'Trinidad and Tobago', 
            'Turks And Caicos Islands ^', 'Uruguay', 'Venezuela', 'Hong Kong', 'Indonesia +', 'Malaysia', 'Mongolia', 'Myanmar (Burma)', 
            'Philippines', 'Singapore', 'Thailand', 'Vietnam', 'Brunei', 'Cambodia (Kampuchea)', 'Fiji', 'Laos', 'Macau', 'Maldives', 
            'Mauritius', 'Myanmar (Burma)', 'Papua New Guinea'
        };
    
        for(EAW_Window_Guideline_Strand__c wgls : windowGLStrands){
        
            Boolean containsTerritory = false;    
            if(String.isNotBlank(wgls.EAW_Window_Guideline__c) && String.isNotBlank(wgls.Territory__c)){
                
                if(!windowGlIdWglTemp.containsKey(wgls.EAW_Window_Guideline__c)){
                
                    for(String ter : wgls.Territory__c.split(';')){
                    
                        System.debug('EAW_Window_Guideline__c :::: ' + wgls.EAW_Window_Guideline__c + ' ::::Territory__c:::: ' + ter);
                        
                        if(territorySet.contains(ter)){
                            windowGlIdWglTemp.put(wgls.EAW_Window_Guideline__c, ter);
                            containsTerritory = true;
                            break;
                        }
                    }
                    
                    System.debug('containsTerritory ::::: ' + containsTerritory );
                    
                    if(!containsTerritory){
                        windowGlIdWglTemp.put(wgls.EAW_Window_Guideline__c, null);
                    }
                }
            }
        }
        System.debug('windowGlIdWglTemp:::::: ' + windowGlIdWglTemp);
        
        for(EAW_Window_Guideline__c wgl : [SELECT Id, Window_Guideline_Temp__c FROM EAW_Window_Guideline__c 
                                           WHERE Id IN :windowGlIdWglTemp.keySet()]){
            
            wgl.Window_Guideline_Temp__c = windowGlIdWglTemp.get(wgl.Id);
            windowGuidelines.add(wgl);
            
        }
        
        if(windowGuidelines != null && windowGuidelines.size() > 0){
            update windowGuidelines;
        }
    }
    */
    
    //LRCC-1009, LRCC-1358
    public void preventRecordChangeWhenActiveStatus(List<EAW_Window_Guideline_Strand__c> windowGLStrands){
        if(!checkOuboundRecursiveData.bypassTriggerFire){
	        Set<Id> windowGLIds = new Set<Id> ();
	        Set<Id> activeWindowGLs = new Set<Id> ();
	        
	        for(EAW_Window_Guideline_Strand__c wgs :windowGLStrands) {
	            if(wgs.EAW_Window_Guideline__c != null) {
	                windowGLIds.add(wgs.EAW_Window_Guideline__c);
	            }
	        }
	        
	        for(EAW_Window_Guideline__c wg : [SELECT Id, Status__c FROM EAW_Window_Guideline__c WHERE Id IN :windowGLIds and Status__c = 'Active']) {
	            activeWindowGLs.add(wg.Id);
	        }
	        
	        for(EAW_Window_Guideline_Strand__c wgs : windowGLStrands) {
	            if(activeWindowGLs.contains(wgs.EAW_Window_Guideline__c)) {
	                wgs.EAW_Window_Guideline__c.addError('You can\'t Add/Edit a Window Guideline Strand under Active Window Guidelines');
	            }
	        }
        }
    }
    
     //LRCC-1653 - -In this ticket once passed QA it with remove the commented code.
    /*public void windowStrandOffsetUpdate(Map<Id,EAW_Window_Guideline_Strand__c> oldWindowGLStrands, Map<Id,EAW_Window_Guideline_Strand__c> newWindowGLStrands){
    
        Set<Id> WGKeySet = new Set<Id>();
        Set<Id> changedWGStrandSet = new Set<Id>();
        List<EAW_Window_Strand__c> windowStrandList = new List<EAW_Window_Strand__c>();
        WGKeySet = oldWindowGLStrands.keySet();
        if(WGKeySet.size() > 0){
            for(Id WgID : WGKeySet){
                if(oldWindowGLStrands.get(WgID).Start_Date_Months_Offset__c != newWindowGLStrands.get(WgID).Start_Date_Months_Offset__c || 
                   oldWindowGLStrands.get(WgID).Start_Date_Days_Offset__c != newWindowGLStrands.get(WgID).Start_Date_Days_Offset__c || 
                   oldWindowGLStrands.get(WgID).End_Date_Months_Offset__c != newWindowGLStrands.get(WgID).End_Date_Months_Offset__c ||
                   oldWindowGLStrands.get(WgID).End_Date_Days_Offset__c != newWindowGLStrands.get(WgID).End_Date_Days_Offset__c || 
                   oldWindowGLStrands.get(WgID).Start_Date_Source__c != oldWindowGLStrands.get(WgID).Start_Date_Source__c || 
                   oldWindowGLStrands.get(WgID).End_Date_Source__c != oldWindowGLStrands.get(WgID).End_Date_Source__c){
                      changedWGStrandSet.add(WgID);
                  }
            }
            if(changedWGStrandSet.size() > 0){
                windowStrandList = [SELECT Id,Start_Date_Source__c,End_Date_Source__c,Window_Guideline_Strand__c,Start_Date_Months_After__c,Start_Date_Days_After__c,End_Date_Months_After__c,End_Date_Days_After__c,Is_Overridden__c 
                                   FROM EAW_Window_Strand__c WHERE Window_Guideline_Strand__c IN :changedWGStrandSet AND Is_Overridden__c = false];
                if(windowStrandList.size() > 0){
                    for(EAW_Window_Strand__c windowStrand : windowStrandList){
                        windowStrand.Start_Date_Months_After__c = newWindowGLStrands.get(windowStrand.Window_Guideline_Strand__c).Start_Date_Months_Offset__c;
                        windowStrand.Start_Date_Days_After__c = newWindowGLStrands.get(windowStrand.Window_Guideline_Strand__c).Start_Date_Days_Offset__c;
                        windowStrand.End_Date_Months_After__c = newWindowGLStrands.get(windowStrand.Window_Guideline_Strand__c).End_Date_Months_Offset__c;
                        windowStrand.End_Date_Days_After__c = newWindowGLStrands.get(windowStrand.Window_Guideline_Strand__c).End_Date_Days_Offset__c;
                        windowStrand.Start_Date_Source__c = newWindowGLStrands.get(windowStrand.Window_Guideline_Strand__c).Start_Date_Source__c;
                        windowStrand.End_Date_Source__c = newWindowGLStrands.get(windowStrand.Window_Guideline_Strand__c).End_Date_Source__c;
                    }
                    update windowStrandList;
                }
            }
        }
    }*/
    //LRCC-1432
    public void preventDeletionofStrandsRelatedToActiveGuideline(List<EAW_Window_Guideline_Strand__c> OldwindowGLStrands) {
        if(!checkOuboundRecursiveData.bypassTriggerFire){
	        Set<Id> windowGuideLineActiveIdSet = new Set<Id>();
	        Map<Id, String> activeWindowGuidlineandStrandMap = new Map<Id, String>();
	        
	        for(EAW_Window_Guideline_Strand__c wgsIns : OldwindowGLStrands) {
	            if(wgsIns.EAW_Window_Guideline__c != null) {
	                
	                windowGuideLineActiveIdSet.add(wgsIns.EAW_Window_Guideline__c );
	            }           
	        }
	        if(windowGuideLineActiveIdSet.isEmpty() == False) {
	            for(EAW_Window_Guideline__c wgIns : [SELECT Id, Name, Status__c from EAW_Window_Guideline__c WHERE Id IN :windowGuideLineActiveIdSet]) {
	                
	                activeWindowGuidlineandStrandMap.put(wgIns.Id, wgIns.Status__c);
	            }
	        }
	        
	        if(activeWindowGuidlineandStrandMap.isEmpty() == False) {
	            
	            for(EAW_Window_Guideline_Strand__c wgins : OldwindowGLStrands) {
	                
	                if(activeWindowGuidlineandStrandMap.get(wgins.EAW_Window_Guideline__c ) == 'Active') {
	                    
	                    wgins.addError('Window guideline strand cannot be deleted since it has associated active window guideline to it');
	                }
	            }
	        }
        }
    }
    
    //LRCC-1399
    public void limitStrandWithNoSpecificeLanguage(List<EAW_Window_Guideline_Strand__c> windowGLStrandsList) {
        if(!checkOuboundRecursiveData.bypassTriggerFire){  
	        for(EAW_Window_Guideline_Strand__c wgsIns : windowGLStrandsList) {
	         //LRCC-1487                       
	         	if(wgsIns.Language__c !=  null && wgsIns.Language__c.split(';').size() > 1 && wgsIns.Language__c.split(';').contains('No Specific Language')) {
	               
	               wgsIns.addError('The window guideline strand cannot be created or updated with combination of "No Specific Language".Please select the specific language or only the "No Specific Language"');
	           } 
	        }
	    }
    }
    
    /*public static void setWindowStrandDates(List<EAW_Window_Guideline_Strand__c> oldwindowGLStrandsList,List<EAW_Window_Guideline_Strand__c> newwindowGLStrandsList){
        Set<Id> windowGLStrandSet = new Set<Id>();
        for(Integer i=0;i<oldwindowGLStrandsList.size();i++){
            if(oldwindowGLStrandsList[i].Start_Date_Months_Offset__c != newwindowGLStrandsList[i].Start_Date_Months_Offset__c || oldwindowGLStrandsList[i].End_Date_Months_Offset__c != newwindowGLStrandsList[i].End_Date_Months_Offset__c){
                windowGLStrandSet.add(newwindowGLStrandsList[i].Id);
            }
        }
        if(windowGLStrandSet.size() > 0){
            List<EAW_Window_Strand__c> windowStrandList = [SELECT Id,Window_Guideline_Strand__c FROM EAW_Window_Strand__c WHERE Window_Guideline_Strand__c IN :windowGLStrandSet];
            if(windowStrandList != null && windowStrandList.size() > 0){
                EAW_WindowStrandTrigger_Handler.setWindowStrandDates(windowStrandList,false);
            }
        }
    }*/
    
    //Notifications related implementation: LRCC-1050
    public void sentNotificationToRReviewGroup(List<EAW_Window_Guideline_Strand__c> triggerOld, List<EAW_Window_Guideline_Strand__c> triggerNew) {
        if(!checkOuboundRecursiveData.bypassTriggerFire){
	        String bodyContent = '';
	        //Get custom settings - ON/OFF switches for notification.
	        EAW_Notification_Status__c ns = EAW_Notification_Status__c.getOrgDefaults();
	        //Get chatter group Id.
	        List<CollaborationGroup> lstChatterGroup = [SELECT Id, Name FROM CollaborationGroup WHERE Name = 'Rights Review'];
	        
	        List<FeedItem> feedItems = new List<FeedItem>();
	        
	        try {
	            
	            if(!lstChatterGroup.isEmpty()) {
	            
	                for(Integer i = 0; i < triggerNew.size(); i++) {
	                    
	                    EAW_Window_Guideline_Strand__c oldWS = triggerOld.get(i);
	                    EAW_Window_Guideline_Strand__c newWS = triggerNew.get(i);
	                    
	                    if(ns.RS_Category_Notification__c == true) {
	                        
	                        //NA-36
	                        if(oldWS != null 
	                            && newWS.Imported__c == true
	                            && ns.RSISChangeNotification__c == true) {
	                            
	                            if(String.isNotBlank(bodyContent)) {
	                                bodyContent += 'A Previously imported window strand has been modified.'+'\n\n'+newWS.SFDC_Base_Instance__c+'/'+newWS.Id;
	                            }
	                            else {
	                                bodyContent += '\n\n'+ 'A Previously imported window strand has been modified.'+'\n\n'+newWS.SFDC_Base_Instance__c+'/'+newWS.Id;
	                            }
	                        }
	                    }
	                }
	                
	                if(String.isNotBlank(bodyContent)) {
	                        
	                    String temp = bodyContent;
	                    
	                    if(bodyContent.length() < 10000) {
	                        bodyContent = temp.substring(0, temp.length());
	                    } 
	                    else {
	                        bodyContent = temp.substring(0,9999);    
	                    }
	                                        
	                    feedItems.add(
	                        new FeedItem(
	                            Body = bodyContent,
	                            ParentId = lstChatterGroup[0].Id
	                        )
	                    );  
	                            
	                    if(feedItems != null && feedItems.size() > 0) {
	                        insert feedItems;
	                    }
	                }
	                
	                if(feedItems != null && feedItems.size() > 0) {
	                    insert feedItems;
	                }
	            }
	        }
	        catch(Exception e) {
	            System.debug('The following exception has occurred: ' + e.getMessage());
	        }
        }  
    }
    
    //LRCC-1780
    public void sendData(List<EAW_Window_Guideline_Strand__c> newWgsList,String operation){
    	if(!checkOuboundRecursiveData.bypassTriggerFire){
	    	Set<Id> modifiedWgsIds = new Set<ID>();
	    	for(Integer i = 0; i < newWgsList.size(); i++) {
	    		EAW_Window_Guideline_Strand__c newWgs = newWgsList.get(i);
	            if(!(operation == 'Update' && newWgs.Soft_Deleted__c=='TRUE'))
	            	modifiedWgsIds.add(newWgs.Id);            
	        }
	        
	        if (!(System.isBatch() || System.isFuture() || System.isScheduled() || System.isQueueable())) {
	       		EAW_WindowGuidelineStrands_Outbound.sendDataAsync(modifiedWgsIds,operation);
			} else {
			    EAW_WindowGuidelineStrands_Outbound.sendDataSync(modifiedWgsIds,operation);
			} 
    	} 
    }
}