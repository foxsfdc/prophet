global class EAW_AutoFirmBatchController implements Database.Batchable<sObject>, Database.Stateful{

    global String releaseDateQuery;
    global Date lastday;
    global Set<Id> releaseDateGuidelineIds = new Set<Id>();
    global Set<Id> titleIds = new Set<Id>();
    global EAW_AutoFirmBatchController( String releaseDateQueryBatch,  Date lastdayBatch){
    
    
        System.debug('::::::releaseDateQuery::::'+releaseDateQueryBatch);
        System.debug('::::::lastday::::'+lastdayBatch);
      
        releaseDateQuery = releaseDateQueryBatch;
        lastday= lastdayBatch;
    
        System.debug('::::::releaseDateQuery::after::'+releaseDateQuery);
        System.debug('::::::lastday::after::'+lastday);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
       System.debug('::::::releaseDateQuery::start::'+releaseDateQuery);
        System.debug('::::::lastday::sata::'+lastday);
        String query = releaseDateQuery;
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
    
        System.debug(':::scope:auto::' + scope);
        System.debug(':::scope::lenght:auto:::' + scope.size());
        
        Set<String> releaseDateTypesName = new Set<String> ();
        Map<String,EAW_Release_Date_Type__c> releaseDateTypeNameWithInst = new Map<String,EAW_Release_Date_Type__c>();
        List<EAW_Release_Date__c> releaseDateList = new  List<EAW_Release_Date__c>();
        Map<String,String> releaseDateIdWithName= new Map<String,String>();
        
        List<EAW_Release_Date__c> releaseDateInstantsList = new  List<EAW_Release_Date__c>();
        releaseDateInstantsList = scope;
        
        for(EAW_Release_Date__c releaseDateInst :releaseDateInstantsList ){
        
            releaseDateInst.Status__c = 'Firm'; 
            releaseDateList.add(releaseDateInst); 
            
            if(!releaseDateIdWithName.containsKey(releaseDateInst.Id)){
            
                releaseDateIdWithName.put(releaseDateInst.Id,releaseDateInst.Name);
            }
            //LRCC-611 - This is for downstream Date calculation
            if(releaseDateInst.Release_Date_Guideline__c != NULL){
            
                 releaseDateGuidelineIds.add(releaseDateInst.Release_Date_Guideline__c);
            }
            if(releaseDateInst.Title__c != NULL){
            
                 titleIds.add(releaseDateInst.Title__c);
            }
        }
        
        System.debug('releaseDateList::::auto:::'+releaseDateList);
        if(releaseDateList != NULL && releaseDateList.size() > 0) {
        
            checkRecursiveData.bypassDateCalculation = true;
            System.debug('releaseDateList::::sss::auto:'+releaseDateList);
            List<Database.SaveResult> updateResults = database.update(releaseDateList,false);
        }
    }  
    
    global void finish(Database.BatchableContext BC){
    
        System.debug('releaseDateGuidelineIds::::auto:::'+releaseDateGuidelineIds);
        System.debug('titleIds::::auto:::'+titleIds);
        /*EAW_CreateRDForNewRDGBatch releaseDateBatch = new EAW_CreateRDForNewRDGBatch(releaseDateGuidelineIds, titleIds);
        releaseDateBatch.titleIdSet = titleIds;
        releaseDateBatch.releaseDateGuidelineIdSet = releaseDateGuidelineIds;
        releaseDateBatch.windowGuidelineIdSet = NULL;
        ID batchprocessid = Database.executeBatch(releaseDateBatch, 200);*/
        
        EAW_RDGDateCalculationBatchClass releaseDateBatch = new EAW_RDGDateCalculationBatchClass(releaseDateGuidelineIds);
        releaseDateBatch.titleIdSet = titleIds;
        releaseDateBatch.windowGuidelineIdSet = NULL;
        ID batchprocessid = Database.executeBatch(releaseDateBatch, 200);
    }
}