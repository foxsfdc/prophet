@isTest
public class EAW_DateGuidelineAffectedCntrl_Test {
	
    @isTest static void getReleaseDatesMapFromOperandRDG_Test(){
        EAW_TestDataFactory.createEAWCustomer(1, true);
        List<EAW_Customer__c> customerList = [SELECT Id, Name FROM EAW_Customer__c LIMIT 1];
        
        EAW_Release_Date_Guideline__c releaseDateGuideLine = new EAW_Release_Date_Guideline__c( Product_Type__c = 'Compilation Episode', Territory__c = 'United States +', Language__c = 'English', Customers__c = customerList[0].Name, EAW_Release_Date_Type__c = 'Television');
        insert releaseDateGuideLine;
        
        EAW_Release_Date_Guideline__c conditionalOperand = new EAW_Release_Date_Guideline__c( Product_Type__c = 'Compilation Episode', Territory__c = 'United States +', Language__c = 'English', Customers__c = customerList[0].Name, EAW_Release_Date_Type__c = 'EST-HD');
        insert conditionalOperand;
        
        EAW_Title__c title = new EAW_Title__c();
        title.Name = 'Test Title';
        insert title;
        
        EAW_Release_Date__c releaseDate = new EAW_Release_Date__c();
        releaseDate.Title__c = title.Id;
        releaseDate.Release_Date__c = system.today();
        releaseDate.Release_Date_Guideline__c = conditionalOperand.Id;
        insert releaseDate;
        
        String rule = '{"sObjectType":"EAW_Rule__c"}';
        String ruleDetail = '[{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Conditional_Operand_Id__c":"aA73D0000004m3gSAA","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Alto Adige Region / Maori / Direct to Video"},"dateCalcs":[],"nodeCalcs":[]}]';
        EAW_TestRuleDataFactory.saveRuleAndRuleDetails(rule,ruleDetail,true,releaseDateGuideLine.Id,null,conditionalOperand.Id);
        EAW_DateGuidelineAffectedWindowsPopup.getReleaseDatesMapFromOperandRDG(releaseDateGuideLine.Id,title.Id);
    }
    @isTest static void getAffectedWindowRecords_Test(){
        
        EAW_TestDataFactory.createEAWCustomer(1, true);
        List<EAW_Customer__c> customerList = [SELECT Id, Name FROM EAW_Customer__c LIMIT 1];
        
        EAW_Release_Date_Guideline__c releaseDateGuideLine = new EAW_Release_Date_Guideline__c( Product_Type__c = 'Compilation Episode', Territory__c = 'United States +', Language__c = 'English', Customers__c = customerList[0].Name, EAW_Release_Date_Type__c = 'Television');
        insert releaseDateGuideLine;
        
        EAW_Release_Date_Guideline__c conditionalOperand = new EAW_Release_Date_Guideline__c( Product_Type__c = 'Compilation Episode', Territory__c = 'United States +', Language__c = 'English', Customers__c = customerList[0].Name, EAW_Release_Date_Type__c = 'EST-HD');
        insert conditionalOperand;
        
        EAW_Title__c title = new EAW_Title__c();
        title.Name = 'Test Title';
        insert title;
        
        EAW_Release_Date__c releaseDate = new EAW_Release_Date__c();
        releaseDate.Title__c = title.Id;
        releaseDate.Release_Date__c = system.today();
        releaseDate.Release_Date_Guideline__c = conditionalOperand.Id;
        insert releaseDate;
        
        String rule = '{"sObjectType":"EAW_Rule__c"}';
        String ruleDetail = '[{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Conditional_Operand_Id__c":"aA73D0000004m3gSAA","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Alto Adige Region / Maori / Direct to Video"},"dateCalcs":[],"nodeCalcs":[]}]';
        EAW_TestRuleDataFactory.saveRuleAndRuleDetails(rule,ruleDetail,true,releaseDateGuideLine.Id,null,conditionalOperand.Id);
        
        EAW_Window_Guideline__c newWg = new EAW_Window_Guideline__c ( Status__c = 'Draft', Product_Type__c = 'Compilation Episode', Window_Type__c = 'First-Run', Window_Guideline_Alias__c = 'Test');
        insert newWg; 
        EAW_Window_Guideline_Strand__c windowGuideLineStrand =  new EAW_Window_Guideline_Strand__c(Language__c = 'Tamil',Media__c='Basic TV	',Territory__c='Afghanistan', EAW_Window_Guideline__c = newWg.Id, License_Type__c = 'Exhibition License');
        insert windowGuideLineStrand;
        newWg.Status__c = 'Active';
        update newWg;
        
        String ruleContent = '{"sObjectType":"EAW_Rule__c"}';
        String ruleDetailContent = '[{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Conditional_Operand_Id__c":"aA73D0000004m3gSAA","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Alto Adige Region / Maori / Direct to Video"},"dateCalcs":[],"nodeCalcs":[]}]';
        EAW_TestRuleDataFactory.saveRuleAndRuleDetails(ruleContent,ruleDetailContent,false,newWg.Id,'Start Date',conditionalOperand.Id);
        
        EAW_Window__c window = new EAW_Window__c (Name='Test E', EAW_Title_Attribute__c = title.Id, Window_Type__c = 'Library',EAW_Window_Guideline__c = newWg.Id);
        insert window;
        
        EAW_DateGuidelineAffectedWindowsPopup.getAffectedWindowRecords(conditionalOperand.Id,title.Id);
        
    }
}