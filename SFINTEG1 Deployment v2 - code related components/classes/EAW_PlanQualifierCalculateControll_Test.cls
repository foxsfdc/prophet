@isTest
public class EAW_PlanQualifierCalculateControll_Test {
    
    @isTest static void testCase(){
    
        EAW_PlanQualifierCalculationController planqualify = new EAW_PlanQualifierCalculationController();
    
        planqualify.arithmeticIntegerConditionalChecking(1, '<', 2);
        planqualify.arithmeticIntegerConditionalChecking(1, '<=', 2);
        planqualify.arithmeticIntegerConditionalChecking(1, '>', 2);
        planqualify.arithmeticIntegerConditionalChecking(1, '>=', 2);
        planqualify.arithmeticIntegerConditionalChecking(1, '=', 2);
    }
    
    @isTest static void testCaseDecimal(){
    
        EAW_PlanQualifierCalculationController planqualify = new EAW_PlanQualifierCalculationController();
    
        planqualify.arithmeticDecimalConditionalChecking(1.0, '<', 2.0);
        planqualify.arithmeticDecimalConditionalChecking(1.0, '<=', 2.0);
        planqualify.arithmeticDecimalConditionalChecking(1.0, '>', 2.0);
        planqualify.arithmeticDecimalConditionalChecking(1.0, '>=', 2.0);
        planqualify.arithmeticDecimalConditionalChecking(1.0, '=', 2.0);
    }
    
    @isTest static void testCaseDate(){
    
        EAW_PlanQualifierCalculationController planqualify = new EAW_PlanQualifierCalculationController();
    
        planqualify.arithmeticDateConditionalChecking(system.today(), '<', system.today()+10);
        planqualify.arithmeticDateConditionalChecking(system.today(), '<=', system.today()+10);
        planqualify.arithmeticDateConditionalChecking(system.today(), '>', system.today()+10);
        planqualify.arithmeticDateConditionalChecking(system.today(), '>=', system.today()+10);
        planqualify.arithmeticDateConditionalChecking(system.today(), '=', system.today()+10);
    }
}