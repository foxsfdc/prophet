public class EAW_ResultFilterController {

    @AuraEnabled
    public static Map<String, RecordsList> getObjectNames (List<String> objects) {
    
        Map<String, RecordsList> records = new Map<String, RecordsList>();
        string query;
    
        if(objects.size() > 0) {
            
            for(String obj : objects) {
                
                if(obj.contains('__c')) {
                    if(Schema.getGlobalDescribe().get(obj) != null) {
                        query = 'SELECT Name FROM ' + obj + ' ORDER BY Name ';
                        records.put(obj, new RecordsList(Database.query(query)));
                    } else {
                        List<SerializableSelectOption> options = new List<SerializableSelectOption>();
                        List<Schema.PicklistEntry> ple = Schema.getGlobalDescribe().get('EAW_Window__c').getDescribe().fields.getMap().get(obj).getDescribe().getPicklistValues();
                        for( Schema.PicklistEntry pValues : ple) {
                            if( pValues.isActive() ) {
                                SerializableSelectOption wrapperValue = new SerializableSelectOption(pValues.getValue(), pValues.getLabel());
                                options.add(wrapperValue);
                            }
                        }
                        records.put(obj, new RecordsList(options));
                    }
                    
                } else if(obj.contains('Tag')) {
                    query = 'SELECT Name FROM EAW_Tag__c WHERE Tag_Type__c = :obj' + ' ORDER BY Name ';
                    records.put(obj.deleteWhitespace(), new RecordsList(Database.query(query)));
                }
            }
        }
        
        return records;
    }
    
    public class RecordsList {
    
        @AuraEnabled
        public List<SObject> sObjectRecords { get; set; }
        @AuraEnabled
        public List<SerializableSelectOption> picklistValues { get; set; }
        
        public RecordsList(List<SObject> sObjectRecords) {
            this.sObjectRecords = sObjectRecords;
        }
        public RecordsList(List<SerializableSelectOption> picklistValues) {
            this.picklistValues = picklistValues;
        }
    }
    
     public class SerializableSelectOption {
        
        @AuraEnabled public String value;
        @AuraEnabled public String label;
        
        public SerializableSelectOption(String value, String label) {
            this.value = value;
            this.label = label;
        }
    }
}