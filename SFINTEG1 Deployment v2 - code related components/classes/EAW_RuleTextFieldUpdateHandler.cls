public class EAW_RuleTextFieldUpdateHandler {
	
    public static void guidelineRuleTextFieldUpdate(Set<String> conditionalOperandIdSet,Map<String,String> guidelineNameMap){
        
        
        List<EAW_Rule__c> conditionalOperandRG = new List<EAW_Rule__c>();
        List<EAW_Rule__c> conditionalOperandWG = new List<EAW_Rule__c>();
        
        Set<String> ruleSet = new Set<String>();
        
        system.debug('::::::::::guidelineNameMap::::::'+guidelineNameMap);
        if(conditionalOperandIdSet.size() > 0){
            String searchword = String.join(new List<String>(conditionalOperandIdSet), ' OR ');  
            system.debug(':::searchword::::'+searchword);
            List<List<sObject>> searchResult = [FIND :searchword IN ALL FIELDS RETURNING EAW_Rule_Detail__c(Id,Rule_No__c),EAW_Rule__c];
            System.debug(':::::searchResult:::::::'+searchResult);
            for(List<sObject> sObjectList : searchResult){
                for(sObject ruleRuleDetail : sObjectList){
                    if(ruleRuleDetail.getSObjectType().getDescribe().getName() == 'EAW_Rule__c'){
                        ruleSet.add(ruleRuleDetail.Id);
                    } else if(ruleRuleDetail.get('Rule_No__c') != null){
                        ruleSet.add(String.valueof(ruleRuleDetail.get('Rule_No__c')));
                    }
                }
            }
            system.debug('ruleSet:::'+ruleSet);
            if(ruleSet.size() > 0){
                operandUsedTextFieldUpdate(ruleSet,guidelineNameMap);
            }   
        }
    }
    public static void operandUsedTextFieldUpdate(Set<String> ruleSet,Map<String,String> guidelineNameMap){
        
        List<EAW_Rule__c> conditionalOperandRG = new List<EAW_Rule__c>();
        List<EAW_Rule__c> conditionalOperandWG = new List<EAW_Rule__c>();
        List<EAW_Release_Date_Guideline__c> RGsToUpdate = new List<EAW_Release_Date_Guideline__c>();
        List<EAW_Window_Guideline__c> WGsToUpdate = new List<EAW_Window_Guideline__c>();
        
        if(ruleSet.size() > 0){
            Id RGRecordTypeId = Schema.SObjectType.EAW_Rule__c.getRecordTypeInfosByName().get('Release Date Calculation').getRecordTypeId();
            Id WGRecordTypeId = Schema.SObjectType.EAW_Rule__c.getRecordTypeInfosByName().get('Window Date Calculation').getRecordTypeId();
            conditionalOperandRG = [SELECT Id,Release_Date_Guideline__c,Release_Date_Guideline__r.All_Rules__c FROM EAW_Rule__c WHERE Id IN :ruleSet AND RecordTypeId = :RGRecordTypeId];
            conditionalOperandWG = [SELECT Id,Window_Guideline__c,Window_Guideline__r.Start_Date_Rule__c,Window_Guideline__r.End_Date_Rule__c,Window_Guideline__r.Outside_Date_Rule__c,Window_Guideline__r.Tracking_Date_Rule__c FROM EAW_Rule__c WHERE Id IN :ruleSet AND RecordTypeId = :WGRecordTypeId];
            system.debug('::::::conditionalOperandRG::::::'+conditionalOperandRG);
            system.debug('::::::::conditionalOperandWG:::::'+conditionalOperandWG);
            Map<Id,EAW_Window_Guideline__c> WGKeyMap = new Map<Id,EAW_Window_Guideline__c>();
            
            for(String oldName : guidelineNameMap.keySet()){
                if(conditionalOperandRG != null && conditionalOperandRG.size() > 0){
                    for(EAW_Rule__c RGRule : conditionalOperandRG){
                        if(RGRule.Release_Date_Guideline__r.All_Rules__c != null){
                            system.debug('::::::'+RGRule.Release_Date_Guideline__r.All_Rules__c.contains(oldName));
                        }
                        if(RGRule.Release_Date_Guideline__r.All_Rules__c != null && RGRule.Release_Date_Guideline__r.All_Rules__c.contains(oldName)){
                            EAW_Release_Date_Guideline__c newRG = new EAW_Release_Date_Guideline__c();
                            newRG.All_Rules__c = RGRule.Release_Date_Guideline__r.All_Rules__c.replace(oldName,guidelineNameMap.get(oldName));
                            newRG.Id = RGRule.Release_Date_Guideline__c;
                            RGsToUpdate.add(newRG);
                        }
                    }
                }
                
                if(conditionalOperandWG != null && conditionalOperandWG.size() > 0){
                    for(EAW_Rule__c WGRule : conditionalOperandWG){
                        EAW_Window_Guideline__c newWG = new EAW_Window_Guideline__c();
                        
                        if(WGRule.Window_Guideline__r.Start_Date_Rule__c != null && WGRule.Window_Guideline__r.Start_Date_Rule__c.contains(oldName)){
                            newWG.Start_Date_Rule__c = WGRule.Window_Guideline__r.Start_Date_Rule__c.replace(oldName,guidelineNameMap.get(oldName));
                        }
                        if(WGRule.Window_Guideline__r.End_Date_Rule__c != null && WGRule.Window_Guideline__r.End_Date_Rule__c.contains(oldName)){
                            newWG.End_Date_Rule__c = WGRule.Window_Guideline__r.End_Date_Rule__c.replace(oldName,guidelineNameMap.get(oldName));
                        }
                        if(WGRule.Window_Guideline__r.Outside_Date_Rule__c != null && WGRule.Window_Guideline__r.Outside_Date_Rule__c.contains(oldName)){
                            newWG.Outside_Date_Rule__c = WGRule.Window_Guideline__r.Outside_Date_Rule__c.replace(oldName,guidelineNameMap.get(oldName));
                        }
                        if(WGRule.Window_Guideline__r.Tracking_Date_Rule__c != null && WGRule.Window_Guideline__r.Tracking_Date_Rule__c.contains(oldName)){
                            newWG.Tracking_Date_Rule__c = WGRule.Window_Guideline__r.Tracking_Date_Rule__c.replace(oldName,guidelineNameMap.get(oldName));
                        }
                        if(newWG.Start_Date_Rule__c != null || newWG.End_Date_Rule__c != null || newWG.Outside_Date_Rule__c != null || newWG.Tracking_Date_Rule__c != null){
                            if(WGKeyMap.containsKey(WGRule.Window_Guideline__c)){
                                EAW_Window_Guideline__c wg = WGKeyMap.get(WGRule.Window_Guideline__c);
                                if(newWG.Start_Date_Rule__c != null){
                                    wg.Start_Date_Rule__c = newWG.Start_Date_Rule__c;
                                }
                                if(newWG.End_Date_Rule__c != null){
                                    wg.End_Date_Rule__c = newWG.End_Date_Rule__c;
                                }
                                if(newWG.Outside_Date_Rule__c != null){
                                    wg.Outside_Date_Rule__c = newWG.Outside_Date_Rule__c;
                                }
                                if(newWG.Tracking_Date_Rule__c != null){
                                    wg.Tracking_Date_Rule__c = newWG.Tracking_Date_Rule__c;
                                }
                                WGKeyMap.put(WGRule.Window_Guideline__c,wg);
                            } else{
                                newWG.Id = WGRule.Window_Guideline__c;
                                WGKeyMap.put(WGRule.Window_Guideline__c,newWG);
                            }
                        }
                    }
                }
            }
           
            if(RGsToUpdate != null && RGsToUpdate.size() > 0){
                update RGsToUpdate;
            }
            if(WGKeyMap.keySet().size() > 0){
                WGsToUpdate = WGKeyMap.values();
                system.debug('::WGsToUpdate:::::::'+WGsToUpdate); 
                update WGsToUpdate; 
            }
        } 
    }
}