@RestResource(urlMapping='/receiveGalileo/*')
global with sharing class EAW_GenericReceiverClass {
   
    //Added for LRCC-631 to receive the response to be handled*/
    // tempRightsEndDateArray = windowWrap.intersectingStartDate;
    @HttpPost
    global static  List<EAW_Window__c>  updateWindows() {   
    
        //EAW_Window__c testWindow = [select Id, Right_Status__c from EAW_Window__c where Id='aAI3D000000Cd1LWAS'];
        //testWindow.Right_Status__c = 'Clear';
        //update testWindow;
        EAW_Inbound_Notifications__c eawIN = new EAW_Inbound_Notifications__c(/*Name='EAW_GenericReceiverClass'+DateTime.now().getTime(),*/Source_System__c='Galileo Rights Check'/*,Created_Date__c=Datetime.now()*/);
        insert eawIN;
        //above method just for testing. will be removed later.
       
        Set<String> windowIds = new Set<String>();
        Set<String> conflictingRightTypeIdSet = new Set<String>();
        List<Rights_Status_And_Licensing_Code__mdt> rsals = new List<Rights_Status_And_Licensing_Code__mdt> ();
        Map<String,String> codeWithRigtsStatusMap = new Map<String,String>();
        Map<String,Rights_Status_And_Licensing_Code__mdt> codeWithLicCodeMap= new Map<String,Rights_Status_And_Licensing_Code__mdt>();
      
        
        Map <String,set<String>> windowStrandWithCodeMap = new  Map <String,set<String>>();
        Map <String,set<String>> windowStrandWithLICode = new  Map <String,set<String>>();
        Map <String,List<Date>> windowStrandWithRightEndDates = new  Map <String,List<Date>>();
        Map <String,List<String>> windowStrandWithFailedReason = new  Map <String,List<String>>();
        Map <String,set<String>> windowStrandWithconflictingRightTypeId = new  Map <String,set<String>>();
         
        RestResponse res = RestContext.response;
        String requestBody = RestContext.request.requestBody.toString(); 
        System.debug('responsebody ::after::'+requestBody);  
        
        String decoded = EncodingUtil.urlDecode(RestContext.request.requestBody.toString(), 'UTF-8');    
        List<windowWrapper> windowStrandwrapper = (List<windowWrapper>)JSON.deserialize(decoded , List<windowWrapper>.class);
 
        System.debug('windowStrandwrapper  ::after::'+windowStrandwrapper );  
        eawIN.conflict_Json__c=(decoded!=null && decoded.length()>32767)?decoded.substring(0,32767):decoded;
        for(windowWrapper windowWrap: windowStrandwrapper){
        	windowIds.add(windowWrap.windowId);
        }
        Map<String,Eaw_window__c> windowMap=new Map<String,Eaw_window__c>();
        system.debug('windowIds: '+windowIds);
        if(windowIds!=null && !windowIds.isempty()){
        	//windowMap = [SELECT Id,Start_Date__c FROM EAW_Window__c WHERE Id IN : windowIds];
		    for(EAW_Window__c ewindow : [SELECT Id,Start_Date__c FROM EAW_Window__c WHERE Id IN : windowIds]){		            
	            if(!windowMap.containsKey(ewindow.Id)){	                
	                windowMap.put(ewindow.Id,ewindow);
	            }
	        }
        }
        system.debug('windowMap: '+windowMap);        
        for(windowWrapper windowWrap: windowStrandwrapper){
            
            for(WindowStrands windowStr:windowWrap.WindowStrands ){
            
                set<String> descriptionSet = new Set<String>();
                set<String> licenseInfoCodesSet = new set<String>();
                List<String> failedReasonList = new List<String>();
                List<Date> intersectingStartDateList = new List<Date>();
                Boolean noRightsStatusflag = false;
                
                for(Conflicts conflict: windowStr.Conflicts){
                    
                    if(!conflictingRightTypeIdSet.contains(conflict.conflictingRightTypeId)){
                    
                        if(conflict.conflictingRightTypeId != NULL){
                            
                            conflictingRightTypeIdSet.add(conflict.conflictingRightTypeId);
                        }
                    }
                    if(!descriptionSet.contains(conflict.conflictingRightType)){
                    
                        if(conflict.conflictingRightType!= NULL){
                        
                            descriptionSet.add(conflict.conflictingRightType);
                        }
                    }
                    if(conflict.intersectingStartDate != NULL){
                    
                        List<Integer> tempRightsEndDateArray = conflict.intersectingStartDate;
                        if(tempRightsEndDateArray != NULL && tempRightsEndDateArray[0] != NULL && tempRightsEndDateArray[1] != NULL && tempRightsEndDateArray[2] != NULL){
                            
                            Date tempDate = Date.newInstance(tempRightsEndDateArray[0], tempRightsEndDateArray[1], tempRightsEndDateArray[2]);
                            
                            if(conflict.conflictType != 'No Rights' && noRightsStatusflag == false){
                            	//GJ 06282017 Don't consider intersecting start dates of info codes
                                /*if(!intersectingStartDateList.contains(tempDate)){
                                    
                                    intersectingStartDateList.add(tempDate);
                                }*/
                            //GJ 06282017: If conflict.conflictType == 'No Rights' and intersecting start date is equal to window start date, then consider that MLT has no rights   
                            }else if(conflict.conflictType == 'No Rights' && windowMap.get(windowWrap.windowId)!=null && tempDate==windowMap.get(windowWrap.windowId).Start_Date__c){
                                 if(conflict.mediaName != NULL && conflict.terrLangName != NULL && !failedReasonList.contains(conflict.mediaName+'/'+conflict.terrLangName+' :'+'No Rights')){  
                    
			                        failedReasonList.add(conflict.mediaName+'/'+conflict.terrLangName+' :'+'No Rights');
			                     }
			                     if(String.isblank(conflict.conflictingRightTypeId)){
			                     	conflictingRightTypeIdSet.add('10031');
			                     }
                                 
                            }else if(conflict.conflictType == 'No Rights'){
                                 
                                 noRightsStatusflag = true;   
                                 intersectingStartDateList = new List<Date>();
                                 intersectingStartDateList.add(tempDate);
                            }
                        }
                    }
                    if(conflict.conflictingRightType != NULL){  
                    
                        licenseInfoCodesSet.add(conflict.conflictingRightType);
                    }
                    
                    if('Rights Excluded'.equals(conflict.conflictType) && conflict.mediaName != NULL && conflict.terrLangName != NULL && !failedReasonList.contains(conflict.mediaName+'/'+conflict.terrLangName)){  
                    
                        failedReasonList.add(conflict.mediaName+'/'+conflict.terrLangName+' :'+'Rights Excluded');
                    }
                   
                }
                if(!windowStrandWithCodeMap.containsKey(windowStr.windowStrandID)){
                
                     windowStrandWithCodeMap.put(windowStr.windowStrandID,descriptionSet);
                }
                if(!windowStrandWithLICode.containsKey(windowStr.windowStrandID)){
                
                     windowStrandWithLICode.put(windowStr.windowStrandID,licenseInfoCodesSet);
                }
                if(!windowStrandWithRightEndDates.containsKey(windowStr.windowStrandID)){
                
                     windowStrandWithRightEndDates.put(windowStr.windowStrandID,intersectingStartDateList);
                }
                //failed reason comment generation
                if(!windowStrandWithFailedReason.containsKey(windowStr.windowStrandID)){
                
                     windowStrandWithFailedReason.put(windowStr.windowStrandID,failedReasonList);
                }
                if(!windowStrandWithconflictingRightTypeId.containsKey(windowStr.windowStrandID)){
                    
                     windowStrandWithconflictingRightTypeId.put(windowStr.windowStrandID,conflictingRightTypeIdSet);
                }
            }
            
            windowIds.add(windowWrap.windowId);
        }
        System.debug('conflictingRightTypeIdSet:::::'+conflictingRightTypeIdSet);    
        for(Rights_Status_And_Licensing_Code__mdt rsalc : [SELECT Code__c,Conflicting_Right_Type_Id__c,Description__c,Rights_Status__c,Is_Renderable__c FROM Rights_Status_And_Licensing_Code__mdt ])
        {                                                                     
            if(!codeWithRigtsStatusMap.containsKey(rsalc.Conflicting_Right_Type_Id__c))
            {
                
                codeWithRigtsStatusMap .put(rsalc.Conflicting_Right_Type_Id__c,rsalc.Rights_Status__c);
            }   
            if(!codeWithLicCodeMap.containsKey(rsalc.Conflicting_Right_Type_Id__c)){
                
                codeWithLicCodeMap.put(rsalc.Conflicting_Right_Type_Id__c,rsalc);
            }                                                           
            
        }  
         System.debug('codeWithLicCodeMap:::::'+codeWithLicCodeMap);  
         System.debug('codeWithRigtsStatusMap :::::'+codeWithRigtsStatusMap );    
         System.debug('windowStrandWithCodeMap:::::'+windowStrandWithCodeMap);   
         System.debug('windowStrandWithRightEndDates:::::'+windowStrandWithRightEndDates);      
         System.debug('windowStrandWithFailedReason:::::'+windowStrandWithFailedReason);  
         System.debug('windowStrandWithconflictingRightTypeId:::::'+windowStrandWithconflictingRightTypeId);     
      
        return generateRightsStatus(codeWithLicCodeMap,windowStrandwrapper,codeWithRigtsStatusMap ,
                                    windowStrandWithCodeMap,windowStrandWithLICode,windowStrandWithRightEndDates,
                                    windowStrandWithFailedReason,windowIds,windowStrandWithconflictingRightTypeId,eawIN,windowMap);
        //return null;
    } 
     
   global static  List<EAW_Window__c>  generateRightsStatus( Map<String,Rights_Status_And_Licensing_Code__mdt> codeWithLicCodeMap,List<windowWrapper> windowStrandwrapper, Map<String,String> codeWithRigtsStatusMap , 
                                                             Map <String,set<String>> windowStrandWithCodeMap, Map <String,set<String>> windowStrandWithLICode,
                                                             Map<String,List<Date>> windowStrandWithRightEndDates,Map<String,List<String>> windowStrandWithFailedReason,
                                                             Set<String>windowIds,Map <String,set<String>> windowStrandWithconflictingRightTypeId,EAW_Inbound_Notifications__c eawIN,Map<String,Eaw_window__c> windowMap) {
         
        
        Map <String,set<String>> windowStrandWithRightsStatus = new Map <String,set<String>>();
        Map <String,String> windowWithRightsStatus = new Map <String,String>();
        Map <String,String> windowWithFailedReason = new Map <String,String>();
        String windowRightsStatus;
        String licCode;
        string Failedreason ;
        Date rightEndDate;
        List<EAW_Window__c> windowList;
        /*Map <String,EAW_Window__c> windowMap = new  Map <String,EAW_Window__c>();*/
        
        Set<Id> endDateModifiedWindowId = new Set<Id>();
        /*for(EAW_Window__c ewindow : [SELECT Id,End_Date_Rule__c FROM EAW_Window__c WHERE Id IN : windowIds]){
            
            if(!windowMap.containsKey(ewindow.Id)){
                
                windowMap.put(ewindow.Id,ewindow);
            }
        }*/
        
        System.debug('windowStrandWithCodeMap::::::'+windowStrandWithCodeMap);
        for(String windowstrandId : windowStrandWithCodeMap.keySet()){
        
            Set<String> rightsStatusSet =  new Set<String>();
           /* for(String description: windowStrandWithCodeMap.get(windowstrandId)){
            
                 //if(!rightsStatusSet.contains(codeWithRigtsStatusMap .get(description))){
                    System.debug('description:::xx::'+description);
                    System.debug(':::xx::'+codeWithRigtsStatusMap.get(description));
                    rightsStatusSet.add(codeWithRigtsStatusMap.get(description));
                //}  
            }*/
            
            for(String conflictingRightTypeId : windowStrandWithconflictingRightTypeId.get(windowstrandId)){
                
                    System.debug('conflictingRightTypeId:::xx::'+conflictingRightTypeId);
                    System.debug(':::xx::'+codeWithRigtsStatusMap.get(conflictingRightTypeId));
                    rightsStatusSet.add(codeWithRigtsStatusMap.get(conflictingRightTypeId));
            }
            
            if(!windowStrandWithRightsStatus.containsKey(windowstrandId)){
                
                 windowStrandWithRightsStatus.put(windowStrandID,rightsStatusSet);
            }
        }
        System.debug('windowStrandWithRightsStatus:::xx::'+windowStrandWithRightsStatus);
        for(windowWrapper windowWrap: windowStrandwrapper){
        	if(windowMap!=null && !windowMap.isEmpty() && windowMap.containsKey(windowWrap.windowId)){
	           windowRightsStatus = '';
	           licCode = '';
	           Date windowRightsEndDate;
	            
	           for(WindowStrands windowStr:windowWrap.WindowStrands ){
	                
	                set<String> tempWindowStrandStatusSet = windowStrandWithRightsStatus.get(windowStr.windowStrandID);
	                List<String> licCodeList = new List<String>(windowStrandWithconflictingRightTypeId.get(windowStr.windowStrandID));
	                
	                if(licCodeList.size() > 0){
	                    
	                    System.debug('licCodeList::::'+licCodeList);
	                    System.debug('codeWithLicCodeMap::::'+codeWithLicCodeMap);
	                
	                    licCode = format(licCodeList,codeWithLicCodeMap);
	                    System.debug('codeWithLicCodeMap:::licCode:'+licCode);
	                }
	                if(tempWindowStrandStatusSet.isempty()){
	                	windowRightsStatus = 'Clear';
	                }else{ 
		                if(tempWindowStrandStatusSet.contains('No Rights Entered in ERM')){
		                    
		                        windowRightsStatus = 'No Rights Entered in ERM';
		                        if(windowStrandWithRightEndDates.get(windowStr.windowStrandID).size() > 0){
		                        
		                            windowRightsEndDate =  findLeastEndDate(windowStrandWithRightEndDates.get(windowStr.windowStrandID)) - 1;
		                        }
		                        
		                }else if(tempWindowStrandStatusSet.contains('Rights Excluded')){
		                        
		                        windowRightsStatus = 'Rights Excluded';
		                            
		                        if(tempWindowStrandStatusSet.contains('Clear with TBA Start Date')){
		                            
		                            windowRightsStatus = 'Partial Rights with TBA Start Date';
		                        }
		                        if (tempWindowStrandStatusSet.contains('Clear with TBA End Date')){
		                            
		                            windowRightsStatus = 'Partial Rights with TBA End Date';
		                            
		                        }
		                        if(tempWindowStrandStatusSet.contains('Clear with TBA Start Date') && tempWindowStrandStatusSet.contains('Clear with TBA End Date')){
		                            
		                            windowRightsStatus = 'Partial Rights with TBA Start and TBA End';
		                        }
		                        if (tempWindowStrandStatusSet.contains('Clear')){
		                            
		                            windowRightsStatus = 'Partial Rights';
		                        }
		                       if(windowStrandWithFailedReason.get(windowStr.windowStrandID).size() > 0){ 
		                       
		                           Failedreason = formatComments( new List<String>(windowStrandWithFailedReason.get(windowStr.windowStrandID)),Failedreason);
		                           windowWithFailedReason.put(windowWrap.windowId,Failedreason);
		                       }
		                                      
		                       if(windowStrandWithRightEndDates.get(windowStr.windowStrandID).size() > 0){ 
		                                      
		                           windowRightsEndDate =  findLeastEndDate(windowStrandWithRightEndDates.get(windowStr.windowStrandID))-1;
		                       }
		                        
		                }else{
		                    
		                    if(tempWindowStrandStatusSet.contains('Clear')){
		                        
		                        windowRightsStatus = 'Clear';
		                    }
		                    if(tempWindowStrandStatusSet.contains('Clear with TBA Start Date')){
		                        
		                        windowRightsStatus = 'Clear with TBA Start Date';
		                    }
		                    if(tempWindowStrandStatusSet.contains('Clear with TBA End Date')){
		                        
		                        windowRightsStatus = 'Clear with TBA End Date';
		                    }
		                    if(tempWindowStrandStatusSet.contains('Clear with TBA Start Date') && tempWindowStrandStatusSet.contains('Clear with TBA End Date')){
		                        
		                        windowRightsStatus = 'Clear with TBA Start and End Date';
		                        
		                    }
		                    if(windowStrandWithRightEndDates.get(windowStr.windowStrandID).size() > 0){ 
		                                      
		                       windowRightsEndDate =  findLeastEndDate(windowStrandWithRightEndDates.get(windowStr.windowStrandID))-1;
		                   }
		                   if(windowStrandWithFailedReason.get(windowStr.windowStrandID).size() > 0){ 
		                       
	                           Failedreason = formatComments( new List<String>(windowStrandWithFailedReason.get(windowStr.windowStrandID)),Failedreason);
	                           
	                       }
		                }
	                }
	            }
	            
	              System.debug('windowRightsStatus:::'+windowRightsStatus);
	              System.debug('licCode:::'+licCode);
	            if(!String.isBlank(windowRightsStatus) /* && !String.isBlank(licCode)*/){
	            
	               
	                windowList = new List<EAW_Window__c>();
	                EAW_Window__c ew = new EAW_Window__c();
	                ew.Id = windowWrap.windowId;
	                ew.Right_Status__c = windowRightsStatus;
                    
                    if('Rights Excluded'.equals(windowRightsStatus)){
                        windowRightsEndDate=null;
                    }
                    
	                ew.License_Info_Codes__c = licCode;
	                if(ew.Sent_To_Galileo_RC__c){
			        	ew.Galileo_RC_DML_Type__c = 'Update';
			        }else{
			        	ew.Galileo_RC_DML_Type__c = 'Insert';
			        }
			        if(ew.Sent_To_Galileo__c){
			        	ew.Galileo_DML_Type__c = 'Update';
			        }else{
			        	ew.Galileo_DML_Type__c = 'Insert';
			        }
			        if(ew.Sent_To_Repo__c){
			        	ew.Repo_DML_Type__c = 'Update';
			        }else{
			        	ew.Repo_DML_Type__c = 'Insert';
			        }
	                ew.Send_To_Third_Party__c=true;
	                ew.performRightsCheck__c=false;
	                /*if(windowMap.get(windowWrap.windowId) != NULL && windowMap.get(windowWrap.windowId).End_Date_Rule__c == 'Rights End Date' && windowMap.get(windowWrap.windowId).Status__c!='Firm'){*/
	                    
	                    ew.Rights_End_Date__c = windowRightsEndDate;
	                    endDateModifiedWindowId.add(windowWrap.windowId);
	                /*}*/
	                System.debug('ew:::'+ew);
	                windowList.add(ew);
	            } 
        		if(String.isnotblank(Failedreason))windowWithFailedReason.put(windowWrap.windowId,Failedreason);
        	}
       	}
       System.debug('test::::ss::'+windowList);
       if(windowList != NULL && windowList.size()>0){
       		checkRecursiveData.bypassDateCalculation=true;//To bypass recursive calculation of downstream window dates
            update windowList;
             System.debug('updated window:::::'+windowList);
            
             List<Note> existingNotes = [select Id, parentId from Note where parentId in :windowIds];
             if(existingNotes!=null && !existingNotes.isempty()){
             	delete existingNotes;
             }
            List <Note> notelist= new  List <Note>();
            
            for(EAW_Window__c ewIns : windowList){
            
                if(windowWithFailedReason!=null && windowWithFailedReason.get(ewIns.Id)!=null){
	                Note n = new Note();
	                n.Body = windowWithFailedReason.get(ewIns.Id);
	                n.Title = ewIns.Right_Status__c;
	                n.ParentId = ewIns.Id;
	               
	                notelist.add(n);
            	}
            }
            
            if(notelist.size() > 0){
            
                insert notelist;
            }
       }
       if(windowIds!=null && !windowIds.isempty()){
	   		String temp = '';
	   		for(Id i:windowIds)temp += i+';';
	   		eawIN.message__c = temp;
	   		eawIN.status__c='Need Downstream Calc';
	   		update eawIN;
       }
       /*if(endDateModifiedWindowId!=null && !endDateModifiedWindowId.isempty()){
       		Set<Id> windowGuidelineIdSet = new Set<Id>();
        	Set<Id> TitleIdSet = new Set<Id>();
        	List<EAW_window__c> endDateModifiedWindows = [select EAW_Window_Guideline__c,EAW_Title_Attribute__c from EAW_window__c where Id in : endDateModifiedWindowId];
        	if(endDateModifiedWindows!=null && !endDateModifiedWindows.isempty()){
        		for(EAW_window__c endDateModifiedWindow:endDateModifiedWindows){
        			windowGuidelineIdSet.add(endDateModifiedWindow.EAW_Window_Guideline__c);
        			TitleIdSet.add(endDateModifiedWindow.EAW_Title_Attribute__c);
        		}
        		if(windowGuidelineIdSet!=null && !windowGuidelineIdSet.isempty() && TitleIdSet!=null && !TitleIdSet.isempty()){
	                EAW_WGDateCalculationBatchClass windowDateBatch = new EAW_WGDateCalculationBatchClass(windowGuidelineIdSet);
	                windowDateBatch.releaseDateGuidelineIdSet = NULL;
	                windowDateBatch.titleIdSet = TitleIdSet;
	                //windowDateBatch.windowGuidelineIdSet = windowGuidelinesIdToProcess;
	                ID batchprocessid = Database.executeBatch(windowDateBatch, 200);
        		}
    		}
       }else{
       		Id batchJobId = Database.executeBatch(new EAW_Window_API_BatchProcess(), 100);
			system.debug('EAW_GenericReceiverClass batchJobId: '+batchJobId);
       }*/
       return windowList;
    }
    
    public static String formatComments(List<String> values,String failedReason) {
     
        if (values == null) return failedReason;
        if(String.isnotblank(failedReason)) return failedReason+'\n'+String.join(values, '\n');
        return String.join(values, '\n');
    }
    public static String format(List<String> values,Map<String, Rights_Status_And_Licensing_Code__mdt> codeWithLicCodeMap) {
         System.debug('values:::::'+values);   
        System.debug('codeWithLicCodeMap:::::'+codeWithLicCodeMap);   
        
        List<String> licCodeDescription = new list<String>();
        if (values == null) return null;
        
        for(String des : values){
        
            if(codeWithLicCodeMap.get(des) != NULL && codeWithLicCodeMap.get(des).Is_Renderable__c){
                //licCodeDescription.add( codeWithLicCodeMap.get(des).Code__c+' : '+ codeWithLicCodeMap.get(des).Description__c);
                licCodeDescription.add( codeWithLicCodeMap.get(des).Code__c);
            }
        }
        return String.join(licCodeDescription, ';');
    }
    public static Date findLeastEndDate(List<Date> values) {
    
         System.debug('findLeastEndDate:::::'+values);   
         Date minvalue = values[0] ; 
         For (integer i =0;i < values.size();i++){
            
            if( values[i] < minvalue)
                minvalue = values[i];             
        }
        System.debug('minvalue:::::'+minvalue);     
        return  minvalue;
    }
    
    global class Conflicts {
    
        global String id;
        global String customerId;
        global String customer;
        global String rightTypeId;
        global String rightType;
        global String displaySource;
        global String displaySourceType;
        global String conflictingBusinessUnitId;
        global String conflictingBusinessUnit;
        global String conflictingRightTypeId;
        global String conflictingRightType;
        global String conflictingComments;
        global String conflictingEpisodeLimits; //obj
        global String conflictingDistributionRightsOwner;
        global String conflictTypeId;
        global String conflictType;
        global String conflictSeverityId;
        global String conflictSeverity;
        global String conflictStatusId;
        global String productName;
        global String mediaName;
        global String terrLangName;
        global List<Integer> intersectingStartDate;
        global List<Integer> intersectingEndDate;
        global List<Integer> greatestPrimaryStartDate;
        global List<Integer> greatestPrimaryEndDate;
        global List<Integer> greatestConflictingStartDate;
        global List<Integer> greatestConflictingEndDate;
        global String releaseYear; //obj
    }   
    
     global class WindowStrands {
     
        global String windowStrandID;
        global String conflictCount;
        global String error;
        global List<Conflicts> conflicts;
    }
    
    global class windowWrapper {
    
      public String windowId;
      public List<WindowStrands> WindowStrands;
    }

}