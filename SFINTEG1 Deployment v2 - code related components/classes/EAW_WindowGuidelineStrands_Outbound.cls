public with sharing class EAW_WindowGuidelineStrands_Outbound {
	List<EAW_Error_Log__c> repoErrorLogs  = new List<EAW_Error_Log__c>();
	set<id> repoSuccessIds= new set<id>();
	Set<id> repoIgnoredIds = new Set<id>();
	
	String repoUrl = '';
	
	public EAW_WindowGuidelineStrands_Outbound(){
		checkOuboundRecursiveData.bypassTriggerFire=true;
		Rest_Endpoint_Settings__c repoInstance = Rest_Endpoint_Settings__c.getInstance('Outbound-WGS-to-Repo');
		if(repoInstance!=null){
			repoUrl=repoInstance.End_Point__c;
		}
	}
    
    public class WindowGuidelineStrandJson{
	    	public String windowGuidelineStrandId;
			public String windowGuidelineStrandName;
			public String windowGuidelineId;
			public String windowGuidelineName;
			public String licenseType;
			public String licenseTypeId;
			public String territory;
			public String territoryId;
			public String media;
			public String mediaId;
			public String language;
			public String languageId;
			public String startDateDaysAfter;
			public String startDateDaysOffset;
			public String startDateFrom;
			public String startDateMonthsAfter;
			public String startDateMonthsOffset;
			public String startDateSource;
			public String endDateDaysAfter;
			public String endDateDaysOffset;
			public String endDateFrom	;
			public String endDateMonthsAfter;
			public String endDateMonthsOffset;
			public String endDateSource;
			public String deleted;
			public String lastModifiedDate;
			public String lastModifiedBy;
			public String operation;
	    	
	    	public WindowGuidelineStrandJson(String windowGuidelineStrandId,String windowGuidelineStrandName,String windowGuidelineId,String windowGuidelineName,String licenseType,String licenseTypeId,String territory,
	    									String territoryId,String media,String mediaId,String language,String languageId,String startDateDaysAfter,String startDateDaysOffset,String startDateFrom, String startDateMonthsAfter, 
	    									String startDateMonthsOffset,String startDateSource,String endDateDaysAfter,String endDateDaysOffset,String endDateFrom,String endDateMonthsAfter,String endDateMonthsOffset,
	    									String endDateSource,String deleted,String lastModifiedDate,String lastModifiedBy,String operation){
		    	this.windowGuidelineStrandId = windowGuidelineStrandId;
				this.windowGuidelineStrandName = windowGuidelineStrandName;
				this.windowGuidelineId = windowGuidelineId;
				this.windowGuidelineName = windowGuidelineName;
				this.licenseType = licenseType;
				this.licenseTypeId = licenseTypeId;
				this.territory = territory;
				this.territoryId = territoryId;
				this.media = media;
				this.mediaId = mediaId;
				this.language = language;
				this.languageId = languageId;
				this.startDateDaysAfter = startDateDaysAfter;
				this.startDateDaysOffset = startDateDaysOffset;
				this.startDateFrom = startDateFrom;
				this.startDateMonthsAfter = startDateMonthsAfter;
				this.startDateMonthsOffset = startDateMonthsOffset;
				this.startDateSource = startDateSource;
				this.endDateDaysAfter = endDateDaysAfter;
				this.endDateDaysOffset = endDateDaysOffset;
				this.endDateFrom = endDateFrom;
				this.endDateMonthsAfter = endDateMonthsAfter;
				this.endDateMonthsOffset = endDateMonthsOffset;
				this.endDateSource = endDateSource;
				this.deleted = deleted;
				this.lastModifiedDate = lastModifiedDate;
				this.lastModifiedBy = lastModifiedBy;
				this.operation = operation;
	    	}
    }
    
    private void getJson(EAW_Outbound_Utility.OutboundSystem os, List<EAW_Window_Guideline_Strand__c> WGSlist){
    	List<WindowGuidelineStrandJson> wgsJsonlist = new List<WindowGuidelineStrandJson>();
    	List<WindowGuidelineStrandJson> wgsJsonDeleteList = new List<WindowGuidelineStrandJson>();
        List<id> wgsIdList = new List<id>();
        List<id> wgsDeleteIdList = new List<id>();
		Map<String,EAW_Territories__c> territoryMap = EAW_Territories__c.getAll();
    	Map<String,EAW_Languages__c> languageMap = EAW_Languages__c.getAll();
    	Map<String,EAW_Media__c> mediaMap = EAW_Media__c.getAll();
 
        List<List<WindowGuidelineStrandJson>> wgsJsonListOfList = new List<List<WindowGuidelineStrandJson>>();
        List<List<WindowGuidelineStrandJson>> wgsJsonDeleteListOfList = new List<List<WindowGuidelineStrandJson>>();
	    List<List<id>> wgsIdListOfList = new List<List<id>>();
		List<List<id>> wgsDeleteIdListOfList = new List<List<id>>();
        
        for(EAW_Window_Guideline_Strand__c wgs:WGSlist){
			String operation = '';
            if(os==EAW_Outbound_Utility.OutboundSystem.Repo){
                operation = wgs.Repo_DML_Type__c;
            }
            for(String media: wgs.Media__c.split(';')){
                for(String territory: wgs.Territory__c.split(';')){
                	for(String language: wgs.Language__c.split(';')){
						if(operation == 'Insert' || operation == 'Delete'){
							WindowGuidelineStrandJson wgsJson = new WindowGuidelineStrandJson(wgs.Id,wgs.Name,''+wgs.EAW_Window_Guideline__c,wgs.EAW_Window_Guideline__r.name,wgs.License_Type__c,null,territory,
							EAW_Outbound_Utility.getTerritoryIds(territoryMap,territory),media,EAW_Outbound_Utility.getMediaIds(mediaMap,media),language,
							EAW_Outbound_Utility.getLanguageIds(languageMap,language),''+wgs.Start_Date_Days_After__c,''+wgs.Start_Date_Days_Offset__c,wgs.Start_Date_From__c,
							''+wgs.Start_Date_Months_After__c,''+wgs.Start_Date_Months_Offset__c,wgs.Start_Date_Source__c,''+wgs.End_Date_Days_After__c,''+wgs.End_Date_Days_Offset__c,
							wgs.End_Date_From__c,''+wgs.End_Date_Months_After__c,''+wgs.End_Date_Months_Offset__c,wgs.End_Date_Source__c,wgs.Soft_Deleted__c,EAW_Outbound_Utility.getFormattedDateTime(wgs.LastModifiedDate),
							wgs.LastModifiedBy.name,operation);
							
							wgsJsonlist.add(wgsJson);
							wgsIdList.add(wgs.id);
						} else if(operation == 'Update'){
							WindowGuidelineStrandJson wgsDeleteJson = new WindowGuidelineStrandJson(wgs.Id,wgs.Name,''+wgs.EAW_Window_Guideline__c,wgs.EAW_Window_Guideline__r.name,wgs.License_Type__c,null,territory,
							EAW_Outbound_Utility.getTerritoryIds(territoryMap,territory),media,EAW_Outbound_Utility.getMediaIds(mediaMap,media),language,
							EAW_Outbound_Utility.getLanguageIds(languageMap,language),''+wgs.Start_Date_Days_After__c,''+wgs.Start_Date_Days_Offset__c,wgs.Start_Date_From__c,
							''+wgs.Start_Date_Months_After__c,''+wgs.Start_Date_Months_Offset__c,wgs.Start_Date_Source__c,''+wgs.End_Date_Days_After__c,''+wgs.End_Date_Days_Offset__c,
							wgs.End_Date_From__c,''+wgs.End_Date_Months_After__c,''+wgs.End_Date_Months_Offset__c,wgs.End_Date_Source__c,wgs.Soft_Deleted__c,EAW_Outbound_Utility.getFormattedDateTime(wgs.LastModifiedDate),
							wgs.LastModifiedBy.name,'Delete');
							wgsJsonDeleteList.add(wgsDeleteJson);
							wgsDeleteIdList.add(wgs.id);
							
							WindowGuidelineStrandJson wgsInsertJson = new WindowGuidelineStrandJson(wgs.Id,wgs.Name,''+wgs.EAW_Window_Guideline__c,wgs.EAW_Window_Guideline__r.name,wgs.License_Type__c,null,territory,
							EAW_Outbound_Utility.getTerritoryIds(territoryMap,territory),media,EAW_Outbound_Utility.getMediaIds(mediaMap,media),language,
							EAW_Outbound_Utility.getLanguageIds(languageMap,language),''+wgs.Start_Date_Days_After__c,''+wgs.Start_Date_Days_Offset__c,wgs.Start_Date_From__c,
							''+wgs.Start_Date_Months_After__c,''+wgs.Start_Date_Months_Offset__c,wgs.Start_Date_Source__c,''+wgs.End_Date_Days_After__c,''+wgs.End_Date_Days_Offset__c,
							wgs.End_Date_From__c,''+wgs.End_Date_Months_After__c,''+wgs.End_Date_Months_Offset__c,wgs.End_Date_Source__c,wgs.Soft_Deleted__c,EAW_Outbound_Utility.getFormattedDateTime(wgs.LastModifiedDate),
							wgs.LastModifiedBy.name,'Insert');
							wgsJsonlist.add(wgsInsertJson);
							wgsIdList.add(wgs.id);
						}

						if(wgsJsonlist.size()==EAW_Outbound_Utility.MAX_API_RECORDS_LIMIT){
			                wgsJsonListOfList.add(wgsJsonList);
			                wgsIdListOfList.add(wgsIdList);
			                wgsJsonList = new List<WindowGuidelineStrandJson>();
			                wgsIdList = new List<id>();
						}
						
						if(wgsJsonDeleteList.size()==EAW_Outbound_Utility.MAX_API_RECORDS_LIMIT){
			                wgsJsonDeleteListOfList.add(wgsJsonDeleteList);
			                wgsDeleteIdListOfList.add(wgsDeleteIdList);
			                wgsJsonDeleteList = new List<WindowGuidelineStrandJson>();
			                wgsDeleteIdList = new List<id>();
						}
                	}
                }
            }
		}
        if(wgsJsonlist!=null && !wgsJsonlist.isEmpty()){
    			wgsJsonListOfList.add(wgsJsonlist);
				wgsIdListOfList.add(wgsIdList);
    	}
    	if(wgsJsonDeleteList!=null && !wgsJsonDeleteList.isEmpty()){
    		wgsJsonDeleteListOfList.add(wgsJsonDeleteList);
			wgsDeleteIdListOfList.add(wgsDeleteIdList);
    	}
    	
    	if(wgsJsonDeleteListOfList!=null && !wgsJsonDeleteListOfList.isempty()){
        	for(integer i=0;i<wgsJsonDeleteListOfList.size();i++){
    			List<WindowGuidelineStrandJson> tempJsonList = wgsJsonDeleteListOfList[i];
				List<id> tempIdList = wgsDeleteIdListOfList[i];
    			if(os==EAW_Outbound_Utility.OutboundSystem.Repo){
					EAW_Outbound_Utility eouRepo = new EAW_Outbound_Utility(EAW_Outbound_Utility.OutboundDataType.WindowGuidelineStrands,os,tempIdList);
			    	eouRepo.sendDataThroughWebAPI( system.json.serialize(tempJsonList),repoUrl,null,repoSuccessIds);
		    	}
    		}
    	} 
    	
        if(wgsJsonListOfList!=null && !wgsJsonListOfList.isempty()){
    			for(integer i=0;i<wgsJsonListOfList.size();i++){
    				List<WindowGuidelineStrandJson> tempJsonList = wgsJsonListOfList[i];
					List<id> tempIdList = wgsIdListOfList[i];
    				if(os==EAW_Outbound_Utility.OutboundSystem.Repo){
						EAW_Outbound_Utility eouRepo = new EAW_Outbound_Utility(EAW_Outbound_Utility.OutboundDataType.WindowGuidelineStrands,os,tempIdList);
				    	eouRepo.sendDataThroughWebAPI( system.json.serialize(tempJsonList),repoUrl,repoErrorLogs,repoSuccessIds);
		    		}
    			}
    		}
    
    }
    
    public void aggregateAllWGS(List<EAW_Window_Guideline_Strand__c> records,String operation){
    	List<EAW_Window_Guideline_Strand__c> repoRecords = new List<EAW_Window_Guideline_Strand__c>();
    	    	
    	List<EAW_Window_Guideline_Strand__c> updateList = new List<EAW_Window_Guideline_Strand__c>();
    	List<EAW_Window_Guideline_Strand__c> deleteList = new List<EAW_Window_Guideline_Strand__c>();
    	List<EAW_Error_Log__c> insertErrorList = new List<EAW_Error_Log__c>();
    	
    	if(records!=null && !records.isempty()){
    		for(EAW_Window_Guideline_Strand__c record:records){
    			if('update'.equalsignorecase(operation) || 'Insert'.equalsignorecase(operation)){
    				if(record.Sent_To_Repo__c){
    					record.Repo_DML_Type__c='Update';
    				}else{
    					record.Repo_DML_Type__c='Insert';
    				}
    			}else if('delete'.equalsignorecase(operation)){
    				record.Repo_DML_Type__c='Delete';
    			}

                boolean isSoftDeleted = record.Soft_Deleted__c == 'TRUE';//To do after handling soft delete please use the active flag
				if(record.Sent_To_Repo__c || !isSoftDeleted){
    				repoRecords.add(record);
				}else if(!record.Sent_To_Repo__c && isSoftDeleted){
					repoIgnoredIds.add(record.id);
				}				
			}
			if(!repoRecords.isEmpty()){
				getJson(EAW_Outbound_Utility.OutboundSystem.Repo,repoRecords);
			}
			
			for(EAW_Window_Guideline_Strand__c record:records){
					boolean canBeDeleted = false;
					boolean isSoftDeleted = record.Soft_Deleted__c == 'TRUE';//To do after handling soft delete please use the active flag	
					if((repoSuccessIds.contains(record.id)) //Both are success
						|| (repoIgnoredIds.contains(record.id))//If record is inactive and didn't send to both api then we can delete without sending them
						){
						record.Send_To_Third_Party__c=false;
						canBeDeleted = true; // Can be deleted
					}
					if(repoSuccessIds.contains(record.id)){//Repo call is success then repo DML type is blank and sent to repo should be true
						record.Sent_To_Repo__c=true;
						record.Repo_DML_Type__c='';
					}
					if(isSoftDeleted && canBeDeleted){
						deleteList.add(record);
					}else{
						updateList.add(record);
					}
			}
			insertErrorList.addAll(repoErrorLogs);
    	}
    	if(updateList!=null && !updateList.isEmpty()){
    		update updateList;
    	}
    	if(deleteList!=null && !deleteList.isEmpty()){
    		delete deleteList;
    	}
    	if(insertErrorList!=null && !insertErrorList.isEmpty()){
    		upsert insertErrorList;
    	}
    	//To do Skip downstream calculation of Release dates and Windows
    	//if response 200 then Send_To_Third_Party - False, Dml_Type as blank
		//Sent_To_Repo-true
		//Sent_To_Galileo-true
    }
    
    private static List<EAW_Window_Guideline_Strand__c> getWGStrands(Set<id> WGStrandIds){
		List<EAW_Window_Guideline_Strand__c> WGSlist = [select Id,Name,EAW_Window_Guideline__c,EAW_Window_Guideline__r.name,License_Type__c,Territory__c,Media__c,Language__c,Start_Date_Days_After__c,
														Start_Date_Days_Offset__c,Start_Date_From__c,Start_Date_Months_After__c,Start_Date_Months_Offset__c,Start_Date_Source__c,End_Date_Days_After__c,
														End_Date_Days_Offset__c,End_Date_From__c,End_Date_Months_After__c,End_Date_Months_Offset__c,End_Date_Source__c,Soft_Deleted__c,LastModifiedDate,
														LastModifiedBy.name,Repo_DML_Type__c, Sent_To_Repo__c,Send_To_Third_Party__c 
														from EAW_Window_Guideline_Strand__c where id in :WGStrandIds];
		return WGSlist;
	}
	
	 private static void sendData(Set<id> WGStrandIds,String operation){
    	EAW_WindowGuidelineStrands_Outbound ewgso = new EAW_WindowGuidelineStrands_Outbound();
    	ewgso.aggregateAllWGS(getWGStrands(WGStrandIds),operation);
    }
    
    @future(callout=true)
    public static void sendDataAsync(Set<id> WGStrandIds,String operation){
    	sendData(WGStrandIds,operation);
    }
    
    public static void sendDataSync(Set<id> WGStrandIds,String operation){
    	sendData(WGStrandIds,operation);
    }
    
}