@isTest
public class EAW_TagMaintenanceController_Test {
    
    @isTest static void search() {
        
        EAW_TestDataFactory.createTag(1, true, 'Release Date Tag');
        List<EAW_Tag__c> ObtainedList = EAW_TagMaintenanceController.search();
        System.assertEquals(True, ObtainedList != null);
    }
}