public class EAW_WindowTagJunctionTriggerHandler {
    
    Set<Id> tagIdSet = new Set<Id>();
    //Set<String> tagNameSet = new Set<String>();
    Set<Id> windowIdSet = new Set<Id>();
    Set<Id> windowGuidelineRuleIdSet = new Set<Id>();
    
    public void reQualificationOfRules( List<EAW_Window_Tag_Junction__c> newWindowTags ){
         
        try{
            
            if( ! checkRecursiveData.bypassDateCalculation && newWindowTags.size() == 1 ) {
                
                for( Integer i=0; i < newWindowTags.size(); i++){
                    if( newWindowTags[i].Tag__c != NULL && newWindowTags[i].Window__c != NULL ){
                        if(checkRecursiveData.isRecursiveData(newWindowTags[i].Id)) {
                            if( newWindowTags[i].Tag__c != NULL ) tagIdSet.add(newWindowTags[i].Tag__c);
                            if( newWindowTags[i].Window__c != NULL ) windowIdSet.add(newWindowTags[i].Window__c);
                        }
                    }
                }
                System.debug(':::::: windowIdSet :::::::'+windowIdSet);
                if( windowIdSet.size() > 0 ) filterAndProcessRulesForReQualification();
            }
            
        } catch (Exception e){
            System.debug('<<--Exception-->>'+e.getMessage());
        }
    }
    
     //LRCC-1787
    public void updateOldTags( List<EAW_Window_Tag_Junction__c> WindowTags ){
    
        Set<Id> windowIds = new Set<Id>();
        Map<Id, List<String>> windowIdTagsMap = new Map<Id, List<String>>();
        
        for(EAW_Window_Tag_Junction__c windowTag : WindowTags) {
            windowIds.add(windowTag.Window__c);
        }
        system.debug('windowIds:::'+windowIds);
        
        for(EAW_Window_Tag_Junction__c junctionRec : [SELECT Window__c, Tag__r.Name FROM EAW_Window_Tag_Junction__c WHERE Window__c IN : windowIds]) {
            
            if(! windowIdTagsMap.containsKey(junctionRec.Window__c)) {
                windowIdTagsMap.put(junctionRec.Window__c, new List<String>());
            }
            windowIdTagsMap.get(junctionRec.Window__c).add(junctionRec.Tag__r.Name);
        }
        system.debug('windowIdTagsMap:::'+windowIdTagsMap);
        
        List<EAW_Window__c> updateWindows = [SELECT OldTags__c, Tags_Changed_Dates__c, Tags_Changed_Users__c FROM EAW_Window__c WHERE Id IN : windowIds];
        
        for(EAW_Window__c windowRec : updateWindows) {
            
            if(windowRec.OldTags__c != null) {
                
                List<String> oldTagsValues = windowRec.OldTags__c.split('&&&');
                
                if(oldTagsValues.size() == 5) {
                    oldTagsValues.remove(0);
                }
                
                if(windowIdTagsMap.containsKey(windowRec.Id)) {
                    oldTagsValues.add(String.join(windowIdTagsMap.get(windowRec.Id), ';'));
                } else {
                    oldTagsValues.add('null');
                }
                
                windowRec.OldTags__c = String.join(oldTagsValues, '&&&');
            } else {
                
                if(windowIdTagsMap.containsKey(windowRec.Id)) {
                    windowRec.OldTags__c = String.join(windowIdTagsMap.get(windowRec.Id), ';');
                } else {
                    windowRec.OldTags__c = 'null';
                }
            }
            
            if(windowRec.Tags_Changed_Dates__c != null) {
            
                List<String> oldValues = windowRec.Tags_Changed_Dates__c.split('&&&');
                if(oldValues.size() == 5) {
                    oldValues.remove(0);
                }
                oldValues.add(String.valueOfGmt(system.now()));
                windowRec.Tags_Changed_Dates__c = String.join(oldValues,'&&&');
            } else {
                windowRec.Tags_Changed_Dates__c = String.valueOfGmt(system.now());
            }
            
            if(windowRec.Tags_Changed_Users__c != null) {
            
                List<String> oldValues = windowRec.Tags_Changed_Users__c.split('&&&');
                if(oldValues.size() == 5) {
                    oldValues.remove(0);
                }
                oldValues.add(UserInfo.getName());
                windowRec.Tags_Changed_Users__c = String.join(oldValues,'&&&');
            } else {
                windowRec.Tags_Changed_Users__c = UserInfo.getName();
            }
            
        }
        system.debug('updateWindows:::'+updateWindows);
        if(updateWindows.isEmpty() == FALSE) {
            update updateWindows;
        }
    }
    
    public void reQualificationOfRules(List<EAW_Window_Tag_Junction__c> oldWindowTags,List<EAW_Window_Tag_Junction__c> newWindowTags){
         
         try{
         
            if( ! checkRecursiveData.bypassDateCalculation && newWindowTags.size() == 1 ) {
                for(Integer i=0;i<oldWindowTags.size();i++){
                    if( newWindowTags[i].Tag__c != oldWindowTags[i].Tag__c || newWindowTags[i].Window__c != oldWindowTags[i].Window__c ){
                        if(checkRecursiveData.isRecursiveData(newWindowTags[i].Id)){
                            if( oldWindowTags[i].Tag__c != NULL ) tagIdSet.add(oldWindowTags[i].Tag__c);
                            if( newWindowTags[i].Tag__c != NULL ) tagIdSet.add(newWindowTags[i].Tag__c);
                            if( oldWindowTags[i].Window__c != NULL ) windowIdSet.add(oldWindowTags[i].Window__c);
                            if( newWindowTags[i].Window__c != NULL ) windowIdSet.add(newWindowTags[i].Window__c);
                        }
                    }
                }
                System.debug(':::::: windowIdSet :::::::'+windowIdSet);
                if( windowIdSet.size() > 0 ) filterAndProcessRulesForReQualification();
            }
        } catch (Exception e){
            System.debug('<<--Exception-->>'+e.getMessage());
        }
    }
    
    public void filterAndProcessRulesForReQualification() {
        
        if( windowIdSet != NULL && windowIdSet.size() > 0 ){
            
            Set<Id> windowGuidelineIdSet = new Set<Id>();
            Set<Id> titleIdSet = new Set<Id>();
            for( EAW_Window__c window : [ Select Id, Name, EAW_Title_Attribute__c, EAW_Plan__r.EAW_Title__c, EAW_Window_Guideline__c From EAW_Window__c Where Id IN : windowIdSet AND EAW_Window_Guideline__c != NULL ]){
                windowGuidelineIdSet.add(window.EAW_Window_Guideline__c);
                if( window.EAW_Title_Attribute__c != NULL ) titleIdSet.add(window.EAW_Title_Attribute__c);
            }
            
            if( windowGuidelineIdSet.size() > 0 ){
                
                //Multi-Operand selection related changes
                EAW_DownstreamRuleGuidelinesFindUtil dependentGuidelineFindUtil = new EAW_DownstreamRuleGuidelinesFindUtil();
                
                List<String> tagNameList = new List<String>();
                
                if( tagIdSet != NULL && tagIdSet.size() > 0 ) {
                    for( EAW_Tag__c winTag : [ Select Id, Name From EAW_Tag__c Where Id IN : tagIdSet ]){
                        String tagNameString = '\''+winTag.Name+'\'';
                        tagNameList.add(tagNameString);
                    }
                }
                String tagType = 'Window Tag'; // It was used in dynamic soql query
                
                Set<Id> windowGLIdSet = new Set<Id>();
                
                windowGLIdSet = dependentGuidelineFindUtil.findDependentWindowGuidelineToProcessUsingTags(windowGuidelineIdSet, tagNameList, tagType);
                
                System.debug('::::::::: windowGLIdSet ::::::::::'+windowGLIdSet);
                
                if( windowGLIdSet != NULL && windowGLIdSet.size() > 0 ) {
                    //LRCC-1478
                    EAW_WGDateCalculationBatchClass windowDateBatch = new EAW_WGDateCalculationBatchClass(windowGLIdSet);
                    windowDateBatch.releaseDateGuidelineIdSet = NULL;
                    windowDateBatch.titleIdSet = titleIdSet;
                    //windowDateBatch.windowGuidelineIdSet = windowGLIdSet;
                    ID batchprocessid = Database.executeBatch(windowDateBatch, 200);
                }          
            }
        }
    }
}