public with sharing class EAW_RuleBuilderController {
    @AuraEnabled
    public static Map<String, List<SObject>> getRules(String id, String sObjectType) {
        
        String value = sObjectType == 'EAW_Window_Guideline__c' ? 'Window_Guideline__c' : 'Release_Date_Guideline__c';
        
        String rulesQuery = 'SELECT id, Window_Guideline__c, Release_Date_Guideline__c FROM EAW_Rule__c WHERE ' + value + ' = :id';
        List<EAW_Rule__c> rules = Database.query(rulesQuery);

        if(rules.size() > 0) {
            List<String> ruleIds = new List<String>();

            for(EAW_Rule__c rule : rules) {
                ruleIds.add(rule.Id);
            }

            String ruleDetailsQuery = 'SELECT id, Rule_No__c, Condition_Set_Field__c, Condition_Met_Months__c, Condition_Met_Days__c, Condition_Met_Media__c, Condition_Met_Territory__c, ' +
                    'Start_Date__c, End_Date__c, WindowName__c, WindowName__r.Name  FROM EAW_Rule_Detail__c WHERE Rule_No__c = :ruleIds';
            List<EAW_Rule_Detail__c> ruleDetails = Database.query(ruleDetailsQuery);

            if(ruleDetails.size() > 0) {
                Map<String, List<SObject>> maps = new Map<String, List<SObject>>();
                maps.put('rules', rules);
                maps.put('ruleDetails', ruleDetails);

                return maps;
            }
        }

        return null;
    }

    @AuraEnabled
    public static Map<String, List<SObject>> setRules(String id, String sObjectType, String startRuleId, Decimal startMonths, Decimal startDays, String startTerritory,
                                                      String startMedia, String startWindowName, String startDateType, String endRuleId, Decimal endMonths,
                                                      Decimal endDays, String endTerritory, String endMedia, String endWindowName, String endDateType) {

        List<EAW_Rule__c> startRules = new List<EAW_Rule__c>();
        List<EAW_Rule__c> endRules = new List<EAW_Rule__c>();

        if(startRuleId.equals('')) {
            EAW_Rule__c rule = new EAW_Rule__c();
                
            if(sObjectType == 'EAW_Window_Guideline__c') {
                rule.Window_Guideline__c = id;
                rule.Territory__c = startTerritory;
            } else {
                rule.Release_Date_Guideline__c = id;
                rule.Territory__c = startTerritory;
            }

            startRules.add(rule);

            insert startRules;

            List<EAW_Window__c> startWindows = [SELECT Id, Start_Date__c, End_Date__c FROM EAW_Window__c WHERE Name = :startWindowName];
            EAW_Rule_Detail__c startRuleDetail = new EAW_Rule_Detail__c();
            startRuleDetail.Rule_No__c = startRules[0].id;
            startRuleDetail.Condition_Set_Field__c = 'Window Start Date';
            startRuleDetail.Condition_Met_Months__c = startMonths;
            startRuleDetail.Condition_Met_Days__c = startDays;
            startRuleDetail.Condition_Met_Territory__c = startTerritory;
            startRuleDetail.Condition_Met_Media__c = startMedia;
            
            if(startWindows.size() > 0) {
                startRuleDetail.WindowName__c = startWindows.get(0).Id;
    
                if(startDateType == 'Start Date') {
                    startRuleDetail.Start_Date__c = startWindows.get(0).Start_Date__c;
                } else if(startDateType == 'End Date') {
                    startRuleDetail.End_Date__c = startWindows.get(0).End_Date__c;
                }
            }

            insert startRuleDetail;
        } else {
            List<String> ruleIds = new List<String>();
            String query = 'SELECT id, Window_Guideline__c, Release_Date_Guideline__c FROM EAW_Rule__c WHERE id = :startRuleId';
            startRules = Database.query(query);

            for(EAW_Rule__c rule : startRules) {
                ruleIds.add(rule.Id);
            }

            String query2 = 'SELECT id, Rule_No__c, Condition_Set_Field__c, Condition_Met_Months__c, Condition_Met_Days__c, Condition_Met_Media__c, Condition_Met_Territory__c FROM EAW_Rule_Detail__c WHERE Rule_No__c = :ruleIds';
            List<EAW_Rule_Detail__c> ruleDetails = Database.query(query2);

            ruleDetails[0].Rule_No__c = startRules[0].id;
            ruleDetails[0].Condition_Met_Months__c = startMonths;
            ruleDetails[0].Condition_Met_Days__c = startDays;
            ruleDetails[0].Condition_Met_Territory__c = startTerritory;
            ruleDetails[0].Condition_Met_Media__c = startMedia;
            ruleDetails[0].Condition_Set_Field__c = 'Window Start Date';

            update ruleDetails;
        }

        if(endRuleId.equals('')) {
            EAW_Rule__c rule = new EAW_Rule__c();
            
            if(sObjectType == 'EAW_Window_Guideline__c') {
                rule.Window_Guideline__c = id;
                rule.Territory__c = endTerritory;
            } else {
                rule.Release_Date_Guideline__c = id;
                rule.Territory__c = endTerritory;
            }
            
            endRules.add(rule);

            insert endRules;

            List<EAW_Window__c> endWindows = [SELECT Id, Start_Date__c, End_Date__c FROM EAW_Window__c WHERE Name = :endWindowName];
            EAW_Rule_Detail__c endRuleDetail = new EAW_Rule_Detail__c();
            endRuleDetail.Rule_No__c = endRules[0].id;
            endRuleDetail.Condition_Set_Field__c = 'Window End Date';
            endRuleDetail.Condition_Met_Months__c = endMonths;
            endRuleDetail.Condition_Met_Days__c = endDays;
            endRuleDetail.Condition_Met_Territory__c = endTerritory;
            endRuleDetail.Condition_Met_Media__c = endMedia;
            
            if(endWindows.size() > 0) {
                endRuleDetail.WindowName__c = endWindows.get(0).Id;
                
                if(endDateType == 'Start Date') {
                    endRuleDetail.Start_Date__c = endWindows.get(0).Start_Date__c;
                } else if(endDateType == 'End Date') {
                    endRuleDetail.End_Date__c = endWindows.get(0).End_Date__c;
                }
            }
            
            insert endRuleDetail;
        } else {
            List<String> ruleIds = new List<String>();
            String query = 'SELECT id, Window_Guideline__c, Release_Date_Guideline__c FROM EAW_Rule__c WHERE id = :endRuleId';
            endRules = Database.query(query);

            for(EAW_Rule__c rule : endRules) {
                ruleIds.add(rule.Id);
            }

            String query2 = 'SELECT id, Rule_No__c, Condition_Set_Field__c, Condition_Met_Months__c, Condition_Met_Days__c, Condition_Met_Media__c, Condition_Met_Territory__c FROM EAW_Rule_Detail__c WHERE Rule_No__c = :ruleIds';
            List<EAW_Rule_Detail__c> ruleDetails = Database.query(query2);

            ruleDetails[0].Rule_No__c = endRules[0].id;
            ruleDetails[0].Condition_Met_Months__c = endMonths;
            ruleDetails[0].Condition_Met_Days__c = endDays;
            ruleDetails[0].Condition_Met_Territory__c = endTerritory;
            ruleDetails[0].Condition_Met_Media__c = endMedia;
            ruleDetails[0].Condition_Set_Field__c = 'Window End Date';

            update ruleDetails;
        }

        return null;
    }
}