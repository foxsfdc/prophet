@isTest
public class EAW_TitlePlanWindowController_Test{ 
    
    @testSetup static void setupTestData() {
             
         List<EAW_Tag__c> tagList = EAW_TestDataFactory.createTag(1, True,'Window Tag');
        List<EAW_Window_Guideline__c> wgList = EAW_TestDataFactory.createWindowGuideLine(2, FALSE);
        wgList[0].Product_Type__c = 'Feature';
        wgList[1].Product_Type__c = 'Feature';
        
        insert wgList;
        
        List<EAW_Window_Guideline_Strand__c> wgsList = EAW_TestDataFactory.createWindowGuideLineStrand(2, FALSE);
        
        wgsList[0].EAW_Window_Guideline__c = wgList[0].Id;
        wgsList[0].Media__c = 'Free TV';
        wgsList[0].Language__c = 'English';
        wgsList[0].Territory__c = 'India';
        
        wgsList[1].EAW_Window_Guideline__c = wgList[1].Id;
        wgsList[1].Media__c = 'Basic TV';
        wgsList[1].Language__c = 'English';
        wgsList[1].Territory__c = 'Albania';
        
        insert wgsList;
        
        wgList[0].Status__c = 'Active';
        wgList[1].Status__c = 'Active';
        
        update wgList;
        
        List<EAW_Plan_Guideline__c> pgList = EAW_TestDataFactory.createPlanGuideline(2, FALSE);
        pgList[0].Product_Type__c = 'Feature';
        pgList[1].Product_Type__c = 'Feature';
        
        insert pgList;
        
        List<EAW_Plan_Window_Guideline_Junction__c> pwgList = EAW_TestDataFactory.createPlanWindowGuideLineJunction(1, FALSE);
        pwgList[0].Plan__c = pgList[0].Id;
        pwgList[0].Window_Guideline__c = wgList[0].Id;
        
        //pwgList[1].Plan__c = pgList[1].Id;
        //pwgList[1].Window_Guideline__c = wgList[1].Id;
        
        insert pwgList;
        List<EAW_Title__c> title = EAW_TestDataFactory.createEAWTitle (1,false);//[SELECT Id FROM EAW_Title__c LIMIT 1];
        title[0].RLSE_CAL_YR__c = '2001';
        insert title;
        
        //String PlanQualifierRule = '{"sObjectType": "EAW_Rule__c"}';
        //String PlanQualifierRuleRuleDetail='{"sObjectType": "EAW_Rule_Detail__c", "Condition_Operator__c": ">", "Condition_Year__c": "2000", "Condition_Field__c": "Release Year"}';
        //EAW_TestRuleDataFactory.savePgQualifiers( PlanQualifierRule , PlanQualifierRuleRuleDetail, pgList[0].Id);
        
       
        pgList[0].Status__c = 'Active';
       // pgList[1].Status__c = 'Active';
        
        update pgList;
        
        //List<EAW_Plan__c> planList = EAW_TestDataFactory.createPlan(2, FALSE);
        //planList[0].Plan_Guideline__c = pgList[0].Id;
        //planList[1].Plan_Guideline__c = pgList[1].Id;
        
        //insert planList;
        
        String newruleContent = '{"sObjectType":"EAW_Rule__c","Nested__c":true}';
        String newruleDetailContent = '[{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Condition_Timeframe__c":"True_Earliest"},"dateCalcs":[{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Conditional_Operand_Id__c":"aA73D000000CyJjSAK","Condition_Met_Months__c":"2","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"},{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Conditional_Operand_Id__c":"aA73D000000CyJjSAK","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"}],"nodeCalcs":[{"ruleDetail":{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":2,"Condition_Timeframe__c":"True_Earliest"},"dateCalcs":[{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":2,"Conditional_Operand_Id__c":"aA73D000000CyJjSAK","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"}],"nodeCalcs":[]}]}]';
        EAW_TestRuleDataFactory.saveRuleAndRuleDetails(newruleContent,newruleDetailContent,false,wgList[0].Id,'Start Date',String.valueOf(wgList[0].Id));
        
        //EAW_TestDataFactory.createWindow (1,true);
        //String ruleAndRuleDetailNodes = '[{"ruleDetail":{"Id":"aAM3D00000003cjWAA","Rule_No__c":"aAN3D0000008RV7WAM","Rule_Order__c":1,"Nest_Order__c":1,"Condition_Met_Months__c":"2","Condition_Met_Days__c":0,"Conditional_Operand_Id__c":"aA73D0000004mIRSAY","Condition_Operand_Details__c":"Theatrical / Bahrain / Lithuanian / Feature"},"dateCalcs":[],"nodeCalcs":[]}]';
        //EAW_TestRuleDataFactory.saveRuleAndRuleDetails(null,ruleAndRuleDetailNodes,false,wgList[0].Id,'Start Date',wgList[0].Id);
        
    } 
    @isTest static void createPlan_Test(){
        
        List<EAW_Plan_Guideline__c> planguidLineList = [SELECT Id, Name FROM EAW_Plan_Guideline__c LIMIT 1];
        
        List<EAW_Title__c> titleList = [SELECT Id, Name FROM EAW_Title__c LIMIT 1];//EAW_TestDataFactory.createEAWTitle(1, True);
        //List<EAW_Plan__c> planList = [SELECT Id, Name FROM EAW_Plan__c LIMIT 1];
        //planList[0].EAW_Title__c = titleList[0].Id;
        //update planList;
        
        List<EAW_Window_Guideline__c> windowGuideLineList = [SELECT Id, Name FROM EAW_Window_Guideline__c LIMIT 1];
       
        
       List<EAW_Plan_Window_Guideline_Junction__c> planWindowList = [SELECT Id, Name FROM EAW_Plan_Window_Guideline_Junction__c LIMIT 1];
        
     
        
        EAW_Window_Guideline_Strand__c windowStrandIns = [SELECT Id, Name FROM EAW_Window_Guideline_Strand__c LIMIT 1];
        
        
      
        set<Id> testIdSet= new set<Id>();
        Map<String, set<Id>> testIdMap = new Map<String,set<Id>>();
        //testIdSet.add(winList[0].Id);
        testIdMap.put('1',testIdSet);
        
        Map<String, List<SObject>> createList = EAW_TitlePlanWindowController.createPlan(String.ValueOf(titleList[0].Id),String.ValueOf(planguidLineList[0].Id), 'Test Title', 'Test Body', false,50,0,JSON.serialize(testIdMap),'{"selectedCol":"Status__c","selectedAlpha":"F"}');
      
    }
    @isTest static void createWindow_Test(){
         
        List<EAW_Plan_Guideline__c> planguidLineList = [SELECT Id, Name FROM EAW_Plan_Guideline__c LIMIT 1];
        planguidLineList [0].Status__c = 'Active';
        
        String PlanQualifierRule = '{"sObjectType": "EAW_Rule__c"}';
        String PlanQualifierRuleRuleDetail='{"sObjectType": "EAW_Rule_Detail__c", "Condition_Operator__c": ">", "Condition_Year__c": "2000", "Condition_Field__c": "Release Year"}';
        EAW_TestRuleDataFactory.savePgQualifiers( PlanQualifierRule , PlanQualifierRuleRuleDetail, planguidLineList [0].Id);
        
        update planguidLineList ;
        
        List<EAW_Window_Guideline__c> windowGuideList = [SELECT Id, Name FROM EAW_Window_Guideline__c LIMIT 1];
       
        List<EAW_Title__c> titleList = [SELECT Id, Name FROM EAW_Title__c LIMIT 1];
        
        EAW_Plan__c plan = new EAW_Plan__c ();
        plan.EAW_Title__c = titleList[0].Id;
        plan.Plan_Guideline__c = planguidLineList[0].Id;
        
        List<EAW_Plan__c> planList = new List<EAW_Plan__c>();
        
        planList.add(plan); 
        insert planList;
        
        //List<EAW_Plan__c> planList = [SELECT Id, Name FROM EAW_Plan__c LIMIT 1];
        //planList[0].Plan_Guideline__c = planguidLineList [0].Id;
        //planList[0].EAW_Title__c = titleList[0].Id;
        //update planList;
        
        EAW_Window_Guideline_Strand__c windowStrandIns = [SELECT Id, Name FROM EAW_Window_Guideline_Strand__c LIMIT 1];
        windowGuideList[0].Status__c = 'Active';
        update windowGuideList;
        
        set<Id> testIdSet= new set<Id>();
        Map<String, set<Id>> testIdMap = new Map<String,set<Id>>();
       
        Map<String,List<SObject>> createwindowList = EAW_TitlePlanWindowController.createWindow(String.ValueOf(titleList[0].Id), String.ValueOf(planList[0].Id), String.ValueOf(windowGuideList[0].Id), 'Test Title', 'Test Body',50,0,JSON.serialize(testIdMap),null);        
     }
     @isTest static void createwithoutPlan_Test(){
     
       
        List<EAW_Plan_Guideline__c> planguidLineList = [SELECT Id, Name FROM EAW_Plan_Guideline__c LIMIT 1];
        String PlanQualifierRule = '{"sObjectType": "EAW_Rule__c"}';
        String PlanQualifierRuleRuleDetail='{"sObjectType": "EAW_Rule_Detail__c", "Condition_Operator__c": ">", "Condition_Year__c": "2000", "Condition_Field__c": "Release Year"}';
        EAW_TestRuleDataFactory.savePgQualifiers( PlanQualifierRule , PlanQualifierRuleRuleDetail, planguidLineList [0].Id);
        
        planguidLineList [0].Status__c = 'Active';
        
       
        update planguidLineList ;
        List<EAW_Title__c> titleList = [SELECT Id, Name FROM EAW_Title__c LIMIT 1];//EAW_TestDataFactory.createEAWTitle(1, True);
        EAW_Plan__c plan = new EAW_Plan__c ();
        plan.EAW_Title__c = titleList[0].Id;
        plan.Plan_Guideline__c = planguidLineList[0].Id;
        
        List<EAW_Plan__c> planList = new List<EAW_Plan__c>();
        
        planList.add(plan); 
        insert planList;
        
        
        List<EAW_Window_Guideline__c> windowGuideLineList = [SELECT Id, Name FROM EAW_Window_Guideline__c LIMIT 1];
       
        
       List<EAW_Plan_Window_Guideline_Junction__c> planWindowList = [SELECT Id, Name FROM EAW_Plan_Window_Guideline_Junction__c LIMIT 1];
        
     
        
        EAW_Window_Guideline_Strand__c windowStrandIns = [SELECT Id, Name FROM EAW_Window_Guideline_Strand__c LIMIT 1];
        
        
      
        set<Id> testIdSet= new set<Id>();
        Map<String, set<Id>> testIdMap = new Map<String,set<Id>>();
        //testIdSet.add(winList[0].Id);
        testIdMap.put('1',testIdSet);
        
        Map<String, List<SObject>> createList = EAW_TitlePlanWindowController.createPlan(String.ValueOf(titleList[0].Id),String.ValueOf(planguidLineList[0].Id), 'Test Title', 'Test Body', false,50,0,JSON.serialize(testIdMap),'{"selectedCol":"Status__c","selectedAlpha":"F"}');
      
      }    
}