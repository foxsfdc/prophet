@isTest
public class EAW_PlanHandler_Test {
    
    @isTest static void testMethod1() {
        
        EDM_REF_DIVISION__c refDivision = new EDM_REF_DIVISION__c(Name = 'Fox');
        insert refDivision;
        
        EDM_REF_PRODUCT_TYPE__c refProduct = new EDM_REF_PRODUCT_TYPE__c(Name = 'Feature');
        insert refProduct;
        
        EAW_Title__c title = new EAW_Title__c(Name='Test Title-100', PROD_TYP_CD_INTL__c = refProduct.id, FIN_DIV_CD__c = refDivision.Id, LIFE_CYCL_STAT_GRP_CD__c = 'PUBLC');
        insert title;
        
        List<EAW_Window_Guideline__c> wgList = EAW_TestDataFactory.createWindowGuideLine(2, FALSE);
        wgList[0].Product_Type__c = 'Feature';
        wgList[1].Product_Type__c = 'Feature';
        
        insert wgList;
        
        List<EAW_Window_Guideline_Strand__c> wgsList = EAW_TestDataFactory.createWindowGuideLineStrand(2, FALSE);
        
        wgsList[0].EAW_Window_Guideline__c = wgList[0].Id;
        wgsList[0].Media__c = 'Free On Demand';
        wgsList[0].Language__c = 'English';
        wgsList[0].Territory__c = 'India';
        
        wgsList[1].EAW_Window_Guideline__c = wgList[1].Id;
        wgsList[1].Media__c = 'EST';
        wgsList[1].Language__c = 'English';
        wgsList[1].Territory__c = 'Albania';
        
        insert wgsList;
        
        wgList[0].Status__c = 'Active';
        wgList[1].Status__c = 'Active';
        
        update wgList;
        
        List<EAW_Plan_Guideline__c> pgList = EAW_TestDataFactory.createPlanGuideline(2, FALSE);
        pgList[0].Product_Type__c = 'Feature';
        pgList[1].Product_Type__c = 'Feature';
        
        insert pgList;
        
        List<EAW_Plan_Window_Guideline_Junction__c> pwgList = EAW_TestDataFactory.createPlanWindowGuideLineJunction(2, FALSE);
        pwgList[0].Plan__c = pgList[0].Id;
        pwgList[0].Window_Guideline__c = wgList[0].Id;
        
        pwgList[1].Plan__c = pgList[1].Id;
        pwgList[1].Window_Guideline__c = wgList[1].Id;
        
        insert pwgList;
        
        pgList[0].Status__c = 'Active';
        pgList[1].Status__c = 'Active';
        
        update pgList;
        
        List<EAW_Plan__c> planList = EAW_TestDataFactory.createPlan(2, FALSE);
        planList[0].Plan_Guideline__c = pgList[0].Id;
        planList[0].EAW_Title__c = title.Id;
        
        planList[1].Plan_Guideline__c = pgList[1].Id;
        planList[1].EAW_Title__c = title.Id;
            
        insert planList;
    }
    
    @isTest static void testMethod2() {
    
        EDM_REF_DIVISION__c refDivision = new EDM_REF_DIVISION__c(Name = 'Fox 2000');
        insert refDivision;
        
        EDM_REF_PRODUCT_TYPE__c refProduct = new EDM_REF_PRODUCT_TYPE__c(Name = 'Compilation Episode');
        insert refProduct;
        
        EAW_Title__c title = new EAW_Title__c(Name='Test Title-101', PROD_TYP_CD_INTL__c = refProduct.id, FIN_DIV_CD__c = refDivision.Id, LIFE_CYCL_STAT_GRP_CD__c = 'PUBLC');
        insert title;
        
        List<EAW_Window_Guideline__c> wgList = EAW_TestDataFactory.createWindowGuideLine(1, FALSE);
        
        wgList[0].Product_Type__c = 'Compilation Episode';
        wgList[0].Window_Guideline_Alias__c = 'Test WG1';
        
        insert wgList;
        
        List<EAW_Window_Guideline_Strand__c> wgsList = EAW_TestDataFactory.createWindowGuideLineStrand(1, FALSE);
        
        wgsList[0].EAW_Window_Guideline__c = wgList[0].Id;
        wgsList[0].Media__c = 'Free On Demand';
        wgsList[0].Language__c = 'English';
        wgsList[0].Territory__c = 'Algeria';
        
        insert wgsList;
        
        wgList[0].Status__c = 'Active';
        
        update wgList;
        
        List<EAW_Plan_Guideline__c> pgList = EAW_TestDataFactory.createPlanGuideline(1, FALSE);
        pgList[0].Name = 'Test PG-1';
        pgList[0].Product_Type__c = 'Compilation Episode';
        
        insert pgList;
        
        List<EAW_Plan_Window_Guideline_Junction__c> pwgList = EAW_TestDataFactory.createPlanWindowGuideLineJunction(1, FALSE);
        pwgList[0].Plan__c = pgList[0].Id;
        pwgList[0].Window_Guideline__c = wgList[0].Id;
        
        insert pwgList;
        
        pgList[0].Status__c = 'Active';
        
        update pgList;
        
        EAW_Plan__c plan = new EAW_Plan__c ();
        plan.EAW_Title__c = title.Id;
        plan.Plan_Guideline__c = pgList[0].Id;
        
        insert plan;
        
        plan.Name = 'Test Plan';
        update plan;
    }
}