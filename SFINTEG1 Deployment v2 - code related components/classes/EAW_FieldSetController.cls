public with sharing class EAW_FieldSetController {
    
    @AuraEnabled
    public static List<DataTableColumns> getFields(String strObjectName, String strFieldSetName) {
        
        List<DataTableColumns> lstDataColumns = new List<DataTableColumns>();

        try {
        
            Schema.SObjectType SObjectTypeObj = Schema.getGlobalDescribe().get(strObjectName);
            Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
            Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(strFieldSetName);
            Map<String, Schema.SObjectField> fieldMap = DescribeSObjectResultObj.fields.getMap();

            if (fieldSetObj == null) {
                throw new AuraHandledException(strFieldSetName + ' fieldset not found');
            }

            for (Schema.FieldSetMember eachFieldSetMember : fieldSetObj.getFields()) {
                
                Boolean updateable = false;
                Boolean accessible = false;
                DataTableColumns datacolumns;

                String dataType = String.valueOf(eachFieldSetMember.getType()).toLowerCase();
                String fieldApiName = String.valueOf(eachFieldSetMember.getFieldPath());
                List<picklistEntry> picklistValues = new List<picklistEntry>();
                
                if (dataType == 'datetime') {
                    dataType = 'date';
                }
                
                if( dataType == 'picklist' ){
                    Schema.DescribeFieldResult fieldResult = fieldMap.get(fieldApiName).getDescribe();
                    List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                    for( Schema.PicklistEntry pValues : ple){
                        if( pValues.isActive() ){
                            picklistValues.add(new picklistEntry(pValues.getLabel(),  pValues.getValue()));
                        }
                    }
                }
                
                if (fieldMap.get(eachFieldSetMember.getFieldPath()).getDescribe().isAccessible())
                    accessible = true;
                if (fieldMap.get(eachFieldSetMember.getFieldPath()).getDescribe().isUpdateable())
                    updateable = true;

                if (accessible) {
                    //Create a wrapper instance and store label, fieldname and type.
                    
                    String labelVal = String.valueOf(eachFieldSetMember.getLabel());
                    
                    if( ! fieldMap.get(eachFieldSetMember.getFieldPath()).getDescribe().isCustom() ) {
                        if( labelVal == 'Created By ID')
                            labelVal = labelVal.replace('ID','').trim();
                    }
                    
                    datacolumns = new DataTableColumns( labelVal ,
                            fieldApiName,
                            dataType,
                            updateable, picklistValues);
                }
                lstDataColumns.add(datacolumns);
            }
        } 
        catch (Exception e) {
            system.debug('EAW_FieldSetController.getFields:' + e.getMessage() + e.getStackTraceString());
        }
        return lstDataColumns;
    }
     @AuraEnabled
    public static List<sObject> getRecords(String strObjectName, String strFieldSetName, String queryCondition,
                                           List<String> planNames, List<String> wgsBasedPGIds) {
         //LRCC-1675 - added last null                                   
        return genericGetRecords(strObjectName, strFieldSetName, queryCondition,
                                           planNames, wgsBasedPGIds, NULL,NULL);
        
    }

    @AuraEnabled//LRCC-1675 - added lastBatchIds  
    public static List<sObject> genericGetRecords(String strObjectName, String strFieldSetName, String queryCondition,
                                           List<String> planNames, List<String> wgsBasedPGIds, List<String> paginationIdSet,List<String> lastBatchIds) {
        
        List<SObject> tableData = new List<SObject>();
        List<String> lstFieldsToQuery = new List<String>();
        
        try {
        
            Schema.SObjectType SObjectTypeObj = Schema.getGlobalDescribe().get(strObjectName);
            Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
            Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(strFieldSetName);
            Map<String, Schema.SObjectField> fieldMap = DescribeSObjectResultObj.fields.getMap();
            
            if (fieldSetObj == null) {
                throw new AuraHandledException(strFieldSetName + ' fieldset not found');
            }
            for (Schema.FieldSetMember eachFieldSetMember : fieldSetObj.getFields()) {
            
                if (String.valueOf(eachFieldSetMember.getType()).toLowerCase() == 'reference') {
                    String fieldAPI;
                    if( fieldMap.get(eachFieldSetMember.getFieldPath()).getDescribe().isCustom() ){
                        fieldAPI = String.valueOf(eachFieldSetMember.getFieldPath());
                        fieldAPI = fieldAPI.removeEndIgnoreCase('__c') + '__r.Name';
                    } else {
                        fieldAPI = String.valueOf(eachFieldSetMember.getFieldPath());
                        fieldAPI = fieldAPI.removeEndIgnoreCase('Id')+'.Name';
                    }
                    lstFieldsToQuery.add(fieldAPI);
                } else {
                    lstFieldsToQuery.add(String.valueOf(eachFieldSetMember.getFieldPath()));
                }
            }
            
            //Form an SOQL to fetch the data - Set the wrapper instance and return as response
            //LRCC-1675 - Added createdDate field in the query
            
            String query = 'SELECT Id, ' + String.join(lstFieldsToQuery, ',') + ' ,CreatedDate  FROM ' + strObjectName + queryCondition;
            tableData = Database.query(query);
            system.debug(';;;;;;'+tableData );
        } 
        catch (Exception e) {}
        
        return tableData;
    }
    
    @AuraEnabled
    public static List<sObject> GetRecordsCount(String strObjectName, String strFieldSetName, String queryCondition,
                                                List<String> planNames, List<String> wgsBasedPGIds, List<String> paginationIdSet) {
        
        List<SObject> tableData = new List<SObject>();
        List<String> lstFieldsToQuery = new List<String>();
        
        try {
        
            Schema.SObjectType SObjectTypeObj = Schema.getGlobalDescribe().get(strObjectName);
            Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
            Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(strFieldSetName);
            Map<String, Schema.SObjectField> fieldMap = DescribeSObjectResultObj.fields.getMap();
            
            if (fieldSetObj == null) {
                throw new AuraHandledException(strFieldSetName + ' fieldset not found');
            }
            for (Schema.FieldSetMember eachFieldSetMember : fieldSetObj.getFields()) {
            
                if (String.valueOf(eachFieldSetMember.getType()).toLowerCase() == 'reference') {
                    String fieldAPI;
                    if( fieldMap.get(eachFieldSetMember.getFieldPath()).getDescribe().isCustom() ){
                        fieldAPI = String.valueOf(eachFieldSetMember.getFieldPath());
                        fieldAPI = fieldAPI.removeEndIgnoreCase('__c') + '__r.Name';
                    } else {
                        fieldAPI = String.valueOf(eachFieldSetMember.getFieldPath());
                        fieldAPI = fieldAPI.removeEndIgnoreCase('Id')+'.Name';
                    }
                    lstFieldsToQuery.add(fieldAPI);
                } else {
                    lstFieldsToQuery.add(String.valueOf(eachFieldSetMember.getFieldPath()));
                }
            }
            
            //Form an SOQL to fetch the data - Set the wrapper instance and return as response
            String query = 'SELECT COUNT(Id) FROM ' + strObjectName + queryCondition;
            tableData = Database.query(query);
            system.debug('tableData ::::'+tableData );
        } 
        catch (Exception e) {}
        
        return tableData;
    }

    @AuraEnabled
    public static string getBaseUrl() {
        return URL.getSalesforceBaseUrl().toExternalForm();
    }

    //Wrapper class to hold Columns with headers
    public class DataTableColumns {
        @AuraEnabled
        public String label { get; set; }
        @AuraEnabled
        public String fieldName { get; set; }
        @AuraEnabled
        public String type { get; set; }
        @AuraEnabled
        public Boolean updateable { get; set; }
        @AuraEnabled
        public List<picklistEntry> picklistValues { get; set; }

        //Create and set three variables label, fieldname and type as required by the lightning:datatable
        public DataTableColumns(String label, String fieldName, String type, Boolean updateable, List<picklistEntry> picklistValues) {
            this.label = label;
            this.fieldName = fieldName;
            this.type = type;
            this.updateable = updateable;
            this.picklistValues  = picklistValues;
        }
    }
    
    public class picklistEntry {
        @AuraEnabled
        public String label { get; set; }
        @AuraEnabled
        public String value { get; set; }
        public picklistEntry(String label, String value) {
            this.label = label;
            this.value = value;
        }
    }
    @AuraEnabled
    public static List<sObject> getLookupRecords(String sObjectName,String whereCondition){
        List<sObject> objectList = new List<sObject>();
        String queryCondition = 'SELECT Id,Name FROM ' + sObjectName;
        if(whereCondition != null){
           queryCondition += ' '+ whereCondition;
        }
        //LRCC-1695
        queryCondition += ' ORDER BY Name ';
        objectList = Database.Query(queryCondition);
        return objectList;
    }
}