/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class EAW_ProjectDatesController_Test {

    static testMethod void myUnitTest() {
    	EDM_REF_PRODUCT_TYPE__c prodType = new EDM_REF_PRODUCT_TYPE__c(Name='Feature',PROD_TYP_CD__c='FEATR',PROD_TYP_DESC__c='Feature'); 
		insert prodType;
		EAW_title__C etc =new EAW_title__C(FOX_ID__c='10775',name='Mr. & Mrs. Smith',PROD_TYP_CD_INTL__c=prodType.id,PROD_TYP_CD__c=prodType.id,FIN_PROD_ID__c='10775'); 
        insert etc;
        EAW_Release_Date_Guideline__c releaseDateGuideLine = new EAW_Release_Date_Guideline__c( Product_Type__c = 'Feature', Territory__c = 'United States +', Language__c = 'English', EAW_Release_Date_Type__c='EST-HD');
        insert releaseDateGuideLine;
        EAW_Release_Date__c erdc = new EAW_Release_Date__c(Active__c=true,Language__c=releaseDateGuideLine.Language__c,Territory__c=releaseDateGuideLine.Territory__c
        													,Product_Type__c=releaseDateGuideLine.Product_Type__c,EAW_Release_Date_Type__c=releaseDateGuideLine.EAW_Release_Date_Type__c
        													,Title__c=etc.ID,Status__c='Estimated',Projected_Date__c=Date.today(),Release_Date_Guideline__c=releaseDateGuideLine.Id);
        insert erdc;
        EAW_Territories__c etec = new EAW_Territories__c(Name='United States +',Name__c='United States +',Territory_Code__c='US',Territory_Id__c=248);
        insert etec;
        EAW_Languages__c elec = new EAW_Languages__c(Name='English',Name__c='English',Language_Id__c=18);
        insert elec;
        List<EAW_ProjectDatesController.ProjectedDate> projectedDates = EAW_ProjectDatesController.getProjectedDate(new String[]{'10775'},new String[]{'248'});
        system.assertEquals(projectedDates.size(),1);
    }
    
    
     static testMethod void createTestData(){
                
                /*List < EAW_ProjectDatesController.ProjectedDate > lstProjectedDates = new List < EAW_ProjectDatesController.ProjectedDate >();
                        
                EAW_ProjectDatesController.ProjectedDate objProjectedDate = new EAW_ProjectDatesController.ProjectedDate();
                objProjectedDate.territoryId = '505';
                objProjectedDate.territoryCode = null;
                objProjectedDate.title = 'Test Title';
                objProjectedDate.projectedDate = '2019-07-29';
                objProjectedDate.territoryName = 'UK';
                objProjectedDate.langId = '2';
                objProjectedDate.channel = 'EST';
                objProjectedDate.finTitleId = 'TEST100';
                lstProjectedDates.add( objProjectedDate );
                                
                objProjectedDate = new EAW_ProjectDatesController.ProjectedDate();
                objProjectedDate.territoryId = '505';
                objProjectedDate.territoryCode = null;
                objProjectedDate.title = 'Test Title';
                objProjectedDate.projectedDate = '2019-07-30';
                objProjectedDate.territoryName = 'UK';
                objProjectedDate.langId = '2';
                objProjectedDate.channel = 'RETAIL';
                objProjectedDate.finTitleId = 'TEST100';
                lstProjectedDates.add( objProjectedDate );
                
                objProjectedDate = new EAW_ProjectDatesController.ProjectedDate();
                objProjectedDate.territoryId = '505';
                objProjectedDate.territoryCode = null;
                objProjectedDate.title = 'Test Title';
                objProjectedDate.projectedDate = '2019-07-31';
                objProjectedDate.territoryName = 'UK';
                objProjectedDate.langId = '2';
                objProjectedDate.channel = 'VOD';
                objProjectedDate.finTitleId = 'TEST100';
                lstProjectedDates.add( objProjectedDate );

                EAW_ProjectDatesController.createTestProjectedDates( lstProjectedDates );
                EAW_ProjectDatesController.getProjectedDate(new String[]{'TEST100'},new String[]{'505'});*/
                List < EAW_ProjectDatesController.ProjectedDate > lstProjectedDates = new List < EAW_ProjectDatesController.ProjectedDate > ();
				lstProjectedDates.add( HEP_ProphetProjectedDateWrapper.createEAWProjectedDate('79', 'BG', 'Bulgaria', '197429', 'Where\'d You Go Bernadette', '2', 'SPEST', '2019-07-20' ) );
				lstProjectedDates.add( HEP_ProphetProjectedDateWrapper.createEAWProjectedDate('79', 'BG', 'Bulgaria', '197429', 'Where\'d You Go Bernadette', '2', 'PEST', '2019-07-20' ) );
				lstProjectedDates.add( HEP_ProphetProjectedDateWrapper.createEAWProjectedDate('79', 'BG', 'Bulgaria', '197429', 'Where\'d You Go Bernadette', '2', 'EST', '2019-07-06' ) );
				lstProjectedDates.add( HEP_ProphetProjectedDateWrapper.createEAWProjectedDate('79', 'BG', 'Bulgaria', '197429', 'Where\'d You Go Bernadette', '2', 'SPVOD', '2019-07-20' ) );
				lstProjectedDates.add( HEP_ProphetProjectedDateWrapper.createEAWProjectedDate('79', 'BG', 'Bulgaria', '197429', 'Where\'d You Go Bernadette', '2', 'PVOD', '2019-07-20' ) );
				lstProjectedDates.add( HEP_ProphetProjectedDateWrapper.createEAWProjectedDate('79', 'BG', 'Bulgaria', '197429', 'Where\'d You Go Bernadette', '2', 'VOD', '2019-07-20' ) );
				lstProjectedDates.add( HEP_ProphetProjectedDateWrapper.createEAWProjectedDate('79', 'BG', 'Bulgaria', '197429', 'Where\'d You Go Bernadette', '2', 'RETAIL', '2019-07-20' ) );
				lstProjectedDates.add( HEP_ProphetProjectedDateWrapper.createEAWProjectedDate('79', 'BG', 'Bulgaria', '197429', 'Where\'d You Go Bernadette', '2', 'RENTAL', '2019-07-20' ) );
				lstProjectedDates.add( HEP_ProphetProjectedDateWrapper.createEAWProjectedDate('79', 'BG', 'Bulgaria', '197429', 'Where\'d You Go Bernadette', '2', 'KIOSK', '2019-07-20' ) );
				lstProjectedDates.add( HEP_ProphetProjectedDateWrapper.createEAWProjectedDate('509', 'US', 'US', '197429', 'Where\'d You Go Bernadette', '2', 'EST', '2019-07-11' ) );
				EAW_ProjectDatesController.createTestProjectedDates( lstProjectedDates );
                
    }
    
}