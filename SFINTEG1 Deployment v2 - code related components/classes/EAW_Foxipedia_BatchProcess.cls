public class EAW_Foxipedia_BatchProcess implements Database.Batchable<sObject>,Database.Stateful, Schedulable,Database.AllowsCallouts {
    
    public Set<Id> windowGuideLineIdSet = new Set<Id>();
    public Set<Id> rdGuideLineIdSet = new Set<Id>();
    public Set<Id> titleIdSet = new Set<Id>();
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
    
        List<String> statusToProcess = new List<String>{'Failed','Need to Process'};
        //Maximum 100 title will be handled in whole batch execution, since we may hit apex heap size exception
        return Database.getQueryLocator(
        
        'select Changed_Entity__c,FOX_ID__c,Source_System__c,Status__c from EAW_Inbound_Notifications__c where Source_System__c=\'foxipedia\' and Status__c IN :statusToProcess ORDER BY CreatedDate ASC NULLS FIRST LIMIT 100'
       //'select Changed_Entity__c,FOX_ID__c,Source_System__c,Status__c from EAW_Inbound_Notifications__c where Source_System__c=\'foxipedia\' and Status__c IN :statusToProcess and FOX_ID__c in (\'2047118\')'
        );
    }
    
    public void execute(Database.BatchableContext bc, List<EAW_Inbound_Notifications__c> records){
        if(records!=null && records.isempty()==False){
            
            List<EAW_Inbound_Notifications__c> inboundList = new List<EAW_Inbound_Notifications__c>();
            EAW_Inbound_Notifications__c tempInbound;// = new EAW_Inbound_Notifications__c();
            
            try {
                for(EAW_Inbound_Notifications__c eawIN: records) {
                    
                    if(String.isNotBlank(eawIN.FOX_ID__c)){
                        
                        checkRecursiveData.bypassDateCalculation = True;
                        EAW_FoxipediaRestAPIReqController efarcJob = new EAW_FoxipediaRestAPIReqController('Title.Title',eawIN.FOX_ID__c,eawIN);
                        efarcJob.getAllTitleDetails();
                        
                        if(efarcJob.titleIdFoxeBatch.size()>0) {
                            titleIdSet.addAll(efarcJob.titleIdFoxeBatch);
                        }
                        if(EAW_PlanGuidelineQualifierController.wgIds.size()>0) {
                            windowGuideLineIdSet.addAll(EAW_PlanGuidelineQualifierController.wgIds);
                        }
                        
                        if(efarcJob.releaseDateGuidelineIdsFoxeBatch.size()>0) {
                            rdGuideLineIdSet.addAll(efarcJob.releaseDateGuidelineIdsFoxeBatch);
                        }
                        tempInbound = new EAW_Inbound_Notifications__c(Id=eawIN.Id, Status__c='Success');
                        inboundList.add(tempInbound);
                    }
                }
            }catch(Exception e) {
                for(EAW_Inbound_Notifications__c eawIN: records) {
                    tempInbound = new EAW_Inbound_Notifications__c(Id=eawIN.Id, Status__c='Failed');
                    tempInbound.Message__c = e.getMessage();
                    inboundList.add(tempInbound);
                }
            }
            
            if(inboundList.size()>0){
                update inboundList;
            }
        }
    }
        
    public void finish(Database.BatchableContext bc) {
        // execute any post-processing operations
        EAW_RDGDateCalculationBatchClass releaseDateBatch = new EAW_RDGDateCalculationBatchClass(rdGuideLineIdSet);
        releaseDateBatch.titleIdSet = titleIdSet;
        releaseDateBatch.windowGuidelineIdSet = windowGuideLineIdSet;
        ID batchprocessid = Database.executeBatch(releaseDateBatch, 200);
    }    
    
    public void execute(SchedulableContext sc) {
        
        EAW_Foxipedia_BatchProcess foxiBatch = new EAW_Foxipedia_BatchProcess();
        Database.executeBatch(foxiBatch,1);//size should be one, since we may hit too many dml rows exception
    }
}