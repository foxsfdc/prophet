public with sharing class EAW_CollectPicklistValues {

    @AuraEnabled
    public static list<String> getGenericObject(String genericObjectName, String genericFieldName) {
        Schema.SObjectType genericObjectType = Schema.getGlobalDescribe().get(genericObjectName);
        SObject genericObject = genericObjectType.newSObject();

        Schema.sObjectType objectType = genericObject.getSObjectType();
        Schema.DescribeSObjectResult objectDescribe = objectType.getDescribe();   
        map<String, Schema.SObjectField> fieldMap = objectDescribe.fields.getMap(); 
        list<Schema.PicklistEntry> picklistEntries = fieldMap.get(genericFieldName).getDescribe().getPickListValues();

        list<String> values = new list<String>();
        for(Schema.PicklistEntry pickListEntry : picklistEntries) {
            values.add(pickListEntry.getValue());
        }

        return values;
    }
}