@isTest
public class EAW_ReleaseDateUpdate_Test {
    
   @testSetup static void setupTestData() {
        
        EAW_TestDataFactory.createReleaseDate(1, True);
        
    }
    
   @isTest static void EAW_ReleaseDateUpdate_Test(){
   
       List<EAW_Release_Date__c> releaseDateList = [SELECT Id, Name FROM EAW_Release_Date__c LIMIT 1];
       List<EAW_ReleaseDateUpdate.releaseDateUpdate> releaseDateWrapperList = new List<EAW_ReleaseDateUpdate.releaseDateUpdate>();
       
       EAW_ReleaseDateUpdate.releaseDateUpdate releasedate = new EAW_ReleaseDateUpdate.releaseDateUpdate();
       releasedate.releaseDateId = releaseDateList[0].Id;
       releasedate.manualDate = '2018-10-09 00:00:00';
       releasedate.projectedDate = '2018-10-26 00:00:00';
       releasedate.releaseDate = '2018-10-31 00:00:00';
       releasedate.feedDate = '2018-08-06 00:00:00';
       
       releaseDateWrapperList.add(releasedate);
       
       EAW_ReleaseDateUpdate.updateReleaseDates(releaseDateWrapperList );
       
       List<EAW_Release_Date__c> releaseDateListObt = [SELECT Id, Name FROM EAW_Release_Date__c LIMIT 1];
       System.assertEquals(true, releaseDateListObt != null);
   }
}