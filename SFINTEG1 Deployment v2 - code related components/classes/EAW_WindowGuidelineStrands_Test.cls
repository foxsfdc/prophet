/**
 
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class EAW_WindowGuidelineStrands_Test {
    @testSetup static void setupTestData() {
    
        List<Rest_Endpoint_Settings__c> restEndPoint = new List<Rest_Endpoint_Settings__c>{
            
            new Rest_Endpoint_Settings__c(Name  = 'Import Categories', End_Point__c  = 'https://feg-devapi.foxinc.com/repo-service-external/titleListCategory')
        };
        insert restEndPoint;
        
        List<Rest_Endpoint_Settings__c> restsend = new List<Rest_Endpoint_Settings__c> {
            
            new Rest_Endpoint_Settings__c (Name = 'Send Categories', End_Point__c  = 'https://feg-devapi.foxinc.com/repo-service-external/MLT')
        };
        insert restsend;
        EAW_TestDataFactory.createWindowGuideLine(1, True);
    }
    
    static testMethod void myUnitTest() {
        
        Map<String, Object> dynamicFieldsAndValuesMap; 
        
       // dynamicFieldsAndValuesMap = new Map<String, Object>{ 'Window_Type__c' => 'First-Run ', 'Media__c' => 'Non-Theatrical', 'Product_Type__c' => 'Episode', 'Status__c' => 'Draft'};
        List<EAW_Window_Guideline__c> windowGuideList = EAW_TestDataFactory.createWindowGuideLine( 2, true);
        
        
        dynamicFieldsAndValuesMap = new Map<String, Object>{'EAW_Window_Guideline__c' => windowGuideList[0].Id, 'Territory__c' => 'Australia', 'Media__c' => 'Non-Theatrical - Other', 'Language__c' => 'English;Somali', 'License_Type__c' => 'Exhibition License' };
        List<EAW_Window_Guideline_Strand__c> windowStrandList = (List<EAW_Window_Guideline_Strand__c>)EAW_TestDataFactory.createGenericSameTestRecords( dynamicFieldsAndValuesMap,  'EAW_Window_Guideline_Strand__c', 1, False);
        
        EAW_WindowGuidelineStrands.insertImportedWindowGuidelineStrands(windowStrandList.Clone());
        EAW_WindowGuidelineStrands.removeWindowGuidelineStrands(windowStrandList[0].Id);
        
    }
    static testMethod void sendIDValues_Test() {    
        
        EAW_WindowGuidelineStrands.idClass idclassWrapper = new EAW_WindowGuidelineStrands.idClass();
        try {
        
        EAW_WindowGuidelineStrands.sendIDValues('810', '2', '611723');
        } catch(Exception ex) {
        }
    }    
    
    static testMethod void sendIDcate_Test() {
        List<EAW_Window_Guideline__c> windowGuideLineList = [SELECT Id, Name FROM EAW_Window_Guideline__c]; 
        String body =  '{"titleListCategoryId": "1591034","rightsVarianceId": "4383","rightsVarianceDescription": "NEX Dominican Republic","explicitMediaId": "183457","titleListCategoryName": "SVOD Series - New & Returning - Linear"}';
        try {
            EAW_WindowGuidelineStrands.sendCategories(JSON.serialize(body), 'Append', String.ValueOf(windowGuideLineList[0].Id));
        } catch(Exception ex) {
        }
    }
}