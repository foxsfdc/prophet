@isTest
public class EAW_GenericCustomerLookupController_Test{
    
    static testMethod void updateCustomerField_Test() {
    
       List<EAW_Window_Guideline__c> windowGuidelineList =  EAW_TestDataFactory.createWindowGuideLine(1, true);
       List<EAW_Customer__c> customerList =  EAW_TestDataFactory.createEAWCustomer(1, true);
       EAW_GenericCustomerLookupController.updateCustomerField(customerList[0].Id,windowGuidelineList[0].Id);
       EAW_GenericCustomerLookupController.getCustomer('EAW_Window_Guideline__c',windowGuidelineList[0].Id);
    }
}