/**
 * Test class for EAWPlanSearchController
 * This class has logic to cover the SOSL search results of EAW_Plan_Guideline__c object
*/
@isTest
private class EAW_PlanSearchController_Test {

    @testSetup static void setupTestData() {
        // Call appropriate methods in EAW_TestDataFactory to create test records
        EAW_TestDataFactory.createPlanGuideline(2, true);
    }
    
    @isTest static void testEAWPlanSearch(){
        
        List<EAW_Plan_Guideline__c> planGuideList = [ Select Id, Name From EAW_Plan_Guideline__c ] ;
        System.debug(':: planguideList ::'+planguideList+':: size ::'+planguideList.size());
        
        Id[] fixedSearchResults = new Id[]{};
        
        for(Integer i = 0; i < 2; i++ ) {
            fixedSearchResults.add(planGuideList[i].Id);
        }
        // Use setFixedSearchResults method in test class for the SOSL query
        Test.setFixedSearchResults(fixedSearchResults);
        List<String> searchResults = EAWPlanSearchController.searchForIds('Test Plan Guideline');
        System.debug(':: searchResults ::'+searchResults +':: size ::'+searchResults .size());

        system.assertequals(2,searchResults.size());
    } 
}