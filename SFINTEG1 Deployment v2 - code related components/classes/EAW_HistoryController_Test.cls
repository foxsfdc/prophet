@isTest
private class EAW_HistoryController_Test {

    @testSetup static void setupTestData() {
                
        List<EAW_Release_Date__c> releaseDates = EAW_TestDataFactory.createReleaseDate(1,true);
        List<EAW_Window__c> windows = EAW_TestDataFactory.createWindow(1,true);
        
        List<EAW_Tag__c> tags = new List<EAW_Tag__c>();
        EAW_Tag__c releaseDateTag = new EAW_Tag__c(Name = 'Release Date Tag', Tag_Type__c = 'Release Date Tag');
        EAW_Tag__c windowTag = new EAW_Tag__c(Name = 'Window Tag', Tag_Type__c = 'Window Tag');        
        tags.add(releaseDateTag);
        tags.add(windowTag);
        insert tags;
        
        List<SObject> junctions = new List<SObject>{
                                        new EAW_Release_Date_Tag_Junction__c(Release_Date__c = releaseDates[0].Id, Tag__c = releaseDateTag.Id),
                                        new EAW_Window_Tag_Junction__c(Window__c = windows[0].Id, Tag__c = windowTag.Id)};
        insert junctions; 
        
        insert (new Note(Title = 'RD Test Title', Body = 'RD Test Body', ParentId = releaseDates[0].Id));
        insert (new Note(Title = 'Window Test Title', Body = 'Window Test Body', ParentId = windows[0].Id));
      
    }
    
    @isTest static void testgetWindowHistoryMethod() {
    
        List<EAW_Window__c> windowList = [SELECT Id FROM EAW_Window__c];
        
        update (new EAW_Window__c(Id = windowList[0].Id, License_Info_Codes__c = 'ADV'));
        
        EAW_HistoryController.WrapperClass windowRecordDetail = EAW_HistoryController.getWindowHistory(windowList[0].Id);
        EAW_Window__c windowRec = (EAW_Window__c)windowRecordDetail.records[0];
        
        System.assertEquals(windowRec.LicenseInfoCodes_OldValue__c != null, true);
        System.assertEquals(windowRec.OldTags__c != null, true);
        System.assertEquals(windowRec.OldNotes__c != null, true);
        
    }
    
    @isTest static void testgetReleaseDateHistoryMethod() {
    
        List<EAW_Release_Date__c> RDList = [SELECT Id FROM EAW_Release_Date__c];
        
        EAW_HistoryController.WrapperClass RDRecordDetail = EAW_HistoryController.getReleaseDateHistory(RDList[0].Id);
        EAW_Release_Date__c RDRec = (EAW_Release_Date__c)RDRecordDetail.records[0];
        
        System.assertEquals(RDRec.OldTags__c != null, true);
        System.assertEquals(RDRec.OldNotes__c != null, true);
        
    }
}