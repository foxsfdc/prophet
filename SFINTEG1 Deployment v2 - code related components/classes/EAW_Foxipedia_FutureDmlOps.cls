public with sharing class EAW_Foxipedia_FutureDmlOps {
    
    @future 
	public static void upsertBoxOffice(String eawBoxOfficeListJson) {
  		system.debug('eawBoxOfficeListJson: '+eawBoxOfficeListJson);
  		List<EAW_Box_Office__c> eawBoxOfficeList = (List<EAW_Box_Office__c>)System.JSON.deserialize(eawBoxOfficeListJson,List<EAW_Box_Office__c>.class);
  		system.debug('eawBoxOfficeList: '+System.JSON.serialize(eawBoxOfficeList));
        upsert eawBoxOfficeList;
	}
	
	@future 
	public static void upsertReleaseDates(String releaseDateListJson) {
  		system.debug('releaseDateListJson: '+releaseDateListJson);
  		List<EAW_Release_Date__c> releaseDateList = (List<EAW_Release_Date__c>) System.JSON.deserialize(releaseDateListJson,List<EAW_Release_Date__c>.class);
  		system.debug('releaseDateList: '+System.JSON.serialize(releaseDateList));
        upsert releaseDateList;
	}
    
}