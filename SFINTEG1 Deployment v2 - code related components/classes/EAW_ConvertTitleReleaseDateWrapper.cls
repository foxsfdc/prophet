public class EAW_ConvertTitleReleaseDateWrapper  {

    public class GetTitleReleaseDateWrapper {
        public List<Data> data;
        public Links_Z links;
    }
    public class Data {
    
        public String type_Z;
        public String id;
        public Relationships relationships;
        public Attributes attributes;
    
        
    }
    
    public class Relationships {
    
        public Title title;
    }
    
    public class Links_Z {
    
        public String self;
    
    }
    
    public class Attributes {
    
        public String type_Z;
        public String foxId;
        public String titleReleaseId;
        public String rowIdObject;
        public String foxVersionId;
        public String countryDescription;
        public String countryCode;
        public String languageDescription;
        public String languageCode;
        public String mediaCode;
        public String mediaDescription;
        public String releaseTypeCode;
        public String releaseTypeDescription;
        public String tuneInTime;
        public String tuneOutTime;
        public String airTimeCode;
        public String airTimeDescription;
        public String actualRuntime;
        public String releaseDate;
        public String releaseStatusCode;
        public String distributionCompanyCode;
        public String distributionCompanyDescription;
        public String releaseDescription;
        public String jdeVaultId;
        public String releaseStatusDescription;
        public String networkCode;
        public String networkDescription;
        public String runNumber;
        public String isFirstRelease;
        public String releaseNote;
        public String titleVersionTypeCode;
        public String titleVersionTypeDescription;
        public String titleVersionDescription;
    
    }
    
    public class Links {
        public String related;
    }
    
    public class Title {
        public Links links;
    }
        
    
}