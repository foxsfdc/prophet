@isTest
public class EAW_RulesTrigger_Test {
    @isTest static void handleRuleRecordDelete_Test(){
    
        Id releaseDateRuleRecordTypeId = Schema.SObjectType.EAW_Rule__c.getRecordTypeInfosByName().get('Release Date Calculation').getRecordTypeId();
        
        List<EAW_Customer__c> customerList =  EAW_TestDataFactory.createEAWCustomer(1,true); 
        
        List<EAW_Release_Date_Type__c> releaseDateTypeList = EAW_TestDataFactory.createReleaseDateType(1, true);
        
        //LRCC-1560 - Replace Customer lookup field with Customer text field
        //EAW_Release_Date_Guideline__c releaseDateGuideLineList= new EAW_Release_Date_Guideline__c( Product_Type__c = 'Episode', Territory__c = 'Aruba', Language__c = 'English', Customer__c = customerList[0].Id, EAW_Release_Date_Type__c = 'HV-Retail');
        EAW_Release_Date_Guideline__c releaseDateGuideLineList= new EAW_Release_Date_Guideline__c( Product_Type__c = 'Episode', Territory__c = 'Aruba', Language__c = 'English', Customers__c = customerList[0].name, EAW_Release_Date_Type__c = 'HV-Retail');
        insert releaseDateGuideLineList;
        
        List<EAW_Rule__c> ruleList = EAW_TestDataFactory.createEAWRule(1, true);
        ruleList[0].RecordTypeId = releaseDateRuleRecordTypeId;
        ruleList[0].Release_Date_Guideline__c = releaseDateGuideLineList.Id;
        update ruleList;
        
        List<EAW_Release_Date__c> releaseDateList = EAW_TestDataFactory.createReleaseDate(1, true);
        releaseDateList[0].Release_Date_Guideline__c = releaseDateGuideLineList.Id;
        update releaseDateGuideLineList;
        
        delete ruleList;
        
        List<EAW_Release_Date__c> obtainedReleaseDateList = [SELECT Id, Name FROM EAW_Release_Date__c LIMIT 1];
        System.assertEquals(false, obtainedReleaseDateList == null);
    }
}