@isTest
public class EAW_ReleaseDateTypeController_Test {    
    
    @isTest static void testEAWReleaseDateGuideLineMethod(){
        
        List<EAW_Release_Date_Type__c> releaseDateType = EAW_ReleaseDateTypeController.getAllTypes();
        
        System.assertEquals(true, releaseDateType != null);
    }  
}