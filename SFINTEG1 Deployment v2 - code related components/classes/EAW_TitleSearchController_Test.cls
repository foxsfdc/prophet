@isTest
public with sharing class EAW_TitleSearchController_Test {
    @isTest
    static void method1(){
         EDM_REF_DIVISION__c div = new EDM_REF_DIVISION__c(DIV_CD__c = '12345',Name='Test1');
        insert div;
        EDM_REF_PRODUCT_TYPE__c ptype = new EDM_REF_PRODUCT_TYPE__c(PROD_TYP_CD__c ='123454',Name='Test1');
        insert ptype;
        //1250-Replace Title EDM with Title Attribute
        //List< EDM_GLOBAL_TITLE__c > edmTitleList = EAW_TestDataFactory.createEDMTitle(1, True);
        //EAW_Title__c eawTitle = new EAW_Title__c(Title_EDM__c = edmTitleList[0].Id,Name='Test1');
        //insert eawTitle;
        List<EAW_Title__c> eawTitles = EAW_TestDataFactory.createEAWTitle(2, false);
        eawTitles[0].Name = 'Test:Test title1'; 
        eawTitles[1].Name = 'Test:Test title2';      
        insert eawTitles;

        EAW_Tag__c tag = new EAW_Tag__c(Name='Test',Active__c = true,Tag_Type__c='Title Tag');
        insert tag;
        EAW_Title_Tag_Junction__c j1 = new EAW_Title_Tag_Junction__c(/*Title__c = edmTitleList[0].Id,*/Title_Attribute__c=eawTitles[0].Id,Tag__c = tag.Id);
        EAW_Title_Tag_Junction__c j2 = new EAW_Title_Tag_Junction__c(/*Title__c = edmTitleList[0].Id,*/Title_Attribute__c=eawTitles[0].Id,Tag__c = tag.Id);
        EAW_Title_Tag_Junction__c j3 = new EAW_Title_Tag_Junction__c(/*Title__c = edmTitleList[0].Id,*/Title_Attribute__c=eawTitles[0].Id,Tag__c = tag.Id);
        EAW_Title_Tag_Junction__c j4 = new EAW_Title_Tag_Junction__c(/*Title__c = edmTitleList[0].Id,*/Title_Attribute__c=eawTitles[1].Id,Tag__c = tag.Id);
        List<EAW_Title_Tag_Junction__c> EAWTitleTagJunctionList = new List<EAW_Title_Tag_Junction__c>{j1,j2,j3,j4};
        insert EAWTitleTagJunctionList;
       
        EAW_TitleSearchController.getFields(new Map<String,String>{'EAW_Release_Date__c'=>'Search_Result_Fields'});
        EAW_TitleSearchController.searchForEAWBoxOffices('\'' + eawTitles[0].Id + '\'');
        List<EAW_Title__c> GlobalTitleList2 = EAW_TitleSearchController.getTitleAttributes(eawTitles[0].Id + ' ' + eawTitles[1].Id);
        system.assertEquals(2,GlobalTitleList2.size());
        List<EAW_Title__c> GlobalTitleList = EAW_TitleSearchController.searchForPastedTitles(String.valueOf(eawTitles[0].Name));
        system.assertEquals(1, GlobalTitleList.size());
        List<EAW_Title__c> GlobalTitleList1 = EAW_TitleSearchController.searchForAdvancedTitles(new List<String>{'Test:Test title1'},new List<String>{'Test:Test title1'},'1234567','2009-10-02','2019-10-02',new List<String>{'Test1'},'0','-1',new List<String>{ptype.Id},new List<String>{'Test1;test2;'},new List<String>{tag.Name},'Test1','test2','Test talent');
        system.assertEquals(0,GlobalTitleList1.size());
    }
    @isTest
    static void failureTestCase(){
        
        EAW_TitleSearchController.getTitleAttributes('ab,cd');
       
    }    
}