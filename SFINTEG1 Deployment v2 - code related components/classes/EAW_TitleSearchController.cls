public without sharing class EAW_TitleSearchController {
    
    @AuraEnabled
    public static List<EAW_Title__c> getTitleAttributes(String titleIdsString) {
        
        //Objects to be returned
        List<EAW_Title__c > results = new List<EAW_Title__c >();
        
        List<String> titleIds = new List<String>();
        List<EAW_Title_Tag_Junction__c> titleTags = new List<EAW_Title_Tag_Junction__c>();
        Map<String, String> titleIdToTagMap = new Map<String, String>();
        
        //Parse titleIdsString into list of title Ids
        titleIds = titleIdsString.split(' ');
        //LRCC-1098 query condition changed from EDM to EAW title
        String query = 'SELECT Id,' +
                       'Name, ' +
                       'Name__c,'+  // LRCC - 1870
                       //1095 & 1250-Replace Title EDM with Title Attribute
                       'FIN_PROD_ID__c, ' + 
                       'PROD_TYP_CD_INTL__c,' +
                       'PROD_TYP_CD_INTL__r.Name, ' + 
                       'PROD_TYP_CD__r.Name, ' +
                       'FIN_DIV_CD__r.Name, ' + 
                       'First_Release_Date__c ' + 
                       'FROM EAW_Title__c WHERE Id IN :titleIds';
        
       
        try {
            results = Database.query(query);
            system.debug('results:::'+results);
        } catch(Exception e) {
            System.debug('Exception caught: ' + e);
        }
        
        try {
            //Search for tags associated with titles
            String titleTagsQuery = 'SELECT Tag__r.Name, Title_Attribute__r.Id FROM EAW_Title_Tag_Junction__c WHERE Title_Attribute__r.Id IN :titleIds';
            titleTags = database.query(titleTagsQuery);
        } catch(Exception e) {}
        
        //Build the Tag Strings
        for(EAW_Title_Tag_Junction__c titleTag: titleTags) {
            
            //One title can have many tags, if thats the case just build out the string
            if(titleIdToTagMap.containsKey(titleTag.Title_Attribute__r.Id)) {
                String existingTags = titleIdToTagMap.get(titleTag.Title_Attribute__r.Id);
                titleIdToTagMap.put(titleTag.Title_Attribute__r.Id, existingTags + ', ' + titleTag.Tag__r.Name);
            } else {
                titleIdToTagMap.put(titleTag.Title_Attribute__r.Id, titleTag.Tag__r.Name);
            }
        }
        
        // Attach the tag names to the Title Obj
        for(EAW_Title__c result : results){
            
            // we do some weird stuff here
            // we shove in the Tag Names into an arbitrary column that isn't being used
            // then on the aura JS side we know where to look for it and re-map the title accordingly
            if(titleIdToTagMap.keySet().contains(result.Id) /*&& result.Title_EDM__c != null*/){
                //store TITLE TAG in DIRECTOR NAME field where titleTag.Title__r.Id == result.Id
                result.DIR_NM__c = titleIdToTagMap.get(result.Id);
            }
        }
        return results;
    }
    
    /**
     * Take into param a String which is a list of title names or title Ids
     * Return a list of Titles
     *
     *   -- Method logic
     *      1. parse && split the string
     *      2. build query param --> where titleId IN (...) or name LIKE, and run query
     *      3. find tags && attach them to the object
     *      4. return our results
     *
     *      **NOTE** TitleId that users actually use is actually EDM_Global_Title__c.fin_prod_id__c
     */
    @AuraEnabled
    public static List<EAW_Title__c> searchForPastedTitles(String pastedTitles) {
        
        // List of titles to return
        List<EAW_Title__c > results = new List<EAW_Title__c>();
        List<String> resultIds = new List<String>(); 
        
        // Data structures used to grab tag information
        List<EAW_Title_Tag_Junction__c> titleTags = new List<EAW_Title_Tag_Junction__c>();
        Map<String, String> titleTagMap = new Map<String, String>();
        
        // Parse the input, don't search if its empty or blank
        String[] titleList = pastedTitles.split('\n');
        if(pastedTitles.replace('\n','') != '' && pastedTitles.length() != 0 && titleList.size() > 0) {
            //LRCC-1617
            // Build the Search Str for the array
            String titleNameSearchStr = '';
            String titleIdSearchStr = '';
            //LRCC-1617
            //LRCC-1629
            /*
            for(Integer i = 0; i < titleList.size(); i++) {

                // Add in OR if you're not the first
                titleNameSearchStr += i == 0 ? ('Name IN :titleList') : (' OR Name IN :titleList');
                //Add in a comma if you're not the first
                //titleIdSearchStr += i == 0 ? ('\'' + titleList[i] + '\'') : (',\'' + titleList[i] + '\'');
            }
            */  
            if(titleList.size() > 0) {
                titleNameSearchStr += 'Name__c IN :titleList OR FIN_PROD_ID__c IN :titleList';  // LRCC - 1870
            }
            String titleWhereClause = titleNameSearchStr ;

            // Search the DB for titles
            try {
                String queryFields = 'id,'
                                    + 'fin_prod_id__c,'
                                   + 'name,'
                                   + 'Name__c,'
                                   + 'prod_typ_cd__r.name,'
                                   + 'PROD_TYP_CD_INTL__r.name,'
                                   + 'fin_div_cd__r.name,'
                                   + 'First_Release_Date__c';
                                   
                String query = 'select ' + queryFields + ' from EAW_Title__c where ' + titleWhereClause;
                system.debug('<<--query ->>>'+query );
                results = Database.query(query);

                // Search for Tags associated with the titles
                for(EAW_Title__c result : results) {
                    resultIds.add(result.Id);
                }
                String titleTagsQuery = 'SELECT Tag__r.Name, Title_Attribute__r.Id FROM EAW_Title_Tag_Junction__c WHERE Title_Attribute__r.Id IN :resultIds';
                titleTags = database.query(titleTagsQuery);

                // Build the Tag Strings
                for(EAW_Title_Tag_Junction__c titleTag: titleTags) {
            
                    //One title can have many tags, if thats the case just build out the string
                    if(titleTagMap.containsKey(titleTag.Title_Attribute__r.Id)) {
                        String existingTags = titleTagMap.get(titleTag.Title_Attribute__r.Id);
                        titleTagMap.put(titleTag.Title_Attribute__r.Id, existingTags + ', ' + titleTag.Tag__r.Name);
                    } else {
                        titleTagMap.put(titleTag.Title_Attribute__r.Id, titleTag.Tag__r.Name);
                    }
                }

                // Attach the tag names to the Title Obj
                for(EAW_Title__c result : results) {
                    
                    // we do some weird stuff here
                    // we shove in the Tag Names into an arbitrary column that isn't being used
                    // then on the aura JS side we know where to look for it and re-map the title accordingly
                    if(titleTagMap.keySet().contains(result.Id)){
                        //store TITLE TAG in DIRECTOR NAME field where titleTag.Title__r.Id == result.Id
                        result.DIR_NM__c = titleTagMap.get(result.Id);
                    }
                }
                
            } catch(Exception e) {}
        }
        
        return results;
    }
    
    @AuraEnabled
    public static List<EAW_Title__c> searchForAdvancedTitles(List<String> titleName, List<String> selectedDivisions, String finlTitleId, String initialReleaseDateFrom,
                                                             String initialReleaseDateTo, List<String> selectedIntlProductTypes, String usBoxOfficeFrom, String usBoxOfficeTo,
                                                             List<String> selectedProductTypes, List<String> genre, List<String> selectedTitleTags,  String directorName, String franchise, String talent) {
         
        List<EAW_Title__c> results = new List<EAW_Title__c>();
        List<String> resultIds = new List<String>(); 

        List<EAW_Title_Tag_Junction__c> titleTags = new List<EAW_Title_Tag_Junction__c>();
        List<String> titleIds = new List<String>();
        Map<String, String> titleIdToTagMap = new Map<String, String>();
        String whereClause = '';
        //LRCC - 1617
        Set<String> titleNameSet = new Set<String>();
        Boolean hasChanged = false;
        String replaceTitle;
        
        //LRCC - 1617        
        if(titleName !=  null && !titleName.isEmpty()) {
            if(hasChanged) {
                whereClause += ' AND ';
            }
            for(Integer i =0; i<titleName.size(); i++) {
                if(titleName[i].contains(':')) {     
                   // String[] titleNameString = titleName[i].split(': ');             
                    //for(String titlenameun : titleNameString ) {
                        //replaceTitle = titlenameun.replace(',' , '');                                          
                   // }
                   if(titleName[i].substringAfter(':')!=null){
                        titleNameSet.add(titleName[i].substringAfter(':').trim());
                   }                                          
                }
                //titleNameSet.add(replaceTitle);  
            }
            
            if(titleNameSet.isEmpty() == False) {
                whereClause += ' Name__c IN :titleNameSet'; // LRCC - 1870
            } 
            hasChanged= true;
        }        
        //1095 & 1250-Replace Title EDM with Title Attribute
         if(String.isNotBlank(directorName)){
            if(hasChanged){
                whereClause += ' AND ';
            }
            whereClause += ' DIR_NM__c LIKE \'%'+ directorName +'%\'';
            hasChanged= true;
        }
                
        if(String.isNotBlank(franchise)){
            if(hasChanged){
                whereClause += ' AND ';
            }
            whereClause += ' Franchise__c LIKE \'%'+ franchise +'%\'';
            hasChanged= true;
        }
        
        //LRCC-1884
        /*
        if(String.isNotBlank(talent)){
            if(hasChanged){
                whereClause += ' AND ';
            }
            whereClause += ' Title_EDM__r.Talent__c LIKE \'%'+ talent +'%\'';
            hasChanged= true;
        }
        */
                    
        if(selectedDivisions.size() > 0) {
            if(hasChanged) {
               whereClause += ' AND ';
            }
            whereClause += ' FIN_DIV_CD__r.Name IN :selectedDivisions';
            hasChanged= true;
        }
        
        if(String.isNotBlank(finlTitleId)) {
            if(hasChanged) {
                whereClause += ' AND ';
            }
            whereClause += ' FIN_PROD_ID__c = \''+ finlTitleId +'\'';
            hasChanged= true;
        }

        if(String.isNotEmpty(initialReleaseDateFrom)) {
            if(hasChanged){
                whereClause += ' AND ';
            }
            whereClause += ' First_Release_Date__c >= ' + initialReleaseDateFrom;
            hasChanged = true;
        }

        if(String.isNotEmpty(initialReleaseDateTo)) {
            if(hasChanged){
                whereClause += ' AND ';
            }
            whereClause += ' First_Release_Date__c < ' + initialReleaseDateTo;
            hasChanged= true;
        }

        if(String.isNotBlank(usBoxOfficeFrom)) {
            if(hasChanged){
                whereClause += ' AND ';
            }
            whereClause += ' US_Box_Office__c >= ' + usBoxOfficeFrom; //Total_US_Box_Office__c replaced with US_Box_Office__c 
            hasChanged= true;
        }
        
         if(String.isNotBlank(usBoxOfficeTo)) {
            if(hasChanged){
                whereClause += ' AND ';
            }
            whereClause += ' US_Box_Office__c < ' + usBoxOfficeTo; //Total_US_Box_Office__c replaced with US_Box_Office__c 
            hasChanged= true;
        }
        
        if(selectedIntlProductTypes.size() > 0) {
            if(hasChanged) {
               whereClause += ' AND ';
            }
            whereClause += ' PROD_TYP_CD_INTL__r.Name IN :selectedIntlProductTypes'; //(' + getFormattedList(intlProductTypeList) + ')';
            hasChanged = true;
        }
        
        if(selectedProductTypes.size() > 0) {
            if(hasChanged) {
                whereClause += ' AND ';
            }
            whereClause += ' PROD_TYP_CD__r.Name IN :selectedProductTypes';
            hasChanged = true;
        }
        
        if(genre != null && genre.size() >0 ) {
            
            //LRCC-1884
            if(hasChanged){ 
                whereClause += ' AND ';
            }
            
            //LRCC-1095 field is changed from EDM to EAW
            whereClause += 'Genre__c INCLUDES (';
            for (Integer i = 0; i < genre.size(); i++) {
                String name = genre.get(i);
                whereClause += '\'' + name + '\'';
                if (genre.size() != (i + 1)) {
                    whereClause += ',';
                }
            }
            whereClause += ')';   
            hasChanged = true; //LRCC-1884
        }
        
        //This one is a little different
        if(selectedTitleTags.size() > 0) {
            
            if(hasChanged) {
                whereClause += ' AND ';
            }
            //Find the Title Ids associated with these tags && add the filter - Keep the Tag Name, so we can attach them to the objects we return later 
            String titleIdQuery = 'SELECT Title__c, Tag__r.Name, Title_Attribute__c FROM EAW_Title_Tag_Junction__c WHERE Tag__r.Name IN :selectedTitleTags';
            
            
            try {
                titleTags = Database.query(titleIdQuery);
            } catch(Exception e) {} 
            
            
            //1095 changes made from EDM to EAW title    
            for(EAW_Title_Tag_Junction__c titleTag : titleTags) {
                titleIds.add(titleTag.Title_Attribute__c);
                titleIdToTagMap.put(titleTag.Title_Attribute__c, titleTag.Tag__r.Name);
            }
            whereClause += 'Id in :titleIds';
        }
        
        //LRCC-1884 some of the below fields are commented
        
        String titleSearchFields = 'Id,'
                                   // + 'FIN_DIV_CD__c,' 
                                   + 'FIN_DIV_CD__r.Name,'
                                   + 'name,'
                                   // + 'Name__c,' 
                                   // + 'First_Release_Date_Fallback__c,'                                  
                                   // + 'US_Admissions__c,'//; 
                                   // + 'Genre__c,' //TODO: add genre later 
                                 //LRCC-1098
                                 //1095 & 1250-Replace Title EDM with Title Attribute
                                    +'FIN_PROD_ID__c, ' 
                                    +'PROD_TYP_CD_INTL__r.Name, '
                                    // +'PROD_TYP_CD__r.Name, '  
                                  //  +'Title_EDM__r.PROD_TYP_CD__r.Name, ' 
                                  //  +'FIN_DIV_CD__r.Name, ' 
                                    // +'Title_EDM__r.Talent__c,' 
                                    +'First_Release_Date__c ';
        
        //LRCC-1884
        if(String.isNotBlank(talent)){  
            titleSearchFields += ',Talent_Name__c';
        }
                                    
        try {
            if(String.isNotBlank(whereClause)) { 
                String query = 'SELECT ' + titleSearchFields + ' FROM EAW_Title__c WHERE ' + whereClause + ' ORDER BY Name ASC';
                
                //LRCC-1884
                if(String.isNotBlank(talent)){  
                
                    query += ' Limit 40000 ';
                    List<EAW_Title__c> filteredTitles = new List<EAW_Title__c>();
                    
                    for(EAW_Title__c result : Database.query(query)) {                        
                       if(result.Talent_Name__c == talent) {
                           filteredTitles.add(result);
                       } 
                    }                    
                    results = filteredTitles; 
                    
                } else {
                
                    query += ' Limit 10000 ';
                    results = Database.query(query);
                }
        
                system.debug('query:::'+query);                
                system.debug('results1: '+results.size());
                system.debug('heap;;;'+limits.getHeapSize());
            }
        } catch(Exception e) {}
            system.debug('heap;;;'+limits.getHeapSize());
        //Add all result Ids to separate list
        for(EAW_Title__c result : results) {
            resultIds.add(result.Id);
        }
                                        system.debug('heap;;;'+limits.getHeapSize());
        //Populate Tags based off of WHERE clause
        try {
        
        //LRCC-1884 Title__c is removed from the below query
        
            String titleTagsQuery = 'SELECT Tag__r.Name, Title_Attribute__c FROM EAW_Title_Tag_Junction__c WHERE Title_Attribute__c IN :resultIds';
            titleTags = database.query(titleTagsQuery);
        } catch(Exception e) {}
                                system.debug('heap;;;'+limits.getHeapSize());
        //Build the Tag Strings
        for(EAW_Title_Tag_Junction__c titleTag: titleTags) {
            
            //One title can have many tags, if thats the case just build out the string
            if(titleIdToTagMap.containsKey(titleTag.Title_Attribute__c)) {
                String existingTags = titleIdToTagMap.get(titleTag.Title_Attribute__c);
                titleIdToTagMap.put(titleTag.Title_Attribute__c, existingTags + ', ' + titleTag.Tag__r.Name);
            } else {
                titleIdToTagMap.put(titleTag.Title_Attribute__c, titleTag.Tag__r.Name);
            }
        }
                        system.debug('heap;;;'+limits.getHeapSize());
        //Attach the tag names to the Title Obj
        for(EAW_Title__c result : results) {
            //we do some weird stuff here
            //we shove in the Tag Names into an arbitrary column that isn't being used
            //then on the aura JS side we know where to look for it and re-map the title accordingly
            if(titleIdToTagMap.keySet().contains(result.Id) /*&& result.Title_EDM__c != null*/){
                //store TITLE TAG in DIRECTOR NAME field where titleTag.Title__r.Id == result.Id
                result.DIR_NM__c = titleIdToTagMap.get(result.Id);
            }
        }
        system.debug('heap;;;'+limits.getHeapSize());
        //system.debug('final results: '+results);
        return results;
    }
    /*
    private static String getFormattedList(String itemList) {
    
        if(itemList.contains(';')) {
            String[] items = itemList.split(';');
            itemList = '';
            
            for(Integer i = 0; i < items.size(); i++) {
                itemList += '\'' + items[i] + '\'';
                if(i != items.size() - 1) {
                    itemList += ',';
                }
            }
        } else {
            itemList = '\'' + itemList + '\'';    
        }
        return itemList;
    }

    @AuraEnabled
    public static List<Object> searchForTVDBoxOffices(String titleIdsString) {
        
        List<Object> fieldValues = new List<Object>();
        
        String queryCondition = ' WHERE TVD_Title__c IN ('+ titleIdsString +')';
        queryCondition += ' ORDER BY Name ASC';
        
        fieldValues.add(EAW_FieldSetController.getRecords('TVD_Box_Office__c', 'Territory_Attributes_Table_Fields', queryCondition, null, null));
        
        return fieldValues;
    }
    */
    @AuraEnabled
    public static List<Object> searchForEAWBoxOffices(String titleIdsString) {
        
        List<Object> fieldValues = new List<Object>();
        
        String queryCondition = ' WHERE Title_Attribute__c IN ('+ titleIdsString +')';
        queryCondition += ' ORDER BY Name ASC';
        
        fieldValues.add(EAW_FieldSetController.getRecords('EAW_Box_Office__c', 'Box_Office_Table', queryCondition, null, null));
        
        return fieldValues;
    }
    
    @AuraEnabled
    public static List<Object> getFields(Map<String,String> objFieldSetMap) {
        
        List<Object>fieldNames = new List<Object>();
        for(String strObjectName: objFieldSetMap.keySet()) {
            fieldNames.add(EAW_FieldSetController.getFields(strObjectName, objFieldSetMap.get(strObjectName)));
        }
        return fieldNames;
    }
}