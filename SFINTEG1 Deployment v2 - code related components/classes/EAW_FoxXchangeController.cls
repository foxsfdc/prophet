public class EAW_FoxXchangeController {
    @AuraEnabled
    public static List<AuditHistory> getAuditHistory(String releaseDateId) {
    	system.debug('EAW_FoxXchangeController releaseDateId: '+releaseDateId);
    	EAW_Release_Date__c erdc =[ select HEP_Promotion_Dating_Id__c from EAW_Release_Date__c where id=:releaseDateId];
    	List<HEP_Promotion_Date_API.AuditHistory> auditHistory = EAW_PromotionDateController.getAuditHistory(erdc.HEP_Promotion_Dating_Id__c);
    	system.debug('EAW_FoxXchangeController auditHistory: '+auditHistory);
		return (List<AuditHistory>)system.json.deserialize(system.JSON.serialize(auditHistory),List<AuditHistory>.class);
    }
    
    public class AuditHistory {
        @AuraEnabled
        public String createdBy{get; set;}
        @AuraEnabled
		public DateTime createdDate{get; set;}
        @AuraEnabled
		public String fieldName{get; set;}
        @AuraEnabled
		public String newValue{get; set;}
        @AuraEnabled
		public String oldValue{get; set;}
		
		// Constructor - no inputs
		public AuditHistory(){}
		
		// Constructor - all inputs
		public AuditHistory(
			String createdBy,
			DateTime createdDate,
			String fieldName,
			String newValue,
			String oldValue ) {
				
			this.createdBy = createdBy;
			this.createdDate = createdDate;
			this.fieldName = fieldName;
			this.newValue = newValue;
			this.oldValue = oldValue;
		}
    }
    
    
}