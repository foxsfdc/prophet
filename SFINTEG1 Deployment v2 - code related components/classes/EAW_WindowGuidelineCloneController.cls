public with sharing class EAW_WindowGuidelineCloneController {

    @AuraEnabled
    public static EAW_Window_Guideline__c getWindowGuideline(String recordId) {
        try {
            // Life cycle Field Changed
            //LRCC-1511 - Remove Lifecycle
            //EAW_Window_Guideline__c windowGuideline = [SELECT Id, EAW_Life_cycle__c, Clone_WG_Name__c, Name, Status__c, EAW_Territory__c, Window_Guideline_Alias__c, Product_Type__c, Window_Type__c, Description__c, Version__c, Version_Notes__c, Start_Date__c, End_Date__c, Outside_Date__c, Tracking_Date__c, Start_Date_Rule__c, End_Date_Rule__c, Outside_Date_Rule__c, Tracking_Date_Rule__c, Customers__c FROM EAW_Window_Guideline__c WHERE Id =: recordId];
            EAW_Window_Guideline__c windowGuideline = [SELECT Id, Clone_WG_Name__c, Name, Status__c, EAW_Territory__c, Window_Guideline_Alias__c, Product_Type__c, Window_Type__c, Description__c, Version__c, Version_Notes__c, Start_Date__c, End_Date__c, Outside_Date__c, Tracking_Date__c, Start_Date_Rule__c, End_Date_Rule__c, Outside_Date_Rule__c, Tracking_Date_Rule__c, Customers__c FROM EAW_Window_Guideline__c WHERE Id =: recordId];
            return windowGuideline;
        } catch (Exception e) {
            throw New AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static void cloneRuleAndRuleDetails(String oldWG, String newWG) {
        EAW_WindowGuidelineCloneController.cloneRuleAndRuleDetailUtil(oldWG, newWG);
    }
    
    public static void cloneRuleAndRuleDetailUtil(String oldWG, String newWG) {
        
        //Find strands from existing version
        String strandFields = 'End_Date_Days_Offset__c,'
                            + 'End_Date_Months_Offset__c,'
                            + 'End_Date_From__c,'
                            + 'Start_Date_Days_Offset__c,'
                            + 'Start_Date_Months_Offset__c,'
                            + 'Start_Date_From__c,'
                            + 'Media__c,'
                            + 'Territory__c,'
                            + 'Language__c,'
                            + 'License_Type__c';
        String strandQuery = 'SELECT ' + strandFields + ' FROM EAW_Window_Guideline_Strand__c where EAW_Window_Guideline__c =\'' + oldWG + '\'';
        List<EAW_Window_Guideline_Strand__c> oldStrands = Database.query(strandQuery);
    
        //Clone the strands & then save em
        List<EAW_Window_Guideline_Strand__c> clonedStrands = new List<EAW_Window_Guideline_Strand__c>();
        for(EAW_Window_Guideline_Strand__c strand : oldStrands) {

            //Clone it
            EAW_Window_Guideline_Strand__c clone = new EAW_Window_Guideline_Strand__c();
            clone.End_Date_Days_Offset__c = strand.End_Date_Days_Offset__c;
            clone.End_Date_Months_Offset__c = strand.End_Date_Months_Offset__c;
            clone.End_Date_From__c = strand.End_Date_From__c;
            clone.Start_Date_Days_Offset__c = strand.Start_Date_Days_Offset__c;
            clone.Start_Date_Months_Offset__c = strand.Start_Date_Months_Offset__c;
            clone.Start_Date_From__c = strand.Start_Date_From__c;
            clone.Media__c = strand.Media__c;
            clone.Territory__c = strand.Territory__c;
            clone.Language__c = strand.Language__c;
            clone.License_Type__c = strand.License_Type__c;
            
            //add new guideline as its parent
            clone.EAW_Window_Guideline__c = newWG;

            //add to list
            clonedStrands.add(clone);
        }
        
        //actually save the strands now
        insert clonedStrands;
        
        Map < String, Schema.SObjectField > ruleFieldMap = Schema.SObjectType.EAW_Rule__c.Fields.getMap();

        String ruleQueryString = 'SELECT';
        for (Schema.SObjectField fieldName: ruleFieldMap.values()) {
            String fieldAPI = String.valueOf(fieldName);
            ruleQueryString = ruleQueryString + ' ' + fieldAPI + ' ,';
        }
        ruleQueryString = ruleQueryString.removeEnd(',');
        ruleQueryString += ' FROM EAW_Rule__c WHERE Window_Guideline__c = :oldWG ';
        System.debug('ruleQueryString:::::' + ruleQueryString);

        List < EAW_Rule__c > rules = Database.query(ruleQueryString);
        System.debug('Rules:::::' + rules);

        if (rules != null && rules.size() > 0) {

            List < EAW_Rule__c > clonedRules = new List < EAW_Rule__c > ();
            Map < Id, List < EAW_Rule__c >> ruleIdClonedRules = new Map < Id, List < EAW_Rule__c >> ();

            for (EAW_Rule__c r: rules) {

                if (String.isNotBlank(newWG)) {

                    EAW_Rule__c rule = r.clone(false, true, false, false);
                    rule.Window_Guideline__c = newWG;

                    if (!ruleIdClonedRules.containsKey(r.Id)) {
                        ruleIdClonedRules.put(r.Id, new List < EAW_Rule__c > ());
                    }
                    ruleIdClonedRules.get(r.Id).add(rule);
                    clonedRules.add(rule);
                }
            }
            System.debug('clonedRules:::::: ' + clonedRules);

            if (clonedRules != null && clonedRules.size() > 0) {

                insert clonedRules;
                System.debug('ruleIdClonedRules::::: ' + ruleIdClonedRules);

                Map < String, Schema.SObjectField > ruleDetailFieldMap = Schema.SObjectType.EAW_Rule_Detail__c.Fields.getMap();
                Set < Id > ruleIds = ruleIdClonedRules.keySet();

                String ruleDetailQueryString = 'SELECT ';
                for (Schema.SObjectField fieldName: ruleDetailFieldMap.values()) {
                    String fieldAPI = String.valueOf(fieldName);
                    ruleDetailQueryString = ruleDetailQueryString + ' ' + fieldAPI + ' ,';
                }
                ruleDetailQueryString = ruleDetailQueryString.removeEnd(',');
                ruleDetailQueryString += 'FROM EAW_Rule_Detail__c WHERE Rule_No__c IN :ruleIds ';
                System.debug('ruleDetailQueryString::::' + ruleDetailQueryString);

                List < EAW_Rule_Detail__c > ruleDetails = Database.query(ruleDetailQueryString);
                System.debug('ruleDetails ::::' + ruleDetails);

                if (ruleDetails != null && ruleDetails.size() > 0) {

                    List < EAW_Rule_Detail__c > clonedRuleDetails = new List < EAW_Rule_Detail__c > ();
                    Map < Id, List < EAW_Rule_Detail__c >> ruleDetailIdClonedRuleDetails = new Map < Id, List < EAW_Rule_Detail__c >> ();
                    Map < Id, Id > ruleDetailIdParentRDId = new Map < Id, Id > ();

                    for (EAW_Rule_Detail__c rd: ruleDetails) {

                        if (rd.Rule_No__c != null && ruleIdClonedRules.get(rd.Rule_No__c) != null) {

                            for (EAW_Rule__c r: ruleIdClonedRules.get(rd.Rule_No__c)) {

                                EAW_Rule_Detail__c ruleDetail = rd.clone(false, true, false, false);
                                ruleDetail.Rule_No__c = r.Id;
                                ruleDetail.WindowName__c = r.Window__c;

                                if (!ruleDetailIdClonedRuleDetails.containsKey(rd.Id)) {
                                    ruleDetailIdClonedRuleDetails.put(rd.Id, new List < EAW_Rule_Detail__c > ());
                                }
                                ruleDetailIdClonedRuleDetails.get(rd.Id).add(ruleDetail);
                                clonedRuleDetails.add(ruleDetail);
                            }
                        }

                        if (rd.Parent_Rule_Detail__c != null) {
                            ruleDetailIdParentRDId.put(rd.Id, rd.Parent_Rule_Detail__c);
                        }
                    }
                    System.debug('clonedRuleDetails::::: ' + clonedRuleDetails);
                    System.debug('ruleDetailIdParentRDId::::: ' + ruleDetailIdParentRDId);

                    if (clonedRuleDetails != null && clonedRuleDetails.size() > 0) {

                        insert clonedRuleDetails;
                        System.debug('ruleDetailIdClonedRuleDetails::::: ' + ruleDetailIdClonedRuleDetails);

                        List < EAW_Rule_Detail__c > updatedRuleDetails = new List < EAW_Rule_Detail__c > ();

                        for (Id rdId: ruleDetailIdClonedRuleDetails.keySet()) {

                            if (ruleDetailIdParentRDId.containsKey(rdId)) {

                                for (Integer i = 0; i < ruleDetailIdClonedRuleDetails.get(rdId).size(); i++) {

                                    if (ruleDetailIdClonedRuleDetails.get(rdId)[i].Parent_Rule_Detail__c != null) {

                                        ruleDetailIdClonedRuleDetails.get(rdId)[i].Parent_Rule_Detail__c =
                                            ruleDetailIdClonedRuleDetails.get(ruleDetailIdParentRDId.get(rdId))[i].Id;
                                    }
                                }
                                updatedRuleDetails.addAll(ruleDetailIdClonedRuleDetails.get(rdId));
                            }
                        }

                        if (updatedRuleDetails != null && updatedRuleDetails.size() > 0) {
                            update updatedRuleDetails;
                            System.debug('updatedRuleDetails::::::' + updatedRuleDetails);
                        }
                    }
                }
                checkForProductTypeChange(oldWG,newWG); // LRCC - 1624
            }
        }
    }   
    public static void checkForProductTypeChange(String oldWg, String newWg){
        
        EAW_Window_Guideline__c oldGuideline = [SELECT Id,Product_Type__c FROM EAW_Window_Guideline__c WHERE Id = :oldWg];
        EAW_Window_Guideline__c newGuideline = [SELECT Id,Product_Type__c,Start_Date_Rule__c,End_Date_Rule__c,Outside_Date_Rule__c,Tracking_Date_Rule__c FROM EAW_Window_Guideline__c WHERE Id = :newWg];
        
        Set<Id> modifiedWgIds = new Set<ID>();
        Map<Id,String> oldProductTypeMap = new Map<Id,String>();
        Map<Id,String> newProductTypeMap = new Map<Id,String>();
        Map<Id,List<String>> oldConditionalNameMap = new Map<Id,List<String>>();
        Map<Id,String> clonedGuidelineMap = new Map<Id,String>();
        
        if(oldGuideline != null && newGuideline != null && oldGuideline.Product_Type__c != null && newGuideline.Product_Type__c != null && oldGuideline.Product_Type__c != newGuideline.Product_Type__c){
            modifiedWgIds.add(newGuideline.Id);
            oldProductTypeMap.put(newGuideline.Id,oldGuideline.Product_Type__c);
            oldProductTypeMap.put(oldGuideline.Id,oldGuideline.Product_Type__c);
            newProductTypeMap.put(newGuideline.Id,newGuideline.Product_Type__c);
            clonedGuidelineMap.put(oldGuideline.Id,String.valueOf(newGuideline.Id));
        }
        if(modifiedWgIds.size() > 0){
            for(EAW_Rule__c rule :  [SELECT Id,Condition_Operand_Details__c,Conditional_Operand_Id__c,Window_Guideline__c,
                                      (SELECT Id,Condition_Operand_Details__c,Conditional_Operand_Id__c FROM Rule_Orders__r)
                                     FROM EAW_Rule__c WHERE Window_Guideline__c IN :modifiedWgIds]){
            	List<String> conditionalNameList = new List<String>();
                if(oldConditionalNameMap.containsKey(rule.Window_Guideline__c)){
                    conditionalNameList = oldConditionalNameMap.get(rule.Window_Guideline__c);                  
                }  
                if(rule.Condition_Operand_Details__c != null){
                	conditionalNameList.addAll(String.valueOf(rule.Condition_Operand_Details__c).split(';'));
                }
                if(rule.Rule_Orders__r != null && rule.Rule_Orders__r.size() > 0){
                	for(EAW_Rule_Detail__c ruleDetail : rule.Rule_Orders__r){
                        if(ruleDetail.Condition_Operand_Details__c != null){
                            conditionalNameList.addAll(String.valueOf(ruleDetail.Condition_Operand_Details__c).split(';'));
                        }
                    }
                }
                oldConditionalNameMap.put(rule.Window_Guideline__c,conditionalNameList);                       
            }
            if(oldConditionalNameMap != null && oldConditionalNameMap.keySet().size() > 0){
                system.debug(':::::oldConditionalNameMap::::::'+oldConditionalNameMap);
                Map<String,String> allRulesUpdateMap = EAW_GuidelineProductTypeChangeHandler.updateRDGRulesBasedOnProductType(oldConditionalNameMap,oldProductTypeMap,newProductTypeMap,false,clonedGuidelineMap);
                if(allRulesUpdateMap != null && allRulesUpdateMap.keySet().size() > 0){
                    if(allRulesUpdateMap.containsKey(String.valueOf(newGuideline.Id) + '-SD')){
                        newGuideline.Start_Date_Rule__c = allRulesUpdateMap.get(String.valueOf(newGuideline.Id) + '-SD');
                    } 
                    if(allRulesUpdateMap.containsKey(String.valueOf(newGuideline.Id) + '-ED')){
                        newGuideline.End_Date_Rule__c = allRulesUpdateMap.get(String.valueOf(newGuideline.Id) + '-ED');
                    } 
                    if(allRulesUpdateMap.containsKey(String.valueOf(newGuideline.Id) + '-OD')){
                        newGuideline.Outside_Date_Rule__c = allRulesUpdateMap.get(String.valueOf(newGuideline.Id) + '-OD');
                    } 
                    if(allRulesUpdateMap.containsKey(String.valueOf(newGuideline.Id) + '-TD')){
                        newGuideline.Tracking_Date_Rule__c = allRulesUpdateMap.get(String.valueOf(newGuideline.Id) + '-TD');
                    }
                }
                update newGuideline;
            }
        }
    }
}