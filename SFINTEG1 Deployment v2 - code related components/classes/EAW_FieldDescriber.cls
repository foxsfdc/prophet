public class EAW_FieldDescriber {
    
    public static string getSessionId() {
    
        PageReference ref = Page.sessionIdPage;
        String vfContent = '';
        if(!Test.isRunningTest()) {
            vfContent = ref.getContent().toString();
        } else {
            vfContent = 'Start_Of_Session_Id Session_Id End_Of_Session_Id';
        }
            
        String sessionFromVF = vfContent.substringBetween('Start_Of_Session_Id', 'End_Of_Session_Id');
        return sessionFromVF;
    }
    
    public static EAW_FieldDescriber.dependentPicklistValuesWrapper getDependentPicklistValues(String recordTypeId, String objectName, String dependentFieldName) {
        
        //Declarations
        Map<String, String> controllingKeyMapValues = new Map<String,String> ();
        Map<String, List<String>> returnValues = new Map<String, List<String>> ();
        
        dependentPicklistValuesWrapper dependentPicklistValueWrapper = new dependentPicklistValuesWrapper();
        
        List<picklistValuesWrapper> controllingPicklistValues = new List<picklistValuesWrapper>();
        Map<String,List<picklistValuesWrapper>> dependentPicklistValues = new Map<String,List<picklistValuesWrapper>>();
        
        //Get Session Id value
        String sessionId = EAW_FieldDescriber.getSessionId();
        
        if(String.isNotBlank(sessionId) && String.isNotBlank(recordTypeId) && String.isNotBlank(objectName) && String.isNotBlank(dependentFieldName)) {
                    
            try { 
                
                Http http = new Http();
                HttpRequest request = new HttpRequest();
                String host = System.Url.getSalesforceBaseURL().toExternalForm();
                
                String url = host+'/services/data/v42.0/ui-api/object-info/'+objectName+'/picklist-values/'+recordTypeId+'/'+dependentFieldName;        
                request.setEndpoint(url);
                request.setMethod('GET');  
                request.setHeader('Authorization', 'OAuth '+sessionId);
                HttpResponse response;        
                response = http.send(request); 
                
                Map<String, Object> meta = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
                Map<String,Object> controllerValues = (Map<String,Object>)meta.get('controllerValues');
                
                for(String strKey: controllerValues.keySet()) {
                    String ctrlIndexVal = String.valueof(controllerValues.get(strKey));
                    controllingKeyMapValues.put(ctrlIndexVal, strKey);
                    controllingPicklistValues.add( new picklistValuesWrapper(strKey, strKey) );
                }
                
                if(meta.containsKey('values') ) {
                    
                    for(Object o: (List<Object>)meta.get('values')) {
                        
                        Map<String,Object> dependentValueMap = (Map<String,Object>) o;
                        String validFor = String.valueOf(dependentValueMap.get('validFor'));
                        
                        if( validFor != '' && validFor.contains('(') && validFor.contains(')') ){
                            
                            validFor = validFor.replace('(','');
                            validFor = validFor.replace(')','');
                            
                            if( controllingKeyMapValues.containsKey(validFor) ) {
                                String ctrlVal = controllingKeyMapValues.get(validFor);
                                List<picklistValuesWrapper> dependentValues;
                                if( dependentPicklistValues.containsKey(ctrlVal) ){
                                    dependentValues = dependentPicklistValues.get(ctrlVal);
                                } else {
                                    dependentValues = new List<picklistValuesWrapper>();
                                }
                                dependentValues.add( new picklistValuesWrapper( String.valueOf(dependentValueMap.get('label')), String.valueOf(dependentValueMap.get('value')) ) );
                                dependentPicklistValues.put(ctrlVal, dependentValues);
                            }
                        }
                    }
                }
                
                dependentPicklistValueWrapper.controllingPicklistValues = controllingPicklistValues;
                dependentPicklistValueWrapper.dependentPicklistValues = dependentPicklistValues;
            }
            catch(Exception e) {}
        }
        return dependentPicklistValueWrapper;
    }
    
    public class dependentPicklistValuesWrapper{
        @AuraEnabled
        public List<picklistValuesWrapper> controllingPicklistValues;
        @AuraEnabled
        public Map<String, List<picklistValuesWrapper>> dependentPicklistValues;
    }
    
    public class picklistValuesWrapper{
        @AuraEnabled
        public String label;
        @AuraEnabled
        public String value;
        
        public picklistValuesWrapper(String label, String value) {
            this.label = label;
            this.value = value;
        }
    }
}