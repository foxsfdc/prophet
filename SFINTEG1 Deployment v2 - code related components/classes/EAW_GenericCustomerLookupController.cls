public with sharing class EAW_GenericCustomerLookupController{

    
    @AuraEnabled
    public static Boolean updateCustomerField(String customerIds, String objectId) {
        
        try {
            String customerNames;
            
                Id objId = objectId;
                SObject record = objId.getSObjectType().newSObject(objId);
                record.put('customers__c',customerIds);
        
                if(record != NULL){
                    update record;
                    return true;
                }
        }
        catch(Exception e) {
            throw new AuraHandledException('Error:'+e.getMessage());
        }
        return false;
    }
    
    @AuraEnabled
    public static List<EAW_LookupSearchResult> getCustomer(String objectName,String objectId) {
        
        List<EAW_LookupSearchResult> lookupSearchResults = new List<EAW_LookupSearchResult>();
        String queryStr = 'SELECT Id,customers__c FROM '+objectName+' WHERE Id =: objectId';
        List<sObject> sobjList = Database.query(queryStr);
        
        if(sobjList.size() > 0 && sobjList[0].get('customers__c') != NULL && !String.isBlank((String)sobjList[0].get('customers__c'))) {
        
            String customers = (String)sobjList[0].get('customers__c') ;
            List<String> customerNamelist = customers.split(';');
            
            for(SObject result : [SELECT Id, Name, Customer_Id__c FROM EAW_Customer__c WHERE Name IN:customerNamelist ORDER BY Name ASC]) {
                lookupSearchResults.add(new EAW_LookupSearchResult(String.valueOf(result.get('Id')), 'EAW_Customer__c' , 'standard:account', String.valueOf(result.get('Name')), String.valueOf(result.get('Customer_Id__c'))));
            }
        }
        return lookupSearchResults; 
    }
}