public with sharing class EAW_WindowGuidelineStrands {

    public static String endPointURL {get;set;}
    public static String key {get;set;}
    public static String secret {get;set;}
    public static String token {get;set;}
    public static String host {get;set;}
    public static String grant_type {get;set;}
    
    @AuraEnabled
    public static List<EAW_Window_Guideline_Strand__c> insertImportedWindowGuidelineStrands(List<EAW_Window_Guideline_Strand__c> strand) {
        if(strand != null && strand.size() > 0) {
            insert strand;
        }
        return strand;
    }
    
    @AuraEnabled
    public static List<EAW_Window_Guideline_Strand__c> removeWindowGuidelineStrands(String windowGuidelineId) {
        
        String query = 'select id from EAW_Window_Guideline_Strand__c where EAW_Window_Guideline__r.Id =\'' + windowGuidelineId + '\'';
        List<EAW_Window_Guideline_Strand__c> deletedWindowGuidelineStrands = Database.query(query);
        
        if(deletedWindowGuidelineStrands != null && deletedWindowGuidelineStrands.size() > 0) {
            delete deletedWindowGuidelineStrands;
        }
        
        return deletedWindowGuidelineStrands;
    }
    
    @AuraEnabled
    public static String sendIDValues(String source, String idType, String idValue) {
        
        String categoryString;
            
        idClass cls = new idClass();
        cls.applicationId = source;
        cls.contractType = idType;
        cls.titleListId = idValue;
        String body = JSON.serialize(cls);
        
        //hardcoded values need to change that.
        /*body = '{"titleListId":"611723","contractType":"2","applicationId":"810"}';*/
        
        List<Rest_Endpoint_Settings__c> endPoints = new List<Rest_Endpoint_Settings__c> ([
            SELECT Name, End_Point__c 
            FROM Rest_Endpoint_Settings__c
            WHERE Name = 'Import Categories'
        ]);
        
        if(endPoints.size() > 0 && endPoints[0].End_Point__c != null) {
        
            endPointURL = endPoints[0].End_Point__c;
            
            String authToken='';
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            HTTPResponse res = new HTTPResponse();
            authToken = connect();
            //authToken = '6kQjeCVJoOshPWMCOKptVTaWkrnD';
            request.setMethod('POST');
            request.setHeader('Content-Type', 'application/json');
            request.setHeader('Accept', 'application/json');
            request.setBody(body);
            request.setHeader('Authorization', 'Bearer ' + authToken);
            request.setEndpoint(endPointURL);
            
            if(!Test.isRunningTest()) {
                //Execute web service call here
                res = http.send(request);
            } else {}
             
            if(res.getStatusCode() == 200) {
                categoryString = res.getBody();
            } else {
                String statusMessage = 'Callout Failed : Response Code: '+res.getStatusCode() + ' Response message: ' + res.getStatus();
                throw new AuraHandledException(statusMessage);
            }
        }
        
        return categoryString;
    }
    
    @AuraEnabled
    public static List<EAW_Window_Guideline_Strand__c> sendCategories(String requestParamBody, String actionType, String windowGuidelineId) {
        
        try {
            
            /*requestParamBody = '{"titleListCategoryId": "1591034","rightsVarianceId": "4383","rightsVarianceDescription": "NEX Dominican Republic","explicitMediaId": "183457","titleListCategoryName": "SVOD Series - New & Returning - Linear"}';*/
            
            List<Rest_Endpoint_Settings__c> endPoints = new List<Rest_Endpoint_Settings__c> ([
                SELECT Name, End_Point__c 
                FROM Rest_Endpoint_Settings__c
                WHERE Name = 'Send Categories'
            ]);
                        
            if(endPoints.size() > 0 && endPoints[0].End_Point__c != null) {
                
                endPointURL = endPoints[0].End_Point__c;
                String authToken='';
                authToken = connect();
                
                Http http = new Http();
                HttpRequest request = new HttpRequest();
                HTTPResponse res = new HTTPResponse();
                request.setMethod('POST');
                request.setHeader('Content-Type', 'application/json');
                request.setHeader('Accept', 'application/json');
                request.setBody(requestParamBody);
                request.setHeader('Authorization', 'Bearer ' + authToken);
                request.setEndpoint(endPointURL);
                
                if(!Test.isRunningTest()) {
                    //Execute web service call here
                    res = http.send(request);
                }
                else {}
                
                if(res.getStatusCode() == 200) {
                
                    List<windowStrand> windowStrandsListFromResponse = (List<windowStrand>)JSON.deserialize(res.getBody(), List<windowStrand>.class);
                    List<EAW_Window_Guideline_Strand__c> wgsList = new List<EAW_Window_Guideline_Strand__c>();
                    List<EAW_Window_Guideline_Strand__c> removedWGSList = new List<EAW_Window_Guideline_Strand__c>();
                    
                    List<EAW_Territories__c> listCustomSettingTerritories = EAW_Territories__c.getAll().values();
                    Map<Integer, String> mapCustomSettingTerritories = new Map<Integer, String>();
                    for(EAW_Territories__c territory : listCustomSettingTerritories){
                    	mapCustomSettingTerritories.put(territory.Territory_Id__c.intValue(), territory.Name__c);
                    }
                    
                    Map<Integer, String> mapCustomSettingLanguages = new Map<Integer, String>();
                    List<EAW_Languages__c> listCustomSettingLanguages = EAW_Languages__c.getAll().values();
                    
                    for(EAW_Languages__c language : listCustomSettingLanguages){
                    	mapCustomSettingLanguages.put(language.Language_Id__c.intValue(), language.Name__c);
                    }
                    
                    Map<Integer, String> mapCustomSettingMedia = new Map<Integer, String>();
                    List<EAW_Media__c> listCustomSettingMedia = EAW_Media__c.getAll().values();
                    
                    for(EAW_Media__c media : listCustomSettingMedia){
                    	mapCustomSettingMedia.put(media.Media_Id__c.intValue(), media.name);
                    }
                    
                    if(windowStrandsListFromResponse != NULL && windowStrandsListFromResponse.size() > 0) {
                           
                        for(windowStrand ws : windowStrandsListFromResponse) {
                            
                            List<String> territoriesList = getMultiselectListTerritories(ws.territoryIds, 100, mapCustomSettingTerritories);                        
                            List<Integer> languageIds = new List<Integer>();
                            if(ws.languages.contains('No Specific Language') || ws.languages.contains('All Languages')){
                            	languageIds = new List<Integer>(mapCustomSettingLanguages.keySet());
                            } else{
                            	languageIds = ws.languageIds;
                            }                      
                            List<String> languagesList = getMultiselectListLanguages(languageIds, 100, mapCustomSettingLanguages);
                            String media = getMultiselectListMedia(ws.mediaIds, mapCustomSettingMedia);
                   
                            for(String territories : territoriesList){
                            	for(String languages : languagesList){
                            		EAW_Window_Guideline_Strand__c wgs = getWindowGuidelineStrand(windowGuidelineId, ws.licenseType, media, languages, territories);
                            		wgsList.add(wgs);
                            	}
                            }
                        }
                    }
                    
                    if(!String.isBlank(actionType)) {
                        if(actionType == 'Append') {
                            wgsList = EAW_WindowGuidelineStrands.insertImportedWindowGuidelineStrands(wgsList);
                        } 
                        else if(actionType == 'Replace') {
                            removedWGSList = EAW_WindowGuidelineStrands.removeWindowGuidelineStrands(windowGuidelineId);
                            wgsList = EAW_WindowGuidelineStrands.insertImportedWindowGuidelineStrands(wgsList);
                        }
                    }
                    return wgsList;
                } else {
                    String statusMessage = 'Callout Failed : Response Code: '+res.getStatusCode() + ' Response message: ' + res.getStatus();
                    throw new AuraHandledException(statusMessage);
                }
            }
        } catch(Exception e) {
            throw new AuraHandledException('Error : ' + e.getMessage());
        } 
        return null;
    }
    
    public static String makeValuesAsMultiselct(string inputStr) {
        
        String outputStr='';
        inputStr = inputStr.replace('[','');
        inputStr = inputStr.replace(']','');
        
        if(inputStr.contains(',')) {
            List<String> tempList = inputStr.split(',');
            for(String s : tempList) {
                s = s.trim();
                outputStr += s +';';
            }
            outputStr = outputStr.removeEnd(';');
        } else {
            outputStr = inputStr;
        }
        
        return outputStr;
    }
    
    public static EAW_Window_Guideline_Strand__c getWindowGuidelineStrand(String windowGuidelineId, String licenseType, String medias, String languages, String territories) {
    	EAW_Window_Guideline_Strand__c wgs = new EAW_Window_Guideline_Strand__c();
        wgs.EAW_Window_Guideline__c = windowGuidelineId;
        wgs.License_Type__c = licenseType;
        wgs.Imported__c = true;
                            
        if(medias != NULL) {
        	wgs.Media__c = makeValuesAsMultiselct(medias);
        }
        if(languages != NULL) {
           wgs.Language__c = languages;
        }
        if(territories != NULL) {
           wgs.Territory__c = territories;
        }
        return wgs;
    }
    
    public static List<String> getMultiselectListLanguages(List<Integer> languageIds, Integer max, Map<Integer, String> mapCustomSettingLanguages) {
        List<String> outputList = new List<String>();
        
        Integer i = 0;
        String outputStr = '';
        for(Integer languageId : languageIds) {
            String language = null;
            if(mapCustomSettingLanguages.containsKey(languageId)){
            	language = mapCustomSettingLanguages.get(languageId);
                if(!language.contains('No Specific Language')){
	                if(i < max)
	                	outputStr += language +';';
	                else {
	                	outputStr = outputStr.removeEnd(';');
	                	outputList.add(outputStr);
	                	outputStr = '';
	                	outputStr += language +';';
	                	i=0;
	                }
                }
            }
            i = i+1;
        }
        outputStr = outputStr.removeEnd(';');
        outputList.add(outputStr);
        
        return outputList;
    }
    
    public static String getMultiselectListMedia(List<Integer> mediaIds, Map<Integer, String> mapCustomSettingMedia) {
        List<String> outputList = new List<String>();
        
        String outputStr = '';
        for(Integer mediaId : mediaIds) {
            String media = null;
            if(mapCustomSettingMedia.containsKey(mediaId)){
            	media = mapCustomSettingMedia.get(mediaId);
               	outputStr += media +';';
            }
        }
        outputStr = outputStr.removeEnd(';');
              
        return outputStr;
    }
    
     public static List<String> getMultiselectListTerritories(List<Integer> territoryIds, Integer max, Map<Integer, String> mapCustomSettingTerritories) {
        List<String> outputList = new List<String>();
        
        Integer i = 0;
        String outputStr = '';
        for(Integer territoryId : territoryIds) {
        	String territory = null;
            if(mapCustomSettingTerritories.containsKey(territoryId)){
            	territory = mapCustomSettingTerritories.get(territoryId);
            	if(i < max) {
            		outputStr += territory +';';
            	}
            	else {
                	outputStr = outputStr.removeEnd(';');
                	outputList.add(outputStr);
                	outputStr = '';
                	outputStr += territory +';';
                	i=0;
            	}
            }
            
            i = i+1;
        }
        outputStr = outputStr.removeEnd(';');
        outputList.add(outputStr);
        
        return outputList;
    }
    
    
    public class categories {
    
        public categories() {}
        public String titleListCategoryId;
        public String rightsVarianceId;
        public String rightsVarianceDescription;
        public String explicitMediaId;
        public String titleListCategoryName;
    }
    
    public class idClass {
    
        public idClass() {}
        public String applicationId;
        public String contractType;
        public String titleListId;
    }
    
    public class responseClass {
    
        public responseClass() {}
        public String access_token;
        public String token_type;
        public String expires_in;
    }
    
    public class windowStrand {
    
        public windowStrand() {}
        public String licenseType;
        public String medias;
        public String languages;
        public String territories;
        public List<Integer> languageIds;
        public List<Integer> territoryIds;
        public List<Integer> mediaIds;
    }
    
    //Returns the Auth Token
    public static String connect() {
    
        key = 'eeiBGpo4K2jQSuKXMzLdpDW38IdcEtQA';
        secret='T3IIwtxJeBHljEkk';
        token='https://ms-devapi.foxinc.com/oauth/oauth20/token';
        
        grant_type = 'client_credentials';
        String requestBody = 'grant_type=' + grant_type +'&client_id=' + key + '&client_secret=' + secret;
        HttpRequest req = new HttpRequest();
        req.setEndpoint(token);
        req.setHeader('Content-Type','application/x-www-form-urlencoded');
        //req.setHeader('Host', host);
        req.setHeader('Connection', ' Keep-Alive');
        req.setHeader('scope', 'read');
        req.setMethod('POST');
        req.setBody(requestBody);
        Http http = new Http();
        HTTPResponse response;
        
        if(!Test.isRunningTest()) {
            response = http.send(req);
        } 
        else {}
        
        responseClass rc = (responseClass)JSON.deserialize(response.getBody(),responseClass.class);
        //system.debug('Response Body:'+responseBody);
        String tokenSetter = 'Not Set';
        tokenSetter = rc.access_token;
    
        return tokenSetter;
    }
}