public class EAW_GalileoRightsCheck_BatchProcess implements Database.Batchable<sObject>,Database.Stateful, Schedulable {
    
    public Set<Id> windowGuideLineIdSet = new Set<Id>();
    public Set<Id> titleIdSet = new Set<Id>();
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
    
        List<String> statusToProcess = new List<String>{'Need Downstream Calc'};
		
        return Database.getQueryLocator(
        	'SELECT id, Message__c, Source_System__c, Status__c FROM EAW_Inbound_Notifications__c WHERE Source_System__c  =  \'Galileo Rights Check\'  AND  Status__c  in: statusToProcess ORDER BY CreatedDate ASC NULLS FIRST'
        );
    }
    
    public void execute(Database.BatchableContext bc, List<EAW_Inbound_Notifications__c> eawList){
        if(eawList!=null && eawList.isempty()==False){
        	
        	Set<String> windowIds = new Set<String>();
			if(eawList!=null && !eawList.isEmpty()){
				for(EAW_Inbound_Notifications__c eaw:eawList){
					if(String.isNotBlank(eaw.Message__c)){
						for(String wid:eaw.Message__c.split(';')){
							windowIds.add(wid);
						}
					}
					eaw.status__c='Success';
				}
	        	List<EAW_Window__c> records = [SELECT EAW_Title_Attribute__c,EAW_Window_Guideline__c,Id FROM EAW_Window__c WHERE Id IN :windowIds];
	        	if(records!=null && records.isempty()==False){
		        	for(EAW_Window__c eaw: records){
		        		windowGuideLineIdSet.add(eaw.EAW_Window_Guideline__c);
		        		titleIdSet.add(eaw.EAW_Title_Attribute__c);
		        	}
	        	}
	        	update eawList;
			}        	
        }
    }
    
    public void finish(Database.BatchableContext bc) {
        // execute any post-processing operations
        if(windowGuideLineIdSet!=null && !windowGuideLineIdSet.isEmpty() && titleIdSet!=null && !titleIdSet.isEmpty()){
	        EAW_WGDateCalculationBatchClass windowDateBatch = new EAW_WGDateCalculationBatchClass(windowGuideLineIdSet);
            windowDateBatch.releaseDateGuidelineIdSet = NULL;
            windowDateBatch.titleIdSet = titleIdSet;
            ID batchprocessid = Database.executeBatch(windowDateBatch, 200);
        }
    }
    
    public void execute(SchedulableContext sc) {
        EAW_GalileoRightsCheck_BatchProcess eawBatch = new EAW_GalileoRightsCheck_BatchProcess();
        Database.executeBatch(eawBatch,1000);
    }
}