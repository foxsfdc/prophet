@RestResource(urlMapping='/ReleaseDate/*')
global with sharing class EAW_ReleaseDateUpdate {
    
    // EndPoint: /services/apexrest/ReleaseDate/updateReleaseDates
    //{"releaseDateUpdateList":[{"releaseDateId":"aAA3D00000001cPWAQ","manualDate":"2018-08-01","projectedDate":"2018-08-03","releaseDate":"2018-08-03","feedDate":"2018-08-02"}]}
	@HttpPOST
    global static void updateReleaseDates(List<releaseDateUpdate> releaseDateUpdateList) 
    {
        try 
        {
            List<EAW_Release_Date__c> releaseDates = new List<EAW_Release_Date__c>();
            
            for(releaseDateUpdate rU: releaseDateUpdateList)
            {
            	EAW_Release_Date__c releaseDate = new EAW_Release_Date__c();
            	releaseDate.Id = ru.releaseDateId;
            	releaseDate.Manual_Date__c = date.valueOf(ru.manualDate);
            	releaseDate.Projected_Date__c = date.valueOf(rU.projectedDate);
            	releaseDate.Feed_Date__c = date.valueOf(rU.feedDate);
            	releaseDates.add(releaseDate);
            }
            if ( releaseDates.size() > 0 )
            	upsert releaseDates;
        } 
        catch(Exception e) 
        {
            system.debug('EXCEPTION: ' + e.getMessage());
        }
    }
    
    global class releaseDateUpdate
    {
    	public String releaseDateId;
    	public String manualDate;
    	public String projectedDate;
    	public String releaseDate;
    	public String feedDate;
    }
}