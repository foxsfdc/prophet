/*    
    Trigger : EAW_TitleTrigger
    Handler : EAW_Title_Handler
*/
@isTest
public class EAW_TitleTrigger_Test {
    
    @testSetup static void setupTestData() {
        EAW_TestDataFactory.createEAWRule(1, true);
        EAW_TestDataFactory.createRuleDetail(1, true);
        EAW_TestDataFactory.createEAWCustomer(1,true);
        EAW_TestDataFactory.createReleaseDateType(1, true);
        EAW_TestDataFactory.createEAWTitle(1, true);
    }
    
    @isTest static void Notifiation_Test(){
        
        EDM_REF_DIVISION__c refDivision = new EDM_REF_DIVISION__c(Name = 'Fox 2000');
        insert refDivision;
        
        EDM_REF_DIVISION__c refDivision1 = new EDM_REF_DIVISION__c(Name = 'Fox');
        insert refDivision1;
        
        CollaborationGroup chatterGroup = new CollaborationGroup (
            Name = 'Test2 Review',
            CollaborationType = 'Public'
        );
        insert chatterGroup;
        
        EAW_Notification_Status__c ns = new EAW_Notification_Status__c(
            TACategoryNotification__c = TRUE, 
            TATitlePlanDisQualifierNotification__c = TRUE,
            TACreateNotification__c = TRUE,
            TAProductTypeNotification__c = TRUE,
            TADivisionNotification__c = TRUE,
            TAStatusNotification__c = TRUE
        );
        insert ns;
        
        EDM_REF_PRODUCT_TYPE__c refProduct = new EDM_REF_PRODUCT_TYPE__c(Name = 'Compilation Episode');
        insert refProduct;
        
        EAW_Title__c title = new EAW_Title__c(Name='Test Title', PROD_TYP_CD_INTL__c = refProduct.id, FIN_DIV_CD__c = refDivision.Id, LIFE_CYCL_STAT_GRP_CD__c = 'PUBLC');
        insert title;
        
        title.LIFE_CYCL_STAT_GRP_CD__c = 'CNFDL';
        title.FIN_DIV_CD__c = refDivision1.Id;
        update title;
    }
    
    @isTest static void processTitleToCheckForPlanQualification_Test(){
        
        EDM_REF_DIVISION__c refDivision = new EDM_REF_DIVISION__c(Name = 'Fox 2000');
        insert refDivision;
        
        CollaborationGroup chatterGroup = new CollaborationGroup (
            Name = 'Test2 Review',
            CollaborationType = 'Public'
        );
        insert chatterGroup;
        
        EAW_Notification_Status__c ns = new EAW_Notification_Status__c(
            TACategoryNotification__c = TRUE, 
            TATitlePlanDisQualifierNotification__c = TRUE,
            TACreateNotification__c = TRUE,
            TAProductTypeNotification__c = TRUE,
            TADivisionNotification__c = TRUE,
            TAStatusNotification__c = TRUE
        );
        insert ns;
        
        List<EAW_Plan_Guideline__c> planGuideLineList = EAW_TestDataFactory.createPlanGuideline(1, false);
        planGuideLineList[0].Name = 'Test plan Guideline';
        planGuideLineList[0].Product_Type__c = 'Compilation Episode';
        insert planGuideLineList;
        
        List<EAW_Window_Guideline__c> windowGuidelineList = EAW_TestDataFactory.createWindowGuideLine(1, false);
        windowGuidelineList[0].Status__c = 'Draft';
        windowGuidelineList[0].Product_Type__c = 'Compilation Episode';
        windowGuidelineList[0].Window_Type__c = 'Library';
        insert windowGuidelineList;
        
        List<EAW_Window_Guideline_Strand__c> windowGuideLineStrandList = EAW_TestDataFactory.createWindowGuideLineStrand(1, false);
        windowGuideLineStrandList[0].License_Type__c = 'Exhibition License';
        windowGuideLineStrandList[0].EAW_Window_Guideline__c = windowGuideLineList[0].Id;
        windowGuideLineStrandList[0].Media__c = 'Theatrical';
        windowGuideLineStrandList[0].Territory__c = 'Afghanistan';
        windowGuideLineStrandList[0].Language__c = 'Afrikaans';

        insert windowGuideLineStrandList; 
        
        windowGuidelineList[0].Status__c = 'Active';
        update windowGuidelineList;
        
        List<EAW_Plan_Window_Guideline_Junction__c> planWindowList = new List<EAW_Plan_Window_Guideline_Junction__c> {
            new EAW_Plan_Window_Guideline_Junction__c(Plan__c = planGuideLineList[0].Id , Window_Guideline__c = windowGuideLineList[0].Id)
        };
        insert planWindowList;
        
        planGuideLineList[0].Status__c = 'Active';
        update planGuideLineList;
        
        RecordType recType = [Select Id from RecordType WHERE Name = 'Qualifier' AND SObjectType = 'EAW_Rule__c'];
        
        RecordType recTypeDetail = [Select Id from RecordType WHERE Name = 'Qualifier' AND SObjectType = 'EAW_Rule_Detail__c'];
        
        EAW_Rule__c ruleInstance = new EAW_Rule__c(RecordTypeId = recType.Id, Plan_Guideline__c = planGuideLineList[0].Id);
        insert ruleInstance;
                
        EAW_Rule_Detail__c ruledetailList = new EAW_Rule_Detail__c(RecordTypeId = recTypeDetail.Id, Rule_No__c = ruleInstance.Id, Condition_Field__c = 'Division',
        Condition_Year__c= '2000', Condition_Operator__c= '>');        
        insert ruledetailList;
        
        EDM_REF_PRODUCT_TYPE__c refProduct = new EDM_REF_PRODUCT_TYPE__c(Name = 'Compilation Episode');
        insert refProduct;
        
        EAW_Title__c title = new EAW_Title__c(Name='Test Title', PROD_TYP_CD_INTL__c = refProduct.id, FIN_DIV_CD__c = refDivision.Id);
        insert title;
    }
    
    @isTest static void filterTitleToCheckForPlanQualification_Test() {
        
        EAW_Notification_Status__c ns = new EAW_Notification_Status__c(
            TACategoryNotification__c = TRUE, 
            TATitlePlanDisQualifierNotification__c = TRUE,
            TACreateNotification__c = TRUE,
            TAProductTypeNotification__c = TRUE,
            TADivisionNotification__c = TRUE,
            TAStatusNotification__c = TRUE
        );
        insert ns;
        
        EDM_REF_PRODUCT_TYPE__c refProduct = new EDM_REF_PRODUCT_TYPE__c(Name = 'Feature');
        insert refProduct;
        
        EDM_REF_PRODUCT_TYPE__c refProduct1 = new EDM_REF_PRODUCT_TYPE__c(Name = 'Compilation Episode');
        insert refProduct1;
        
        EAW_Title__c title = new EAW_Title__c(Name='Test Title-101', PROD_TYP_CD_INTL__c = refProduct.id, RLSE_CAL_YR__c = '2019', LIFE_CYCL_STAT_GRP_CD__c = 'PUBLC');
        insert title;
        
        List<EAW_Window_Guideline__c> wgList = EAW_TestDataFactory.createWindowGuideLine(2, FALSE);
        wgList[0].Product_Type__c = 'Feature';
        wgList[1].Product_Type__c = 'Feature';
        
        insert wgList;
        
        List<EAW_Window_Guideline_Strand__c> wgsList = EAW_TestDataFactory.createWindowGuideLineStrand(2, FALSE);
        
        wgsList[0].EAW_Window_Guideline__c = wgList[0].Id;
        wgsList[0].Media__c = 'Basic On Demand';
        wgsList[0].Language__c = 'English';
        wgsList[0].Territory__c = 'India';
        
        wgsList[1].EAW_Window_Guideline__c = wgList[1].Id;
        wgsList[1].Media__c = 'EST';
        wgsList[1].Language__c = 'English';
        wgsList[1].Territory__c = 'Albania';
        
        insert wgsList;
        
        wgList[0].Status__c = 'Active';
        wgList[1].Status__c = 'Active';
        
        update wgList;
        
        List<EAW_Plan_Guideline__c> pgList = EAW_TestDataFactory.createPlanGuideline(2, FALSE);
        pgList[0].Product_Type__c = 'Feature';
        pgList[1].Product_Type__c = 'Feature';
        
        insert pgList;
        
        List<EAW_Plan_Window_Guideline_Junction__c> pwgList = EAW_TestDataFactory.createPlanWindowGuideLineJunction(2, FALSE);
        pwgList[0].Plan__c = pgList[0].Id;
        pwgList[0].Window_Guideline__c = wgList[0].Id;
        
        pwgList[1].Plan__c = pgList[1].Id;
        pwgList[1].Window_Guideline__c = wgList[1].Id;
        
        insert pwgList;
        
        pgList[0].Status__c = 'Active';
        pgList[1].Status__c = 'Active';
        
        update pgList;
        
        RecordType recType = [Select Id from RecordType WHERE Name = 'Qualifier' AND SObjectType = 'EAW_Rule__c'];
        
        RecordType recTypeDetail = [Select Id from RecordType WHERE Name = 'Qualifier' AND SObjectType = 'EAW_Rule_Detail__c'];
        
        EAW_Rule__c ruleInstance = new EAW_Rule__c(RecordTypeId = recType.Id, Plan_Guideline__c = pgList[0].Id);
        insert ruleInstance;
                
        EAW_Rule_Detail__c ruledetailList = new EAW_Rule_Detail__c(RecordTypeId = recTypeDetail.Id, Rule_No__c = ruleInstance.Id, Condition_Field__c = 'Release Year',
        Condition_Year__c= '2000', Condition_Operator__c= '>');        
        insert ruledetailList;
        
        EAW_Plan__c plan = new EAW_Plan__c ();
        plan.EAW_Title__c = title.Id;
        plan.Plan_Guideline__c = pgList[0].Id;
        
        insert plan;    
        
        title.Name = 'Test Title-102';
        title.PROD_TYP_CD_INTL__c = refProduct1.id;
        title.LIFE_CYCL_STAT_GRP_CD__c = 'SCNFDL';
        
        update title;
    }
    
    @isTest static void deleteRelatedReleaseDates_Test() {
        
        List<EAW_Title__c> eawTitleList = [SELECT Id, Name FROM EAW_Title__c LIMIT 1];
        
        EAW_Release_Date__c releaseDate = new EAW_Release_Date__c(Title__c = eawTitleList[0].Id);
        insert releaseDate;
        
        delete eawTitleList;  
        
        List<EAW_Title__c> obtainedList = [SELECT Id, Name FROM EAW_Title__c LIMIT 1];
        
        System.assertEquals(true, obtainedList != null);
    }
}