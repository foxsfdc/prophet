public with sharing class EAW_ReleaseDateController {
    
    /*@AuraEnabled
    public static List<EAW_Release_Date__c> deleteReleaseDates(List<String> selectedIds) {
        try {
            String query = 'SELECT Id FROM EAW_Release_Date__c WHERE Id in :selectedIds';
            List<EAW_Release_Date__c> releaseDates = Database.query(query);

            delete releaseDates;

            return releaseDates;
        } catch(Exception e) {
            throw new AuraHandledException('Error : ' + e.getMessage());
        }
    }*/

    /*@AuraEnabled
    public static Map<String, List<SObject>> massUpdate(List<Id> selectedIds, String status, 
                                                        String tempPerm, String manualDate, List<Id> tags, 
                                                        Boolean clearManualDate, Boolean clearTags,
                                                        String notesTitle, String notesBody) {
                                                            system.debug('selectedIds::'+selectedIds);
                                                            system.debug('tempPerm::'+tempPerm);
                                                            system.debug('manualDate::'+manualDate);
                                                            system.debug('tags::'+tags);
                                                            system.debug('clearManualDate::'+clearManualDate);
                                                            system.debug('clearTags::'+clearTags);
                                                            system.debug('notesTitle::'+notesTitle);
                                                            system.debug('notesBody::'+notesBody);
        
        try {
            List<EAW_Release_Date_Tag_Junction__c> oldReleaseDateTagJunctions = new List<EAW_Release_Date_Tag_Junction__c>();
            List<EAW_Release_Date_Tag_Junction__c> newReleaseDateTagJunctions = new List<EAW_Release_Date_Tag_Junction__c>();
            List<EAW_Release_Date_Tag_Junction__c> releaseDateTagJunctions = new List<EAW_Release_Date_Tag_Junction__c>();
            List<EAW_Release_Date_Tag_Junction__c> deleteList = new List<EAW_Release_Date_Tag_Junction__c>();
            List<Note> notes = new List<Note>();
            Map<Id, Set<Id>> oldMap = new Map<Id, Set<Id>>();
            Map<Id, Set<Id>> deleteMap = new Map<Id, Set<Id>>();
            Date formattedManualDate = null;
 
            //LRCC:1096            
            String releaseDateQuery = 'SELECT Status__c, Temp_Perm__c, Manual_Date__c FROM EAW_Release_Date__c WHERE id IN :selectedIds';
            List<EAW_Release_Date__c> releaseDates = Database.query(releaseDateQuery); 
            
            //LRCC:1096
            String tagsQuery = 'SELECT Id, Tag__c, Release_Date__c FROM EAW_Release_Date_Tag_Junction__c WHERE Release_Date__c IN :selectedIds';
            oldReleaseDateTagJunctions = Database.query(tagsQuery);
            
            //To avoid the default value getting update with Release Date records. If we not select any date then it default returns '1/1/1970' as a value
            Date defaultDate = Date.parse('1/1/1970');
            
            if(manualDate != null && manualDate != '' && Date.parse(manualDate) != defaultDate ) {
                formattedManualDate = Date.parse(manualDate);
            }
            
            //LRCC:1096 start
            if(clearTags != null && clearTags == true) {
                
                deleteList = oldReleaseDateTagJunctions;
                
                for(EAW_Release_Date__c releaseDate : releaseDates) {
                    Note note = new Note();
                    note.ParentId = releaseDate.Id;
                    note.Title = notesTitle;
                    
                    if(tags.size() > 0) {
                        
                        for(String tagId : tags) {
                            
                            EAW_Release_Date_Tag_Junction__c releaseDateTagJunction = new EAW_Release_Date_Tag_Junction__c();
                            releaseDateTagJunction.Release_Date__c = releaseDate.Id;
                            releaseDateTagJunction.Tag__c = tagId;
                            
                            newReleaseDateTagJunctions.add(releaseDateTagJunction);
                        }
                    }
                    
                    if(notesBody != null && notesBody != '') {
                        note.Body = notesBody;
                    }
                    
                    if(status != null && status != '') {
                        releaseDate.Status__c = status;
                    }
                    
                    if(tempPerm != null && tempPerm != '') {
                        releaseDate.Temp_Perm__c = tempPerm;
                    }
                    
                    releaseDate.Manual_Date__c = ((clearManualDate != null && clearManualDate == true) ? formattedManualDate : formattedManualDate);
                    
                    notes.add(note);
                }
            } else {
                
                
                for (EAW_Release_Date_Tag_Junction__c dateTagRec : oldReleaseDateTagJunctions) {
                    
                    if (! oldMap.containsKey(dateTagRec.Release_Date__c)) {
                        oldMap.put(dateTagRec.Release_Date__c, new Set<Id>());
                    }
                    oldMap.get(dateTagRec.Release_Date__c).add(dateTagRec.Tag__c);
                }
                system.debug('oldMap::::'+oldMap);
                
                
                for(EAW_Release_Date__c releaseDate : releaseDates) {
                    
                    Note note = new Note();
                    note.ParentId = releaseDate.Id;
                    note.Title = notesTitle;
                    
                    Set<Id> oldTags = new Set<Id>();
                    Set<Id> newTags = new Set<Id>();
                    newTags = new Set<Id> (tags);
                    
                    if (oldMap.containsKey(releaseDate.Id)) {
                        oldTags = oldMap.get(releaseDate.Id);
                    }
                    
                    
                    for (Id newTagId : newTags) {
                        if (! oldTags.contains(newTagId)) {
                            
                            EAW_Release_Date_Tag_Junction__c releaseDateTagJunction = new EAW_Release_Date_Tag_Junction__c();
                            releaseDateTagJunction.Release_Date__c = releaseDate.Id;
                            releaseDateTagJunction.Tag__c = newTagId;
                            
                            newReleaseDateTagJunctions.add(releaseDateTagJunction);
                        }
                    }
                    
                    if(notesBody != null && notesBody != '') {
                        note.Body = notesBody;
                    }
                    
                    if(status != null && status != '') {
                        releaseDate.Status__c = status;
                    }
                    
                    if(tempPerm != null && tempPerm != '') {
                        releaseDate.Temp_Perm__c = tempPerm;
                    }
                    
                    releaseDate.Manual_Date__c = ((clearManualDate != null && clearManualDate == true) ? formattedManualDate : formattedManualDate);
                    
                    notes.add(note);                
                }
            }
    
            insert notes;
            update releaseDates;
            
            //LRCC-1010: Add a check box to clear the Release Date Tag Junctions
            
            if (deleteList.size() > 0) {
                
                system.debug('deleteList ::: '+deleteList);
                system.debug('deleteListSize::'+deleteList.size());
                delete deleteList;
            }
            
            if(newReleaseDateTagJunctions.size() > 0) {
                system.debug('newReleaseDateTagJunctions:::'+newReleaseDateTagJunctions);
                insert newReleaseDateTagJunctions;
                releaseDateTagJunctions = [SELECT id, Release_Date__c, Tag__c, Tag__r.Name FROM EAW_Release_Date_Tag_Junction__c WHERE Id IN : newReleaseDateTagJunctions ];
            }

            Map<String, List<SObject>> objectMap = new Map<String, List<SObject>>();
            objectMap.put('releaseDates', releaseDates);
            objectMap.put('releaseDateTagJunctions', releaseDateTagJunctions);
            objectMap.put('notes', notes);
            //LRCC:1096 end

            return objectMap;
        } catch(Exception e) {
            throw new AuraHandledException('Error : ' + e.getMessage());
        }
    }*/
    
    
    @AuraEnabled
    public static void deleteReleaseDates(List<String> selectedIds ){
        
        try {
            List<EAW_Release_Date_Tag_Junction__c> oldReleaseDateTagJunctions = new List<EAW_Release_Date_Tag_Junction__c>();
            if( selectedIds.size() > 0) {
                String tagsQuery = 'SELECT Id, Tag__c, Release_Date__c FROM EAW_Release_Date_Tag_Junction__c WHERE Release_Date__c = :selectedIds';
                oldReleaseDateTagJunctions = Database.query(tagsQuery);
                if( oldReleaseDateTagJunctions != NULL && oldReleaseDateTagJunctions.size() > 0 ) {
                    checkRecursiveData.bypassDateCalculation = TRUE;
                    delete oldReleaseDateTagJunctions;
                }
                
                /*String releaseDateQuery = 'SELECT Status__c, Temp_Perm__c, Manual_Date__c FROM EAW_Release_Date__c WHERE id = :selectedIds';
                List<EAW_Release_Date__c> releaseDates = Database.query(releaseDateQuery);
                if( releaseDates != NULL && releaseDates.size() > 0 ) delete releaseDates;*/
                
                //Instead of Delete we have to do soft delete for the Release date records
                List<EAW_Release_Date__c> releaseDates = [ Select Id, Release_Date_Guideline__c, Title__c, Active__c, Send_To_Third_Party__c, Repo_DML_Type__c, Galileo_DML_Type__c From EAW_Release_Date__c Where Id IN : selectedIds AND Active__c = TRUE ];
                
                Set<Id> parentRdgIdSet = new Set<Id>();
                Set<Id> titleIdSet = new Set<Id>();
                
                if( releaseDates != NULL && releaseDates.size() > 0 ) {
                    for(EAW_Release_Date__c newRd : releaseDates){
                        newRd.Active__c = False;
                        //LRCC-1821
                        newRd.Soft_Deleted__c = 'TRUE';
                        newRd.Projected_Date__c = null;
                        
                        newRd.Send_To_Third_Party__c = True;
                        newRd.Repo_DML_Type__c = 'Delete';
                        newRd.Galileo_DML_Type__c = 'Delete';
                        if( newRd.Release_Date_Guideline__c != NULL ) parentRdgIdSet.add(newRd.Release_Date_Guideline__c);
                        if( newRd.Title__c != NULL ) titleIdSet.add(newRd.Title__c);
                    }
                    checkRecursiveData.bypassDateCalculation = TRUE;
                    update releaseDates;
                    
                    if( parentRdgIdSet.size() > 0 ) {
                        EAW_DownstreamRuleGuidelinesFindUtil dependentGuidelineFindUtil = new EAW_DownstreamRuleGuidelinesFindUtil();
                        Set<Id>  rdgIdSet = dependentGuidelineFindUtil.findDependentReleaseDateGuidelineToProcess(parentRdgIdSet);
                        if( rdgIdSet != NULL && rdgIdSet.size() > 0 ) {
                            /*EAW_CreateRDForNewRDGBatch releaseDateBatch = new EAW_CreateRDForNewRDGBatch(rdgIdSet,null);
                            releaseDateBatch.releaseDateGuidelineIdSet = rdgIdSet;
                            releaseDateBatch.titleIdSet = titleIdSet;
                            ID batchprocessid = Database.executeBatch(releaseDateBatch, 200);*/
                            
                            EAW_RDGDateCalculationBatchClass releaseDateBatch = new EAW_RDGDateCalculationBatchClass(rdgIdSet);
                            releaseDateBatch.titleIdSet = titleIdSet;
                            releaseDateBatch.windowGuidelineIdSet = NULL;
                            releaseDateBatch.disQualifyFlag = true;
                            ID batchprocessid = Database.executeBatch(releaseDateBatch, 200);
                        } else {
                            Set<Id> wgIds = dependentGuidelineFindUtil.findDependentWindowGuidelineToProcess(parentRdgIdSet);
                            if( wgIds != NULL && wgIds.size() > 0 ){
                                EAW_WGDateCalculationBatchClass windowDateBatch = new EAW_WGDateCalculationBatchClass(wgIds);
                                windowDateBatch.titleIdSet = titleIdSet;
                                ID batchprocessid = Database.executeBatch(windowDateBatch, 200);
                            }
                        }
                    }
                }
            }
        } catch(Exception e) {
            throw new AuraHandledException('Error : ' + e.getMessage());
        }
    }
    
    @AuraEnabled
    public static List<Object> getFields(Map<String,String> objFieldSetMap) {
            
        List<Object>fieldNames = new List<Object>();
        for(String strObjectName: objFieldSetMap.keySet())
        {
            fieldNames.add(EAW_FieldSetController.getFields(strObjectName, objFieldSetMap.get(strObjectName)));
        }
        return fieldNames;
    }
    
    @AuraEnabled
    public static List<EAW_Release_Date__c> saveReleaseDateRecords(List<EAW_Release_Date__c> releaseDateRecords, String recordsMap) {
        // LRCC:1096 start
        Map<Id, List<Id>> recordMap = (Map<Id, List<Id>>) JSON.deserialize(recordsMap, Map<Id, List<Id>>.class);
        Map<Id, List<Id>> oldDateTagMap = new Map<Id, List<Id>>();
        
        system.debug('releaseDateRecords::'+releaseDateRecords);
        system.debug('recordMap::'+recordMap);
        
        List<EAW_Release_Date_Tag_Junction__c> newReleaseDateTagJunctions = new List<EAW_Release_Date_Tag_Junction__c>();
        List<EAW_Release_Date_Tag_Junction__c> deleteList = new List<EAW_Release_Date_Tag_Junction__c>();
        Map<Id,List<Id>> deleteMap = new Map<Id,List<Id>>();
        Set<Id> releaseDateIds = new Set<Id>();
        //LRCC:1096 end
        //LRCC-1539
        String errorMsg = '';
        
        //Related to Date calculation
        Set<Id> parentRDGIdSet = new Set<Id>();
        Set<Id> titleIdSet = new Set<Id>();
        Set<Id> releaseDateIdSet = new Set<Id>();
        
        try {
            
            if(releaseDateRecords != null && releaseDateRecords.size() > 0) {
                
                List<String>customers = new List<String>();
                Map<String, Id>customerMap = new Map<String, Id>();
                //List<String>releaseDtTypes = new List<String>();
                
                Map<Id, EAW_Release_Date__c> releaseDateOldMap = new Map<Id, EAW_Release_Date__c>([Select Id, Title__c, Release_Date_Guideline__c, Status__c, Manual_Date__c, Feed_Date__c, Projected_Date__c, Release_Date__c, EAW_Release_Date_Type__c, Manual_Not_Released_Overridden__c From EAW_Release_Date__c Where Id IN : releaseDateRecords]);
                
                for(EAW_Release_Date__c releaseDate : releaseDateRecords) {
                    //LRCC-1560 - Replace Customer lookup field with Customer text field
                    //if(releaseDate.Customer__c != null)customers.add(releaseDate.Customer__c);
                    if(releaseDate.Customers__c != null)customers.add(releaseDate.Customers__c);
                    //releaseDtTypes.add(releaseDate.Release_Date_Type__c);
                    releaseDateIds.add(releaseDate.Id); //LRCC:1096    
                }
                
                if(customers != null && customers.size()>0){
                    for(EAW_Customer__c c : [Select Id,Name from EAW_Customer__c where Name IN:customers]) {
                        customerMap.put(c.Name, c.Id);
                    }
                }
                /*if(releaseDtTypes != null && releaseDtTypes.size()>0){
for(EAW_Release_Date_Type__c r : [Select Id,Name from EAW_Release_Date_Type__c where Name IN:releaseDtTypes]) {
releaseDtTypeMap.put(r.Name, r.Id);
}
}*/
                for(EAW_Release_Date__c releaseDate : releaseDateRecords) {
                    //LRCC-1560 - Replace Customer lookup field with Customer text field
                    //releaseDate.Customer__c = customerMap.get(releaseDate.Customer__c);
                    if(customerMap.get(releaseDate.Customers__c)!=null)releaseDate.Customers__c = customerMap.get(releaseDate.Customers__c);
                    //releaseDate.Release_Date_Type__c = releaseDtTypeMap.get(releaseDate.Release_Date_Type__c);
                    if(releaseDateOldMap.containsKey(releaseDate.Id) && releaseDate.Status__c != releaseDateOldMap.get(releaseDate.Id).Status__c && releaseDate.Status__c == 'Not Released' && (releaseDateOldMap.get(releaseDate.Id).EAW_Release_Date_Type__c).startsWith('Initial')){
                        releaseDate.Manual_Not_Released_Overridden__c = true;
                    } else if(releaseDateOldMap.containsKey(releaseDate.Id) && releaseDate.Manual_Not_Released_Overridden__c && releaseDate.Status__c != releaseDateOldMap.get(releaseDate.Id).Status__c && releaseDate.Status__c != 'Not Released' && (releaseDateOldMap.get(releaseDate.Id).EAW_Release_Date_Type__c).startsWith('Initial')){
                        releaseDate.Manual_Not_Released_Overridden__c = false;
                    }
                }
                
                checkRecursiveData.bypassDateCalculation = TRUE;
                
                // LRCC-1539
                Savepoint sp = Database.setSavepoint();
                Boolean flag = false;
                List<Database.SaveResult> updateResults = database.update(releaseDateRecords,false);
                for (Integer i = 0; i < releaseDateRecords.size(); i++) {
                
                    EAW_Release_Date__c releaseDateInst = releaseDateRecords[i];
                    if(!updateResults[i].isSuccess()){
                         System.debug('releaseDateInst :::::'+releaseDateInst );
                        System.debug('updateResults[i].errror()::::::'+updateResults[i].getId());
                         Database.Error err = updateResults[i].getErrors()[0];
                         system.debug(':::err:::::'+err);
                        if(errorMsg == ''){
                            errorMsg  = err.getMessage()+' : ['+releaseDateInst.EAW_Release_Date_Type__c+'/'+releaseDateInst.Territory__c +'/'+releaseDateInst.Language__c+'/'+releaseDateInst.Product_Type__c+']'; 
                        }else{
                            errorMsg  = errorMsg+','+err.getMessage()+' : ['+releaseDateInst.EAW_Release_Date_Type__c+'/'+releaseDateInst.Territory__c +'/'+releaseDateInst.Language__c+'/'+releaseDateInst.Product_Type__c+']'; 
                        } 
                        flag = true;         
                    }
                    
                }
                System.debug('errorMsg  :::::::'+errorMsg);
                if(String.isNotBlank(errorMsg)) {
                
                    Database.rollback(sp);
                    throw new AuraHandledException(errorMsg);
                }
                system.debug('hello');
                
                //LRCC:1096 start
                
                system.debug('releaseDateIds::'+releaseDateIds);
                
                List<EAW_Release_Date_Tag_Junction__c> oldReleaseDateTagRecords = [SELECT Id, Release_Date__c, Tag__c FROM EAW_Release_Date_Tag_Junction__c WHERE Release_Date__c IN :releaseDateIds];
                system.debug('oldReleaseDateTagRecords::'+oldReleaseDateTagRecords);
                system.debug('oldReleaseDateTagRecords Size::'+oldReleaseDateTagRecords.size());
                
                for (EAW_Release_Date_Tag_Junction__c dateTagRec : oldReleaseDateTagRecords) {
                    
                    if (! oldDateTagMap.containsKey(dateTagRec.Release_Date__c)) {
                        oldDateTagMap.put(dateTagRec.Release_Date__c, new List<Id>());
                    }
                    oldDateTagMap.get(dateTagRec.Release_Date__c).add(dateTagRec.Tag__c);
                }
                system.debug('oldDateTagMap:::'+oldDateTagMap);
                
                for (EAW_Release_Date__c releaseDate : releaseDateRecords) {
                    
                    List<Id> newTags = new List<Id>();
                    List<Id> oldTags = new List<Id>();
                    
                    if (recordMap.containsKey(releaseDate.Id)) {
                        newTags = recordMap.get(releaseDate.Id);                        
                    }   
                    if (oldDateTagMap.containsKey(releaseDate.Id)) {
                        
                        oldTags = oldDateTagMap.get(releaseDate.Id);
                    }
                    system.debug('newTags::::'+newTags);
                    system.debug('oldTags::::'+oldTags);
                    
                    for (Id newTagId : newTags) {
                        if (! oldTags.contains(newTagId)) {
                            
                            EAW_Release_Date_Tag_Junction__c releaseDateTagJunction = new EAW_Release_Date_Tag_Junction__c();
                            releaseDateTagJunction.Release_Date__c = releaseDate.Id;
                            releaseDateTagJunction.Tag__c = newTagId;
                            
                            //This logic for the downstream calculation. We referred old map to get Title and Release date guideline Id from the Release date record
                            if( releaseDateOldMap.containsKey(releaseDate.Id) ) {
                                EAW_Release_Date__c tempReleaseDate = releaseDateOldMap.get(releaseDate.Id);
                                if( tempReleaseDate.Release_Date_Guideline__c != NULL ) parentRDGIdSet.add(tempReleaseDate.Release_Date_Guideline__c);
                                if( tempReleaseDate.Title__c != NULL ) titleIdSet.add(tempReleaseDate.Title__c);
                            }
                            
                            newReleaseDateTagJunctions.add(releaseDateTagJunction);
                        }
                    }
                    if (! deleteMap.containsKey(releaseDate.Id)) {
                        deleteMap.put(releaseDate.Id, new List<Id>());
                    }
                    if (newTags.size() == 0 && oldTags.size() > 0) {
                        deleteMap.get(releaseDate.Id).addAll(oldTags);
                    } else {
                        
                        for (Id oldTagId : oldTags) {
                            if (! newTags.contains(oldTagId)) {
                                system.debug('deleteIf::::');
                                deleteMap.get(releaseDate.Id).add(oldTagId);
                            }
                        }
                    }
                    
                }
                system.debug('newReleaseDateTagJunctions::'+newReleaseDateTagJunctions);
                system.debug('deleteMap::'+deleteMap);     
                                
                for (EAW_Release_Date_Tag_Junction__c dateTagRec : [SELECT Id, Release_Date__c, Release_Date__r.Title__c, Release_Date__r.Release_Date_Guideline__c, Tag__c FROM EAW_Release_Date_Tag_Junction__c WHERE Release_Date__c IN : deleteMap.keySet()]) {
                    if (deleteMap.containsKey(dateTagRec.Release_Date__c)) {
                        List<Id> tagList = deleteMap.get(dateTagRec.Release_Date__c);
                        if (tagList.contains(dateTagRec.Tag__c)) {
                            deleteList.add(dateTagRec);
                            if( dateTagRec.Release_Date__r.Release_Date_Guideline__c != NULL ) parentRDGIdSet.add(dateTagRec.Release_Date__r.Release_Date_Guideline__c);
                            if( dateTagRec.Release_Date__r.Title__c != NULL ) titleIdSet.add(dateTagRec.Release_Date__r.Title__c);
                        }                        
                    }
                }
                
                if (newReleaseDateTagJunctions.size() > 0) {
                    system.debug('upsert...'+newReleaseDateTagJunctions);
                    upsert newReleaseDateTagJunctions;
                }
                
                if (deleteList.size() > 0) {
                    
                    system.debug('deleteList ::: '+deleteList);
                    system.debug('deleteListSize::'+deleteList.size());
                    delete deleteList;
                }
                // LRCC:1096 end
                
                //Date calculation batch processing logic for downstream
                Map<Id, EAW_Release_Date__c> releaseDateNewMap = new Map<Id, EAW_Release_Date__c>([Select Id, Title__c, Status__c, Manual_Date__c, Feed_Date__c, Projected_Date__c, Release_Date__c, Release_Date_Guideline__c From EAW_Release_Date__c Where Id IN : releaseDateRecords]);
                
                if( releaseDateNewMap != NULL && releaseDateNewMap.size() > 0 ) {
                
                    Set<Id> reCalculateSelfRDGIdsSet = new Set<Id>();
                    
                    for( Id releaseDateId : releaseDateNewMap.keySet() ){
                        
                        EAW_Release_Date__c newRD = releaseDateNewMap.get(releaseDateId);
                        EAW_Release_Date__c oldRD = releaseDateOldMap.get(releaseDateId);
                        
                        if( oldRD.Release_Date__c != newRD.Release_Date__c || oldRD.Title__c != newRD.Title__c || oldRD.Status__c != newRD.Status__c ){
                            if( newRD.Release_Date_Guideline__c != NULL ) parentRDGIdSet.add(newRD.Release_Date_Guideline__c);
                            if( newRD.Title__c != NULL ) titleIdSet.add(newRD.Title__c);
                            releaseDateIdSet.add(newRD.Id);
                            if( oldRD.Status__c != newRD.Status__c && newRD.Status__c != 'Firm' && newRD.Status__c != 'Not Released' && (oldRD.Status__c == 'Firm' || oldRD.Status__c == 'Not Released' )){     // LRCC - 1444
                                if( newRD.Release_Date_Guideline__c != NULL ) reCalculateSelfRDGIdsSet.add(newRD.Release_Date_Guideline__c);
                            }
                        }
                    }
                    
                    //We have to call the date calculation for the same RDG since the Tag's are getting inserted or deleted
                    if( parentRDGIdSet != NULL && parentRDGIdSet.size() > 0 ){
                        
                        
                        EAW_DownstreamRuleGuidelinesFindUtil dependentGuidelineFindUtil = new EAW_DownstreamRuleGuidelinesFindUtil();
                        Set<Id>  rdgIdSet = dependentGuidelineFindUtil.findDependentReleaseDateGuidelineToProcess(parentRdgIdSet);   // Uncommented for LRCC - 1444
                        Set<Id> windowGLIdSet = dependentGuidelineFindUtil.findDependentWindowGuidelineToProcess(parentRdgIdSet);
                        
                        if(rdgIdSet != null && rdgIdSet.size() > 0)reCalculateSelfRDGIdsSet.addAll(rdgIdSet);   // LRCC - 1444
                        
                        if(reCalculateSelfRDGIdsSet != null && reCalculateSelfRDGIdsSet.size() > 0){
                        
                            EAW_RDGDateCalculationBatchClass releaseDateBatch = new EAW_RDGDateCalculationBatchClass(reCalculateSelfRDGIdsSet);
                            releaseDateBatch.titleIdSet = titleIdSet;
                            releaseDateBatch.windowGuidelineIdSet = windowGLIdSet;
                            releaseDateBatch.disQualifyFlag = true;
                            ID batchprocessid = Database.executeBatch(releaseDateBatch, 200);
                       }
                        
                    } /* else if( releaseDateIdSet != NULL && releaseDateIdSet.size() > 0 ){
                        EAW_RDPlanQualifierBatchClass planQualifierBatch = new EAW_RDPlanQualifierBatchClass();
                        planQualifierBatch.releaseDateIdSet  = releaseDateIdSet;
                        planQualifierBatch.windowGuidelineIdSet = windowGLIdSet;
                        planQualifierBatch.releaseDateGuidelineIdSet = parentRDGIdSet;
                        planQualifierBatch.titleIdSet = titleIdSet;
                        ID batchprocessid = Database.executeBatch(planQualifierBatch, 200);
                    }*/
                }
            }
            
            return releaseDateRecords;
        }
        catch(Exception e) {
             //LRCC-1539
            if(String.isNotBlank(errorMsg)) {
            
            
                throw new AuraHandledException(errorMsg);
            }else{
            
                throw new AuraHandledException('Error : ' + e.getMessage());
            }
        }
    }
    //LRCC:1010
    @AuraEnabled
    public static void insertNoteRecords(List<Id> selectedIds, String notesTitle, String notesBody,String notesAction) {
        List<Note> notes = new List<Note>();
        String releaseDateQuery = 'SELECT Status__c, Temp_Perm__c, Manual_Date__c FROM EAW_Release_Date__c WHERE id IN :selectedIds';
        List<EAW_Release_Date__c> releaseDates = Database.query(releaseDateQuery); 
        
        for(EAW_Release_Date__c releaseDate : releaseDates) {
            Note note = new Note();
            note.ParentId = releaseDate.Id;
            note.Title = notesTitle;
            if(notesBody != null && notesBody != '') {
                note.Body = notesBody;
            }
            notes.add(note);            
        }
        List<Note> allNotes = [SELECT Body,Id,ParentId,Title FROM Note WHERE ParentId IN: selectedIds];
        if('append'.equals(notesAction)){
        
            if (notes != null && notes.size() > 0) {
                insert notes; 
            }  
        
        }else if('replace'.equals(notesAction)){
        
            if(allNotes.size() > 0){
            
                delete allNotes;
            }
            if (notes != null && notes.size() > 0) {
                insert notes; 
            } 
        }else if('clear'.equals(notesAction)){
        
            if(allNotes.size() > 0){
            
                delete allNotes;
            }
        }
    }    
}