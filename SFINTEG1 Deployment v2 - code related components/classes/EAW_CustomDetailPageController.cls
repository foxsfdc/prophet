public with sharing class EAW_CustomDetailPageController {
    
    @AuraEnabled
    public static List<Object> getFields(Map<String,String> objFieldSetMap) {
    
        system.debug('objFieldSetMap;;;;'+objFieldSetMap);
    
        List<Object>fieldNames = new List<Object>();
        for(String strObjectName: objFieldSetMap.keySet())
        {
            fieldNames.add(EAW_FieldSetController.getFields(strObjectName, objFieldSetMap.get(strObjectName)));
        }
        return fieldNames;
    }    
}