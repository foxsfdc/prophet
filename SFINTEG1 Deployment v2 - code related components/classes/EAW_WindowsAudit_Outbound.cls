//LRCC-1708
public with sharing class EAW_WindowsAudit_Outbound {

	List<EAW_Error_Log__c> repoErrorLogs  = new List<EAW_Error_Log__c>();
	set<id> repoSuccessIds= new set<id>();
	Set<id> repoIgnoredIds = new Set<id>();
	
	String repoUrl = '';
	
	public EAW_WindowsAudit_Outbound(){
		checkOuboundRecursiveData.bypassTriggerFire=true;
		checkRecursiveData.bypassDateCalculation = true;
		Rest_Endpoint_Settings__c repoInstance = Rest_Endpoint_Settings__c.getInstance('Outbound-W-Hist-to-Repo');
		if(repoInstance!=null){
			repoUrl=repoInstance.End_Point__c;
		}		
	}
	
    public class WindowAuditJson{
    	public String windowId;
    	public String windowName;
    	public String fieldName;
    	public String newValue;
    	public String oldValue;
    	public String updatedBy;
    	public String updateDate;
    	public String operation;
    	
    	public WindowAuditJson(String windowId,String windowName, String fieldName,String newValue,
							    	String oldValue,String updatedBy,String updateDate,String operation){
    		this.windowId = windowId;
    		this.windowName = windowName;
    		this.fieldName = fieldName;
    		this.newValue = newValue;
    		this.oldValue = oldValue;
    		this.updatedBy = updatedBy;
    		this.operation = operation;
    	}
	}
    private void getWindowAuditJson(EAW_Outbound_Utility.OutboundSystem os, List<EAW_Window__c> windowList){
    	try{
	    	String operation = 'Insert';
			datetime historyDate = datetime.now();
			List<EAW_Window__c> updateList = new List<EAW_Window__c>();
			
	    	List<List<WindowAuditJson>> wJsonListOfList = new List<List<WindowAuditJson>>();
	    	List<List<id>> wIdListOfList = new List<List<id>>();
	    	
	    	List<WindowAuditJson> wJsonList = new List<WindowAuditJson>();
			List<id> wIdList = new List<id>();
	    	for(EAW_Window__c w:windowList){	    		
				List<EAW_Window__History> waList = w.Histories;
	    		for(EAW_Window__History wa:waList){
					if(w.History_Sent_Date__c == null || (wa.CreatedDate > w.History_Sent_Date__c)){
						wJsonList.add(new WindowAuditJson(''+w.id,w.name,wa.Field,String.valueOf(wa.NewValue),String.valueOf(wa.OldValue),wa.CreatedBy.Name,EAW_Outbound_Utility.getFormattedDateTime(wa.CreatedDate),operation));
						wIdList.add(w.id);
						if(wJsonList.size()==100){
							wJsonListOfList.add(wJsonList);
							wIdListOfList.add(wIdList);
							wJsonList = new List<WindowAuditJson>();
							wIdList = new List<id>();
						}
					}
				}
    		}
    		if(wJsonList!=null && !wJsonList.isEmpty()){
    			wJsonListOfList.add(wJsonList);
				wIdListOfList.add(wIdList);
    		}
    		
    		if(wJsonListOfList!=null && !wJsonListOfList.isempty()){
    			for(integer i=0;i<wJsonListOfList.size();i++){
    				List<WindowAuditJson> tempWJsonList = wJsonListOfList[i];
					List<id> tempIdList = wIdListOfList[i];
    			    EAW_Outbound_Utility eouRepo = new EAW_Outbound_Utility(EAW_Outbound_Utility.OutboundDataType.WindowAuditInformation,os,tempIdList);
    				if(os==EAW_Outbound_Utility.OutboundSystem.Repo){
					   	eouRepo.sendDataThroughWebAPI( system.json.serialize(tempWJsonList),repoUrl,repoErrorLogs,repoSuccessIds);
		    		}
    			}
    			for(EAW_Window__c w:windowList){
    				w.History_Sent_Date__c = historyDate;
    				updateList.add(w);
    			}
    			update updateList;
    		}
    		
    	}catch(exception e){
    		system.debug('EAW_WindowAudit_Outbound Exception: '+e.getmessage());
    	}
    }
    
    
    public void aggregateAllWs(List<EAW_Window__c> records){
    	List<EAW_Window__c> repoRecords = new List<EAW_Window__c>();
    	    	
    	List<EAW_Window__c> updateList = new List<EAW_Window__c>();
    	List<EAW_Window__c> deleteList = new List<EAW_Window__c>();
    	List<EAW_Error_Log__c> insertErrorList = new List<EAW_Error_Log__c>();
    	
    	if(records!=null && !records.isempty()){
    		for(EAW_Window__c w:records){
				if(w.Sent_To_Repo__c){
    				repoRecords.add(w);
				}				
			}
			if(!repoRecords.isEmpty()){
				getWindowAuditJson(EAW_Outbound_Utility.OutboundSystem.Repo,repoRecords);
			}
			insertErrorList.addAll(repoErrorLogs);
		}
		
		if(insertErrorList!=null && !insertErrorList.isEmpty()){
            upsert insertErrorList;
        }  	
    }
    
    /*
    @future(callout=true)
    public static void sendDataAsync(List<EAW_Window__c> windows){
    	EAW_WindowsAudit_Outbound ewo = new EAW_WindowsAudit_Outbound();
    	ewo.sendData(windows);
    }
    
    public static void sendDataSync(List<EAW_Window__c> windows){
    	EAW_WindowsAudit_Outbound ewo = new EAW_WindowsAudit_Outbound();
    	ewo.sendData(windows);
    }*/
	
	/*private void sendData(Set<Id> windowIds){
    	List<EAW_Window__c> windows = [select id,name,History_Sent_Date__c, Sent_To_Repo__c, 
    									(Select Field, OldValue, NewValue, CreatedBy.Name,CreatedDate From Histories order by CreatedDate desc) 
    									from EAW_Window__c where id in: windowIds and Soft_Deleted__c <> 'TRUE'];
    	aggregateAllWs(windows);
    }*/
    
    public void sendData(List<EAW_Window__c> windows){
    	aggregateAllWs(windows);
    }
    
}