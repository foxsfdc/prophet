@isTest
public class EAW_TitlePlanManagement_Test {

    @isTest static void getFields(){
        
        EAW_TitlePlanManagement.getFields('EAW_Tag__c','');
    }
    
    @isTest static void searchTitleByName_Test() {
        
        String TestTitle;
        
        EAW_TestDataFactory.createEAWTitle(1, true);
        List<EAW_Title__c> titleList = EAW_TitlePlanManagement.searchTitleByName('TestTitle');
        
        System.assertEquals(true, titleList != null);
    }
}