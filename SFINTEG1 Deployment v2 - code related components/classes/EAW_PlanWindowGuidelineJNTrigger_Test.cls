@isTest
public class EAW_PlanWindowGuidelineJNTrigger_Test{ 
 
    /*@isTest static void createEAWWindows_Test(){
        
        List<EAW_Window_Guideline__c> windowGuideLineList = new List<EAW_Window_Guideline__c> {
            
            new EAW_Window_Guideline__c(Status__c  = 'Draft')
        };
        insert windowGuideLineList;   
        
        List<EAW_Window_Guideline_Strand__c> windowGuideLineStrandList = new List<EAW_Window_Guideline_Strand__c> {
            
            new EAW_Window_Guideline_Strand__c(License_Type__c = 'Exhibition License', EAW_Window_Guideline__c = windowGuideLineList[0].Id,
            Media__c = 'Theatrical', Territory__c = 'Abu Dhabi', Language__c = 'Afrikaans')
        };    
        insert windowGuideLineStrandList ; 
        
        EAW_Window__c winowIns = new EAW_Window__c();
        winowIns.EAW_Window_Guideline__c = windowGuideLineList[0].Id;        
        insert winowIns;
        
        List<EAW_Plan_Guideline__c> planGuideLineList = new List<EAW_Plan_Guideline__c> {
            
            new EAW_Plan_Guideline__c(Status__c = 'Draft')
        };
        insert planGuideLineList ;
        
        List<EAW_Plan_Window_Guideline_Junction__c> planWindowList = new List<EAW_Plan_Window_Guideline_Junction__c> {
            new EAW_Plan_Window_Guideline_Junction__c(Plan__c = planGuideLineList[0].Id , Window_Guideline__c = windowGuideLineList[0].Id)
        };
        insert planWindowList;
        
        windowGuideLineList[0].Status__c = 'Active';
        update windowGuideLineList;
        
        planGuideLineList[0].Status__c = 'Active';
        update planGuideLineList;
        
    } */      
    
    @testSetup static void setupTestData() {
    
        List<EAW_Release_Date_Guideline__c> rdgList =  EAW_TestDataFactory.createReleaseDateGuideline(1, TRUE, 'Feature', new List<String>{'Theatrical'}, 'United States +', 'English');
              
        List<EAW_Title__c> titleList = EAW_TestDataFactory.createEAWTitle(2, FALSE);
        titleList[0].RLSE_CAL_YR__c = '2019';
        titleList[1].RLSE_CAL_YR__c = '2019';
        insert titleList;
        
        List<EAW_Release_Date__c> releaseDateList = EAW_TestDataFactory.createReleaseDate(4, FALSE);
              
        releaseDateList[0].Release_Date_Guideline__c = rdgList[0].Id;
        releaseDateList[0].Title__c = titleList[0].Id;
        releaseDateList[0].Manual_Date__c = System.Today();
        releaseDateList[0].Temp_Perm__c = 'Temporary';
        
        releaseDateList[1].Release_Date_Guideline__c = rdgList[0].Id;
        releaseDateList[1].Title__c = titleList[1].Id;
        releaseDateList[1].Manual_Date__c = System.Today();
        releaseDateList[1].Temp_Perm__c = 'Temporary';
        
        insert releaseDateList;
        
        List<EAW_Window_Guideline__c> wgList = EAW_TestDataFactory.createWindowGuideLine(2, TRUE);
        
        List<EAW_Window_Guideline_Strand__c> wgStrandList = EAW_TestDataFactory.createWindowGuideLineStrand(2, FALSE);
        
        wgStrandList[0].EAW_Window_Guideline__c = wgList[0].Id;
        wgStrandList[1].EAW_Window_Guideline__c = wgList[1].Id;
        
        insert wgStrandList;
        
        String ruleJson = '{"sObjectType":"EAW_Rule__c","Nested__c":false}';
        String ruleDetailJson = '[{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Conditional_Operand_Id__c":"aA73D000000CyJjSAK","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature", "Condition_Date_Duration__c":"Month","Condition_Criteria_Amount__c":"1"},"dateCalcs":[],"nodeCalcs":[]}]';
        EAW_TestRuleDataFactory.saveRuleAndRuleDetails(ruleJson, ruleDetailJson, FALSE ,wgList[0].Id, 'Start Date',rdgList[0].Id); // Parameters: (String ruleAndRuleDetailNodes,Boolean isRDG,String guidelineId,String wgRuleDate,String conditionalOperandId);
        EAW_TestRuleDataFactory.saveRuleAndRuleDetails(ruleJson, ruleDetailJson, FALSE ,wgList[0].Id, 'End Date',rdgList[0].Id);
        EAW_TestRuleDataFactory.saveRuleAndRuleDetails(ruleJson, ruleDetailJson, FALSE ,wgList[0].Id, 'Outside Date',rdgList[0].Id);
        EAW_TestRuleDataFactory.saveRuleAndRuleDetails(ruleJson, ruleDetailJson, FALSE ,wgList[0].Id, 'Tracking Date',rdgList[0].Id);
        
        wgList[0].Status__c = 'Active';
        wgList[1].Status__c = 'Active';
        update wgList;
        
        List<EAW_Plan_Guideline__c> pgList = EAW_TestDataFactory.createPlanGuideline(1,FALSE);
        pgList[0].Product_Type__c  = 'Feature';
        insert pgList;
        
        String ruleDetailJson1 = '{"sObjectType": "EAW_Rule_Detail__c", "Condition_Operator__c": ">", "Condition_Year__c": "2000", "Condition_Field__c": "Release Year"}';
        String ruleJson1 = '{"sObjectType": "EAW_Rule__c"}';
        EAW_TestRuleDataFactory.savePgQualifiers(ruleJson1,ruleDetailJson1,pgList[0].Id);
        
        List<EAW_Plan_Window_Guideline_Junction__c> planWindowGuidelineJunctionList = EAW_TestDataFactory.createPlanWindowGuideLineJunction ( 2, FALSE);
        
        planWindowGuidelineJunctionList[0].Plan__c = pgList[0].Id;
        planWindowGuidelineJunctionList[0].Window_Guideline__c = wgList[0].Id;
        
        planWindowGuidelineJunctionList[1].Plan__c = pgList[0].Id;
        planWindowGuidelineJunctionList[1].Window_Guideline__c = wgList[1].Id;
        
        insert planWindowGuidelineJunctionList;
    }
    
    @isTest static void planWindowGuidelineJNTriggerTest1() {
        
        List<EAW_Plan_Guideline__c> pgList = [ Select Id, Status__c From EAW_Plan_Guideline__c ];
        pgList[0].Status__c = 'Active';
        update pgList;
        
        List<EAW_Plan_Window_Guideline_Junction__c> pgJunList = [ Select Id From EAW_Plan_Window_Guideline_Junction__c limit 1];
        delete pgJunList;
    }
}