/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class EAW_EAWBoxOfficeTrigger_Test {

   @testSetup static void setup() {
        //1250-Replace Title EDM with Title Attribute
       EAW_TestDataFactory.createEAWTitle(1, true);
    }
    @isTest static void processTVDBoxOfficeToQualifyTitleForInsert() {
    
        RecordType recType = [Select Id from RecordType WHERE Name='Qualifier' AND SObjectType = 'EAW_Rule__c'];
        
        RecordType recTypeDetail = [Select Id from RecordType WHERE Name='Qualifier' AND SObjectType = 'EAW_Rule_Detail__c'];
        
        EDM_REF_PRODUCT_TYPE__c refProduct = new EDM_REF_PRODUCT_TYPE__c(Name = 'Compilation Episode');
        insert refProduct;
    
        List<EAW_Plan_Guideline__c> planGuideLineList = new List<EAW_Plan_Guideline__c> {
            
            new EAW_Plan_Guideline__c(Product_Type__c = 'Feature', Name = 'Test plan Guideline')
        };
        insert planGuideLineList;
        
        EAW_Window_Guideline__c windowGuidelineList = new EAW_Window_Guideline__c(Window_Guideline_Alias__c ='Test WG_001', Status__c = 'Draft', Window_Type__c = 'First-Run', Product_Type__c = 'Feature');
        insert windowGuidelineList;
        
         List<EAW_Window_Guideline_Strand__c> windowGuideLineStrandList = new List<EAW_Window_Guideline_Strand__c> {
            
            new EAW_Window_Guideline_Strand__c(License_Type__c = 'Exhibition License', EAW_Window_Guideline__c = windowGuideLineList.Id,
            Media__c = 'Theatrical', Territory__c = 'Afghanistan', Language__c = 'Afrikaans')
        };    
        insert windowGuideLineStrandList ; 
        
        List<EAW_Plan_Window_Guideline_Junction__c> planWindowList = new List<EAW_Plan_Window_Guideline_Junction__c> {
            new EAW_Plan_Window_Guideline_Junction__c(Plan__c = planGuideLineList[0].Id , Window_Guideline__c = windowGuideLineList.Id)
        };
        insert planWindowList;
        
        windowGuidelineList.Status__c = 'Active';
        update windowGuidelineList;
         
        planGuideLineList[0].Status__c = 'Active';
        update planGuideLineList;
    
        EAW_Rule__c ruleInstance = new EAW_Rule__c(RecordTypeId = recType.Id, Plan_Guideline__c = planGuideLineList[0].Id);
        insert ruleInstance;
        
        EAW_Rule_Detail__c ruledetailList = new EAW_Rule_Detail__c(RecordTypeId = recTypeDetail.Id, Rule_No__c = ruleInstance.Id, Condition_Field__c = 'US Admissions', Condition_Tag__c = 'Blockbuster',
        Condition_Operator__c = '<', Condition_Criteria_Amount__c = 2);
        insert ruledetailList;
        
        //List<EDM_GLOBAL_TITLE__c> edmTitleList = [SELECT Id,Name FROM EDM_GLOBAL_TITLE__c LIMIT 1];
        List<EAW_Title__c> eawTitleList = [SELECT Id,Name FROM EAW_Title__c LIMIT 1];
        
        eawTitleList[0].PROD_TYP_CD_INTL__c = refProduct.id;
        update eawTitleList;
        
        EAW_Window__c winowIns = new EAW_Window__c();
        winowIns.EAW_Window_Guideline__c = windowGuideLineList.Id;
        winowIns.EAW_Title_Attribute__c = eawTitleList[0].Id;   
        winowIns.Window_Type__c = 'Current';     
        insert winowIns;
        
        //LRCC-1510 - Converting Name field From Text to Auto Number, before that remove the references.
        EAW_Box_Office__c boxOfficeInstance = new EAW_Box_Office__c(Title_Attribute__c = eawTitleList[0].Id, Territory__c = 'India', TVD_Local_Admissions_Count__c = 1);
        insert boxOfficeInstance;
    }
    
    @isTest static void processTVDBoxOfficeToQualifyTitleForTerritory() {
    
        RecordType recType = [Select Id from RecordType WHERE Name='Qualifier' AND SObjectType = 'EAW_Rule__c'];
        
        RecordType recTypeDetail = [Select Id from RecordType WHERE Name='Qualifier' AND SObjectType = 'EAW_Rule_Detail__c'];
        
        EDM_REF_PRODUCT_TYPE__c refProduct = new EDM_REF_PRODUCT_TYPE__c(Name = 'Compilation Episode');
        insert refProduct;
    
        List<EAW_Plan_Guideline__c> planGuideLineList = new List<EAW_Plan_Guideline__c> {
            
            new EAW_Plan_Guideline__c(Product_Type__c = 'Feature', Name = 'Test plan Guideline')
        };
        insert planGuideLineList;
        
        EAW_Window_Guideline__c windowGuidelineList = new EAW_Window_Guideline__c(Window_Guideline_Alias__c ='Test WG_001',  Status__c = 'Draft', Window_Type__c = 'First-Run', Product_Type__c = 'Feature');
        insert windowGuidelineList;
        
         List<EAW_Window_Guideline_Strand__c> windowGuideLineStrandList = new List<EAW_Window_Guideline_Strand__c> {
            
            new EAW_Window_Guideline_Strand__c(License_Type__c = 'Exhibition License', EAW_Window_Guideline__c = windowGuideLineList.Id,
            Media__c = 'Theatrical', Territory__c = 'Afghanistan', Language__c = 'Afrikaans')
        };    
        insert windowGuideLineStrandList ; 
        
        List<EAW_Plan_Window_Guideline_Junction__c> planWindowList = new List<EAW_Plan_Window_Guideline_Junction__c> {
            new EAW_Plan_Window_Guideline_Junction__c(Plan__c = planGuideLineList[0].Id , Window_Guideline__c = windowGuideLineList.Id)
        };
        insert planWindowList;
        
        windowGuidelineList.Status__c = 'Active';
        update windowGuidelineList;
         
        planGuideLineList[0].Status__c = 'Active';
        update planGuideLineList;
    
        EAW_Rule__c ruleInstance = new EAW_Rule__c(RecordTypeId = recType.Id, Plan_Guideline__c = planGuideLineList[0].Id);
        insert ruleInstance;
        
        EAW_Rule_Detail__c ruledetailList = new EAW_Rule_Detail__c(RecordTypeId = recTypeDetail.Id, Rule_No__c = ruleInstance.Id, Condition_Field__c = 'US Admissions', Condition_Tag__c = 'Blockbuster',
        Condition_Operator__c = '<', Condition_Criteria_Amount__c = 2,Territory__c = 'Afghanistan');
        insert ruledetailList;
        
        //List<EDM_GLOBAL_TITLE__c> edmTitleList = [SELECT Id,Name FROM EDM_GLOBAL_TITLE__c LIMIT 1];
        List<EAW_Title__c> eawTitleList = [SELECT Id,Name FROM EAW_Title__c LIMIT 1];
        
        eawTitleList[0].PROD_TYP_CD_INTL__c = refProduct.id;
        update eawTitleList;
        
        
        EAW_Window__c winowIns = new EAW_Window__c();
        winowIns.EAW_Window_Guideline__c = windowGuideLineList.Id;
        winowIns.EAW_Title_Attribute__c = eawTitleList[0].Id;   
        winowIns.Window_Type__c = 'Current';     
        insert winowIns;
        
        //LRCC-1510 - Converting Name field From Text to Auto Number, before that remove the references.
        EAW_Box_Office__c boxOfficeInstance = new EAW_Box_Office__c(Title_Attribute__c = eawTitleList[0].Id, Territory__c = 'Afghanistan', TVD_Local_Admissions_Count__c = 1);
        insert boxOfficeInstance;
    }
    
    @isTest static void processTVDBoxOfficeToQualifyTitleForUpdate() {
    
        RecordType recType = [Select Id from RecordType WHERE Name='Qualifier' AND SObjectType = 'EAW_Rule__c'];
        
        RecordType recTypeDetail = [Select Id from RecordType WHERE Name='Qualifier' AND SObjectType = 'EAW_Rule_Detail__c'];
        
        EDM_REF_PRODUCT_TYPE__c refProduct = new EDM_REF_PRODUCT_TYPE__c(Name = 'Compilation Episode');
        insert refProduct;
    
        List<EAW_Plan_Guideline__c> planGuideLineList = new List<EAW_Plan_Guideline__c> {
            
            new EAW_Plan_Guideline__c(Product_Type__c = 'Feature', Name = 'Test plan Guideline')
        };
        insert planGuideLineList;
        
        EAW_Window_Guideline__c windowGuidelineList = new EAW_Window_Guideline__c(Window_Guideline_Alias__c ='Test WG_001', Status__c = 'Draft', Window_Type__c = 'Library', Product_Type__c = 'Feature');
        insert windowGuidelineList;
        
         List<EAW_Window_Guideline_Strand__c> windowGuideLineStrandList = new List<EAW_Window_Guideline_Strand__c> {
            
            new EAW_Window_Guideline_Strand__c(License_Type__c = 'Exhibition License', EAW_Window_Guideline__c = windowGuideLineList.Id,
            Media__c = 'Theatrical', Territory__c = 'Afghanistan', Language__c = 'Afrikaans')
        };    
        insert windowGuideLineStrandList ; 
        
        List<EAW_Plan_Window_Guideline_Junction__c> planWindowList = new List<EAW_Plan_Window_Guideline_Junction__c> {
            new EAW_Plan_Window_Guideline_Junction__c(Plan__c = planGuideLineList[0].Id , Window_Guideline__c = windowGuideLineList.Id)
        };
        insert planWindowList;
        
        windowGuidelineList.Status__c = 'Active';
        update windowGuidelineList;
         
        planGuideLineList[0].Status__c = 'Active';
        update planGuideLineList;
    
        EAW_Rule__c ruleInstance = new EAW_Rule__c(RecordTypeId = recType.Id, Plan_Guideline__c = planGuideLineList[0].Id);
        insert ruleInstance;
        
        EAW_Rule_Detail__c ruledetailList = new EAW_Rule_Detail__c(RecordTypeId = recTypeDetail.Id, Rule_No__c = ruleInstance.Id, Condition_Field__c = 'US Admissions', Condition_Tag__c = 'Blockbuster',
        Condition_Operator__c = '<', Condition_Criteria_Amount__c = 2,Territory__c = 'Afghanistan');
        insert ruledetailList;
        
        //List<EDM_GLOBAL_TITLE__c> edmTitleList = [SELECT Id,Name FROM EDM_GLOBAL_TITLE__c LIMIT 1];
        List<EAW_Title__c> eawTitleList = [SELECT Id,Name FROM EAW_Title__c LIMIT 1];
        
        EAW_Window__c winowIns = new EAW_Window__c();
        winowIns.EAW_Window_Guideline__c = windowGuideLineList.Id;
        winowIns.EAW_Title_Attribute__c = eawTitleList[0].Id;   
        winowIns.Window_Type__c = 'Current';     
        insert winowIns;
        
        eawTitleList[0].PROD_TYP_CD_INTL__c = refProduct.id;
        update eawTitleList;
        //LRCC-1510 - Converting Name field From Text to Auto Number, before that remove the references.
        EAW_Box_Office__c boxOfficeInstance = new EAW_Box_Office__c(Title_Attribute__c = eawTitleList[0].Id, Territory__c = 'Afghanistan', TVD_Local_Admissions_Count__c = 1);
        insert boxOfficeInstance;
        
        boxOfficeInstance.Territory__c = 'Antigua';
        update boxOfficeInstance;
    }
}