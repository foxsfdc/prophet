public with sharing class EAW_ReleaseDateAutoFirmController implements Schedulable {

    public void execute(SchedulableContext ctx) {
    
       calculateAutoFirm();
    }   

    public void calculateAutoFirm() {
    
        Set<String> releaseDateTypesName = new Set<String> ();
        Map<String,EAW_Release_Date_Type__c> releaseDateTypeNameWithInst = new Map<String,EAW_Release_Date_Type__c>();
        List<EAW_Release_Date__c> releaseDateList = new  List<EAW_Release_Date__c>();
        Date maxLastDate;
        Date Lastday;
        String query = 'SELECT Id,Name,Release_Date__c,Status__c,EAW_Release_Date_Type__c FROM EAW_Release_Date__c WHERE Status__c != \'Firm\' AND Release_Date__c != NULL AND (';
            
        
        for(EAW_Release_Date_Type__c rd : [SELECT Auto_Firm__c,Days_To_Auto_Firm_Period__c,Id,Months_To_Auto_Firm_Period__c,Name,Source_Type__c,Source__c 
                                           FROM EAW_Release_Date_Type__c
                                           WHERE Auto_Firm__c = true 
                                           AND (Days_To_Auto_Firm_Period__c != null OR Months_To_Auto_Firm_Period__c != null)]) {
            
            if(rd.Auto_Firm__c == true) {
            
                Date currentDate = date.today();
                
                if(rd.Months_To_Auto_Firm_Period__c != NULL) {
                
                    currentDate  = currentDate.addMonths(Integer.valueOf(rd.Months_To_Auto_Firm_Period__c));        
                }
                if(rd.Days_To_Auto_Firm_Period__c != NULL) {
                
                    currentDate  = currentDate.addDays(Integer.valueOf(rd.Days_To_Auto_Firm_Period__c));
                }
                Lastday = currentDate.addMonths(1).toStartofMonth().addDays(-1);
                 
                if(!releaseDateTypesName.contains(rd.Name)) {
                
                    if(releaseDateTypesName.size() == 0){
                    
                        query =query+'((EAW_Release_Date_Type__c =\''+ rd.Name +'\' AND Release_Date__c < :lastday) OR (EAW_Release_Date_Type__c =\''+ rd.Name +'\' AND Release_Date__c = :lastday))'; 
                    }else {
                    
                        query =query+' OR ((EAW_Release_Date_Type__c =\''+ rd.Name +'\' AND Release_Date__c < :lastday)OR (EAW_Release_Date_Type__c =\''+ rd.Name +'\' AND Release_Date__c = :lastday))';
                    }
                }
                releaseDateTypesName.add(rd.Name);
                releaseDateTypeNameWithInst.put(rd.Name,rd);
            }
        }
        query =query+')';
        System.debug('query ::sche:::'+query );
        System.debug('maxLastDate ::sche:::'+maxLastDate );
        System.debug('releaseDateTypesName::sche:::'+releaseDateTypesName);
        if(releaseDateTypesName!= null && releaseDateTypesName.size() > 0) {
        
            EAW_AutoFirmBatchController eafbc= new EAW_AutoFirmBatchController (query,Lastday);
            Integer bcount = Integer.valueOf(Label.EAW_AutoFirmBatchCount);
            System.debug('::::::bcount ::auto::'+bcount);
            database.executeBatch(eafbc,bcount);
            
            /*for(EAW_Release_Date__c releaseDateInst: DataBase.query(query)){
                
                System.debug('Release_Date__c:::sche::'+releaseDateInst);
                releaseDateInst.Status__c = 'Firm'; 
                releaseDateList.add(releaseDateInst); 
            }
            System.debug('releaseDateList::::scc:::'+releaseDateList);
            if(releaseDateList != NULL && releaseDateList.size() > 0) {
            
                update releaseDateList;
            }*/
        }
    }
}