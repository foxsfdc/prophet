@isTest(SeeAllData = true)
public class EAW_GenericReceiverClass_Test {

    @isTest static void testEAWPlanSearch(){
        
        List<EAW_Window__c> windowlist = EAW_TestDataFactory.createWindow (1, True);
        List<EAW_GenericReceiverClass.windowWrapper> genericReceiverClass = new List<EAW_GenericReceiverClass.windowWrapper>();
        
        EAW_GenericReceiverClass.windowWrapper windowListWrapper = new EAW_GenericReceiverClass.windowWrapper();
        windowListWrapper.windowId = windowlist[0].Id;
        
        genericReceiverClass.add(windowListWrapper);
        
        Test.startTest();
        EAW_GenericReceiverClass.updateWindows();
        Test.stopTest();
        
        List<EAW_Window__c> windowListQueried = [SELECT Id, Name, Status__c FROM EAW_Window__c];
        //System.assertEquals('Estimated', windowListQueried[0].Status__c );
    }
}