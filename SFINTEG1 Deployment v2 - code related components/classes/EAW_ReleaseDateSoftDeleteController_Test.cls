@isTest
private class EAW_ReleaseDateSoftDeleteController_Test {
    @isTest 
    static void EAW_ReleaseDateSoftDeleteController_Test(){
    
        List<EAW_Release_Date_Guideline__c> rdgList = EAW_TestDataFactory.createReleaseDateGuideline (1,true, 'Direct to Video', new List<String>{'EST-HD'},'Afghanistan','Afrikaans');
        List<EAW_Release_Date__c> rdList = EAW_TestDataFactory.createReleaseDate(1,false);
        rdList[0].Release_Date_Guideline__c = rdgList[0].Id;
        rdList[0].Feed_Date__c = system.today();
        rdList[0].Status__c= 'Firm';
        insert rdList;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(rdList[0]);
        EAW_ReleaseDateSoftDeleteController rdsSoftDelete = new EAW_ReleaseDateSoftDeleteController(sc);
    }
}