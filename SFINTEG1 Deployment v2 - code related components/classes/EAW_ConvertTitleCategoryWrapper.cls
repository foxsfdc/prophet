public class EAW_ConvertTitleCategoryWrapper {

    public List<Data> data; 
    public Links_Z links;
    
	public class Attributes {
	    public String type; // in json: type
	    public String foxId ; 
	    public Integer categoryId;
	    public Integer rowIdObject ; 
	    public String categoryTypeCode ;
	    public String categoryTypeDescription;
	    public String categoryCode;
	    public String categoryDescription;
	    public Integer categorySortOrder;
	    public String primaryTitleIndicator;
	    public Integer foxVersionId ; 
		public String countryDescription ;
		public String countryCode ; 
		public String mediaDescription ; 
		public String mediaCode ;
	    public String titleVersionTypeCode ; 
	    public String titleVersionTypeDescription ; 
	    public String titleVersionDescription ;
	}


	public class Title {
	
	    public Links links ; 
	}


	public class Data {
		public String type;
		public Integer id;
		public Relationships relationships;
	    public Attributes attributes ; 
	}


	public class Relationships {
	
	    public Title title ; 
	}
	
	public class Links_Z {
	
	    public String self ; 
	}
	
	
	public class Links {
	
	    public String related ;
	}
    
}