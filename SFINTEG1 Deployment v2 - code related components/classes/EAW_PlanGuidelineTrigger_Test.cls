@isTest
public class EAW_PlanGuidelineTrigger_Test {
    
    @testSetup static void setupTestData() {
        
        EAW_TestDataFactory.createPlanGuideline(1, True);
        EAW_TestDataFactory.createWindowGuideLine(1,true);
        EAW_TestDataFactory.createEAWTitle(1,True);
        
    }
    
    @isTest static void preventDeletionOfNonDraftPlans_Test(){
    
        List<EAW_Window_Guideline__c> windowGuideLineList = [SELECT Id,Name FROM EAW_Window_Guideline__c LIMIT 1];
        
        List<EAW_Window_Guideline_Strand__c> windowGuideLineStrandList = new List<EAW_Window_Guideline_Strand__c> {
            
            new EAW_Window_Guideline_Strand__c(License_Type__c = 'Exhibition License', EAW_Window_Guideline__c = windowGuideLineList[0].Id,
            Media__c = 'Theatrical', Territory__c = 'Afghanistan', Language__c = 'Afrikaans')
        };    
        insert windowGuideLineStrandList ;  
        
        windowGuideLineList[0].Status__c = 'Active';
        windowGuideLineList[0].Product_Type__c = 'Shorts';
        update windowGuideLineList;
        
        List<EAW_Title__c> title = [SELECT Id FROM EAW_Title__c LIMIT 1];
        
        EAW_Window__c winowIns = new EAW_Window__c();
        winowIns.EAW_Window_Guideline__c = windowGuideLineList[0].Id;
        winowIns.EAW_Title_Attribute__c = title[0].Id;
        winowIns.Window_Type__c = 'Current';
        insert winowIns;
        
        //LRCC-1653 - -In this ticket once passed QA it with remove the commented code.
        /*EAW_Window_Strand__c windowStrandIns = new EAW_Window_Strand__c();
        windowStrandIns.Territory__c = 'Afghanistan';
        windowStrandIns.Language__c = 'Afrikaans';
        windowStrandIns.Media__c = 'Theatrical';
        windowStrandIns.License_Type__c = 'Exhibition License';
        windowStrandIns.Window__c = winowIns.Id;        
        insert windowStrandIns;*/            
        
        EAW_Plan_Guideline__c planParentIns = new EAW_Plan_Guideline__c (Name = 'Test plan guideline parent', Product_Type__c = 'Shorts');
        insert planParentIns;
                
        List<EAW_Plan_Guideline__c >  planGuidelineList = new List<EAW_Plan_Guideline__c > {
            new EAW_Plan_Guideline__c (Name = 'Test plan guideline', Previous_Version__c  = planParentIns.Id, Product_Type__c = 'Shorts')  
        };
        insert planGuidelineList;        
                
        List<EAW_Plan_Window_Guideline_Junction__c> planWindowList = new List<EAW_Plan_Window_Guideline_Junction__c> {
            new EAW_Plan_Window_Guideline_Junction__c (Window_Guideline__c = windowGuideLineList[0].Id, Plan__c = planGuidelineList[0].Id)
        };
        
        insert planWindowList; 
        
       // windowGuideLineList[0].Status__c = 'Active';
      //  update windowGuideLineList;        
           
        planGuidelineList[0].Status__c = 'active';
        update planGuidelineList;
        
        Test.startTest();
        
        try {
            delete planGuidelineList;
        } catch(Exception ex) {
            
            System.assert(ex.getMessage().contains('You cannot delete an Active Plan Guideline'));
        }
        Test.stopTest();
        
    }
    
     @isTest static void preventDeletionOfNonDraftPlanscase_Test(){
        
        EAW_Plan_Guideline__c planIns = new EAW_Plan_Guideline__c ();
        planIns.Product_Type__c = 'Shorts';
        insert planIns; 
        
        List<EAW_Window_Guideline__c> windowGuideLineList = [SELECT Id,Name FROM EAW_Window_Guideline__c LIMIT 1];
        List<EAW_Plan_Guideline__c >  planGuidelineList = [SELECT Id, Name FROM EAW_Plan_Guideline__c LIMIT 1]; 
        List<EAW_Plan_Window_Guideline_Junction__c> planWindowList = new List<EAW_Plan_Window_Guideline_Junction__c> {
            new EAW_Plan_Window_Guideline_Junction__c (Window_Guideline__c = windowGuideLineList[0].Id, Plan__c = planGuidelineList[0].Id)
        };
        
        insert planWindowList;       
        
        planGuidelineList[0].Status__c = 'inactive';
        planGuidelineList[0].Next_Version__c = planIns.Id;
        update planGuidelineList;        
      
        Test.startTest();
        
        try {
            delete planGuidelineList;
        } catch(Exception ex) {
            
            System.assert(ex.getMessage().contains('You cannot delete an Inactive Plan Guideline with newer versions'));
        }
           Test.stopTest();
    }
     @isTest static void preventDeletionofPGchild_Test(){
        
        EAW_Plan_Guideline__c planIns = new EAW_Plan_Guideline__c();
        planIns.Product_Type__c = 'Feature';
        insert planIns; 
        
        List<EAW_Window_Guideline__c> windowGuideLineList = [SELECT Id,Name FROM EAW_Window_Guideline__c LIMIT 1];
        List<EAW_Plan_Guideline__c >  planGuidelineList = [SELECT Id, Name,Product_Type__c FROM EAW_Plan_Guideline__c LIMIT 1]; 
        List<EAW_Plan_Window_Guideline_Junction__c> planWindowList = new List<EAW_Plan_Window_Guideline_Junction__c> {
            new EAW_Plan_Window_Guideline_Junction__c (Window_Guideline__c = windowGuideLineList[0].Id, Plan__c = planGuidelineList[0].Id)
        };
        
        insert planWindowList;       
        
        List<EAW_Window_Guideline_Strand__c> windowGuideLineStrandList = new List<EAW_Window_Guideline_Strand__c> {
            
            new EAW_Window_Guideline_Strand__c(License_Type__c = 'Exhibition License', EAW_Window_Guideline__c = windowGuideLineList[0].Id,
            Media__c = 'Theatrical', Territory__c = 'Afghanistan', Language__c = 'Afrikaans')
        };    
        insert windowGuideLineStrandList ;
        
        windowGuideLineList[0].Status__c = 'Active';
        windowGuideLineList[0].Product_Type__c = 'Feature';
        update windowGuideLineList;
        
        List<EAW_Title__c> title = [SELECT Id,PROD_TYP_CD__c FROM EAW_Title__c LIMIT 1];
        
        planGuidelineList[0].Status__c = 'Active';
        planGuidelineList[0].Next_Version__c = planIns.Id;
        planGuidelineList[0].Product_Type__c = 'Feature';
        update planGuidelineList;
        
        EAW_Plan__c planchildIns = new EAW_Plan__c (Plan_Guideline__c = planGuidelineList[0].Id, EAW_Title__c = title[0].Id);
        insert planchildIns;
        
        Test.startTest();
        
        try {
            delete planGuidelineList;
        } catch(Exception ex) {
            
            System.assert(ex.getMessage().contains('This Plan Guideline cannot be deleted since it has associated plans'));
        }        
                
       Test.stopTest();
    }
    @isTest static void ensureValidationIsNotBypassed_Test(){
        
    List<EAW_Window_Guideline__c> windowGuideLineList = [SELECT Id,Name FROM EAW_Window_Guideline__c LIMIT 1];
        
        List<EAW_Window_Guideline_Strand__c> windowGuideLineStrandList = new List<EAW_Window_Guideline_Strand__c> {
            
            new EAW_Window_Guideline_Strand__c(License_Type__c = 'Exhibition License', EAW_Window_Guideline__c = windowGuideLineList[0].Id,
            Media__c = 'Theatrical', Territory__c = 'Afghanistan', Language__c = 'Afrikaans')
        };    
        insert windowGuideLineStrandList ;  
        
        windowGuideLineList[0].Status__c = 'Active';
        windowGuideLineList[0].Product_Type__c = 'Shorts';
        update windowGuideLineList;
        
        List<EAW_Title__c> title = [SELECT Id FROM EAW_Title__c LIMIT 1]; 
        
        EAW_Window__c winowIns = new EAW_Window__c();
        winowIns.EAW_Window_Guideline__c = windowGuideLineList[0].Id;
        winowIns.EAW_Title_Attribute__c = title[0].Id;
        winowIns.Window_Type__c = 'Current';
        insert winowIns;
        
        //LRCC-1653 - -In this ticket once passed QA it with remove the commented code.
        /*EAW_Window_Strand__c windowStrandIns = new EAW_Window_Strand__c();
        windowStrandIns.Territory__c = 'Afghanistan';
        windowStrandIns.Language__c = 'Afrikaans';
        windowStrandIns.Media__c = 'Theatrical';
        windowStrandIns.License_Type__c = 'Exhibition License';
        windowStrandIns.Window__c = winowIns.Id;        
        insert windowStrandIns;*/           
        
        EAW_Plan_Guideline__c planParentIns = new EAW_Plan_Guideline__c (Name = 'Test plan guideline parent', Product_Type__c = 'Shorts');
        insert planParentIns;
                
        List<EAW_Plan_Guideline__c >  planGuidelineList = new List<EAW_Plan_Guideline__c > {
            new EAW_Plan_Guideline__c (Name = 'Test plan guideline', Previous_Version__c  = planParentIns.Id, Product_Type__c = 'Shorts')  
        };
        insert planGuidelineList;        
                
        List<EAW_Plan_Window_Guideline_Junction__c> planWindowList = new List<EAW_Plan_Window_Guideline_Junction__c> {
            new EAW_Plan_Window_Guideline_Junction__c (Window_Guideline__c = windowGuideLineList[0].Id, Plan__c = planGuidelineList[0].Id)
        };
        
        insert planWindowList; 
        
      //  windowGuideLineList[0].Status__c = 'Active';
     //   update windowGuideLineList;        
        planGuidelineList[0].Status__c = 'active';
        update planGuidelineList;
    }
    
    @isTest static void updateAssociatedPlans_Test() {
        
        List<EAW_Window_Guideline__c> windowGuideLineList = [SELECT Id,Name FROM EAW_Window_Guideline__c LIMIT 1];
        
        List<EAW_Window_Guideline_Strand__c> windowGuideLineStrandList = new List<EAW_Window_Guideline_Strand__c> {
            
            new EAW_Window_Guideline_Strand__c(License_Type__c = 'Exhibition License', EAW_Window_Guideline__c = windowGuideLineList[0].Id,
            Media__c = 'Theatrical', Territory__c = 'Afghanistan', Language__c = 'Afrikaans')
        };    
        insert windowGuideLineStrandList ;  
        
        windowGuideLineList[0].Status__c = 'Active';
        windowGuideLineList[0].Product_Type__c = 'Feature';
        update windowGuideLineList;
        
        List<EAW_Title__c> title = [SELECT Id FROM EAW_Title__c LIMIT 1];  
        
        EAW_Window__c winowIns = new EAW_Window__c();
        winowIns.EAW_Window_Guideline__c = windowGuideLineList[0].Id;
        winowIns.EAW_Title_Attribute__c = title[0].Id;
        winowIns.Window_Type__c = 'Current';
        insert winowIns;
        
        //LRCC-1653 - -In this ticket once passed QA it with remove the commented code.
        /*EAW_Window_Strand__c windowStrandIns = new EAW_Window_Strand__c();
        windowStrandIns.Territory__c = 'Afghanistan';
        windowStrandIns.Language__c = 'Afrikaans';
        windowStrandIns.Media__c = 'Theatrical';
        windowStrandIns.License_Type__c = 'Exhibition License';
        windowStrandIns.Window__c = winowIns.Id;        
        insert windowStrandIns;*/            
        
        EAW_Plan_Guideline__c planParentIns = new EAW_Plan_Guideline__c (Name = 'Test plan guideline parent', Product_Type__c = 'Shorts');
        insert planParentIns;
                
        List<EAW_Plan_Guideline__c >  planGuidelineList = new List<EAW_Plan_Guideline__c > {
            new EAW_Plan_Guideline__c (Name = 'Test plan guideline', Previous_Version__c  = planParentIns.Id, Product_Type__c = 'Feature')  
        };
        insert planGuidelineList;        
                
        List<EAW_Plan_Window_Guideline_Junction__c> planWindowList = new List<EAW_Plan_Window_Guideline_Junction__c> {
            new EAW_Plan_Window_Guideline_Junction__c (Window_Guideline__c = windowGuideLineList[0].Id, Plan__c = planGuidelineList[0].Id)
        };
        
        insert planWindowList; 
        
      //  windowGuideLineList[0].Status__c = 'Active';
      //  update windowGuideLineList;        
        
        planGuidelineList[0].Status__c = 'active';
        update planGuidelineList; 
        
        EAW_Plan__c planchildIns = new EAW_Plan__c (Plan_Guideline__c = planGuidelineList[0].Id, EAW_Title__c = title[0].Id);
        insert planchildIns;
    }
    
    @isTest static void planReQualificationCheckForActivePG_Test() {
        
        List<EAW_Window_Guideline__c> windowGuideLineList = [SELECT Id,Name FROM EAW_Window_Guideline__c LIMIT 1];
        
        List<EAW_Window_Guideline_Strand__c> windowGuideLineStrandList = new List<EAW_Window_Guideline_Strand__c> {
            
            new EAW_Window_Guideline_Strand__c(License_Type__c = 'Exhibition License', EAW_Window_Guideline__c = windowGuideLineList[0].Id,
            Media__c = 'Theatrical', Territory__c = 'Afghanistan', Language__c = 'Afrikaans')
        };    
        insert windowGuideLineStrandList ;
        
        windowGuideLineList[0].Status__c = 'Active';
        windowGuideLineList[0].Product_Type__c = 'Feature';
        update windowGuideLineList;
        
        List<EAW_Title__c> title = [SELECT Id FROM EAW_Title__c LIMIT 1];  
        
        EAW_Window__c winowIns = new EAW_Window__c();
        winowIns.EAW_Window_Guideline__c = windowGuideLineList[0].Id;
        winowIns.EAW_Title_Attribute__c = title[0].Id;
        winowIns.Window_Type__c = 'Current';
        insert winowIns;
        
        //LRCC-1653 - -In this ticket once passed QA it with remove the commented code.
        /*EAW_Window_Strand__c windowStrandIns = new EAW_Window_Strand__c();
        windowStrandIns.Territory__c = 'Afghanistan';
        windowStrandIns.Language__c = 'Afrikaans';
        windowStrandIns.Media__c = 'Theatrical';
        windowStrandIns.License_Type__c = 'Exhibition License';
        windowStrandIns.Window__c = winowIns.Id;        
        insert windowStrandIns;*/            
        
        EAW_Plan_Guideline__c planParentIns = new EAW_Plan_Guideline__c (Name = 'Test plan guideline parent', Product_Type__c = 'Feature');
        insert planParentIns;
                
        List<EAW_Plan_Guideline__c >  planGuidelineList = new List<EAW_Plan_Guideline__c > {
            new EAW_Plan_Guideline__c (Name = 'Test plan guideline', Previous_Version__c  = planParentIns.Id, Product_Type__c = 'Feature')  
        };
        insert planGuidelineList; 
        
        EAW_Rule__c ruleIns = new EAW_Rule__c(Plan_Guideline__c = planGuidelineList[0].Id);
        insert ruleIns;
        
        EAW_Rule_Detail__c ruleDetail = new EAW_Rule_Detail__c (Rule_No__c = ruleIns.Id);
        insert ruleDetail; 
                
        List<EAW_Plan_Window_Guideline_Junction__c> planWindowList = new List<EAW_Plan_Window_Guideline_Junction__c> {
            new EAW_Plan_Window_Guideline_Junction__c (Window_Guideline__c = windowGuideLineList[0].Id, Plan__c = planGuidelineList[0].Id)
        };
        
        insert planWindowList; 
        
        planGuidelineList[0].Status__c = 'active';
        update planGuidelineList;       
        
        planGuidelineList[0].Sales_Region__c = 'United States';
        update planGuidelineList;
        
        EAW_Plan__c planchildIns = new EAW_Plan__c (Plan_Guideline__c = planGuidelineList[0].Id, EAW_Title__c = title[0].Id);
        insert planchildIns;
            
    }
    @isTest static void clonePlanQualifier_Test(){
        
        EAW_Plan_Guideline__c planParentIns = new EAW_Plan_Guideline__c (Name = 'Test plan guideline 1', Product_Type__c = 'Feature');
        insert planParentIns;
        
        String ruleContent = '{"sObjectType": "EAW_Rule__c"}';
        String ruleDetailContent =  '{"sObjectType": "EAW_Rule_Detail__c", "Condition_Operator__c": ">", "Condition_Year__c": "2000", "Condition_Field__c": "Release Year"}';
        EAW_TestRuleDataFactory.savePgQualifiers(ruleContent,ruleDetailContent,planParentIns.Id);
        List<EAW_Window_Guideline__c> windowGuideLineList = [SELECT Id,Name FROM EAW_Window_Guideline__c LIMIT 1];
        
        List<EAW_Window_Guideline_Strand__c> windowGuideLineStrandList = new List<EAW_Window_Guideline_Strand__c> {
            
            new EAW_Window_Guideline_Strand__c(License_Type__c = 'Exhibition License', EAW_Window_Guideline__c = windowGuideLineList[0].Id,
            Media__c = 'Theatrical', Territory__c = 'Afghanistan', Language__c = 'Afrikaans')
        };    
        insert windowGuideLineStrandList ;
        
        windowGuideLineList[0].Status__c = 'Active';
        windowGuideLineList[0].Product_Type__c = 'Feature';
        update windowGuideLineList;
        
        List<EAW_Plan_Window_Guideline_Junction__c> planWindowList = new List<EAW_Plan_Window_Guideline_Junction__c> {
            new EAW_Plan_Window_Guideline_Junction__c (Window_Guideline__c = windowGuideLineList[0].Id, Plan__c = planParentIns.Id)
        };
        insert planWindowList;    
        planParentIns.Status__c = 'Active';
        update planParentIns;
        
        EAW_Plan_Guideline__c childPlan = new EAW_Plan_Guideline__c (Name = 'Test plan guideline 2', Product_Type__c = 'Feature',Previous_Version__c  = planParentIns.Id);
        insert childPlan;
        
    }
    
    //check for draft to active
    @isTest static void testDraftToActiveVersion(){
        
        List<EAW_Window_Guideline__c> windowGuideLineList = [SELECT Id,Name FROM EAW_Window_Guideline__c LIMIT 1];
        List<EAW_Plan_Guideline__c >  planGuidelineList = [SELECT Id,Status__c ,Name,Product_Type__c FROM EAW_Plan_Guideline__c LIMIT 1]; 
        List<EAW_Plan_Window_Guideline_Junction__c> planWindowList = new List<EAW_Plan_Window_Guideline_Junction__c> {
            new EAW_Plan_Window_Guideline_Junction__c (Window_Guideline__c = windowGuideLineList[0].Id, Plan__c = planGuidelineList[0].Id)
        };
        
        insert planWindowList;       
        
        List<EAW_Window_Guideline_Strand__c> windowGuideLineStrandList = new List<EAW_Window_Guideline_Strand__c> {
            
            new EAW_Window_Guideline_Strand__c(License_Type__c = 'Exhibition License', EAW_Window_Guideline__c = windowGuideLineList[0].Id,
            Media__c = 'Theatrical', Territory__c = 'Afghanistan', Language__c = 'Afrikaans')
        };    
        insert windowGuideLineStrandList ;
        
        windowGuideLineList[0].Status__c = 'Active';
        windowGuideLineList[0].Product_Type__c = 'Feature';
        update windowGuideLineList;
        
        List<EAW_Title__c> title = [SELECT Id,PROD_TYP_CD__c FROM EAW_Title__c LIMIT 1];
        
        planGuidelineList[0].Status__c = 'Active';
        planGuidelineList[0].Product_Type__c = 'Feature';
        
        Test.startTest();
            update planGuidelineList;
        Test.stopTest();
    }
        
    //check for active to inactive
    @isTest static void testActiveToInactiveVersion(){

        List<EAW_Window_Guideline__c> windowGuideLineList = [SELECT Id,Name FROM EAW_Window_Guideline__c LIMIT 1];
        List<EAW_Plan_Guideline__c >  planGuidelineList = [SELECT Id,Status__c ,Name,Product_Type__c FROM EAW_Plan_Guideline__c LIMIT 1]; 
        List<EAW_Plan_Window_Guideline_Junction__c> planWindowList = new List<EAW_Plan_Window_Guideline_Junction__c> {
            new EAW_Plan_Window_Guideline_Junction__c (Window_Guideline__c = windowGuideLineList[0].Id, Plan__c = planGuidelineList[0].Id)
        };
        
        insert planWindowList;       
        
        List<EAW_Window_Guideline_Strand__c> windowGuideLineStrandList = new List<EAW_Window_Guideline_Strand__c> {
            
            new EAW_Window_Guideline_Strand__c(License_Type__c = 'Exhibition License', EAW_Window_Guideline__c = windowGuideLineList[0].Id,
            Media__c = 'Theatrical', Territory__c = 'Afghanistan', Language__c = 'Afrikaans')
        };    
        insert windowGuideLineStrandList ;
        
        windowGuideLineList[0].Status__c = 'Active';
        windowGuideLineList[0].Product_Type__c = 'Feature';
        update windowGuideLineList;
        
        List<EAW_Title__c> title = [SELECT Id,PROD_TYP_CD__c FROM EAW_Title__c LIMIT 1];
        
        planGuidelineList[0].Status__c = 'Active';
        planGuidelineList[0].Product_Type__c = 'Feature';
        update planGuidelineList;
        
        planGuidelineList[0].Status__c = 'inactive';
        
        Test.startTest();
            try {
                update planGuidelineList;
            } catch(Exception ex) {                
                System.assert(ex.getMessage().contains('Plan Qualification work is under Processing'));
            }
        Test.stopTest();
    }
        
    //check for draft to inactive
    @isTest static void testDraftToInactiveVersion(){
        
        List<EAW_Plan_Guideline__c >  planGuidelineList = [SELECT Id,Status__c ,Name,Product_Type__c FROM EAW_Plan_Guideline__c LIMIT 1]; 
        
        planGuidelineList[0].Status__c = 'inactive';
        
        Test.startTest();
            update planGuidelineList;
        Test.stopTest();
    }
        
    //check for inactive to active
    @isTest static void testInactiveToActiveVersion(){
        
        List<EAW_Plan_Guideline__c >  planGuidelineList = [SELECT Id,Status__c ,Name,Product_Type__c FROM EAW_Plan_Guideline__c LIMIT 1]; 
        
        planGuidelineList[0].Status__c = 'inactive';
        update planGuidelineList;
        
        List<EAW_Window_Guideline__c> windowGuideLineList = [SELECT Id,Name FROM EAW_Window_Guideline__c LIMIT 1];
        List<EAW_Plan_Window_Guideline_Junction__c> planWindowList = new List<EAW_Plan_Window_Guideline_Junction__c> {
            new EAW_Plan_Window_Guideline_Junction__c (Window_Guideline__c = windowGuideLineList[0].Id, Plan__c = planGuidelineList[0].Id)
        };
        
        insert planWindowList;       
        
        List<EAW_Window_Guideline_Strand__c> windowGuideLineStrandList = new List<EAW_Window_Guideline_Strand__c> {
            
            new EAW_Window_Guideline_Strand__c(License_Type__c = 'Exhibition License', EAW_Window_Guideline__c = windowGuideLineList[0].Id,
            Media__c = 'Theatrical', Territory__c = 'Afghanistan', Language__c = 'Afrikaans')
        };    
        insert windowGuideLineStrandList ;
        
        windowGuideLineList[0].Status__c = 'Active';
        windowGuideLineList[0].Product_Type__c = 'Feature';
        update windowGuideLineList;
        
        List<EAW_Title__c> title = [SELECT Id,PROD_TYP_CD__c FROM EAW_Title__c LIMIT 1];
        
        planGuidelineList[0].Status__c = 'Active';
        planGuidelineList[0].Product_Type__c = 'Feature';
        
        Test.startTest();
            update planGuidelineList;
        Test.stopTest();
    }
}