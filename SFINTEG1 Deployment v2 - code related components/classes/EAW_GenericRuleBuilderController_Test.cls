@isTest
public class EAW_GenericRuleBuilderController_Test {

     @testSetup static void setupTestData() {
     
         //EAW_TestDataFactory.createReleaseDateType(1, true);
         EAW_TestDataFactory.createEAWCustomer(1,true);
         
     }
    

      static testMethod void validate() {
  
        List<EAW_Rule_Detail__c> edmRuleDetailList = new  List<EAW_Rule_Detail__c>();
        
        // Call appropriate methods in EAW_TestDataFactory to create test records
       // List<EAW_Release_Date_Type__c> releaseDateTypeList = [SELECT id, Name FROM EAW_Release_Date_Type__c LIMIT 1];
        List<EAW_Customer__c> customerList = [SELECT id, Name FROM EAW_Customer__c LIMIT 1];
        //LRCC-1560 - Replace Customer lookup field with Customer text field
        List<EAW_Release_Date_Guideline__c> releaseDateGuideLineList = EAW_TestDataFactory.createEAWReleaseDateGuideline( null, customerList[0].Id, 2,False,customerList[0].Name );
        
        releaseDateGuideLineList[0].Active__c = TRUE;
        releaseDateGuideLineList[0].Territory__c = 'India';
        releaseDateGuideLineList[0].Product_Type__c = 'Special';
        releaseDateGuideLineList[0].EAW_Release_Date_Type__c = 'HV-Rental';
        
        releaseDateGuideLineList[1].Active__c = TRUE;
        releaseDateGuideLineList[1].EAW_Release_Date_Type__c = 'HV-Retail';
        releaseDateGuideLineList[1].Territory__c = 'India';
        releaseDateGuideLineList[1].Product_Type__c = 'Season';
        insert releaseDateGuideLineList;
        
        EAW_Rule__c rule= new EAW_Rule__c(Release_Date_Guideline__c = releaseDateGuideLineList[0].Id);
        insert rule ;
        
        edmRuleDetailList =  EAW_TestDataFactory.createRuleDetail(3,false);
        edmRuleDetailList[0].Rule_No__c = rule.Id;
        edmRuleDetailList[1].Rule_No__c = rule.Id;
        edmRuleDetailList[2].Rule_No__c = rule.Id;
        insert edmRuleDetailList; 
        
        edmRuleDetailList[0].Parent_Rule_Detail__c = edmRuleDetailList[1].Id;
        edmRuleDetailList[1].Parent_Rule_Detail__c = edmRuleDetailList[2].Id;
        update edmRuleDetailList;
        
        EAW_GenericRuleBuilderController.ruleWrapper rulewrapper  = EAW_GenericRuleBuilderController.getRulesAndDetails(releaseDateGuideLineList[0].Id);
        System.assertEquals(true, rulewrapper != null);
        
        EAW_FieldDescriber.dependentPicklistValuesWrapper depList = EAW_GenericRuleBuilderController.getDependentFieldSet('Condition_Tag_Type__c', 'Release Date Calculation');
        System.assertEquals(true, depList != null);
        
        List<String> testruleDetilsToDelete= new List<String>{ edmRuleDetailList[0].Id, edmRuleDetailList[1].Id};
        String testruleAndRuleDetailNodes = '[{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Condition_Type__c":"If","Condition_Timeframe__c":"True_Earliest","Rule_Order__c":1},"dateCalcs":[{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Condition_Met_Days__c":"4","Condition_Met_Media__c":"Theatrical","Condition_Met_Territory__c":"India"},{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Condition_Met_Days__c":"5","Condition_Met_Media__c":"Theatrical","Condition_Met_Territory__c":"Pakistan"}],"nodeCalcs":[{"ruleDetail":{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":2,"Condition_Timeframe__c":"True_Earliest"},"dateCalcs":[{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":2,"Condition_Met_Days__c":"6","Condition_Met_Media__c":"Theatrical","Condition_Met_Territory__c":"China"}],"nodeCalcs":[]}]},{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Rule_Order__c":2,"Nest_Order__c":1,"Condition_Type__c":"Else/If","Condition_Met_Days__c":"9","Condition_Met_Media__c":"Theatrical","Condition_Met_Territory__c":"China"},"dateCalcs":[],"nodeCalcs":[]}]';
        String testrule= JSON.serialize(rule); 
        
		//LRCC-1560 - Replace Customer lookup field with Customer text field        
        EAW_GenericRuleBuilderController.saveRuleAndRuleDetails(testrule,testruleAndRuleDetailNodes,testruleDetilsToDelete, 'Test release date guideline',releaseDateGuideLineList[0].Id);
        List<EAW_Rule__c> rulelist = [SELECT Id, Name FROM EAW_Rule__c];
        System.assertEquals(2, rulelist.size());
        
        //LRCC-1560 - Replace Customer lookup field with Customer text field
        EAW_GenericRuleBuilderController.deleteRuleAndDetails(true,false,rule.Id,testruleDetilsToDelete,releaseDateGuideLineList[0].Id);
        List<EAW_Rule_Detail__c> parentRuleDetail = [SELECT Id,Rule_No__c FROM EAW_Rule_Detail__c];
        System.assertEquals(0, parentRuleDetail.size());
    }
}