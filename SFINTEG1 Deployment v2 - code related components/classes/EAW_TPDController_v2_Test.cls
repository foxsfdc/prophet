@isTest
public class EAW_TPDController_v2_Test {
    @testSetup static void setup() {
        EAW_TestDataFactory.createPlanGuideline(1,true);
        EAW_TestDataFactory.createTag(2, true, 'Window Tag');
        EAW_TestDataFactory.createWindowGuideLine(2, True);
    }
    @isTest static void applyLatestVersion() {
        
        List<String> windowIdList = new List<String>();
        
        List<EAW_Window_Guideline__c> windowGuideLineNext = EAW_TestDataFactory.createWindowGuideLine(1, false);
        windowGuideLineNext[0].Status__c = 'Draft';
        windowGuideLineNext[0].Name = 'Test window GuideLine rec';
        windowGuideLineNext[0].Product_Type__c = 'shorts';
        windowGuideLineNext[0].Window_Type__c = 'First-Run';
        insert windowGuideLineNext;
        
        List<EAW_Window_Guideline__c> windowGuideLineList = [SELECT Id, Name FROM EAW_Window_Guideline__c LIMIT 2];
        windowGuideLineList[0].Next_Version__c = windowGuideLineNext[0].Id;
        update windowGuideLineList;
        
        List<EAW_Window_Guideline_Strand__c> windowGuideLineStrandList = EAW_TestDataFactory.createWindowGuideLineStrand(1, false);
        windowGuideLineStrandList[0].License_Type__c = 'Exhibition License';
        windowGuideLineStrandList[0].EAW_Window_Guideline__c = windowGuideLineList[0].Id;
        windowGuideLineStrandList[0].Media__c = 'Theatrical';
        windowGuideLineStrandList[0].Territory__c = 'Afghanistan'; 
        windowGuideLineStrandList[0].Language__c = 'Afrikaans';
        insert windowGuideLineStrandList; 
        
        windowGuidelineList[0].Status__c = 'Active';
        update windowGuidelineList; 
        
        List<EAW_Window__c> windowList = EAW_TestDataFactory.createWindow(1, false);
        windowList[0].EAW_Window_Guideline__c = windowGuideLineList[0].Id;
        insert windowList;
        
        List<EAW_Plan_Guideline__c> planinstanceList = EAW_TestDataFactory.createPlanGuideline(2, false); 
        planinstanceList[0].Name = 'Test plan guideline Next';
        insert planinstanceList[0];
        
        planinstanceList[1].Name = 'Test plan guideline';
        insert planinstanceList[1]; 
        
        List<EAW_Plan_Guideline__c> planGuidelist = [SELECT Id,Name FROM EAW_Plan_Guideline__c LIMIT 1];
        
        EAW_Plan_Window_Guideline_Junction__c ruleDetail = new EAW_Plan_Window_Guideline_Junction__c (Plan__c = planGuidelist[0].Id, Window_Guideline__c = windowGuideLineList[0].Id);
        insert ruleDetail;
        
        planGuidelist[0].Next_Version__c = planinstanceList[1].Id;
        planGuidelist[0].Status__c = 'active';
        update planGuidelist;
        
        for(EAW_Window__c windowIns : windowList) {
            windowIdList.add(windowIns.Id);
        }
        
        List<EAW_Plan__c> planList = EAW_TestDataFactory.createPlan(1, false);
        planList[0].Plan_Guideline__c = planGuidelist[0].Id;
        insert planList;
        
        Map<String, List<SObject>> applyList = EAW_TPDController_v2.applyLatestVersion(String.ValueOf(planList[0].Id), windowIdList);
        
        System.assertEquals(True, applyList != null);
    }
    
    @isTest static void massUpdateWindows() {
        
        List<String> windowIdList = new List<String>();
        List<String> tagStringList = new List<String>();
        List<EAW_Tag__c> tagList = [SELECT Id, Name FROM EAW_Tag__c LIMIT 3 ];
        List<EAW_Window_Guideline__c> windowGuideLineList = [SELECT Id, Name FROM EAW_Window_Guideline__c LIMIT 2];
        
        List<EAW_Window_Guideline_Strand__c> windowGuideLineStrandList = EAW_TestDataFactory.createWindowGuideLineStrand(1, false);
        windowGuideLineStrandList[0].License_Type__c = 'Exhibition License';
        windowGuideLineStrandList[0].EAW_Window_Guideline__c = windowGuideLineList[0].Id;
        windowGuideLineStrandList[0].Media__c = 'Theatrical';
        windowGuideLineStrandList[0].Territory__c = 'Afghanistan'; 
        windowGuideLineStrandList[0].Language__c = 'Afrikaans';
        insert windowGuideLineStrandList; 
        
        windowGuidelineList[0].Status__c = 'Active';        
        update windowGuidelineList[0];
        
        List<EAW_Window__c> windowList = EAW_TestDataFactory.createWindow(1, false);
        windowList[0].EAW_Window_Guideline__c = windowGuideLineList[0].Id;
        insert windowList;
        
        List<EAW_Plan_Guideline__c> planinstanceList = EAW_TestDataFactory.createPlanGuideline(2, false); 
        planinstanceList[0].Name = 'Test plan guideline Next';
        insert planinstanceList[0];
        
        planinstanceList[1].Name = 'Test plan guideline';
        planinstanceList[1].Next_Version__c = planinstanceList[0].Id;
        insert planinstanceList[1]; 
        
        List<EAW_Plan_Guideline__c> planGuidelist = [SELECT Id,Name FROM EAW_Plan_Guideline__c LIMIT 1];
        
        EAW_Plan_Window_Guideline_Junction__c ruleDetail = new EAW_Plan_Window_Guideline_Junction__c (Plan__c = planGuidelist[0].Id, Window_Guideline__c = windowGuideLineList[0].Id);
        insert ruleDetail;
        
        planGuidelist[0].Next_Version__c = planinstanceList[1].Id;
        planGuidelist[0].Status__c = 'active';
        update planGuidelist;
        
        for(EAW_Window__c windowIns : windowList) {
            windowIdList.add(windowIns.Id);
        }
        for(EAW_Tag__c tagIns : tagList) {
            
            tagStringList.add(tagIns.Id);
        }
        List<EAW_Plan__c> planList = EAW_TestDataFactory.createPlan(1, false);
        planList[0].Plan_Guideline__c = planGuidelist[0].Id;
        insert planList;
        
        List<EAW_Window__c> windowListobt = EAW_TPDController_v2.massUpdateWindows(windowIdList, tagStringList, '2018-10-05', '2018-10-25', 'Test window', 'Estimated', true, true, true,
        'Monday', 'tuesday', true, 'Test window Title');
        
        System.assertEquals(True, windowListobt != null);
        
        List<String> planStringList = new List<String>();
        
        for(EAW_Plan__c planIns : planList ) {
            planStringList.add(planIns.id);
        }
        
        Map<String, List<String>> deleteList = EAW_TPDController_v2.deleteTitlePlanWindows(windowIdList , planStringList );
        
        System.assertEquals(True, deleteList != null);
    }
    /*@isTest static void deleteWindows() {
    
        List<String> windowIdList = new List<String>();
        List<String> planStringList = new List<String>();
        List<EAW_Window__c> windowList = [SELECT Id, Name FROM EAW_Window__c LIMIT 3];
        
        for(EAW_Window__c windowIns : windowList) {
            windowIdList.add(windowIns.Id);
        }
        
        List<EAW_Plan__c> planList = EAW_TestDataFactory.createPlan(1, false);
        planList[0].Plan_Guideline__c = planGuidelist[0].Id;
        insert planList;
        
        for(EAW_Plan__c planIns : planList ) {
            planStringList.add(planIns.id);
        }
        
        Map<String, List<String>> deleteList = EAW_TPDController_v2.deleteTitlePlanWindows(windowIdList , planStringList );
        
        System.assertEquals(True, deleteList != null);
    }*/
    @isTest static void deleteWindowsWrapper() {
        
        String objectPickListString = '{"EAW_Rule_Detail__c":["Condition_Criteria_Field__c","Condition_Date_Duration__c"]}';
        EAW_TPDController_v2.getPicklistValues(objectPickListString );
    }
}