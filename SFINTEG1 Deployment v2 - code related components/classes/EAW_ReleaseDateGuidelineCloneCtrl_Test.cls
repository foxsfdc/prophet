@isTest
public class EAW_ReleaseDateGuidelineCloneCtrl_Test {
    
    
    @testSetup static void setupTestData() {
          
          List<EAW_Release_Date_Guideline__c> rdgList =  EAW_TestDataFactory.createReleaseDateGuideline(2, TRUE, 'Feature', new List<String>{'Theatrical','HV-Retail'}, 'United States +', 'English');
          
          List<EAW_Title__c> titleList = EAW_TestDataFactory.createEAWTitle(2, TRUE);
          
          List<EAW_Release_Date__c> releaseDateList = EAW_TestDataFactory.createReleaseDate(2, FALSE);
          
          releaseDateList[0].Release_Date_Guideline__c = rdgList[0].Id;
          releaseDateList[0].Title__c = titleList[0].Id;
          releaseDateList[0].Manual_Date__c = System.Today();
          releaseDateList[0].Temp_Perm__c = 'Temporary';
          
          releaseDateList[1].Release_Date_Guideline__c = rdgList[0].Id;
          releaseDateList[1].Title__c = titleList[1].Id;
          releaseDateList[1].Manual_Date__c = System.Today();
          releaseDateList[1].Temp_Perm__c = 'Temporary';
          
          insert releaseDateList;
          
          String ruleJson = '{"sObjectType":"EAW_Rule__c","Nested__c":true,"Condition_Type__c":"case"}';
          String ruleDetailJson = '[{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Condition_Type__c":"Case then","Condition_Set_Operator__c":"IN","Condition_Field__c":"Release Date Status","Condition_Criteria__c":"Estimated","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature","Conditional_Operand_Id__c":"aA73D000000CyJjSAK","Multi_conditional_operands__c":false},"dateCalcs":[],"nodeCalcs":[{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":2,"Condition_Type__c":"Else/If","Condition_Timeframe__c":"True_Earliest"},"dateCalcs":[],"nodeCalcs":[{"ruleDetail":{"sObjectType":"EAW_Rule_Detail__c","Parent_Condition_Type__c":"Condition","Condition_Type__c":"If","Condition_Set_Operator__c":"IN","Condition_Field__c":"Release Date Status","Criteria__c":"","Condition_Operand_Details__c":"EST-HD / Australia / No Specific Language / Feature","Conditional_Operand_Id__c":"aA73D0000000K1TSAU","Multi_conditional_operands__c":false,"Condition_Criteria__c":"Estimated"},"dateCalcs":[],"nodeCalcs":[{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Condition_Type__c":"If","Condition_Timeframe__c":"True_Earliest"},"dateCalcs":[{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Conditional_Operand_Id__c":"aA73D0000008di0SAA","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Antigua / Nauruan / Feature"}],"nodeCalcs":[]},{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Rule_Order__c":2,"Nest_Order__c":1,"Condition_Type__c":"Else/If","Conditional_Operand_Id__c":"aA73D0000008di0SAA","Condition_Met_Months__c":"2","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Antigua / Nauruan / Feature"},"dateCalcs":[],"nodeCalcs":[]}]}]}]},{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Rule_Order__c":2,"Nest_Order__c":1,"Condition_Type__c":"Otherwise"},"dateCalcs":[],"nodeCalcs":[{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":2,"Condition_Type__c":"Else/If","Condition_Timeframe__c":"True_Latest"},"dateCalcs":[],"nodeCalcs":[{"ruleDetail":{"sObjectType":"EAW_Rule_Detail__c","Parent_Condition_Type__c":"Case","Condition_Type__c":"Case"},"dateCalcs":[],"nodeCalcs":[{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Condition_Type__c":"Case then","Condition_Set_Operator__c":"IN","Condition_Field__c":"Release Date Status","Condition_Criteria__c":"Firm","Condition_Operand_Details__c":"EST-HD / Australia / No Specific Language / Feature","Conditional_Operand_Id__c":"aA73D0000000K1TSAU","Multi_conditional_operands__c":false},"dateCalcs":[],"nodeCalcs":[{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":2,"Condition_Type__c":"","Conditional_Operand_Id__c":"aA73D000000CyJjSAK","Condition_Met_Months__c":"2","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"},"dateCalcs":[],"nodeCalcs":[]}]},{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Rule_Order__c":2,"Nest_Order__c":1,"Condition_Type__c":"Otherwise"},"dateCalcs":[],"nodeCalcs":[{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":2,"Condition_Type__c":"Else/If","Condition_Timeframe__c":"True_Earliest"},"dateCalcs":[],"nodeCalcs":[{"ruleDetail":{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":2,"Nest_Order__c":3,"Condition_Timeframe__c":"True_Latest"},"dateCalcs":[{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":2,"Nest_Order__c":3,"Conditional_Operand_Id__c":"aA73D000000CyJjSAK","Condition_Met_Months__c":"2","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"}],"nodeCalcs":[]}]}]}]}]}]}]';
          EAW_TestRuleDataFactory.saveRuleAndRuleDetails(ruleJson, ruleDetailJson,TRUE,rdgList[1].Id, NULL,rdgList[0].Id); // Parameters: (String ruleAndRuleDetailNodes,Boolean isRDG,String guidelineId,String wgRuleDate,String conditionalOperandId);
     }
     
     @isTest static void processCloneLogicTest1() {
     
         
          List<EAW_Release_Date_Guideline__c> rdgList =  EAW_TestDataFactory.createReleaseDateGuideline(1, TRUE, 'Feature', new List<String>{'HV-Rental'}, 'United States +', 'English');
          
          EAW_ReleaseDateGuidelineCloneController.getReleaseDateGuideline( rdgList[0].Id );
          
          List<EAW_Release_Date_Guideline__c> rdgList2 = [ Select Id, EAW_Release_Date_Type__c From EAW_Release_Date_Guideline__c Where EAW_Release_Date_Type__c = 'HV-Retail' ];
          
          EAW_ReleaseDateGuidelineCloneController.cloneRuleAndRuleDetails( rdgList2[0].Id, rdgList[0].Id );
          
          rdgList[0].Product_Type__c = 'Episode';
          update rdgList;
          
          EAW_ReleaseDateGuidelineCloneController.checkForProductTypeChange( rdgList2[0].Id, rdgList[0].Id );
          
          
     }
    
}