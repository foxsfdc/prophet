//LRCC-1708
global class EAW_WindowAudit_API_BatchProcess implements Database.AllowsCallouts,Database.Batchable<sObject> {
	public List<Id> windowIdList = new List<Id>();
    
    global EAW_WindowAudit_API_BatchProcess(List<Id> windowIdList){
    	this.windowIdList = windowIdList;
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
    	return Database.getQueryLocator(
            'select id,name, History_Sent_Date__c, Sent_To_Repo__c, '+
			'(Select Field, OldValue, NewValue, CreatedBy.Name,CreatedDate From Histories order by CreatedDate desc) '+
			'from EAW_Window__c where '+
			' ID IN : windowIdList AND Soft_Deleted__c <> \'TRUE\''			
        );
    }
    global void execute(Database.BatchableContext bc, List<EAW_Window__c> records){
        try{
        	EAW_WindowsAudit_Outbound ewao = new EAW_WindowsAudit_Outbound();
			ewao.sendData(records);        	
        }catch(Exception e){
            String message = e.getMessage();
    		//System.debug('e.getMessage(): '+e.getMessage());
    		if(message!=null && message.length()>250)message=message.substring(0, 250);
            
        }
    	
    }    
    global void finish(Database.BatchableContext bc){
    	
    }    
}