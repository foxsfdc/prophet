@isTest
public with sharing class EAW_TitlePlanDateManagementControll_Test {
    @testSetup
    static void setupmethod(){
        
          
        List<EAW_Tag__c> tagList = EAW_TestDataFactory.createTag(1, True,'Window Tag');
        List<EAW_Window_Guideline__c> wgList = EAW_TestDataFactory.createWindowGuideLine(2, FALSE);
        wgList[0].Product_Type__c = 'Feature';
        wgList[1].Product_Type__c = 'Feature';
        
        insert wgList;
        
        List<EAW_Window_Guideline_Strand__c> wgsList = EAW_TestDataFactory.createWindowGuideLineStrand(2, FALSE);
        
        wgsList[0].EAW_Window_Guideline__c = wgList[0].Id;
        wgsList[0].Media__c = 'Free TV';
        wgsList[0].Language__c = 'English';
        wgsList[0].Territory__c = 'India';
        
        wgsList[1].EAW_Window_Guideline__c = wgList[1].Id;
        wgsList[1].Media__c = 'Basic TV';
        wgsList[1].Language__c = 'English';
        wgsList[1].Territory__c = 'Albania';
        
        insert wgsList;
        
        wgList[0].Status__c = 'Active';
        wgList[1].Status__c = 'Active';
        
        update wgList;
        
        List<EAW_Plan_Guideline__c> pgList = EAW_TestDataFactory.createPlanGuideline(2, FALSE);
        pgList[0].Product_Type__c = 'Feature';
        pgList[1].Product_Type__c = 'Feature';
        
        insert pgList;
        
        List<EAW_Plan_Window_Guideline_Junction__c> pwgList = EAW_TestDataFactory.createPlanWindowGuideLineJunction(1, FALSE);
        pwgList[0].Plan__c = pgList[0].Id;
        pwgList[0].Window_Guideline__c = wgList[0].Id;
        
        //pwgList[1].Plan__c = pgList[1].Id;
        //pwgList[1].Window_Guideline__c = wgList[1].Id;
        
        insert pwgList;
        List<EAW_Title__c> title = EAW_TestDataFactory.createEAWTitle (1,false);//[SELECT Id FROM EAW_Title__c LIMIT 1];
        title[0].RLSE_CAL_YR__c = '2001';
        insert title;
        
        String PlanQualifierRule = '{"sObjectType": "EAW_Rule__c"}';
        String PlanQualifierRuleRuleDetail='{"sObjectType": "EAW_Rule_Detail__c", "Condition_Operator__c": ">", "Condition_Year__c": "2000", "Condition_Field__c": "Release Year"}';
        EAW_TestRuleDataFactory.savePgQualifiers( PlanQualifierRule , PlanQualifierRuleRuleDetail, pgList[0].Id);
        
       
        pgList[0].Status__c = 'Active';
       // pgList[1].Status__c = 'Active';
        
        update pgList;
        
        //List<EAW_Plan__c> planList = EAW_TestDataFactory.createPlan(2, FALSE);
        //planList[0].Plan_Guideline__c = pgList[0].Id;
        //planList[1].Plan_Guideline__c = pgList[1].Id;
        
        //insert planList;
        
        String newruleContent = '{"sObjectType":"EAW_Rule__c","Nested__c":true}';
        String newruleDetailContent = '[{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Condition_Timeframe__c":"True_Earliest"},"dateCalcs":[{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Conditional_Operand_Id__c":"aA73D000000CyJjSAK","Condition_Met_Months__c":"2","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"},{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Conditional_Operand_Id__c":"aA73D000000CyJjSAK","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"}],"nodeCalcs":[{"ruleDetail":{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":2,"Condition_Timeframe__c":"True_Earliest"},"dateCalcs":[{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":2,"Conditional_Operand_Id__c":"aA73D000000CyJjSAK","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"}],"nodeCalcs":[]}]}]';
        EAW_TestRuleDataFactory.saveRuleAndRuleDetails(newruleContent,newruleDetailContent,false,wgList[0].Id,'Start Date',String.valueOf(wgList[0].Id));
        
        //EAW_TestDataFactory.createWindow (1,true);
        //String ruleAndRuleDetailNodes = '[{"ruleDetail":{"Id":"aAM3D00000003cjWAA","Rule_No__c":"aAN3D0000008RV7WAM","Rule_Order__c":1,"Nest_Order__c":1,"Condition_Met_Months__c":"2","Condition_Met_Days__c":0,"Conditional_Operand_Id__c":"aA73D0000004mIRSAY","Condition_Operand_Details__c":"Theatrical / Bahrain / Lithuanian / Feature"},"dateCalcs":[],"nodeCalcs":[]}]';
        //EAW_TestRuleDataFactory.saveRuleAndRuleDetails(null,ruleAndRuleDetailNodes,false,wgList[0].Id,'Start Date',wgList[0].Id);
        
    }
    @isTest
    static void getAllPickListValues_Test(){
        
        EAW_TitlePlanDateManagementController.getAllPickListValues();
    }
    @isTest
    static void getWGRelaseDateGuidelineAndgetWGWithRelaseDateType_Test(){
        
        List<EAW_Window_Guideline__c> wgList =  [SELECT Id FROM EAW_Window_Guideline__c LIMIT 1];
        List<EAW_Window_Guideline_Strand__c> wgsList = [SELECT Id FROM EAW_Window_Guideline_Strand__c LIMIT 1];
        EAW_Tag__c tag = [SELECT Id FROM EAW_Tag__c LIMIT 1];
        EAW_Window__c window = [SELECT ID FROM EAW_Window__c LIMIT 1];
        List<EAW_Title__c> title = [SELECT Id FROM EAW_Title__c LIMIT 1];
        
        
        EAW_TitlePlanDateManagementController.getWGRelaseDateGuideline(new List<String>{tag.Id},new List<String>{wgsList[0].Id},new List<String>{'Library'},new List<String>{'Estimated'},String.valueOf(system.today()),String.valueOf(system.today()));
        EAW_TitlePlanDateManagementController.getWGWithRelaseDateType(new List<String>{'EST-HD'});
       
    }
    @isTest
    static void getTitlePlanAndWindows_Test(){
    
        List<EAW_Window_Guideline__c> wgList =  [SELECT Id FROM EAW_Window_Guideline__c LIMIT 1];
        List<EAW_Window_Guideline_Strand__c> wgsList = [SELECT Id FROM EAW_Window_Guideline_Strand__c LIMIT 1];
        EAW_Window__c window = [SELECT Id FROM EAW_Window__c LIMIT 1];
        EAW_Tag__c tag = [SELECT Id FROM EAW_Tag__c LIMIT 1];
        List<EAW_Title__c> title = [SELECT Id FROM EAW_Title__c LIMIT 1];
        List<EAW_Window__c> winList = [SELECT Id FROM EAW_Window__c LIMIT 1];
        winList[0].EAW_Window_Guideline__c = wgList[0].Id;
        winList[0].EAW_Title_Attribute__c = title[0].Id;
        update winList; 
        List<EAW_Plan_Guideline__c> pgList = [SELECT Id FROM EAW_Plan_Guideline__c LIMIT 1];
        
        set<Id> testIdSet= new set<Id>();
        Map<String, set<Id>> testIdMap = new Map<String,set<Id>>();
        testIdSet.add(winList[0].Id);
        testIdMap.put('1',testIdSet);
        
        EAW_TitlePlanDateManagementController.getTitlePlanAndWindows(new List<String>{'Theatrical','Pay TV'},new List<String>{'Afghanistan'},new List<String>{'Afrikaans'},new List<String>{title[0].Id},new List<String>{pgList[0].Id},new List<String>{tag.Id},new List<String>{wgList[0].Id},new List<String>{'Library'},new List<String>{'Estimated'},String.valueOf(system.today()),String.valueOf(system.today()),String.valueOf(system.today()),String.valueOf(system.today()),String.valueOf(system.today()),String.valueOf(system.today()),new List<String>{'Estimated'},new List<String>{'ACC : Accounting'},'Any','Retired Only',50,2,JSON.serialize(testIdMap),'{"selectedCol":"Status__c","selectedAlpha":"F"}');
        
        EAW_TitlePlanDateManagementController.exportWindows(new List<String>{'Theatrical','Pay TV'},new List<String>{'Afghanistan'},new List<String>{'Afrikaans'},new List<String>{title[0].Id},new List<String>{pgList[0].Id},new List<String>{tag.Id},new List<String>{wgList[0].Id},new List<String>{'Library'},new List<String>{'Estimated'},String.valueOf(system.today()),String.valueOf(system.today()),String.valueOf(system.today()),String.valueOf(system.today()),String.valueOf(system.today()),String.valueOf(system.today()),new List<String>{'Estimated'},new List<String>{'ACC : Accounting'},'Any','Retired Only','SELECT Id FROM EAW_Window__c', String.valueOf(system.today()),new List<String>{winList[0].Id},200,'{"selectedCol":"Status__c","selectedAlpha":"F"}');
    }                                                                                                                                                                                                                                                                                                                                                                                                                                   
    @isTest
    static void searchReleaseDateByCriteria_Test(){
    
        EAW_Tag__c tag = [SELECT Id FROM EAW_Tag__c LIMIT 1];
       // EAW_TestDataFactory.createEAWTitle(1, true);
        List<EAW_Title__c> title = [SELECT Id FROM EAW_Title__c LIMIT 1];
        List<EAW_Release_Date_Guideline__c> rdgList = EAW_TestDataFactory.createReleaseDateGuideline (1,true, 'Direct to Video', new List<String>{'EST-HD'},'Afghanistan','Afrikaans');
        List<EAW_Release_Date__c> rdList = EAW_TestDataFactory.createReleaseDate(1,false);
        rdList[0].Release_Date_Guideline__c = rdgList[0].Id;
        insert rdList;
        
        set<Id> testIdSet= new set<Id>();
        Map<String, set<Id>> testIdMap = new Map<String,set<Id>>();
        testIdSet.add(rdList[0].Id);
        testIdMap.put('1',testIdSet);
        
        //EAW_TitlePlanDateManagementController.searchReleaseDateByCriteria(new List<String>{'Theatrical','Pay TV'},new List<String>{'Afghanistan'},new List<String>{'Afrikaans'},new List<String>{'Library'},new List<String>{tag.Id},new List<String>{title[0].Id},String.valueOf(system.today()),String.valueOf(system.today()),new List<String>{'Estimated'},50,2,JSON.serialize(testIdMap),'{"selectedCol":"Status__c","selectedAlpha":"F"}',100,'export',String.valueOf(system.today()),new List<String>{String.valueOf(rdList[0].Id)},200,String.valueOf(rdList[0].Id),new List<String>{String.valueOf(rdgList[0].Id)});
        EAW_TitlePlanDateManagementController.searchReleaseDateByCriteria(new List<String>{'Theatrical','Pay TV'},new List<String>{'Afghanistan'},new List<String>{'Afrikaans'},new List<String>{'Library'},new List<String>{tag.Id},new List<String>{title[0].Id},String.valueOf(system.today()),String.valueOf(system.today()),new List<String>{'Estimated'},50,2,JSON.serialize(testIdMap),'{"selectedCol":"Status__c","selectedAlpha":"F"}',null,null,String.valueOf(system.today()),new List<String>{String.valueOf(rdList[0].Id)},200,String.valueOf(rdList[0].Id),new List<String>{String.valueOf(rdgList[0].Id)});
            
    } 
    @isTest
    static void coverFilterReleaseDate_Test(){
        List<EAW_Tag__c> tagList = EAW_TestDataFactory.createTag(1, True,'Release Date Tag');
        EAW_Tag__c tag = [SELECT Id FROM EAW_Tag__c LIMIT 1];
        
        //EAW_TestDataFactory.createEAWTitle(1, true);
       
        List<EAW_Title__c> title = [SELECT Id FROM EAW_Title__c LIMIT 1];
        List<EAW_Release_Date_Guideline__c> rdgList = EAW_TestDataFactory.createReleaseDateGuideline (1,true, 'Direct to Video', new List<String>{'EST-HD'},'Afghanistan','Afrikaans');
        
        List<EAW_Release_Date__c> rdList = EAW_TestDataFactory.createReleaseDate(1,false);
        rdList[0].Release_Date_Guideline__c = rdgList[0].Id;
        rdList[0].Feed_Date__c = system.today();
        rdList[0].Status__c= 'Firm';
        //rdList[0].Active__c = true;
        insert rdList;
        rdList[0].Active__c = true;
        update rdList;
        
        List<EAW_Release_Date_Tag_Junction__c> rdTagJunc = EAW_TestDataFactory.createReleaseDateTagJunction(1,false);
        rdTagJunc[0].Release_Date__c = rdList[0].Id;
        rdTagJunc[0].Tag__c = tag.Id;
        
        insert rdTagJunc;
        
       
        
        set<Id> testIdSet= new set<Id>();
        Map<String, set<Id>> testIdMap = new Map<String,set<Id>>();
        //testIdSet.add(rdList[0].Id);
        testIdMap.put('1',testIdSet);
        
        Map<String, String> testSearchMap= new Map<String,String>();
        testSearchMap.put('Status__c','= \'Firm\'');
        testSearchMap.put('releaseDateTags','\''+tag.Id+'\'');
        
        System.debug('testSearchMap:::::test:::'+testSearchMap);
        System.debug('tag :::::test:::'+tag );
        System.debug('rdList:::::test:::'+rdList);
        System.debug('testIdMap:::::test:::'+testIdMap);
        System.debug('title :::::test:::'+title );
       //new List<String>{title[0].Id} 
      EAW_TitlePlanDateManagementController.searchReleaseDateByCriteria(new List<String>(),new List<String>(),new List<String>(),new List<String>(),new List<String>(),new List<String>(),null,null,new List<String>{'Firm'},50,1,'{}',JSON.serialize(testSearchMap),null,null,String.valueOf(system.today()),new List<String>{String.valueOf(rdList[0].Id)},200,String.valueOf(rdList[0].Id),new List<String>());
      EAW_TitlePlanDateManagementController.searchReleaseDateByCriteria(new List<String>(),new List<String>(),new List<String>(),new List<String>(),new List<String>(),new List<String>(),null,null,new List<String>{'Firm'},50,1,'{}',JSON.serialize(testSearchMap),null,'export',String.valueOf(system.today()),new List<String>{String.valueOf(rdList[0].Id)},200,String.valueOf(rdList[0].Id),new List<String>());
         //EAW_TitlePlanDateManagementController.searchReleaseDateByCriteria(new List<String>(),new List<String>(),new List<String>(),new List<String>(),new List<String>(),new List<String>(),null,String.valueOf(system.today()),new List<String>(),50,1,'{}', JSON.serialize(testSearchMap),null,'export',String.valueOf(system.today()),new List<String>{String.valueOf(rdList[0].Id)},200,String.valueOf(rdList[0].Id),new List<String>());
                   
    }     
    @isTest
    static void updateWindowRecords_Test(){
        
        List<EAW_Window_Guideline__c> wgList =  [SELECT Id FROM EAW_Window_Guideline__c LIMIT 1];
        List<EAW_Window_Guideline_Strand__c> wgsList = [SELECT Id FROM EAW_Window_Guideline_Strand__c LIMIT 1];
        EAW_Window__c window = [SELECT ID FROM EAW_Window__c LIMIT 1];
        EAW_Tag__c tag = [SELECT Id FROM EAW_Tag__c LIMIT 1];
        List<EAW_Title__c> title = [SELECT Id FROM EAW_Title__c LIMIT 1];
        List<EAW_Window__c> winList = [SELECT Id FROM EAW_Window__c LIMIT 1];
        winList[0].EAW_Window_Guideline__c = wgList[0].Id;
        winList[0].EAW_Title_Attribute__c = title[0].Id;
        update winList; 
        
        Map<Id, List<Id>> testRecordMap = new Map<Id, List<Id>>();
        testRecordMap.put(winList[0].Id,new  List<Id>{tag.Id});
        EAW_TitlePlanDateManagementController.updateWindowRecords(winList,JSON.serialize(testRecordMap),'{"title":"test2","Body":"test","applyNoteValue":"append"}',new List<Id>{winList[0].Id});
        
        //(List<EAW_Window__c> windowList, String recordsMap,String notes,List<Id>noteWindows){
        
    }
    @isTest
    static void updateWindowRecordsReplace_Test(){
        
        List<EAW_Window_Guideline__c> wgList =  [SELECT Id FROM EAW_Window_Guideline__c LIMIT 1];
        List<EAW_Window_Guideline_Strand__c> wgsList = [SELECT Id FROM EAW_Window_Guideline_Strand__c LIMIT 1];
        EAW_Window__c window = [SELECT ID FROM EAW_Window__c LIMIT 1];
        EAW_Tag__c tag = [SELECT Id FROM EAW_Tag__c LIMIT 1];
        List<EAW_Title__c> title = [SELECT Id FROM EAW_Title__c LIMIT 1];
        List<EAW_Window__c> winList = [SELECT Id FROM EAW_Window__c LIMIT 1];
        winList[0].EAW_Window_Guideline__c = wgList[0].Id;
        winList[0].EAW_Title_Attribute__c = title[0].Id;
        update winList; 
        
        Map<Id, List<Id>> testRecordMap = new Map<Id, List<Id>>();
        testRecordMap.put(winList[0].Id,new  List<Id>{tag.Id});
        
        EAW_TitlePlanDateManagementController.updateWindowRecords(winList,JSON.serialize(testRecordMap),'{"title":"test2","Body":"test","applyNoteValue":"replace"}',new List<Id>{winList[0].Id});
      
    }
    @isTest
    static void updateWindowRecordClear_Test(){
        
        List<EAW_Window_Guideline__c> wgList =  [SELECT Id FROM EAW_Window_Guideline__c LIMIT 1];
        List<EAW_Window_Guideline_Strand__c> wgsList = [SELECT Id FROM EAW_Window_Guideline_Strand__c LIMIT 1];
        EAW_Window__c window = [SELECT ID FROM EAW_Window__c LIMIT 1];
        EAW_Tag__c tag = [SELECT Id FROM EAW_Tag__c LIMIT 1];
        List<EAW_Title__c> title = [SELECT Id FROM EAW_Title__c LIMIT 1];
        List<EAW_Window__c> winList = [SELECT Id FROM EAW_Window__c LIMIT 1];
        winList[0].EAW_Window_Guideline__c = wgList[0].Id;
        winList[0].EAW_Title_Attribute__c = title[0].Id;
        winList[0].Start_Date_Text__c= 'TBD';
        update winList; 
        
        Map<Id, List<Id>> testRecordMap = new Map<Id, List<Id>>();
        testRecordMap.put(winList[0].Id,new  List<Id>{tag.Id});
       
        EAW_TitlePlanDateManagementController.updateWindowRecords(winList,JSON.serialize(testRecordMap),'{"title":"test2","Body":"test","applyNoteValue":"clear"}',new List<Id>{winList[0].Id});
        EAW_TitlePlanDateManagementController.deleteWindows(winList);   
    }
     @isTest
    static void searchTitlePlanWindows_Test(){
        
        List<EAW_Window_Guideline__c> wgList =  [SELECT Id FROM EAW_Window_Guideline__c LIMIT 1];
        List<EAW_Window_Guideline_Strand__c> wgsList = [SELECT Id FROM EAW_Window_Guideline_Strand__c LIMIT 1];
        EAW_Window__c window = [SELECT ID FROM EAW_Window__c LIMIT 1];
        EAW_Tag__c tag = [SELECT Id FROM EAW_Tag__c LIMIT 1];
        List<EAW_Title__c> title = [SELECT Id FROM EAW_Title__c LIMIT 1];
        List<EAW_Window__c> winList = [SELECT Id FROM EAW_Window__c LIMIT 1];
        winList[0].EAW_Window_Guideline__c = wgList[0].Id;
        winList[0].EAW_Title_Attribute__c = title[0].Id;
        winList[0].Start_Date_Text__c= 'TBD';
        update winList; 
        
        set<Id> testIdSet= new set<Id>();
        Map<String, set<Id>> testIdMap = new Map<String,set<Id>>();
        testIdSet.add(window.Id);
        testIdMap.put('1',testIdSet);
        EAW_TitlePlanDateManagementController.searchTitlePlanWindows(new List<String>{title[0].Id},new List<String>{wgList[0].Id},50,0,JSON.serialize(testIdMap),'{"selectedCol":"Status__c","selectedAlpha":"F"}');
                            //(List<String> titles, List<String> windowNames,Integer pagesPerRecord,Integer currentPageNumber,String pageIdMapUI,String selectedFilterMap) ;
    }  
    
}