@isTest
public class EAW_RuleBuilderController_Test {
    @testSetup static void setup() {
        
        EAW_TestDataFactory.createEAWRule(2, true);
        EAW_TestDataFactory.createWindowGuideLine(1, true);
        EAW_TestDataFactory.createRuleDetail(1, true);
        EAW_TestDataFactory.createReleaseDateType(1, true);
        EAW_TestDataFactory.createEAWCustomer(1,true);
    }
    @isTest static void getRules_Test() {
       
        List<EAW_Window_Guideline__c> windowGuideLineList = [SELECT Id, Name FROM EAW_Window_Guideline__c LIMIT 1];
        
         List<EAW_Rule__c> ruleList = [SELECT Id, Name, Window_Guideline__c  FROM EAW_Rule__c LIMIT 1];
         ruleList[0].Window_Guideline__c = windowGuideLineList[0].Id;
         update ruleList;
         
         List<EAW_Rule_Detail__c> ruleDetailList = [SELECT Id, Name FROM EAW_Rule_Detail__c LIMIT 1];
         ruleDetailList[0].Rule_No__c = ruleList[0].Id;
         update ruleDetailList;
         
    
        Map<String, List<SObject>> maps = EAW_RuleBuilderController.getRules(String.ValueOf(windowGuideLineList[0].Id), 'EAW_Window_Guideline__c');
        
        System.assertEquals(true, maps != null);
    }
    @isTest static void setRules_Test() {
        
        List<EAW_Window_Guideline__c> windowGuideLineList = [SELECT Id, Name FROM EAW_Window_Guideline__c LIMIT 1];
        
        EAW_Window__c ruleDetail = new EAW_Window__c (Name='Test E', EAW_Window_Guideline__c = windowGuideLineList[0].Id);
        insert ruleDetail;
                
        EAW_RuleBuilderController.setRules(String.ValueOf(windowGuideLineList[0].Id), 'EAW_Window_Guideline__c', '', 1.0, 3.0, 'Australia', 'EST', 'Test E', 'Start Date', '', 5.0, 10.0, 'Argentina', 'Format', 
        'Test E', 'End Date');
    }
    
     @isTest static void setRulesCase_Test() {
        
        List<EAW_Release_Date_Type__c> releaseDateTypeList = [SELECT id, Name FROM EAW_Release_Date_Type__c LIMIT 1];
        List<EAW_Customer__c> customerList = [SELECT id, Name FROM EAW_Customer__c LIMIT 1];
                
        EAW_Release_Date_Guideline__c releaseDateGuideLineList= new EAW_Release_Date_Guideline__c( Product_Type__c = 'Episode', Territory__c = 'Aruba', Language__c = 'English', Customer__c = customerList[0].Id, EAW_Release_Date_Type__c = 'HV-Retail');
        insert releaseDateGuideLineList;
        
        List<EAW_Rule__c> ruleList = EAW_TestDataFactory.createEAWRule(1, true);
        
        List<EAW_Rule_Detail__c> ruleDetailList = [SELECT Id, Name FROM EAW_Rule_Detail__c LIMIT 1];
        ruleDetailList[0].Rule_No__c = ruleList[0].Id;
        update ruleDetailList;
        
        EAW_RuleBuilderController.setRules(String.ValueOf(releaseDateGuideLineList.Id), 'EAW_Window__c', String.ValueOf(ruleList[0].Id), 1.0, 3.0, 'Australia', 'EST', 'Test E', 'Start Date', '', 5.0, 10.0, 'Argentina', 'Format', 
        'Test E', 'End Date');
    }
     @isTest static void setRulesCase_Test2() {
        
        List<EAW_Release_Date_Type__c> releaseDateTypeList = [SELECT id, Name FROM EAW_Release_Date_Type__c LIMIT 1];
        List<EAW_Customer__c> customerList = [SELECT id, Name FROM EAW_Customer__c LIMIT 1];
        
                
        EAW_Release_Date_Guideline__c releaseDateGuideLineList= new EAW_Release_Date_Guideline__c( Product_Type__c = 'Episode', Territory__c = 'Aruba', Language__c = 'English', Customer__c = customerList[0].Id, EAW_Release_Date_Type__c = 'Television');
        insert releaseDateGuideLineList;
        
        List<EAW_Rule__c> ruleList = EAW_TestDataFactory.createEAWRule(1, true);
        
        List<EAW_Rule_Detail__c> ruleDetailList = [SELECT Id, Name FROM EAW_Rule_Detail__c LIMIT 1];
        ruleDetailList[0].Rule_No__c = ruleList[0].Id;
        update ruleDetailList;
        
        EAW_RuleBuilderController.setRules(String.ValueOf(releaseDateGuideLineList.Id), 'EAW_Window__c', String.ValueOf(ruleList[0].Id), 1.0, 3.0, 'Australia', 'EST', 'Test E', 'Start Date', String.ValueOf(ruleList[0].Id), 5.0, 10.0, 'Argentina', 'Format', 
        'Test E', 'End Date');
    }
    
}