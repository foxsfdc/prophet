@isTest
public class EAW_FieldDescriber_Test {

      
    @isTest static void testgetSessionId(){
        
        String session = EAW_FieldDescriber.getSessionId();
        System.assertEquals(true, session != null);        
    }
    
     @isTest static void testdependent_Test(){
     

         Test.startTest();
         
             Test.setMock(HttpCalloutMock.class, new EAW_FieldDescriber_CalloutMock());
             EAW_FieldDescriber.dependentPicklistValuesWrapper picklist = EAW_FieldDescriber.getDependentPicklistValues('aA43D0000004Cqu', 'EAW_Plan_Guideline__c','Release_Type__c');

         Test.stopTest();
         
         System.assertEquals(true, picklist != null);
     }
}