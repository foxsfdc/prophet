@isTest
public class EAW_NewWindowGuidelineStrandCntrl_Test {
	
    @isTest static void EAW_NewWindowGuidelineStrandController_Test(){
        EAW_Window_Guideline__c newWg = new EAW_Window_Guideline__c ( Status__c = 'Draft', Product_Type__c = 'Compilation Episode', Window_Type__c = 'First-Run', Window_Guideline_Alias__c = 'Test');
        insert newWg; 
        EAW_Window_Guideline_Strand__c windowGuideLineStrand =  new EAW_Window_Guideline_Strand__c(Language__c = 'Tamil',Media__c='Basic TV	',Territory__c='Afghanistan', EAW_Window_Guideline__c = newWg.Id, License_Type__c = 'Exhibition License');
        insert windowGuideLineStrand;
        newWg.Status__c = 'Active';
        update newWg;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(windowGuideLineStrand);
        EAW_NewWindowGuidelineStrandController windowController = new EAW_NewWindowGuidelineStrandController(sc);
    }
}