/**
 * This class contains unit tests for validating the behavior of EAW_PlanController
 * 
 */
@isTest
private class EAW_PlanController_Test {

    @testSetup static void setupTestData() {
                
        EAW_TestDataFactory.createPlanGuideline(1, True);
        EAW_TestDataFactory.createWindowGuideLine(1,True);
        
     //   List<EDM_GLOBAL_TITLE__c> edmTitle = EAW_TestDataFactory.createEDMTitle (1, true);
        List<EAW_title__c> eawTitle = EAW_TestDataFactory.createEAWTitle(1, true);
        //LRCC-1510 - Converting Name field From Text to Auto Number, before that remove the references.
        List<EAW_Box_Office__c> boxOfficeList = new List<EAW_Box_Office__c> {
            new EAW_Box_Office__c(/*Name  = 'test tvd box office',*/ Title_Attribute__c = eawTitle[0].Id,Territory__c = 'Albania')
        };
        insert boxOfficeList;
    }
    
    @isTest static void testEAWPlanController() {
        
        Map<String,String>  objFieldSetMap = new Map<String, String>();
        objFieldSetMap.put('EAW_Plan_Guideline__c','Plan_Guideline_Name');
        EAW_PlanController.getFields(objFieldSetMap);
        //LRCC-1510 - Converting Name field From Text to Auto Number, before that remove the references.
        EAW_Box_Office__c boxOfficeListQuired = [SELECT id/*,Name*/ FROM EAW_Box_Office__c LIMIT 1];
        
        EAW_Plan_Guideline__c planList = [ Select Id, Name, Media__c, Product_Type__c, Territory__c, Status__c, CreatedDate From EAW_Plan_Guideline__c LIMIT 1];
        System.debug('::: PlanList :::'+planList);

        EAW_PlanController.searchPlanByCriteria(objFieldSetMap, 'Active', 'New Release', new List<String> {}, new List<String> {'Australia;Baltics'}, new List<String> {'Afghanistan'}, new List<String>{'English,Somali'}, new List<String> {'care'}, new List<String> {''}, 1, 1, JSON.serialize(new Map<Integer,Set<Id>>{1 => new set<Id>{planList.Id}}), '{"selectedCol":"Status__c","selectedAlpha":"F"}', 'true', NULL, NULL, 50);
        EAW_PlanController.searchPlanByCriteria(objFieldSetMap, 'Active', 'New Release', new List<String> {planList.Id}, new List<String> {'Australia'}, new List<String> {}, new List<String>{}, new List<String> {'care'}, new List<String> {}, 100, 2, JSON.serialize(new Map<Integer,Set<Id>>{1 => new set<Id>{planList.Id}}), '{"selectedCol":"Status__c","selectedAlpha":"F"}', 'true', NULL, NULL, 50);
        EAW_PlanController.searchPlanByCriteria(objFieldSetMap, 'Active', 'New Release', new List<String> {}, new List<String> {'Australia'}, new List<String> {'Afghanistan'}, new List<String>{'English,Somali'}, new List<String> {'care'}, new List<String> {''}, 100, 2, JSON.serialize(new Map<Integer,Set<Id>>{1 => new set<Id>{planList.Id}, 2 => new set<Id>{planList.Id}}), '{"selectedCol":"Status__c","selectedAlpha":"F"}', 'export', String.valueOf(planList.CreatedDate), new List<String>{planList.Id}, 50);
        
        EAW_PlanController.saveRecords(JSON.serialize(new List<EAW_Plan_Guideline__c> {planList }));
        EAW_PlanController.clonePlanNoWindows(JSON.serialize(new List<EAW_Plan_Guideline__c> {planList }));
        EAW_Plan_Guideline__c clonePlanList = [ Select Id, Name, Media__c, Product_Type__c, Territory__c, Status__c From EAW_Plan_Guideline__c WHERE Name = 'Test Plan Guideline 0 clone' LIMIT 1];
        EAW_PlanController.clonePlanAndWindows(JSON.serialize(new List<EAW_Plan_Guideline__c> {clonePlanList}));
        EAW_Plan_Guideline__c cloneclonePlanList = [ Select Id, Name, Media__c, Product_Type__c, Territory__c, Status__c From EAW_Plan_Guideline__c WHERE Name = 'Test Plan Guideline 0 clone clone' LIMIT 1];
        EAW_PlanController.clonePlanSameWindows(JSON.serialize(new List<EAW_Plan_Guideline__c> {cloneclonePlanList}));
        EAW_PlanController.getRecordDetail(String.ValueOf(planList.Id));
        EAW_PlanController.getRecordDetail(String.ValueOf(planList.Id));
        EAW_PlanController.deactivatePlans(JSON.serialize(new List<EAW_Plan_Guideline__c> {planList }));        
        Id newVersionId = EAW_PlanController.getNewVersionId(planList.Id, 'Test Title', 'Test body');
        EAW_PlanController.getAllVersions(newVersionId);
        List<EAW_Box_Office__c> saveofficeList = EAW_PlanController.saveEAWBoxOfficeRecords(JSON.serialize(new List<EAW_Box_Office__c> {boxOfficeListQuired }));
        system.assertEquals(True, saveofficeList != null);

    }
    
    @isTest static void testEAWPlanDelete() {
        
        
        EAW_Plan_Guideline__c planGuideLineList  = new EAW_Plan_Guideline__c(Name = 'test plan guideLine', Product_Type__c = 'Shorts');
       
        insert planGuideLineList;
        
        EAW_PlanController.deleteDraftPlans(JSON.serialize(new List<EAW_Plan_Guideline__c> {planGuideLineList}));
    
    }
    /*
    @isTest static void testEAWPlanDeleteBox() {
    
        //List<EDM_GLOBAL_TITLE__c> edmTitleList = EAW_TestDataFactory.createEDMTitle (1, true);
        List<EAW_Title__c> eawTitle = EAW_TestDataFactory.createEAWTitle(1, true);
        List<String> boxOfficeIdList = new List<String>();
        //LRCC-1510 - Converting Name field From Text to Auto Number, before that remove the references.
        List<EAW_Box_Office__c> boxOfficeTVDList = new List<EAW_Box_Office__c> {
            new EAW_Box_Office__c(*//*Name  = 'test tvd box office',*//* Title_Attribute__c = eawTitle [0].Id, Territory__c = 'Afghanistan')
        };
        system.debug(';;;;;'+[select Title_Attribute__c, Territory__c from EAW_Box_Office__c]);
        system.debug(';;;;;'+boxOfficeTVDList);
        insert boxOfficeTVDList;
        for(EAW_Box_Office__c boxOffice :  boxOfficeTVDList ) {
            boxOfficeIdList.add(boxOffice.Id);
        }
        
        //EAW_PlanController.deleteBoxOfficeRecords(boxOfficeIdList);
        
    }
    */
    @isTest static void testExibition_Test() {
         
        EAW_Plan_Guideline__c planGuideLineListReg  = new EAW_Plan_Guideline__c(Name = 'test plan guideLine', Product_Type__c = 'Shorts');
       
        insert planGuideLineListReg  ;
        
        List<EAW_Window_Guideline__c> windowGuideLineList = [SELECT Id,Name FROM EAW_Window_Guideline__c LIMIT 1];  
        
        List<EAW_Plan_Window_Guideline_Junction__c> planWindowList = new List<EAW_Plan_Window_Guideline_Junction__c> {
            new EAW_Plan_Window_Guideline_Junction__c (Window_Guideline__c = windowGuideLineList[0].Id, Plan__c = planGuideLineListReg.Id)
        };
        insert planWindowList;        
         
        List<String> exbList = EAW_PlanController.getExhibitionData(planGuideLineListReg.Id);
        
        System.assertEquals(true, exbList != null);
    }
    
    @isTest static void testgetRelatedWindowGuidelinesMethod() {
    
        List<Id> planGuidelineIds = new List<Id>();
        
        for(EAW_Plan_Guideline__c planGuideline : [ Select Id From EAW_Plan_Guideline__c ]) {
            
            planGuidelineIds.add(planGuideline.Id);
        }
        
        List < EAW_Plan_Window_Guideline_Junction__c > junctions = EAW_PlanController.getRelatedWindowGuidelines(planGuidelineIds);
        System.assertEquals(junctions != null, true);
    }    
    
    @isTest static void testgetPlanGuidelineMethod() {
    
        EAW_Plan_Guideline__c planGuideline = EAW_PlanController.getPlanGuideline([ Select Id From EAW_Plan_Guideline__c LIMIT 1][0].Id);
        System.assertEquals(planGuideline != null, true);
    }
    
    @isTest static void testautoSelectedMethod() {
    
        Sales_Regions__c testSalesRegion = new Sales_Regions__c(Name = 'Test Sales region', Territories__c = 'Territory 1;Territory2');
        insert testSalesRegion;
        
        List<EAW_LookupSearchResult> result = EAW_PlanController.autoSelected(testSalesRegion.Name);
        System.assertEquals(result != null, true);
    }    
}