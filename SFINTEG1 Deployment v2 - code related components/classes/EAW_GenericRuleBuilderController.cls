public class EAW_GenericRuleBuilderController {
    
    @AuraEnabled
    public static ruleWrapper getRulesAndDetails(String releasedateGuidelineId){
        
        ruleWrapper newRuleWrapper = new ruleWrapper();
        List<EAW_Rule__c> ruleList = new List<EAW_Rule__c>();
        List<EAW_Rule_Detail__c> parentRuleDetailList = new List<EAW_Rule_Detail__c>();
        List<EAW_Rule_Detail__c> ruleDetailList = new List<EAW_Rule_Detail__c>();
        EAW_Release_Date_Guideline__c releaseDateGuideline = new EAW_Release_Date_Guideline__c(); 
        
        try {
            newruleWrapper.releaseWindowDateWrapper = retrieveRelatedReleaseAndWindowDates(releasedateGuidelineId);
                
            //LRCC-1540 Allow Manual field added
            releaseDateGuideline = [SELECT Id, Active__c, All_Rules__c, Allow_Manual__c FROM EAW_Release_Date_Guideline__c WHERE Id = :releasedateGuidelineId LIMIT 1];
            ruleList = [SELECT Id, Release_Date_Guideline__c, Criteria__c,Condition_Field__c, Condition_Type__c,Nested__c,Nested_Times__c,Date_Type_Description__c,Product_Type__c,Operator__c,Conditional_Operand_Id__c,Territory__c,Multi_conditional_operands__c, Condition_Operand_Details__c FROM EAW_Rule__c WHERE Release_Date_Guideline__c = :releasedateGuidelineId LIMIT 1];
            
            if(ruleList != null && ruleList.size() > 0) {
                
                newruleWrapper.rule = ruleList[0];
                
                parentRuleDetailList = [SELECT Id,Rule_No__c,Rule_Order__c,Nest_Order__c,Condition_Timeframe__c,Condition_Met_Months__c, Condition_Met_Days__c, Condition_Met_Media__c, Condition_Met_Territory__c,Condition_Type__c,Condition_Field__c,Condition_Criteria__c, Condition_Set_Operator__c,Conditional_Operand_Id__c,Condition_Date_Duration__c,Condition_Criteria_Amount__c,Day_of_the_Week__c,Multi_conditional_operands__c,Parent_Condition_Type__c, Condition_Operand_Details__c FROM EAW_Rule_Detail__c WHERE Rule_No__c = :ruleList[0].Id AND Parent_Rule_Detail__c = null];
                ruleDetailList = [SELECT id, Rule_No__c, Condition_Set_Field__c,Parent_Rule_Detail__c , Condition_Met_Months__c, Condition_Met_Days__c, Condition_Met_Media__c, Condition_Met_Territory__c,Conditional_Operand_Id__c,Parent_Condition_Type__c,Condition_Type__c,Condition_Field__c,Condition_Criteria__c,
                                                           Start_Date__c, End_Date__c, WindowName__c, WindowName__r.Name, Condition_Not_Met_Days__c, Condition_Not_Met_Months__c, Condition_Not_Met_Media__c,Condition_Not_Met_Territory__c,Condition_Set_Operator__c,Condition_Operand_Details__c ,
                                                           Alt_Conditon_Days__c, Alt_Condition_Months__c, Alt_Condition_Media__c,Alt_Condition_Territory__c,Rule_Order__c,Nest_Order__c,Condition_Timeframe__c,Condition_Date_Duration__c,Condition_Criteria_Amount__c,Day_of_the_Week__c,Multi_conditional_operands__c
                                                           FROM EAW_Rule_Detail__c WHERE Rule_No__c = :ruleList[0].Id AND Parent_Rule_Detail__c != null]; 
                newruleWrapper.parentRuleDetailList = parentRuleDetailList;
                newruleWrapper.ruleDetailList = ruleDetailList;
            }
            //LRCC-1631
            newruleWrapper.isActive = releaseDateGuideline.Active__c;
            //LRCC-1540
            newruleWrapper.isAllowManual = releaseDateGuideline.Allow_Manual__c;
        } 
        catch(Exception e) {
            newruleWrapper.rule = null;
        }
        return newRuleWrapper;
    }
    
    @AuraEnabled
    public static EAW_FieldDescriber.dependentPicklistValuesWrapper getDependentFieldSet(String fieldName,String recordTypeName){
        Id ruleDetailRecordTypeId = Schema.SObjectType.EAW_Rule_Detail__c.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        EAW_FieldDescriber.dependentPicklistValuesWrapper dependentPicklistValuesWrapper = EAW_FieldDescriber.getDependentPicklistValues(ruleDetailRecordTypeId,'EAW_Rule_Detail__c',fieldName);
        system.debug(':::: Final Dependent Picklist Values ::::'+dependentPicklistValuesWrapper);
        
        Map<String,List<EAW_FieldDescriber.picklistValuesWrapper>> dependentPicklistValues = new Map<String,List<EAW_FieldDescriber.picklistValuesWrapper>>();
        List<EAW_FieldDescriber.picklistValuesWrapper> releaseDateStatusPicklist = getReleaseDateStaus('EAW_Release_Date__c');
        List<EAW_FieldDescriber.picklistValuesWrapper> windowStatusPicklist = getReleaseDateStaus('EAW_Window__c');
        dependentPicklistValues.put('Release Date Status',releaseDateStatusPicklist);
        dependentPicklistValues.put('Window Schedule Status',windowStatusPicklist);
        dependentPicklistValuesWrapper.dependentPicklistValues = dependentPicklistValues;
       // if(dependentPicklistValuesWrapper != null && dependentPicklistValuesWrapper.dependentPicklistValues.get('Release Date Status').size() > 0){
          //  List<EAW_FieldDescriber.picklistValuesWrapper> releaseDateStatusPicklist = getReleaseDateStaus();
           // dependentPicklistValuesWrapper.dependentPicklistValues.put('Release Date Status',releaseDateStatusPicklist);
        //}
        return dependentPicklistValuesWrapper;
    }
    private static List<EAW_FieldDescriber.picklistValuesWrapper> getReleaseDateStaus(String objectName){
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(objectName);//From the Object Api name retrieving the SObject
        Sobject Object_name = targetType.newSObject();
        Schema.sObjectType sobject_type = Object_name.getSObjectType(); //grab the sobject that was passed
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap();
        List<Schema.PicklistEntry> ple = field_map.get('Status__c').getDescribe().getPicklistValues();
        List<EAW_FieldDescriber.picklistValuesWrapper> pickListValuesList = new List<EAW_FieldDescriber.picklistValuesWrapper>();
        for( Schema.PicklistEntry pickListVal : ple){
            EAW_FieldDescriber.picklistValuesWrapper newpickListWrapper = new EAW_FieldDescriber.picklistValuesWrapper(pickListVal.getLabel(),pickListVal.getValue());
            pickListValuesList.add(newpickListWrapper);
        }
        return pickListValuesList;
    }
    private static List<ReleaseWindowDateWrapper> retrieveRelatedReleaseAndWindowDates(String recordId) {
        
        List<ReleaseWindowDateWrapper> picklistWrappers = new List<ReleaseWindowDateWrapper> ();
        
        EAW_Release_Date_Guideline__c rec = [ SELECT Id, Product_Type__c FROM EAW_Release_Date_Guideline__c WHERE Id =: recordId LIMIT 1 ];
        //List<String> multiPicklistStrings = new List<String> ();
       // multiPicklistStrings.addAll(rec.Product_Type__c);
        
        for ( EAW_Release_Date_Guideline__c  rdg : [ SELECT Id,Name, EAW_Release_Date_Type__c , Territory__c, Language__c, Product_Type__c, Active__c FROM EAW_Release_Date_Guideline__c WHERE Product_Type__c LIKE : rec.Product_Type__c AND Id != :recordId AND Active__c = TRUE ORDER BY Name ASC] ) {
            
            if (rdg.EAW_Release_Date_Type__c != null && rdg.Territory__c != null) {
                //String pickVal = rdg.EAW_Release_Date_Type__c + ' / ' +rdg.Territory__c;
                picklistWrappers.add(
                    new ReleaseWindowDateWrapper(rdg.Name, rdg.Id)
                ); 
            }
        }
        return picklistWrappers;
    }

    @AuraEnabled
    public static List<EAW_Rule_Detail__c> saveRuleAndRuleDetails(String rule,String ruleAndRuleDetailNodes,List<String> ruleDetilsToDelete,String ruleText,String releaseDateGuidelineId){
        system.debug('saveee');
        if(ruleDetilsToDelete != null){
            deleteRuleAndDetails(false,false,null,ruleDetilsToDelete,releaseDateGuidelineId);
        }
        Id ruleRecordTypeId = Schema.SObjectType.EAW_Rule__c.getRecordTypeInfosByName().get('Release Date Calculation').getRecordTypeId();
        Id ruleDetailRecordTypeId = Schema.SObjectType.EAW_Rule_Detail__c.getRecordTypeInfosByName().get('Release Date Calculation').getRecordTypeId();
        
        EAW_Rule__c newRule = (EAW_Rule__c)JSON.deserialize(rule,EAW_Rule__c.class);
        newRule.RecordTypeId = ruleRecordTypeId;
        system.debug('newRule::::'+newRule);
        upsert newRule;
        List<ruleDetailNodeWrapper> newruleDetailNodeWrapper = new List<ruleDetailNodeWrapper>();
        newruleDetailNodeWrapper = (List<ruleDetailNodeWrapper> )JSON.deserialize(ruleAndRuleDetailNodes,List<ruleDetailNodeWrapper> .class);
        List<EAW_Rule_Detail__c> parentRuleDetailList = new List<EAW_Rule_Detail__c>();
        for(ruleDetailNodeWrapper newruleDetail : newruleDetailNodeWrapper){
            EAW_Rule_Detail__c parentRuleDetail = newruleDetail.ruleDetail;
            parentRuleDetail.Rule_No__c = newRule.Id;
            parentRuleDetail.RecordTypeId = ruleDetailRecordTypeId;
            parentRuleDetailList.add(parentRuleDetail);
        }       
        system.debug('parentRuleDetailList::::'+parentRuleDetailList);
        upsert parentRuleDetailList;
        List<EAW_Rule_Detail__c> recursiveRuleDetailList = new List<EAW_Rule_Detail__c>();
        List<EAW_Rule_Detail__c> ruleDetailList = new List<EAW_Rule_Detail__c>();
        for(ruleDetailNodeWrapper newruleDetailNode : newruleDetailNodeWrapper){
            if(newruleDetailNode.dateCalcs != null && newruleDetailNode.nodeCalcs != null){
                recursiveRuleDetailList = new List<EAW_Rule_Detail__c>();
                ruleDetailList.addAll(recursiveNodeSave(ruleDetailRecordTypeId,recursiveRuleDetailList,newRule.Id,newruleDetailNode.ruleDetail.Id,newruleDetailNode.dateCalcs,newruleDetailNode.nodeCalcs));            
            }
        }       
        system.debug('ruleDetailList::::'+ruleDetailList);
        upsert ruleDetailList;
        if(ruleDetailList != null && ruleDetailList.size() >= 1){
            newRule.Nested_Times__c = ruleDetailList[ruleDetailList.size()-1].Nest_Order__c;
        }
        else{
            newRule.Nested_Times__c = 1;
        }
        system.debug('newRule::::'+newRule);
        update newRule;
        //To dynmaic creation of Release Date for each Title
        //LRCC:1137
        EAW_Release_Date_Guideline__c releaseDateGuideline = [SELECT Id, Active__c,All_Rules__c FROM EAW_Release_Date_Guideline__c WHERE Id = :newRule.Release_Date_Guideline__c LIMIT 1];
        releaseDateGuideline.All_Rules__c = ruleText;
        system.debug('releaseDateGuideline::::'+releaseDateGuideline);
        update releaseDateGuideline;
        if(releaseDateGuideline != NULL && releaseDateGuideline.Active__c == true){
            //LRCC-1478
            /*EAW_QueueableDateCalculationClass caclQueueJob = new EAW_QueueableDateCalculationClass();
            caclQueueJob.releaseDateGuidelineIdSet = new Set<Id>{releaseDateGuideline.Id};
            caclQueueJob.filteredTitleIdSet = Null;
            caclQueueJob.windowGuidelineIdSet = Null;
            String jobId = System.enqueueJob(caclQueueJob);
            System.debug(':::: Job Id ::::'+ jobId);
            //Batch processing call
            EAW_CreateRDForNewRDGBatch dateCalcBatch = new EAW_CreateRDForNewRDGBatch(new Set<Id>{releaseDateGuideline.Id},null);
            dateCalcBatch.releaseDateGuidelineIdSet = new Set<Id>{releaseDateGuideline.Id}; 
            ID batchprocessid = Database.executeBatch(dateCalcBatch, 200);*/
            
            EAW_RDGDateCalculationBatchClass releaseDateBatch = new EAW_RDGDateCalculationBatchClass(new Set<Id>{releaseDateGuideline.Id});
            releaseDateBatch.titleIdSet = NULL;
            releaseDateBatch.windowGuidelineIdSet = NULL;
            ID batchprocessid = Database.executeBatch(releaseDateBatch, 200);
        }
        return ruleDetailList;
    }
    private static List<EAW_Rule_Detail__c> recursiveNodeSave(Id ruleDetailRecordTypeId,List<EAW_Rule_Detail__c> ruleDetailList,Id ruleId,Id parentRuleDetailId,List<EAW_Rule_Detail__c> dateCalcs,List<ruleDetailNodeWrapper> nodeCalcs){
        
        for(EAW_Rule_Detail__c dateCalcRuleDetail : dateCalcs){
            dateCalcRuleDetail.Rule_No__c = ruleId;
            dateCalcRuleDetail.RecordTypeId = ruleDetailRecordTypeId;
            dateCalcRuleDetail.Parent_Rule_Detail__c = parentRuleDetailId;
            ruleDetailList.add(dateCalcRuleDetail);
        }       

        List<EAW_Rule_Detail__c> childComeParentList = new List<EAW_Rule_Detail__c>();
        for(ruleDetailNodeWrapper ruleDetailNode : nodeCalcs){          
            EAW_Rule_Detail__c childComeParent = ruleDetailNode.ruleDetail;
            childComeParent.Rule_No__c = ruleId;
            childComeParent.RecordTypeId = ruleDetailRecordTypeId;
            childComeParent.Parent_Rule_Detail__c = parentRuleDetailId;
            childComeParentList.add(childComeParent);           
        }
        upsert childComeParentList;
        for(ruleDetailNodeWrapper ruleDetailNode : nodeCalcs){
            recursiveNodeSave(ruleDetailRecordTypeId,ruleDetailList,ruleId,ruleDetailNode.ruleDetail.Id,ruleDetailNode.dateCalcs,ruleDetailNode.nodeCalcs);
        }
        return ruleDetailList;
    }
    @AuraEnabled
    public static String deleteRuleAndDetails(Boolean isRule,Boolean isDateCalc,String ruleOrRuleDetailId,List<String> ruleDetilsToDelete,String releaseDateGuidelineId){
        try{
            if(isRule == true){
                EAW_Rule__c rule = [SELECT Id FROM EAW_Rule__c WHERE Id = :ruleOrRuleDetailId];
                delete rule;
                //LRCC - 1410
                EAW_Release_Date_Guideline__c releaseDateGuideline = [SELECT Id, Active__c,All_Rules__c FROM EAW_Release_Date_Guideline__c WHERE Id = :releaseDateGuidelineId LIMIT 1];
                if(releaseDateGuideline != null && releaseDateGuideline.All_Rules__c != null){
                    releaseDateGuideline.All_Rules__c = '';
                    update releaseDateGuideline;
                }
                return 'success';
            } else{
                
                Map<Id,Set<Id>> ruleDetailMap = new Map<Id,Set<Id>>();
                List<EAW_Rule_Detail__c> ruleDetailList = new List<EAW_Rule_Detail__c> ();
                
                if(String.isNotBlank(releaseDateGuidelineId)) {
                    ruleDetailList = [SELECT Id,Rule_No__r.Release_Date_Guideline__c,Parent_Rule_Detail__r.Id FROM EAW_Rule_Detail__c WHERE Rule_No__r.Release_Date_Guideline__c =:releaseDateGuidelineId AND Parent_Rule_Detail__c != null];
                }
                for(EAW_Rule_Detail__c ruleDetail : ruleDetailList){
                    Set<Id> temp = new Set<Id>();
                    if(ruleDetailMap.containsKey(ruleDetail.Parent_Rule_Detail__c)){
                        temp = ruleDetailMap.get(ruleDetail.Parent_Rule_Detail__c);
                        temp.add(ruleDetail.Id);
                    } else{
                        temp.add(ruleDetail.Id);
                    }
                    ruleDetailMap.put(ruleDetail.Parent_Rule_Detail__c,temp);
                }
                Set<Id> ruleDetailSetId = new Set<Id>();
                
                for(String ruleDetailId : ruleDetilsToDelete){
                    if(ruleDetailMap.containsKey(ruleDetailId)){
                        ruleDetailSetId.addAll(setChildRuleDetailById(ruleDetailMap,ruleDetailId));
                        ruleDetailSetId.add(ruleDetailId);
                    } else{
                        ruleDetailSetId.add(ruleDetailId);
                    }
                }
                system.debug('ruleDetailSetId:::::'+ruleDetailSetId.size()+':::::'+ruleDetailSetId);
                
                if(ruleDetailSetId.size()> 0){
                    List<EAW_Rule_Detail__c> parentRuleDetail = [SELECT Id,Rule_No__c FROM EAW_Rule_Detail__c WHERE Id IN :ruleDetailSetId];
                    system.debug(parentRuleDetail);
                    delete parentRuleDetail;
                }
                return 'success';
                
            }
        } catch (Exception e){
            throw New AuraHandledException(e.getMessage());
        }        
    }
    public static Set<Id> setChildRuleDetailById(Map<Id,Set<Id>> ruleDetailMap,Id parentRuleDetailId){
        Set<Id> wholeIdSet = new Set<Id>();
        for(Id ruleDetailId : ruleDetailMap.get(parentRuleDetailId)){
            system.debug('Map:::'+ruleDetailId);
            Set<Id> ruleDetailSet = new Set<Id>();
            if(ruleDetailMap.containsKey(ruleDetailId)){
                ruleDetailSet.add(ruleDetailId);
                ruleDetailSet.addAll(setChildRuleDetailById(ruleDetailMap,ruleDetailId));
            } else{
                ruleDetailSet.add(ruleDetailId);                
            }
            wholeIdSet.addAll(ruleDetailSet);
            ruleDetailMap.put(parentRuleDetailId,wholeIdSet);        
        }
        return wholeIdSet;
    }
    public class ruleDetailNodeWrapper{
        @AuraEnabled
        public EAW_Rule_Detail__c ruleDetail;
        @AuraEnabled
        public List<EAW_Rule_Detail__c> dateCalcs;
        @AuraEnabled
        public List<ruleDetailNodeWrapper> nodeCalcs;
    }
    public class ruleWrapper{
        @AuraEnabled
        public EAW_Rule__c rule;
        @AuraEnabled
        public List<EAW_Rule_Detail__c> parentRuleDetailList;
        @AuraEnabled
        public List<EAW_Rule_Detail__c> ruleDetailList;
        @AuraEnabled
        public List<ReleaseWindowDateWrapper> releaseWindowDateWrapper;
        //LRCC-1631
        @AuraEnabled
        public Boolean isActive;
        //LRCC - 1540
        @AuraEnabled
        public Boolean isAllowManual;
    }
    public class ReleaseWindowDateWrapper {
        @AuraEnabled
        public String label;
        @AuraEnabled
        public String value;
        
        public ReleaseWindowDateWrapper(String label, String value) {
            this.label = label;
            this.value = value;
        }
    }
}