@isTest
public with sharing class EAW_TitleEDMBoxOfficesController_Test {
    @isTest
    static void method1(){
        EDM_GLOBAL_TITLE__c title = new EDM_GLOBAL_TITLE__c(Name='Test');
         insert title;
         EAW_Title__c eawTitle = new EAW_Title__c(Title_EDM__c = title.Id);
         insert eawTitle;
         List<EAW_Box_Office__c> boxOfficeList = new List<EAW_Box_Office__c>();
         //LRCC-1510 - Converting Name field From Text to Auto Number, before that remove the references.
         EAW_Box_Office__c boxoffice1 = new EAW_Box_Office__c(/*Name='Test',*/Title_Attribute__c = eawTitle.Id,Territory__c='Abu Dhabi ^');
         boxOfficeList.add(boxoffice1);
         insert boxOfficeList;
         
         //boxOfficeList[0].Name = 'Test1';
         boxOfficeList[0].Territory__c='Afghanistan';
         string boxOfficeListStr = JSON.serialize(boxOfficeList);
         
         EAW_TitleEDMBoxOfficesController.getFields(new Map<String,String>{'EAW_Release_Date__c'=>'Search_Result_Fields'});
         EAW_TitleEDMBoxOfficesController.searchForTVDBoxOffices(eawTitle.Id);
         EAW_TitleEDMBoxOfficesController.saveBoxOfficeRecords(boxOfficeListStr);
         EAW_TitleEDMBoxOfficesController.massUpdateTVDBoxOfficeRecords(new List<String>{boxOfficeList[0].Id},10,10,10,'Override');
         
         EAW_Box_Office__c boxOffice = [SELECT Name,TVD_US_Box_Office__c,TVD_Local_Currency_Value__c FROM EAW_Box_Office__c];
         //system.assertEquals('Test1',boxOffice.Name);
         //system.assertEquals('Afghanistan',boxOffice.Name);
         system.assertEquals(10,boxOffice.TVD_US_Box_Office__c);
         system.assertEquals(10,boxOffice.TVD_Local_Currency_Value__c);
         
         EAW_TitleEDMBoxOfficesController.deleteBoxOfficeRecords(new List<String>{boxOfficeList[0].Id});
         try{
         EAW_Box_Office__c boxOff = [SELECT Name,TVD_US_Box_Office__c,TVD_Local_Currency_Value__c FROM EAW_Box_Office__c];
         }catch(exception ex){
             system.assert(ex.getMessage().Contains('List has no rows for assignment to SObject'));
         }
         
    }
    @isTest
    static void failureMethod(){
        
        try {
            EAW_TitleEDMBoxOfficesController.deleteBoxOfficeRecords(new List<String>{''});
        }
        catch(Exception ex) {
            
            System.assertEquals('Script-thrown exception', ex.getMessage());
        }
        try {
            EAW_TitleEDMBoxOfficesController.massUpdateTVDBoxOfficeRecords(new List<String>{''}, 10,10,10,'Override');
        }
        catch(Exception ex) {
            
            System.assertEquals('Script-thrown exception', ex.getMessage());
        }
    }
}