public class EAW_PlanGuidelineRuleBuilderController {
    
    @AuraEnabled
    public static List<EAW_Rule__c> saveConditionSetFields(String ruleWrapper,String planGuidelineId,List<String> rulesToDelete,List<String> ruleDetailsToDelete,String ruleJSON){
        try{
            if(ruleDetailsToDelete != null){
                List<EAW_Rule_Detail__c> ruleDetailList = [SELECT Id FROM EAW_Rule_Detail__c WHERE ID IN :ruleDetailsToDelete];
                delete ruleDetailList;
            }
            if(rulesToDelete != null){
                List<EAW_Rule__c> ruleListToDelete = [SELECT Id FROM EAW_Rule__c WHERE Id IN :rulesToDelete];
                delete ruleListToDelete;
            }
            List<ruleAndRuleDetailWrapper> ruleAndRuleDetailWrapperToSave = (List<ruleAndRuleDetailWrapper>)JSON.deserialize(ruleWrapper, List<ruleAndRuleDetailWrapper>.class);
            System.debug(ruleAndRuleDetailWrapperToSave);
            List<EAW_Rule__c> ruleList = new List<EAW_Rule__c>();
            String ruleRecordTypeId = Schema.SObjectType.EAW_Rule__c.getRecordTypeInfosByName().get('Qualifier').getRecordTypeId(); 
            String ruleDetailRecordType = Schema.SObjectType.EAW_Rule_Detail__c.getRecordTypeInfosByName().get('Qualifier').getRecordTypeId();
            
            for(ruleAndRuleDetailWrapper ruleAndRuleDetailWrapper : ruleAndRuleDetailWrapperToSave){
                EAW_Rule__c newRule = ruleAndRuleDetailWrapper.rule;
                newRule.Plan_Guideline__c = planGuidelineId;
                newRule.RecordTypeId = ruleRecordTypeId;
                ruleList.add(newRule);
            }
            upsert ruleList;
            List<EAW_Rule_Detail__c> ruleDetailList = new List<EAW_Rule_Detail__c>();
            for(ruleAndRuleDetailWrapper ruleAndRuleDetailWrapper : ruleAndRuleDetailWrapperToSave){
                List<EAW_Rule_Detail__c> tempRuleDetailList = ruleAndRuleDetailWrapper.ruleDetailList;
                for(EAW_Rule_Detail__c ruleDetail : tempRuleDetailList){
                    ruleDetail.Rule_No__c = ruleAndRuleDetailWrapper.rule.Id;
                    ruleDetail.RecordTypeId = ruleDetailRecordType;
                }
                ruleDetailList.addAll(tempRuleDetailList);
            }
            upsert ruleDetailList;
            EAW_Plan_Guideline__c planGuideline = [SELECT Id,Status__c FROM EAW_Plan_Guideline__c WHERE Id = :planGuidelineId];
            if(ruleJSON != null){
               planGuideline.All_Rules__c = ruleJSON;
               update planGuideline;
            }
            if(planGuideline.Status__c == 'Active'){
            
                //LRCC-1239
               // EAW_PlanReQualifierController.checkForPlanReQualifications(ruleDetailList,planGuidelineId); // Qualification for Titles
            }
            return ruleList;
        } catch (Exception e){
            throw new AuraHandledException(e.getMessage());
        }
    }
    
    
    
    @AuraEnabled
    public static dependentAndFieldWrapper getDependentFieldSet(String fieldName,String planGuidelineId){
        dependentAndFieldWrapper newdependentAndFieldWrapper = new dependentAndFieldWrapper();
        Id ruleDetailRecordTypeId = Schema.SObjectType.EAW_Rule_Detail__c.getRecordTypeInfosByName().get('Qualifier').getRecordTypeId();
        EAW_FieldDescriber.dependentPicklistValuesWrapper dependentPicklistValuesWrapper = EAW_FieldDescriber.getDependentPicklistValues(ruleDetailRecordTypeId,'EAW_Rule_Detail__c',fieldName);
        system.debug(':::: Final Dependent Picklist Values ::::'+dependentPicklistValuesWrapper);
        if(dependentPicklistValuesWrapper.dependentPicklistValues != null && dependentPicklistValuesWrapper.dependentPicklistValues.containsKey('Release Date Status') && dependentPicklistValuesWrapper.dependentPicklistValues.get('Release Date Status').size() > 0){
           List<EAW_FieldDescriber.picklistValuesWrapper> releaseDateStatusPicklist = getReleaseDateStaus();
            dependentPicklistValuesWrapper.dependentPicklistValues.put('Release Date Status',releaseDateStatusPicklist);
        }
        newdependentAndFieldWrapper.dependentWrapper = dependentPicklistValuesWrapper;
        newdependentAndFieldWrapper.fieldWrapper = getFieldSet();
        newdependentAndFieldWrapper.ruleAndRuleDetailList = getRuleAndRuleDetail(planGuidelineId); 
        EAW_Plan_Guideline__c planGuidline = [SELECT Id,Status__c FROM EAW_Plan_Guideline__c WHERE Id = :planGuidelineId];
        if(planGuidline.Status__c == 'Active'){
            newdependentAndFieldWrapper.isActivePlanGuideline = true;
        } else{
            newdependentAndFieldWrapper.isActivePlanGuideline = false;
        }
        return newdependentAndFieldWrapper;
    }
    public static List<ruleAndRuleDetailWrapper> getRuleAndRuleDetail(String planGuidelineId){
        List<ruleAndRuleDetailWrapper> newruleAndRuleDetailWrapperList = new List<ruleAndRuleDetailWrapper>();
        //try{
            Schema.SObjectType targetType = Schema.getGlobalDescribe().get('EAW_Rule_Detail__c');//From the Object Api name retrieving the SObject
            Sobject Object_name = targetType.newSObject();
            Schema.sObjectType sobject_type = Object_name.getSObjectType(); //grab the sobject that was passed
            Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
            Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap();
            
            String ruleDetailfieldSet = 'SELECT ';
            for(Schema.SObjectField fieldName : field_map.values()){
                String fieldAPI = String.valueOf(fieldName);
                ruleDetailfieldSet = ruleDetailfieldSet + ' ' + fieldAPI + ' ,';
            }
            ruleDetailfieldSet = ruleDetailfieldSet.removeEnd(',');
            ruleDetailfieldSet = ruleDetailfieldSet + 'FROM Rule_Orders__r';
            String ruleSet = 'SELECT Id,' + '(' + ruleDetailfieldSet + ')' + ' FROM EAW_Rule__c WHERE Plan_Guideline__c = :planGuidelineId';
            List<EAW_Rule__c> ruleList = Database.query(ruleSet);
        System.debug(planGuidelineId+':::'+ruleSet+'::::::::::::::::::::::::::::::::::::::'+ruleList);
            for(EAW_Rule__c rule : ruleList){
                ruleAndRuleDetailWrapper newruleAndRuleDetailWrapper = new ruleAndRuleDetailWrapper();
                newruleAndRuleDetailWrapper.rule = rule;
                newruleAndRuleDetailWrapper.ruleDetailList = rule.Rule_Orders__r;
                newruleAndRuleDetailWrapperList.add(newruleAndRuleDetailWrapper);
            }
            // fieldSet = fieldSet + 'FROM EAW_Rule_Detail__c WHERE Rule_No__c = :ruleId';
            // 'Select Id, Plan_Guideline__c, (Select Id from Rule_Orders__r) from EAW_Rule__c WHERE RecordTypeId=: recType.Id AND Plan_Guideline__c != NULL';
            //List<EAW_Rule_Detail__c> ruleDetailList = Database.query(fieldSet);
            //newruleAndRuleDetailWrapper.rule = rule;
           // newruleAndRuleDetailWrapper.ruleDetailList = ruleDetailList;
       // } catch(Exception e){
           //  newruleAndRuleDetailWrapperList = null;
        //}
        return newruleAndRuleDetailWrapperList;
        
    }
    public static List<EAW_FieldDescriber.picklistValuesWrapper> getReleaseDateStaus(){
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get('EAW_Release_Date__c');//From the Object Api name retrieving the SObject
        Sobject Object_name = targetType.newSObject();
        Schema.sObjectType sobject_type = Object_name.getSObjectType(); //grab the sobject that was passed
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap();
        List<Schema.PicklistEntry> ple = field_map.get('Status__c').getDescribe().getPicklistValues();
        List<EAW_FieldDescriber.picklistValuesWrapper> pickListValuesList = new List<EAW_FieldDescriber.picklistValuesWrapper>();
        for( Schema.PicklistEntry pickListVal : ple){
            EAW_FieldDescriber.picklistValuesWrapper newpickListWrapper = new EAW_FieldDescriber.picklistValuesWrapper(pickListVal.getLabel(),pickListVal.getValue());
            pickListValuesList.add(newpickListWrapper);
        }
        return pickListValuesList;
    }
    public static List<fieldSetWrapper> getFieldSet(){
        
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get('EAW_Rule_Detail__c');//From the Object Api name retrieving the SObject
        Sobject Object_name = targetType.newSObject();
        Schema.sObjectType sobject_type = Object_name.getSObjectType(); //grab the sobject that was passed
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap();
        
        List<fieldSetWrapper> newfieldSetWrapper = new List<fieldSetWrapper>();
        for(Schema.SObjectField fieldName : field_map.values()){
            String fieldAPI = String.valueOf(fieldName);
            
            String dataType = String.valueOf(field_map.get(fieldAPI).getDescribe().getType());
            String fieldLabel = field_map.get(fieldAPI).getDescribe().getLabel();
            fieldSetWrapper newWrapper = new fieldSetWrapper();
            
            if(dataType == 'PICKLIST' || dataType == 'MULTIPICKLIST'){
                newWrapper.dataType = dataType;
                newWrapper.fieldLabel = fieldLabel;
                newWrapper.fieldName = fieldAPI;
                newWrapper.isRequired = false;
                List<Schema.PicklistEntry> ple = field_map.get(fieldAPI).getDescribe().getPicklistValues();
                List<pickListWrapper> pickListValuesList = new List<pickListWrapper>();
                for( Schema.PicklistEntry pickListVal : ple){
                    pickListWrapper newpickListWrapper = new pickListWrapper();
                    newpickListWrapper.label = String.valueOf(pickListVal.getLabel());
                    newpickListWrapper.value = String.valueOf(pickListVal.getValue());
                    pickListValuesList.add(newpickListWrapper);
                }  
                newWrapper.pickListValues = pickListValuesList;
            } else{
                newWrapper.dataType = dataType;
                newWrapper.fieldLabel = fieldLabel;
                newWrapper.fieldName = fieldAPI;
                newWrapper.isRequired = false;
            }
            newfieldSetWrapper.add(newWrapper);
        }
        return newfieldSetWrapper;
    }
    public class dependentAndFieldWrapper {
        @AuraEnabled
        public EAW_FieldDescriber.dependentPicklistValuesWrapper dependentWrapper;
        
        @AuraEnabled
        public List<fieldSetWrapper> fieldWrapper;
        
        @AuraEnabled
        public List<ruleAndRuleDetailWrapper> ruleAndRuleDetailList;
        
        @AuraEnabled
        public Boolean isActivePlanGuideline;
    }
    public class fieldSetWrapper{
        @AuraEnabled
        public String fieldLabel;
        @AuraEnabled
        public String fieldName;
        @AuraEnabled
        public List<pickListWrapper> pickListValues;
        @AuraEnabled
        public Boolean isRequired;
        @AuraEnabled
        public String dataType;
    }
    public class pickListWrapper{
        @AuraEnabled
        public String label;
        @AuraEnabled
        public String value;
    }
    public class ruleAndRuleDetailWrapper {
        @AuraEnabled
        public EAW_Rule__c rule;
        
        @AuraEnabled
        public List<EAW_Rule_Detail__c> ruleDetailList;     
    }
}