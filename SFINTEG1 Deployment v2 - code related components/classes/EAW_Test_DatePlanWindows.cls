@IsTest
private class EAW_Test_DatePlanWindows {

    /******************************************************
     * TEST CASES FOR - Apply Latest Version Functionality
     ******************************************************
     *
     * 1. Invalid Ids (bogus planId, windowId)
     * 2. Null Inputs
     * 3. Null Plan, 2 Windows
     *      a. window 1 updates in this case
     *      b. no windows update
     * 4. 1 Plan, No windows
     *      a. Plan updates
     *      b. Plan does not update
     * 5. 1 Plan, 3 Windows
     *      a. none of them update
     *      b. 1 Plan, 1 Window updates
     *
     ******************************************/


    /**
     * Case 1: bogus inputs
     */
    @IsTest
    public static void testInvalidIds_ApplyLatestVersion() {
        try {

            List<String> bogusIds = new List<String>();
            bogusIds.add('asdasdasdsrewrwe');
            bogusIds.add('sdfiosdjhfiuwaehe');
            bogusIds.add('fsdfuwervwe2');

            // Run our controller code
            EAW_TPDController_v2.applyLatestVersion(bogusIds.get(0), bogusIds);

            // If the above line runs
            System.assert(true);

        } catch (Exception e) {
            System.assert(false, 'Code Errored out');
        }
    }

    /**
     * Case 2: Null inputs
     */
    @IsTest
    public static void testNullInputs_ApplyLatestVersion() {
        try {
            // Run our controller code
            EAW_TPDController_v2.applyLatestVersion(null, null);

            // If the above line runs
            System.assert(true);

        } catch (Exception e) {
            System.assert(false, 'Code Errored out');
        }
    }

    /**
     * CASE 3A: Test Apply Latest Version Functionality
     **
     *
     * User clicks 'Apply Latest Version' to a window
     *      - the associated Window Guideline is NOT the latest version,
     *        so we update the window with its latest window guideline
     *        (Note we only update the header not the strands, for now)
     *
     * Inputs -> 2 Window Guidelines (version 1, and version 2)
     *        -> 1 Window (Points to Version 1)
     *
     * Output -> Window w/ updated values
     */
    @IsTest
    public static void testWindowUpdate_ApplyLatestVersion() {

        // Create some test data

        List<EAW_Window__c> windows = new List<EAW_Window__c>();
        List<EAW_Window_Guideline__c> windowGuidelines = new List<EAW_Window_Guideline__c>();

        // Create some Window Guidelines
        List<EAW_Window_Guideline__c> wg = EAW_TestDataFactory.createWindowGuideLine(2, false);
  
      //  wg2.Name = 'WG_TEST_VERSION_2';
        wg[1].Media__c = 'Free TV';
        wg[1].Window_Type__c = 'First-Run';
        wg[1].Version__c = 2;

        insert wg[1];

    //    wg1.Name = 'WG_TEST_VERSION_1';
        wg[0].Media__c = 'Basic TV';
        wg[0].Window_Type__c = 'First-Run';
        wg[0].Version__c = 1;
        wg[0].Next_Version__c = wg[1].Id;

        insert wg[0];

        List<EAW_Window_Guideline_Strand__c> windowStrand = EAW_TestDataFactory.createWindowGuideLineStrand(2, false);
        windowStrand[0].EAW_Window_Guideline__c = wg[0].Id;
        windowStrand[0].License_Type__c = 'Exhibition License';
        windowStrand[0].Territory__c = 'Aruba';
        windowStrand[0].Media__c = 'Home Video';
        windowStrand[1].EAW_Window_Guideline__c = wg[1].Id;
        windowStrand[1].License_Type__c = 'Exhibition License';
        windowStrand[1].Territory__c = 'Aruba';
        windowStrand[1].Media__c = 'Home Video';
        insert windowStrand;
        
        windowGuidelines.addall(wg);
        windowGuidelines[0].Status__c = 'Active';   
        windowGuidelines[1].Status__c = 'Active';  
        update windowGuidelines;

        // Create some windows
        List<EAW_Window__c> w = EAW_TestDataFactory.createWindow(2, false);

        w[0].Name = 'WINDOW_TEST_VERSION_1';
        w[0].EAW_Window_Guideline__c = wg[0].Id; // Set it to Version #1 so we can test the updated version
        w[0].Media__c = 'Basic TV';
        w[0].Window_Type__c = 'First-Run';

        w[1].Name = 'WINDOW_TEST_VERSION_2';
        w[1].EAW_Window_Guideline__c = wg[1].Id;

        windows.addall(w);
        insert windows;

        List<String> windowIds = new List<String>();
        windowIds.add(w[0].Id);
        //windowIds.add(w2.Id);

        try {
            // Run our controller code
            EAW_TPDController_v2.applyLatestVersion(null, windowIds);

            // See if it works as expected;
            EAW_Window__c window1 = [SELECT Id, Media__c, Window_Type__c FROM EAW_Window__c WHERE Id = :w[0].Id LIMIT 1];
            System.assert(window1 != null);
            System.assert(window1.Media__c == 'Free TV');
            System.assert(window1.Window_Type__c == 'First-Run');

        } catch (Exception e) {
            System.assert(false, 'Code Errored out');
        } finally {
            // Delete our mock data from the system
            delete windows;
            windowGuidelines[0].Status__c = 'Draft';   
            windowGuidelines[1].Status__c = 'Draft';  
            update windowGuidelines;
            delete windowGuidelines;
        }
    }

    /**
     * CASE 4A: Test Apply Latest Version Functionality
     **
     *
     * User clicks 'Apply Latest Version' to a Plan
     *      - the Plan Window Guideline is NOT the latest version, update accordingly
     *
     * Inputs -> 2 Plan Guidelines (version 1, and version 2)
     *        -> 1 Plan (Points to Version 1)
     *
     * Output -> Plan w/ updated values
     */
    @IsTest
    public static void testPlanUpdate_ApplyLatestVersion() {

        // Create some test data

        // Create some Window Guidelines
        List<EAW_Plan_Guideline__c> pg = EAW_TestDataFactory.createPlanGuideline(2, false);

        pg[1].Name = 'PG_TEST_VERSION_2';
        pg[1].Territory__c = 'Australia';
        pg[1].Version__c = 2;
        insert pg[1];

        pg[0].Name = 'PG_TEST_VERSION_1';
        pg[0].Territory__c = 'France';
        pg[0].Version__c = 1;
        pg[0].Next_Version__c = pg[1].Id;
        insert pg[0];

        pg[0].Status__c = 'Active';
        update pg[0];
        
        // Create some windows
        List<EAW_Plan__c> plan = EAW_TestDataFactory.createPlan(1, false);
        plan[0].Name = 'AUTOMATION_TEST_PLAN';
        plan[0].Territory__c = 'China';
        plan[0].Plan_Guideline__c = pg[0].Id;
        insert plan;


        try {
            // Run our controller code
            EAW_TPDController_v2.applyLatestVersion(plan[0].Id, null);

            // See if it works as expected;
            EAW_Plan__c planResults = [SELECT Id, Name, Territory__c FROM EAW_Plan__c WHERE Id = :plan[0].Id LIMIT 1];

            System.assert(planResults != null);
            System.assert(planResults.Name == 'PG_TEST_VERSION_2');
            System.assert(planResults.Territory__c == 'Australia');

        } catch (Exception e) {
            System.assert(false, 'Code Errored out');
        } finally {
            // Delete our mock data from the system
            delete plan;
            delete pg[0];
            delete pg[1];
        }
    }

    /**
     * CASE 4B: Test Apply Latest Version Functionality
     **
     *
     * User clicks 'Apply Latest Version' to a Plan
     *      - the Plan Window Guideline is the latest version, nothing to update
     *
     * Inputs -> 2 Plan Guidelines (not linked)
     *        -> 1 Plan (Points to Version 1)
     *
     * Output -> Plan w/ updated values
     */
    @IsTest
    public static void testPlanNoUpdate_ApplyLatestVersion() {

        // Create some test data

        // Create some Window Guidelines
        List<EAW_Plan_Guideline__c> pg = EAW_TestDataFactory.createPlanGuideline(2, false);

        pg[1].Name = 'PG_TEST_VERSION_2';
        pg[1].Territory__c = 'Australia';
        pg[1].Version__c = 2;
        insert pg[1];

        pg[0].Name = 'PG_TEST_VERSION_1';
        pg[0].Territory__c = 'France';
        pg[0].Version__c = 1;
        insert pg[0];

        // Create some windows
        List<EAW_Plan__c> plan = EAW_TestDataFactory.createPlan(1, false);
        plan[0].Name = 'AUTOMATION_TEST_PLAN';
        plan[0].Territory__c = 'China';
        insert plan;


        try {
            // Run our controller code
            EAW_TPDController_v2.applyLatestVersion(plan[0].Id, null);

            // See if it works as expected;
            EAW_Plan__c planResults = [SELECT Id, Name, Territory__c FROM EAW_Plan__c WHERE Id = :plan[0].Id LIMIT 1];

            System.assert(planResults != null);
            System.assert(planResults.Name == 'AUTOMATION_TEST_PLAN');
            System.assert(planResults.Territory__c == 'China');

        } catch (Exception e) {
            System.assert(false, 'Code Errored out');
        } finally {
            // Delete our mock data from the system
            delete plan;
            delete pg[0];
            delete pg[1];
        }
    }


    /**
     * CASE 5A: Test Apply Latest Version Functionality
     **
     *
     * User clicks 'Apply Latest Version' to a Plan && Windows
     *      - the Plan Window Guideline is the latest version, nothing to update
     *
     * Inputs -> 2 Plan Guidelines (not linked)
     *        -> 1 Plan (Points to Version 1)
     *        -> 2 Window Guidelines (not linked)
     *        -> 1 Window (Points to Version 1)
     * Output -> Plan w/ updated values
     */
    @IsTest
    public static void testPlanAndWindowsNoUpdate_ApplyLatestVersion() {

        // Create some test data

        // Create some Window Guidelines
        List<EAW_Plan_Guideline__c> pg = EAW_TestDataFactory.createPlanGuideline(2, false);

        pg[1].Name = 'PG_TEST_VERSION_2';
        pg[1].Territory__c = 'Australia';
        pg[1].Version__c = 2;
        insert pg[1];

        pg[0].Name = 'PG_TEST_VERSION_1';
        pg[0].Territory__c = 'France';
        pg[0].Version__c = 1;
        insert pg[0];

        // Create some windows
        List<EAW_Plan__c> plan = EAW_TestDataFactory.createPlan(1, false);
        plan[0].Name = 'AUTOMATION_TEST_PLAN';
        plan[0].Territory__c = 'China';
        insert plan;

        // Create some test data

        List<EAW_Window__c> windows = new List<EAW_Window__c>();
        List<EAW_Window_Guideline__c> windowGuidelines = new List<EAW_Window_Guideline__c>();

        // Create some Window Guidelines
        List<EAW_Window_Guideline__c> wg = EAW_TestDataFactory.createWindowGuideLine(2, false);

       // wg2.Name = 'WG_TEST_VERSION_2';
        wg[1].Media__c = 'Free TV';
        wg[1].Window_Type__c = 'First-Run';
        wg[1].Version__c = 2;

        insert wg[1];

      //  wg1.Name = 'WG_TEST_VERSION_1';
        wg[0].Media__c = 'Basic TV';
        wg[0].Window_Type__c = 'First-Run';
        wg[0].Version__c = 1;

        insert wg[0];
        
        List<EAW_Window_Guideline_Strand__c> windowStrand = EAW_TestDataFactory.createWindowGuideLineStrand(2, false);
        windowStrand[0].EAW_Window_Guideline__c = wg[0].Id;
        windowStrand[0].License_Type__c = 'Exhibition License';
        windowStrand[0].Territory__c = 'Aruba';
        windowStrand[0].Media__c = 'Home Video';
        windowStrand[1].EAW_Window_Guideline__c = wg[1].Id;
        windowStrand[1].License_Type__c = 'Exhibition License';
        windowStrand[1].Territory__c = 'Aruba';
        windowStrand[1].Media__c = 'Home Video';
        insert windowStrand;
        
        windowGuidelines.addall(wg);
        windowGuidelines[0].Status__c = 'Active';   
        windowGuidelines[1].Status__c = 'Active';  
        update windowGuidelines;

        // Create some windows
        List<EAW_Window__c> w = EAW_TestDataFactory.createWindow(2, false);
        w[0].Name = 'WINDOW_TEST_VERSION_1';
        w[0].EAW_Window_Guideline__c = wg[0].Id; // Set it to Version #1 so we can test the updated version
        w[0].Media__c = 'Basic TV';
        w[0].Window_Type__c = 'First-Run';

        w[1].Name = 'WINDOW_TEST_VERSION_2';
        w[1].EAW_Window_Guideline__c = wg[1].Id;

        windows.addall(w);
        insert windows;

        List<String> windowIds = new List<String>();
        windowIds.add(w[0].Id);
        windowIds.add(w[1].Id);

        try {
            // Run our controller code
            EAW_TPDController_v2.applyLatestVersion(plan[0].Id, windowIds);

            // See if it works as expected;
            //check plan
            EAW_Plan__c planResults = [SELECT Id, Name, Territory__c FROM EAW_Plan__c WHERE Id = :plan[0].Id LIMIT 1];
            System.assert(planResults != null);
            System.assert(planResults.Name == 'AUTOMATION_TEST_PLAN', 'Plan Name Updated');
            System.assert(planResults.Territory__c == 'China', 'Plan Territory Updated');

            // check window
            EAW_Window__c window1 = [SELECT Id, Media__c, Window_Type__c FROM EAW_Window__c WHERE Id = :w[0].Id LIMIT 1];
            System.assert(window1 != null);
            System.assert(window1.Media__c == 'Basic TV', 'Window Media Updated');
            System.assert(window1.Window_Type__c == 'First-Run', 'Window Type Updated');

        } catch (Exception e) {
            System.assert(false, 'Code Errored out');
        } finally {
            // Delete our mock data from the system
            delete plan;
            delete pg[0];
            delete pg[1];
            delete windows;
            windowGuidelines[0].Status__c = 'Draft';   
            windowGuidelines[1].Status__c = 'Draft';  
            update windowGuidelines;
            delete windowGuidelines;
        }
    }

    /**
     * CASE 5B: Test Apply Latest Version Functionality
     **
     *
     * User clicks 'Apply Latest Version' to a Plan && Windows
     *      - the Plan/Window Guidelines are NOT the latest version, nothing to update
     *
     * Inputs -> 2 Plan Guidelines (linked v1 -> v2)
     *        -> 1 Plan (Points to Version 1)
     *        -> 2 Window Guidelines (linked v1->v2)
     *        -> 1 Window (Points to Version 1)
     * Output -> Plan w/ updated values
     */
    @IsTest
    public static void testPlanAndWindowsUpdate_ApplyLatestVersion() {

        // Create some test data

        // Create some Window Guidelines
        List<EAW_Plan_Guideline__c> pg = EAW_TestDataFactory.createPlanGuideline(2, false);

        pg[1].Name = 'PG_TEST_VERSION_2';
        pg[1].Territory__c = 'Australia';
        pg[1].Version__c = 2;
        insert pg[1];

        pg[0].Name = 'PG_TEST_VERSION_1';
        pg[0].Territory__c = 'France';
        pg[0].Version__c = 1;
        pg[0].Next_Version__c = pg[1].Id;
        insert pg[0];
        
        pg[0].Status__c = 'Active';
        update pg[0];

        // Create some windows
        List<EAW_Plan__c> plan = EAW_TestDataFactory.createPlan(1, false);
        plan[0].Name = 'AUTOMATION_TEST_PLAN';
        plan[0].Territory__c = 'China';
        plan[0].Plan_Guideline__c = pg[0].Id;
        insert plan;

        // Create some test data

        List<EAW_Window__c> windows = new List<EAW_Window__c>();
        List<EAW_Window_Guideline__c> windowGuidelines = new List<EAW_Window_Guideline__c>();

        // Create some Window Guidelines
        List<EAW_Window_Guideline__c> wg = EAW_TestDataFactory.createWindowGuideLine(2, false);

      //  wg2.Name = 'WG_TEST_VERSION_2';
        wg[1].Media__c = 'Free TV';
        wg[1].Window_Type__c = 'First-Run';
        wg[1].Version__c = 2;

        insert wg[1];

      //  wg1.Name = 'WG_TEST_VERSION_1';
        wg[0].Media__c = 'Basic TV';
        wg[0].Window_Type__c = 'First-Run';
        wg[0].Version__c = 1;
        wg[0].Next_Version__c = wg[1].Id;
        insert wg[0];

        windowGuidelines.addall(wg);
        update windowGuidelines;

        // Create some windows
        List<EAW_Window__c> w = EAW_TestDataFactory.createWindow(2, false);

        w[0].Name = 'WINDOW_TEST_VERSION_1';
        w[0].EAW_Window_Guideline__c = wg[0].Id; // Set it to Version #1 so we can test the updated version
        w[0].Media__c = 'Basic TV';
        w[0].Window_Type__c = 'First-Run';

        w[1].Name = 'WINDOW_TEST_VERSION_2';
        w[1].EAW_Window_Guideline__c = wg[1].Id;

        windows.addall(w);
        insert windows;

        List<String> windowIds = new List<String>();
        windowIds.add(w[0].Id);
        windowIds.add(w[1].Id);

        try {
            // Run our controller code
            EAW_TPDController_v2.applyLatestVersion(plan[0].Id, windowIds);

            // See if it works as expected;
            //check plan
            EAW_Plan__c planResults = [SELECT Id, Name, Territory__c FROM EAW_Plan__c WHERE Id = :plan[0].Id LIMIT 1];
            System.assert(planResults != null);
            System.assert(planResults.Name == 'PG_TEST_VERSION_2', 'Plan Name  NOT Updated');
            System.assert(planResults.Territory__c == 'Australia', 'Plan Territory  NOT Updated');

            // check window
            EAW_Window__c window1 = [SELECT Id, Name, Media__c, Window_Type__c FROM EAW_Window__c WHERE Id = :w[0].Id LIMIT 1];
            System.assert(window1 != null);
            System.assert(window1.Name == 'WG_TEST_VERSION_2', 'Window Name NOT update');
            System.assert(window1.Media__c == 'Free TV', 'Window Media  NOT Updated');
            System.assert(window1.Window_Type__c == 'First-Run', 'Window Type NOT Updated');

        } catch (Exception e) {
            System.assert(false, 'Code Errored out');
        } finally {
            // Delete our mock data from the system
            delete plan;
            delete pg[0];
            delete pg[1];
            delete windows;
            delete windowGuidelines;
        }
    }


    /******************************************************
     * TEST CASES FOR - Mass Update Windows
     ******************************************************
     *
     * 1. Invalid Ids
     * 2. Clear everything
     * 3. Mass Update w/ valid values
     *
     ******************************************/

    /**
     * Case 1: Invalid Inputs
     */
//    @IsTest
//    public static void testNullInputs_MassUpdateWindows() {
//
//        try {
//            List<String> windowIds = null;
//            List<String> tagIds = null;
//            String startDate = null;
//            String endDate = null;
//            String noteText = null;
//            String windowStatus = null;
//            List<EAW_Window__c> results = EAW_TPDController_v2.massUpdateWindows(windowIds, tagIds, startDate, endDate, noteText, windowStatus);
//
//            // if we hit this point nothing errors out we're good
//            System.assert(results == null, 'Did not return null for invalid inputs');
//
//        } catch(Exception e) {
//            System.debug(e);
//            System.assert(false, 'Code Errored out');
//        }
//
//    }

    /**
     * Case 2: Clear everything
     */
//    @IsTest
//    public static void testClearFields_MassUpdateWindows() {
//
//        // Create some windows
//        EAW_Window__c w1 = new EAW_Window__c();
//        EAW_Window__c w2 = new EAW_Window__c();
//
//        w1.Name = 'WINDOW_TEST_1';
//        w1.Start_Date__c = Date.newInstance(1960, 2, 17);
//        w1.End_Date__c = Date.newInstance(1960, 2, 17);
//
//        w2.Name = 'WINDOW_TEST_2';
//        w2.Start_Date__c = Date.newInstance(1960, 2, 17);
//        w2.End_Date__c = Date.newInstance(1960, 2, 17);
//
//        insert w1;
//        insert w2;
//
//        //Create some tags
//        EAW_Tag__c testTag = new EAW_Tag__c();
//        testTag.Name = 'Test Tag';
//        testTag.Tag_Type__c = 'Window Tag';
//        insert testTag;
//
//        EAW_Window_Tag_Junction__c tagJunc = new EAW_Window_Tag_Junction__c();
//        tagJunc.Name = 'test';
//        tagJunc.Tag__c = testTag.Id;
//        tagJunc.Window__c = w1.Id;
//        insert tagJunc;
//
//
//        try {
//
//            // Make sure Dates aren't null
//            System.assert(w1.Start_Date__c != null, 'Window1 Start Date is NULL');
//            System.assert(w2.Start_Date__c != null, 'Window2 Start Date is NULL');
//            System.assert(w1.End_Date__c != null, 'Window1 End Date is NULL');
//            System.assert(w2.End_Date__c != null, 'Window2 End Date is NULL');
//
//            List<String> windowIds = new List<String>();
//            windowIds.add(w1.Id);
//            windowIds.add(w2.Id);
//            List<String> tagIds = null;
//            String startDate = null;
//            String endDate = null;
//            String noteText = null;
//            String windowStatus = null;
//            List<EAW_Window__c> results = EAW_TPDController_v2.massUpdateWindows(windowIds, tagIds, startDate, endDate, noteText, windowStatus);
//
//            // if we hit this point nothing errors out we're good
//
//            w1 = [SELECT Id, Start_Date__c, End_Date__c FROM EAW_Window__c WHERE Id = :w1.Id];
//            w2 = [SELECT Id, Start_Date__c, End_Date__c FROM EAW_Window__c WHERE Id = :w2.Id];
//            List<EAW_Window_Tag_Junction__c> tagJuncList = [SELECT Id FROM EAW_Window_Tag_Junction__c WHERE Window__r.Id IN :windowIds];
//
//            System.assert(results != null, '');
//            System.assert(w1.Start_Date__c == null, 'Window1 Start Date not Cleared');
//            System.assert(w2.Start_Date__c == null, 'Window1 Start Date not Cleared');
//            System.assert(w1.End_Date__c == null, 'Window1 End Date not Cleared');
//            System.assert(w2.End_Date__c == null, 'Window2 End Date not Cleared');
//            System.assert(tagJuncList.size() == 0, 'Windows Have Associated Tags');
//
//        } catch(Exception e) {
//            System.debug(e);
//            System.assert(false, 'Code Errored out');
//        } finally {
//            // delete the test data
//            delete w1;
//            delete w2;
//            delete [SELECT Id FROM EAW_Window_Tag_Junction__c WHERE Id = :tagJunc.Id];
//            delete testTag;
//        }
//
//    }

    /**
     * Case 3: Mass update everything
     */
//    @IsTest
//    public static void testUpdateFields_MassUpdateWindows() {
//
//        // Create some windows
//        List<String> windowIds = new List<String>();
//        EAW_Window__c w1 = new EAW_Window__c();
//        EAW_Window__c w2 = new EAW_Window__c();
//
//        w1.Name = 'WINDOW_TEST_1';
//        w2.Name = 'WINDOW_TEST_2';
//
//        insert w1;
//        insert w2;
//        windowIds.add(w1.Id);
//        windowIds.add(w2.Id);
//
//        //Create some tags
//        EAW_Tag__c testTag = new EAW_Tag__c();
//        testTag.Name = 'Test Tag';
//        testTag.Tag_Type__c = 'Window Tag';
//        insert testTag;
//
//        try {
//
//            // Make sure Dates are null
//            System.assert(w1.Start_Date__c == null, 'Window1 Start Date is NOT NULL');
//            System.assert(w2.Start_Date__c == null, 'Window2 Start Date is NOT NULL');
//            System.assert(w1.End_Date__c == null, 'Window1 End Date is NOT NULL');
//            System.assert(w2.End_Date__c == null, 'Window2 End Date is NOT NULL');
//
//
//            List<String> tagIds = new List<String>();
//            tagIds.add(testTag.Id);
//            String startDate = '2020-12-12';
//            String endDate = '2020-12-12';
//            String noteText = 'this is a test';
//            String windowStatus = 'Firm';
//            // Run our controller
//            List<EAW_Window__c> results = EAW_TPDController_v2.massUpdateWindows(windowIds, tagIds, startDate, endDate, noteText, windowStatus);
//
//            // if we hit this point nothing errors out we're good
//
//            w1 = [SELECT Id, Start_Date__c, End_Date__c FROM EAW_Window__c WHERE Id = :w1.Id];
//            w2 = [SELECT Id, Start_Date__c, End_Date__c FROM EAW_Window__c WHERE Id = :w2.Id];
//            List<EAW_Window_Tag_Junction__c> tagJuncList = [SELECT Id FROM EAW_Window_Tag_Junction__c WHERE Window__r.Id IN :windowIds];
//            List<Note> noteList = [SELECT Id FROM Note WHERE parentId IN :windowIds];
//
//            System.assert(results != null, '');
//            System.assert(w1.Start_Date__c == Date.newInstance(2020, 12, 12), 'Window1 Start Date not Updated');
//            System.assert(w2.Start_Date__c == Date.newInstance(2020, 12, 12), 'Window1 Start Date not Updated');
//            System.assert(w1.End_Date__c == Date.newInstance(2020, 12, 12), 'Window1 End Date not Updated');
//            System.assert(w2.End_Date__c == Date.newInstance(2020, 12, 12), 'Window2 End Date not Updated');
//            System.assert(tagJuncList.size() == 2, 'Windows Do not have Associated Tags');
//            System.assert(noteList.size() == 2, 'Windows do have additional Notes');
//
//        } catch(Exception e) {
//            System.debug(e);
//            System.assert(false, 'Code Errored out');
//        } finally {
//            // delete the test data
//
//            // ORDER MATTERS HERE ELSE THE VALIDATION WILL TRIGGER AND THE TEST
//            // WILL FAIL
//            delete [SELECT Id FROM Note WHERE parentId IN :windowIds];
//            delete [SELECT Id FROM EAW_Window_Tag_Junction__c WHERE Window__r.Id = :windowIds];
//            delete testTag;
//            delete w1;
//            delete w2;
//        }
//
//    }


    /******************************************************
     * TEST CASES FOR - Title-Plan Date Search
     ******************************************************
     *
     * Returns a Map of Titles-Plans && Windows
     *
     ******************************************/

    /**
     *
     */
//    @IsTest
//    public static void testNullInputs_TitlePlanWindowSearch() {
//        List<String> medias = null;
//        List<String> territories = null;
//        List<String> languages = null;
//        List<String> titles = null;
//        List<String> plans = null;
//        List<String> windowTags = null;
//        List<String> windowNames = null;
//        String windowType = null;
//        String windowStatus = null;
//        String windowDateStart = null;
//        String windowDateEnd = null;
//        String windowOutsideDateStart = null;
//        String windowOutsideDateEnd = null;
//        String windowRightStatus = null;
//        String windowLicenseInfoCodes = null;
//
//        try {
//            Map<EDM_GLOBAL_TITLE__c, List<EAW_Window__c>> results = EAW_TPDMController_clone.searchTitlePlanWindows(medias,
//                    territories,
//                    languages,
//                    titles,
//                    plans,
//                    windowTags,
//                    windowNames,
//                    windowType,
//                    windowStatus,
//                    windowDateStart,
//                    windowDateEnd,
//                    windowOutsideDateStart,
//                    windowOutsideDateEnd,
//                    windowRightStatus,
//                    windowLicenseInfoCodes);
//
//            System.assert(results != null, 'Results is null, an error occurred in the search');
//        } catch(Exception e) {
//            System.debug(e);
//            System.assert(false, 'Null Inputs threw an error');
//        }
//
//    }

//    @IsTest
//    public static void testInvalidInputs_TitlePlanWindowSearch() {
//        List<String> medias = null;
//        List<String> territories = null;
//        List<String> languages = null;
//        List<String> titles = null;
//        List<String> plans = null;
//        List<String> windowTags = null;
//        List<String> windowNames = null;
//        String windowType = null;
//        String windowStatus = null;
//        String windowDateStart = null;
//        String windowDateEnd = null;
//        String windowOutsideDateStart = null;
//        String windowOutsideDateEnd = null;
//        String windowRightStatus = null;
//        String windowLicenseInfoCodes = null;
//
//        try {
//            Map<EDM_GLOBAL_TITLE__c, List<EAW_Window__c>> results = EAW_TPDMController_clone.searchTitlePlanWindows(medias,
//                    territories,
//                    languages,
//                    titles,
//                    plans,
//                    windowTags,
//                    windowNames,
//                    windowType,
//                    windowStatus,
//                    windowDateStart,
//                    windowDateEnd,
//                    windowOutsideDateStart,
//                    windowOutsideDateEnd,
//                    windowRightStatus,
//                    windowLicenseInfoCodes);
//
//            System.assert(results != null, 'Results is null, an error occured in the search');
//        } catch(Exception e) {
//            System.debug(e);
//            System.assert(false, 'Null Inputs threw an error');
//        }
//    }

}