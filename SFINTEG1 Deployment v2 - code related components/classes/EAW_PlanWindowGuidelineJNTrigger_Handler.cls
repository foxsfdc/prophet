public with sharing class EAW_PlanWindowGuidelineJNTrigger_Handler {
    
    //LRCC-510, LRCC-1565.
    public void checkToRemoveJunction(List<EAW_Plan_Window_Guideline_Junction__c> triggerOld) {
        
        system.debug('triggerOld:::'+triggerOld);
        Set<Id> planIds = new Set<Id> ();
        Set<Id> activeWGIds = new Set<Id> ();
        
        List<EAW_Plan_Guideline__c> planGuidelines = new List<EAW_Plan_Guideline__c>();
        
        Map<Id, List<EAW_Plan_Window_Guideline_Junction__c>> junctionMap = new Map<Id,  List<EAW_Plan_Window_Guideline_Junction__c>> ();
        
        for(EAW_Plan_Window_Guideline_Junction__c pwgj: triggerOld) {
            planIds.add(pwgj.Plan__c);
        }
        
        if(planIds != null && planIds.size() > 0) {
        
            planGuidelines = [SELECT Id, Next_Version__c FROM EAW_Plan_Guideline__c WHERE Id IN :planIds /*AND Next_Version__c = null*/ AND Status__c != 'Inactive']; // LRCC - 1848
        }
        
        if(planGuidelines != null && planGuidelines.size() > 0) {
        
            planIds = new Set<Id> ();
                
            for(EAW_Plan_Window_Guideline_Junction__c junctionRec : [SELECT Id, Plan__c, Plan__r.Status__c, Window_Guideline__c, Window_Guideline__r.Status__c FROM EAW_Plan_Window_Guideline_Junction__c WHERE Plan__c IN: planGuidelines /*AND Plan__r.Status__c = 'Draft'*/ AND Window_Guideline__r.Status__c = 'Active']) {
                system.debug(':junctionRec::'+junctionRec);
                planIds.add(junctionRec.Plan__c);                
            }
            
            for(EAW_Plan_Window_Guideline_Junction__c pwgj: triggerOld) {
                 system.debug(':Plan__c::'+pwgj.Plan__c);
                if(! planIds.contains(pwgj.Plan__c)) {
                    pwgj.addError('Atleast one active window guideline is mandatory.');
                } 
            }
            
        }
        
        /*
        
        List<EAW_Plan_Guideline__c> planGuidelines = [SELECT Next_Version__c FROM EAW_Plan_Guideline__c WHERE Id IN :planIds AND Next_Version__c = null];
        
        if(planIds != null && planIds.size() > 0 && planGuidelines != null && planGuidelines.size() > 0) {
            
            if([SELECT Id, Plan__c, Plan__r.Status__c, Window_Guideline__c, Window_Guideline__r.Status__c FROM EAW_Plan_Window_Guideline_Junction__c WHERE Plan__c IN: planIds AND Plan__r.Status__c = 'Draft' AND Window_Guideline__r.Status__c = 'Active'].size() == 0) {
                triggerOld[0].addError('Atleast one active window guideline is mandatory.');
            }   
            */
            /*
            //LRCC-1655
            Set<Id> windowGuidelineIds = new Set<Id>();
            
            for(EAW_Plan_Window_Guideline_Junction__c pwgj: triggerOld) {
                windowGuidelineIds.add(pwgj.Window_Guideline__c);
            }
            
            List<EAW_Window__c> deleteWindows = [SELECT Id FROM EAW_Window__c WHERE EAW_Window_Guideline__c IN :windowGuidelineIds AND Status__c != 'Firm'];
            
            if(deleteWindows != null && deleteWindows.size() > 0) {
                delete deleteWindows;
            }
            */
        
        
    }
    
    //This method was not called from Trigger and it was commented out
    /*public void createEAWWindows(List<EAW_Plan_Window_Guideline_Junction__c> triggerNew) {
        
        Set<Id> windowGuidelineIdSet = new Set<Id>();
        Map<Id, Set<Id>> planWindowGuidelineIdMap = new Map<Id, Set<Id>>();
        
        for( EAW_Plan_Window_Guideline_Junction__c planWindow : triggerNew ){
            
            if( planWindow.Plan_Guideline_Status__c == 'Active' ){
                
                windowGuidelineIdSet.add(planWindow.Window_Guideline__c);
                
                if( planWindowGuidelineIdMap.containsKey(planWindow.Plan__c)) {
                    Set<Id> tempSet = planWindowGuidelineIdMap.get(planWindow.Plan__c);
                    tempSet.add(planWindow.Window_Guideline__c);
                    planWindowGuidelineIdMap.put(planWindow.Plan__c, tempSet);
                } else {
                    planWindowGuidelineIdMap.put(planWindow.Plan__c, new Set<Id>{planWindow.Window_Guideline__c});
                }
                
            }
        }
        
        system.debug('::: windowGuidelineIdSet :::'+windowGuidelineIdSet);
        system.debug('::: planWindowGuidelineIdMap :::'+planWindowGuidelineIdMap);
        
        if( planWindowGuidelineIdMap.size() > 0 ){
            
            List<EAW_Plan__c> plansList = [ Select Id, Name, Plan_Guideline__c From EAW_Plan__c Where Plan_Guideline__c IN : planWindowGuidelineIdMap.keySet() ];
            
            system.debug('::: plansList :::'+plansList);
            
            if( plansList != NULL && plansList.size() > 0 ) {
                
                Map<Id, List<EAW_Plan__c>> planMap = new  Map<Id, List<EAW_Plan__c>>();
                
                for( EAW_Plan__c plan : plansList ){
                    
                    if( planMap.containsKey(plan.Plan_Guideline__c) ){
                        List<EAW_Plan__c> tempPlanList = planMap.get(plan.Plan_Guideline__c);
                        tempPlanList.add(plan);
                        planMap.put( plan.Plan_Guideline__c,  tempPlanList); 
                    } else {
                        planMap.put( plan.Plan_Guideline__c,  new List<EAW_Plan__c>{ plan } );
                    }
                }
            
                System.debug(':::::: planMap :::::::'+planMap);
                
                //LRCC-1560 - Replace Customer lookup field with Customer text field
                //Map<Id, EAW_Window_Guideline__c> windowGuidelineMap = new Map<Id, EAW_Window_Guideline__c>([Select Id, Name, CurrencyIsoCode, Customer__c, End_Date__c, Media__c, Outside_Date__c, Start_Date__c, Tracking_Date__c, Window_Type__c From EAW_Window_Guideline__c Where Id IN : windowGuidelineIdSet ]);
                Map<Id, EAW_Window_Guideline__c> windowGuidelineMap = new Map<Id, EAW_Window_Guideline__c>([Select Id, Name, CurrencyIsoCode, Customers__c, End_Date__c, Media__c, Outside_Date__c, Start_Date__c, Tracking_Date__c, Window_Type__c From EAW_Window_Guideline__c Where Id IN : windowGuidelineIdSet ]);
                
                system.debug('::: windowGuidelineMap :::'+windowGuidelineMap);
                
                List<EAW_Window__c> eawWindowListToInsert = new List<EAW_Window__c>();
                
                for( Id planGuidelineId : planWindowGuidelineIdMap.keySet() ){
                    
                    for( Id windowGuidelineId : planWindowGuidelineIdMap.get(planGuidelineId) ){
                        
                        EAW_Window_Guideline__c windowGuidelineRecord = windowGuidelineMap.get(windowGuidelineId);
                        
                        for( EAW_Plan__c planRecord : planMap.get(planGuidelineId) ){
                            
                            EAW_Window__c newWindow = new EAW_Window__c();
                            
                            newWindow.EAW_Plan__c = planRecord.Id;
                            newWindow.EAW_Window_Guideline__c = windowGuidelineRecord.Id;
                            
                            newWindow.Name = planRecord.Name + '-' + windowGuidelineRecord.Name;
                            newWindow.CurrencyIsoCode = windowGuidelineRecord.CurrencyIsoCode;
                            //LRCC-1560 - Replace Customer lookup field with Customer text field
                            //newWindow.Customer__c = windowGuidelineRecord.Customer__c;
                            newWindow.Customers__c = windowGuidelineRecord.Customers__c;
                            newWindow.End_Date__c = windowGuidelineRecord.End_Date__c;
                            newWindow.Media__c = windowGuidelineRecord.Media__c;
                            newWindow.Outside_Date__c = windowGuidelineRecord.Outside_Date__c;
                            newWindow.Start_Date__c = windowGuidelineRecord.Start_Date__c;
                            newWindow.Tracking_Date__c = windowGuidelineRecord.Tracking_Date__c;
                            newWindow.Window_Type__c = windowGuidelineRecord.Window_Type__c;
                            
                            eawWindowListToInsert.add(newWindow);
                        }
                        
                    }
                    
                }
                
                system.debug('::: eawWindowListToInsert :::'+eawWindowListToInsert);
                
                if( eawWindowListToInsert != NULL && eawWindowListToInsert.size() > 0 ){
                    insert eawWindowListToInsert;
                }
            }
        }
        
    }*/
    
}