public class EAW_GenerateAlertforLocalTheatrical {
    
    //LRCC-1248
    public static Map<String, String> regionBasedTerritories = new Map<String, String> ();
    public static Map<String, String> chatterGroup = new Map<String, String> ();
    
    @InvocableMethod(label='localTheatricalDate')
    public static List<FeedItem> sentAlertToLocalTheatrical(List<EAW_Release_Date__c> releaseDates) {
            
        Date releaseDate;
        String territory;
        
        List<FeedItem> feedItems = new List<FeedItem>();
        Set<Id> titleIds = new Set<Id> ();
        
        if(chatterGroup.size() == 0) {
            for(CollaborationGroup cg: [SELECT Id, Name FROM CollaborationGroup WHERE Name like '%Release Date Review']) {
                chatterGroup.put(cg.Name, cg.Id);
            }
        }
        
        if(chatterGroup.size() > 0) {
        
            if(releaseDates != null && releaseDates.size() > 0) {
                titleIds.add(releaseDates[0].Title__c);
                releaseDate = releaseDates[0].Release_Date__c;
                territory = releaseDates[0].Territory__c;
            }
            
            if(titleIds != null && titleIds.size() > 0) {
                
                List<EAW_Release_Date__c> rds;
                Boolean filterFlag = false;
                if(String.isNotBlank(releaseDates[0].EAW_Release_Date_Type__c) && releaseDates[0].EAW_Release_Date_Type__c != 'Theatrical') {
                
                    filterFlag = true;
                    rds = new List<EAW_Release_Date__c> (
                        [
                            SELECT Id, Release_Date__c, Title__c, Release_Date_Guideline__c, Release_Date_Guideline__r.EAW_Release_Date_Type__c, 
                                Title__r.Name, Release_Date_Guideline__r.Territory__c, SFDC_Base_Instance__c 
                            FROM EAW_Release_Date__c    
                            WHERE Title__c IN: titleIds AND
                            Release_Date_Guideline__r.Territory__c =: territory AND 
                            Release_Date_Guideline__r.EAW_Release_Date_Type__c = 'Theatrical'
                        ]
                    );
                }
                else if(String.isNotBlank(releaseDates[0].EAW_Release_Date_Type__c) && releaseDates[0].EAW_Release_Date_Type__c == 'Theatrical') {
                    
                    rds = new List<EAW_Release_Date__c> (
                        [
                            SELECT Id, Release_Date__c, Title__c, Release_Date_Guideline__c, Release_Date_Guideline__r.EAW_Release_Date_Type__c, 
                                Title__r.Name, Release_Date_Guideline__r.Territory__c, SFDC_Base_Instance__c 
                            FROM EAW_Release_Date__c    
                            WHERE Title__c IN: titleIds AND 
                            Release_Date_Guideline__r.Territory__c =: territory AND 
                            Release_Date_Guideline__r.EAW_Release_Date_Type__c != 'Theatrical'
                        ]
                    );    
                } 
                
                if(rds != null && rds.size() > 0) {
                    
                    Boolean flag;
                    String bodyContent;
                    
                    if(regionBasedTerritories.size() == 0) {
                    
                        for(Sales_Regions__c sg: [SELECT Id,Name,Territories__c FROM Sales_Regions__c WHERE Territories__c != null]) {
                                
                            List<String> territories = sg.Territories__c.split(';');
                            
                            if(territories != null && territories.size() > 0) {
                                
                                if(sg.Name == 'Australia / New Zealand') {
                                    sg.Name = sg.Name.replaceAll('\\s+', '');    
                                }
                                String tempName = sg.Name+'-Release Date Review';
                                
                                for(String terr: territories) {
                                    regionBasedTerritories.put(terr, tempName);
                                }
                            }
                        } 
                    }
                        
                    bodyContent = rds[0].Title__r.Name +' - Release Date is Prior to the Local Theatrical date.'+'\n\n';
                    
                    for(EAW_Release_Date__c rd: rds) {
                        
                        if(checkRecursiveDataforNotification.isRecursiveData(rd.Id)) {
                        
                            if(rd.Release_Date__c != null && releaseDate != null && releaseDate < rd.Release_Date__c && filterFlag == true) {
                                flag = true;
                                bodyContent += rd.SFDC_Base_Instance__c+'/'+rd.Id+'\n';
                            } 
                            else if(rd.Release_Date__c != null && releaseDate != null && rd.Release_Date__c < releaseDate && filterFlag == false) {
                                flag = true;
                                bodyContent += rd.SFDC_Base_Instance__c+'/'+rd.Id+'\n';
                            }
                        }     
                    }
                    
                    if(String.isNotBlank(bodyContent)) {
                                    
                        String chatterId;
                        String temp = bodyContent;
                    
                        if(bodyContent.length() < 10000) {
                            bodyContent = temp.substring(0, temp.length());
                        } 
                        else {
                            bodyContent = temp.substring(0,9988);    
                        }
                        
                        if(regionBasedTerritories.containsKey(territory) 
                            && chatterGroup.get(regionBasedTerritories.get(territory)) != null) {
                            chatterId = chatterGroup.get(regionBasedTerritories.get(territory));
                        }
                        else {
                            chatterId = chatterGroup.get('General-Release Date Review');
                        }
                        
                        if(String.isNotBlank(chatterId) && flag == true) {
                            feedItems.add(
                                new FeedItem(
                                    body = bodyContent,
                                    ParentId = chatterId
                                )
                            );
                        }
                    }
                }
            }
                
            if(feedItems.size() > 0) {
                insert feedItems;
            }
        }
        return feedItems;
    }
}