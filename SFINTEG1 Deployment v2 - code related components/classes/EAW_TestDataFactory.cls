/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 **/

@isTest
public class EAW_TestDataFactory {

    public static List<EAW_Plan_Guideline__c> createPlanGuideline( Integer numOffRecords, Boolean IsCreate ) {
        
        List<EAW_Plan_Guideline__c> planGuideList = new List<EAW_Plan_Guideline__c>();
        
        if( numOffRecords != NULL && numOffRecords > 0 ) {
            for(Integer i = 0; i < numOffRecords; i++ ) {                
                EAW_Plan_Guideline__c planGuideline = new EAW_Plan_Guideline__c(Name='Test Plan Guideline '+i, Product_Type__c = 'Feature');
                planGuideList.add(planGuideline);                
            }
            if( planGuideList != NULL && planGuideList.size() > 0 && IsCreate ) {
                insert planGuideList;
            } 
        }
        return planGuideList;
    }
    
    public static List<EAW_Release_Date_Type__c>  createReleaseDateType ( Integer numOffRecords, Boolean IsCreate ) {
        // Refactored on 24/10
        List<EAW_Release_Date_Type__c> releaseDateList = new List<EAW_Release_Date_Type__c>();
        
        if( numOffRecords != NULL && numOffRecords > 0 ){
            
            for( Integer i = 0; i < numOffRecords; i++ ){
                EAW_Release_Date_Type__c releaseDate = new EAW_Release_Date_Type__c(Name='Test Release Date Type '+i, Media__c = 'Free TV');
                releaseDateList.add(releaseDate);
            } 
            if( releaseDateList != NULL && releaseDateList.size() > 0 && IsCreate ) {
                insert releaseDateList;
            }            
        }
        return releaseDateList; 
    }
    
    
    public static List<EAW_Customer__c>  createEAWCustomer ( Integer numOffRecords, Boolean isCreated ) {
        
        List<EAW_Customer__c> customerList = new List<EAW_Customer__c>();
        
        if( numOffRecords != NULL && numOffRecords > 0 ){
            
            for( Integer i = 0; i < numOffRecords; i++ ){
                EAW_Customer__c customer = new EAW_Customer__c(Name='Test customer '+i, Media__c = 'Free TV', Customer_Id__c = '123456789');
                customerList.add(customer);
            }
            if( customerList != NULL && customerList.size() > 0 && isCreated ) {
                insert customerList;          
           }
        }
        return customerList;    
    }
    
    public static List<EAW_Title__c>  createEAWTitle ( Integer numOffRecords, Boolean isCreated ) {
        
        EDM_REF_DIVISION__c refDivision = new EDM_REF_DIVISION__c();
        insert refDivision;
        
        EDM_REF_PRODUCT_TYPE__c refProdType = new EDM_REF_PRODUCT_TYPE__c( Name = 'Feature', PROD_TYP_CD__c = 'Feature', PROD_TYP_DESC__c = 'Feature' );
        insert refProdType;
        
        List<EAW_Title__c> titleList = new List<EAW_Title__c>();
        
        if( numOffRecords != NULL && numOffRecords > 0 ){
            
            for( Integer i = 0; i < numOffRecords; i++ ){
               // EAW_Title__c title = new EAW_Title__c(Name='Test Title '+i, Title_EDM__c = globalTitle[i].Id );
               EAW_Title__c title = new EAW_Title__c(Name='Test Title '+i, FIN_DIV_CD__c = refDivision.Id, LIFE_CYCL_STAT_GRP_CD__c = 'PUBLC', PROD_TYP_CD_INTL__c = refProdType.Id );
               
                titleList.add(title);
            }
            if( titleList != NULL && titleList.size() > 0 && isCreated ) insert titleList;           
        }
        return titleList;   
    }
    
    public static List<EAW_Release_Date_Guideline__c>  createEAWReleaseDateGuideline ( ID releaseDateTypeId, ID customerId, Integer numOffRecords, Boolean isCreated,String customerName ) {
        
        List<EAW_Release_Date_Guideline__c> releaseDateGuideLineList = new List<EAW_Release_Date_Guideline__c>();
        
        
        if( numOffRecords != NULL && numOffRecords > 0 ){
            
            for( Integer i = 0; i < numOffRecords; i++ ){
                
                EAW_Release_Date_Guideline__c releaseDateGuideLine = new EAW_Release_Date_Guideline__c( Product_Type__c = 'Episode', Territory__c = 'United States +', Language__c = 'English', Customers__c = customerName);
                releaseDateGuideLineList.add(releaseDateGuideLine);
            }
            if( releaseDateGuideLineList != NULL && releaseDateGuideLineList.size() > 0 && isCreated ) {
                insert releaseDateGuideLineList;
            }
        }
        return releaseDateGuideLineList;     
    }
    
    public static List<sObject> createGenericSameTestRecords( Map<String, Object> dynamicFieldsAndValuesMap, String sObjectString, Integer numOfRecords, Boolean IsCreated ){
        // Refactored on 24/10
        List<sObject> sObjList;
        if( numOfRecords != NULL && numOfRecords > 0 ){
            sObjList = new  List<sObject>();
            for( Integer i = 1; i <= numOfRecords; i++ ){
                sObject sObj = Schema.getGlobalDescribe().get(sObjectString).newSObject() ;
                for( String keyVal : dynamicFieldsAndValuesMap.keySet() ){
                    if( keyVal.equalsIgnoreCase('Name') ) dynamicFieldsAndValuesMap.put( keyVal,  dynamicFieldsAndValuesMap.get(keyVal)+' '+i);
                    sObj.put(keyVal, dynamicFieldsAndValuesMap.get(keyVal) );
                }
                sObjList.add(sObj);
            } if(IsCreated ) {            
                insert sObjList;
            }
        }
        return sObjList;
    }
    
    public static List<sObject> createGenericDifferentTestRecords( List<Map<String, Object>> dynamicFieldsAndValuesMapList, String sObjectString ){
        
        List<sObject> sObjList;
        if( dynamicFieldsAndValuesMapList != NULL && dynamicFieldsAndValuesMapList.size() > 0 ){
            sObjList = new  List<sObject>();
            for( Integer i = 0; i < dynamicFieldsAndValuesMapList.size(); i++ ){
                sObject sObj = Schema.getGlobalDescribe().get(sObjectString).newSObject() ;
                for( String keyVal : dynamicFieldsAndValuesMapList[i].keySet() ){
                    if( keyVal.equalsIgnoreCase('Name') ) dynamicFieldsAndValuesMapList[i].put( keyVal,  dynamicFieldsAndValuesMapList[i].get(keyVal)+' '+i);
                    sObj.put(keyVal, dynamicFieldsAndValuesMapList[i].get(keyVal) );
                }
                sObjList.add(sObj);
            }
            insert sObjList;
        }
        return sObjList;
    }
    
    public static List<EAW_Rule__c>  createEAWRule ( Integer numOffRecords, Boolean isCreated ) {
        
        List<EAW_Rule__c> ruleList = new List<EAW_Rule__c>();
        
        if( numOffRecords != NULL && numOffRecords > 0 ){
            
            for( Integer i = 0; i < numOffRecords; i++ ){
                EAW_Rule__c rule= new EAW_Rule__c();
                ruleList.add(rule);
            }
            if( ruleList.isEmpty() == False && isCreated ) {
                insert ruleList ;
            }           
        }
        return ruleList ;   
    }
    public static List<EAW_Rule_Detail__c> createRuleDetail ( Integer numoffRecords, Boolean isCreated){
      
        List<EAW_Rule_Detail__c> edmRuleDetailList = new List<EAW_Rule_Detail__c>();
        List<EAW_Rule__c> ruleList = createEAWRule (1, True);
      
        if( numOffRecords != NULL && numOffRecords > 0 ){
        
            for( Integer i = 0; i < numOffRecords; i++){            
                EAW_Rule_Detail__c ruleDetail = new EAW_Rule_Detail__c (Rule_No__c = ruleList[0].Id);
                edmRuleDetailList.add(ruleDetail);
            }  
            if( edmRuleDetailList.isEmpty() == False && isCreated) {       
        
                insert edmRuleDetailList;        
            }                 
        }
        return edmRuleDetailList;
    }
    public static List<EAW_Window__c> createWindow ( Integer numoffRecords, boolean isCreated){
          
        List<EAW_Window__c> edmWindowList = new List<EAW_Window__c>();
        List<EAW_Title__c> title = createEAWTitle(1, true);
        List<EAW_Window_Guideline__c> WindowGuideLine = createWindowGuideLine( 1, true );
        
        if( numOffRecords != NULL && numOffRecords > 0 ) {
            
            for( Integer i = 0; i < numOffRecords; i++){
                    
                EAW_Window__c ruleDetail = new EAW_Window__c (Name='Test E'+i, EAW_Title_Attribute__c = title[0].Id, Window_Type__c = 'Library', EAW_Window_Guideline__c = windowGuideline[0].Id);
                edmWindowList.add(ruleDetail);
                    
            }  
            if( edmWindowList.isEmpty() == False && isCreated) {     
                
                    insert edmWindowList;        
            }       
        }
        return edmWindowList;
    }
    
    public static List<EAW_Window_Guideline__c> createWindowGuideLine ( Integer numoffRecords, boolean isCreated){
          
        List<EAW_Window_Guideline__c> edmWindowGuidelineList = new List<EAW_Window_Guideline__c>();
          
        if( numOffRecords != NULL && numOffRecords > 0 ){
            
            for( Integer i = 0; i < numOffRecords; i++){
                               
                EAW_Window_Guideline__c ruleDetail = new EAW_Window_Guideline__c ( Status__c = 'Draft', Product_Type__c = 'Feature', Window_Type__c = 'First-Run', Window_Guideline_Alias__c = 'Test'+i);
                edmWindowGuidelineList.add(ruleDetail);                   
                         
            }              
            if( edmWindowGuidelineList.isEmpty() == False && isCreated) {       
                
                    insert edmWindowGuidelineList;        
            }       
        }
        return edmWindowGuidelineList;
    }
    
    public static List<EAW_Plan_Window_Guideline_Junction__c> createPlanWindowGuideLineJunction ( Integer numoffRecords, boolean isCreated){
          
        List<EAW_Plan_Window_Guideline_Junction__c> edmPlanWindowGuideLineJunctionList = new List<EAW_Plan_Window_Guideline_Junction__c>();
          
        if( numOffRecords != NULL && numOffRecords > 0 ){
            
            for( Integer i = 0; i < numOffRecords; i++){
                
                
                EAW_Plan_Window_Guideline_Junction__c ruleDetail = new EAW_Plan_Window_Guideline_Junction__c ();
                edmPlanWindowGuideLineJunctionList.add(ruleDetail);
                
            } 
            
            if( edmPlanWindowGuideLineJunctionList.isEmpty() == False && isCreated ) {       
            
                    insert edmPlanWindowGuideLineJunctionList;        
            }        
        }
        return edmPlanWindowGuideLineJunctionList;
    }
    
    public static List<EAW_Plan__c> createPlan( Integer numoffRecords, boolean isCreated){
          
        List<EAW_Plan__c> edmPlanList = new List<EAW_Plan__c>();
        List<EAW_Title__c> title = createEAWTitle(1, true);
        List<EAW_Plan_Guideline__c> planGuidelineList = createPlanGuideline(1, true);
          
        if( numOffRecords != NULL && numOffRecords > 0 ){

            for( Integer i = 0; i < numOffRecords; i++){            
               
                EAW_Plan__c plan = new EAW_Plan__c ();
                plan.EAW_Title__c = title[0].Id;
                plan.Plan_Guideline__c = planGuidelineList[0].Id;
                edmPlanList.add(plan);                    
            }  
            if( edmPlanList.isEmpty() == False && isCreated ) {      
                insert edmPlanList;        
            }       
        }
        return edmPlanList ;
    }
    public static List<EAW_Tag__c > createTag( Integer numoffRecords, boolean isCreated, String tagType){
          
        List<EAW_Tag__c> tagList = new List<EAW_Tag__c>();
          
        if( numOffRecords != NULL && numOffRecords > 0 ){
            
            for( Integer i = 0; i < numOffRecords; i++){            
               
                EAW_Tag__c tag = new EAW_Tag__c ( Name = tagType+'-'+i, Tag_Type__c = tagType, Active__c = TRUE);
                tagList.add(tag);             
                          
            }            
            if( tagList.isEmpty() == False && isCreated ) {      
            
                    insert tagList;        
            }        
        }
        return tagList ;
    }
    public static List<EAW_Release_Date__c> createReleaseDate(Integer numoffRecords, Boolean isCreated) {
        
        List<EAW_Release_Date__c> releaseDateList = new List<EAW_Release_Date__c>();
        
        if(numOffRecords != NULL && numOffRecords > 0) {
            
            for(Integer i = 0; i < numOffRecords; i++) {
                
                EAW_Release_Date__c releaseDate = new EAW_Release_Date__c();
                releaseDateList.add(releaseDate);
            }            
            if( releaseDateList.isEmpty() == False && isCreated ) {      
            
                    insert releaseDateList;        
            }
        }
        
        return releaseDateList;
    }
    
    public static List<EAW_Release_Date_Tag_Junction__c> createReleaseDateTagJunction(Integer numoffRecords, Boolean isCreated) {
        
        List<EAW_Release_Date_Tag_Junction__c> releaseDateTagJunctionList = new List<EAW_Release_Date_Tag_Junction__c>();
        
        if(numOffRecords != NULL && numOffRecords > 0) {
            
            for(Integer i = 0; i < numOffRecords; i++) {
                
                EAW_Release_Date_Tag_Junction__c releaseDate = new EAW_Release_Date_Tag_Junction__c();
                releaseDateTagJunctionList.add(releaseDate);
            }            
            if( releaseDateTagJunctionList.isEmpty() == False && isCreated ) {      
            
                    insert releaseDateTagJunctionList;        
            }
        }
        
        return releaseDateTagJunctionList;
    }
    
    public static List<EAW_Title_Tag_Junction__c> createTitleTagJunction(Integer numoffRecords, Boolean isCreated) {
        
        List<EAW_Title_Tag_Junction__c> titleTagJunctionList = new List<EAW_Title_Tag_Junction__c>();
        
        if(numOffRecords != NULL && numOffRecords > 0) {
            
            for(Integer i = 0; i < numOffRecords; i++) {
                
                EAW_Title_Tag_Junction__c titleTagJunc = new EAW_Title_Tag_Junction__c();
                titleTagJunctionList.add(titleTagJunc);
            }            
            if( titleTagJunctionList.isEmpty() == False && isCreated ) {      
            
                    insert titleTagJunctionList;        
            }
        }
        
        return titleTagJunctionList;
    }
    
    public static List<EAW_Window_Tag_Junction__c> createWindowTagJunction(Integer numoffRecords, Boolean isCreated) {
        
        List<EAW_Window_Tag_Junction__c> windowTagJunctionList = new List<EAW_Window_Tag_Junction__c>();
        
        if(numOffRecords != NULL && numOffRecords > 0) {
            
            for(Integer i = 0; i < numOffRecords; i++) {
                
                EAW_Window_Tag_Junction__c windowTagJunc = new EAW_Window_Tag_Junction__c();
                windowTagJunctionList.add(windowTagJunc);
            }            
            if( windowTagJunctionList.isEmpty() == False && isCreated ) {      
            
                    insert windowTagJunctionList;        
            }
        }
        
        return windowTagJunctionList;
    }
    
    public static List<EAW_Window_Guideline_Strand__c> createWindowGuideLineStrand ( Integer numoffRecords, boolean isCreated){
          
        List<EAW_Window_Guideline_Strand__c> edmWindowGuidelineStrandList = new List<EAW_Window_Guideline_Strand__c>();
        List<EAW_Window_Guideline__c> windowGuidelines = createWindowGuideLine(1, true);
        
        if( numOffRecords != NULL && numOffRecords > 0 ){
            
            for( Integer i = 0; i < numOffRecords; i++){
                               
                EAW_Window_Guideline_Strand__c windowGuideLineStrand =  new EAW_Window_Guideline_Strand__c(Language__c = 'Tamil', EAW_Window_Guideline__c = windowGuidelines[0].Id, License_Type__c = 'Exhibition License', Media__c = 'EST', Territory__c = 'India');
                edmWindowGuidelineStrandList.add(windowGuideLineStrand);                   
            }              
            if( edmWindowGuidelineStrandList.isEmpty() == False && isCreated) {       
                
                    insert edmWindowGuidelineStrandList;        
            }       
        }
        return edmWindowGuidelineStrandList;
    }
    
    public static List<EAW_Release_Date_Guideline__c> createReleaseDateGuideline ( Integer numoffRecords, Boolean isCreated, String productType, List<String> releaseDateType, String territory, String language ) {
    
        List<EAW_Release_Date_Guideline__c> rdgList = new List<EAW_Release_Date_Guideline__c>();
    
        If( numOffRecords != NULL && numOffRecords > 0 ) {
            
            for( Integer i = 0; i < numOffRecords; i++ ) {
                EAW_Release_Date_Guideline__c rdg = new EAW_Release_Date_Guideline__c( Name = 'Test'+i, Product_Type__c = productType, EAW_Release_Date_Type__c = releaseDateType[i], Territory__c = territory, Language__c = language );
                rdgList.add(rdg);
            }
            
            if( rdgList.isEmpty() == False && isCreated) {       
                
                    insert rdgList;        
            } 
        }
        
        return rdgList;
    }
    
}