public class EAW_ConvertTitleDetailsWrapper {

public class GetTitleDetailsWrapper {

    public List<Data> data; 
    public Links_Z links;

}

	public class Attributes {
	
	//    public Relationships relationships ; 
	
	
	    public String type; // in json: type
		//public String id;
	    public String foxId ; 
	    public Integer rowIdObject ; 
	    public Integer rowIdVersion;
	    public String titleName ; 
		public String financialProductId;
		public String publicCode;
		public String publicCodeDescription;
		public String scopeIndicator;
		public Version version;
	
	    public String seriesName ; 
	    public String seasonNumber ; 
	    public String productEpisodeNumber ; 
	    public String productTypeCode ;
	     
	    public String productTypeDescription ; 
	    public String episodeName ; 
	    public String lifecycleStatusDescription ; 
	    public String lifecycleStatusCode ; 
	    public String lifecycleStatusGroup ; 
	    public String lifecycleEffectiveDate ; 
	    public String lifecycleNotes ; 
	    public String productEpisodeCount ; 
	    public Integer productionCalendarYear ; 
	    public Integer productionFiscalYear ; 
	    public String releaseCalendarYear ; 
	    public String releaseFiscalYear ; 
	    public String primaryProductionCountryCode ; 
	    public String primaryProductionCountryDescription ; 
	    public String financeDivisionCode ; 
	    public String financeDivisionDescription ; 
	    public String internationalProductTypeCode ; 
	    public String internationalProductTypeDescription ; 
	    public String publishIndicatorCode ; 
	    public String publishIndicatorDescription ; 
	    public String mergedTitle ; 
	    public String unsortedTitle ; 
	    public String sortedTitle ; 
	    public String originalMediaCode ; 
	    public String originalMediaDescription ; 
	    public String originalLanguageCode ; 
	    public String originalLanguageDescription ; 
	    public String directorName ; 
	    public String productAcqInd;
	    public String productFinanceCode;
	    
	    public List<Location> location ; 
	    public List<ProdCompany> prodCompany ; 
	    public List<Extensions> extensions ; 
	}


	public class Title {
	
	    public Links links ; 
	}


	public class Data {
		public String type;
		public Integer id;
		public Relationships relationships;
	    public Attributes attributes ; 
	}


	public class Relationships {
	
	    public Title title ; 
	}
	
	public class Links_Z {
	
	    public String self ; 
	}
	
	
	public class Links {
	
	    public String related ;
	}



	public class Version{
		public String titleSubTypCode;
	    public String programRunTime ; 
		public Integer actualRunTime;
	}
	public class Location{
	
	}
	public class ProdCompany{
	 	public String foxId ; 
	 	public String rowIdObject ; 
	 	public String rowIdTitle;
	 	public String productionCompanySortOrder;
	 	public String productionCompanyCode;
	 	public String productionCompanyDescription;
	}
	public class Extensions{
    	public String rowIdObject ; 
    	public String attributeValueCode;
    	public String attributeTypeCode;
    	public String attributeTypeDescription;
		public String attributeValueDescription;
	}
}