public with sharing class EAW_WindowPlanGuidelineController {
    
    @AuraEnabled
    public static List<EAW_Plan_Guideline__c> getPlanGuidelines(String recordId) {
        
        String junctionQuery = 'SELECT Plan__c, Window_Guideline__c FROM EAW_Plan_Window_Guideline_Junction__c WHERE Window_Guideline__c = \'' + recordId + '\'';
        List<EAW_Plan_Window_Guideline_Junction__c> planWindowGuidelineJunctions = Database.query(junctionQuery);
        List<String> planGuidelineIds = new List<String>();
        
        for(EAW_Plan_Window_Guideline_Junction__c planWindowGuidelineJunction : planWindowGuidelineJunctions) {
            planGuidelineIds.add(planWindowGuidelineJunction.Plan__c);
        }

        String planGuidelineQuery = 'SELECT Name, Status__c, Version__c, Territory__c, Language__c, Product_Type__c, Release_Type__c ' +
            'FROM EAW_Plan_Guideline__c WHERE Id IN :planGuidelineIds ORDER BY Name';
        List<EAW_Plan_Guideline__c> planGuidelines = Database.query(planGuidelineQuery);

        return planGuidelines;
    }
}