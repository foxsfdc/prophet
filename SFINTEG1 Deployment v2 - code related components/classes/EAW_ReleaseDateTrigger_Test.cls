/*
 Test class for
 Trigger : EAW_ReleaseDateTrigger
 Handler : EAW_ReleaseDate_Handler
 */
@isTest
public class EAW_ReleaseDateTrigger_Test {
    
    
    @testSetup static void setup() {
        
        List<EAW_Release_Date__c> releaseDateList = EAW_TestDataFactory.createReleaseDate(1, true);
        
        List<EAW_Customer__c> customerList = EAW_TestDataFactory.createEAWCustomer(1,true);
        //LRCC-1560 - Replace Customer lookup field with Customer text field
        //EAW_Release_Date_Guideline__c releaseDateGuideLine = new EAW_Release_Date_Guideline__c( Product_Type__c = 'Format', Territory__c = 'United States +', Language__c = 'Dutch', Customer__c = customerList[0].Id, EAW_Release_Date_Type__c = 'SPVOD' ,
        //Active__c = true);
        EAW_Release_Date_Guideline__c releaseDateGuideLine = new EAW_Release_Date_Guideline__c( Product_Type__c = 'Format', Territory__c = 'United States +', Language__c = 'Dutch', Customers__c = customerList[0].name, EAW_Release_Date_Type__c = 'SPVOD' ,
        Active__c = true);
        insert releaseDateGuideLine;
    }
    /*
    @isTest static void preventDuplicate_Test() {
    
        EAW_Release_Date__c releaseDateIns = new EAW_Release_Date__c();
        insert releaseDateIns;
    }
    @isTest static void preventDuplicatebeforeupdate_Test() {
    
        EAW_Release_Date__c releaseDateIns = new EAW_Release_Date__c(Release_Date__c = system.today());
        insert releaseDateIns;
        
        releaseDateIns.Release_Date__c = system.today() + 10; 
        
        try {
            
            update releaseDateIns;
        } catch(Exception ex) {
        }
    }
    @isTest static void preventDuplicateUpdate_Test() {
        
        List<EAW_Customer__c> customerList = EAW_TestDataFactory.createEAWCustomer(1,true);
        
        EAW_Release_Date_Guideline__c releaseDateGuideLine = new EAW_Release_Date_Guideline__c( Product_Type__c = 'Feature', Territory__c = 'United States +', Language__c = 'Dutch', Customer__c = customerList[0].Id, EAW_Release_Date_Type__c = 'SPVOD');
        insert releaseDateGuideLine;
        
        List<EAW_Title__c> titleIns = EAW_TestDataFactory.createEAWTitle( 1, true); 
    
        EAW_Release_Date__c releaseDateIns = new EAW_Release_Date__c(Release_Date__c = system.today(), Release_Date_Guideline__c = releaseDateGuideLine.Id, Title__c = titleIns[0].Id);
        insert releaseDateIns;        
        
        //EAW_Rule__c ruleInstance = new EAW_Rule__c(RecordTypeId = releaseDateRuleRecordTypeId, Plan_Guideline__c = planGuideLineList[0].Id);
        //insert ruleInstance;
        
        //EAW_Rule_Detail__c ruledetailList = new EAW_Rule_Detail__c(Rule_No__c = ruleInstance.Id, Condition_Field__c = 'Local Theatrical Release', Condition_Tag__c = 'Blockbuster',
        //Condition_Criteria__c = tagInstupdate.Id, RecordTypeId = ruleDetailRuleRecordTypeId, Territory__c = 'United States +', Condition_Operator__c = '<', Condition_Date__c = system.today() + 15);
        //insert ruledetailList;

        releaseDateIns.Release_Date__c = system.today() + 10;      
        releaseDateIns.Release_Date_Guideline__c = releaseDateGuideLine.Id;
        releaseDateIns.Title__c = titleIns[0].Id;
        
        update releaseDateIns;        
        
    }
    
    @isTest static void preventDuplicateUpdateWithTitle_Test() { 
        
        EAW_Customer__c customer = new EAW_Customer__c(Name='Test customer on 18/12', Media__c = 'Free TV', Customer_Id__c = 1234789);
        insert customer;
        
        EDM_REF_PRODUCT_TYPE__c refProdType = new EDM_REF_PRODUCT_TYPE__c(PROD_TYP_CD__c = 'Episode', Name = 'Episode');
        insert refProdType;
                                
        List<EAW_Title__c> titleIns = EAW_TestDataFactory.createEAWTitle( 1, true);     
        
        List<EDM_GLOBAL_TITLE__c>  edmTile = [SELECT Id, Name FROM EDM_GLOBAL_TITLE__c LIMIT 1];
        edmTile[0].PROD_TYP_CD_INTL__c = refProdType.id;
        update edmTile;
        
        Id releaseDateRuleRecordTypeId = Schema.SObjectType.EAW_Rule__c.getRecordTypeInfosByName().get('Qualifier').getRecordTypeId();
        Id ruleDetailRuleRecordTypeId = Schema.SObjectType.EAW_Rule_Detail__c.getRecordTypeInfosByName().get('Qualifier').getRecordTypeId(); 
        
               
        List<EAW_Plan_Guideline__c> planGuideLineList = new List<EAW_Plan_Guideline__c> {
            new EAW_Plan_Guideline__c(Product_Type__c = 'Episode', Auto_Create__c = true)
        };
        insert planGuideLineList;

         EAW_Window_Guideline__c windowGuidelineList = new EAW_Window_Guideline__c(Status__c = 'Draft');
         insert windowGuidelineList;
         
         List<EAW_Window_Guideline_Strand__c> windowGuideLineStrandList = new List<EAW_Window_Guideline_Strand__c> {
            
            new EAW_Window_Guideline_Strand__c(License_Type__c = 'Exhibition License', EAW_Window_Guideline__c = windowGuideLineList.Id,
            Media__c = 'Theatrical', Territory__c = 'Afghanistan', Language__c = 'Afrikaans')
        };    
        insert windowGuideLineStrandList ; 
        
        EAW_Window__c winowIns = new EAW_Window__c();
        winowIns.EAW_Window_Guideline__c = windowGuideLineList.Id;        
        insert winowIns;
        
        //LRCC-1653 - -In this ticket once passed QA it with remove the commented code.
        //EAW_Window_Strand__c windowStrandIns = new EAW_Window_Strand__c();
        //windowStrandIns.Territory__c = 'Afghanistan';
        //windowStrandIns.Language__c = 'Afrikaans';
        //windowStrandIns.Media__c = 'Theatrical';
        //windowStrandIns.License_Type__c = 'Exhibition License';
        //windowStrandIns.Window__c = winowIns.Id;        
        //insert windowStrandIns;   
        
        List<EAW_Plan_Window_Guideline_Junction__c> planWindowList = new List<EAW_Plan_Window_Guideline_Junction__c> {
            new EAW_Plan_Window_Guideline_Junction__c(Plan__c = planGuideLineList[0].Id , Window_Guideline__c = windowGuideLineList.Id)
        };
        insert planWindowList;

        windowGuidelineList.Status__c = 'Active';
        update windowGuidelineList;
        
        planGuideLineList[0].Status__c = 'Active';      
        update planGuideLineList;
        
        EAW_Tag__c tagInst = new EAW_Tag__c(Tag_Type__c = 'Title Tag');
        insert tagInst;
        
        EAW_Tag__c tagInstupdate = new EAW_Tag__c(Tag_Type__c = 'Title Tag');
        insert tagInstupdate;
        
        List<EAW_Customer__c> customerList = EAW_TestDataFactory.createEAWCustomer(1,true);
        
        List<EAW_Release_Date_Type__c> releaseDateTypeList = EAW_TestDataFactory.createReleaseDateType(1, true);
                
        EAW_Release_Date_Guideline__c releaseDateGuideLine = new EAW_Release_Date_Guideline__c( Product_Type__c = 'Feature', Territory__c = 'United States +', Language__c = 'Dutch', Customer__c = customerList[0].Id, EAW_Release_Date_Type__c = 'SPVOD' ,
        Active__c = true);
        insert releaseDateGuideLine;
        
        EAW_Rule__c ruleInstance = new EAW_Rule__c(RecordTypeId = releaseDateRuleRecordTypeId, Plan_Guideline__c = planGuideLineList[0].Id);
        insert ruleInstance;
        
        EAW_Rule_Detail__c ruledetailList = new EAW_Rule_Detail__c(Rule_No__c = ruleInstance.Id, Condition_Field__c = 'Local Theatrical Release', Condition_Tag__c = 'Blockbuster',
        Condition_Criteria__c = tagInstupdate.Id, RecordTypeId = ruleDetailRuleRecordTypeId, Territory__c = 'United States +', Condition_Operator__c = '<', Condition_Date__c = system.today() + 15);
        insert ruledetailList;   
        
         EAW_Release_Date__c releaseDateIns = new EAW_Release_Date__c(Release_Date__c = system.today(), Title__c = titleIns[0].Id, Release_Date_Guideline__c = releaseDateGuideLine.Id );
        insert releaseDateIns;
        
        releaseDateIns.Release_Date_Guideline__c = releaseDateGuideLine.Id;
        update releaseDateIns;
        
    } 
    */
    @isTest static void reQualificationCheckForReleaseDateGuidelineAndWindowGuidline_Test() { 
        
        EAW_Release_Date_Guideline__c releaseDateGuideLineIns = [SELECT Id, Name FROM EAW_Release_Date_Guideline__c LIMIT 1];
        
        Id releaseDateRuleRecordTypeId = Schema.SObjectType.EAW_Rule__c.getRecordTypeInfosByName().get('Release Date Calculation').getRecordTypeId();
        
        Id releaseDateRuleDetiailRecordTypeId = Schema.SObjectType.EAW_Rule_Detail__c.getRecordTypeInfosByName().get('Release Date Calculation').getRecordTypeId();
        
        EAW_Release_Date__c releaseDateIns = new EAW_Release_Date__c(Release_Date__c = system.today(), Release_Date_Guideline__c = releaseDateGuideLineIns.Id);
        insert releaseDateIns;    
        
        EAW_Rule__c ruleInstance = new EAW_Rule__c(RecordTypeId = releaseDateRuleRecordTypeId, Release_Date_Guideline__c = releaseDateGuideLineIns.Id);
        insert ruleInstance;
       
        EAW_Rule_Detail__c ruledetailList = new EAW_Rule_Detail__c(Rule_No__c = ruleInstance.Id, Condition_Tag__c = 'Blockbuster',
        RecordTypeId = releaseDateRuleDetiailRecordTypeId, Territory__c = 'United States +', Condition_Operator__c = '<', Condition_Date__c = system.today() + 15,
        Conditional_Operand_Id__c = releaseDateGuideLineIns.Id);
        
        insert ruledetailList;  
        
        releaseDateIns.Release_Date__c = system.today() +10;
        try {
            
            update releaseDateIns;
        } catch(Exception ex) {
        }
    
    }
}