public class EAW_NewWindowGuidelineStrandController {
   
    public EAW_Window_Guideline_Strand__c windowGuidelineStrand {get;set;}
    public String windowGuidelineId {get;set;}  
    public String windowGuidelineName {get;set;}  
 
    public EAW_NewWindowGuidelineStrandController(ApexPages.StandardController controller) {
    
        windowGuidelineStrand = (EAW_Window_Guideline_Strand__c)controller.getRecord();
        windowGuidelineId = windowGuidelineStrand.EAW_Window_Guideline__c;
        if(String.isNotBlank(windowGuidelineId)){
            windowGuidelineName = [SELECT Id, Name FROM EAW_Window_Guideline__c WHERE Id =:windowGuidelineId].Name;
        }
    }
}