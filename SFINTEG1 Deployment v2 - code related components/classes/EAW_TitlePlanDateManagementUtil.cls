public class EAW_TitlePlanDateManagementUtil {

    Schema.DescribeSobjectResult rdgSchema = EAW_Release_Date_Guideline__c.sObjectType.getDescribe();
    String rdgKeyPrefix = rdgSchema.getKeyPrefix();
    Set<Id> windowGuideLinesRDGIdSet = new Set<Id>();
    
    public EAW_TitlePlanDateManagementUtil.TitlePDMUtilResult getWindowReleaseDateGuideline(List<String> windowTags,List<String> windowNames, List<String> windowTypes,
                                                                                            List<String> windowStatusValue, String windowDateStart,String windowDateEnd){
        //System.debug('windowTypes:::'+windowTypes);
        List<String> windowType = windowTypes;//new List<String>{'Library','Current'};
        List<String> windowStatus = windowStatusValue;//new List<String>{'Estimated','Firm'};
        List<Id> windowTagId = windowTags;//new List<String>{'aAB3D0000000ZVT','aAB3D000000Cxrf'};
        String windowStartDateFrom = windowDateStart;//'';
        String windowStartDateTo = windowDateEnd;//'';
        Set<Id> windowGuideLineNameSet = new Set<Id>();//{'aAF3D000000DEsqWAG'};
        if(windowNames.size() > 0){
        
            windowGuideLineNameSet.addAll((Set<Id>)JSON.deserialize(JSON.serialize(windowNames), Set<Id>.class));
        }
        
        Set<Id> windowGuideLineIdSet = new Set<Id>();
        Set<Id> tempWGLSet = new Set<Id>();
        Set<Id> windowTitleIdSet = new Set<Id>();
        
        EAW_TitlePlanDateManagementUtil.TitlePDMUtilResult  responseWrapper = new EAW_TitlePlanDateManagementUtil.TitlePDMUtilResult();
        responseWrapper.result = 'success';
        try {
        
            if(windowGuideLineNameSet.isEmpty()==False) {
                windowGuideLineIdSet.addAll(windowGuideLineNameSet);
            }
            
            //based on Window_Type
            if(windowType!=null & windowType.isEmpty()==False) {
                
                tempWGLSet = new Set<Id>();
                String wgQuery = 'SELECT Id FROM EAW_Window_Guideline__c WHERE Status__c = \'Active\' AND Soft_Deleted__c=\'FALSE\'';
                if(windowGuideLineIdSet.size() > 0){
                
                    wgQuery = wgQuery+' AND Id IN :windowGuideLineIdSet';
                }
                if(windowType.size() > 0){
                
                    wgQuery = wgQuery+' AND Window_Type__c IN :windowType';
                }
                System.debug('wgQuery :::'+wgQuery );
                 
                for(EAW_Window_Guideline__c wg :Database.query(wgQuery)) {
                    System.debug('wg :::'+wg );
                    tempWGLSet.add(wg.Id);
                }
                windowGuideLineIdSet = new Set<Id>(tempWGLSet);
            }
            //System.debug('windowGuideLineIdSet :::'+windowGuideLineIdSet );
            
            if(String.isNotBlank(windowStartDateFrom) || String.isNotBlank(windowStartDateTo) || 
                (windowStatus!=null & windowStatus.isEmpty()==False) ) {
                
                String soqlStr = 'SELECT Id,EAW_Window_Guideline__c,EAW_Title_Attribute__c FROM EAW_Window__c WHERE ';
                String whereClause = 'Soft_Deleted__c = \'FALSE\' ';
                tempWGLSet = new Set<Id>();
                
                if(windowGuideLineIdSet.isEmpty()==False) {
                    whereClause += 'AND EAW_Window_Guideline__c IN :windowGuideLineIdSet';
                }
                if(String.isNotBlank(windowStartDateFrom)) {
                    
                    //if(whereClause.length()>0) {
                        whereClause += ' AND ';
                    //}
                    whereClause += 'Start_Date__c >='+ windowStartDateFrom;
                }
                if(String.isNotBlank(windowStartDateTo)) {
                
                    //if(whereClause.length()>0) {
                        whereClause += ' AND ';
                    //}
                    whereClause += 'Start_Date__c <='+ windowStartDateTo;
                    //whereClause += 'End_Date__c <='+ windowStartDateTo;
                }
                if(windowStatus.isEmpty()==False) {
                    
                    //if(whereClause.length()>0) {
                        whereClause += ' AND ';
                    //}
                    whereClause += 'Status__c IN :windowStatus';
                }
                //System.debug('Query Rows After:' + Limits.getQueryRows());
                Integer queryLimit = ((LIMITS.getLimitQueryRows() -  Limits.getQueryRows())-1000);
                if(queryLimit > 0){
                
                    whereClause += ' LIMIT '+ queryLimit;
                }
                System.debug('SOQL Window Query:::'+soqlStr+whereClause);
                for(EAW_Window__c win : Database.query(soqlStr+whereClause)) {
                    tempWGLSet.add(win.EAW_Window_Guideline__c );
                    windowTitleIdSet.add(win.EAW_Title_Attribute__c);
                }
                
                windowGuideLineIdSet = new Set<Id>(tempWGLSet);
            }
            
            if(windowTagId!=null & windowTagId.isEmpty()==False) {
                
                tempWGLSet = new Set<Id>();
                Set<Id> tempTitleIdSet = new Set<Id>();
                String wgTagJuncQuery = 'SELECT Id,Window__r.EAW_Window_Guideline__c,Window__r.EAW_Title_Attribute__c FROM EAW_Window_Tag_Junction__c WHERE Tag__c IN :windowTagId';
                if(windowGuideLineIdSet != NULL && windowGuideLineIdSet.size() > 0){
                
                 wgTagJuncQuery = wgTagJuncQuery +' AND Window__r.EAW_Window_Guideline__c IN :windowGuideLineIdSet';
                }
                if(windowTitleIdSet!= NULL && windowTitleIdSet.size() > 0){
                
                 wgTagJuncQuery = wgTagJuncQuery +' AND Window__r.EAW_Title_Attribute__c IN :windowTitleIdSet';
                }
                //System.debug('wgTagJuncQuery:::::'+wgTagJuncQuery); 
                for(EAW_Window_Tag_Junction__c winTagJunction : Database.query(wgTagJuncQuery)) {
                        
                    tempWGLSet.add(winTagJunction.Window__r.EAW_Window_Guideline__c );
                    tempTitleIdSet.add(winTagJunction.Window__r.EAW_Title_Attribute__c);
                }
                windowGuideLineIdSet = new Set<Id>(tempWGLSet);
                windowTitleIdSet = new Set<Id>(tempTitleIdSet);
            } 
            
            if(windowGuideLineIdSet.isEmpty() == False) {
                for(EAW_Rule__c rule : [SELECT Id,Condition_Field__c, Conditional_Operand_Id__c, 
                                    (select Id,Conditional_Operand_Id__c from Rule_Orders__r WHERE Conditional_Operand_Id__c != null AND (Condition_Met_Days__c != null OR Condition_Met_Months__c != null))
                                         FROM EAW_Rule__c WHERE Window_Guideline__c IN :windowGuideLineIdSet AND Date_To_Modify__c='Start Date']) {
                    
                    if(rule.Condition_Field__c != null && rule.Condition_Field__c.equals('Release Date Status') && String.isNotBlank(rule.Conditional_Operand_Id__c) ) {
                        
                        filterRDGId(rule.Conditional_Operand_Id__c.split(';'));
                    }
                    
                    if(rule.Rule_Orders__r != null) {
                        List<EAW_Rule_Detail__c> childRuleDetailList = rule.Rule_Orders__r;
                        //System.debug('childRuleDetailList ::::'+childRuleDetailList );
                        for(EAW_Rule_Detail__c ruleDetail : childRuleDetailList) {
                            filterRDGId(ruleDetail.Conditional_Operand_Id__c.split(';'));
                        }
                    }
                }
                responseWrapper.rdGuideLineIdSet = windowGuideLinesRDGIdSet;
            }
            responseWrapper.titleIdSet = windowTitleIdSet;
        }catch(Exception e) {
            responseWrapper.result = 'Exception -'+ e.getMessage();
            responseWrapper.rdGuideLineIdSet = null;
            responseWrapper.titleIdSet = null;
        }
        System.debug('responseWrapper::::'+responseWrapper);
        return responseWrapper;
    }
    
    private void filterRDGId(List<String> idsList) {
        //Set<Id> filteredIdSet = new Set<Id>();
        for(String s : idsList) {
            if(s.startsWith(rdgKeyPrefix)) {
                windowGuideLinesRDGIdSet.add(s);
            }
        }
        //System.debug('windowGuideLinesRDGIdSet:::'+windowGuideLinesRDGIdSet);
    }
    
    public EAW_TitlePlanDateManagementUtil.TitlePDMUtilResult getReleaseWindowDateGuideline( List<String> types) {
    
        List<String> releaseDateType = types;//new List<String>();
        Set<Id> rdgIdSet = new Set<Id>();
        Set<Id> windowGuideLineIdSet = new Set<Id>();
        Set<String> windowGuideLineAliasSet = new Set<String>();
         System.debug('types::::'+types);
        EAW_TitlePlanDateManagementUtil.TitlePDMUtilResult  responseWrapper = new EAW_TitlePlanDateManagementUtil.TitlePDMUtilResult();
        responseWrapper.result = 'success';
        try {
            if(releaseDateType != null && releaseDateType.size()>0) {
                
                for(EAW_Release_Date_Guideline__c rdg : [SELECT Id FROM EAW_Release_Date_Guideline__c 
                                WHERE EAW_Release_Date_Type__c IN :releaseDateType AND Active__c=True AND Soft_Deleted__c='FALSE']) {
                    rdgIdSet.add(rdg.Id);
                    System.debug('rdgIdSet::::'+rdg.Id);
                }
                  System.debug('rdgIdSet::::'+rdgIdSet);
                if(rdgidSet.size()>0) {
                    for(EAW_Rule_Detail__c ruleDetail : [SELECT Id,Condition_Field__c, Conditional_Operand_Id__c,Rule_No__r.Window_Guideline__c, Rule_No__r.Window_Guideline__r.Window_Guideline_Alias__c 
                                FROM EAW_Rule_Detail__c WHERE Conditional_Operand_Id__c != null AND Rule_No__r.Date_To_Modify__c='Start Date' 
                            AND (Condition_Met_Days__c != null OR Condition_Met_Months__c != null) AND Rule_No__r.Window_Guideline__c != null ]) {
                        System.debug('ruleDetail ::::'+ruleDetail );
                        System.debug('ruleDetail.Rule_No__r.Window_Guideline__c::::'+ruleDetail.Rule_No__r.Window_Guideline__c);
                       //if(windowGuideLineIdSet.contains(ruleDetail.Rule_No__r.Window_Guideline__c)) {
                           //break;
                       //} else {
                           for(String operandStr : ruleDetail.Conditional_Operand_Id__c.split(';')) {
                               Id tempId = Id.valueOf(operandStr);
                               if(rdgIdSet.contains(tempId)) {
                                   windowGuideLineIdSet.add(ruleDetail.Rule_No__r.Window_Guideline__c);
                                   windowGuideLineAliasSet.add( ruleDetail.Rule_No__r.Window_Guideline__r.Window_Guideline_Alias__c);
                                   /*if(ruleDetail.Rule_No__r.Window_Guideline__c != NULL){
                                       windowGuideLineIdSet.add(ruleDetail.Rule_No__r.Window_Guideline__c);
                                   }
                                   if(ruleDetail.Rule_No__r.Window_Guideline__r.Window_Guideline_Alias__c != NULL){
                                       windowGuideLineAliasSet.add( ruleDetail.Rule_No__r.Window_Guideline__r.Window_Guideline_Alias__c);
                                   }*/
                                   break;
                               }
                           }
                       //}                                    
                    } //ruleDetail for loop
                    
                    responseWrapper.windowGuideLineIdSet = windowGuideLineIdSet;
                    responseWrapper.windowGuideLineAliasSet = windowGuideLineAliasSet;
                }
            }
        }catch(Exception e) {
            responseWrapper.result = 'Exception -'+ e.getMessage();
            responseWrapper.windowGuideLineIdSet = null;
        }
         System.debug('responseWrapper::::'+responseWrapper);
        return responseWrapper;
    }
    
    public class TitlePDMUtilResult {
        
        public String result;
        public Set<Id> rdGuideLineIdSet = new Set<Id>();
        public Set<Id> windowGuideLineIdSet = new Set<Id>();
        public Set<String> windowGuideLineAliasSet = new Set<String>();
        public Set<Id> titleIdSet = new Set<Id>();
    }

}