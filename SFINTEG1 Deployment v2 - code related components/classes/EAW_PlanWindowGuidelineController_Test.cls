@isTest
public class EAW_PlanWindowGuidelineController_Test {
    
    @testSetup static void setupTestData() {
        
        EAW_TestDataFactory.createWindowGuideLine(2, true);
        EAW_TestDataFactory.createPlanGuideline(1, true);        
    }
    
    @isTest static void removeSelectedJunctions_Test(){
        
        Map<String, List<EAW_Window_Guideline__c>> obtainedresult = new Map<String, List<EAW_Window_Guideline__c>>();
        
        List<EAW_Window_Guideline__c> windowGuidelineList = [SELECT Id, Name  FROM EAW_Window_Guideline__c LIMIT 2];
        
        List<EAW_Plan_Guideline__c> planguidelineList = [SELECT Id, Name  FROM EAW_Plan_Guideline__c LIMIT 1];
        
        List<EAW_Plan_Window_Guideline_Junction__c> planWindowJuncList = EAW_TestDataFactory.createPlanWindowGuideLineJunction(2, FALSE);
        planWindowJuncList[0].Plan__c = planguidelineList[0].Id;
        planWindowJuncList[0].Window_Guideline__c = windowGuidelineList[0].Id;
        
        planWindowJuncList[1].Plan__c = planguidelineList[0].Id;
        planWindowJuncList[1].Window_Guideline__c = windowGuidelineList[1].Id;
        
        obtainedresult = EAW_PlanWindowGuidelineController.removeSelectedJunctions( new List<EAW_Window_Guideline__c> {windowGuidelineList[0]}, String.ValueOf(planguidelineList[0].Id));
        
        System.assertEquals(True, obtainedresult != null);
    }
    
    @isTest static void invalidateWindowGuidelines_Test(){ 
        
        List<EAW_Window_Guideline__c> windowGuidelineList = [SELECT Id, Name  FROM EAW_Window_Guideline__c LIMIT 2];        
        
        EAW_PlanWindowGuidelineController.invalidateWindowGuidelines(windowGuidelineList);
        
        List<EAW_Window_Guideline__c> updatedList = [SELECT Id, Name, Status__c  FROM EAW_Window_Guideline__c LIMIT 2];
        
        System.assertEquals( true, updatedList[0].Status__c  == 'Inactive');
    }
    
    @isTest static void cloneSelectedWindowGuidelines_Test() {
        
         
         List<EAW_Window_Guideline__c> windowGuidelineList = [ Select Id, Name, Clone_WG_Name__c, Window_Guideline_Alias__c, Product_Type__c, Customers__c, Description__c,  End_Date__c, Media__c, Outside_Date__c, Start_Date__c, Tracking_Date__c, Version__c,  Window_Type__c, Start_Date_Rule__c, End_Date_Rule__c, Outside_Date_Rule__c, Tracking_Date_Rule__c, Version_Notes__c From EAW_Window_Guideline__c limit 1 ];
         List<EAW_Plan_Guideline__c> planguidelineList = [SELECT Id, Name  FROM EAW_Plan_Guideline__c LIMIT 1];
         
         EAW_Window_Guideline__c obtainedwindow = EAW_PlanWindowGuidelineController.cloneSelectedWindowGuidelines(windowGuidelineList[0], String.ValueOf(planguidelineList[0].Id));
         
         System.assertEquals( true, obtainedwindow != null );
    }
    
    @isTest static void addSelectedExistingWindowGuidelines_Test() {        
         
         List<EAW_Plan_Guideline__c> planguidelineList = [SELECT Id, Name  FROM EAW_Plan_Guideline__c LIMIT 1];
         
         List<EAW_Window_Guideline__c> windowGuidelineList = [SELECT Id, Name  FROM EAW_Window_Guideline__c LIMIT 2];  
         
         List<String> windowIdList = new List<String>();
         
         for(EAW_Window_Guideline__c windIns :  windowGuidelineList ) {
             
             windowIdList.add(windIns.Id);
         }
         
         List<EAW_Window_Guideline__c> obtainedList = EAW_PlanWindowGuidelineController.addSelectedExistingWindowGuidelines(windowIdList, String.ValueOf(planguidelineList[0].Id));
         
         System.assertEquals( true, obtainedList != null );
    }
    
    @isTest static void getWindowGuidelines_Test() {
        
        List<EAW_Plan_Guideline__c> planguidelineList = [SELECT Id, Name  FROM EAW_Plan_Guideline__c LIMIT 1];
        
        List<EAW_Window_Guideline__c> windowGuidelineList = [SELECT Id, Name  FROM EAW_Window_Guideline__c LIMIT 2];
                
        EAW_Plan_Window_Guideline_Junction__c ruleDetail = new EAW_Plan_Window_Guideline_Junction__c (Plan__c = planguidelineList[0].Id, Window_Guideline__c = windowGuidelineList[0].Id);
        insert ruleDetail;
        
        List<String> obtainedList = EAW_PlanWindowGuidelineController.getExhibitionData(String.ValueOf(planguidelineList[0].Id));
        
        System.assertEquals( true, obtainedList != null );
        
    }
    @isTest static void searchForIds_Test() {
        
        EAW_Window_Guideline__c ruleDetail = new EAW_Window_Guideline__c ( Status__c = 'Draft', Name = 'Test', Window_Type__c = 'First-Run', Product_Type__c = 'Mini-series', Window_Guideline_Alias__c = 'Test alias');
        insert ruleDetail;
        
        EAW_PlanWindowGuidelineController.searchForIds('Test');
    }    
    
    @isTest static void createPlanWindowGuidelineJunction_Test() {
        
        List<EAW_Plan_Guideline__c> planguidelineList = [SELECT Id, Name  FROM EAW_Plan_Guideline__c LIMIT 1];
        
        List<EAW_Window_Guideline__c> windowGuidelineList = [SELECT Id, Name  FROM EAW_Window_Guideline__c LIMIT 2];
        
        List<EAW_Window_Guideline__c > obtainedList = EAW_PlanWindowGuidelineController.createPlanWindowGuidelineJunction(String.ValueOf(planguidelineList[0].Id), String.ValueOf(windowGuidelineList [0].Id));
        
        System.assertEquals( true, obtainedList != null );
    }
    
    @isTest static void getPlanStatusFromRecordId_Test() {
        
        List<EAW_Plan_Guideline__c> planguidelineList = [SELECT Id, Name  FROM EAW_Plan_Guideline__c LIMIT 1];
        
        EAW_PlanWindowGuidelineController.getPlanStatusFromRecordId(String.ValueOf(planguidelineList[0].Id));
    }
}