global class EAW_ReleaseDate_API_BatchProcess implements Database.AllowsCallouts,Database.Batchable<sObject>{
	public List<Id> releaseDateIdList = new List<Id>();
    global Database.QueryLocator start(Database.BatchableContext bc) {
    	//If following query is modified make sure to change the query in EAW_ReleaseDates_Outbound.sendData method
        return Database.getQueryLocator(
            'SELECT '+
					'ID, '+  
					'Release_Date_Guideline__c, '+  
					'Release_Date_Guideline__r.name, '+  
					'Product_Type__c, '+  
					'Title__r.FIN_PROD_ID__c, '+  
					'Title__r.FOX_ID__c, '+
					'Title__r.Product_Version_Id__c, '+
					'Title__r.Name, '+  
					'EAW_Release_Date_Type__c, '+  
					'Territory__c, '+  
					'Language__c, '+  
					'Release_Date__c, '+  
					'Status__c, '+  
					'Feed_Date__c, '+  
					'Feed_Date_Status__c, '+  
					'Manual_Date__c, '+  
					'Temp_Perm__c, '+  
					'LastModifiedDate, '+  
					'Release_Date_Guideline__r.All_Rules__c, '+  
					'Galileo_DML_Type__c, '+  
					'Repo_DML_Type__c, '+  
					'Sent_To_Galileo__c, '+  
					'Sent_To_Repo__c, '+  
					'Send_To_Third_Party__c, '+  
					'Soft_Deleted__c '+
				'FROM '+
					'Eaw_release_date__c '+
				'WHERE '+
					'(Send_To_Third_Party__c  =  true)'+
					'AND ID IN :releaseDateIdList');
    }
    global void execute(Database.BatchableContext bc, List<Eaw_release_date__c> records){
    try{
    	EAW_ReleaseDates_Outbound ero = new EAW_ReleaseDates_Outbound();
    	checkRecursiveData.bypassDateCalculation=true;//To bypass recursive calculation of downstream release dates
    	ero.aggregateAllRds(records);
    	
        }catch(Exception e){
    		System.debug('e.getMessage(): '+e.getMessage());
            
        }
    }    
    global void finish(Database.BatchableContext bc){
    	// TO do Call Window API batch api
    	//Id batchJobId = Database.executeBatch(new EAW_Window_API_BatchProcess(), EAW_Outbound_Utility.MAX_API_RECORDS_LIMIT);
		//system.debug('EAW_Window_API_BatchProcess batchJobId: '+batchJobId);
    }    
}