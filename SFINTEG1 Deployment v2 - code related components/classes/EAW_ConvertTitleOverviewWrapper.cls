public class EAW_ConvertTitleOverviewWrapper {

public class GetTitleOverviewWrapper {

    public Data data; 
    public Links_Z links;

}

public class Attributes {

    public String type; // in json: type
    public Relationships relationships ; 
    public String id;
    public String foxId ; 
    public String rowIdObject ; 
    public String initialReleaseDateWorldwide ; 
    public String actualRunTime ; 
    public String originalAspectRatioCode ; 
    public String originalAspectRatioDescription ; 
    public String originalLanguageCode ; 
    public String originalLanguageDescription ; 
    public String directorName ; 
    public String titleName ; 
    public String financeDivisionCode ; 
    public String financeDivisionDescription ; 
    public String financialTitleId ; 
    public String initialReleaseDate ; 
    public String productTypeCode ; 
    public String internationalProductTypeCode ; 
    public String internationalProductTypeDescription ; 
    public String lifecycleStatusGroup ; 
    public String lifecycleStatusCode ; 
    public String lifecycleStatusDescription ; 
    public String originalCountryCode ; 
    public String originalCountryDescription ; 
    public String franchiseCode ; 
    public String franchiseDescription ; 
    public String originalMediaCode ; 
    public String originalMediaDescription ; 
    public String programRunTime ; 
    public String originalSoundFormatCode ; 
    public String originalSoundFormatDescription ; 
    public String primaryGenreCode ; 
    public String primaryGenreDescription ; 
    public String ratingCode ; 
    public String ratingDescription ; 
    public String ratingShortDescription ; 
    public String usBoxOfficeAmount ; 
    public Production production ; 
    public Copyright copyright ; 
    public List<Versions> versions ; 
    public List<Synopsis> synopsis ; 
    public List<Alternate> alternate ; 
    public List<Talent> talent ; 
}


public class Title {

    public Links links ; 
}

public class BusinessProject {

    public String businessUnitCode ; 
    public String businessUnitDescription ; 
    public String projectTypeCode ; 
    public String projectTypeDescription ; 
}

public class Data {

    public Attributes attributes ; 
}

public class Talent {

    public String talentTypeCode ; 
    public String talentTypeDescription ; 
    public String titleTalentId ; 
    public String titleOrderValue ; 
    public String talentSortOrder ; 
    public String talentFamilyName ; 
    public String talentMiddleName ; 
    public String talentRoleName ; 
    public String majorTalentIndicator ; 
    public String talentSpeciality ; 
    public String talentHonorific ; 
    public String talentSuffix ; 
    public String talentGivenName ; 
    public String foxVersionId ; 
    public String alternateTalentGivenName ; 
    public String alternateTalentMiddleName ; 
    public String alternateTalentFamilyName ; 
    public String alternateTalentHonorific ; 
    public String alternateTalentSpecialty ; 
    public String alternateTalentSuffix ; 
    public String rowIdTalentAlternate ; 
}

public class Synopsis {

    public String synopsisTypeCode ; 
    public String mediaCode ; 
    public String titleSynopsisText ; 
    public String foxVersionId ; 
}

public class Copyright {

    public String copyrightId ; 
    public String copyrightNotice ; 
    public String foxVersionId ; 
}

public class Relationships {

    public Title title ; 
}

public class Links_Z {

    public String self ; 
}

public class Versions {

    public String parentVersion ; 
    public String jdeVaultId ; 
    public String titleVersionTypeCode ; 
    public String titleVersionTypeDescription ; 
    public String titleVersionDescription ; 
    public String actualRunTime ; 
    public String foxVersionId ; 
    public String madeForMediaCode ; 
    public String madeForMediaDescription ; 
    public String madeForTerritory ; 
}

public class Production {

    public String psConfirmationStatusCode ; 
    public String psConfirmationStatusDescription ; 
    public String titleFinancialId ; 
    public List<BusinessProject> businessProject ; 
}

public class Links {

    public String related ;
}



public class Alternate {

    public String titleAlternateId ; 
    public String alternateIdTypeCode ; 
    public String alternateIdTypeDescription ; 
    public String alternateIdCode ; 
    public String countryCode ; 
    public String countryDescription ; 
    public String foxVersionId ; 
    public String isActive ; 
}
}