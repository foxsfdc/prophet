/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class EAW_TagController_Test {
    
    @testSetup static void setupTestData() {
        String sObjectString = 'EAW_Tag__c';
        Integer noOfRecords = 3;
        Map<String, Object> dynamicFieldsAndValuesMapList = new Map<String, Object>();
        
        dynamicFieldsAndValuesMapList.put('Name','Test tag');
        dynamicFieldsAndValuesMapList.put('Tag_Type__c','Title Tag');
        dynamicFieldsAndValuesMapList.put('Active__c',true);
        EAW_TestDataFactory.createGenericSameTestRecords(dynamicFieldsAndValuesMapList,sObjectString,noOfRecords, true);
    }

    @isTest static void myUnitTest() {
        List<EAW_Tag__c> tagIdsList = [select id from EAW_Tag__c];
        String tagIdstr = '';
        for(EAW_Tag__c tagId:tagIdsList){
            tagIdstr+=tagId.id+', ';
        }
        tagIdstr=tagIdstr.substring(0,tagIdstr.length()-2);
        List<EAW_Tag__c> tags = EAW_TagController.getTagInfo(tagIdstr);
        System.assertEquals(3,tags.size());        
    }
}