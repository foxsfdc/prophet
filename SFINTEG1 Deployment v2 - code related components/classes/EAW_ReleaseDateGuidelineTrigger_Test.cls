@isTest
public class EAW_ReleaseDateGuidelineTrigger_Test {
   
    @isTest static void duplicateCheck_Test() {
        
     List<EAW_Customer__c> customerInstance = EAW_TestDataFactory.createEAWCustomer(1, true);
     //LRCC-1560 - Replace Customer lookup field with Customer text field
        EAW_Release_Date_Guideline__c releaseDateGuideLine = new EAW_Release_Date_Guideline__c( Product_Type__c = 'Compilation Episode', Territory__c = 'United States +', Language__c = 'English', Customers__c = customerInstance[0].Name, EAW_Release_Date_Type__c = 'Television');
        insert releaseDateGuideLine;
     List<EAW_Release_Date_Guideline__c> releaseDateList = [SELECT Id FROM EAW_Release_Date_Guideline__c];
     
     System.assertEquals(True, releaseDateList != null);
      
    }
    
     @isTest static void validateRDGAndReleaseDates_Test() {
     
        List<EAW_Customer__c> customerInstance = EAW_TestDataFactory.createEAWCustomer(1, true);
        //LRCC-1560 - Replace Customer lookup field with Customer text field
        //EAW_Release_Date_Guideline__c releaseDateGuideLine = new EAW_Release_Date_Guideline__c( Product_Type__c = 'Episode', Territory__c = 'United States +', Language__c = 'English', Customer__c = customerInstance[0].Id,
        //Active__c = True);
        EAW_Release_Date_Guideline__c releaseDateGuideLine = new EAW_Release_Date_Guideline__c( Product_Type__c = 'Episode', Territory__c = 'United States +', Language__c = 'English', Customers__c = customerInstance[0].Id,
        Active__c = True, EAW_Release_Date_Type__c = 'Television');        
        insert releaseDateGuideLine;         
        
        EAW_Rule__c ruleInstance = new EAW_Rule__c(Release_Date_Guideline__c = releaseDateGuideLine.Id, Conditional_Operand_Id__c = releaseDateGuideLine.Id);
        insert ruleInstance;
        
        EAW_Rule_Detail__c ruleDetailInstance = new EAW_Rule_Detail__c(Conditional_Operand_Id__c = releaseDateGuideLine.Id, Rule_No__c = ruleInstance.Id);
        insert ruleDetailInstance;
         
        releaseDateGuideLine.Active__c = False;
        update releaseDateGuideLine;
        
     }
     @isTest static void preventDeletionofRDG_Test() {
         
         List<EAW_Customer__c> customerInstance = EAW_TestDataFactory.createEAWCustomer(1, true);
        //LRCC-1560 - Replace Customer lookup field with Customer text field
        //EAW_Release_Date_Guideline__c releaseDateGuideLine = new EAW_Release_Date_Guideline__c( Product_Type__c = 'Episode', Territory__c = 'United States +', Language__c = 'Dutch', Customer__c = customerInstance[0].Id,
        //Active__c = True);
        EAW_Release_Date_Guideline__c releaseDateGuideLine = new EAW_Release_Date_Guideline__c( Product_Type__c = 'Episode', Territory__c = 'United States +', Language__c = 'Dutch', Customers__c = customerInstance[0].Id,
        Active__c = True,EAW_Release_Date_Type__c = 'Television');       
        
        insert releaseDateGuideLine; 
        
         EAW_Release_Date__c releaseDate = new EAW_Release_Date__c(Release_Date_Guideline__c = releaseDateGuideLine.Id);
         insert releaseDate;
        
        List<EAW_Release_Date_Guideline__c > releaseDateObtained = [SELECT Id, Name FROM EAW_Release_Date_Guideline__c LIMIT 1];
        
        try {
            delete releaseDateObtained;
        } catch(Exception ex) {
        }         
     }
     
    @isTest static void preventDuplicate_Test() {
        
        List<EAW_Customer__c> customerInstance = EAW_TestDataFactory.createEAWCustomer(1, true);
        //LRCC-1560 - Replace Customer lookup field with Customer text field
        //EAW_Release_Date_Guideline__c releaseDateGuideLine = new EAW_Release_Date_Guideline__c( Product_Type__c = 'Episode', Territory__c = 'Belgium', Language__c = 'English', Customer__c = customerInstance[0].Id,
        //Active__c = True, EAW_Release_Date_Type__c = 'Television');
        EAW_Release_Date_Guideline__c releaseDateGuideLine = new EAW_Release_Date_Guideline__c( Product_Type__c = 'Episode', Territory__c = 'Belgium', Language__c = 'English', Customers__c = customerInstance[0].Id,
        Active__c = True, EAW_Release_Date_Type__c = 'Television'); 
              
        insert releaseDateGuideLine;
        
    }
    
    @isTest static void reCalculateReleaseDates_Test() {
        
        List<EAW_Customer__c> customerInstance = EAW_TestDataFactory.createEAWCustomer(1, true);
        //LRCC-1560 - Replace Customer lookup field with Customer text field
        //EAW_Release_Date_Guideline__c releaseDateGuideLine = new EAW_Release_Date_Guideline__c( Product_Type__c = 'Episode', Territory__c = 'Belgium', Language__c = 'English', Customer__c = customerInstance[0].Id,
        //EAW_Release_Date_Type__c = 'Television', Active__c = False); 
        EAW_Release_Date_Guideline__c releaseDateGuideLine = new EAW_Release_Date_Guideline__c( Product_Type__c = 'Episode', Territory__c = 'Belgium', Language__c = 'English', Customers__c = customerInstance[0].Id,
        EAW_Release_Date_Type__c = 'Television', Active__c = False); 
              
        insert releaseDateGuideLine;
        
        releaseDateGuideLine.Active__c = true;
        update releaseDateGuideLine;
        
    }
    @isTest static void productTypeChange_Test(){
        
        List<EAW_Customer__c> customerList = EAW_TestDataFactory.createEAWCustomer(1, true);
        
        EAW_Release_Date_Guideline__c releaseDateGuideLine = new EAW_Release_Date_Guideline__c( Product_Type__c = 'Feature', Territory__c = 'Albania', Language__c = 'Albanian', Customers__c = customerList[0].Name, EAW_Release_Date_Type__c = 'EST-HD');
        insert releaseDateGuideLine; 
        
        EAW_Release_Date_Guideline__c newreleaseDateGuideLine = new EAW_Release_Date_Guideline__c( Product_Type__c = 'Episode', Territory__c = 'Albania', Language__c = 'Albanian', Customers__c = customerList[0].Name, EAW_Release_Date_Type__c = 'EST-HD');
        insert newreleaseDateGuideLine; 
        
        EAW_Release_Date_Guideline__c ruleRDG = new EAW_Release_Date_Guideline__c( Product_Type__c = 'Feature', Territory__c = 'Albania', Language__c = 'Albanian', Customers__c = customerList[0].Name, EAW_Release_Date_Type__c = 'EST-SD',All_Rules__c = ' 1 month 2 days from EST-HD / Albania / Albanian / Feature');
        insert ruleRDG; 
		
		String newruleContent = '{"sObjectType":"EAW_Rule__c","Nested__c":true}';
        String newruleDetailContent = '[{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Condition_Timeframe__c":"True_Earliest"},"dateCalcs":[{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Conditional_Operand_Id__c":"'+releaseDateGuideLine.Id+'","Condition_Met_Months__c":"2","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Albania / Albanian / Feature"},{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Conditional_Operand_Id__c":"'+releaseDateGuideLine.Id+'","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Albania / Albanian / Feature"}],"nodeCalcs":[{"ruleDetail":{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":2,"Condition_Timeframe__c":"True_Earliest"},"dateCalcs":[{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":2,"Conditional_Operand_Id__c":"aA73D000000CyJjSAK","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"}],"nodeCalcs":[]}]}]';
        EAW_TestRuleDataFactory.saveRuleAndRuleDetails(newruleContent,newruleDetailContent,true,ruleRDG.Id,null,null);
        
        EAW_Window_Guideline__c newWg = new EAW_Window_Guideline__c ( Status__c = 'Draft', Product_Type__c = 'Feature', Window_Type__c = 'First-Run', Window_Guideline_Alias__c = 'Test');
        insert newWg; 
        EAW_Window_Guideline_Strand__c windowGuideLineStrand =  new EAW_Window_Guideline_Strand__c(Language__c = 'Tamil',Media__c='Basic TV	',Territory__c='Afghanistan', EAW_Window_Guideline__c = newWg.Id, License_Type__c = 'Exhibition License');
		insert windowGuideLineStrand;
        //newWg.Status__c = 'Active';
     //   update newWg;
        
        String ruleContent = '{"sObjectType":"EAW_Rule__c","Nested__c":true}';
        String ruleDetailContent = '[{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Condition_Timeframe__c":"True_Earliest"},"dateCalcs":[{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Conditional_Operand_Id__c":"'+releaseDateGuideLine.Id+'","Condition_Met_Months__c":"2","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Albania / Albanian / Feature"},{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Conditional_Operand_Id__c":"'+releaseDateGuideLine.Id+'","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Albania / Albanian / Feature"}],"nodeCalcs":[{"ruleDetail":{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":2,"Condition_Timeframe__c":"True_Earliest"},"dateCalcs":[{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":2,"Conditional_Operand_Id__c":"aA73D000000CyJjSAK","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"}],"nodeCalcs":[]}]}]';
        EAW_TestRuleDataFactory.saveRuleAndRuleDetails(ruleContent,ruleDetailContent,false,newWg.Id,'Start Date',null);
		
        ruleRDG.Product_Type__c = 'Episode';
        update ruleRDG;
        
        newWg.Product_Type__c = 'Episode';
        update newWg;
                
    }
    @isTest static void nestedGuidelineProductTypeChange_Test(){
        List<EAW_Customer__c> customerList = EAW_TestDataFactory.createEAWCustomer(1, true);
        
        EAW_Release_Date_Guideline__c releaseDateGuideLine = new EAW_Release_Date_Guideline__c( Product_Type__c = 'Feature', Territory__c = 'Albania', Language__c = 'Albanian', Customers__c = customerList[0].Name, EAW_Release_Date_Type__c = 'EST-HD');
        insert releaseDateGuideLine; 
        
        EAW_Release_Date_Guideline__c newreleaseDateGuideLine = new EAW_Release_Date_Guideline__c( Product_Type__c = 'Episode', Territory__c = 'Albania', Language__c = 'Albanian', Customers__c = customerList[0].Name, EAW_Release_Date_Type__c = 'EST-HD');
        insert newreleaseDateGuideLine; 
        
        EAW_Release_Date_Guideline__c ruleRDG = new EAW_Release_Date_Guideline__c( Product_Type__c = 'Feature', Territory__c = 'Albania', Language__c = 'Albanian', Customers__c = customerList[0].Name, EAW_Release_Date_Type__c = 'EST-SD',All_Rules__c = ' 1 month 2 days from EST-HD / Albania / Albanian / Feature');
        insert ruleRDG; 
		
		String newruleContent = '{"sObjectType":"EAW_Rule__c","Operator__c":"IN","Condition_Field__c":"Release Date Status","Criteria__c":"Estimated","Condition_Operand_Details__c":"EST-HD / Albania / Albanian / Feature","Conditional_Operand_Id__c":"'+releaseDateGuideLine.Id+'","Multi_conditional_operands__c":false,"Nested__c":true,"Condition_Type__c":"If"}';
        String newruleDetailContent = '[{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Condition_Timeframe__c":"True_Earliest"},"dateCalcs":[{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Conditional_Operand_Id__c":"'+releaseDateGuideLine.Id+'","Condition_Met_Months__c":"2","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Albania / Albanian / Feature"},{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Conditional_Operand_Id__c":"'+releaseDateGuideLine.Id+'","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Albania / Albanian / Feature"}],"nodeCalcs":[{"ruleDetail":{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":2,"Condition_Timeframe__c":"True_Earliest"},"dateCalcs":[{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":2,"Conditional_Operand_Id__c":"aA73D000000CyJjSAK","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"}],"nodeCalcs":[]}]}]';
        EAW_TestRuleDataFactory.saveRuleAndRuleDetails(newruleContent,newruleDetailContent,true,ruleRDG.Id,null,null);
        
        EAW_Window_Guideline__c newWg = new EAW_Window_Guideline__c ( Status__c = 'Draft', Product_Type__c = 'Feature', Window_Type__c = 'First-Run', Window_Guideline_Alias__c = 'Test');
        insert newWg; 
        EAW_Window_Guideline_Strand__c windowGuideLineStrand =  new EAW_Window_Guideline_Strand__c(Language__c = 'Tamil',Media__c='Basic TV	',Territory__c='Afghanistan', EAW_Window_Guideline__c = newWg.Id, License_Type__c = 'Exhibition License');
		insert windowGuideLineStrand;
        
        String ruleContent = '{"sObjectType":"EAW_Rule__c","Operator__c":"IN","Condition_Field__c":"Release Date Status","Criteria__c":"Estimated","Condition_Operand_Details__c":"EST-HD / Albania / Albanian / Feature","Conditional_Operand_Id__c":"'+releaseDateGuideLine.Id+'","Multi_conditional_operands__c":false,"Nested__c":true,"Condition_Type__c":"If"}';
        String ruleDetailContent = '[{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Condition_Timeframe__c":"True_Earliest"},"dateCalcs":[{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Conditional_Operand_Id__c":"'+releaseDateGuideLine.Id+'","Condition_Met_Months__c":"2","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Albania / Albanian / Feature"},{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Conditional_Operand_Id__c":"'+releaseDateGuideLine.Id+'","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Albania / Albanian / Feature"}],"nodeCalcs":[{"ruleDetail":{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":2,"Condition_Timeframe__c":"True_Earliest"},"dateCalcs":[{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":2,"Conditional_Operand_Id__c":"aA73D000000CyJjSAK","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"}],"nodeCalcs":[]}]}]';
        EAW_TestRuleDataFactory.saveRuleAndRuleDetails(ruleContent,ruleDetailContent,false,newWg.Id,'Start Date',null);
        
        String TrackingruleContent = '{"sObjectType":"EAW_Rule__c","Operator__c":"IN","Condition_Field__c":"Release Date Status","Criteria__c":"Estimated","Condition_Operand_Details__c":"EST-HD / Albania / Albanian / Feature","Conditional_Operand_Id__c":"'+releaseDateGuideLine.Id+'","Multi_conditional_operands__c":false,"Nested__c":true,"Condition_Type__c":"If"}';
        String TrackingruleDetailContent = '[{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Condition_Timeframe__c":"True_Earliest"},"dateCalcs":[{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Conditional_Operand_Id__c":"'+releaseDateGuideLine.Id+'","Condition_Met_Months__c":"2","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Albania / Albanian / Feature"},{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Conditional_Operand_Id__c":"'+releaseDateGuideLine.Id+'","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Albania / Albanian / Feature"}],"nodeCalcs":[{"ruleDetail":{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":2,"Condition_Timeframe__c":"True_Earliest"},"dateCalcs":[{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":2,"Conditional_Operand_Id__c":"aA73D000000CyJjSAK","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"}],"nodeCalcs":[]}]}]';
        EAW_TestRuleDataFactory.saveRuleAndRuleDetails(TrackingruleContent,TrackingruleDetailContent,false,newWg.Id,'Tracking Date',null);
        
        String OutsideruleContent = '{"sObjectType":"EAW_Rule__c","Operator__c":"IN","Condition_Field__c":"Release Date Status","Criteria__c":"Estimated","Condition_Operand_Details__c":"EST-HD / Albania / Albanian / Feature","Conditional_Operand_Id__c":"'+releaseDateGuideLine.Id+'","Multi_conditional_operands__c":false,"Nested__c":true,"Condition_Type__c":"If"}';
        String OutsideruleDetailContent = '[{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Condition_Timeframe__c":"True_Earliest"},"dateCalcs":[{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Conditional_Operand_Id__c":"'+releaseDateGuideLine.Id+'","Condition_Met_Months__c":"2","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Albania / Albanian / Feature"},{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Conditional_Operand_Id__c":"'+releaseDateGuideLine.Id+'","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Albania / Albanian / Feature"}],"nodeCalcs":[{"ruleDetail":{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":2,"Condition_Timeframe__c":"True_Earliest"},"dateCalcs":[{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":2,"Conditional_Operand_Id__c":"aA73D000000CyJjSAK","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"}],"nodeCalcs":[]}]}]';
        EAW_TestRuleDataFactory.saveRuleAndRuleDetails(OutsideruleContent,OutsideruleDetailContent,false,newWg.Id,'Outside Date',null);
        
        String EndruleContent = '{"sObjectType":"EAW_Rule__c","Operator__c":"IN","Condition_Field__c":"Release Date Status","Criteria__c":"Estimated","Condition_Operand_Details__c":"EST-HD / Albania / Albanian / Feature","Conditional_Operand_Id__c":"'+releaseDateGuideLine.Id+'","Multi_conditional_operands__c":false,"Nested__c":true,"Condition_Type__c":"If"}';
        String EndruleDetailContent = '[{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Condition_Timeframe__c":"True_Earliest"},"dateCalcs":[{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Conditional_Operand_Id__c":"'+releaseDateGuideLine.Id+'","Condition_Met_Months__c":"2","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Albania / Albanian / Feature"},{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Conditional_Operand_Id__c":"'+releaseDateGuideLine.Id+'","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Albania / Albanian / Feature"}],"nodeCalcs":[{"ruleDetail":{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":2,"Condition_Timeframe__c":"True_Earliest"},"dateCalcs":[{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":2,"Conditional_Operand_Id__c":"aA73D000000CyJjSAK","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"}],"nodeCalcs":[]}]}]';
        EAW_TestRuleDataFactory.saveRuleAndRuleDetails(EndruleContent,EndruleDetailContent,false,newWg.Id,'End Date',null);
        
        newWg.Start_Date_Rule__c  = '1 month 2 days from EST-HD / Albania / Albanian / Feature';
        newWg.End_Date_Rule__c  = '1 month 2 days from EST-HD / Albania / Albanian / Feature';
        newWg.Tracking_Date_Rule__c  = '1 month 2 days from EST-HD / Albania / Albanian / Feature';
        newWg.Outside_Date_Rule__c  = '1 month 2 days from EST-HD / Albania / Albanian / Feature';
        update newWg;
        
        ruleRDG.Product_Type__c = 'Episode';
        update ruleRDG;
        
        newWg.Product_Type__c = 'Episode';
        update newWg;
                
        ruleRDG.Product_Type__c = 'Compilation Episode';
        update ruleRDG;
        
        newWg.Product_Type__c = 'Compilation Episode';
        update newWg;
    }
}