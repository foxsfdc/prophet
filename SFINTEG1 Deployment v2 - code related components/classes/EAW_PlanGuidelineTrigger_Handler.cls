public with sharing class EAW_PlanGuidelineTrigger_Handler {
    
    //LRCC-1338: 
    public void preventChangesWhenActiveStatus(List<EAW_Plan_Guideline__c> newPlanGLs, List<EAW_Plan_Guideline__c> oldPlanGLs, Map<Id, EAW_Plan_Guideline__c> oldPlanGLMap) {
        
        Map<String, Schema.SObjectField> fieldMap = Schema.SObjectType.EAW_Plan_Guideline__c.Fields.getMap();
        
        for(Integer i = 0; i < newPlanGLs.size(); i++) {
            
            EAW_Plan_Guideline__c pg = newPlanGLs.get(i);
            EAW_Plan_Guideline__c oldPG = oldPlanGLs.get(i);
            
            if(String.isNotBlank(pg.Status__c) && pg.Status__c.equals('Active') && (pg.Status__c == oldPG.Status__c)) {
            
                Set<String> updatedFields = new Set<String> ();
                for(String field : fieldMap.keySet()) { 
                    if(pg.get(field) != oldPlanGLMap.get(pg.Id).get(field) ) {
                        updatedFields.add(field);
                    }
                }
                //LRCC-739 Sales Region, Territory and Language fields are added
                  //LRCC-1745 isPlanQualifyBatchProcessing__c field is added
                updatedFields.removeAll(new Set<String>{'status__c', 'activateddate__c', 'activatedby__c', 'bypassvalidation__c',
                                                        'version__c', 'next_version__c', 'previous_version__c', 'sales_region__c', 
                                                        'territory__c', 'language__c', 'isplanqualifybatchprocessing__c'});
                                        
                if(updatedFields != null && updatedFields.size()>0){              
                    pg.addError('You can\'t make any changes for the Active guideline');               
                }
            }
        }
    }
    
    //LRCC-1745
    public void validateStatusUpdateToInactive(Map<Id, EAW_Plan_Guideline__c> oldPGMap, List<EAW_Plan_Guideline__c> newPGs) {
    
        Set<Id> planGuidelineIds = new Set<Id>();
    
        for(EAW_Plan_Guideline__c newPG : newPGs) {
            
            EAW_Plan_Guideline__c oldPg = oldPGMap.get(newPG.Id); 
            
            if(oldPg != null && oldPg.Status__c == 'Draft' && newPG.Status__c == 'Active' && newPG.Next_Version__c == null) {
                newPG.isPlanQualifyBatchProcessing__c = true;
            }
            
            if(oldPg != null && oldPg.Status__c == 'Active' && newPG.Status__c == 'Inactive' && newPG.Next_Version__c == null) {
                
                if(newPG.isPlanQualifyBatchProcessing__c == true) {
                    newPG.addError('Plan Qualification work is under Processing');
                } else {
                    planGuidelineIds.add(newPG.Id);
                }
            }
        }
        
        if(planGuidelineIds != null && planGuidelineIds.size() > 0) {
            
            if([SELECT Id FROM EAW_Plan__c WHERE Plan_Guideline__c IN : planGuidelineIds].size() > 0) {
                newPGs[0].addError('This Plan Guideline cannot be Inactive since it has associated plans');
            }
        }
    }
    
    //LRCC-716 added validation when PG is tried to delete 
    public void preventDeletionofPG(List<EAW_Plan_Guideline__c> oldPGs) {
        
        Map<Id, List<EAW_Plan__c>> planGuidelineAndPlanMap = new Map<Id, List<EAW_Plan__c>> ();
        
        if(oldPGs.isEmpty() == False) {
            
            for(EAW_Plan__c planIns : [SELECT Id, Name, Plan_Guideline__c FROM EAW_Plan__c WHERE Plan_Guideline__c IN: oldPGs]) {
                            
                if(planGuidelineAndPlanMap.containsKey(planIns.Plan_Guideline__c) == True) {                    
                    planGuidelineAndPlanMap.get(planIns.Plan_Guideline__c).add(planIns);                    
                }                 
                planGuidelineAndPlanMap.put(planIns.Plan_Guideline__c, new List<EAW_Plan__c> {planIns});
            }
            
            for(EAW_Plan_Guideline__c PGins : oldPGs) {     
            
                if(PGins.Status__c == 'Active' && planGuidelineAndPlanMap.isEmpty() == True) {
                    PGins.addError('You cannot delete an Active Plan Guideline');                    
                } 
                
                else if(planGuidelineAndPlanMap.isEmpty() == False ) {           
                    if(planGuidelineAndPlanMap.containsKey(PGins.Id) && planGuidelineAndPlanMap.get(PGins.Id) != null) {                        
                        PGins.addError('This Plan Guideline cannot be deleted since it has associated plans');
                    }
                }
                
                else if(PGins.Status__c == 'Inactive' && PGins.Next_Version__c != null){
                    PGins.addError('You cannot delete an Inactive Plan Guideline with newer versions');
                }
            }
        }
    }

    public void ensureValidationIsNotBypassed(List<EAW_Plan_Guideline__c> triggerOld, List<EAW_Plan_Guideline__c> triggerNew) {
        
        List<EAW_Plan_Guideline__c> oldPlanGuidelineList = new List<EAW_Plan_Guideline__c>();
        Set<Id> newActivePlanIdSet = new Set<Id>();
        Set<Id> prevInActivePlanIdSet = new Set<Id>();
        
        for(Integer i = 0; i < triggerOld.size(); i++) {
            
            EAW_Plan_Guideline__c oldPlan = triggerOld.get(i);
            EAW_Plan_Guideline__c newPlan = triggerNew.get(i);

            //Default, don't let users bypass validation
            newPlan.BypassValidation__c = false;
            
            //Prevent other versions from being removed
            if(oldPlan.Previous_Version__c != null && newPlan.Previous_Version__c == null) {
                //prevent next version from being removed
                newPlan.addError('You cannot edit the version lineage');
            } 
            else if(oldPlan.Next_Version__c != null && newPlan.Next_Version__c == null) {
                //prevent the prev version from being removed
                newPlan.addError('You cannot edit the version lineage');
            }
            
            if(oldPlan.Status__c != 'active' && newPlan.Status__c == 'active') {
                        
                //set the activated by user deets
                newPlan.ActivatedBy__c = UserInfo.getUserId();
                newPlan.ActivatedDate__c = System.today();
                
                newActivePlanIdSet.add(newPlan.Id);
                    
                if(oldPlan.Previous_Version__c != null) {
                    
                    newPlan.BypassValidation__c = true;
                    //set the previous version to inactive
                    EAW_Plan_Guideline__c prevPG = new EAW_Plan_Guideline__c();
                    prevPG.Id = newPlan.Previous_Version__c;
                    prevPG.Status__c = 'inactive';
                    oldPlanGuidelineList.add(prevPG);
                    
                    prevInActivePlanIdSet.add(newPlan.Previous_Version__c);
                }
            }

            if(oldPlan.Status__c == 'inactive') {

                //Inactive Versions can ONLY go from inactive > Active
                if(newPlan.Status__c == 'draft') {
                    //prevent inactive versions from being set to draft
                    newPlan.addError('You cannot change a version from inactive to draft');
                } 
                else if(newPlan.Status__c == 'active' && newPlan.Next_Version__c != null) {
                    //Only allow the latest version to go from inactive to active
                    newPlan.addError('You can only reactivate an inactive Plan if it is the latest version');
                }
            } 
            else if(oldPlan.Status__c == 'active' && newPlan.Status__c == 'draft') {
                newPlan.addError('You cannot draft an active plan, please make changes in a new version');
            }
        }
        
        //Check that all associated Window Guidelines are Active to make the Plan Guidelines Active
        if(newActivePlanIdSet != NULL && newActivePlanIdSet.size() > 0) {
        
            String wgQuery = 'SELECT Id, Window_Guideline__r.Status__c, Plan__c FROM EAW_Plan_Window_Guideline_Junction__c WHERE '
                           + 'Plan__c IN : newActivePlanIdSet';
            
            Set<Id> planIdSetToThrowError = new Set<Id>();
            
            for(EAW_Plan_Window_Guideline_Junction__c windowGuideline : Database.query(wgQuery) ) {
                if(windowGuideline.Window_Guideline__r.Status__c != 'active') {
                    planIdSetToThrowError.add(windowGuideline.Plan__c);
                }
            }
            
            for(Integer i = 0; i < triggerNew.size(); i++) {
            
                EAW_Plan_Guideline__c oldPlan = triggerOld.get(i);
                EAW_Plan_Guideline__c newPlan = triggerNew.get(i);
                
                if( newPlan.Status__c != oldPlan.Status__c && newPlan.Status__c == 'Active' && planIdSetToThrowError.contains(newPlan.Id) ){
                    newPlan.addError('All attached window guidelines must be active to activate a plan guideline');
                }
            }
        }
        
        if(oldPlanGuidelineList.size() > 0) {
            
            update oldPlanGuidelineList;
            
            //LRCC-510
            List<EAW_Plan_Window_Guideline_Junction__c> juncList = [ 
                SELECT Id, Window_Guideline__c, Plan__c, Plan_Guideline_Status__c  
                FROM EAW_Plan_Window_Guideline_Junction__c 
                WHERE Plan__c IN : oldPlanGuidelineList 
            ];
            
            if(juncList != NULL && juncList.size() > 0) {
                delete juncList;
            }
        }
    }
    
    public void updateAssociatedPlans(List<EAW_Plan_Guideline__c> triggerOld, List<EAW_Plan_Guideline__c> triggerNew) {
    
        Map<Id, EAW_Plan_Guideline__c> planGuidelineMap = new Map<Id, EAW_Plan_Guideline__c>();
        List<EAW_Plan__c> planListToUpdate = new  List<EAW_Plan__c>();
        
        for(Integer i = 0; i < triggerOld.size(); i++) {
            
            EAW_Plan_Guideline__c oldPlan = triggerOld.get(i);
            EAW_Plan_Guideline__c newPlan = triggerNew.get(i);
            
            //LRCC-739
            if(oldPlan.Status__c =='Active' && newPlan.Status__c =='Active' && ( newPlan.Sales_Region__c != oldPlan.Sales_Region__c || newPlan.Territory__c != oldPlan.Territory__c  || newPlan.Language__c != oldPlan.Language__c || newPlan.Status__c != oldPlan.Status__c) ){
                planGuidelineMap.put(newPlan.Id, newPlan);
            }
        }
        
        if( planGuidelineMap != NULL && planGuidelineMap.size() > 0 ){
            
            for( EAW_Plan__c plan : [ Select Id, Name, Sales_Region__c, Territory__c, Language__c, Plan_Guideline__c From EAW_Plan__c Where Plan_Guideline__c IN : planGuidelineMap.keySet() ]){
                
                if( planGuidelineMap.containsKey(plan.Plan_Guideline__c) ){
                    EAW_Plan_Guideline__c planGuideline = planGuidelineMap.get(plan.Plan_Guideline__c);
                    plan.Sales_Region__c = planGuideline.Sales_Region__c;
                    plan.Territory__c = planGuideline.Territory__c;
                    plan.Language__c = planGuideline.Language__c;
                    planListToUpdate.add(plan);
                }
            }
        }
        
        if( planListToUpdate != NULL && planListToUpdate.size() > 0 ) update planListToUpdate;
        
    }
    
    public void createEAWWindows(List<EAW_Plan_Guideline__c> triggerOld, List<EAW_Plan_Guideline__c> triggerNew) {
        
        Set<Id> planGuidelineIdSet = new Set<Id>();
        Set<Id> windowGuidelineIdSet = new Set<Id>();
        Map<Id, Set<Id>> planWindowGuidelineIdMap = new Map<Id, Set<Id>>();
        
        for( Integer i=0; i < triggerNew.size(); i++ ){
            
            EAW_Plan_Guideline__c newPlanGuideline = triggerNew.get(i);
            EAW_Plan_Guideline__c oldPlanGuideline = triggerOld.get(i);
            
            if( newPlanGuideline.Status__c != oldPlanGuideline.Status__c && newPlanGuideline.Status__c == 'Active' ){
                planGuidelineIdSet.add(newPlanGuideline.Id);
            }
        }
        
        if( planGuidelineIdSet.size() > 0 ) {
                
            List<EAW_Plan_Window_Guideline_Junction__c> planWindowGuidelineList = [ Select Id, Plan__c, Window_Guideline__c From EAW_Plan_Window_Guideline_Junction__c Where  Plan__c IN : planGuidelineIdSet];
            
            for( EAW_Plan_Window_Guideline_Junction__c planWindow : planWindowGuidelineList ){
                
                windowGuidelineIdSet.add(planWindow.Window_Guideline__c);
                
                if( planWindowGuidelineIdMap.containsKey(planWindow.Plan__c)) {
                    Set<Id> tempSet = planWindowGuidelineIdMap.get(planWindow.Plan__c);
                    tempSet.add(planWindow.Window_Guideline__c);
                    planWindowGuidelineIdMap.put(planWindow.Plan__c, tempSet);
                } else {
                    planWindowGuidelineIdMap.put(planWindow.Plan__c, new Set<Id>{planWindow.Window_Guideline__c});
                }
            }
            
            if( planWindowGuidelineIdMap.size() > 0 ){
                
                List<EAW_Plan__c> plansList = [ Select Id, Name, Plan_Guideline__c From EAW_Plan__c Where Plan_Guideline__c IN : planWindowGuidelineIdMap.keySet() ];
                
                if( plansList != NULL && plansList.size() > 0 ) {
                    
                    Map<Id, List<EAW_Plan__c>> planMap = new  Map<Id, List<EAW_Plan__c>>();
                    
                    for( EAW_Plan__c plan : plansList ){
                        
                        if( planMap.containsKey(plan.Plan_Guideline__c) ){
                            List<EAW_Plan__c> tempPlanList = planMap.get(plan.Plan_Guideline__c);
                            tempPlanList.add(plan);
                            planMap.put( plan.Plan_Guideline__c,  tempPlanList); 
                        } else {
                            planMap.put( plan.Plan_Guideline__c,  new List<EAW_Plan__c>{ plan } );
                        }
                    }
                    
                    //LRCC-1560 - Replace Customer lookup field with Customer text field
                    //Map<Id, EAW_Window_Guideline__c> windowGuidelineMap = new Map<Id, EAW_Window_Guideline__c>([Select Id, Name, CurrencyIsoCode, Customer__c, End_Date__c, Media__c, Outside_Date__c, Start_Date__c, Tracking_Date__c, Window_Type__c From EAW_Window_Guideline__c Where Id IN : windowGuidelineIdSet ]);
                    Map<Id, EAW_Window_Guideline__c> windowGuidelineMap = new Map<Id, EAW_Window_Guideline__c>([Select Id, Name, CurrencyIsoCode, Customers__c, End_Date__c, Media__c, Outside_Date__c, Start_Date__c, Tracking_Date__c, Window_Type__c From EAW_Window_Guideline__c Where Id IN : windowGuidelineIdSet ]);
                    List<EAW_Window__c> eawWindowListToInsert = new List<EAW_Window__c>();
                    
                    for( Id planGuidelineId : planWindowGuidelineIdMap.keySet() ){
                        
                        for( Id windowGuidelineId : planWindowGuidelineIdMap.get(planGuidelineId) ){
                            
                            EAW_Window_Guideline__c windowGuidelineRecord = windowGuidelineMap.get(windowGuidelineId);
                            
                            for( EAW_Plan__c planRecord : planMap.get(planGuidelineId) ){
                                
                                EAW_Window__c newWindow = new EAW_Window__c();
                                
                                newWindow.EAW_Plan__c = planRecord.Id;
                                newWindow.EAW_Window_Guideline__c = windowGuidelineRecord.Id;
                                
                                newWindow.Name = planRecord.Name + '-' + windowGuidelineRecord.Name;
                                newWindow.CurrencyIsoCode = windowGuidelineRecord.CurrencyIsoCode;
                                //LRCC-1560 - Replace Customer lookup field with Customer text field
                                //newWindow.Customer__c = windowGuidelineRecord.Customer__c;
                                newWindow.Customers__c = windowGuidelineRecord.Customers__c;
                                newWindow.End_Date__c = windowGuidelineRecord.End_Date__c;
                                newWindow.Media__c = windowGuidelineRecord.Media__c;
                                newWindow.Outside_Date__c = windowGuidelineRecord.Outside_Date__c;
                                newWindow.Start_Date__c = windowGuidelineRecord.Start_Date__c;
                                newWindow.Tracking_Date__c = windowGuidelineRecord.Tracking_Date__c;
                                newWindow.Window_Type__c = windowGuidelineRecord.Window_Type__c;
                                
                                eawWindowListToInsert.add(newWindow);
                            }
                            
                        }
                        
                    }
                                
                    if( eawWindowListToInsert != NULL && eawWindowListToInsert.size() > 0 ){
                        insert eawWindowListToInsert;
                    }
                }
            }
        }
    }
    
    public void validatePlanGuidelineStatus(List<EAW_Plan_Guideline__c> triggerNew ){
        
        if( triggerNew != NULL && triggerNew.size() > 0 ){
            
            for( EAW_Plan_Guideline__c pg : triggerNew ){
                
                if( pg.Status__c != 'Draft' ){
                    pg.Status__c.addError(Label.Planguideline_Status_Check);
                }
            }
        }
    }
    
   public void validateStatusUpdateToActive( List<EAW_Plan_Guideline__c> triggerNew, List<EAW_Plan_Guideline__c> triggerOld ) {
        
        Set<Id> activePlanGuidelineIdSet = new Set<Id>();
        Map<Id,Set<String>> productTypeMap = new Map<Id,Set<String>>();
        Map<Id,Set<String>> junctionMap = new Map<Id,Set<String>>();
        if( triggerNew != NULL && triggerNew.size() > 0 ){
            
            for( Integer i = 0; i < triggerNew.size(); i++ ){
                
                EAW_Plan_Guideline__c newPG = triggerNew[i];
                EAW_Plan_Guideline__c oldPG = triggerOld[i];
                
                if( newPG.Status__c != oldPG.Status__c && newPG.Status__c == 'Active' ){
                    activePlanGuidelineIdSet.add(newPG.Id);
                    if(newPG.Product_Type__c != null){
                        productTypeMap.put(newPG.Id,new Set<String>((newPG.Product_Type__c.toUpperCase()).split(';'))); // LRCC - 1373
                    }
                    
                }
            }
            
            if( activePlanGuidelineIdSet.size() > 0 ){
                for( EAW_Plan_Window_Guideline_Junction__c pwgj : [ Select Id, Window_Guideline__c,Window_Guideline__r.Product_Type__c, Plan__c From EAW_Plan_Window_Guideline_Junction__c Where Plan__c IN : activePlanGuidelineIdSet] ){
                    if(pwgj.Window_Guideline__r.Product_Type__c != null){
                        Set<String> tempJunctionList = new Set<String>();
                        if(junctionMap.containsKey(pwgj.Plan__c)){
                            tempJunctionList = junctionMap.get(pwgj.Plan__c);
                        }
                        tempJunctionList.add(pwgj.Window_Guideline__r.Product_Type__c.toUpperCase());
                        junctionMap.put(pwgj.Plan__c,tempJunctionList); // LRCC - 1373
                    }
                    if( activePlanGuidelineIdSet.contains(pwgj.Plan__c) ) activePlanGuidelineIdSet.remove(pwgj.Plan__c);
                }
                
                if( activePlanGuidelineIdSet.size() > 0 ){
                    for( EAW_Plan_Guideline__c pg : triggerNew ){
                        if( pg.Status__c == 'Active' && activePlanGuidelineIdSet.contains(pg.Id)){
                            pg.Status__c.addError(Label.EAW_Planguideline_Active_Status_Check);
                        }
                    }
                }
                for( EAW_Plan_Guideline__c pg : triggerNew ){ // LRCC - 1373
                    if( pg.Status__c == 'Active'){
                        if(productTypeMap.containsKey(pg.Id) && junctionMap.containsKey(pg.Id) &&
                           productTypeMap.get(pg.Id) != null && junctionMap.get(pg.Id) != null &&
                           junctionMap.get(pg.Id).size() > 0 && !productTypeMap.get(pg.Id).containsAll(junctionMap.get(pg.Id))){
                               pg.Status__c.addError('Plan Guideline cannot be activated, please modify the Product Type');
                           }
                    }
                }
            }
        }
    }
    public void planReQualificationCheckForActivePG(List<EAW_Plan_Guideline__c> oldPlanGuidelines,List<EAW_Plan_Guideline__c> newPlanGuidelines){
        Set<Id> planGuidelineIdSet = new Set<Id>();
        for(Integer i=0;i<newPlanGuidelines.size();i++){
            if(oldPlanGuidelines[i].Status__c != NULL && newPlanGuidelines[i].Status__c != NULL && oldPlanGuidelines[i].Status__c != 'Active' && newPlanGuidelines[i].Status__c == 'Active'){
                planGuidelineIdSet.add(newPlanGuidelines[i].Id);
            }
        }
        if(planGuidelineIdSet.size() > 0){
            
            List<EAW_Rule_Detail__c> ruleDetailList = [SELECT Id,Condition_Field__c,Rule_No__r.Plan_Guideline__c FROM EAW_Rule_Detail__c WHERE Rule_No__r.Plan_Guideline__c IN :planGuidelineIdSet];
            if(ruleDetailList.size() > 0){
                Map<String,List<EAW_Rule_Detail__c>> planGuidelineAndRuleDetail = new Map<String,List<EAW_Rule_Detail__c>>();
                for(Id planGuideline : planGuidelineIdSet){
                    for(EAW_Rule_Detail__c ruleDetail : ruleDetailList){
                        if(ruleDetail.Rule_No__r.Plan_Guideline__c == planGuideline){
                            List<EAW_Rule_Detail__c> tempRuleDetailList = new List<EAW_Rule_Detail__c>();
                            if( planGuidelineAndRuleDetail.containsKey(planGuideline) ) {
                                tempRuleDetailList = planGuidelineAndRuleDetail.get(planGuideline);
                            }
                            tempRuleDetailList.add(ruleDetail);
                            planGuidelineAndRuleDetail.put(planGuideline, tempRuleDetailList);
                        }
                    }
                } 
                system.debug('planGuidelineAndRuleDetail:::::::::'+planGuidelineAndRuleDetail);
                //LRCC-1239
                Set<Id> isTVDBoxOfficeWithPGIds =  new Set<Id>();
                Set<Id> isTitleAttributeWithPGIds =  new Set<Id>();
                Set<Id> isTitleTagWithPGIds =  new Set<Id>();
                Set<Id> isReleaseDateWithPGIds =  new Set<Id>();
                Set<String> productTypeSet = new Set<String>();
              
                String planGuidelineId;
                Set<Id> qualifiedEAWTitleIdSet = new Set<Id>();
                
                for(Id planGuideline : planGuidelineIdSet){
                    if(planGuidelineAndRuleDetail.get(planGuideline).size() > 0){
                    
                        //LRCC-1239
                        //EAW_PlanReQualifierController.checkForPlanReQualifications(planGuidelineAndRuleDetail.get(planGuideline),planGuideline);
                        
                        planGuidelineId = planGuideline;
                        //Set<Id> qualifiedEAWTitleIdSet = new Set<Id>();
                      
                        
                        System.debug('::::::ruleDetailList::::'+ruleDetailList);
                        System.debug('::::::planGuidelineId::::'+planGuidelineId);
                        
                        for(EAW_Rule_Detail__c ruleDetail :  planGuidelineAndRuleDetail.get(planGuideline)) {
                        
                            if( ruleDetail.Condition_Field__c == 'US Box Office' || ruleDetail.Condition_Field__c == 'US Admissions' || ruleDetail.Condition_Field__c == 'US Number of Screens' || ruleDetail.Condition_Field__c == 'Local Box Office' || ruleDetail.Condition_Field__c == 'Local Admissions' || ruleDetail.Condition_Field__c == 'Local Number of Screens' ){
                              
                                isTVDBoxOfficeWithPGIds.add(planGuideline); 
                            } else if(ruleDetail.Condition_Field__c == 'Division' || ruleDetail.Condition_Field__c == 'Release Year'){
                            
                                isTitleAttributeWithPGIds .add(planGuideline);
                                 
                            } else if(ruleDetail.Condition_Field__c == 'Title Tag'){
                                
                                isTitleTagWithPGIds.add(planGuideline);
                                 
                            } else if(ruleDetail.Condition_Field__c == 'Local Theatrical Release'){
                                
                                isReleaseDateWithPGIds.add(planGuideline); 
                            }
                        }
                        
                        EAW_Plan_Guideline__c planGuidelineInst =[SELECT Id, Product_Type__c FROM EAW_Plan_Guideline__c WHERE Id =: planGuidelineId];
                        //Set<String> productTypeSet = new Set<String>();
                        if( planGuidelineInst .Product_Type__c != NULL ) {
                        
                            productTypeSet.addAll(planGuidelineInst.Product_Type__c.split(';')); 
                        }
                        System.debug('::::::isTVDBoxOffice::::'+isTVDBoxOfficeWithPGIds+':::::isTitleAttribute::::'+isTitleAttributeWithPGIds );
                        System.debug('::::::isTitleTag::::'+isTitleTagWithPGIds+':::::isReleaseDate::::'+isReleaseDateWithPGIds);
                    }
                }
                EAW_PlanReQualifierBatchController ebprqc= new EAW_PlanReQualifierBatchController( productTypeSet,planGuidelineId, qualifiedEAWTitleIdSet,
                                                                                                   isTVDBoxOfficeWithPGIds,isTitleAttributeWithPGIds,isTitleTagWithPGIds,isReleaseDateWithPGIds);
                Integer bcount = Integer.valueOf(Label.PlanReQualifierBacthCount);
                System.debug('::::::bcount ::::'+bcount);
                database.executeBatch(ebprqc,bcount);
            }
        }
        
    }
    public void cloneRuleDetailsToNewVersion(List<EAW_Plan_Guideline__c> newPlanGuidelines){ //LRCC - 1357
        Set < Id > previousSet = new Set<Id>();
        Map < Id, Id > newPGMap = new Map<Id,Id>();
        
        for(Integer i=0;i<newPlanGuidelines.size();i++){
            if(newPlanGuidelines[i].Status__c == 'Draft' && newPlanGuidelines[i].Previous_Version__c != null){
                previousSet.add(newPlanGuidelines[i].Previous_Version__c);
                newPGMap.put(newPlanGuidelines[i].Previous_Version__c,newPlanGuidelines[i].Id);             
            }
        }
        if(previousSet.size() > 0){
            clonePlanQualifier(previousSet,newPGMap);
        }
    }
    public void cloneRuleDetailsToNewVersion(List<EAW_Plan_Guideline__c> oldPlanGuidelines,List<EAW_Plan_Guideline__c> newPlanGuidelines){
        Set < Id > previousSet = new Set<Id>();
        Map < Id, Id > newPGMap = new Map<Id,Id>();
        
        for(Integer i=0;i<oldPlanGuidelines.size();i++){
            if(newPlanGuidelines[i].Status__c == 'Draft' && oldPlanGuidelines[i].Previous_Version__c != newPlanGuidelines[i].Previous_Version__c){
                previousSet.add(newPlanGuidelines[i].Previous_Version__c);
                newPGMap.put(newPlanGuidelines[i].Previous_Version__c,newPlanGuidelines[i].Id);             
            }
        }
        system.debug('::::::newPGMap:::;'+newPGMap);
        if(previousSet.size() > 0){
            clonePlanQualifier(previousSet,newPGMap);
        } 
    }
    public static Map < Id, List < EAW_Rule__c >> clonePlanQualifier(Set < Id > oldPlans, Map < Id, Id > newClonedPlans) {
        
        Map < Id, List < EAW_Rule__c >> clonedMap = new Map < Id, List < EAW_Rule__c >> ();
        Map < Id, EAW_Rule__c > clonedRuleReference = new Map < Id, EAW_Rule__c > ();
        Map < Id, List < EAW_Rule_Detail__c >> clonedRuleDetailMap = new Map < Id, List < EAW_Rule_Detail__c >> ();

        Schema.SObjectType targetType = Schema.getGlobalDescribe().get('EAW_Rule_Detail__c'); //From the Object Api name retrieving the SObject
        Sobject Object_name = targetType.newSObject();
        Schema.sObjectType sobject_type = Object_name.getSObjectType(); //grab the sobject that was passed
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
        Map < String, Schema.SObjectField > field_map = sobject_describe.fields.getMap();

        String ruleDetailfieldSet = 'SELECT ';
        for (Schema.SObjectField fieldName: field_map.values()) {
            String fieldAPI = String.valueOf(fieldName);
            ruleDetailfieldSet = ruleDetailfieldSet + ' ' + fieldAPI + ' ,';
        }
        ruleDetailfieldSet = ruleDetailfieldSet.removeEnd(',');
        ruleDetailfieldSet = ruleDetailfieldSet + 'FROM Rule_Orders__r';
        
        String ruleSet = 'SELECT Id, RecordTypeId, Plan_Guideline__c,' + '(' + ruleDetailfieldSet + ')' + ' FROM EAW_Rule__c WHERE Plan_Guideline__c IN :oldPlans AND Plan_Guideline__r.status__c = \'Active\'';
        List < EAW_Rule__c > ruleList = Database.query(ruleSet);
        
        List < EAW_Rule__c > clonedRuleList = new List < EAW_Rule__c > ();
        List < EAW_Rule_Detail__c > clonedRuleDetailList = new List < EAW_Rule_Detail__c > ();
        if (ruleList.size() > 0) {
            List<EAW_Rule__c> newPGRuleList = [SELECT Id,Plan_Guideline__c FROM EAW_Rule__c WHERE Plan_Guideline__c IN :newClonedPlans.values()];
            system.debug(':::::newPGRuleList:::::::'+newPGRuleList);
            for (EAW_Rule__c rule: ruleList) {
                EAW_Rule__c clonedRule = rule.clone(false, true, false, false);
                system.debug('::::::guideline:::;'+newClonedPlans.get(rule.Plan_Guideline__c));
                clonedRule.Plan_Guideline__c = newClonedPlans.get(rule.Plan_Guideline__c);
                clonedRuleList.add(clonedRule);
                
                clonedRuleDetailList = new List < EAW_Rule_Detail__c > ();
                for(EAW_Rule_Detail__c ruleDetail: rule.Rule_Orders__r) {
                    EAW_Rule_Detail__c clonedRuleDetail = ruleDetail.clone(false, true, false, false);
                    clonedRuleDetailList.add(clonedRuleDetail);
                }
                clonedRuleDetailMap.put(rule.Id, clonedRuleDetailList);
                clonedRuleReference.put(rule.Id, clonedRule);
            }    
            if(newPGRuleList != null && newPGRuleList.size() > 0){
                delete newPGRuleList;
            }       
            insert clonedRuleList;
            
            system.debug('clonedRuleList::::'+clonedRuleList);
            
            clonedRuleDetailList = new List < EAW_Rule_Detail__c > ();
            for (Id originalId: clonedRuleReference.keySet()) {
                for (EAW_Rule_Detail__c clonedRuleDetail: clonedRuleDetailMap.get(originalId)) {
                    clonedRuleDetail.Rule_No__c = clonedRuleReference.get(originalId).Id;
                    clonedRuleDetailList.add(clonedRuleDetail);
                }
            }
            insert clonedRuleDetailList;
        }
        return clonedMap;
    }
}