public class EAW_RDGDateCalculationBatchClass implements Database.Stateful,Database.Batchable<sObject>{

    public Set<Id> releaseDateGuidelineIdSet = new Set<Id>();
    public Set<Id> windowGuidelineIdSet = new Set<Id>();
    public Set<Id> titleIdSet = new Set<Id>();
    public boolean disQualifyFlag = false;
    Set<Id> releaseDateIdSet = new Set<Id>(); //For Plan Qualifier logic check
    
    Set<Id> parentRDGIdSet = new Set<Id>();
   
    //duplicate title running prevent set
    Set<Id> skipDuplicateTitleIdProcessing = new Set<Id>();
    
    public EAW_RDGDateCalculationBatchClass( Set<Id> rdgIdSet ){
        
        this.releaseDateGuidelineIdSet = rdgIdSet;
        
        System.debug('::::; releaseDateGuidelineIdSet :::::'+releaseDateGuidelineIdSet);
        
        //The following logic is defined to remove the child RDG for the date calculation if the parent RDG is also present
        EAW_DownstreamRuleGuidelinesFindUtil util = new EAW_DownstreamRuleGuidelinesFindUtil();
        Map<Id, Set<Id>> referrencedRDGs = new Map<Id, Set<Id>>();
        referrencedRDGs = util.findReferencedReleaseDateGuidelineRelatedToReleaseDateGuideline(rdgIdSet);
        for( Id rdgId : referrencedRDGs.keySet()){
            releaseDateGuidelineIdSet.removeAll(referrencedRDGs.get(rdgId));
        }
        
        if( releaseDateGuidelineIdSet != NULL && releaseDateGuidelineIdSet.size() > 0 ) {
            
            parentRDGIdSet.addAll(releaseDateGuidelineIdSet); // To handle if the release date record get deleted from parent RDG when it's child RDG is inActive and activating it again
            
            List<EAW_Rule__c> ruleRecordList = [ Select Id, Release_Date_Guideline__c, Conditional_Operand_Id__c, ( Select Id, Conditional_Operand_Id__c From Rule_Orders__r ) From EAW_Rule__c Where Release_Date_Guideline__c IN : releaseDateGuidelineIdSet ];            
            
            if( ruleRecordList != NULL && ruleRecordList.size() > 0 ) {
            
                for( EAW_Rule__c ruleRecord : ruleRecordList ){
                    
                    if( String.isNotBlank(ruleRecord.Conditional_Operand_Id__c) ) {
                        parentRDGIdSet.addAll((List<Id>)ruleRecord.Conditional_Operand_Id__c.split('\\;'));
                    }
                    
                    for( EAW_Rule_Detail__c ruleDetail : ruleRecord.Rule_Orders__r ) {
                        if( String.isNotBlank(ruleDetail.Conditional_Operand_Id__c) ) {
                            parentRDGIdSet.addAll((List<Id>)ruleDetail.Conditional_Operand_Id__c.split('\\;'));
                        }
                    }
                    /*if( parentRDGIdSet != NULL && parentRDGIdSet.size() > 0 ) {
                        rdgAndParentRDGMap.put(ruleRecord.Release_Date_Guideline__c, parentRDGIdSet);
                        parentIdSet.addAll(parentRDGIdSet);
                    }*/
                }
            
            }
        
        }
    }
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
    
        System.debug(':::parentRDGIdSet:::' + parentRDGIdSet);
        System.debug(':::releaseDateGuidelineIdSet:::' + releaseDateGuidelineIdSet);
        system.debug('::titleIdSet::'+titleIdSet);
        
        String query = '';
        String softDeleted = 'FALSE';
        
        if( titleIdSet != NULL && titleIdSet.size() > 0 ) {
            query = 'Select Id, Title__c, Status__c, Active__c, Projected_Date__c, Release_Date__c, Feed_Date__c, Manual_Date__c, Release_Date_Guideline__c,Title__r.PROD_TYP_CD_INTL__c,Title__r.PROD_TYP_CD_INTL__r.Name, Send_To_Third_Party__c, Repo_DML_Type__c, Galileo_DML_Type__c, Sent_To_Galileo__c, Sent_To_Repo__c FROM EAW_Release_Date__c WHERE Title__c IN : titleIdSet AND Release_Date_Guideline__c IN : parentRDGIdSet AND Soft_Deleted__c =: softDeleted ORDER BY Release_Date_Guideline__c, Title__c';
        } else {
            query = 'Select Id, Title__c, Status__c, Active__c, Projected_Date__c, Release_Date__c, Feed_Date__c, Manual_Date__c, Release_Date_Guideline__c,Title__r.PROD_TYP_CD_INTL__c,Title__r.PROD_TYP_CD_INTL__r.Name, Send_To_Third_Party__c, Repo_DML_Type__c, Galileo_DML_Type__c, Sent_To_Galileo__c, Sent_To_Repo__c FROM EAW_Release_Date__c Where Release_Date_Guideline__c IN : parentRDGIdSet AND Soft_Deleted__c =: softDeleted ORDER BY Release_Date_Guideline__c, Title__c';
            titleIdSet = new Set<Id>();
        }
        return Database.getQueryLocator(query);
    }

    
    public void execute(Database.BatchableContext BC, list<sObject> rdRecList){
        System.debug(':::::::: rdRecList :::::::::'+rdRecList.size() );
        EAW_NRDGRuleBuilderCalculationController rdDateCalcLogic = new EAW_NRDGRuleBuilderCalculationController();
        
        Set<Id> tempTitleIdSet = new Set<Id>();
        for( EAW_Release_Date__c rdRec : (List<EAW_Release_Date__c>) rdRecList ) {
            if( ! skipDuplicateTitleIdProcessing.contains(rdRec.Title__c) ) {
                tempTitleIdSet.add(rdRec.Title__c);
                skipDuplicateTitleIdProcessing.add(rdRec.Title__c);
            }
        }
        
        System.debug(':::::::: tempTitleIdSet Size :::::::::'+tempTitleIdSet.size() );
        System.debug(':::::::: tempTitleIdSet :::::::::'+tempTitleIdSet);
        
        if( tempTitleIdSet.size() > 0 ) {
            titleIdSet.addAll(tempTitleIdSet);
            Map<String, Set<Id>> idSetMap = rdDateCalcLogic.executeRDGRuleCalculations(releaseDateGuidelineIdSet, tempTitleIdSet);
            if( idSetMap != NULL && idSetMap.containsKey('releasedateidset') ) releaseDateIdSet.addAll(idSetMap.get('releasedateidset')); //For plan qualifier check
        }
    }
    
    
    public void finish(Database.BatchableContext BC){
        
        System.debug(':::::: releaseDateGuidelineIdSet ::::::::::::'+releaseDateGuidelineIdSet);
        System.debug(':::::: windowGuidelineIdSet ::::::::::::'+windowGuidelineIdSet);
        System.debug(':::::: releaseDateIdSet ::::::::::::'+releaseDateIdSet);
        
        if( releaseDateIdSet != NULL && titleIdSet != NULL ) {
        
            EAW_RDPlanQualifierBatchClass planQualifierBatch = new EAW_RDPlanQualifierBatchClass();
            planQualifierBatch.releaseDateIdSet  = releaseDateIdSet;
            planQualifierBatch.windowGuidelineIdSet = windowGuidelineIdSet;
            planQualifierBatch.releaseDateGuidelineIdSet = releaseDateGuidelineIdSet;
            planQualifierBatch.titleIdSet = titleIdSet;
            planQualifierBatch.disQualifyFlag = disQualifyFlag;
            ID batchprocessid = Database.executeBatch(planQualifierBatch, 200);
            
        } else if( releaseDateGuidelineIdSet != NULL ) {
        
            EAW_DownstreamRuleGuidelinesFindUtil dependentGuidelineFindUtil = new EAW_DownstreamRuleGuidelinesFindUtil();
            
            Set<Id> windowGuidelinesIdToProcess = new Set<Id>();
            
            if( windowGuidelineIdSet != NULL && windowGuidelineIdSet.size() > 0 ) windowGuidelinesIdToProcess.addAll(windowGuidelineIdSet);
            
            if( releaseDateGuidelineIdSet != NULL && releaseDateGuidelineIdSet.size() > 0 ) {
                windowGuidelinesIdToProcess.addAll(dependentGuidelineFindUtil.findDependentWindowGuidelineToProcess(releaseDateGuidelineIdSet));
            }
            
            System.debug('::::::: windowGuidelinesIdToProcess ::::::::::'+windowGuidelinesIdToProcess);
            if( windowGuidelinesIdToProcess != NULL && windowGuidelinesIdToProcess.size() > 0 ) {
                EAW_WGDateCalculationBatchClass windowDateBatch = new EAW_WGDateCalculationBatchClass(windowGuidelinesIdToProcess);
                windowDateBatch.releaseDateGuidelineIdSet = NULL;
                windowDateBatch.titleIdSet = titleIdSet;
                
                //windowDateBatch.windowGuidelineIdSet = windowGuidelinesIdToProcess;
                ID batchprocessid = Database.executeBatch(windowDateBatch, 200);
            } else {
                EAW_ReleaseDate_API_BatchProcess rdBatch = new EAW_ReleaseDate_API_BatchProcess();
                ID batchprocessid = Database.executeBatch(rdBatch, 100);
            }
        } 
    }



}