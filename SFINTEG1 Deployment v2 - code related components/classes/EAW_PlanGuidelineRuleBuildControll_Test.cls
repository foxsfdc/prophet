@isTest
public class EAW_PlanGuidelineRuleBuildControll_Test {
    
    @testSetup static void setupTestData() {
        
        EAW_TestDataFactory.createEAWRule (2, True);
        EAW_TestDataFactory.createRuleDetail (2,True);
        EAW_TestDataFactory.createPlanGuideline(1,True);
    }
    
    @isTest static void testruleDetail_Test(){
    
        List<String> rulesDetailString = new List<String>();
        List<String> rulesString = new List<String>();
        EAW_Rule_Detail__c ruleDetail = [SELECT ID, Name FROM EAW_Rule_Detail__c LIMIT 1];
        EAW_Plan_Guideline__c planGuideLine = [SELECT Id, Name FROM EAW_Plan_Guideline__c LIMIT 1];
        
        EAW_Rule__c ruleInstance = [SELECT ID, Name FROM EAW_Rule__c LIMIT 1];       
        
        EAW_Rule_Detail__c ruleDetailDelete = [SELECT ID, Name FROM EAW_Rule_Detail__c LIMIT 1];
        rulesDetailString.add(ruleDetailDelete.Id);
        
        EAW_Rule__c ruleInstanceDelete = [SELECT ID, Name FROM EAW_Rule__c LIMIT 1];
        rulesString.add(ruleInstanceDelete.Id);
        try {
       
            EAW_PlanGuidelineRuleBuilderController.saveConditionSetFields(JSON.Serialize(new List<EAW_Rule_Detail__c>{ruleDetail}), planGuideLine.Id, rulesString, rulesDetailString, 'Test All Rules');
        } catch(Exception ex) {
        }
        List<EAW_Rule_Detail__c> obtainedDetail = [SELECT ID, Name,Rule_No__c  FROM EAW_Rule_Detail__c LIMIT 1];
        System.assertEquals( true ,obtainedDetail[0].Rule_No__c != null );
    }
    
    @isTest static void getDependentFieldSet_Test() {
        
        List<EAW_Plan_Guideline__c> planGuideLine = [SELECT id, Name FROM EAW_Plan_Guideline__c LIMIT 1];
        EAW_Rule__c ruleInstance = [SELECT ID, Name, Plan_Guideline__c  FROM EAW_Rule__c LIMIT 1]; 
        ruleInstance.Plan_Guideline__c  = planGuideLine[0].Id;
        update ruleInstance;
                
        
        Test.startTest();
        
        Test.setMock(HttpCalloutMock.class, new EAW_PlanGuidelineRuleBuilder_CalloutMock());        
        
        EAW_PlanGuidelineRuleBuilderController.getDependentFieldSet('Condition_Operator__c', String.ValueOf(planGuideLine[0].Id));
        
        Test.stopTest();
        
        EAW_PlanGuidelineRuleBuilderController.getReleaseDateStaus();
    }
   /* @isTest static void getFieldSet_Test() {
    
        EAW_PlanGuidelineRuleBuilderController.getFieldSet();
    }*/
}