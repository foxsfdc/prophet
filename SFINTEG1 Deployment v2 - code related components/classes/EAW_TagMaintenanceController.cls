public with sharing class EAW_TagMaintenanceController {

    @AuraEnabled
    public static List<EAW_Tag__c> search() {
        List<EAW_Tag__c> tags;
        String query = 'SELECT Name, Tag_Type__c, Active__c from EAW_Tag__c';
        
        tags = Database.query(query);
        return tags;
    }
    
}