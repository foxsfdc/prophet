public class EAW_Outbound_FailedRecordProcess implements Database.Batchable<sObject>,Database.Stateful, Schedulable,Database.AllowsCallouts {
    public List<String> recordTypes = new List<String>{EAW_Outbound_Utility.OutboundDataType.WindowGuideline.name(),
        				EAW_Outbound_Utility.OutboundDataType.WindowGuidelineStrands.name(),EAW_Outbound_Utility.OutboundDataType.ReleaseDateGuideline.name()};
    public String recordType = recordTypes[0];
    
    //TO do: Need to add flag to skip error logging if api callouts happen from this class, also need to handle the Retired flag after successful api call.
                            
	public Database.QueryLocator start(Database.BatchableContext bc) {
    
        return Database.getQueryLocator(
        	'SELECT Id,Record_Type__c,System__c,Failed_Record_Ids__c FROM EAW_Error_Log__c WHERE Record_Type__c = :recordType'
        );
    }

    public void execute(Database.BatchableContext bc, List<EAW_Error_Log__c> records){
        if(records!=null && records.isempty()==False){
            Set<ID> recordIds = new Set<ID>();
        	for(EAW_Error_Log__c eaw: records){
                if(String.isNotBlank(eaw.Failed_Record_Ids__c)){
                    for(String rid:eaw.Failed_Record_Ids__c.split(';')){
                        recordIds.add(rid);
                    }
                }
        	}
            if(recordIds!=null && !recordIds.isEmpty()){
               if(EAW_Outbound_Utility.OutboundDataType.WindowGuideline.name()==recordType){
                	EAW_WindowGuidelines_Outbound.sendDataSync(recordIds,'');
               }else if(EAW_Outbound_Utility.OutboundDataType.WindowGuidelineStrands.name()==recordType){
                	EAW_WindowGuidelineStrands_Outbound.sendDataSync(recordIds,'');
               }else if(EAW_Outbound_Utility.OutboundDataType.ReleaseDateGuideline.name()==recordType){
                	EAW_ReleaseDateGuidelines_Outbound.sendDataSync(recordIds,'');
               } 
            }
            delete records;
        }
    }
    
    public void finish(Database.BatchableContext bc) {
        if(recordTypes!=null && !recordTypes.isEmpty()){
            recordTypes.remove(0);
            if(recordTypes!=null && !recordTypes.isEmpty()){
                EAW_Outbound_FailedRecordProcess eawBatch = new EAW_Outbound_FailedRecordProcess();
                eawBatch.recordTypes=recordTypes;
                eawBatch.recordType=eawBatch.recordTypes[0];
                ID batchprocessid = Database.executeBatch(eawBatch, 1);//We need to process record by record, since for each record we may have 100 Failed record ids
                system.debug('batchprocessid: '+batchprocessid);
            }
        }
    }
    
    public void execute(SchedulableContext sc) {
        EAW_Outbound_FailedRecordProcess eawBatch = new EAW_Outbound_FailedRecordProcess();
        Database.executeBatch(eawBatch,1);//We need to process record by record, since for each record we may have 100 Failed record ids
    }
}