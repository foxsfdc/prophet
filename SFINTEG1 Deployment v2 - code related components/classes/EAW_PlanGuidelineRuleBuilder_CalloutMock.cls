@isTest
global class EAW_PlanGuidelineRuleBuilder_CalloutMock implements HttpCalloutMock  {

    EAW_Plan_Guideline__c planguideLine = new EAW_Plan_Guideline__c();
    
    String testString = '{"aA43D0000004Cqu" : "EAW_Rule_Detail__c ", "controllerValues" : {"Condition_Operator__c" : "NOT"}, "values" : ["EAW_Rule_Detail__c : {}"]}';
    Blob bodyAsBlob = Blob.valueof(testString );

        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse resp = new HttpResponse();
            resp.setStatusCode(200);
            resp.setStatus('ok');
            resp.setBodyAsBlob(bodyAsBlob);
           
            return resp;
    }
}