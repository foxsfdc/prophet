public class EAW_SalesRegionsController {

    @AuraEnabled
    public static String saveSalesRegions(String salesRegion , String territoryList) {
    
        System.debug('territory::::'+ territoryList);
        System.debug('salesRegion ::::'+ salesRegion );
        
        String errorStr;
        
        if(salesRegion != NULL && territoryList != NULL){
            Sales_Regions__c upsertsr=  Sales_Regions__c.getValues(salesRegion);
            if(upsertsr != NULL){
            
            
             String terr= upsertsr.Territories__c;

            List<String> lstterr = terr.split(';');
            System.debug(lstterr);
           /* for(String lststr : lstterr ){
            
                if(territoryList.indexOf(lststr) ==  -1){
                
                    territoryList = territoryList+ ',' + lststr;
                }
            }*/
            
            System.debug('territoryList :::'+territoryList );
            upsertsr.Territories__c = territoryList;
            try {
                
                update upsertsr;
                return upsertsr.Name;
            }catch(Exception e){
            
            
                  errorStr = 'Error : ' + e.getMessage();
            }
        }else{
            
                Sales_Regions__c sr= new Sales_Regions__c();
                sr.Name= salesRegion ;
                sr.Territories__c = territoryList; 
                try {
                
                    insert sr;
                    return sr.Name;
                    
                }catch(Exception e){
                
                   
                      errorStr = 'Error : ' + e.getMessage();
                }
            }
        }
         return errorStr ;
    }
    
    @AuraEnabled
    public static   List<EAW_LookupSearchResult> autoSelected(String selectedSalesRegionStr){
        
            List<EAW_LookupSearchResult> lookupSearchResults  = new List<EAW_LookupSearchResult>();
            List<SObject> searchResults = new List<SObject>();
            System.debug('selectedSalesRegionStr::x::::'+selectedSalesRegionStr);
            List<String> scltRegionName = new List<String>();
         
            set<String> respectiveTerritryName = new set<String>();
            List<Sales_Regions__c> saleRegionMap = new List<Sales_Regions__c>();
            List<String> selectedSalesRegionStrName = new List<String>();
            
            if(!String.isBlank(selectedSalesRegionStr)){
                
                selectedSalesRegionStrName = selectedSalesRegionStr.split(',');
            }
            System.debug('selectedSalesRegionStrName:::'+selectedSalesRegionStrName); 
        
            for(Sales_Regions__c sr : [SELECT Id, Name,Territories__c FROM Sales_Regions__c WHERE Name IN: selectedSalesRegionStrName ORDER BY Name ASC]){
                    
                for(String stTer : sr.Territories__c.split(';')) {
                     
                    if(!respectiveTerritryName.contains(stTer)){
                        if(stTer != NULL && !String.isBlank(stTer)){
                            
                             respectiveTerritryName.add(stTer);
                        }
                    }
                }
            }
           System.debug('respectiveTerritryName:::'+respectiveTerritryName);
           
           for(SObject result : (List<SObject>) [SELECT Id, Name FROM EAW_Territories__c WHERE Name IN :respectiveTerritryName ORDER BY Name ASC]){
            
             lookupSearchResults.add(new EAW_LookupSearchResult(String.valueOf(result.get('Id')), 'EAW_Territories__c', 'standard:account', String.valueOf(result.get('Name')), null));
           }
           return lookupSearchResults;
    }
    
}