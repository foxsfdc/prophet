@isTest
private class EAW_GenericSoftDeleteOverride_Test {

    @testSetup 
    static void setupTestData() {
        
                  
        List<EAW_Tag__c> tagList = EAW_TestDataFactory.createTag(1, True,'Window Tag');
        List<EAW_Window_Guideline__c> wgList = EAW_TestDataFactory.createWindowGuideLine(2, FALSE);
        wgList[0].Product_Type__c = 'Feature';
        wgList[1].Product_Type__c = 'Feature';
        
        insert wgList;
        
        List<EAW_Window_Guideline_Strand__c> wgsList = EAW_TestDataFactory.createWindowGuideLineStrand(2, FALSE);
        
        wgsList[0].EAW_Window_Guideline__c = wgList[0].Id;
        wgsList[0].Media__c = 'Free TV';
        wgsList[0].Language__c = 'English';
        wgsList[0].Territory__c = 'India';
        
        wgsList[1].EAW_Window_Guideline__c = wgList[1].Id;
        wgsList[1].Media__c = 'Basic TV';
        wgsList[1].Language__c = 'English';
        wgsList[1].Territory__c = 'Albania';
        
        insert wgsList;
        
        wgList[0].Status__c = 'Active';
        wgList[1].Status__c = 'Active';
        
        update wgList;
        
        List<EAW_Plan_Guideline__c> pgList = EAW_TestDataFactory.createPlanGuideline(2, FALSE);
        pgList[0].Product_Type__c = 'Feature';
        pgList[1].Product_Type__c = 'Feature';
        
        insert pgList;
        
        List<EAW_Plan_Window_Guideline_Junction__c> pwgList = EAW_TestDataFactory.createPlanWindowGuideLineJunction(1, FALSE);
        pwgList[0].Plan__c = pgList[0].Id;
        pwgList[0].Window_Guideline__c = wgList[0].Id;
        
        //pwgList[1].Plan__c = pgList[1].Id;
        //pwgList[1].Window_Guideline__c = wgList[1].Id;
        
        insert pwgList;
        List<EAW_Title__c> title = EAW_TestDataFactory.createEAWTitle (1,false);//[SELECT Id FROM EAW_Title__c LIMIT 1];
        title[0].RLSE_CAL_YR__c = '2001';
        insert title;
        
        String PlanQualifierRule = '{"sObjectType": "EAW_Rule__c"}';
        String PlanQualifierRuleRuleDetail='{"sObjectType": "EAW_Rule_Detail__c", "Condition_Operator__c": ">", "Condition_Year__c": "2000", "Condition_Field__c": "Release Year"}';
        EAW_TestRuleDataFactory.savePgQualifiers( PlanQualifierRule , PlanQualifierRuleRuleDetail, pgList[0].Id);
        
       
        pgList[0].Status__c = 'Active';
       // pgList[1].Status__c = 'Active';
        
        update pgList;
        
        //List<EAW_Plan__c> planList = EAW_TestDataFactory.createPlan(2, FALSE);
        //planList[0].Plan_Guideline__c = pgList[0].Id;
        //planList[1].Plan_Guideline__c = pgList[1].Id;
        
        //insert planList;
        
        String newruleContent = '{"sObjectType":"EAW_Rule__c","Nested__c":true}';
        String newruleDetailContent = '[{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Condition_Timeframe__c":"True_Earliest"},"dateCalcs":[{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Conditional_Operand_Id__c":"aA73D000000CyJjSAK","Condition_Met_Months__c":"2","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"},{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Conditional_Operand_Id__c":"aA73D000000CyJjSAK","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"}],"nodeCalcs":[{"ruleDetail":{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":2,"Condition_Timeframe__c":"True_Earliest"},"dateCalcs":[{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":2,"Conditional_Operand_Id__c":"aA73D000000CyJjSAK","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"}],"nodeCalcs":[]}]}]';
        EAW_TestRuleDataFactory.saveRuleAndRuleDetails(newruleContent,newruleDetailContent,false,wgList[0].Id,'Start Date',String.valueOf(wgList[0].Id));
        
        //EAW_TestDataFactory.createWindow (1,true);
        //String ruleAndRuleDetailNodes = '[{"ruleDetail":{"Id":"aAM3D00000003cjWAA","Rule_No__c":"aAN3D0000008RV7WAM","Rule_Order__c":1,"Nest_Order__c":1,"Condition_Met_Months__c":"2","Condition_Met_Days__c":0,"Conditional_Operand_Id__c":"aA73D0000004mIRSAY","Condition_Operand_Details__c":"Theatrical / Bahrain / Lithuanian / Feature"},"dateCalcs":[],"nodeCalcs":[]}]';
        //EAW_TestRuleDataFactory.saveRuleAndRuleDetails(null,ruleAndRuleDetailNodes,false,wgList[0].Id,'Start Date',wgList[0].Id);
        List<EAW_Release_Date_Guideline__c> rdgList = EAW_TestDataFactory.createReleaseDateGuideline (1,true, 'Direct to Video', new List<String>{'EST-HD'},'Afghanistan','Afrikaans');
        List<EAW_Release_Date__c> rdList = EAW_TestDataFactory.createReleaseDate(1,false);
        rdList[0].Release_Date_Guideline__c = rdgList[0].Id;
        insert rdList;
    }
    @isTest
    static void getErrorWindow_Test() {
    
        List<EAW_Window_Guideline_Strand__c> wgsList = [SELECT Id FROM EAW_Window_Guideline_Strand__c LIMIT 1];
        List<EAW_Window_Guideline__c> wgList = [SELECT Id FROM EAW_Window_Guideline__c LIMIT 1];
        List<EAW_Title__c> title = [SELECT Id FROM EAW_Title__c LIMIT 1];
        List<EAW_Window__c> winList= [SELECT ID FROM EAW_Window__c LIMIT 1];
        winList[0].EAW_Window_Guideline__c = wgList[0].Id;
        winList[0].EAW_Title_Attribute__c = title[0].Id;
        update winList; 
        List<EAW_Release_Date_Guideline__c> rdgList = [SELECT Id FROM EAW_Release_Date_Guideline__c LIMIT 1];
        List<EAW_Release_Date__c> rdList = [SELECT Id FROM EAW_Release_Date__c LIMIT 1];
        rdList[0].Release_Date_Guideline__c = rdgList[0].Id;
        update rdList;
        
        EAW_GenericSoftDeleteOverride.getError(JSON.serialize(winList),'EAW_Window__c');
    }
    
    @isTest
    static void getErrorWG_Test() {
    
        List<EAW_Window_Guideline_Strand__c> wgsList = [SELECT Id FROM EAW_Window_Guideline_Strand__c LIMIT 1];
        List<EAW_Window_Guideline__c> wgList = [SELECT Id FROM EAW_Window_Guideline__c LIMIT 1];
        List<EAW_Title__c> title = [SELECT Id FROM EAW_Title__c LIMIT 1];
        List<EAW_Window__c> winList= [SELECT ID FROM EAW_Window__c LIMIT 1];
        winList[0].EAW_Window_Guideline__c = wgList[0].Id;
        winList[0].EAW_Title_Attribute__c = title[0].Id;
        update winList; 
        List<EAW_Release_Date_Guideline__c> rdgList = [SELECT Id FROM EAW_Release_Date_Guideline__c LIMIT 1];
        List<EAW_Release_Date__c> rdList = [SELECT Id FROM EAW_Release_Date__c LIMIT 1];
        rdList[0].Release_Date_Guideline__c = rdgList[0].Id;
        update rdList;
        
        EAW_GenericSoftDeleteOverride.getError(JSON.serialize(wgList),'EAW_Window_Guideline__c');
        
    }
    
    @isTest
    static void getErrorWGS_Test() {
    
        List<EAW_Window_Guideline_Strand__c> wgsList = [SELECT Id FROM EAW_Window_Guideline_Strand__c LIMIT 1];
        List<EAW_Window_Guideline__c> wgList = [SELECT Id FROM EAW_Window_Guideline__c LIMIT 1];
        List<EAW_Title__c> title = [SELECT Id FROM EAW_Title__c LIMIT 1];
        List<EAW_Window__c> winList= [SELECT ID FROM EAW_Window__c LIMIT 1];
        winList[0].EAW_Window_Guideline__c = wgList[0].Id;
        winList[0].EAW_Title_Attribute__c = title[0].Id;
        update winList; 
        List<EAW_Release_Date_Guideline__c> rdgList = [SELECT Id FROM EAW_Release_Date_Guideline__c LIMIT 1];
        List<EAW_Release_Date__c> rdList = [SELECT Id FROM EAW_Release_Date__c LIMIT 1];
        rdList[0].Release_Date_Guideline__c = rdgList[0].Id;
        update rdList;
        
        EAW_GenericSoftDeleteOverride.getError(JSON.serialize(wgsList),'EAW_Window_Guideline_Strand__c');
    }
    
    @isTest
    static void getErrorRD_Test() {
    
        List<EAW_Window_Guideline_Strand__c> wgsList = [SELECT Id FROM EAW_Window_Guideline_Strand__c LIMIT 1];
        List<EAW_Window_Guideline__c> wgList = [SELECT Id FROM EAW_Window_Guideline__c LIMIT 1];
        List<EAW_Title__c> title = [SELECT Id FROM EAW_Title__c LIMIT 1];
        List<EAW_Window__c> winList= [SELECT ID FROM EAW_Window__c LIMIT 1];
        winList[0].EAW_Window_Guideline__c = wgList[0].Id;
        winList[0].EAW_Title_Attribute__c = title[0].Id;
        update winList; 
        List<EAW_Release_Date_Guideline__c> rdgList = [SELECT Id FROM EAW_Release_Date_Guideline__c LIMIT 1];
        List<EAW_Release_Date__c> rdList = [SELECT Id FROM EAW_Release_Date__c LIMIT 1];
        rdList[0].Release_Date_Guideline__c = rdgList[0].Id;
        update rdList;
        
        String newruleContent = '{"sObjectType":"EAW_Rule__c","Nested__c":true}';
        String newruleDetailContent = '[{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Condition_Timeframe__c":"True_Earliest"},"dateCalcs":[{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Conditional_Operand_Id__c":"aA73D000000CyJjSAK","Condition_Met_Months__c":"2","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"},{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Conditional_Operand_Id__c":"aA73D000000CyJjSAK","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"}],"nodeCalcs":[{"ruleDetail":{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":2,"Condition_Timeframe__c":"True_Earliest"},"dateCalcs":[{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":2,"Conditional_Operand_Id__c":"aA73D000000CyJjSAK","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"}],"nodeCalcs":[]}]}]';
        EAW_TestRuleDataFactory.saveRuleAndRuleDetails(newruleContent,newruleDetailContent,true,rdgList[0].Id,'Start Date',String.valueOf(rdgList[0].Id));
        
        EAW_GenericSoftDeleteOverride.getError(JSON.serialize(rdList),'EAW_Release_Date__c');
    }
    
    @isTest
    static void getErrorRDG_Test() {
    
        List<EAW_Window_Guideline_Strand__c> wgsList = [SELECT Id FROM EAW_Window_Guideline_Strand__c LIMIT 1];
        List<EAW_Window_Guideline__c> wgList = [SELECT Id FROM EAW_Window_Guideline__c LIMIT 1];
        List<EAW_Title__c> title = [SELECT Id FROM EAW_Title__c LIMIT 1];
        List<EAW_Window__c> winList= [SELECT ID FROM EAW_Window__c LIMIT 1];
        winList[0].EAW_Window_Guideline__c = wgList[0].Id;
        winList[0].EAW_Title_Attribute__c = title[0].Id;
        update winList; 
        
        List<EAW_Release_Date_Guideline__c> rdgList = [SELECT Id FROM EAW_Release_Date_Guideline__c LIMIT 1];
        List<EAW_Release_Date__c> rdList = [SELECT Id FROM EAW_Release_Date__c LIMIT 1];
        rdList[0].Release_Date_Guideline__c = rdgList[0].Id;
        update rdList;
        
        String newruleContent = '{"sObjectType":"EAW_Rule__c","Nested__c":true}';
        String newruleDetailContent = '[{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Condition_Timeframe__c":"True_Earliest"},"dateCalcs":[{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Conditional_Operand_Id__c":"aA73D000000CyJjSAK","Condition_Met_Months__c":"2","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"},{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Conditional_Operand_Id__c":"aA73D000000CyJjSAK","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"}],"nodeCalcs":[{"ruleDetail":{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":2,"Condition_Timeframe__c":"True_Earliest"},"dateCalcs":[{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":2,"Conditional_Operand_Id__c":"aA73D000000CyJjSAK","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"}],"nodeCalcs":[]}]}]';
        EAW_TestRuleDataFactory.saveRuleAndRuleDetails(newruleContent,newruleDetailContent,true,rdgList[0].Id,'Start Date',String.valueOf(rdgList[0].Id));
        
        EAW_GenericSoftDeleteOverride.getError(JSON.serialize(rdgList ),'EAW_Release_Date_Guideline__c');
    }
    @isTest
    static void getErrorWG_SoftDelete_Test() {
    
        
        List<EAW_Window_Guideline__c> wgList = [SELECT Id FROM EAW_Window_Guideline__c LIMIT 1];
        wgList[0].Soft_Deleted__c = 'TRUE'; 
          
        EAW_GenericSoftDeleteOverride.getError(JSON.serialize(wgList),'EAW_Window_Guideline__c');
    }
    @isTest
    static void getErrorWGS_SoftDelete_Test() {
    
        
        List<EAW_Window_Guideline_Strand__c> wgsList = [SELECT Id FROM EAW_Window_Guideline_Strand__c LIMIT 1];
        wgsList[0].Soft_Deleted__c = 'TRUE'; 
          
        EAW_GenericSoftDeleteOverride.getError(JSON.serialize(wgsList ),'EAW_Window_Guideline_Strand__c');
    }
     @isTest
    static void getErrorWindow_SoftDelete_Test() {
    
        
        List<EAW_Window__c> winList= [SELECT ID FROM EAW_Window__c LIMIT 1];
        winList[0].Soft_Deleted__c = 'TRUE'; 
          
        EAW_GenericSoftDeleteOverride.getError(JSON.serialize(winList),'EAW_Window__c');
    }
    @isTest
    static void getErrorRDG_SoftDelete_Test() {
    
        
        List<EAW_Release_Date_Guideline__c> rdgList =  [SELECT ID FROM EAW_Release_Date_Guideline__c LIMIT 1];
        rdgList[0].Soft_Deleted__c = 'TRUE'; 
          
        EAW_GenericSoftDeleteOverride.getError(JSON.serialize(rdgList),'EAW_Release_Date_Guideline__c');
    }
    @isTest
    static void getErrorReleaseDate_SoftDelete_Test() {
    
        
        List<EAW_Release_Date__c> rdList =  [SELECT Id FROM EAW_Release_Date__c LIMIT 1];
        rdList[0].Soft_Deleted__c = 'TRUE'; 
          
        EAW_GenericSoftDeleteOverride.getError(JSON.serialize(rdList),'EAW_Release_Date__c');
    }
    @isTest
    static void getErrorRDGNoError_SoftDelete_Test() {
    
        
        List<EAW_Release_Date_Guideline__c> rdgList = EAW_TestDataFactory.createReleaseDateGuideline (1,true, 'Direct to Video', new List<String>{'EST-HD'},'India','Afrikaans');
        //List<EAW_Release_Date__c> rdList = EAW_TestDataFactory.createReleaseDate(1,false);
        //rdList[0].Release_Date_Guideline__c = rdgList[0].Id;
        //rdList;
          
        EAW_GenericSoftDeleteOverride.getError(JSON.serialize(rdgList),'EAW_Release_Date_Guideline__c');
    }
    @isTest
    static void getErrorWGNoError_SoftDelete_Test() {
    
        
       List<EAW_Window_Guideline__c> wgList = EAW_TestDataFactory.createWindowGuideLine(1, FALSE);
        wgList[0].Product_Type__c ='Direct to Video';
     
        
        insert wgList;
          
        EAW_GenericSoftDeleteOverride.getError(JSON.serialize(wgList ),'EAW_Window_Guideline__c');
    }
   
}