@isTest
public class EAW_WindowTrigger_Test {
    
     @testSetup static void setupTestData() {
        
        List<EAW_Release_Date_Guideline__c> rdgList =  EAW_TestDataFactory.createReleaseDateGuideline(1, TRUE, 'Feature', new List<String>{'Theatrical'}, 'United States +', 'English');
              
        List<EAW_Title__c> titleList = EAW_TestDataFactory.createEAWTitle(2, FALSE);
        titleList[0].RLSE_CAL_YR__c = '2019';
        titleList[1].RLSE_CAL_YR__c = '2019';
        insert titleList;
        
        List<EAW_Release_Date__c> releaseDateList = EAW_TestDataFactory.createReleaseDate(4, FALSE);
              
        releaseDateList[0].Release_Date_Guideline__c = rdgList[0].Id;
        releaseDateList[0].Title__c = titleList[0].Id;
        releaseDateList[0].Manual_Date__c = System.Today();
        releaseDateList[0].Temp_Perm__c = 'Temporary';
        
        releaseDateList[1].Release_Date_Guideline__c = rdgList[0].Id;
        releaseDateList[1].Title__c = titleList[1].Id;
        releaseDateList[1].Manual_Date__c = System.Today();
        releaseDateList[1].Temp_Perm__c = 'Temporary';
        
        insert releaseDateList;
        
        List<EAW_Window_Guideline__c> wgList = EAW_TestDataFactory.createWindowGuideLine(2, TRUE);
        
        List<EAW_Window_Guideline_Strand__c> wgStrandList = EAW_TestDataFactory.createWindowGuideLineStrand(2, FALSE);
        
        wgStrandList[0].EAW_Window_Guideline__c = wgList[0].Id;
        wgStrandList[1].EAW_Window_Guideline__c = wgList[1].Id;
        
        insert wgStrandList;
        
        String ruleJson = '{"sObjectType":"EAW_Rule__c","Nested__c":false}';
        String ruleDetailJson = '[{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Conditional_Operand_Id__c":"aA73D000000CyJjSAK","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature", "Condition_Date_Duration__c":"Month","Condition_Criteria_Amount__c":"1"},"dateCalcs":[],"nodeCalcs":[]}]';
        EAW_TestRuleDataFactory.saveRuleAndRuleDetails(ruleJson, ruleDetailJson, FALSE ,wgList[0].Id, 'Start Date',rdgList[0].Id); // Parameters: (String ruleAndRuleDetailNodes,Boolean isRDG,String guidelineId,String wgRuleDate,String conditionalOperandId);
        EAW_TestRuleDataFactory.saveRuleAndRuleDetails(ruleJson, ruleDetailJson, FALSE ,wgList[0].Id, 'End Date',rdgList[0].Id);
        EAW_TestRuleDataFactory.saveRuleAndRuleDetails(ruleJson, ruleDetailJson, FALSE ,wgList[0].Id, 'Outside Date',rdgList[0].Id);
        EAW_TestRuleDataFactory.saveRuleAndRuleDetails(ruleJson, ruleDetailJson, FALSE ,wgList[0].Id, 'Tracking Date',rdgList[0].Id);
        
        wgList[0].OriginWindowGuidelineId__c = wgList[0].Id;
        wgList[0].Status__c = 'Active';
        wgList[1].OriginWindowGuidelineId__c = wgList[1].Id;
        wgList[1].Status__c = 'Active';
        update wgList;
        
        List<EAW_Plan_Guideline__c> pgList = EAW_TestDataFactory.createPlanGuideline(1,FALSE);
        pgList[0].Product_Type__c  = 'Feature';
        insert pgList;
        
        String ruleDetailJson1 = '{"sObjectType": "EAW_Rule_Detail__c", "Condition_Operator__c": ">", "Condition_Year__c": "2000", "Condition_Field__c": "Release Year"}';
        String ruleJson1 = '{"sObjectType": "EAW_Rule__c"}';
        EAW_TestRuleDataFactory.savePgQualifiers(ruleJson1,ruleDetailJson1,pgList[0].Id);
        
        List<EAW_Plan_Window_Guideline_Junction__c> planWindowGuidelineJunctionList = EAW_TestDataFactory.createPlanWindowGuideLineJunction ( 2, FALSE);
        
        planWindowGuidelineJunctionList[0].Plan__c = pgList[0].Id;
        planWindowGuidelineJunctionList[0].Window_Guideline__c = wgList[0].Id;
        
        planWindowGuidelineJunctionList[1].Plan__c = pgList[0].Id;
        planWindowGuidelineJunctionList[1].Window_Guideline__c = wgList[1].Id;
        
        insert planWindowGuidelineJunctionList;
        
        Test.StartTest();
        pgList[0].Status__c = 'Active';
        update pgList[0];
        Test.StopTest();
    }
    
    @isTest static void processWindowTriggerLogicTest1() {
        
        EAW_Notification_Status__c ns = new EAW_Notification_Status__c(
            RS_Category_Notification__c = TRUE, 
            RSRightsStatusChangeNotification__c = TRUE,
            RSRightsStatusChangedtoRightsExluded__c = TRUE,
            RSRightsStatusisFirmed__c = TRUE,
            WGCategoryNotification__c = TRUE,
            WGStartDateOutsideDateNotification__c = TRUE,
            WGStartDateEndDateNotification__c = TRUE,
            WGCurrentDateNotification__c = TRUE,
            WGOutsideDateNotification__c = TRUE,
            WGOutsideDateUpstreamNotification__c = TRUE
        );
        insert ns;
        
        Sales_Regions__c sr = new Sales_Regions__c (
            Name = 'Asia Pacific',
            Territories__c = 'Abu Dhabi;Algeria;Andorra'
        );
        insert sr;
        
        CollaborationGroup chatterGroup = new CollaborationGroup (
            Name = 'Test1-Window Review',
            CollaborationType = 'Public'
        );
        insert chatterGroup;
        
        List<EAW_Window__c> windowList = [ Select Id, License_Info_Codes__c, Start_Date__c, End_Date__c, Outside_Date__c, Tracking_Date__c From EAW_Window__c limit 1 ];
        
        windowList[0].License_Info_Codes__c = 'ADV';
        windowList[0].Status__c = 'Firm';
        update windowList[0];
        
        windowList[0].License_Info_Codes__c = 'ANA';
        windowList[0].Start_Date__c = System.Today()+20;
        windowList[0].End_Date__c = System.Today()-10;
        windowList[0].Outside_Date__c = System.Today()+10;
        windowList[0].Tracking_Date__c = System.Today();
        windowList[0].Status__c = 'Estimated';
        update windowList[0];
    }
    
    @isTest static void processWindowTriggerLogicTest() {
        
        Test.StartTest();
        
        EAW_Notification_Status__c ns = new EAW_Notification_Status__c(
            RS_Category_Notification__c = TRUE, 
            RSRightsStatusChangeNotification__c = TRUE,
            RSRightsStatusChangedtoRightsExluded__c = TRUE,
            RSRightsStatusisFirmed__c = TRUE,
            WGCategoryNotification__c = TRUE,
            WGStartDateOutsideDateNotification__c = TRUE,
            WGStartDateEndDateNotification__c = TRUE,
            WGCurrentDateNotification__c = TRUE,
            WGOutsideDateNotification__c = TRUE,
            WGOutsideDateUpstreamNotification__c = TRUE,
            WGCurrDateSpecificRangeNotification__c = 60,
            WGOutsideDateWithinSpecificDays__c = 15,
            WGOutsideDateWithinXDays__c = 20
        );
        insert ns;
        
        Sales_Regions__c sr = new Sales_Regions__c (
            Name = 'Asia Pacific',
            Territories__c = 'Abu Dhabi;Algeria;Andorra'
        );
        insert sr;
        
        CollaborationGroup chatterGroup = new CollaborationGroup (
            Name = 'Test1-Window Review',
            CollaborationType = 'Public'
        );
        insert chatterGroup;
        
        List<EAW_Release_Date_Guideline__c> rdgList =  EAW_TestDataFactory.createReleaseDateGuideline(1, TRUE, 'Feature', new List<String>{'Theatrical'}, 'Albania', 'Tamil');
        
        EDM_REF_DIVISION__c refDivision = new EDM_REF_DIVISION__c(Name = 'Fox 2001');
        insert refDivision;
        
        EDM_REF_PRODUCT_TYPE__c refProdType = new EDM_REF_PRODUCT_TYPE__c( Name = 'Shorts', PROD_TYP_CD__c = 'Shorts', PROD_TYP_DESC__c = 'Shorts' );
        insert refProdType;
              
        EAW_Title__c titleList = new EAW_Title__c(Name='Test Title-123456543', PROD_TYP_CD_INTL__c = refProdType.id, FIN_DIV_CD__c = refDivision.Id, LIFE_CYCL_STAT_GRP_CD__c = 'PUBLC');
        insert titleList;
        
        List<EAW_Release_Date__c> releaseDateList = EAW_TestDataFactory.createReleaseDate(2, FALSE);
              
        releaseDateList[0].Release_Date_Guideline__c = rdgList[0].Id;
        releaseDateList[0].Title__c = titleList.Id;
        releaseDateList[0].Manual_Date__c = System.Today();
        releaseDateList[0].Temp_Perm__c = 'Temporary';
        
        releaseDateList[1].Release_Date_Guideline__c = rdgList[0].Id;
        releaseDateList[1].Title__c = titleList.Id;
        releaseDateList[1].Manual_Date__c = System.Today();
        releaseDateList[1].Temp_Perm__c = 'Temporary';
        
        insert releaseDateList;
        
        List<EAW_Window_Guideline__c> wgList = EAW_TestDataFactory.createWindowGuideLine(1, FALSE);
        wgList[0].Window_Guideline_Alias__c = 'TestWG_102435';
        wgList[0].Product_Type__c = 'Shorts';
        
        insert wgList;
        
        List<EAW_Window_Guideline_Strand__c> wgsList = EAW_TestDataFactory.createWindowGuideLineStrand(1, FALSE);
        
        wgsList[0].EAW_Window_Guideline__c = wgList[0].Id;
        wgsList[0].Media__c = 'Free On Demand';
        wgsList[0].Language__c = 'English';
        wgsList[0].Territory__c = 'India';
        
        insert wgsList;
        
        List<EAW_Plan_Guideline__c> pgList = EAW_TestDataFactory.createPlanGuideline(1, FALSE);
        pgList[0].Name = 'Test PG-1';
        pgList[0].Product_Type__c = 'Shorts';
        
        insert pgList;
        
        List<EAW_Plan_Window_Guideline_Junction__c> pwgList = EAW_TestDataFactory.createPlanWindowGuideLineJunction(1, FALSE);
        pwgList[0].Plan__c = pgList[0].Id;
        pwgList[0].Window_Guideline__c = wgList[0].Id;
        
        insert pwgList;
        
        wgList[0].Status__c = 'Active';
        update wgList;
        pgList[0].Status__c = 'Active';
        update pgList;
        
        EAW_Window__c winowIns = new EAW_Window__c();
        winowIns.EAW_Window_Guideline__c = wgList[0].Id;   
        winowIns.Window_Type__c = 'Current';
        winowIns.EAW_Title_Attribute__c = titleList.Id;
        winowIns.License_Info_Codes__c = 'ADV';
        winowIns.Start_Date__c = System.Today()+10;
        winowIns.End_Date__c = System.Today()+5;
        winowIns.Outside_Date__c = System.Today()+7;
        winowIns.Tracking_Date__c = System.Today()+8;
        winowIns.Status__c = 'Estimated';
        winowIns.LicenseInfoCodes_OldValue__c = 'ABCD';
        winowIns.Right_Status__c = 'Clear';
         
        insert winowIns;
        
        winowIns.License_Info_Codes__c = 'EXHONLY';
        winowIns.Start_Date__c = System.Today()+20;
        winowIns.End_Date__c = System.Today()+6;
        winowIns.Outside_Date__c = System.Today()+10;
        winowIns.Tracking_Date__c = System.Today()+29;
        winowIns.Status__c = 'Tentative';
        winowIns.LicenseInfoCodes_OldValue__c = 'ABCD';
        winowIns.Right_Status__c = 'Processing';
        winowIns.RightsStatusOldValue__c = 'Rights Exluded';
        
        update winowIns;
            
        Test.StopTest();
    }
    
    @isTest static void rightsCheckTest() {
        
        EAW_Notification_Status__c ns = new EAW_Notification_Status__c(
            RS_Category_Notification__c = TRUE, 
            RSRightsStatusChangeNotification__c = TRUE,
            RSRightsStatusChangedtoRightsExluded__c = TRUE,
            RSRightsStatusisFirmed__c = TRUE,
            WGCategoryNotification__c = TRUE,
            WGStartDateOutsideDateNotification__c = TRUE,
            WGStartDateEndDateNotification__c = TRUE,
            WGCurrentDateNotification__c = TRUE,
            WGOutsideDateNotification__c = TRUE,
            WGOutsideDateUpstreamNotification__c = TRUE,
            WGCurrDateSpecificRangeNotification__c = 60,
            WGOutsideDateWithinSpecificDays__c = 15,
            WGOutsideDateWithinXDays__c = 20
        );
        insert ns;
        
        Sales_Regions__c sr = new Sales_Regions__c (
            Name = 'Asia Pacific',
            Territories__c = 'Abu Dhabi;Algeria;Andorra'
        );
        insert sr;
        
        CollaborationGroup chatterGroup = new CollaborationGroup (
            Name = 'Test2-Window Review',
            CollaborationType = 'Public'
        );
        insert chatterGroup;
        
        List<EAW_Window__c> windowList = [ Select Id, License_Info_Codes__c, Prevent_RightsCheck_Recursive_Callout__c, Start_Date__c, End_Date__c, Outside_Date__c, Tracking_Date__c From EAW_Window__c limit 1 ];
        
        EAW_Window_Handler windowHandler = new EAW_Window_Handler();
        
        windowList[0].Start_Date__c = System.Today()+20;
        windowList[0].End_Date__c = System.Today()+6;
        windowList[0].Outside_Date__c = System.Today()+10;
        windowList[0].Tracking_Date__c = System.Today()+29;
        windowList[0].Status__c = 'Tentative';
        windowList[0].LicenseInfoCodes_OldValue__c = 'ABCD';
        windowList[0].Right_Status__c = 'Clear with TBA End Date';
        
        update windowList;
        
        windowHandler.initiateRightsCheckCallout( windowList );
    }    
}