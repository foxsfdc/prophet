public class EAW_HistoryController {
    
    @AuraEnabled
    public static WrapperClass getWindowHistory(String windowId){
    
        //LRCC-1787 Right_Status__c,LicenseInfoCodes_OldValue__c,LicenseInfoCodes_Changed_Dates__c,LicenseInfoCodes_Changed_Users__c,OldTags__c,Tags_Changed_Dates__c,Tags_Changed_Users__c,OldNotes__c,Notes_Changed_Dates__c,Notes_Changed_Users__c fields are included
        //LRCC-1729 Start_Date_Text__c,End_Date_Text__c fields are added
        List<EAW_Window__c> windows = [Select Id, Start_Date__c,End_Date__c,Start_Date_Text__c,End_Date_Text__c,Outside_Date__c,Tracking_Date__c,Status__c,License_Info_Codes__c,Retired__c,Right_Status__c,LicenseInfoCodes_OldValue__c,LicenseInfoCodes_Changed_Dates__c,LicenseInfoCodes_Changed_Users__c,OldTags__c,Tags_Changed_Dates__c,Tags_Changed_Users__c,OldNotes__c,Notes_Changed_Dates__c,Notes_Changed_Users__c, (Select Field, OldValue, NewValue, CreatedBy.Name,CreatedDate From Histories order by CreatedDate desc) From EAW_Window__c WHERE Id = :windowId];
        
        List<String> windowNewTags = new List<String>();
        
        for(EAW_Window_Tag_Junction__c junctionRec : [SELECT Window__c, Tag__r.Name FROM EAW_Window_Tag_Junction__c WHERE Window__c = : windowId]) {
            windowNewTags.add(junctionRec.Tag__r.Name);
        }
        
        List<String> windowNewNotes = new List<String>();
        
        for(Note noteRec: [Select ParentId, Title, Body From Note where ParentId = : windowId]) {
        
            String newNote = noteRec.Title;
            if(String.isNotBlank(noteRec.Body)) {
                newNote = newNote + '-' + noteRec.Body;
            }
            windowNewNotes.add(newNote);
        }
        
        return (new WrapperClass(windows, windowNewTags, windowNewNotes));
    }
    
    @AuraEnabled
    public static WrapperClass getReleaseDateHistory(String releaseDateId){
    
        //LRCC-1787 OldTags__c,Tags_Changed_Dates__c,Tags_Changed_Users__c,OldNotes__c,Notes_Changed_Dates__c,Notes_Changed_Users__c field are included
        
        List<EAW_Release_Date__c> releaseDates = [Select Id, Active__c,Allow_Manual__c,Feed_Date__c,Is_Confidential__c,Manual_Date__c,Projected_Date__c,Release_Date__c,Status__c,EAW_Release_Date_Type__c,Temp_Perm__c,Title__c,CreatedBy.Name,CreatedDate,OldTags__c,Tags_Changed_Dates__c,Tags_Changed_Users__c,OldNotes__c,Notes_Changed_Dates__c,Notes_Changed_Users__c, (Select Field, OldValue, NewValue, CreatedBy.Name,CreatedDate From Histories order by CreatedDate desc) From EAW_Release_Date__c WHERE Id = :releaseDateId];
        
        List<String> releaseDateNewTags = new List<String>();
        
        for(EAW_Release_Date_Tag_Junction__c junctionRec : [SELECT Release_Date__c, Tag__r.Name FROM EAW_Release_Date_Tag_Junction__c WHERE Release_Date__c = : releaseDateId]) {
            releaseDateNewTags.add(junctionRec.Tag__r.Name);
        }
        
        List<String> releaseDateNewNotes = new List<String>();
        
        for(Note noteRec: [Select ParentId, Title, Body From Note where ParentId = : releaseDateId]) {
        
            String newNote = noteRec.Title;
            if(String.isNotBlank(noteRec.Body)) {
                newNote = newNote + '-' + noteRec.Body;
            }
            releaseDateNewNotes.add(newNote);
        }
        
        return (new WrapperClass(releaseDates, releaseDateNewTags, releaseDateNewNotes));
    }
    
    public class WrapperClass {
    
        @AuraEnabled
        public List<SObject> records {get; set;}
        @AuraEnabled
        public List<String> tags {get; set;}
        @AuraEnabled
        public List<String> notes {get; set;}
        
        public WrapperClass(List<SObject> records, List<String> tags, List<String> notes) {
            
            this.records = records;
            this.tags = tags;
            this.notes = notes;
        }
    }
}