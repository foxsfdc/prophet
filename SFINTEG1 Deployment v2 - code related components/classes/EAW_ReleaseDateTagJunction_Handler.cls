public class EAW_ReleaseDateTagJunction_Handler {
    
    Set<Id> tagSet = new Set<Id>();
    Set<Id> releaseDateIdSet = new Set<Id>();
    Set<Id> releaseDateGuidelineIdSet = new Set<Id>();
    Set<Id> titleIdSet = new Set<Id>();
    
    public void reQualificationOfRules(List<EAW_Release_Date_Tag_Junction__c> newReleaseDateTags){
        try{
            if( newReleaseDateTags != NULL && newReleaseDateTags.size() == 1 ) {
                for(Integer i=0; i < newReleaseDateTags.size(); i++){
                    if( ! checkRecursiveData.bypassDateCalculation && newReleaseDateTags[i].Tag__c != NULL && newReleaseDateTags[i].Release_Date__c != NULL ) {
                        if(checkRecursiveData.isRecursiveData(newReleaseDateTags[i].Id)){
                            if( newReleaseDateTags[i].Tag__c != NULL ) tagSet.add(newReleaseDateTags[i].Tag__c);
                            if( newReleaseDateTags[i].Release_Date__c != NULL ) releaseDateIdSet.add(newReleaseDateTags[i].Release_Date__c);
                        }
                    }
                }
            }  
            if( releaseDateIdSet != NULL && releaseDateIdSet.size() > 0 ) filterAndProcessRulesForReQualification();
        } catch (Exception e){
            System.debug('<<--Exception-->>'+e.getMessage());
        }
    }
      
    //LRCC-1787
    public void updateOldTags( List<EAW_Release_Date_Tag_Junction__c> ReleaseDateTags ){
    
        Set<Id> releaseDateIds = new Set<Id>();
        Map<Id, List<String>> releaseDateIdTagsMap = new Map<Id, List<String>>();
        
        for(EAW_Release_Date_Tag_Junction__c releaseDateTag : ReleaseDateTags) {
            releaseDateIds.add(releaseDateTag.Release_Date__c);
        }
        system.debug('releaseDateIds:::'+releaseDateIds);
        
        for(EAW_Release_Date_Tag_Junction__c junctionRec : [SELECT Release_Date__c, Tag__r.Name FROM EAW_Release_Date_Tag_Junction__c WHERE Release_Date__c IN : releaseDateIds]) {
            
            if(! releaseDateIdTagsMap.containsKey(junctionRec.Release_Date__c)) {
                releaseDateIdTagsMap.put(junctionRec.Release_Date__c, new List<String>());
            }
            releaseDateIdTagsMap.get(junctionRec.Release_Date__c).add(junctionRec.Tag__r.Name);
        }
        system.debug('releaseDateIdTagsMap:::'+releaseDateIdTagsMap);
        
        List<EAW_Release_Date__c> updateReleaseDates = [SELECT OldTags__c, Tags_Changed_Dates__c, Tags_Changed_Users__c FROM EAW_Release_Date__c WHERE Id IN : releaseDateIds];
        
        for(EAW_Release_Date__c releaseDateRec : updateReleaseDates) {
            
            if(releaseDateRec.OldTags__c != null) {
                
                List<String> oldTagsValues = releaseDateRec.OldTags__c.split('&&&');
                
                if(oldTagsValues.size() == 5) {
                    oldTagsValues.remove(0);
                }
                
                if(releaseDateIdTagsMap.containsKey(releaseDateRec.Id)) {
                    oldTagsValues.add(String.join(releaseDateIdTagsMap.get(releaseDateRec.Id), ';'));
                } else {
                    oldTagsValues.add('null');
                }
                
                releaseDateRec.OldTags__c = String.join(oldTagsValues, '&&&');
            } else {
                
                if(releaseDateIdTagsMap.containsKey(releaseDateRec.Id)) {
                    releaseDateRec.OldTags__c = String.join(releaseDateIdTagsMap.get(releaseDateRec.Id), ';');
                } else {
                    releaseDateRec.OldTags__c = 'null';
                }
            }
            
            if(releaseDateRec.Tags_Changed_Dates__c != null) {
            
                List<String> oldValues = releaseDateRec.Tags_Changed_Dates__c.split('&&&');
                if(oldValues.size() == 5) {
                    oldValues.remove(0);
                }
                oldValues.add(String.valueOfGmt(system.now()));
                releaseDateRec.Tags_Changed_Dates__c = String.join(oldValues,'&&&');
            } else {
                releaseDateRec.Tags_Changed_Dates__c = String.valueOfGmt(system.now());
            }
            
            if(releaseDateRec.Tags_Changed_Users__c != null) {
            
                List<String> oldValues = releaseDateRec.Tags_Changed_Users__c.split('&&&');
                if(oldValues.size() == 5) {
                    oldValues.remove(0);
                }
                oldValues.add(UserInfo.getName());
                releaseDateRec.Tags_Changed_Users__c = String.join(oldValues,'&&&');
            } else {
                releaseDateRec.Tags_Changed_Users__c = UserInfo.getName();
            }
 
        }
        system.debug('updateReleaseDates:::'+updateReleaseDates);
        
        if(updateReleaseDates.isEmpty() == FALSE) {
            update updateReleaseDates;
        }
    }
    
    public void reQualificationOfRules(List<EAW_Release_Date_Tag_Junction__c> oldReleaseDateTags,List<EAW_Release_Date_Tag_Junction__c> newReleaseDateTags){
        
        try{
            if( newReleaseDateTags != NULL && newReleaseDateTags.size() == 1 ) {
                for(Integer i=0; i < newReleaseDateTags.size(); i++){
                    if( ! checkRecursiveData.bypassDateCalculation && newReleaseDateTags[i].Tag__c != oldReleaseDateTags[i].Tag__c || newReleaseDateTags[i].Release_Date__c != oldReleaseDateTags[i].Release_Date__c ){
                        if(checkRecursiveData.isRecursiveData(newReleaseDateTags[i].Id)){
                            if( newReleaseDateTags[i].Tag__c != NULL ) tagSet.add(newReleaseDateTags[i].Tag__c);
                            if( oldReleaseDateTags[i].Tag__c != NULL )  tagSet.add(oldReleaseDateTags[i].Tag__c);
                            if( newReleaseDateTags[i].Release_Date__c != NULL ) releaseDateIdSet.add(newReleaseDateTags[i].Release_Date__c);
                            if( oldReleaseDateTags[i].Release_Date__c != NULL ) releaseDateIdSet.add(oldReleaseDateTags[i].Release_Date__c);
                        }
                    }
                }
            }
            if( releaseDateIdSet != NULL && releaseDateIdSet.size() > 0 ) filterAndProcessRulesForReQualification();
        } catch (Exception e){
            System.debug('<<--Exception-->>'+e.getMessage());
        }
    }
    
    public void filterAndProcessRulesForReQualification(){
        
        system.debug('tagSet:::'+tagSet);
        system.debug('releaseDateIdSet:::'+releaseDateIdSet);
        
        if( releaseDateIdSet != NULL && releaseDateIdSet.size() > 0 ){
            for( EAW_Release_Date__c rd : [ Select Id, Name, Release_Date_Guideline__c, Title__c From EAW_Release_Date__c Where Id IN : releaseDateIdSet AND Release_Date_Guideline__c != NULL AND Title__c != NULL ] ){
                releaseDateGuidelineIdSet.add(rd.Release_Date_Guideline__c);
                titleIdSet.add(rd.Title__c);
            }
        }
        
        system.debug('releaseDateGuidelineIdSet:::'+releaseDateGuidelineIdSet);
        
        if( releaseDateGuidelineIdSet != NULL ) {
            
            List<String> tagNameList = new List<String>();
    
            if( tagSet != NULL && tagSet.size() > 0 ) {
                for( EAW_Tag__c relTag : [ Select Id, Name From EAW_Tag__c Where Id IN : tagSet ]){
                    String tagNameString = '\''+relTag.Name+'\'';
                    tagNameList.add(tagNameString);
                }
            }
            
            String tagType = 'Release Date Tag'; // It was used in dynamic soql query
            
            Set<Id> tempReleaseDateGuidelineIdSet = new Set<Id>();
            
            //Multi-Operand selection related changes
            EAW_DownstreamRuleGuidelinesFindUtil dependentGuidelineFindUtil = new EAW_DownstreamRuleGuidelinesFindUtil();
            tempReleaseDateGuidelineIdSet = dependentGuidelineFindUtil.findDependentReleaseDateGuidelineToProcessUsingTags( releaseDateGuidelineIdSet, tagNameList, tagType );
            
            if( tempReleaseDateGuidelineIdSet != NULL && tempReleaseDateGuidelineIdSet.size() > 0 ) {
                
                /*EAW_QueueableDateCalculationClass caclQueueJob = new EAW_QueueableDateCalculationClass();
                caclQueueJob.releaseDateGuidelineIdSet = tempReleaseDateGuidelineIdSet;
                caclQueueJob.filteredTitleIdSet = titleIdSet;
                caclQueueJob.windowGuidelineIdSet = Null;
                String jobId = System.enqueueJob(caclQueueJob);
                System.debug(':::: Job Id ::::'+ jobId);*/
                
                //LRCC-1478
                EAW_RDGDateCalculationBatchClass releaseDateBatch = new EAW_RDGDateCalculationBatchClass(tempReleaseDateGuidelineIdSet);
                releaseDateBatch.titleIdSet = titleIdSet;
                releaseDateBatch.windowGuidelineIdSet = NULL;
                ID batchprocessid = Database.executeBatch(releaseDateBatch, 200);
            }
            
        } 
    }
}