/**
 *  Test class for EAW_ReleaseDateGuidelineController
 * 
 */
@isTest
private class EAW_ReleaseDateGuidelineController_Test {

    @testSetup static void setupTestData() {
        // Call appropriate methods in EAW_TestDataFactory to create test records
        List<EAW_Release_Date_Type__c> releaseDateTypeList = EAW_TestDataFactory.createReleaseDateType(1, true);
        List<EAW_Customer__c> customerList = EAW_TestDataFactory.createEAWCustomer(1,true);
        EAW_TestDataFactory.createEAWTitle(1, true);
        //EAW_TestDataFactory.createEAWReleaseDateGuideline( releaseDateTypeList[0].Id, customerList[0].Id, 2, true, customerList[0].Name);
        EAW_TestDataFactory.createReleaseDateGuideline(1, true, 'Episode', new List<String>{'Theatrical'}, 'Albania', 'Albanian');
    }
    
    @isTest static void testEAWReleaseDateGuideLineMethod(){
        
        Map<String,String>  objFieldSetMap = new Map<String, String>();
        objFieldSetMap.put('EAW_Release_Date_Guideline__c','Search_Result_Fields');
        objFieldSetMap.put('EAW_Title__c','Release_Date_Page_Fields');
        EAW_ReleaseDateGuidelineController.getFields(objFieldSetMap);
        List<EAW_Customer__c> cusList = [ Select Id From EAW_Customer__c ];
        EAW_LookupSearchResult lookup = new EAW_LookupSearchResult();
        lookup.title = 'Episode';
        EAW_LookupSearchResult custlookup = new EAW_LookupSearchResult();
        custlookup.title = 'Episode : Theretical';
        
        List<EAW_Release_Date_Type__c> relDateList = [ Select Id From EAW_Release_Date_Type__c];
        EAW_ReleaseDateGuidelineController.getRecords( objFieldSetMap, JSON.Serialize(new List<EAW_LookupSearchResult> {lookup}), JSON.Serialize(new List<EAW_LookupSearchResult> {lookup}),  JSON.serialize(new List<EAW_LookupSearchResult> {lookup}), JSON.serialize(new List<EAW_LookupSearchResult> {custlookup}), JSON.serialize(new List<EAW_LookupSearchResult> {lookup}), 100, 1, JSON.serialize(new Map<Integer,Set<Id>>{1 => new set<Id>()}), JSON.serialize(new Map<String,String>{'' => ''}));
    }
    @isTest static void testEAWReleaseDateGuideLineMethodAll(){
        
        Map<String,String>  objFieldSetMap = new Map<String, String>();
        objFieldSetMap.put('EAW_Release_Date_Guideline__c','Search_Result_Fields');
        objFieldSetMap.put('EAW_Title__c','Release_Date_Page_Fields');
        EAW_ReleaseDateGuidelineController.getFields(objFieldSetMap);
        List<EAW_Customer__c> cusList = [ Select Id From EAW_Customer__c ];
        EAW_LookupSearchResult lookup = new EAW_LookupSearchResult();
        lookup.title = 'All';
        EAW_LookupSearchResult custlookup = new EAW_LookupSearchResult();
        custlookup.title = 'Episode : Theretical';
        List<EAW_Release_Date_Guideline__c> RDGList = [SELECT Id FROM EAW_Release_Date_Guideline__c];
        List<EAW_Release_Date_Type__c> relDateList = [ Select Id From EAW_Release_Date_Type__c];
        EAW_ReleaseDateGuidelineController.getRecords( objFieldSetMap, JSON.Serialize(new List<EAW_LookupSearchResult> {lookup}), JSON.Serialize(new List<EAW_LookupSearchResult> {lookup}),  JSON.serialize(new List<EAW_LookupSearchResult> {lookup}), JSON.serialize(new List<EAW_LookupSearchResult> {custlookup}), JSON.serialize(new List<EAW_LookupSearchResult> {lookup}), 1, 1, JSON.serialize(new Map<Integer,Set<Id>>{1 => new set<Id>{RDGList[0].Id}}), '{"selectedCol":"Status__c","selectedAlpha":"F"}');
        EAW_ReleaseDateGuidelineController.getRecords( objFieldSetMap, JSON.Serialize(new List<EAW_LookupSearchResult> {lookup}), JSON.Serialize(new List<EAW_LookupSearchResult> {lookup}),  JSON.serialize(new List<EAW_LookupSearchResult> {lookup}), JSON.serialize(new List<EAW_LookupSearchResult> {custlookup}), JSON.serialize(new List<EAW_LookupSearchResult> {lookup}), 100, 2, JSON.serialize(new Map<Integer,Set<Id>>{1 => new set<Id>{RDGList[0].Id}}), '{"selectedCol":"Status__c","selectedAlpha":"F"}');
        EAW_ReleaseDateGuidelineController.getRecords( objFieldSetMap, JSON.Serialize(new List<EAW_LookupSearchResult> {lookup}), JSON.Serialize(new List<EAW_LookupSearchResult> {lookup}),  JSON.serialize(new List<EAW_LookupSearchResult> {lookup}), JSON.serialize(new List<EAW_LookupSearchResult> {custlookup}), JSON.serialize(new List<EAW_LookupSearchResult> {lookup}), 100, 2, JSON.serialize(new Map<Integer,Set<Id>>{1 => new set<Id>{RDGList[0].Id}, 2 => new set<Id>{RDGList[0].Id}}), '{"selectedCol":"Status__c","selectedAlpha":"F"}');
    }
    
     @isTest static void testEAWReleaseDateGuideLinesave(){
     
        List<EAW_Release_Date_Type__c> releaseDateTypeList = [SELECT id, Name FROM EAW_Release_Date_Type__c LIMIT 1];
        List<EAW_Customer__c> customerList = [SELECT id, Name FROM EAW_Customer__c LIMIT 1];
        
        List<EAW_Release_Date_Guideline__c> releaseDateGuideLineList = EAW_TestDataFactory.createReleaseDateGuideline(1, true, 'Feature', new List<String>{'Initial HE Release'}, 'Afghanistan', 'Afrikaans');
        //EAW_TestDataFactory.createEAWReleaseDateGuideline( releaseDateTypeList[0].Id, customerList[0].Id, 1,True, customerList[0].Name);
        
        try {
         
            EAW_ReleaseDateGuidelineController.saveReleaseDateGuidelinesChanges(releaseDateGuideLineList );
        } catch(Exception ex) {
        }
     }
     
     @isTest static void deleteReleaseDateGuidelines_Test() {    
        
        List<EAW_Release_Date_Type__c> releaseDateTypeList = [SELECT id, Name FROM EAW_Release_Date_Type__c LIMIT 1];
        List<EAW_Customer__c> customerList = [SELECT id, Name FROM EAW_Customer__c LIMIT 1];
        List<String> recordIdList = new List<String>();
        List<EAW_Release_Date_Guideline__c> releaseDateGuideLineList = EAW_TestDataFactory.createReleaseDateGuideline(1, true, 'Format', new List<String>{'HV-Retail'}, 'Algeria', 'Amharic');
        //EAW_TestDataFactory.createEAWReleaseDateGuideline( releaseDateTypeList[0].Id, customerList[0].Id, 1,True,customerList[0].Name);
        
        for(EAW_Release_Date_Guideline__c reIns : releaseDateGuideLineList ) {
            recordIdList.add(reIns.Id);            
        }
         
         EAW_ReleaseDateGuidelineController.deleteReleaseDateGuidelines(recordIdList);
         
     }
     @isTest static void testEAWReleaseDategetNotes() {
 
        List<EAW_Release_Date_Type__c> releaseDateTypeList = [SELECT id, Name FROM EAW_Release_Date_Type__c LIMIT 1];
        List<EAW_Customer__c> customerList = [SELECT id, Name FROM EAW_Customer__c LIMIT 1];
        List<String> recordIdList = new List<String>();
        List<EAW_Release_Date_Guideline__c> releaseDateGuideLineList = EAW_TestDataFactory.createReleaseDateGuideline(1, true, 'Mini-Series', new List<String>{'HV-Rental'}, 'Andorra', 'Arabic');
        //EAW_TestDataFactory.createEAWReleaseDateGuideline( releaseDateTypeList[0].Id, customerList[0].Id, 1,True,customerList[0].Name);
         
        EAW_ReleaseDateGuidelineController.getNotes(String.Valueof(releaseDateGuideLineList[0].Id));
     }
     
     @isTest static void testEAWReleaseDateinsertNotes() {
     
        List<EAW_Release_Date_Type__c> releaseDateTypeList = [SELECT id, Name FROM EAW_Release_Date_Type__c LIMIT 1];
        List<EAW_Customer__c> customerList = [SELECT id, Name FROM EAW_Customer__c LIMIT 1];
        List<String> recordIdList = new List<String>();
        List<EAW_Release_Date_Guideline__c> releaseDateGuideLineList = EAW_TestDataFactory.createReleaseDateGuideline(1, true, 'Movie of the Week', new List<String>{'Initial Digital Release'}, 'Angola', 'Armenian');
        //EAW_TestDataFactory.createEAWReleaseDateGuideline( releaseDateTypeList[0].Id, customerList[0].Id, 1,True,customerList[0].Name);
        for(EAW_Release_Date_Guideline__c release : releaseDateGuideLineList ) {
            
            recordIdList.add(release.Id);
        }
        
         EAW_ReleaseDateGuidelineController.insertNotes('Test Note', recordIdList, 'Title');
     }
     
     @isTest static void testEAWReleaseDatemassupdate() {
             
         List<EAW_Customer__c> cusList = [ Select Id From EAW_Customer__c ];
         
         EAW_Customer__c customerIns = new EAW_Customer__c(Name='Test customer record', Media__c = 'Free TV', Customer_Id__c = '740200');
         insert customerIns;
         
         List<Id> rgIdList = new List<Id>();
         
         List<EAW_Release_Date_Guideline__c> releaseDateGuideLineList = EAW_TestDataFactory.createReleaseDateGuideline(1, true, 'New Media Film', new List<String>{'EST-HD'}, 'Anguilla', 'Assamese');
         
         for(EAW_Release_Date_Guideline__c releaseIns : releaseDateGuideLineList) {             
             rgIdList.add(releaseIns.Id);
         }
         try {
             EAW_ReleaseDateGuidelineController.massUpdateReleaseDateGuidelines(rgIdList, 'Australia', 'Chinese', 'Test customer record', 'Format', 'HV-Retail', 'Test body', 'Test title');
         }
         catch(Exception ex) {}
    }   
    
    @isTest static void testfetchUserMethod() {
        
        EAW_ReleaseDateGuidelineController.UserInformation userInfo = EAW_ReleaseDateGuidelineController.fetchUser();
        system.assertEquals(userInfo != null, true);
    }
    
    @isTest static void testgetContentDocumentLinkMethod() {
    
        List<EAW_Release_Date_Guideline__c> releaseDateGuideLineList = EAW_TestDataFactory.createReleaseDateGuideline(1, true, 'New Media Film', new List<String>{'Initial Digital Release'}, 'Andorra', 'Assamese');
        
        ContentVersion contentVersion = new ContentVersion(
          Title = 'Penguins',
          PathOnClient = 'Penguins.jpg',
          VersionData = Blob.valueOf('Test Content'),
          IsMajorVersion = true
        );
        insert contentVersion;    
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        
        //create ContentDocumentLink  record 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = releaseDateGuideLineList[0].id;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        insert cdl;
    
        EAW_ReleaseDateGuidelineController.AttachmentWrapper attachment = EAW_ReleaseDateGuidelineController.getContentDocumentLink(releaseDateGuideLineList[0].Id);
        system.assertEquals(attachment != null, true);
    }
}