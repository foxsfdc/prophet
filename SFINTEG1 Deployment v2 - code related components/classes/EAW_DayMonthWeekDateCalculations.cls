public with sharing class EAW_DayMonthWeekDateCalculations {
    
    /*
    This class was developed for : LRCC-1360 calculation related logic which was used in both Window and Release date claculations
    */
    public Date getCalculatedDate( Date calculatedDate, EAW_Rule_Detail__c ruleDetail ){
        
        Date finalCalculatedDate;
        if( ruleDetail.Condition_Date_Duration__c == 'Day' ){
            DateTime tempCalculatedDate;
            if( ! String.isBlank(ruleDetail.Day_of_the_Week__c) && ruleDetail.Condition_Criteria_Amount__c != NULL && ruleDetail.Condition_Criteria_Amount__c != 0 ){
                tempCalculatedDate = getCalculatedDate( calculatedDate, ruleDetail.Day_of_the_Week__c, Integer.valueOf(ruleDetail.Condition_Criteria_Amount__c), 1 );
            } else if( ! String.isBlank(ruleDetail.Day_of_the_Week__c) ) {
                tempCalculatedDate = getNearestSelectedDay( calculatedDate,  ruleDetail.Day_of_the_Week__c );
            }
            finalCalculatedDate = tempCalculatedDate.Date();
        } else if( ruleDetail.Condition_Date_Duration__c == 'Week' ){
            DateTime tempCalculatedDate = getCalculatedDate( calculatedDate, 'SUNDAY', Integer.valueOf(ruleDetail.Condition_Criteria_Amount__c), 1 );
            finalCalculatedDate = tempCalculatedDate.Date();
        } else if( ruleDetail.Condition_Date_Duration__c == 'Month' ){
            finalCalculatedDate = getCalculatedDate( calculatedDate, Integer.valueOf(ruleDetail.Condition_Criteria_Amount__c) );
        }
        return finalCalculatedDate;
        
    }
    
    public DateTime getCalculatedDate( Date calcDate, String selectedDayofWeek, Integer selectedWeek, Integer monthToAdd ) {
        
        DateTime checkDate = DateTime.newInstance(Date.newInstance(calcDate.Year(), calcDate.Month(), 1), Time.newInstance(0, 0, 0, 0));
        checkDate = checkDate.addHours(1);
        
        if( monthToAdd > 1 ) {
            checkDate = checkDate.addMonths(1);
        } else {
            
            //System.debug('::::calcDate Hour::: ' + getPacificOffset(calcDate) + ' ::: calcDate ::: ' + calcDate);
            //System.debug('::::checkDate Hour::: ' + getPacificOffset(checkDate) + ' ::: checkDate ::: ' + checkDate);
            
            Integer difference;
            if(getPacificOffset(calcDate) < getPacificOffset(checkDate)){
                difference = getPacificOffset(checkDate) - getPacificOffset(calcDate);                              
            }else{
                difference = getPacificOffset(calcDate) - getPacificOffset(checkDate);                
            }
            checkDate = checkDate.addHours(difference);
            
            //System.debug('difference ::: ' + difference);
        }
        
        DateTime startDateofNextMonth = checkDate.addMonths(monthToAdd);
        startDateofNextMonth = startDateofNextMonth.addHours(1);
        
        while( checkDate < startDateofNextMonth ){
            if( checkDate.format('EEEE') == selectedDayofWeek ){
                //System.debug('::::checkDate before add 7 days:::'+checkDate);
                checkDate = checkdate.addDays(7*(selectedWeek-1));
                break;
            } else {
                checkDate = checkDate.addDays(1);
            }
        }
        //System.debug('::::calcDate:::'+calcDate);
        //System.debug('::::checkDate:::'+checkDate);
        
        if( checkDate < calcDate ) {
            checkDate = getCalculatedDate( calcDate, selectedDayofWeek, selectedWeek, 2);
        } 
               
        return checkDate; 
    }
    
    public DateTime getNearestSelectedDay(Date calcDate, String selectedDayofWeek) {
        
        calcDate = calcDate.addDays(1);
            
        DateTime checkDate = DateTime.newInstance(calcDate, Time.newInstance(0, 0, 0, 0));
        
        DateTime addedByOneWeek = checkDate.addDays(7);
        //System.debug('checkDate:::' + checkDate + ' :::addedByOneWeek::: ' + addedByOneWeek);
        
        while( checkDate < addedByOneWeek ){
            
            //System.debug('checkDate.format(EEEE):::: ' + checkDate.format('EEEE') + ' :::selectedDayofWeek::: ' + selectedDayofWeek);
            
            if( checkDate.format('EEEE') == selectedDayofWeek){
                break;
            } else {
                checkDate = checkDate.addDays(1);
            }
        }  
        //System.debug(':::result::: ' + checkDate);
        return checkDate;
    } 
    
    public Date getCalculatedDate(Date calcDate, Integer daysToAdd ) { 
        Date finalCalcDate = calcDate.toStartOfMonth().addDays(daysToAdd-1);
        if(finalCalcDate < calcDate){
            finalCalcDate = finalCalcDate.addMonths(1);
        }
        return finalCalcDate; 
    }
    
    public static Integer getPacificOffset(DateTime calcDateTime){
        return TimeZone.getTimeZone('America/Los_Angeles').getOffset(calcDateTime) /
            (1000 * 60 * 60);
    } 
}