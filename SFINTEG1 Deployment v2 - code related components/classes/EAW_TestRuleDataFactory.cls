/*Sample Rule String - '{"sObjectType":"EAW_Rule__c","Nested__c":true,"Condition_Type__c":"case"}'
 
Sample Rule Detail String - 
'[{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Condition_Type__c":"Case then",
"Condition_Set_Operator__c":"IN","Condition_Field__c":"Release Date Status","Condition_Criteria__c":"Estimated",
"Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature","Conditional_Operand_Id__c":"aA73D000000CyJjSAK",
"Multi_conditional_operands__c":false},"dateCalcs":[],"nodeCalcs":[{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c",
"Rule_Order__c":1,"Nest_Order__c":2,"Condition_Type__c":"Else/If","Condition_Timeframe__c":"True_Earliest"},"dateCalcs":[],
"nodeCalcs":[{"ruleDetail":{"sObjectType":"EAW_Rule_Detail__c","Parent_Condition_Type__c":"Condition","Condition_Type__c":"If",
"Condition_Set_Operator__c":"IN","Condition_Field__c":"Release Date Status","Criteria__c":"","Condition_Operand_Details__c":"EST-HD / Australia / No Specific Language / Feature",
"Conditional_Operand_Id__c":"aA73D0000000K1TSAU","Multi_conditional_operands__c":false,"Condition_Criteria__c":"Estimated"},
"dateCalcs":[],"nodeCalcs":[{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Condition_Type__c":"If","Condition_Timeframe__c":"True_Earliest"},"dateCalcs":[{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,
"Conditional_Operand_Id__c":"aA73D0000008di0SAA","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Antigua / Nauruan / Feature"}],"nodeCalcs":[]},
{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Rule_Order__c":2,"Nest_Order__c":1,"Condition_Type__c":"Else/If","Conditional_Operand_Id__c":"aA73D0000008di0SAA","Condition_Met_Months__c":"2",
"Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Antigua / Nauruan / Feature"},"dateCalcs":[],"nodeCalcs":[]}]}]}]},{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Rule_Order__c":2,"Nest_Order__c":1,"Condition_Type__c":"Otherwise"},"dateCalcs":[],
"nodeCalcs":[{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":2,"Condition_Type__c":"Else/If","Condition_Timeframe__c":"True_Latest"},"dateCalcs":[],"nodeCalcs":[{"ruleDetail":{"sObjectType":"EAW_Rule_Detail__c","Parent_Condition_Type__c":"Case","Condition_Type__c":"Case"},"dateCalcs":[],"nodeCalcs":[{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,
"Condition_Type__c":"Case then","Condition_Set_Operator__c":"IN","Condition_Field__c":"Release Date Status","Condition_Criteria__c":"Firm","Condition_Operand_Details__c":"EST-HD / Australia / No Specific Language / Feature","Conditional_Operand_Id__c":"aA73D0000000K1TSAU","Multi_conditional_operands__c":false},
"dateCalcs":[],"nodeCalcs":[{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":2,"Condition_Type__c":"","Conditional_Operand_Id__c":"aA73D000000CyJjSAK","Condition_Met_Months__c":"2","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"},
"dateCalcs":[],"nodeCalcs":[]}]},{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Rule_Order__c":2,"Nest_Order__c":1,"Condition_Type__c":"Otherwise"},"dateCalcs":[],"nodeCalcs":[{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":2,"Condition_Type__c":"Else/If","Condition_Timeframe__c":"True_Earliest"},
"dateCalcs":[],"nodeCalcs":[{"ruleDetail":{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":2,"Nest_Order__c":3,"Condition_Timeframe__c":"True_Latest"},"dateCalcs":[{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":2,"Nest_Order__c":3,"Conditional_Operand_Id__c":"aA73D000000CyJjSAK",
"Condition_Met_Months__c":"2","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"}],"nodeCalcs":[]}]}]}]}]}]}]'
*/

@isTest
public class EAW_TestRuleDataFactory {
	
    public static void saveRuleAndRuleDetails(String rule,String ruleAndRuleDetailNodes,Boolean isRDG,String guidelineId,String wgRuleDate,String conditionalOperandId){
       
        Id ruleRecordTypeId = Schema.SObjectType.EAW_Rule__c.getRecordTypeInfosByName().get('Release Date Calculation').getRecordTypeId();
        Id ruleDetailRecordTypeId = Schema.SObjectType.EAW_Rule_Detail__c.getRecordTypeInfosByName().get('Release Date Calculation').getRecordTypeId();
        if(!isRDG){
            ruleRecordTypeId = Schema.SObjectType.EAW_Rule__c.getRecordTypeInfosByName().get('Window Date Calculation').getRecordTypeId();
        	ruleDetailRecordTypeId = Schema.SObjectType.EAW_Rule_Detail__c.getRecordTypeInfosByName().get('Window Date Calculation').getRecordTypeId();
        }
        EAW_Rule__c newRule = new EAW_Rule__c();
        if(rule != null){
            newRule = (EAW_Rule__c)JSON.deserialize(rule,EAW_Rule__c.class);
        }
        newRule.RecordTypeId = ruleRecordTypeId;
        if(newRule.Condition_Type__c != null && newRule.Condition_Type__c == 'If'){
            if(conditionalOperandId != null)newRule.Conditional_Operand_Id__c = conditionalOperandId;
        }
        if(isRDG){
            newRule.Release_Date_Guideline__c = guidelineId;
        } else{
            newRule.Window_Guideline__c = guidelineId;
            newRule.Date_To_Modify__c = wgRuleDate;
        }
        //system.debug('newRule::::'+newRule);
        upsert newRule;
        if(ruleAndRuleDetailNodes != null){
            List<ruleDetailNodeWrapper> newruleDetailNodeWrapper = new List<ruleDetailNodeWrapper>();
            newruleDetailNodeWrapper = (List<ruleDetailNodeWrapper> )JSON.deserialize(ruleAndRuleDetailNodes,List<ruleDetailNodeWrapper> .class);
            List<EAW_Rule_Detail__c> parentRuleDetailList = new List<EAW_Rule_Detail__c>();
            for(ruleDetailNodeWrapper newruleDetail : newruleDetailNodeWrapper){
                EAW_Rule_Detail__c parentRuleDetail = newruleDetail.ruleDetail;
                parentRuleDetail.Rule_No__c = newRule.Id;
                parentRuleDetail.RecordTypeId = ruleDetailRecordTypeId;
                if(conditionalOperandId != null){
                    parentRuleDetail.Conditional_Operand_Id__c = conditionalOperandId;
                }
                parentRuleDetailList.add(parentRuleDetail);
            }       
            system.debug('parentRuleDetailList::::'+parentRuleDetailList);
            upsert parentRuleDetailList;
            List<EAW_Rule_Detail__c> recursiveRuleDetailList = new List<EAW_Rule_Detail__c>();
            List<EAW_Rule_Detail__c> ruleDetailList = new List<EAW_Rule_Detail__c>();
            for(ruleDetailNodeWrapper newruleDetailNode : newruleDetailNodeWrapper){
                if(newruleDetailNode.dateCalcs != null && newruleDetailNode.nodeCalcs != null){
                    recursiveRuleDetailList = new List<EAW_Rule_Detail__c>();
                    ruleDetailList.addAll(recursiveNodeSave(ruleDetailRecordTypeId,recursiveRuleDetailList,newRule.Id,newruleDetailNode.ruleDetail.Id,newruleDetailNode.dateCalcs,newruleDetailNode.nodeCalcs,conditionalOperandId));            
                }
            }       
            system.debug('ruleDetailList::::'+ruleDetailList);
            upsert ruleDetailList;
        }
        /*if(ruleDetailList != null && ruleDetailList.size() >= 1){
            newRule.Nested_Times__c = ruleDetailList[ruleDetailList.size()-1].Nest_Order__c;
        }
        else{
            newRule.Nested_Times__c = 1;
        }
        system.debug('newRule::::'+newRule);
        update newRule;*/
    }
    
    private static List<EAW_Rule_Detail__c> recursiveNodeSave(Id ruleDetailRecordTypeId,List<EAW_Rule_Detail__c> ruleDetailList,Id ruleId,Id parentRuleDetailId,List<EAW_Rule_Detail__c> dateCalcs,List<ruleDetailNodeWrapper> nodeCalcs,String conditionalOperandId){
        
        for(EAW_Rule_Detail__c dateCalcRuleDetail : dateCalcs){
            dateCalcRuleDetail.Rule_No__c = ruleId;
            dateCalcRuleDetail.RecordTypeId = ruleDetailRecordTypeId;
            dateCalcRuleDetail.Parent_Rule_Detail__c = parentRuleDetailId;
            if(conditionalOperandId != null){
                dateCalcRuleDetail.Conditional_Operand_Id__c = conditionalOperandId;
            }
            ruleDetailList.add(dateCalcRuleDetail);
        }       
        
        List<EAW_Rule_Detail__c> childComeParentList = new List<EAW_Rule_Detail__c>();
        for(ruleDetailNodeWrapper ruleDetailNode : nodeCalcs){          
            EAW_Rule_Detail__c childComeParent = ruleDetailNode.ruleDetail;
            childComeParent.Rule_No__c = ruleId;
            childComeParent.RecordTypeId = ruleDetailRecordTypeId;
            childComeParent.Parent_Rule_Detail__c = parentRuleDetailId;
            if(conditionalOperandId != null){
                childComeParent.Conditional_Operand_Id__c = conditionalOperandId;
            }
            childComeParentList.add(childComeParent);           
        }
        upsert childComeParentList;
        for(ruleDetailNodeWrapper ruleDetailNode : nodeCalcs){
            recursiveNodeSave(ruleDetailRecordTypeId,ruleDetailList,ruleId,ruleDetailNode.ruleDetail.Id,ruleDetailNode.dateCalcs,ruleDetailNode.nodeCalcs,conditionalOperandId);
        }
        return ruleDetailList;
    }
    public static void savePgQualifiers(String rule,String ruleDetailNodes,String guidelineId){
        
        Id ruleRecordTypeId = Schema.SObjectType.EAW_Rule__c.getRecordTypeInfosByName().get('Qualifier').getRecordTypeId();
        Id ruleDetailRecordTypeId = Schema.SObjectType.EAW_Rule_Detail__c.getRecordTypeInfosByName().get('Qualifier').getRecordTypeId();
        EAW_Rule__c newRule = new EAW_Rule__c();
        if(rule != null){
            newRule = (EAW_Rule__c)JSON.deserialize(rule,EAW_Rule__c.class);
        }
        newRule.RecordTypeId = ruleRecordTypeId;
        newRule.Plan_Guideline__c = guidelineId;
        upsert newRule;
        EAW_Rule_Detail__c ruleDetail = new EAW_Rule_Detail__c();
        if(ruleDetailNodes != null){
            ruleDetail = (EAW_Rule_Detail__c)JSON.deserialize(ruleDetailNodes,EAW_Rule_Detail__c.class);
        }
        ruleDetail.RecordTypeId = ruleDetailRecordTypeId;
        ruleDetail.Rule_No__c = newRule.Id;
        upsert ruleDetail;
    }
    public class ruleDetailNodeWrapper{
        public EAW_Rule_Detail__c ruleDetail;
        public List<EAW_Rule_Detail__c> dateCalcs;
        public List<ruleDetailNodeWrapper> nodeCalcs;
    }
}