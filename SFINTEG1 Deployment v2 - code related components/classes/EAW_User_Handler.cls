public with sharing class EAW_User_Handler {
    public void addNewUserToGroup(List<User> userList){
    	if(userList!=null && !userList.isempty()){
	    	list<GroupMember> finalGroupMembers = new list<GroupMember>();
	    	Set<id> profileIds = new Set<id>();
	    	for(User u:userList){
	    		profileIds.add(u.ProfileId);
	    	}
	    	map<id,profile> idProfileMap = new map<id,profile>([select ID,Name from Profile where id in: profileIds]);
	    	for(User u:userList){
	    		if('EAW Manager'.equalsignorecase(idProfileMap.get(u.ProfileId).Name)){
		    		Group eawManagerUser = [SELECT DeveloperName,Id,Name,Type FROM Group WHERE Name = 'EAW Manager Group'];
		    		GroupMember gm = new GroupMember();
		    		gm.GroupId=eawManagerUser.id;
		    		gm.UserOrGroupId =u.id;
		    		finalGroupMembers.add(gm);
	    		}
    		}
    		if(!finalGroupMembers.isEmpty())insert finalGroupMembers;
    	}
    }
}