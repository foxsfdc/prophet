@isTest
private class EAW_ReleaseDateController_Test {

    @testSetup static void setupTestData() {
        // Call appropriate methods in EAW_TestDataFactory to create test records
        EAW_TestDataFactory.createReleaseDate(1, true);
        EAW_TestDataFactory.createTag(1, True,'Release Date Tag');
        //1250-Replace Title EDM with Title Attribute
        //EAW_TestDataFactory.createEDMTitle(2,True);
        EAW_TestDataFactory.createReleaseDateTagJunction(2, True);
        EAW_TestDataFactory.createReleaseDateGuideline (1,true, 'Direct to Video', new List<String>{'EST-HD'},'Afghanistan','Afrikaans');
        
    }
    
    @isTest static void testEAWPlanSearch(){
    
        List<EAW_Release_Date__c> releaseDateList = [SELECT Id, Name,Status__c FROM EAW_Release_Date__c LIMIT 1];
        List<EAW_Tag__c> tagList = [SELECT Id, Name FROM EAW_Tag__c LIMIT 2];
        List<EAW_Title__c> titleAttributeList = [SELECT Id, Name FROM EAW_Title__c];
        List<String> releaseDateIdList = new List<String>();
        for(EAW_Release_Date__c releaseIns : releaseDateList ) {
            releaseDateIdList.add(releaseIns.Id);            
        }
        List<String> tagIdList = new List<String>();
        for(EAW_Tag__c tagIns : tagList ) {
            
            tagIdList.add(tagIns.Id);
        }
        
        //Map<String, List<SObject>> massList = EAW_ReleaseDateController.massUpdate(releaseDateIdList, 'Estimated', 'Temporary', '02/10/2017', tagIdList, true, true, String.ValueOf(titleAttributeList[0].Id), 'Test Records');
        
        //System.AssertEquals(False, massList.isEmpty());
    }
    
    @isTest static void testEAWPlandeleteReleaseDates() {
        
        List<EAW_Release_Date__c> releaseDateTagJunctionList = [SELECT Id, Name FROM EAW_Release_Date__c LIMIT 1];
        List<EAW_Release_Date_Tag_Junction__c> releaseDateList = [SELECT Id, Name FROM EAW_Release_Date_Tag_Junction__c LIMIT 2];
        List<String> releaseDateIdList = new List<String>();
        for(EAW_Release_Date__c releaseDatIns :  releaseDateTagJunctionList ) {
            releaseDateIdList.add(releaseDatIns.Id);            
        }
        
       
        List<EAW_Release_Date_Guideline__c> rdgList = [SELECT Id FROM EAW_Release_Date_Guideline__c LIMIT1];
        releaseDateTagJunctionList[0].Release_Date_Guideline__c  = rdgList[0].Id;
        update releaseDateTagJunctionList;
        
        String newruleContent = '{"sObjectType":"EAW_Rule__c","Nested__c":true}';
        String newruleDetailContent = '[{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Condition_Timeframe__c":"True_Earliest"},"dateCalcs":[{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Conditional_Operand_Id__c":"aA73D000000CyJjSAK","Condition_Met_Months__c":"2","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"},{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Conditional_Operand_Id__c":"aA73D000000CyJjSAK","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"}],"nodeCalcs":[{"ruleDetail":{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":2,"Condition_Timeframe__c":"True_Earliest"},"dateCalcs":[{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":2,"Conditional_Operand_Id__c":"aA73D000000CyJjSAK","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"}],"nodeCalcs":[]}]}]';
        EAW_TestRuleDataFactory.saveRuleAndRuleDetails(newruleContent,newruleDetailContent,true,rdgList[0].Id,'Start Date',String.valueOf(rdgList[0].Id));
        
        EAW_ReleaseDateController.deleteReleaseDates(releaseDateIdList);
        List<EAW_Release_Date_Tag_Junction__c> obtainedList = [SELECT Id, Tag__c, Release_Date__c FROM EAW_Release_Date_Tag_Junction__c];
        System.assertEquals(2, obtainedList.size());
    }
    
    @isTest static void getFields_Test() {
        
        Map<String, String> objectDateMap = new Map<String, String>{'EAW_Release_Date__c' => 'Customer__c'};
        
        EAW_ReleaseDateController.getFields(objectDateMap );
    }
    
    @isTest static void saveReleaseDateRecords_Test() {
        
        List<EAW_Tag__c> tagList = [SELECT Id, Name FROM EAW_Tag__c  LIMIT 1];
        //LRCC-1560 - Replace Customer lookup field with Customer text field
        //List<EAW_Release_Date__c> releaseDateList = [SELECT Id, Name,Customer__c,EAW_Release_Date_Type__c  FROM EAW_Release_Date__c LIMIT 1];
        List<EAW_Release_Date_Guideline__c> rdgList = [SELECT Id FROM EAW_Release_Date_Guideline__c LIMIT1];
        List<EAW_Release_Date__c> releaseDateList = [SELECT Id, Name,Customers__c,EAW_Release_Date_Type__c,Status__c,Manual_Not_Released_Overridden__c  FROM EAW_Release_Date__c LIMIT 1];

        //String recordsMap = '{"releaseDateList[0].Id" : "tagList[0].Id"}';
        
        //update releaseDateList ;
        releaseDateList[0].Status__c = 'Firm';
        releaseDateList[0].Release_Date_Guideline__c = rdgList[0].Id;
        Map<Id, List<Id>> testRecordMap = new Map<Id, List<Id>>();
        //testRecordMap.put(releaseDateList[0].Id,new  List<Id>{tagList[0].Id});
        
        List<EAW_Release_Date_Tag_Junction__c> rdTagJunc = EAW_TestDataFactory.createReleaseDateTagJunction(1,false);
        rdTagJunc[0].Release_Date__c = releaseDateList[0].Id;
        rdTagJunc[0].Tag__c = tagList[0].Id;
        insert rdTagJunc;
        
        List<EAW_Tag__c> tagList2 = EAW_TestDataFactory.createTag(1,false,'Release Date Tag');
        tagList2[0].Name ='testing 3';
        insert tagList2;
        testRecordMap.put(releaseDateList[0].Id,new  List<Id>{tagList2[0].Id});
        
        List<EAW_Release_Date__c> releaseList = EAW_ReleaseDateController.saveReleaseDateRecords(releaseDateList, JSON.Serialize(testRecordMap));
        
        System.AssertEquals(1, releaseList.size());
    }
    
    @isTest 
    static void insertNoteRecords_Test() {
    
        List<EAW_Release_Date_Guideline__c> rdgList = [SELECT Id FROM EAW_Release_Date_Guideline__c LIMIT1];
        List<EAW_Release_Date__c> releaseDateList = [SELECT Id, Name,Customers__c,EAW_Release_Date_Type__c,Status__c,Manual_Not_Released_Overridden__c  FROM EAW_Release_Date__c LIMIT 1];
        releaseDateList[0].Release_Date_Guideline__c  = rdgList[0].Id;
        update releaseDateList ;
        EAW_ReleaseDateController.insertNoteRecords(new List<Id>{releaseDateList[0].Id},'test','test','append');
        EAW_ReleaseDateController.insertNoteRecords(new List<Id>{releaseDateList[0].Id},'test','test','clear');
        EAW_ReleaseDateController.insertNoteRecords(new List<Id>{releaseDateList[0].Id},'test','test','replace');
    }
    @isTest static void saveReleaseDateRecordsExceptionCover_Test() {
        
        List<EAW_Tag__c> tagList = [SELECT Id, Name FROM EAW_Tag__c  LIMIT 1];
        List<EAW_Release_Date_Guideline__c> rdgList = [SELECT Id FROM EAW_Release_Date_Guideline__c LIMIT1];
        List<EAW_Release_Date__c> releaseDateList = [SELECT Id, Name,Customers__c,EAW_Release_Date_Type__c,Status__c,Manual_Not_Released_Overridden__c  FROM EAW_Release_Date__c LIMIT 1];

       
        releaseDateList[0].Status__c = 'Firm';
        //releaseDateList[0].Manual_Date__c = system.today();
        releaseDateList[0].Release_Date_Guideline__c = rdgList[0].Id;
        Map<Id, List<Id>> testRecordMap = new Map<Id, List<Id>>();
        //testRecordMap.put(releaseDateList[0].Id,new  List<Id>{tagList[0].Id});
        
        List<EAW_Release_Date_Tag_Junction__c> rdTagJunc = EAW_TestDataFactory.createReleaseDateTagJunction(1,false);
        rdTagJunc[0].Release_Date__c = releaseDateList[0].Id;
        rdTagJunc[0].Tag__c = tagList[0].Id;
        insert rdTagJunc;
        
        List<EAW_Tag__c> tagList2 = EAW_TestDataFactory.createTag(1,false,'Release Date Tag');
        tagList2[0].Name ='testing 3';
        insert tagList2;
        testRecordMap.put(releaseDateList[0].Id,new  List<Id>{tagList2[0].Id});
        
        List<EAW_Release_Date__c> releaseList = EAW_ReleaseDateController.saveReleaseDateRecords(releaseDateList, JSON.Serialize(testRecordMap));
        
        System.AssertEquals(1, releaseList.size());
    }
}