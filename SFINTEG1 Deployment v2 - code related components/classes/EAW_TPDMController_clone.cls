public with sharing class EAW_TPDMController_clone {


    /**
     * Search for Release Dates based off of MTL + Tags + Date Range + Release Date Types + Status
     * 		Returns 4 arrays in a map
     * 			1. Release Dates
     * 			2. Release Date Tag Junction
     * 			3. Windows
     * 			4. Title Tag Junctions
     */
    @AuraEnabled
    public static Map<String, List<SObject>> searchReleaseDateByCriteria(List<String> medias, List<String> territories, List<String> languages,
            List<String> types, List<String> tags, String stringDateStart, String stringDateEnd, String status) {

        Map<String, List<SObject>> maps = new Map<String, List<SObject>>();
        List<EAW_Release_Date_Tag_Junction__c> releaseDateTagJunctions = new List<EAW_Release_Date_Tag_Junction__c>();
        List<EAW_Title_Tag_Junction__c> titleTagJunctions = new List<EAW_Title_Tag_Junction__c>();
        List<EAW_Window__c> windows = new List<EAW_Window__c>();

        try {
            /*if (medias.size() > 0 || territories.size() > 0 || languages.size() > 0 || types.size() > 0 ||
                    (status != '' && status != null) || (stringDateStart != '' && stringDateStart != null) || (stringDateEnd != '' && stringDateEnd != null)) {*/
                /*List<EAW_Release_Date__c> releaseDates = getReleaseDates(medias, territories, languages, types, stringDateStart, stringDateEnd, status);
                
                if (releaseDates.size() > 0) {
                    List<String> releaseDateIds = new List<String>();
                    List<String> releaseDateTitleIds = new List<String>();

                    for (EAW_Release_Date__c releaseDate : releaseDates) {
                        releaseDateIds.add(releaseDate.Id);
                        releaseDateTitleIds.add(releaseDate.Title__c);
                    }

                    //if (tags.size() > 0) {
                        String queryReleaseDateTagJunction = 'SELECT id, Release_Date__c, Tag__c, Tag__r.Name FROM EAW_Release_Date_Tag_Junction__c WHERE Release_Date__c IN :releaseDateIds';
                        releaseDateTagJunctions = Database.query(queryReleaseDateTagJunction);
                    //}

                    String queryTitleTagJunction = 'SELECT id, Title_Attribute__c, Tag__c, Tag__r.Name FROM EAW_Title_Tag_Junction__c WHERE Title_Attribute__c IN :releaseDateTitleIds';
                    titleTagJunctions = Database.query(queryTitleTagJunction);

                    maps.put('releaseDates', releaseDates);
                    maps.put('releaseDateTagJunctions', releaseDateTagJunctions);
                    maps.put('titleTagJunctions', titleTagJunctions);
                }*/
            if (tags.size() > 0) {
                List<String> releaseDateIds = new List<String>();
                List<String> releaseDateTitleIds = new List<String>();

                String queryReleaseDateTagJunction = 'SELECT id, Release_Date__c, Tag__c, Tag__r.Name FROM EAW_Release_Date_Tag_Junction__c WHERE Tag__c IN :tags';
                releaseDateTagJunctions = Database.query(queryReleaseDateTagJunction);

                for (EAW_Release_Date_Tag_Junction__c releaseDateTagJunction : releaseDateTagJunctions) {
                    releaseDateIds.add(releaseDateTagJunction.Release_Date__c);
                }

                String queryReleaseDate = 'SELECT id, Release_Date__c, Release_Date_Type__c, Status__c, Language__c, Territory__c, Feed_Date__c, Feed_Date_Status__c,' +
                        ' Manual_Date__c, Temp_Perm__c, Projected_Date__c, Alert_Count__c, Customer__r.Name, Title__r.Name, Title__r.Title_EDM__r.FIN_PROD_ID__c,' +
                        ' Title__r.Title_EDM__r.PROD_TYP_CD_INTL__r.Name FROM EAW_Release_Date__c WHERE id IN :releaseDateIds';
                List<EAW_Release_Date__c> releaseDates = Database.query(queryReleaseDate);

                for (EAW_Release_Date__c releaseDate : releaseDates) {
                    releaseDateTitleIds.add(releaseDate.Title__c);
                }

                String queryTitleTagJunction = 'SELECT id, Title_Attribute__c, Tag__c, Tag__r.Name FROM EAW_Title_Tag_Junction__c WHERE Title_Attribute__c IN :releaseDateTitleIds';
                titleTagJunctions = Database.query(queryTitleTagJunction);

                maps.put('releaseDates', releaseDates);
                maps.put('releaseDateJunctions', releaseDateTagJunctions);
                maps.put('titleTagJunctions', titleTagJunctions);
            } else {
            	List<EAW_Release_Date__c> releaseDates = getReleaseDates(medias, territories, languages, types, stringDateStart, stringDateEnd, status);
                
                if (releaseDates.size() > 0) {
                    List<String> releaseDateIds = new List<String>();
                    List<String> releaseDateTitleIds = new List<String>();

                    for (EAW_Release_Date__c releaseDate : releaseDates) {
                        releaseDateIds.add(releaseDate.Id);
                        releaseDateTitleIds.add(releaseDate.Title__c);
                    }

                    //if (tags.size() > 0) {
                        String queryReleaseDateTagJunction = 'SELECT id, Release_Date__c, Tag__c, Tag__r.Name FROM EAW_Release_Date_Tag_Junction__c WHERE Release_Date__c IN :releaseDateIds';
                        releaseDateTagJunctions = Database.query(queryReleaseDateTagJunction);
                    //}

                    String queryTitleTagJunction = 'SELECT id, Title_Attribute__c, Tag__c, Tag__r.Name FROM EAW_Title_Tag_Junction__c WHERE Title_Attribute__c IN :releaseDateTitleIds';
                    titleTagJunctions = Database.query(queryTitleTagJunction);

                    maps.put('releaseDates', releaseDates);
                    maps.put('releaseDateTagJunctions', releaseDateTagJunctions);
                    maps.put('titleTagJunctions', titleTagJunctions);
                }
            }
        } catch (Exception e) {
            throw new AuraHandledException('Error : ' + e.getMessage());
        }

        return maps;
    }

    /**
     * Helper function for searchReleaseDateByCriteria
     */
    private static List<EAW_Release_Date__c> getReleaseDates(List<String> medias, List<String> territories, List<String> languages,
            List<String> types, String stringDateStart, String stringDateEnd, String status) {

        Boolean hasChanged = false;
        
        String query = 'SELECT id, Release_Date__c, Release_Date_Type__c, Status__c, Language__c, Territory__c, Feed_Date__c, Feed_Date_Status__c,' +
                ' Manual_Date__c, Temp_Perm__c, Projected_Date__c, Alert_Count__c, Customer__r.Name, Title__r.Name, Title__r.Title_EDM__r.FIN_PROD_ID__c,' +
                ' Title__r.Title_EDM__r.PROD_TYP_CD_INTL__r.Name FROM EAW_Release_Date__c';
        
        if (medias.size() > 0 || territories.size() > 0 || languages.size() > 0 || types.size() > 0 ||
                (status != '' && status != null) || (stringDateStart != '' && stringDateStart != null) || (stringDateEnd != '' && stringDateEnd != null)) {
        	query += ' WHERE ';
        }
        
        if (medias.size() > 0 || territories.size() > 0 || languages.size() > 0) {

            //TODO: could have the collectIdsFromNames return values from the function vs passing a param value to avoid confusion?
            Set<String> releaseDateIds = new Set<String>();

            if (medias.size() > 0) {
                collectIdsFromNames(medias, releaseDateIds, 'Media__c');
            }

            if (territories.size() > 0) {
                collectIdsFromNames(territories, releaseDateIds, 'Territory__c');
            }

            if (languages.size() > 0) {
                collectIdsFromNames(languages, releaseDateIds, 'Language__c');
            }

            query += 'Id IN :releaseDateIds';
            hasChanged = true;
        }

        if (types.size() > 0) {
            if (hasChanged) {
                query += ' AND ';
            }

            query += 'Release_Date_Type__r.id IN :types';
            hasChanged = true;
        }

        if (String.isNotBlank(status)) {
            if (hasChanged) {
                query += ' AND ';
            }

            query += 'Status__c = \'' + status + '\'';
            hasChanged = true;
        }

        if (String.isNotBlank(stringDateStart)) {
            if (hasChanged) {
                query += ' AND ';
            }

            query += 'Release_Date__c > ' + stringDateStart;
            hasChanged = true;
        }

        if (String.isNotBlank(stringDateEnd)) {
            if (hasChanged) {
                query += ' AND ';
            }

            query += 'Release_Date__c < ' + stringDateEnd;
            hasChanged = true;
        }

        System.debug('query: ' + query);

        List<EAW_Release_Date__c> releaseDates = new List<EAW_Release_Date__c>();
        releaseDates = Database.query(query);

        return releaseDates;
    }

    /**
     * Helper function for searchReleaseDateByCriteria
     */
    private static void collectIdsFromNames(List<String> names, Set<String> releaseDateIds, String type) {
        String query = 'SELECT id FROM EAW_Release_Date__c WHERE ' + type + ' includes (';

        for (Integer i = 0; i < names.size(); i++) {
            String name = names.get(i);
            query += '\'' + name + '\'';

            if (names.size() != (i + 1)) {
                query += ',';
            }
        }

        query += ')';

        List<EAW_Release_Date__c> releaseDates = Database.query(query);

        for (EAW_Release_Date__c releaseDate : releaseDates) {
            releaseDateIds.add(releaseDate.id);
        }
    }

    
    /*@IsTest static void testSearchTitlePlanWindows() {

        List<String> medias = null;
        List<String> territories = null;
        List<String> languages = null;
        List<String> titles = new List<String>();
        titles.add('');
        List<String> plans = null;
        List<String> windowTags = null;
        List<String> windowNames = null;
        String windowType = null;
        String windowStatus = null;
        String windowDateStart = null;
        String windowDateEnd = null;
        String windowOutsideDateStart = null;
        String windowOutsideDateEnd = null;
        String windowRightStatus = null;
        String windowLicenseInfoCodes = null;

        searchTitlePlanWindows(medias, territories, languages, titles, plans, windowTags, windowNames, windowType,
            windowStatus, windowDateStart, windowDateEnd, windowOutsideDateStart, windowOutsideDateEnd, windowRightStatus, windowLicenseInfoCodes);
    }*/

}