@isTest
public class EAW_ReleaseDateTypeTrigger_Test {
    
    @isTest
    static void preventDelete_Test(){
        
        EAW_Release_Date_Type__c releasedateTypeInstance = new EAW_Release_Date_Type__c(Source_Type__c = 'Data Feed', Name = 'EST-HD', Media__c = 'Pay TV');
        insert releasedateTypeInstance;
        
        List<EAW_Release_Date__c> rdList = EAW_TestDataFactory.createReleaseDate(2, FALSE);
        rdList[0].EAW_Release_Date_Type__c = 'EST-HD';
        rdList[0].Projected_Date__c = System.Today().addMonths(1);
        rdList[1].EAW_Release_Date_Type__c = 'EST-HD';
        rdList[1].Projected_Date__c = System.Today().addMonths(1);
        insert rdList;
        
        Test.StartTest();
        releasedateTypeInstance.Auto_Firm__c = TRUE;
        releasedateTypeInstance.Months_To_Auto_Firm_Period__c = 1;
        releasedateTypeInstance.Days_To_Auto_Firm_Period__c = 1;
        update releasedateTypeInstance;
        Test.StopTest();
        
        try {
            delete releasedateTypeInstance;
        } catch( Exception ex ) {
            System.debug('::::::::: Exception message :::::::::::'+ ex.getDMLMessage(0));
            System.assertequals(ex.getDMLMessage(0), 'Cannot delete. Date Type(s) are referenced in at least one release date guideline.');
        }
    }
}