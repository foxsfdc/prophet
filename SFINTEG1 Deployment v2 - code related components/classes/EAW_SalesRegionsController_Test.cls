@isTest
public class EAW_SalesRegionsController_Test {
    
    @testSetup static void setupTestData() {
    }
    @isTest static void saveSalesRegions_Test(){        
        
        Sales_Regions__c salesIns = new Sales_Regions__c(Name = 'Asia', Territories__c = 'India');
        insert salesIns;
      
        String regionName = EAW_SalesRegionsController.saveSalesRegions('Asia','India');
        
        System.assertEquals(True, regionName != null);
    }
    
    @isTest static void saveSalesRegionscase_Test(){        
        
        String regionName = EAW_SalesRegionsController.saveSalesRegions('Africa','India');
        
        System.assertEquals(True, regionName != null);
    }
    
    @isTest static void autoSelected_Test(){ 
    
        Sales_Regions__c salesIns = new Sales_Regions__c(Name = 'Asia', Territories__c = 'India');
        insert salesIns;       
        
        EAW_SalesRegionsController.autoSelected('Asia');
    }
}