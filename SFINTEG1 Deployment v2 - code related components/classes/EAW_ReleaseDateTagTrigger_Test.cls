/*

Test class for trigger : EAW_Release_Date_Tag_Junction_Trigger
Handler : EAW_ReleaseDateTagJunction_Handler 
*/
@isTest
public class EAW_ReleaseDateTagTrigger_Test {
    
    @testSetup static void setupTestData() {
        
        EAW_TestDataFactory.createReleaseDate(1, true);
        
        EAW_TestDataFactory.createTag(1, True, 'Release Date Tag');       
        EAW_TestDataFactory.createEAWCustomer(1,true); 
        EAW_TestDataFactory.createReleaseDateType(1, true);
        EAW_TestDataFactory.createRuleDetail(1, true);
        EAW_TestDataFactory.createEAWTitle(1, true);
        EAW_TestDataFactory.createWindowGuideLine(1, true);
        
    }
    //Releasedateguideline
    static testMethod void reQualificationOfRules_Test() {
        
        List<EAW_Tag__c> tagList = [SELECT Id,Name FROM EAW_Tag__c LIMIT 1];
        
        List<EAW_Release_Date_Type__c> releaseDateTypeList = [SELECT id, Name FROM EAW_Release_Date_Type__c LIMIT 1];
        List<EAW_Customer__c> customerList = [SELECT id, Name FROM EAW_Customer__c LIMIT 1];
        List<EAW_Title__c> titleList = [SELECT Id, Name FROM EAW_Title__c LIMIT 1];
        
        List<EAW_Window_Guideline__c> windowGuidelineList = EAW_TestDataFactory.createWindowGuideLine(1, false);
        windowGuidelineList[0].Status__c = 'Draft';
        windowGuidelineList[0].Name = 'Test window guidline record';
        insert windowGuidelineList;        
        
        List<EAW_Window_Guideline_Strand__c> windowGuideLineStrandList = new List<EAW_Window_Guideline_Strand__c> {
            
            new EAW_Window_Guideline_Strand__c(License_Type__c = 'Exhibition License', EAW_Window_Guideline__c = windowGuideLineList[0].Id,
            Media__c = 'Theatrical', Territory__c = 'Afghanistan', Language__c = 'Afrikaans')
        };    
        insert windowGuideLineStrandList ; 
        
        windowGuidelineList[0].Status__c = 'Active';
        update windowGuidelineList;
        
        EAW_Window__c window = new EAW_Window__c (Name='Test new', EAW_Title_Attribute__c = titleList[0].Id, Window_Type__c = 'Library');
        window.EAW_Window_Guideline__c = windowGuideLineList[0].Id;        
        insert window;
        
        //LRCC-1653 - -In this ticket once passed QA it with remove the commented code.
        /*EAW_Window_Strand__c windowStrandIns = new EAW_Window_Strand__c();
        windowStrandIns.Territory__c = 'Afghanistan';
        windowStrandIns.Language__c = 'Afrikaans';
        windowStrandIns.Media__c = 'Theatrical';
        windowStrandIns.License_Type__c = 'Exhibition License';
        windowStrandIns.Window__c = winowIns[0].Id;        
        insert windowStrandIns;*/       
        
        List<EAW_Plan_Guideline__c> planGuideLineList = new List<EAW_Plan_Guideline__c> {
            
            new EAW_Plan_Guideline__c(Status__c = 'Draft', Product_Type__c = 'Direct to Video')
        };
        insert planGuideLineList ;
         
        List<EAW_Plan_Window_Guideline_Junction__c> planWindowList = new List<EAW_Plan_Window_Guideline_Junction__c> {
            new EAW_Plan_Window_Guideline_Junction__c(Plan__c = planGuideLineList[0].Id , Window_Guideline__c = windowGuideLineList[0].Id)
        };
        insert planWindowList;        
        
        Id releaseDateRuleRecordTypeId = Schema.SObjectType.EAW_Rule__c.getRecordTypeInfosByName().get('Release Date Calculation').getRecordTypeId();
        //LRCC-1560 - Replace Customer lookup field with Customer text field
        //EAW_Release_Date_Guideline__c releaseDateGuideLineList= new EAW_Release_Date_Guideline__c( Product_Type__c = 'Episode', Territory__c = 'Aruba', Language__c = 'English', Customer__c = customerList[0].Id, EAW_Release_Date_Type__c = 'Television');
        EAW_Release_Date_Guideline__c releaseDateGuideLineList= new EAW_Release_Date_Guideline__c( Product_Type__c = 'Episode', Territory__c = 'Aruba', Language__c = 'English', Customers__c = customerList[0].Name, EAW_Release_Date_Type__c = 'Television');
        insert releaseDateGuideLineList;
        
        releaseDateGuideLineList.Active__c = true;
        update releaseDateGuideLineList;
        
        List<EAW_Release_Date__c> releaseDateList = [SELECT Id,Name FROM EAW_Release_Date__c LIMIT 1];
        releaseDateList[0].Release_Date_Guideline__c = releaseDateGuideLineList.Id;
        update releaseDateGuideLineList;
             
        EAW_Tag__c tagudpateList = new EAW_Tag__c (Tag_Type__c = 'Release Date Tag');
        insert tagudpateList;
        
        EAW_Release_Date_Tag_Junction__c relaseaDateTag = new EAW_Release_Date_Tag_Junction__c(Release_Date__c = releaseDateList[0].Id, Tag__c = tagList[0].Id);
        insert relaseaDateTag;
        
        List<EAW_Rule__c> ruleList = new List<EAW_Rule__c> {
            
            new EAW_Rule__c(RecordTypeId = releaseDateRuleRecordTypeId, Release_Date_Guideline__c = releaseDateGuideLineList.Id, Window_Guideline__c = windowGuidelineList[0].Id)
        };
        
        insert ruleList;
        
        List<EAW_Rule_Detail__c> ruleDetailList = [SELECT Id, Name FROM EAW_Rule_Detail__c LIMIT 1];
        ruleDetailList[0].Rule_No__c = ruleList[0].Id;
        ruleDetailList[0].Conditional_Operand_Id__c =  releaseDateGuideLineList.Id;
        ruleDetailList[0].Condition_Field__c = 'Release Date Tag';
        ruleDetailList[0].Condition_Criteria__c = tagudpateList.Name;        
        update ruleDetailList;
        
        relaseaDateTag.Tag__c = tagudpateList.Id;
        update relaseaDateTag;
        
        List<EAW_Release_Date_Tag_Junction__c> obtainedList = [SELECT Id, Name, Tag__c  FROM EAW_Release_Date_Tag_Junction__c LIMIT 1];
        System.assertEquals(True,obtainedList[0].Tag__c != null);
    }
    
    //Window Guideline
    static testMethod void reQualificationRelease_Test() {
        
        List<EAW_Tag__c> tagList = [SELECT Id,Name FROM EAW_Tag__c LIMIT 1];
        tagList[0].Name = 'Test record';
        tagList[0].Tag_Type__c = 'Release Date Tag';
        update tagList;
        
        List<EAW_Title__c> titleList = [SELECT Id, Name FROM EAW_Title__c LIMIT 1];
        
        List<EAW_Release_Date_Type__c> releaseDateTypeList = [SELECT id, Name FROM EAW_Release_Date_Type__c LIMIT 1];
        List<EAW_Customer__c> customerList = [SELECT id, Name FROM EAW_Customer__c LIMIT 1];
        
        List<EAW_Window_Guideline__c> windowGuidelineList = EAW_TestDataFactory.createWindowGuideLine(1, false);
        windowGuidelineList[0].Status__c = 'Draft';
        windowGuidelineList[0].Name = 'Test window guidline record 1';
        insert windowGuidelineList;      
        
        List<EAW_Window_Guideline_Strand__c> windowGuideLineStrandList = new List<EAW_Window_Guideline_Strand__c> {
            
            new EAW_Window_Guideline_Strand__c(License_Type__c = 'Exhibition License', EAW_Window_Guideline__c = windowGuideLineList[0].Id,
            Media__c = 'Theatrical', Territory__c = 'Afghanistan', Language__c = 'Afrikaans')
        };    
        insert windowGuideLineStrandList ; 
        
        windowGuidelineList[0].Status__c = 'Active';
        update windowGuidelineList;
        
        EAW_Window__c window = new EAW_Window__c (Name='Test E', EAW_Title_Attribute__c = titleList[0].Id, Window_Type__c = 'Library');

        window.EAW_Window_Guideline__c = windowGuideLineList[0].Id;        
        insert window;
        
        //LRCC-1653 - -In this ticket once passed QA it with remove the commented code.
        /*EAW_Window_Strand__c windowStrandIns = new EAW_Window_Strand__c();
        windowStrandIns.Territory__c = 'Afghanistan';
        windowStrandIns.Language__c = 'Afrikaans';
        windowStrandIns.Media__c = 'Theatrical';
        windowStrandIns.License_Type__c = 'Exhibition License';
        windowStrandIns.Window__c = winowIns[0].Id;        
        insert windowStrandIns; */     
        
        List<EAW_Plan_Guideline__c> planGuideLineList = new List<EAW_Plan_Guideline__c> {
            
            new EAW_Plan_Guideline__c(Status__c = 'Draft', Product_Type__c = 'Direct to Video')
        };
        insert planGuideLineList ;
         
         List<EAW_Plan_Window_Guideline_Junction__c> planWindowList = new List<EAW_Plan_Window_Guideline_Junction__c> {
            new EAW_Plan_Window_Guideline_Junction__c(Plan__c = planGuideLineList[0].Id , Window_Guideline__c = windowGuideLineList[0].Id)
        };
        insert planWindowList;        
        
        Id widowDateRuleRecordTypeId = Schema.SObjectType.EAW_Rule__c.getRecordTypeInfosByName().get('Window Date Calculation').getRecordTypeId();        
        //LRCC-1560 - Replace Customer lookup field with Customer text field        
        //EAW_Release_Date_Guideline__c releaseDateGuideLineList= new EAW_Release_Date_Guideline__c( Product_Type__c = 'Episode', Territory__c = 'Aruba', Language__c = 'English', Customer__c = customerList[0].Id, EAW_Release_Date_Type__c = 'Television');
        EAW_Release_Date_Guideline__c releaseDateGuideLineList= new EAW_Release_Date_Guideline__c( Product_Type__c = 'Episode', Territory__c = 'Aruba', Language__c = 'English', Customers__c = customerList[0].Id, EAW_Release_Date_Type__c = 'Television');
        insert releaseDateGuideLineList;
        
        releaseDateGuideLineList.Active__c = true;
        update releaseDateGuideLineList;
        
        EAW_Release_Date_Guideline__c conditionalOperand = new EAW_Release_Date_Guideline__c( Product_Type__c = 'Episode', Territory__c = 'Aruba', Language__c = 'English', Customers__c = customerList[0].Id, EAW_Release_Date_Type__c = 'EST-HD');
        insert conditionalOperand;
        
        List<EAW_Release_Date__c> releaseDateList = [SELECT Id,Name FROM EAW_Release_Date__c LIMIT 1];
        releaseDateList[0].Release_Date_Guideline__c = releaseDateGuideLineList.Id;
        // releaseDateList[0].Title__c = titleList[0].Id;
        update releaseDateList;
             
        /*List<EAW_Rule__c> ruleList = EAW_TestDataFactory.createEAWRule(1, true);
        ruleList[0].RecordTypeId = widowDateRuleRecordTypeId;
        ruleList[0].Release_Date_Guideline__c = releaseDateGuideLineList.Id;
        update ruleList;
        
        List<EAW_Rule_Detail__c> ruleDetailList = [SELECT Id, Name FROM EAW_Rule_Detail__c LIMIT 1];
        ruleDetailList[0].Rule_No__c = ruleList[0].Id;
        ruleDetailList[0].Conditional_Operand_Id__c =  releaseDateGuideLineList.Id;
        ruleDetailList[0].Condition_Field__c = 'Release Date Tag';
        ruleDetailList[0].Condition_Criteria__c = tagList[0].Id;        
        update ruleDetailList;*/
        String ruleContent =  '{"sObjectType":"EAW_Rule__c","Operator__c":"IN","Condition_Field__c":"Release Date Tag","Criteria__c":"Test record","Condition_Operand_Details__c":"EST-HD / Alto Adige Region / Maori / Direct to Video","Conditional_Operand_Id__c":"aA73D0000004m3gSAA","Multi_conditional_operands__c":false,"Nested__c":true,"Condition_Type__c":"If"}';
        String ruleDetailContent = '[{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Condition_Type__c":"If","Conditional_Operand_Id__c":"aA73D0000004m3gSAA","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Alto Adige Region / Maori / Direct to Video"},"dateCalcs":[],"nodeCalcs":[]},{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Rule_Order__c":2,"Nest_Order__c":1,"Condition_Type__c":"Else/If","Conditional_Operand_Id__c":"aA73D0000004m3gSAA","Condition_Met_Months__c":"2","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Alto Adige Region / Maori / Direct to Video"},"dateCalcs":[],"nodeCalcs":[]}]';
        EAW_TestRuleDataFactory.saveRuleAndRuleDetails(ruleContent,ruleDetailContent,true,releaseDateGuideLineList.Id,null,conditionalOperand.Id);
        
        EAW_Release_Date_Tag_Junction__c relaseaDateTag = new EAW_Release_Date_Tag_Junction__c(Release_Date__c = releaseDateList[0].Id, Tag__c = tagList[0].Id);
        
        Test.startTest();
        
        insert relaseaDateTag;
        
        Test.stopTest();
                
        List<EAW_Release_Date_Tag_Junction__c> obtainedList = [SELECT Id, Name, Tag__c  FROM EAW_Release_Date_Tag_Junction__c LIMIT 1];
        System.assertEquals(True,obtainedList[0].Tag__c != null);
        
        EAW_Tag__c tag = new EAW_Tag__c ( Name = 'Test tag', Tag_Type__c = 'Release Date Tag', Active__c = TRUE);
        insert tag;
        obtainedList[0].Tag__c = tag.id;
        update obtainedList;
    }
}