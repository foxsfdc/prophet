@isTest
public class EAW_GenerateAlertforUSTheatrical_Test {
    
    @isTest static void sentAlertToUSTheatrical_Test1() {
            
        List<EAW_Release_Date_Guideline__c> rdgList =  EAW_TestDataFactory.createReleaseDateGuideline(1, TRUE, 'Feature', new List<String>{'Theatrical','HV-Retail'}, 'United States +', 'English');
        List<EAW_Title__c> titleList = EAW_TestDataFactory.createEAWTitle(2, TRUE);
        List<EAW_Release_Date__c> releaseDateList = EAW_TestDataFactory.createReleaseDate(2, FALSE);
        
        releaseDateList[0].Release_Date_Guideline__c = rdgList[0].Id;
        releaseDateList[0].Title__c = titleList[0].Id;
        releaseDateList[0].Manual_Date__c = System.Today()+20;
        releaseDateList[0].Temp_Perm__c = 'Temporary';
        releaseDateList[0].EAW_Release_Date_Type__c = 'HV-Retail';
        releaseDateList[0].Territory__c = 'Albania';
        
        releaseDateList[1].Release_Date_Guideline__c = rdgList[0].Id;
        releaseDateList[1].Title__c = titleList[1].Id;
        releaseDateList[1].Manual_Date__c = System.Today();
        releaseDateList[1].Temp_Perm__c = 'Temporary';
        releaseDateList[1].EAW_Release_Date_Type__c = 'Theatrical';
        releaseDateList[1].Territory__c = 'United States +';
        
        insert releaseDateList;
        
        CollaborationGroup chatterGroup = new CollaborationGroup (
            Name = 'Test1-Release Date Review',
            CollaborationType = 'Public'
        );
        insert chatterGroup;
            
        Sales_Regions__c sr = new Sales_Regions__c (
            Name = 'Asia Pacific',
            Territories__c = 'Abu Dhabi;Algeria;Andorra'
        );
        insert sr;
        
        EAW_GenerateAlertforUSTheatrical.sentAlertToRDChatterGroup(releaseDateList);
    }
    
    @isTest static void sentAlertToUSTheatrical_Test2() {

        List<EAW_Release_Date_Guideline__c> rdgList =  EAW_TestDataFactory.createReleaseDateGuideline(1, TRUE, 'Feature', new List<String>{'Theatrical','HV-Retail'}, 'Albania', 'English');
        List<EAW_Title__c> titleList = EAW_TestDataFactory.createEAWTitle(2, TRUE);
        List<EAW_Release_Date__c> releaseDateList = EAW_TestDataFactory.createReleaseDate(2, FALSE);
        
        releaseDateList[0].Release_Date_Guideline__c = rdgList[0].Id;
        releaseDateList[0].Title__c = titleList[0].Id;
        releaseDateList[0].Manual_Date__c = System.Today()+20;
        releaseDateList[0].Temp_Perm__c = 'Permanent';
        releaseDateList[0].Status__c = 'Tentative';
        releaseDateList[0].EAW_Release_Date_Type__c = 'Theatrical';
        releaseDateList[0].Territory__c = 'United States +';
        
        releaseDateList[1].Release_Date_Guideline__c = rdgList[0].Id;
        releaseDateList[1].Title__c = titleList[1].Id;
        releaseDateList[1].Manual_Date__c = System.Today();
        releaseDateList[1].Temp_Perm__c = 'Temporary';
        releaseDateList[1].EAW_Release_Date_Type__c = 'HV-Retail';
        releaseDateList[1].Territory__c = 'Albania';
        
        insert releaseDateList;
        
        CollaborationGroup chatterGroup = new CollaborationGroup (
            Name = 'Test1-Release Date Review',
            CollaborationType = 'Public'
        );
        insert chatterGroup;
            
        Sales_Regions__c sr = new Sales_Regions__c (
            Name = 'Asia Pacific',
            Territories__c = 'Abu Dhabi;Algeria;Andorra'
        );
        insert sr;
        
        EAW_GenerateAlertforUSTheatrical.sentAlertToRDChatterGroup(releaseDateList);
    }
}