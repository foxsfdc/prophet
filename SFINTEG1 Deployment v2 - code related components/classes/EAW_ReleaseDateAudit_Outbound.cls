//LRCC-1708
public with sharing class EAW_ReleaseDateAudit_Outbound {
	
	List<EAW_Error_Log__c> repoErrorLogs  = new List<EAW_Error_Log__c>();
	set<id> repoSuccessIds= new set<id>();
	Set<id> repoIgnoredIds = new Set<id>();

    String repoUrl = '';
	
	public EAW_ReleaseDateAudit_Outbound(){
		checkOuboundRecursiveData.bypassTriggerFire=true;
		checkRecursiveData.bypassDateCalculation = true;
		Rest_Endpoint_Settings__c repoInstance = Rest_Endpoint_Settings__c.getInstance('Outbound-RD-Hist-to-Repo');
		if(repoInstance!=null){
			repoUrl=repoInstance.End_Point__c;
		}		
	}
    
    
    public class ReleaseDateAuditJson{
    	public String releaseDateId;
    	public String releaseDateName;
    	public String fieldName;
    	public String newValue;
    	public String oldValue;
    	public String updatedBy;
    	public String updateDate;
    	public String operation;
    	
    	public ReleaseDateAuditJson(String releaseDateId,String releaseDateName,String fieldName,String newValue,
    								String oldValue,String updatedBy,String updateDate,String operation){
		
			this.releaseDateId=releaseDateId;
			this.releaseDateName=releaseDateName;
			this.fieldName=fieldName;
			this.newValue=newValue;
			this.oldValue=oldValue;
			this.updatedBy=updatedBy;
			this.updateDate=updateDate;
			this.operation=operation;
    	}
    }
    
    private void getReleaseDateAuditJson(EAW_Outbound_Utility.OutboundSystem os, List<Eaw_release_date__c> eawRDList){
    	try{
	    	datetime historyDate = datetime.now();
	    	List<EAW_Release_Date__c> updateList = new List<EAW_Release_Date__c>();
	    	String operation = 'Insert';
	    	
	    	List<List<ReleaseDateAuditJson>> rdJsonListOfList = new List<List<ReleaseDateAuditJson>>();
	    	List<List<id>> rdIdListOfList = new List<List<id>>();
	    	
			List<ReleaseDateAuditJson> rdJsonList = new List<ReleaseDateAuditJson>();
			List<id> rdIdList = new List<id>();
	    	for(Eaw_release_date__c rd:eawRDList){
	    		List<EAW_Release_Date__History> rdhjList = rd.Histories;
				for(EAW_Release_Date__History rdh:rdhjList){
					if(rd.History_Sent_Date__c == null || (rdh.CreatedDate > rd.History_Sent_Date__c)){
						ReleaseDateAuditJson rdJson = null;
						rdJson = new ReleaseDateAuditJson(''+ rd.ID,rd.name,rdh.Field,String.valueOf(rdh.NewValue),String.valueOf(rdh.OldValue),rdh.CreatedBy.Name,EAW_Outbound_Utility.getFormattedDateTime(rdh.CreatedDate),operation);
						rdJsonList.add(rdJson);
						rdIdList.add(rd.id);
					}
					if(rdJsonList.size()==EAW_Outbound_Utility.MAX_API_RECORDS_LIMIT){
						rdJsonListOfList.add(rdJsonList);
						rdIdListOfList.add(rdIdList);
				    	rdJsonList = new List<ReleaseDateAuditJson>();
				    	rdIdList = new List<id>();
					}					
				}
			}
    		if(rdJsonList!=null && !rdJsonList.isEmpty()){
    			rdJsonListOfList.add(rdJsonList);
				rdIdListOfList.add(rdIdList);
    		}
    		
    		
    		if(rdJsonListOfList!=null && !rdJsonListOfList.isempty()){
    			for(integer i=0;i<rdJsonListOfList.size();i++){
    				List<ReleaseDateAuditJson> tempRdJsonList = rdJsonListOfList[i];
					List<id> tempIdList = rdIdListOfList[i];
					EAW_Outbound_Utility eouRepo = new EAW_Outbound_Utility(EAW_Outbound_Utility.OutboundDataType.ReleaseDateAuditInformation,os,tempIdList);
    				if(os==EAW_Outbound_Utility.OutboundSystem.Repo){
				    	eouRepo.sendDataThroughWebAPI( system.json.serialize(tempRdJsonList),repoUrl,repoErrorLogs,repoSuccessIds);
		    		}
    			}
    			for(Eaw_release_date__c rd:eawRDList){
    				rd.History_Sent_Date__C = historyDate;
    				updateList.add(rd);  				
    			}
    			update updateList;
    		}
    		
    	}catch(exception e){
    		system.debug('EAW_ReleaseDateAudit_Outbound Exception: '+e.getmessage());
    	}
    }
    
    public void aggregateAllRds(List<Eaw_release_date__c> records){
    	List<Eaw_release_date__c> repoRecords = new List<Eaw_release_date__c>();
		List<EAW_Error_Log__c> insertErrorList = new List<EAW_Error_Log__c>();
		if(records!=null && !records.isempty()){
    		for(Eaw_release_date__c rd:records){
				if(rd.Sent_To_Repo__c){
    				repoRecords.add(rd);
				}else if(!rd.Sent_To_Repo__c){
					repoIgnoredIds.add(rd.id);
				}		
			}
			if(!repoRecords.isEmpty()){
				getReleaseDateAuditJson(EAW_Outbound_Utility.OutboundSystem.Repo,repoRecords);
			}
			insertErrorList.addAll(repoErrorLogs);			
    	}
    	if(insertErrorList!=null && !insertErrorList.isEmpty()){
            insert insertErrorList;
        }
    }
   
   /*
    @future(callout=true)
    public static void sendDataAsync(List<Eaw_release_date__c> eawRDList){
    	EAW_ReleaseDateAudit_Outbound ero = new EAW_ReleaseDateAudit_Outbound();
    	ero.sendData(eawRDList);
    }
    
    public static void sendDataSync(List<Eaw_release_date__c> eawRDList){
    	EAW_ReleaseDateAudit_Outbound ero = new EAW_ReleaseDateAudit_Outbound();
    	ero.sendData(eawRDList);
    }
	*/
	public void sendData(List<Eaw_release_date__c> eawRDList){
    	aggregateAllRds(eawRDList);
    }
    
}