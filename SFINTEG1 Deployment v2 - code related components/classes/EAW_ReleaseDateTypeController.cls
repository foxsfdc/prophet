public with sharing class EAW_ReleaseDateTypeController {

    @AuraEnabled
    public static List<EAW_Release_Date_Type__c> getAllTypes() {
        String query = 'SELECT Name, Media__c, Source__c, Source_Type__c, Auto_Firm__c, Months_To_Auto_Firm_Period__c, Days_To_Auto_Firm_Period__c from EAW_Release_Date_Type__c';
        List<EAW_Release_Date_Type__c> types = Database.query(query);
        return types;
    }
}