@isTest
public class EAW_WindowGuidelineController_Test {
    
    @testSetup static void setupTestData() {
        
        EAW_TestDataFactory.createEAWCustomer(1, true);
        EAW_TestDataFactory.createWindowGuideLine(2, true);
        EAW_TestDataFactory.createPlanGuideline(1, true);
    }
    
    static testMethod void myUnitTest() {
        
        List<Object> obtainedList = EAW_WindowGuidelineController.getFields(new Map<String,string>{'EAW_Window_Guideline__c'=>'More_Search_Result_Fields'});
        
        System.assertEquals(true, obtainedList != null);
    }
    
    static testMethod void searchWindows_Test() {
        
        List<EAW_Customer__c> customerList = [SELECT Id, Name FROM EAW_Customer__c LIMIT 1];
        List<EAW_Plan_Guideline__c> planGuideList = [SELECT Id, Name FROM EAW_Plan_Guideline__c LIMIT 1];
        List<EAW_Window_Guideline__c> windowGuidelineList = [SELECT Id, Name FROM EAW_Window_Guideline__c LIMIT 2];
        
        EAW_Plan_Window_Guideline_Junction__c planWindowJunctionIns = new EAW_Plan_Window_Guideline_Junction__c(Plan__c = planGuideList[0].Id, Window_Guideline__c = windowGuidelineList[0].Id);
        insert planWindowJunctionIns;
        
        List<EAW_Window_Guideline__c> ruleDetail = [SELECT Id, Name FROM EAW_Window_Guideline__c  LIMIT 1];
        
        EAW_Window_Guideline_Strand__c windowStrand = new EAW_Window_Guideline_Strand__c (EAW_Window_Guideline__c = ruleDetail[0].Id, License_Type__c = 'Exhibition License', Territory__c = 'Aruba', Language__c = 'French', Media__c = 'Basic TV');
        insert windowStrand;
       
        set<Id> testWindowId = new set<Id>();
        Map<String, set<Id>> testWindowIdMap = new Map<String,set<Id>>();
        testWindowId.add(windowGuidelineList[0].Id);
        testWindowId.add(windowGuidelineList[1].Id);
        testWindowIdMap.put('1',testWindowId);
        
        EAW_WindowGuidelineController.searchWindows(new List<String> {String.valueOf(customerList[0].Id)}, new List<String> {String.valueOf(planGuideList[0].Id)}, new List<String>{'Non-Theatrical'}, new List<String>{'Aruba'}, new List<String>{'French'}, new List<String>{'Test Window Guideline'},'','Release','Active', 0,50,1,JSON.serialize(testWindowIdMap),'{"selectedCol":"Status__c","selectedAlpha":"F"}',new List<String>{'testProductType'});
        EAW_WindowGuidelineController.updateOriginWindowGuidelineId(windowGuidelineList[0].Id);
    } 
    
    static testMethod void searchWindowsAll_Test() {
        
        List<EAW_Customer__c> customerList = [SELECT Id, Name FROM EAW_Customer__c LIMIT 1];
        List<EAW_Plan_Guideline__c> planGuideList = [SELECT Id, Name FROM EAW_Plan_Guideline__c LIMIT 1];
        List<EAW_Window_Guideline__c> windowGuidelineList = [SELECT Id, Name FROM EAW_Window_Guideline__c LIMIT 2];
        
        EAW_Plan_Window_Guideline_Junction__c planWindowJunctionIns = new EAW_Plan_Window_Guideline_Junction__c(Plan__c = planGuideList[0].Id, Window_Guideline__c = windowGuidelineList[0].Id);
        insert planWindowJunctionIns;
        
        List<EAW_Window_Guideline__c> ruleDetail = [SELECT Id, Name FROM EAW_Window_Guideline__c  LIMIT 1];
        
        EAW_Window_Guideline_Strand__c windowStrand = new EAW_Window_Guideline_Strand__c (EAW_Window_Guideline__c = ruleDetail[0].Id, License_Type__c = 'Exhibition License', Territory__c = 'Aruba', Language__c = 'French', Media__c = 'Basic TV');
        insert windowStrand;
        
        set<Id> testWindowId = new set<Id>();
        Map<String, set<Id>> testWindowIdMap = new Map<String,set<Id>>();
        testWindowId.add(windowGuidelineList[0].Id);
        testWindowId.add(windowGuidelineList[1].Id);
        testWindowIdMap.put('1',testWindowId);
        EAW_WindowGuidelineController.searchWindows(new List<String> {String.valueOf(customerList[0].Id)}, new List<String> {String.valueOf(planGuideList[0].Id)}, new List<String>{'Non-Theatrical'}, new List<String>{'All'}, new List<String>{'All'}, new List<String>{'Test Window Guideline'},'','Release','Active', 0,50,1,JSON.serialize(testWindowIdMap),'{"selectedCol":"Status__c","selectedAlpha":"F"}',new List<String>{'testProductType'});

    }  
    
    static testMethod void searchWindowsnull_Test() {
        
        List<EAW_Customer__c> customerList = [SELECT Id, Name FROM EAW_Customer__c LIMIT 1];
        List<EAW_Plan_Guideline__c> planGuideList = [SELECT Id, Name FROM EAW_Plan_Guideline__c LIMIT 1];
        List<EAW_Window_Guideline__c> windowGuidelineList = [SELECT Id, Name FROM EAW_Window_Guideline__c LIMIT 2];
        
        EAW_Plan_Window_Guideline_Junction__c planWindowJunctionIns = new EAW_Plan_Window_Guideline_Junction__c(Plan__c = planGuideList[0].Id, Window_Guideline__c = windowGuidelineList[0].Id);
        insert planWindowJunctionIns;
        
        List<EAW_Window_Guideline__c> ruleDetail = [SELECT Id, Name FROM EAW_Window_Guideline__c  LIMIT 1];
        
        EAW_Window_Guideline_Strand__c windowStrand = new EAW_Window_Guideline_Strand__c (EAW_Window_Guideline__c = ruleDetail[0].Id, License_Type__c = 'Exhibition License', Territory__c = 'Aruba', Language__c = 'French', Media__c = 'Basic TV');
        insert windowStrand;
        
        set<Id> testWindowId = new set<Id>();
        Map<String, set<Id>> testWindowIdMap = new Map<String,set<Id>>();
        testWindowId.add(windowGuidelineList[0].Id);
        testWindowId.add(windowGuidelineList[1].Id);
        testWindowIdMap.put('1',testWindowId);
        EAW_WindowGuidelineController.searchWindows(null, null, null, null, null, null,'','','', 0,null,null,JSON.serialize(testWindowIdMap),null,null);

    } 
    
    static testMethod void deactivateWindows_Test() {
        
        List<EAW_Window_Guideline__c> windowGuide = [SELECT Id, Name FROM EAW_Window_Guideline__c LIMIT 1];
        
        EAW_WindowGuidelineController.deactivateWindows(new List<String>{windowGuide[0].Id});
        
        List<EAW_Window_Guideline__c> windowCloneList = EAW_WindowGuidelineController.cloneWindowGuidelines(new List<String>{windowGuide[0].Id});
        
        System.assertEquals(true, windowCloneList != null);
    }
    
    static testMethod void getRelatedPlanGuidelines_Test() {
        
        List<EAW_Window_Guideline__c> windowGuide = [SELECT Id, Name FROM EAW_Window_Guideline__c LIMIT 1];
        
        List<EAW_Plan_Window_Guideline_Junction__c>  junctionList = EAW_WindowGuidelineController.getRelatedPlanGuidelines(new List<String>{windowGuide[0].Id});
        
        System.assertEquals(true, junctionList != null);
    }
    
    static testMethod void getVersionDetails_Test() {
        
        List<EAW_Window_Guideline__c> windowGuide = [SELECT Id, Name FROM EAW_Window_Guideline__c LIMIT 1];
        
        EAW_Window_Guideline__c versionDetail = EAW_WindowGuidelineController.getVersionDetails(windowGuide[0].Id);
        
        System.assertEquals(True, versionDetail != null);
   }
   
    static testMethod void saveRecords_Test() {
        
        EAW_Window_Guideline__c windowguideIns = new EAW_Window_Guideline__c ( Status__c = 'Draft', Name = 'Test window guideline records', Product_Type__c = 'Season', Window_Type__c = 'First-Run', Window_Guideline_Alias__c = 'Test Window Guideline Alias 1');
        insert windowguideIns;
        
        EAW_Window_Guideline__c windowguidenxt = new EAW_Window_Guideline__c ( Status__c = 'Draft', Name = 'Test window guideline next', Product_Type__c = 'Shorts', Window_Type__c = 'First-Run', Window_Guideline_Alias__c = 'Test Window Guideline Alias 2');
        insert windowguidenxt;
        
        List<EAW_Window_Guideline__c> windowGuide = [SELECT Id, Name FROM EAW_Window_Guideline__c LIMIT 1];
        windowGuide[0].Previous_Version__c = windowguideIns.Id;
        windowGuide[0].Next_Version__c = windowguidenxt.Id;
        update windowGuide;
        
        EAW_Window_Guideline_Strand__c windowStrand = new EAW_Window_Guideline_Strand__c (EAW_Window_Guideline__c = windowGuide[0].Id, Language__c = 'Arabic', Media__c = 'Basic TV', Territory__c = 'Afghanistan');
        insert windowStrand;
        
        EAW_Window_Guideline__c ruleDetail = new EAW_Window_Guideline__c ( Status__c = 'Draft', Name = 'Test window Guideline on 10/dec', Product_Type__c = 'Special', Window_Type__c = 'First-Run', Window_Guideline_Alias__c = 'Test Window Guideline Alias 3');
        insert ruleDetail;
       
        EAW_WindowGuidelineController.saveRecords(JSON.Serialize(windowGuide));
        List<EAW_Window_Guideline__c> versionList = EAW_WindowGuidelineController.getAllVersions(windowGuide[0].Id);
        system.assertEquals(true, versionList != null);
    }
    
    static testMethod void getAllVersions_Test() {
        
        EAW_Window_Guideline__c windowguideIns = new EAW_Window_Guideline__c ( Status__c = 'Draft', Name = 'Test window guideline records', Product_Type__c = 'Special', Window_Type__c = 'First-Run', Window_Guideline_Alias__c = 'Test old Window Guideline Alias');
        insert windowguideIns;
        
        EAW_Window_Guideline__c windowguidenxt = new EAW_Window_Guideline__c ( Status__c = 'Draft', Name = 'Test window guideline next', Product_Type__c = 'New Media Film', Window_Type__c = 'First-Run', Window_Guideline_Alias__c = 'Test new Window Guideline Alias');
        insert windowguidenxt;
        
        List<EAW_Window_Guideline__c> windowGuide = [SELECT Id, Name FROM EAW_Window_Guideline__c LIMIT 1];
        windowGuide[0].Previous_Version__c = windowguideIns.Id;
        windowGuide[0].Next_Version__c = windowguidenxt.Id;
        update windowGuide;
        
        EAW_Window_Guideline_Strand__c windowStrand = new EAW_Window_Guideline_Strand__c (EAW_Window_Guideline__c = windowGuide[0].Id, Language__c = 'Arabic', Media__c = 'Basic TV', Territory__c = 'Afghanistan');
        insert windowStrand;
        
        EAW_Window_Guideline__c ruleDetail = new EAW_Window_Guideline__c ( Status__c = 'Draft', Name = 'Test window Guideline on 10/dec',Product_Type__c = 'Format', Window_Type__c = 'First-Run', Window_Guideline_Alias__c = 'Test Window Guideline Alias 3');
        insert ruleDetail;
        
        Id versionId = EAW_WindowGuidelineController.getNewVersionId(windowGuide[0].Id, 'Test record', 'Test body');
        System.assertEquals(true, versionId != null);
    }
    static testMethod void toCoverPagination_Test() {
          
        List<EAW_Customer__c> customerList = [SELECT Id, Name FROM EAW_Customer__c LIMIT 1];
        List<EAW_Plan_Guideline__c> planGuideList = [SELECT Id, Name FROM EAW_Plan_Guideline__c LIMIT 1];
        List<EAW_Window_Guideline__c> windowGuidelineList = [SELECT Id, Name FROM EAW_Window_Guideline__c LIMIT 2];
        
        EAW_Plan_Window_Guideline_Junction__c planWindowJunctionIns = new EAW_Plan_Window_Guideline_Junction__c(Plan__c = planGuideList[0].Id, Window_Guideline__c = windowGuidelineList[0].Id);
        insert planWindowJunctionIns;
        
        List<EAW_Window_Guideline__c> ruleDetail = [SELECT Id, Name FROM EAW_Window_Guideline__c  LIMIT 1];
        
        EAW_Window_Guideline_Strand__c windowStrand = new EAW_Window_Guideline_Strand__c (EAW_Window_Guideline__c = ruleDetail[0].Id, License_Type__c = 'Exhibition License', Territory__c = 'Aruba', Language__c = 'French', Media__c = 'Basic TV');
        insert windowStrand;
        String newruleContent = '{"sObjectType":"EAW_Rule__c","Nested__c":true}';
        String newruleDetailContent = '[{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Condition_Timeframe__c":"True_Earliest"},"dateCalcs":[{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Conditional_Operand_Id__c":"aA73D000000CyJjSAK","Condition_Met_Months__c":"2","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"},{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Conditional_Operand_Id__c":"aA73D000000CyJjSAK","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"}],"nodeCalcs":[{"ruleDetail":{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":2,"Condition_Timeframe__c":"True_Earliest"},"dateCalcs":[{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":2,"Conditional_Operand_Id__c":"aA73D000000CyJjSAK","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"}],"nodeCalcs":[]}]}]';
        EAW_TestRuleDataFactory.saveRuleAndRuleDetails(newruleContent,newruleDetailContent,false,windowGuidelineList[0].Id,'Start Date',String.valueOf(windowGuidelineList[0].Id));
        
        set<Id> testWindowId = new set<Id>();
        Map<String, set<Id>> testWindowIdMap = new Map<String,set<Id>>();
        testWindowId.add(windowGuidelineList[0].Id);
        
        testWindowIdMap.put('1',testWindowId);
        EAW_WindowGuidelineController.searchWindows(new List<String>(), new List<String> {String.valueOf(planGuideList[0].Id)}, new List<String>{'Non-Theatrical'}, new List<String>{'All'}, new List<String>{'All'}, new List<String>{'Test Window Guideline'},'','Release','Active', 0,50,2,JSON.serialize(testWindowIdMap),'{"selectedCol":"Status__c","selectedAlpha":"F"}',new List<String>{'testProductType'});
        windowGuidelineList[0].Status__c = 'Active';
        update windowGuidelineList;
        EAW_WindowGuidelineController.getNewVersionId(windowGuidelineList[0].Id, 'test', 'test');
    }
}