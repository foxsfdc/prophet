/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class EAW_ContentOwnerDataController_Test {

    @testsetup static void setup(){
        insert new EAW_title__C(FOX_ID__c='10775',name='Mr. & Mrs. Smith');
        insert new EAW_title__C(FOX_ID__c='107751',name='Mr. & Mrs. Smith2');
        insert new EAW_title__C(FOX_ID__c='1322856',name='Mr. & Mrs. Rams');
        insert new EAW_Tag__c(Active__c=true,Name='Baby TVS',Tag_Type__c='Title Tag',Sub_Type__c='Content Owner');
    }
    
    
    static testMethod void testContentOwnerWithNewTag() {
       RestRequest req = new RestRequest(); 
       RestResponse res = new RestResponse();
        req.requestURI = '/contentownerevent';  
        req.httpMethod = 'POST';
        req.requestBody=Blob.valueof('{ "foxId": "10775", "businessUnitId": 2, "rstrcnCdId": 1198, "rstrcnCd": "BTV", "rstrcnDesc": "Baby TV" }');
        req.addHeader('Content-Type','application/json');
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        
        String results = EAW_ContentOwnerDataController.doPost();
        system.assertEquals(results, 'Success');
        Test.stopTest(); 
        
    }
    
    static testMethod void testContentOwnerWithExistingTag() {
       RestRequest req = new RestRequest(); 
       RestResponse res = new RestResponse();
        req.requestURI = '/contentownerevent';  
        req.httpMethod = 'POST';
        req.requestBody=Blob.valueof('{ "foxId": "107751", "businessUnitId": 2, "rstrcnCdId": 1198, "rstrcnCd": "BTS", "rstrcnDesc": "Baby TVS" }');
        req.addHeader('Content-Type','application/json');
        RestContext.request = req;
        RestContext.response = res;
        String results = EAW_ContentOwnerDataController.doPost();
        system.assertEquals(results, 'Success');
       
        
    }
    
    static testMethod void testContentOwnerWithInvalidJson() {
       RestRequest req = new RestRequest(); 
       RestResponse res = new RestResponse();
        req.requestURI = '/contentownerevent';  
        req.httpMethod = 'POST';
        req.requestBody=Blob.valueof('{ "foxIds": "107751", "businessUnitIds": 2, "rstrcnCdIds": 1198, "rstrcnCds": "BTV", "rstrcnDescs": "Baby TV" }');
        req.addHeader('Content-Type','application/json');
        RestContext.request = req;
        RestContext.response = res;
        String results = EAW_ContentOwnerDataController.doPost();
        system.assertEquals(results, 'Invalid Request'); 
    }
    
    static testMethod void testContentOwnerBatch1() {
        
        EAW_Inbound_Notifications__c ein = new EAW_Inbound_Notifications__c();
        //ein.Name = 'Content Owner Batch'+System.now();
        ein.Status__c = 'Need to Process';
        ein.Source_System__c='Content Owner';
        ein.FOX_ID__c = '1322856';
        //ein.Content_Owner_Descriptions__c = 'Annapurna;FNG Germany;Fox International Productions';
        ein.Content_Owner_Codes__c = 'ANA;FGE;FIP';
        insert ein;
        
        Test.startTest();
            EAW_ContentOwner_BatchProcess contOwnerBatch = new EAW_ContentOwner_BatchProcess();
            Database.executeBatch(contOwnerBatch);
        Test.stopTest();
        
    }
    
    static testMethod void testContentOwnerBatch2() {
        
        EAW_Inbound_Notifications__c ein = new EAW_Inbound_Notifications__c();
        //ein.Name = 'Content Owner Batch'+System.now();
        ein.Status__c = 'Need to Process';
        ein.Source_System__c='Content Owner';
        ein.FOX_ID__c = '13744';
        ein.Content_Owner_Descriptions__c = 'Annapurna;FNG Germany;Fox International Productions';
        ein.Content_Owner_Codes__c = 'ANA;FGE;FIP';
        insert ein;
        
        List<EAW_Title__c> titleList = EAW_TestDataFactory.createEAWTitle(1, TRUE);
        titleList[0].FOX_ID__c = '13744';
        update titleList;
        
        EAW_Tag__c tag = new EAW_Tag__c ( Name = 'Annapurna', Tag_Type__c = 'Title Tag', Active__c = TRUE);
        tag.Sub_Type__c = 'Content Owner';
        insert tag;
        
        EAW_Tag__c tag2 = new EAW_Tag__c ( Name = 'Anna', Tag_Type__c = 'Title Tag', Active__c = TRUE);
        tag2.Sub_Type__c = 'Content Owner';
        insert tag2;
        
        EAW_Title_Tag_Junction__c titleTagJunc = new EAW_Title_Tag_Junction__c();
        titleTagJunc.Title_Attribute__c = titleList[0].Id;
        titleTagJunc.Tag__c = tag.Id;
        insert titleTagJunc;
        
        EAW_Title_Tag_Junction__c titleTagJunc2 = new EAW_Title_Tag_Junction__c();
        titleTagJunc2.Title_Attribute__c = titleList[0].Id;
        titleTagJunc2.Tag__c = tag2.Id;
        insert titleTagJunc2;
        
        Test.startTest();
            EAW_ContentOwner_BatchProcess contOwnerBatch = new EAW_ContentOwner_BatchProcess();
            Database.executeBatch(contOwnerBatch);
        Test.stopTest();
        
    }
    
    static testMethod void testContentOwnerBatch3() {
        
        EAW_Inbound_Notifications__c ein = new EAW_Inbound_Notifications__c();
        //ein.Name = 'Content Owner Batch'+System.now();
        ein.Status__c = 'Need to Process';
        ein.Source_System__c='Content Owner';
        ein.FOX_ID__c = '3722';
        ein.Content_Owner_Descriptions__c = 'Disney;FNG INDIA;Fox Productions';
        ein.Content_Owner_Codes__c = 'ANA;FGE;FIP';
        insert ein;
        
        EAW_Tag__c tag = new EAW_Tag__c ( Name = 'Disney', Tag_Type__c = 'Title Tag', Active__c = TRUE);
        tag.Sub_Type__c = 'Content Owner';
        insert tag;
        List<EAW_Title__c> titleList = EAW_TestDataFactory.createEAWTitle(1, TRUE);
        titleList[0].FOX_ID__c = '3722';
        update titleList;
        
        Test.startTest();
            EAW_ContentOwner_BatchProcess contOwnerBatch = new EAW_ContentOwner_BatchProcess();
            Database.executeBatch(contOwnerBatch);
        Test.stopTest();
    }
    
    @isTest static void testContentOwnerBatch4(){
                
        List<EAW_Release_Date_Guideline__c> rdgList =  EAW_TestDataFactory.createReleaseDateGuideline(2, TRUE, 'Feature', new List<String>{'Theatrical','HV-Retail'}, 'United States +', 'English');
          
        List<EAW_Title__c> titleList = EAW_TestDataFactory.createEAWTitle(2, TRUE);
        
        List<EAW_Release_Date__c> releaseDateList = EAW_TestDataFactory.createReleaseDate(2, FALSE);
        
        releaseDateList[0].Release_Date_Guideline__c = rdgList[0].Id;
        releaseDateList[0].Title__c = titleList[0].Id;
        releaseDateList[0].Manual_Date__c = System.Today();
        releaseDateList[0].Temp_Perm__c = 'Temporary';
        
        releaseDateList[1].Release_Date_Guideline__c = rdgList[0].Id;
        releaseDateList[1].Title__c = titleList[1].Id;
        releaseDateList[1].Manual_Date__c = System.Today();
        releaseDateList[1].Temp_Perm__c = 'Temporary';
        
        insert releaseDateList;
        Test.startTest();
            EAW_ContentOwner_BatchProcess contOwnerBatch = new EAW_ContentOwner_BatchProcess();
            contOwnerBatch.RdgIds = new Set<Id>{rdgList[0].Id, rdgList[1].Id};
            contOwnerBatch.titleIdSet = new Set<Id>{titleList[0].Id, titleList[1].Id};
            Database.executeBatch(contOwnerBatch);
        Test.stopTest();
    }
    
    @isTest static void testContentOwnerBatch5(){
        
        EAW_TestDataFactory.createEAWCustomer(1, true);
        List<EAW_Window_Guideline__c> wgLineList = EAW_TestDataFactory.createWindowGuideLine(2, true);
        EAW_TestDataFactory.createPlanGuideline(1, true);
        List<EAW_Title__c> titleList = EAW_TestDataFactory.createEAWTitle(2, TRUE);
        Test.startTest();
            EAW_ContentOwner_BatchProcess contOwnerBatch = new EAW_ContentOwner_BatchProcess();
            contOwnerBatch.wgIds = new Set<Id>{wgLineList[0].Id, wgLineList[1].Id};
            contOwnerBatch.titleIdSet = new Set<Id>{titleList[0].Id, titleList[1].Id};
            Database.executeBatch(contOwnerBatch);
        Test.stopTest();
    }
    
    @isTest static void testContentOwnerscheduler(){
        
        Test.startTest();
            EAW_ContentOwner_BatchProcess  sh1 = new EAW_ContentOwner_BatchProcess ();
            String sch = '0 0 23 * * ?'; 
            system.schedule('Test content owner Check', sch, sh1); 
        Test.stopTest(); 
    }
    
}