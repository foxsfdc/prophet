public with sharing class EAW_RulesEngineHandler 
{
    public static String key{get;set;}
    public static String secret{get;set;}
    public static String token{get;set;}
    public static String host {get;set;}
    public static String grant_type {get;set;}
    public static String endPointURL {get;set;}
    
    public void EAW_RulesEngineHandler()
    {
            
    }
    
    public void createBody(List<EAW_Title__c> titles)
    {/*
        List<titleWrapper> wrapperTitles = new List<titleWrapper>();
        List<Id>edmIds = new List<Id>();
        for(EAW_Title__c t: titles)
        {
            edmIds.add(t.Title_EDM__c);
        }
        Map<Id,EDM_GLOBAL_TITLE__c> titleMap = new Map<Id,EDM_GLOBAL_TITLE__c>( [ Select 
                                                                                    Id, Acquired_Indicator__c,  Actual_Run_Time__c, Actual_Run_Time_mins__c, Approx_Run_Time__c, BCST_SEAS_TWO_YR_PRD__c, CurrencyIsoCode, CreatedById, DIR_NM__c, EP_NM__c, 
                                                                                    FIN_DIV_CD__c, FIN_PROD_ID__c, FRST_REL_DATE__c, FRST_REL_DATE_WW__c, FOX_ID__c, FOX_VERSION_ID__c, CREATE_DATE__c, LAST_UPDATE_DATE__c, In_Progress__c, PROD_TYP_CD_INTL__c, 
                                                                                    TVD_Is_Box_Structure_Created__c, Is_Vam__c, LastModifiedById, LIFE_CYCL_EFF_DT__c, LIFE_CYCL_STAT_CD__c, LIFE_CYCL_STAT_GRP_CD__c, Metadata_URL__c, ORIG_MEDIA_CD__c, 
                                                                                    PRIM_ORG_LNG_CD__c, OwnerId, PRIM_PROD_CNTRY_CD__c, Problem_Title__c, TVD_Product_Bible__c, PROD_TYP_CD__c, PROD_TYP_CD__r.Name, PROD_ACQ_IND__c, PROD_CAL_FSCL_YR__c,  PROD_CAL_YR__c, 
                                                                                    PROD_EPDSE_NUM__c, PROD_FIN_CD__c, Program_Run_Time__c, Program_Run_Time_mins__c, PBLC_CD__c, PUBLIC_CD__c, PUBLISH_IND__c, RLSE_CAL_FSCL_YR__c, RLSE_CAL_YR__c, ROW_ID__c,
                                                                                    SEAS_NM__c, SER_NM__c, TITLE_SRT__c, SRC_PKEY__c, SYS_IsConfidential__c, TITLE_DESC__c, Name, TITLE_SUB_TYPE_CD__c, Total_Global_Box_Office_USD__c, Total_US_Box_Office__c,
                                                                                    TVD_Alias__c, TVD_Media_Maestro_ID__c, EP_NUM_UNSRT__c, SER_NM_UNSRT__c, TITLE_UNSRT__c, VAM_Sub_Type_EDM__c, VAM_Type_EDM__c
                                                                                  From EDM_GLOBAL_TITLE__c 
                                                                                  Where Id in:edmIds
                                                                              ]);
        for(EAW_Title__c t: titles)
        {
            
            EDM_GLOBAL_TITLE__c edmTitle = titleMap.get(t.Title_EDM__c);
            
            titleWrapper tw = new titleWrapper();
            tw.titleId = t.Id;
            tw.edmTitleId = edmTitle.Id;
            tw.acquiredIndicator = edmTitle.Acquired_Indicator__c;
            tw.actualRunTime = edmTitle.Actual_Run_Time__c;
            tw.actualRunTimMins = edmTitle.Actual_Run_Time_mins__c;
            tw.approxRunTime = edmTitle.Approx_Run_Time__c;
            tw.bcstSeasTwoYrPrd = edmTitle.BCST_SEAS_TWO_YR_PRD__c;
            tw.currencyIsoCode = edmTitle.CurrencyIsoCode;
            tw.createdById = edmTitle.CreatedById;
            tw.dirNm = edmTitle.DIR_NM__c;
            tw.epNm = edmTitle.EP_NM__c;
            tw.finDivCd = edmTitle.FIN_DIV_CD__c;
            tw.finProdId = edmTitle.FIN_PROD_ID__c;
            tw.frstRelDate = edmTitle.FRST_REL_DATE__c;
            tw.frstRelDateWw = edmTitle.FRST_REL_DATE_WW__c;
            tw.foxId = edmTitle.FOX_ID__c;
            tw.foxVersionId = edmTitle.FOX_VERSION_ID__c;
            tw.createDate = edmTitle.CREATE_DATE__c;
            tw.lastUpdateDate = edmTitle.LAST_UPDATE_DATE__c;
            tw.inProgress = edmTitle.In_Progress__c;
            tw.prodTypCdIntl = edmTitle.PROD_TYP_CD_INTL__c;
            tw.tvdIsBoxStructureCreated = edmTitle.TVD_Is_Box_Structure_Created__c;
            tw.isVam = edmTitle.Is_Vam__c;
            tw.lastModifiedById = edmTitle.LastModifiedById;
            tw.lifeCyclEffDt = edmTitle.LIFE_CYCL_EFF_DT__c;
            tw.lifeCyclStatCd = edmTitle.LIFE_CYCL_STAT_CD__c;
            tw.lifeCyclStatGrpCd = edmTitle.LIFE_CYCL_STAT_GRP_CD__c;
            tw.metadataUrl = edmTitle.Metadata_URL__c;
            tw.origMediaCd = edmTitle.ORIG_MEDIA_CD__c;
            tw.primOrgLngCd = edmTitle.PRIM_ORG_LNG_CD__c;
            tw.ownerId = edmTitle.OwnerId;
            tw.primProdCntryCd = edmTitle.PRIM_PROD_CNTRY_CD__c;
            tw.problemTitle = edmTitle.Problem_Title__c;
            tw.tvdProductBible = edmTitle.TVD_Product_Bible__c;
            tw.prodTypCd = edmTitle.PROD_TYP_CD__c;
            tw.prodTypCdName = edmTitle.PROD_TYP_CD__r.Name;
            tw.prodAcqInd = edmTitle.PROD_ACQ_IND__c;
            tw.prodCalFsclYr = edmTitle.PROD_CAL_FSCL_YR__c;
            tw.prodCalYr = edmTitle.PROD_CAL_YR__c;
            tw.prodEpdseNum = edmTitle.PROD_EPDSE_NUM__c;
            tw.prodFinCd = edmTitle.PROD_FIN_CD__c;
            tw.programRunTime = edmTitle.Program_Run_Time__c;
            tw.programRunTimeMins = edmTitle.Program_Run_Time_mins__c;
            tw.pblcCd = edmTitle.PBLC_CD__c;
            tw.publicCd = edmTitle.PUBLIC_CD__c;
            tw.publishInd = edmTitle.PUBLISH_IND__c;
            tw.rlseCalFsclYr = edmTitle.RLSE_CAL_FSCL_YR__c;
            tw.rlseCalYr = edmTitle.RLSE_CAL_YR__c;
            tw.rowId = edmTitle.ROW_ID__c;
            tw.seasNm = edmTitle.SEAS_NM__c;
            tw.erNm = edmTitle.SER_NM__c;
            tw.titleSrt = edmTitle.TITLE_SRT__c;
            tw.srcPkey = edmTitle.SRC_PKEY__c;
            tw.sysIsConfidential = edmTitle.SYS_IsConfidential__c;
            tw.titleDesc = edmTitle.TITLE_DESC__c;
            tw.titleName = edmTitle.Name;
            tw.titleSubTypeCd = edmTitle.TITLE_SUB_TYPE_CD__c;
            tw.totalGlobalBoxOfficeUsd = edmTitle.Total_Global_Box_Office_USD__c;
            tw.totalUsBoxOffice = edmTitle.Total_US_Box_Office__c;
            tw.tvdAlias = edmTitle.TVD_Alias__c;
            tw.tvdMediaMaestroId = edmTitle.TVD_Media_Maestro_ID__c;
            tw.epNumUnsrt = edmTitle.EP_NUM_UNSRT__c;
            tw.serNmUnsrt = edmTitle.SER_NM_UNSRT__c;
            tw.titleUnsrt = edmTitle.TITLE_UNSRT__c;
            tw.vamSubTypeEdm = edmTitle.VAM_Sub_Type_EDM__c;
            tw.vamTypeEdm = edmTitle.VAM_Type_EDM__c;
            wrapperTitles.add(tw);
        }
        String bodyContent = JSON.serialize(wrapperTitles);
        system.debug(':::: bodyContent :::::'+bodyContent);
        PostTitleHttpRequest(bodyContent);*/
    }
    
    @future(callout=true)
    public static void PostTitleHttpRequest(String body)
    {
        /*//Establish connection
        Optum_API_Settings__c intakeAPI = Optum_API_Settings__c.getInstance('Eligibility API');
        key = intakeAPI.Client_ID__c;
        secret = intakeAPI.Client_Secret__c;
        host = intakeAPI.Host__c;*/
        //endPointURL = 'https://FFEUSWDUXAP415.foxinc.com:5050/api/testAppProps';
        //endPointURL = 'https://jsonplaceholder.typicode.com/posts';
        //endPointURL = 'https://feg-devapi.foxinc.com/rules-engine/newTitleEvent';
        
        List<Rest_Endpoint_Settings__c> endPoints = new List<Rest_Endpoint_Settings__c> (
            [
                SELECT Name, End_Point__c 
                FROM Rest_Endpoint_Settings__c
                WHERE Name = 'Rules Engine - newTitleEvent'
            ]   
        );
        if (endPoints.size() > 0 && endPoints[0].End_Point__c != null) 
        {
            endPointURL = endPoints[0].End_Point__c;
            
            //token = intakeAPI.Token_URL__c;
            String authToken='';
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            HTTPResponse res = new HTTPResponse();
            //authToken = connect();
            request.setMethod('POST');
            request.setHeader('Content-Type', 'application/json');
            request.setHeader('Accept', 'application/json');
            request.setBody(body);
            //request.setHeader('Host', host); 
            request.setEndpoint(endPointURL);
            request.setTimeout(30*1000);
            //request.setHeader('Authorization', 'Bearer ' + authToken);
            system.debug('HTTP Request sent:'+request.getBody());
            if(!Test.isRunningTest())
            {
                    //Execute web service call here
                res = http.send(request);
            }
            else 
            {
                
            }
            //Helpful debug messages
            System.debug('STATUS:' + res.getStatus());
            System.debug('STATUS_CODE:' + res.getStatusCode());
            System.debug('Eligibility Response:' + res.getBody());  
        }
    }
    //LRCC-1653-In this ticket once passed QA it with remove the commented code.
    /*public void createWindowStrandBody(List<EAW_Window_Strand__c> windowStrands){
        
        if( windowStrands != NULL && windowStrands.size() > 0 ){
            
            List<windowStrandWrapper> wswList = new List<windowStrandWrapper>();
            
            for( EAW_Window_Strand__c windowStrand : windowStrands ){
                windowStrandWrapper wsw = new windowStrandWrapper();
                wsw.windowStrandId = windowStrand.Id;
                wsw.media = windowStrand.Media__c;
                wsw.territory = windowStrand.Territory__c;
                wsw.language = windowStrand.Language__c;
                if(windowStrand.Start_Date__c != NULL){
                    wsw.startDate = windowStrand.Start_Date__c;
                }
                if(windowStrand.End_Date__c != NULL){
                    wsw.endDate = windowStrand.End_Date__c;
                }
                wsw.licenseType = windowStrand.License_Type__c;
                wswList.add(wsw);
            }
            String bodyContent = JSON.serialize(wswList);
                system.debug(':::: bodyContent :::::'+bodyContent);
                //LRCC-1239 -
                //PostWindowStrandHttpRequest(bodyContent);
        }
        
    }*/
    
    @future(callout=true)
    public static void PostWindowStrandHttpRequest(String body){
        
        system.debug(':::::: Body :::::::'+body);
        
        //endPointURL = 'https://feg-devapi.foxinc.com/rules-engine/newTitleEvent';
        List<Rest_Endpoint_Settings__c> endPoints = new List<Rest_Endpoint_Settings__c> (
            [
                SELECT Name, End_Point__c 
                FROM Rest_Endpoint_Settings__c
                WHERE Name = 'Rules Engine - newTitleEvent'
            ]   
        );
        if (endPoints.size() > 0 && endPoints[0].End_Point__c != null) 
        {
            endPointURL = endPoints[0].End_Point__c;
            
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            HTTPResponse res = new HTTPResponse();
            request.setMethod('POST');
            request.setHeader('Content-Type', 'application/json');
            request.setHeader('Accept', 'application/json');
            request.setBody(body);
            request.setEndpoint(endPointURL);
            //request.setTimeout(30*1000);
            if(!Test.isRunningTest())
            {
                //Execute web service call here
                res = http.send(request);
            }
            else 
            {
                
            }
        }
        
    }
    
    public void createWindowBody(List<EAW_Window__c> windows){
        
        if( windows != NULL && windows.size() > 0 ){
            
            List<windowWrapper> wList = new List<windowWrapper>();
            
            for( EAW_Window__c window : windows ){
                windowWrapper w = new windowWrapper();
                w.windowId = window.Id;
                if(window.End_Date__c != NULL){
                    w.endDate = window.End_Date__c;
                }
                w.windowName = window.Name;
                if(window.Outside_Date__c != NULL){
                    w.outsideDate = window.Outside_Date__c;
                }
                if(window.Start_Date__c != NULL){
                    w.startDate = window.Start_Date__c;
                }
                if(window.Tracking_Date__c != NULL){
                    w.trackingDate = window.Tracking_Date__c;
                }
                w.windowGuidelineId = window.EAW_Window_Guideline__c;
                wList.add(w);
            }
            String bodyContent = JSON.serialize(wList);
                system.debug(':::: bodyContent :::::'+bodyContent);
                PostWindowHttpRequest(bodyContent);
        }
    }
    
    @future(callout=true)
    public static void PostWindowHttpRequest(String body)
    {
        system.debug(':::::: Body :::::::'+body);
        //endPointURL = 'https://feg-devapi.foxinc.com/rules-engine/calculateWindows';
        
        List<Rest_Endpoint_Settings__c> endPoints = new List<Rest_Endpoint_Settings__c> (
            [
                SELECT Name, End_Point__c 
                FROM Rest_Endpoint_Settings__c
                WHERE Name = 'Rules Engine - calculateWindows'
            ]   
        );
        if (endPoints.size() > 0 && endPoints[0].End_Point__c != null) 
        {
            endPointURL = endPoints[0].End_Point__c;
                
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            HTTPResponse res = new HTTPResponse();
            request.setMethod('POST');
            request.setHeader('Content-Type', 'application/json');
            request.setHeader('Accept', 'application/json');
            request.setBody(body);
            request.setEndpoint(endPointURL);
            if(!Test.isRunningTest())
            {
                //Execute web service call here
                res = http.send(request);
            }
            else 
            {
                
            }       
        }
    }
    public static void windowGuidelineUpdate( Set<Id> windowGuidelineIdSet ){
        
        if( windowGuidelineIdSet != NULL && windowGuidelineIdSet.size() > 0 ){
            
            List<EAW_Window__c> windowList = [ Select Id, Name, Start_Date__c, End_Date__c, Tracking_Date__c, Outside_Date__c From EAW_Window__c Where EAW_Window_Guideline__c IN : windowGuidelineIdSet ];
            
            List<windowStrandUpdate> windowStrandUpdateList = new List<windowStrandUpdate>();
            
            String bodyContent;
            
            if( windowList != NULL && windowList.size() > 0 ){
                
                for(EAW_Window__c window : windowList) {
                    
                    windowStrandUpdate windowWrapper = new windowStrandUpdate();
                    windowWrapper.windowId = window.Id;
                    if(window.Start_Date__c != NULL){
                        windowWrapper.startDate = window.Start_Date__c;
                    }
                    if(window.End_Date__c != NULL){
                        windowWrapper.endDate = window.End_Date__c;
                    }
                    if(window.Tracking_Date__c != NULL){
                        windowWrapper.trackingDate = window.Tracking_Date__c;
                    }
                    if(window.Outside_Date__c != NULL){
                        windowWrapper.outsideDate = window.Outside_Date__c;
                    }
                    windowStrandUpdateList.add(windowWrapper);
                }
            }
            
            if( windowStrandUpdateList != NULL && windowStrandUpdateList.size() > 0 ){
                bodyContent = JSON.serialize(windowStrandUpdateList);
                System.debug(':::::::::: bodyContent :::::::::::'+bodyContent);
            }
        }
    }
    
    //Returns the Auth Token
    public static String connect()
    {
        grant_type = 'client_credentials';
        String requestBody = 'grant_type=' + grant_type +'&client_id=' + key + '&client_secret=' + secret;
        HttpRequest req = new HttpRequest();
        req.setEndpoint(token);
        req.setHeader('Content-Type','application/x-www-form-urlencoded');
        req.setHeader('Host', host);
        req.setHeader('Connection', ' Keep-Alive');
        req.setHeader('scope', 'read');
        req.setMethod('POST');
        req.setBody(requestBody);
        Http http = new Http();
        HTTPResponse response;
        if(!Test.isRunningTest())
        {
            response = http.send(req);
        } 
        else 
        {
            
        }
        String responseBody = response.getBody();
        system.debug('Response Body:'+responseBody);
        String tokenSetter = 'Not Set';
        //tokenSetter = responseBody.auth_token;
        return tokenSetter;
    }
    
    public class titleWrapper {
    
        public string titleId;
        public string edmTitleId;
        public string acquiredIndicator;
        public Decimal actualRunTime;
        public Decimal actualRunTimMins;
        public string approxRunTime;
        public string bcstSeasTwoYrPrd;
        public string currencyIsoCode;
        public string createdById;
        public string dirNm;
        public string epNm;
        public string finDivCd;
        public string finProdId;
        public Date frstRelDate;
        public Date frstRelDateWw;
        public string foxId;
        public string foxVersionId;
        public Date createDate;
        public Date lastUpdateDate;
        public boolean inProgress;
        public string prodTypCdIntl; 
        public Boolean tvdIsBoxStructureCreated;
        public Boolean isVam;
        public string lastModifiedById;
        public Date lifeCyclEffDt;
        public string lifeCyclStatCd;
        public string lifeCyclStatGrpCd;
        public string metadataUrl;
        public string origMediaCd;
        public string primOrgLngCd;
        public string ownerId;
        public string primProdCntryCd;
        public string problemTitle;
        public string tvdProductBible;
        public string prodTypCd;
        public string prodTypCdName;
        public Boolean prodAcqInd;
        public string prodCalFsclYr;
        public string prodCalYr;
        public Decimal prodEpdseNum;
        public string prodFinCd;
        public Decimal programRunTime;
        public Decimal programRunTimeMins;
        public string pblcCd;
        public string publicCd;
        public string publishInd;
        public string rlseCalFsclYr;
        public string rlseCalYr;
        public string rowId;
        public string seasNm;
        public string erNm;
        public string titleSrt;
        public string srcPkey;
        public Boolean sysIsConfidential;
        public string titleDesc;
        public string titleName;
        public string titleSubTypeCd;
        public Decimal totalGlobalBoxOfficeUsd;
        public Decimal totalUsBoxOffice;
        public string tvdAlias;
        public string tvdMediaMaestroId;
        public string epNumUnsrt;
        public string serNmUnsrt;
        public string titleUnsrt;
        public string vamSubTypeEdm;
        public string vamTypeEdm;
    }
    
    public class windowStrandUpdate {
    
        public String windowId;
        public Date startDate;
        public Date endDate;
        public Date trackingDate;
        public Date outsideDate;
    }
    
    public class windowStrandWrapper {
    
        public String windowStrandId;
        public String media;
        public String territory;
        public String language;
        public Date startDate;
        public Date endDate;
        public String licenseType;
        
    }
    public class windowWrapper {
    
        public String windowId;
        public String windowName;
        public String windowGuidelineId;
        public String startDateDrlDef;
        public Date startDate;
        public Date endDate;
        public Date trackingDate;
        public Date outsideDate;
        public String endDateDrlDef;
    }
}