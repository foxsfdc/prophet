@isTest
public class EAW_WindowGuidelineRuleBuildCtrl_Test {
    
    
    @testSetup static void setupTestData() {
          
          //EAW_TestDataFactory.createWindowGuideLine(1, true);
        EAW_TestDataFactory.createEAWCustomer(1, true);
        EAW_TestDataFactory.createReleaseDateType(1, true);
        
        EAW_Window_Guideline__c newWg = new EAW_Window_Guideline__c ( Status__c = 'Draft', Product_Type__c = 'Compilation Episode', Window_Type__c = 'First-Run', Window_Guideline_Alias__c = 'Test');
        insert newWg; 
        EAW_Window_Guideline__c conditionalOperand = new EAW_Window_Guideline__c ( Status__c = 'Draft', Product_Type__c = 'Compilation Episode', Window_Type__c = 'First-Run', Window_Guideline_Alias__c = 'Test wg1');
        insert conditionalOperand;
        
        String newruleContent = '{"sObjectType":"EAW_Rule__c","Nested__c":true}';
        String newruleDetailContent = '[{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Condition_Timeframe__c":"True_Earliest"},"dateCalcs":[{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Conditional_Operand_Id__c":"aA73D000000CyJjSAK","Condition_Met_Months__c":"2","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"},{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Conditional_Operand_Id__c":"aA73D000000CyJjSAK","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"}],"nodeCalcs":[{"ruleDetail":{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":2,"Condition_Timeframe__c":"True_Earliest"},"dateCalcs":[{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":2,"Conditional_Operand_Id__c":"aA73D000000CyJjSAK","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"}],"nodeCalcs":[]}]}]';
        EAW_TestRuleDataFactory.saveRuleAndRuleDetails(newruleContent,newruleDetailContent,false,newWg.Id,'Start Date',String.valueOf(conditionalOperand.Id));
    }
    
    @isTest static void removeSelectedJunctions_Test(){
        
        EAW_Window_Guideline__c newWg = new EAW_Window_Guideline__c ( Status__c = 'Draft', Product_Type__c = 'Compilation Episode', Window_Type__c = 'First-Run', Window_Guideline_Alias__c = 'Test');
        insert newWg;     
        
        List<EAW_Customer__c> customerList = [SELECT Id, Name FROM EAW_Customer__c LIMIT 1];
        
        List<EAW_Release_Date_Type__c> releaseDateTypeList = [SELECT Id, Name FROM EAW_Release_Date_Type__c LIMIT 1];
        //LRCC-1560 - Replace Customer lookup field with Customer text field
        //EAW_Release_Date_Guideline__c releaseDateGuideLine = new EAW_Release_Date_Guideline__c( Product_Type__c = 'Compilation Episode', Territory__c = 'United States +', Language__c = 'English', Customer__c = customerList[0].Id, EAW_Release_Date_Type__c = 'Television');
        EAW_Release_Date_Guideline__c releaseDateGuideLine = new EAW_Release_Date_Guideline__c( Product_Type__c = 'Compilation Episode', Territory__c = 'United States +', Language__c = 'English', Customers__c = customerList[0].Name, EAW_Release_Date_Type__c = 'Television');
        insert releaseDateGuideLine;
        
        releaseDateGuideLine.Active__c =  true;
        update releaseDateGuideLine;
        
        //List<EAW_Window_Guideline__c> edmWindowGuidelineList = [SELECT Id, Name FROM EAW_Window_Guideline__c LIMIT 1];
        //edmWindowGuidelineList[0].Product_Type__c = 'Compilation Episode';
        //update edmWindowGuidelineList;
        
        EAW_WindowGuidelineRuleBuilderController.getRulesAndDetails(String.ValueOf(newWg.Id), null);
    }
    
     @isTest static void saveWindowRuleAndRuleDetails_Test(){
         
         List<EAW_Window_Guideline__c> windowIns = EAW_TestDataFactory.createWindowGuideLine(1, false);
         windowIns[0].Name = 'Test window Guideline record';
         windowIns[0].Status__c = 'Draft';
         insert windowIns;
         
         List<EAW_Rule__c> ruleList = EAW_TestDataFactory.createEAWRule(1, true);
         ruleList[0].Window_Guideline__c = windowIns[0].Id;
         ruleList[0].Date_To_Modify__c = 'Start Date';
         update ruleList;
         
         EAW_Rule_Detail__c ruleDetailParent = new EAW_Rule_Detail__c (Rule_No__c = ruleList[0].Id);
         insert ruleDetailParent;
         
         List<EAW_Rule_Detail__c> ruleDetailList = EAW_TestDataFactory.createRuleDetail(1, true);
         ruleDetailList[0].Parent_Rule_Detail__c = ruleDetailParent.Id;
         update ruleDetailList;
         
         List<String> ruleDetailStringList = new List<String>();
         
         for(EAW_Rule_Detail__c ruledetail :  ruleDetailList ) {
             
             ruleDetailStringList.add(ruledetail.Id);
         }   
         
        List<List<EAW_WindowGuidelineRuleBuilderController.ruleDetailNodeWrapper>> ruleDetailNodeWrapperList = new List<List<EAW_WindowGuidelineRuleBuilderController.ruleDetailNodeWrapper>>();

        List<EAW_WindowGuidelineRuleBuilderController.ruleDetailNodeWrapper> ruleDetailWrapperNodeList = new List<EAW_WindowGuidelineRuleBuilderController.ruleDetailNodeWrapper>();
        
         EAW_WindowGuidelineRuleBuilderController.ruleDetailNodeWrapper ruleDetailNode = new EAW_WindowGuidelineRuleBuilderController.ruleDetailNodeWrapper();
         ruleDetailNode.ruleDetail = ruleDetailParent;
         ruleDetailNode.dateCalcs = ruleDetailList; 
         ruleDetailWrapperNodeList.add(ruleDetailNode);     
                         
         EAW_WindowGuidelineRuleBuilderController.ruleDetailNodeWrapper ruleDetailNodeWrapper = new EAW_WindowGuidelineRuleBuilderController.ruleDetailNodeWrapper();
         ruleDetailNodeWrapper.ruleDetail = ruleDetailParent;
         ruleDetailNodeWrapper.dateCalcs = ruleDetailList;        
         ruleDetailNodeWrapper.nodeCalcs = ruleDetailWrapperNodeList;
                  
         ruleDetailNodeWrapperList.add(ruleDetailNodeWrapper.nodeCalcs);
         
         EAW_WindowGuidelineRuleBuilderController.ruleDetailNodeWrapper rulewrapper = new EAW_WindowGuidelineRuleBuilderController.ruleDetailNodeWrapper();
         rulewrapper.ruleDetail = ruleDetailParent;         
         
         List<EAW_Rule__c> ruleListObt = EAW_WindowGuidelineRuleBuilderController.saveWindowRuleAndRuleDetails(JSON.serialize(ruleList), JSON.serialize(ruleDetailNodeWrapperList), ruleDetailStringList, JSON.serialize(new Map<String,String>{'Start Date' => 'Date Calculation : 6 Months 7 Days of EST-HD / Angola / Albanian / Feature;'}), null , null);
         
         System.assertEquals(True, ruleListObt != null);
     }
    @isTest static void saveWindowGuidelineRules_Test(){
        EAW_Window_Guideline__c newWg = new EAW_Window_Guideline__c ( Status__c = 'Draft', Product_Type__c = 'Compilation Episode', Window_Type__c = 'First-Run', Window_Guideline_Alias__c = 'Test');
        insert newWg;
        
        EAW_Window_Guideline__c conditionalOperand = new EAW_Window_Guideline__c ( Status__c = 'Draft', Product_Type__c = 'Compilation Episode', Window_Type__c = 'First-Run', Window_Guideline_Alias__c = 'Test wg1');
        insert conditionalOperand;
        
        String ruleDetailContent = '[[{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Condition_Timeframe__c":"True_Earliest"},"dateCalcs":[{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Conditional_Operand_Id__c":"aA73D000000CyJjSAK","Condition_Met_Months__c":"2","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"},{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Conditional_Operand_Id__c":"aA73D000000CyJjSAK","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"}],"nodeCalcs":[{"ruleDetail":{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":2,"Condition_Timeframe__c":"True_Earliest"},"dateCalcs":[{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":2,"Conditional_Operand_Id__c":"aA73D000000CyJjSAK","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"}],"nodeCalcs":[]}]}]]';
        ruleDetailContent.replaceAll('aA73D000000CyJjSAK',String.valueOf(conditionalOperand.Id));
        
        String ruleContent = '[{"sObjectType":"EAW_Rule__c","Nested__c":true}]';
        
        //newWg.Name= 'test WG ';
        //update newWg;
        
        List<EAW_Rule__c> ruleListObt = EAW_WindowGuidelineRuleBuilderController.saveWindowRuleAndRuleDetails(ruleContent, ruleDetailContent, null, JSON.serialize(new Map<String,String>{'Start Date' => 'Date Calculation : 6 Months 7 Days of EST-HD / Angola / Albanian / Feature;','End Date' => 'test','Outside Date' => 'test','Tracking Date' => 'test'}), newWg.Id , null);
        List<String> ruleDetilsToDelete = new List<String>();
        String ruleId = '';
        for(EAW_Rule_Detail__c ruleDetail : [SELECT Id,Rule_No__c FROM EAW_Rule_Detail__c]){
            ruleDetilsToDelete.add(ruleDetail.Id);
            ruleId = ruleDetail.Rule_No__c;
        }
        
        String newruleContent = '{"sObjectType":"EAW_Rule__c","Nested__c":true}';
        String newruleDetailContent = '[{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Condition_Timeframe__c":"True_Earliest"},"dateCalcs":[{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Conditional_Operand_Id__c":"aA73D000000CyJjSAK","Condition_Met_Months__c":"2","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"},{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Conditional_Operand_Id__c":"aA73D000000CyJjSAK","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"}],"nodeCalcs":[{"ruleDetail":{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":2,"Condition_Timeframe__c":"True_Earliest"},"dateCalcs":[{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":2,"Conditional_Operand_Id__c":"aA73D000000CyJjSAK","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"}],"nodeCalcs":[]}]}]';
        EAW_TestRuleDataFactory.saveRuleAndRuleDetails(newruleContent,newruleDetailContent,false,newWg.Id,'Start Date',String.valueOf(conditionalOperand.Id));
        EAW_WindowGuidelineRuleBuilderController.deleteRuleAndDetails(false,false, null,ruleDetilsToDelete,newWg.Id,null);
        EAW_WindowGuidelineRuleBuilderController.deleteRuleAndDetails(true,false, ruleId,null,null,null);
    }
    @isTest static void windowOperandAndStatus_Test(){
        EAW_Window_Guideline__c newWg = new EAW_Window_Guideline__c ( Status__c = 'Draft', Product_Type__c = 'Compilation Episode', Window_Type__c = 'First-Run', Window_Guideline_Alias__c = 'Test');
        insert newWg; 
        EAW_Window_Guideline_Strand__c windowGuideLineStrand =  new EAW_Window_Guideline_Strand__c(Language__c = 'Tamil',Media__c='Basic TV ',Territory__c='Afghanistan', EAW_Window_Guideline__c = newWg.Id, License_Type__c = 'Exhibition License');
        insert windowGuideLineStrand;
        newWg.Status__c = 'Active';
        update newWg;
        
        EAW_Window_Guideline__c conditionalOperand = new EAW_Window_Guideline__c ( Status__c = 'Draft', Product_Type__c = 'Compilation Episode', Window_Type__c = 'First-Run', Window_Guideline_Alias__c = 'Test wg1');
        insert conditionalOperand;
        List<EAW_Customer__c> customerList = [SELECT Id, Name FROM EAW_Customer__c LIMIT 1];
        EAW_Release_Date_Guideline__c releaseDateGuideLine = new EAW_Release_Date_Guideline__c( Product_Type__c = 'Compilation Episode', Territory__c = 'United States +', Language__c = 'English', Customers__c = customerList[0].Name, EAW_Release_Date_Type__c = 'Television');
        insert releaseDateGuideLine; 
        String newruleContent = '{"sObjectType":"EAW_Rule__c","Nested__c":true}';
        String newruleDetailContent = '[{"ruleDetail":{"sObjecType":"EAW_Rule_Detail__c","Condition_Timeframe__c":"True_Earliest"},"dateCalcs":[{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Conditional_Operand_Id__c":"'+releaseDateGuideLine.Id+'","Condition_Met_Months__c":"2","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"},{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":1,"Conditional_Operand_Id__c":"'+conditionalOperand.Id+'","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"}],"nodeCalcs":[{"ruleDetail":{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":2,"Condition_Timeframe__c":"True_Earliest"},"dateCalcs":[{"sObjectType":"EAW_Rule_Detail__c","Rule_Order__c":1,"Nest_Order__c":2,"Conditional_Operand_Id__c":"aA73D000000CyJjSAK","Condition_Met_Months__c":"1","Condition_Met_Days__c":"0","Condition_Operand_Details__c":"EST-HD / Angola / Albanian / Feature"}],"nodeCalcs":[]}]}]';
        EAW_TestRuleDataFactory.saveRuleAndRuleDetails(newruleContent,newruleDetailContent,false,newWg.Id,'Start Date',null);
        
        EAW_Title__c title = new EAW_Title__c();
        title.Name = 'Test Title';
        insert title;
        EAW_Release_Date__c releaseDate = new EAW_Release_Date__c();
        releaseDate.Title__c = title.Id;
        releaseDate.Release_Date_Guideline__c = releaseDateGuideLine.Id;
        
        EAW_Window__c window = new EAW_Window__c (Name='Test E', EAW_Title_Attribute__c = title.Id, Window_Type__c = 'Library',EAW_Window_Guideline__c = newWg.Id);
        insert window;
        
        List<EAW_Rule_Detail__c> parentRuleDetail = [SELECT Id,Conditional_Operand_Id__c FROM EAW_Rule_Detail__c WHERE Parent_Rule_Detail__c = null];
        List<EAW_Rule_Detail__c> childRuleDetail = [SELECT Id,Conditional_Operand_Id__c FROM EAW_Rule_Detail__c WHERE Parent_Rule_Detail__c != null];
        EAW_WindowGuidelineRuleBuilderController.conditionalOperandDateAndStatus(parentRuleDetail,childRuleDetail,window.Id);
    }
}