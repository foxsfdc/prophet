public class EAW_NewWindowController {
   
    public EAW_Window__c window {get;set;}
    public String windowGuidelineId {get;set;}  
    public String windowGuidelineName {get;set;}  
         
    public EAW_NewWindowController(ApexPages.StandardController controller) {
    
        window = (EAW_Window__c)controller.getRecord();
        windowGuidelineId = window.EAW_Window_Guideline__c;
        if(String.isNotBlank(windowGuidelineId)) {
            windowGuidelineName = [SELECT Id, Window_Guideline_Alias__c FROM EAW_Window_Guideline__c WHERE Id =:windowGuidelineId].Window_Guideline_Alias__c;
        }
        system.debug(';;;;;;;;;;;'+windowGuidelineName);
    }
}