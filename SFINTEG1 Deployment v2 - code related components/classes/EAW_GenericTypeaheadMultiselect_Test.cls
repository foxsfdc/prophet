@isTest
public class EAW_GenericTypeaheadMultiselect_Test {
    
    @testSetup static void setup() {
        
        EAW_TestDataFactory.createTag(1, true, 'Window Tag');
        EAW_TestDataFactory.createEAWCustomer(1, true);
        EAW_TestDataFactory.createPlanGuideline(1, true);
        EAW_TestDataFactory.createEAWTitle (1, true);
        EAW_TestDataFactory.createWindowGuideLine (1, true);
        
    }
    
    @isTest static void search_Test() {
    
        List<EAW_LookupSearchResult> tagList = EAW_GenericTypeaheadMultiselect.search('EAW_Tag__c', 'Window Schedule Tag', 'Test', FALSE);
        
        List<EAW_LookupSearchResult> tagListNull = EAW_GenericTypeaheadMultiselect.search('EAW_Tag__c', 'Window Schedule Tag', '', FALSE);
        
        System.assertEquals(True, tagList != null);
    }
    @isTest static void search_TestCustomer() {
    
        List<EAW_LookupSearchResult> customerList = EAW_GenericTypeaheadMultiselect.search('EAW_Customer__c', '', 'Test', FALSE);
        
        List<EAW_LookupSearchResult> customerListNull = EAW_GenericTypeaheadMultiselect.search('EAW_Customer__c', '', '', FALSE);
        
        System.assertEquals(True, customerList != null);
    }
    @isTest static void search_TestPlan() {
        
        List<EAW_LookupSearchResult> planList = EAW_GenericTypeaheadMultiselect.search('EAW_Plan_Guideline__c', 'planWithoutAnyCondition', 'Test', FALSE);
        
        List<EAW_LookupSearchResult> planListNull = EAW_GenericTypeaheadMultiselect.search('EAW_Plan_Guideline__c', 'planWithoutAnyCondition', '', FALSE);
        
        System.assertEquals(True, planList != null);
        
        List<EAW_LookupSearchResult> planListNull1 = EAW_GenericTypeaheadMultiselect.search('EAW_Plan_Guideline__c', '', 'Test', FALSE);
    }
    @isTest static void search_TestTitle() {
    
        List<EAW_LookupSearchResult> titleList = EAW_GenericTypeaheadMultiselect.search('EAW_Title__c', '', 'Test', FALSE);
        
        List<EAW_LookupSearchResult> titleListNull = EAW_GenericTypeaheadMultiselect.search('EAW_Title__c', '', '', FALSE);
        
        System.assertEquals(True, titleList != null);
    }
    @isTest static void search_TestWindow() {
        
        List<EAW_Window_Guideline__c> edmWindowGuidelineList = [SELECT id, Name FROM EAW_Window_Guideline__c LIMIT 1];
        
        EAW_Window__c ruleDetail = new EAW_Window__c (Name='Test E', EAW_Window_Guideline__c = edmWindowGuidelineList[0].Id);
    
        List<EAW_LookupSearchResult> windowList = EAW_GenericTypeaheadMultiselect.search('EAW_Window__c', '', 'Test', FALSE);    
        
        System.assertEquals(True, windowList != null);
    }
    @isTest static void search_Testnull() {
    
        try {
            EAW_GenericTypeaheadMultiselect.search('', '', 'Test', FALSE);   
        } catch(Exception ex) {
        } 
        
    } 
    @isTest static void search_TestwindowGuideline() {
    
        EAW_GenericTypeaheadMultiselect.search('EAW_Window_Guideline__c', '', 'Test', FALSE); 
        EAW_GenericTypeaheadMultiselect.search('EAW_Window_Guideline__c', '', 'Test', TRUE);    
        EAW_GenericTypeaheadMultiselect.search('EAW_Window_Guideline__c', 'windowWithoutAnyCondition', 'Test', TRUE);  
        EAW_GenericTypeaheadMultiselect.search('EAW_Window_Guideline__c', '', '', NULL);
    }   
    
    @isTest static void search_TestEDMREFDIVISIONSearch() {
        EAW_GenericTypeaheadMultiselect.search('EDM_REF_DIVISION__c', '', 'Test', FALSE);  
    }
    
    @isTest static void search_TestProductTypeSearch() {
        EAW_GenericTypeaheadMultiselect.search('EDM_REF_PRODUCT_TYPE__c', '', 'Feature', FALSE);  
    }
    
    @isTest static void search_TestTerritoriesSearch() {
        EAW_GenericTypeaheadMultiselect.search('EAW_Territories__c', '', 'India', FALSE);  
    }
    
    @isTest static void search_TestReleaseDateTypeSearch() {
        EAW_GenericTypeaheadMultiselect.search('EAW_Release_Date_Type__c', '', 'Theatrical', FALSE);  
    }
    
    @isTest static void search_TestEdmTitleSearch() {
        EAW_GenericTypeaheadMultiselect.search('EDM_GLOBAL_TITLE__c', '', 'Theatrical', FALSE);  
    }
}