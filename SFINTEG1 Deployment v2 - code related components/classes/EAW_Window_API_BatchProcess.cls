global class EAW_Window_API_BatchProcess implements Database.AllowsCallouts,Database.Batchable<sObject> {
    global boolean Skip_GALILEO_RIGHTS_CHECK_API = false;
    public List<Id> windowIdList = new List<Id>();
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(
            'SELECT '+
                'Customers__c, '+
                'EAW_Plan__c, '+
                'Soft_Deleted__c, '+
                'EAW_Plan__r.EAW_Title__c, '+
                'EAW_Plan__r.Product_Type__c, '+
                'EAW_Window_Guideline__c, '+
                'EAW_Window_Guideline__r.Next_Version__c, '+
                'EAW_Window_Guideline__r.Previous_Version__c, '+
                'EAW_Window_Guideline__r.Status__c, '+
                'EAW_Window_Guideline__r.Version__c, '+
                'LastModifiedBy.name, '+
                'Media__c, '+
                'Retired__c, '+
                'Start_Date__c,'+ 
                'Status__c, '+
                'Window_Type__c, '+
                'EAW_Plan__r.id, '+
                'id, '+
                'EAW_Window_Guideline__r.id, '+
                'name, '+
                'EAW_Plan__r.name,'+ 
                'EAW_Window_Guideline__r.name, '+
                'EAW_Title_Attribute__r.id, '+
                'EAW_Title_Attribute__r.FIN_PROD_ID__c, '+
                'EAW_Title_Attribute__r.FOX_ID__c, '+
                'EAW_Title_Attribute__r.Product_Version_Id__c, '+
                'EAW_Title_Attribute__r.name, '+
                'EAW_Window_Guideline__r.Start_Date_Rule__c, '+
                'End_Date__c, '+
                'EAW_Window_Guideline__r.End_Date_Rule__c, '+ 
                'Outside_Date__c, '+ 
                'EAW_Window_Guideline__r.Outside_Date_Rule__c, '+ 
                'Tracking_Date__c, '+ 
                'EAW_Window_Guideline__r.Tracking_Date_Rule__c, '+ 
                'Right_Status__c, '+ 
                'License_Info_Codes__c, '+ 
                'LastModifiedDate, '+ 
                'Repo_DML_Type__c, '+ 
                'Sent_To_Repo__c, '+ 
                'Galileo_DML_Type__c, '+ 
                'Sent_To_Galileo__c, '+ 
                'Send_To_Third_Party__c, '+
                'Sent_To_Galileo_RC__c, '+ 
                'Galileo_RC_DML_Type__c, '+ 
                'Start_DATE_TEXT__C, '+
                'END_DATE_TEXT__C, '+
                'performRightsCheck__c '+
            'FROM '+
                'EAW_Window__c '+
            'WHERE '+
                'Send_To_Third_Party__c  =  true '+
                'AND ID IN :windowIdList'
            //',EAW_Plan__r.EAW_Title__c,EAW_Plan__r.EAW_Title__r.Id,EAW_Plan__r.EAW_Title__r.Name,EAW_Plan__r.EAW_Title__r.FOX_ID__c, '+//Title detail column used in Rights check
        );
    }
    global void execute(Database.BatchableContext bc, List<EAW_Window__c> records){
        try{
            /*
            if(!Skip_GALILEO_RIGHTS_CHECK_API){
                //To do Call galileo Rights check
                EAW_RightsCheckCalloutController.genericRightsCheckRequestBuilder(records, new Set<id>(),false,'',new Map<Id, EAW_Title__c>(),new Map<Id,EAW_Window_Guideline__c>(),true);
            }*/
            EAW_Windows_Outbound ewo = new EAW_Windows_Outbound(Skip_GALILEO_RIGHTS_CHECK_API);
            checkRecursiveData.bypassDateCalculation=true;
            //TO do: currently QA team will test single windows manually, commenting below link to skip automatic process.
            ewo.aggregateAllWs(records);
        }catch(Exception e){
            String message = e.getMessage();
            //System.debug('e.getMessage(): '+e.getMessage());
            if(message!=null && message.length()>250)message=message.substring(0, 250);
            
        }
        
    }    
    global void finish(Database.BatchableContext bc){
        // TO do Call Window Audit API batch api
        system.debug('EAW_Window_API_BatchProcess completed');        
        
    }    
}