/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class EAW_WindowPlanGuidelineController_Test {

    static testMethod void myUnitTest() {
        Integer noOfRecords = 2;
        
        List<EAW_Plan_Guideline__c> planGuideList = EAW_TestDataFactory.createPlanGuideline(noOfRecords,true);
        System.debug('planGuideList: '+planGuideList);
        List<String> planGuidelineIds = new List<String>();
        
        if(planGuideList!=null && !planGuideList.isEmpty()){
            for(EAW_Plan_Guideline__c planGuide:planGuideList){
                planGuidelineIds.add(planGuide.Id);
            }
        }
        
        List<Map<String, Object>> dynamicFieldsAndValuesMapList= new  List<Map<String, Object>>();
        List<EAW_Window_Guideline__c> edmWindowGuidelineList = EAW_TestDataFactory.createWindowGuideLine(noOfRecords,true);
        String sObjectString = 'EAW_Plan_Window_Guideline_Junction__c';
        for(integer i=1;i<=noOfRecords;i++){
            Map<String, Object> keyValueMap = new Map<String, Object>();
            keyValueMap.put('Plan__c',planGuidelineIds[i-1]);
            keyValueMap.put('Window_Guideline__c',edmWindowGuidelineList[i-1].Id);
            System.debug('keyValueMap: '+keyValueMap);
            dynamicFieldsAndValuesMapList.add(keyValueMap);
        }
        List<EAW_Plan_Window_Guideline_Junction__c> edmPlanWindowGuideLineJunctionList = EAW_TestDataFactory.createGenericDifferentTestRecords(dynamicFieldsAndValuesMapList,sObjectString);
        for(EAW_Plan_Window_Guideline_Junction__c edmPlanWindowGuideLineJunction:edmPlanWindowGuideLineJunctionList){
            List<EAW_Plan_Guideline__c> planGuidelines = EAW_WindowPlanGuidelineController.getPlanGuidelines(edmPlanWindowGuideLineJunction.Window_Guideline__c);
            System.assertEquals(true, planGuidelines != null);
        }
    }
}