@isTest
public class EAW_TitleTagJunctionTrigger_Test {
    
    @isTest static void processTitleTagToCheckForPlanQualification(){
        
        EDM_REF_PRODUCT_TYPE__c refProduct = new EDM_REF_PRODUCT_TYPE__c(Name = 'Compilation Episode');
        insert refProduct;
        //1250-Replace Title EDM with Title Attribute       
        //List<EDM_GLOBAL_TITLE__c> edmTitleList = EAW_TestDataFactory.createEDMTitle(1, true);
        //edmTitleList[0].PROD_TYP_CD_INTL__c = refProduct.id;
        //update edmTitleList;
        
        List<EAW_Title__c> titleAttribute = EAW_TestDataFactory.createEAWTitle(1, true);
       // titleAttribute[0].Title_EDM__c = edmTitleList[0].Id;
       // update titleAttribute;
        
        RecordType recType = [Select Id from RecordType WHERE Name = 'Qualifier' AND SObjectType = 'EAW_Rule__c'];
        
        List<EAW_Plan_Guideline__c> planGuideLineList = EAW_TestDataFactory.createPlanGuideline(1, true);
        planGuideLineList[0].Product_Type__c = 'Compilation Episode';
        update planGuideLineList;
        
        EAW_Tag__c tagInst = new EAW_Tag__c(Tag_Type__c = 'Title Tag');
        insert tagInst;
        
         EAW_Rule__c ruleInstance = new EAW_Rule__c(RecordTypeId = recType.Id, Plan_Guideline__c = planGuideLineList[0].Id);
        insert ruleInstance;
        
        EAW_Rule_Detail__c ruledetailList = new EAW_Rule_Detail__c(Rule_No__c = ruleInstance.Id, Condition_Field__c = 'Title Tag', Condition_Tag__c = 'Blockbuster');
        insert ruledetailList;
        
        EAW_Title_Tag_Junction__c titleTagInstance = new EAW_Title_Tag_Junction__c(Title_Attribute__c = titleAttribute[0].Id, Tag__c = tagInst.Id);
        insert titleTagInstance;   
        
        List<EAW_Title_Tag_Junction__c> obtainedList = [SELECT Id, Name FROM EAW_Title_Tag_Junction__c LIMIT 1];
        delete obtainedList;             
       
    }
    
    @isTest static void filterTitleTagToCheckForPlanQualification(){
                
        EDM_REF_PRODUCT_TYPE__c refProduct = new EDM_REF_PRODUCT_TYPE__c(Name = 'Compilation Episode');
        insert refProduct;
        
        CollaborationGroup chatterGroup = new CollaborationGroup (
             Name = 'Test2 Review',
             CollaborationType = 'Public'
         );
         insert chatterGroup;
         
         EAW_Notification_Status__c ns = new EAW_Notification_Status__c(
             TACategoryNotification__c = TRUE, 
             TATitlePlanDisQualifierNotification__c = TRUE,
             TACreateNotification__c = TRUE,
             TAProductTypeNotification__c = TRUE,
             TADivisionNotification__c = TRUE,
             TAStatusNotification__c = TRUE,
             TAContentOwnerChangeNotifcation__c = TRUE
         );
         insert ns;
        
        //1250-Replace Title EDM with Title Attribute        
        //List<EDM_GLOBAL_TITLE__c> edmTitleList = EAW_TestDataFactory.createEDMTitle(1, true);
        //edmTitleList[0].PROD_TYP_CD_INTL__c = refProduct.id;
        //update edmTitleList;
        
        List<EAW_Title__c> titleAttribute = EAW_TestDataFactory.createEAWTitle(1, true);
       // titleAttribute[0].Title_EDM__c = edmTitleList[0].Id;
       // update titleAttribute;
        
        Id releaseDateRuleRecordTypeId = Schema.SObjectType.EAW_Rule__c.getRecordTypeInfosByName().get('Qualifier').getRecordTypeId(); 
        
        List<EAW_Plan_Guideline__c> planinstanceList = EAW_TestDataFactory.createPlanGuideline(1, false); 
        planinstanceList[0].Name = 'Test plan guideline Next';
        planinstanceList[0].Product_Type__c = 'Feature';
        insert planinstanceList;
              
        List<EAW_Window_Guideline__c> windowGuidelineList = EAW_TestDataFactory.createWindowGuideLine(1, false);
        windowGuidelineList[0].Status__c = 'Draft';
        windowGuidelineList[0].Name = 'Test window GuideLine';
        windowGuidelineList[0].Product_Type__c = 'Feature';
        windowGuidelineList[0].Window_Type__c = 'First-Run';
        insert windowGuidelineList[0];
        
        List<EAW_Window_Guideline_Strand__c> windowGuideLineStrandList = EAW_TestDataFactory.createWindowGuideLineStrand(2, false);
        windowGuideLineStrandList[0].License_Type__c = 'Exhibition License';
        windowGuideLineStrandList[0].EAW_Window_Guideline__c = windowGuideLineList[0].Id;
        windowGuideLineStrandList[0].Media__c = 'Theatrical';
        windowGuideLineStrandList[0].Territory__c = 'Afghanistan'; 
        windowGuideLineStrandList[0].Language__c = 'Afrikaans';
        insert windowGuideLineStrandList; 
        
        windowGuidelineList[0].Status__c = 'Active';
        update windowGuidelineList[0];
        
        EAW_Window__c winowIns = new EAW_Window__c();
        winowIns.Name='Test window';
        winowIns.EAW_Window_Guideline__c = windowGuidelineList[0].Id;
        winowIns.EAW_Title_Attribute__c = titleAttribute[0].Id;
        winowIns.Window_Type__c = 'Library';
        insert winowIns; 
        
        EAW_Window_Strand__c windowStrandIns = new EAW_Window_Strand__c();
        windowStrandIns.Territory__c = 'Afghanistan';
        windowStrandIns.Language__c = 'Afrikaans';
        windowStrandIns.Media__c = 'Theatrical';
        windowStrandIns.License_Type__c = 'Exhibition License';
        windowStrandIns.Window__c = winowIns.Id;        
        insert windowStrandIns;   
        
        List<EAW_Plan_Window_Guideline_Junction__c> planWindowList = new List<EAW_Plan_Window_Guideline_Junction__c> {
            new EAW_Plan_Window_Guideline_Junction__c(Plan__c = planinstanceList[0].Id , Window_Guideline__c = windowGuideLineList[0].Id)
        };
        insert planWindowList;

        planinstanceList[0].Status__c = 'Active';      
        update planinstanceList;
        
        List<EAW_Tag__c> tagInst = EAW_TestDataFactory.createTag(2, false,'Title Tag');
        tagInst[0].Name = 'Test Tag 1';
        tagInst[0].Tag_Type__c = 'Title Tag';
        insert tagInst[0];
        
        tagInst[1].Name = 'Test Tag 2';
        tagInst[1].Tag_Type__c = 'Title Tag';
        insert tagInst[1];
        
        List<EAW_Customer__c> customerList = EAW_TestDataFactory.createEAWCustomer(1,true);
        
        List<EAW_Release_Date_Type__c> releaseDateTypeList = EAW_TestDataFactory.createReleaseDateType(1, true);
        //LRCC-1560 - Replace Customer lookup field with Customer text field        
        //EAW_Release_Date_Guideline__c releaseDateGuideLine = new EAW_Release_Date_Guideline__c( Product_Type__c = 'Feature', Territory__c = 'United States +', Language__c = 'Dutch', Customer__c = customerList[0].Id, EAW_Release_Date_Type__c = 'SPVOD' ,
        //Active__c = true);
        EAW_Release_Date_Guideline__c releaseDateGuideLine = new EAW_Release_Date_Guideline__c( Product_Type__c = 'Feature', Territory__c = 'United States +', Language__c = 'Dutch', Customers__c = customerList[0].name, EAW_Release_Date_Type__c = 'SPVOD' ,
        Active__c = true);
        insert releaseDateGuideLine;
        
        EAW_Rule__c ruleInstance = new EAW_Rule__c(RecordTypeId = releaseDateRuleRecordTypeId, Plan_Guideline__c = planinstanceList[0].Id);
        insert ruleInstance;
        
        EAW_Rule_Detail__c ruledetailList = new EAW_Rule_Detail__c(Rule_No__c = ruleInstance.Id, Condition_Field__c = 'Title Tag', Condition_Tag__c = 'Blockbuster',
        Condition_Criteria__c = tagInst[1].Id);
        insert ruledetailList;
        
        EAW_Title_Tag_Junction__c titleTagInstance = new EAW_Title_Tag_Junction__c(Title_Attribute__c = titleAttribute[0].Id, Tag__c = tagInst[0].Id);
        insert titleTagInstance;
        
        titleTagInstance.Tag__c = tagInst[1].Id;
        update titleTagInstance;
        
        //titleTagInstance.Title__c = edmTitleList[0].Id;
        titleTagInstance.Title_Attribute__c = titleAttribute[0].Id;
        update titleTagInstance;
        
    }
     @isTest static void filterTitleTagToCheckForPlanQualification_Disqualify_Test(){
         
         CollaborationGroup chatterGroup = new CollaborationGroup (
             Name = 'Test2 Review',
             CollaborationType = 'Public'
         );
         insert chatterGroup;
         
         EAW_Notification_Status__c ns = new EAW_Notification_Status__c(
             TACategoryNotification__c = TRUE, 
             TATitlePlanDisQualifierNotification__c = TRUE,
             TACreateNotification__c = TRUE,
             TAProductTypeNotification__c = TRUE,
             TADivisionNotification__c = TRUE,
             TAStatusNotification__c = TRUE,
             TAContentOwnerChangeNotifcation__c = TRUE
         );
         insert ns;
                
        EDM_REF_PRODUCT_TYPE__c refProduct = new EDM_REF_PRODUCT_TYPE__c(Name = 'Compilation Episode');
        insert refProduct;
        //1250-Replace Title EDM with Title Attribute        
        //List<EDM_GLOBAL_TITLE__c> edmTitleList = EAW_TestDataFactory.createEDMTitle(1, true);
        //edmTitleList[0].PROD_TYP_CD_INTL__c = refProduct.id;
        //update edmTitleList;
        
        List<EAW_Title__c> titleAttribute = EAW_TestDataFactory.createEAWTitle(1, true);
       // titleAttribute[0].Title_EDM__c = edmTitleList[0].Id;
       // update titleAttribute;
        
        Id releaseDateRuleRecordTypeId = Schema.SObjectType.EAW_Rule__c.getRecordTypeInfosByName().get('Qualifier').getRecordTypeId(); 
        
        List<EAW_Plan_Guideline__c> planinstanceList = EAW_TestDataFactory.createPlanGuideline(1, false); 
        planinstanceList[0].Name = 'Test plan guideline Next';
        planinstanceList[0].Product_Type__c = 'Feature';
        insert planinstanceList;
              
        List<EAW_Window_Guideline__c> windowGuidelineList = EAW_TestDataFactory.createWindowGuideLine(1, false);
        windowGuidelineList[0].Status__c = 'Draft';
        windowGuidelineList[0].Name = 'Test window GuideLine';
        windowGuidelineList[0].Product_Type__c = 'Feature';
        windowGuidelineList[0].Window_Type__c = 'First-Run';
        insert windowGuidelineList[0];
        
        List<EAW_Window_Guideline_Strand__c> windowGuideLineStrandList = EAW_TestDataFactory.createWindowGuideLineStrand(2, false);
        windowGuideLineStrandList[0].License_Type__c = 'Exhibition License';
        windowGuideLineStrandList[0].EAW_Window_Guideline__c = windowGuideLineList[0].Id;
        windowGuideLineStrandList[0].Media__c = 'Theatrical';
        windowGuideLineStrandList[0].Territory__c = 'Afghanistan'; 
        windowGuideLineStrandList[0].Language__c = 'Afrikaans';
        insert windowGuideLineStrandList; 
        
        windowGuidelineList[0].Status__c = 'Active';
        update windowGuidelineList[0];
        
        EAW_Window__c winowIns = new EAW_Window__c();
        winowIns.Name='Test window';
        winowIns.EAW_Window_Guideline__c = windowGuidelineList[0].Id;
        winowIns.EAW_Title_Attribute__c = titleAttribute[0].Id;
        winowIns.Window_Type__c = 'Library';
        insert winowIns; 
        
        EAW_Window_Strand__c windowStrandIns = new EAW_Window_Strand__c();
        windowStrandIns.Territory__c = 'Afghanistan';
        windowStrandIns.Language__c = 'Afrikaans';
        windowStrandIns.Media__c = 'Theatrical';
        windowStrandIns.License_Type__c = 'Exhibition License';
        windowStrandIns.Window__c = winowIns.Id;        
        insert windowStrandIns;   
        
        List<EAW_Plan_Window_Guideline_Junction__c> planWindowList = new List<EAW_Plan_Window_Guideline_Junction__c> {
            new EAW_Plan_Window_Guideline_Junction__c(Plan__c = planinstanceList[0].Id , Window_Guideline__c = windowGuideLineList[0].Id)
        };
        insert planWindowList;

        planinstanceList[0].Status__c = 'Active';      
        update planinstanceList;
        
        List<EAW_Tag__c> tagInst = EAW_TestDataFactory.createTag(2, false,'Title Tag');
        tagInst[0].Name = 'Test Tag 1';
        tagInst[0].Tag_Type__c = 'Title Tag';
        insert tagInst[0];
        
        tagInst[1].Name = 'Test Tag 2';
        tagInst[1].Tag_Type__c = 'Title Tag';
        insert tagInst[1];
        
        List<EAW_Customer__c> customerList = EAW_TestDataFactory.createEAWCustomer(1,true);
        
        List<EAW_Release_Date_Type__c> releaseDateTypeList = EAW_TestDataFactory.createReleaseDateType(1, true);
        //LRCC-1560 - Replace Customer lookup field with Customer text field        
        //EAW_Release_Date_Guideline__c releaseDateGuideLine = new EAW_Release_Date_Guideline__c( Product_Type__c = 'Feature', Territory__c = 'United States +', Language__c = 'Dutch', Customer__c = customerList[0].Id, EAW_Release_Date_Type__c = 'SPVOD' ,
        //Active__c = true);
        EAW_Release_Date_Guideline__c releaseDateGuideLine = new EAW_Release_Date_Guideline__c( Product_Type__c = 'Feature', Territory__c = 'United States +', Language__c = 'Dutch', Customers__c = customerList[0].name, EAW_Release_Date_Type__c = 'SPVOD' ,
        Active__c = true);
        insert releaseDateGuideLine;
        
       /* EAW_Rule__c ruleInstance = new EAW_Rule__c(RecordTypeId = releaseDateRuleRecordTypeId, Plan_Guideline__c = planinstanceList[0].Id);
        insert ruleInstance;
        
        EAW_Rule_Detail__c ruledetailList = new EAW_Rule_Detail__c(Rule_No__c = ruleInstance.Id, Condition_Field__c = 'Title Tag', Condition_Tag__c = 'Blockbuster',
        Condition_Criteria__c = tagInst[1].Id);
        insert ruledetailList;*/
         String ruleContent =  '{"sObjectType": "EAW_Rule__c"}';
         String ruleDetailContent = '{"sObjectType": "EAW_Rule_Detail__c", "Condition_Tag__c" : "Test Tag 1", "Condition_Field__c": "Title Tag"}';
         EAW_TestRuleDataFactory.savePgQualifiers(ruleContent,ruleDetailContent,planinstanceList[0].Id);
            
         Test.startTest();
         EAW_Title_Tag_Junction__c titleTagInstance = new EAW_Title_Tag_Junction__c(Title_Attribute__c = titleAttribute[0].Id, Tag__c = tagInst[0].Id);
         insert titleTagInstance;
         Test.stopTest();
         
        
         //titleTagInstance.Tag__c = tagInst[1].Id;
         delete titleTagInstance;
         
         //titleTagInstance.Title__c = edmTitleList[0].Id;
     }
}