public class EAW_PlanNameChangeBatchController implements Database.Stateful,Database.Batchable<sObject>{

    public Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'SELECT Id,Name__c,Plan_Guideline__c,Plan_Guideline__r.Name,EAW_Title__c,EAW_Title__r.Name FROM EAW_Plan__c';
        return Database.getQueryLocator(query);
    }
    public void execute(Database.BatchableContext BC, List<sObject> scope){
        List<EAW_Plan__c > planList = scope;
        if(planList.size() > 0){
            for(EAW_Plan__c plan : planList){
                if(plan.Plan_Guideline__c != null && plan.Plan_Guideline__r.Name != null && plan.EAW_Title__c != null && plan.EAW_Title__r.Name != null){
                    plan.Name__c = plan.EAW_Title__r.Name +'/'+ plan.Plan_Guideline__r.Name;
                }
            }
            Database.update(planList,false);
        }
        
    }
    public void finish(Database.BatchableContext BC){
    }
}