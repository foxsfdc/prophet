public with sharing class EAW_PromotionDateController_Queueable implements Queueable{
    public Map<String, Set<String>> releaseKeys;
    
    public EAW_PromotionDateController_Queueable( Map<String, Set<String>> releaseKeys){
    	this.releaseKeys = releaseKeys;
    }
    
    /*
    1)	Provide Apex Class / Function for Queueable (or Future) method that will be invoked when users update dating records.
	Input: List<Map<String, List<String>>> (List of Maps, Map Key is WPR, Map Values is List of Territory Codes)
	*/
    public void execute(QueueableContext context) {
       EAW_PromotionDateController.handlePromotionDates(releaseKeys);
    }
}