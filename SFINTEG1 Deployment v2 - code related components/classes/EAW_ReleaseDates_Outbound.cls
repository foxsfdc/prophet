//LRCC-1709 & LRCC-1706
public with sharing class EAW_ReleaseDates_Outbound {
    
    List<EAW_Error_Log__c> repoErrorLogs  = new List<EAW_Error_Log__c>();
    set<id> repoSuccessIds= new set<id>();
    Set<id> repoIgnoredIds = new Set<id>();
    //No need to send data separately to Galileo: Galileo currently also gets Release Dates from Prophet using a Kafka pipeline that sources Release Dates from a Oracle View
    //This information will be extracted from Repo based Release Date view for Prophet Sales Force
    /*List<EAW_Error_Log__c> galileoErrorLogs  = new List<EAW_Error_Log__c>();
    set<id> galileoSuccessIds= new set<id>();
    Set<id> galileoIgnoredIds = new Set<id>();*/
    
    String repoUrl = '';
	/*String galileoUrl = '';*/
	
	public EAW_ReleaseDates_Outbound(){
		Rest_Endpoint_Settings__c repoInstance = Rest_Endpoint_Settings__c.getInstance('Outbound-RD-to-Repo');
		if(repoInstance!=null){
			repoUrl=repoInstance.End_Point__c;
		}
		/*
		Rest_Endpoint_Settings__c galileoInstance = Rest_Endpoint_Settings__c.getInstance('Outbound-ReleaseDate-to-Galileo');
		if(galileoInstance!=null){
			galileoUrl=galileoInstance.End_Point__c;
		}
		*/
	}
    
    public class ReleaseDateJson{
        public String releaseDateId;
        public String releaseDateGuidelineId;
        public String releaseDateGuidelineName;
        public String productType;
        public String financialProdId;
        public String foxId;
        public String productVersionId;// added on 06202017
        public String title;// added on 06182017
        public String releaseDateType_desc;
        public String releaseDateTypeId;
        public String territoryDesc;
        public String territoryId;
        public String languageDesc;
        public String languageId;
        public String releaseDate;
        public String releaseDateStatus;
        public String feedDate;
        public String feedStatus;
        public String manualDate;
        public String manualFlag;
        public String releaseDateTags;
        public String updateDate;
        public String operation;
        public String ruleDateText;//TO do get confirmation from GJ
        
        public ReleaseDateJson(String releaseDateId,String releaseDateGuidelineId,String releaseDateGuidelineName,String productType,
                                    String financialProdId,String foxId,String productVersionId,String title,String releaseDateType_desc,String releaseDateTypeId,
                                    String territoryDesc,String territoryId,String languageDesc,String languageId,String releaseDate,
                                    String releaseDateStatus,String feedDate,String feedStatus,String manualDate,String manualFlag,
                                    String releaseDateTags,String updateDate,String operation,String ruleDateText){
        
            this.releaseDateId=releaseDateId;
            this.releaseDateGuidelineId=releaseDateGuidelineId;
            this.releaseDateGuidelineName=releaseDateGuidelineName;
            this.productType=productType;
            this.financialProdId=financialProdId;
            this.foxId=foxId;
            this.productVersionId=productVersionId;
            this.title=title;
            this.releaseDateType_desc=releaseDateType_desc;
            this.territoryDesc=territoryDesc;
            this.territoryId=territoryId;
            this.languageDesc=languageDesc;
            this.languageId=languageId;
            this.releaseDate=releaseDate;
            this.releaseDateStatus=releaseDateStatus;
            this.feedDate=feedDate;
            this.feedStatus=feedStatus;
            this.manualDate=manualDate;
            this.manualFlag=manualFlag;
            this.releaseDateTags=releaseDateTags;
            this.updateDate=updateDate;
            this.operation=operation;
            this.ruleDateText=ruleDateText;
        }
    }
    
    private void getReleaseDatesJson(EAW_Outbound_Utility.OutboundSystem os, List<Eaw_release_date__c> eawRDList){
        try{
        	
            List<id> tempRdIdList = new List<id>();
            
            /*List<List<ReleaseDateJson>> rdJsonListOfList = new List<List<ReleaseDateJson>>();
            List<List<id>> rdIdListOfList = new List<List<id>>();*/
            
            Map<String,EAW_Territories__c> territoryMap = EAW_Territories__c.getAll();
            Map<String,EAW_Languages__c> languageMap = EAW_Languages__c.getAll();
            
            Map<Id,List<EAW_Release_Date_Tag_Junction__c>> rdtgMap = new Map<Id,List<EAW_Release_Date_Tag_Junction__c>>();
            //if we don’t have them, then just send null values, GJ can update them while inserting/updating in repo.
            /*Map<String,EAW_Release_Date_Type__c> rdtMap = new Map<String,EAW_Release_Date_Type__c>();*/
    
            Set<String> rdts = new Set<String>();
            
            for(Eaw_release_date__c rd:eawRDList){
                tempRdIdList.add(rd.id);
                rdts.add(rd.EAW_Release_Date_Type__c);
            }
            List<EAW_Release_Date_Tag_Junction__c> rdtjList = [SELECT Id,Name,Release_Date__c,Tag__c,Tag__r.Name FROM EAW_Release_Date_Tag_Junction__c where Release_Date__c in :tempRdIdList];
            /*List<EAW_Release_Date_Type__c> erdts = [select Name from EAW_Release_Date_Type__c where Name in :rdts];*/
            
            
            for(EAW_Release_Date_Tag_Junction__c rdtg: rdtjList){
                List<EAW_Release_Date_Tag_Junction__c> tempList = new List<EAW_Release_Date_Tag_Junction__c>();
                if(rdtgMap.get(rdtg.Release_Date__c)!=null){
                    tempList = rdtgMap.get(rdtg.Release_Date__c);
                }
                tempList.add(rdtg);
                rdtgMap.put(rdtg.Release_Date__c,tempList);
            }
            
            /*for(EAW_Release_Date_Type__c rdt:erdts){
                rdtMap.put(rdt.name,rdt);
            }*/
            List<ReleaseDateJson> rdJsonList = new List<ReleaseDateJson>();
            List<id> rdIdList = new List<id>();
            for(Eaw_release_date__c rd:eawRDList){
                String rdgTag = '';
                if(rdtgMap.get(rd.id)!=null && !rdtgMap.get(rd.id).isEmpty()){
                    for(EAW_Release_Date_Tag_Junction__c rdtg: rdtgMap.get(rd.id)){
                        rdgTag+=rdtg.Tag__r.Name+';';
                    }
                    rdgTag.removeEnd(';');
                }else{
                    rdgTag = null;
                }
                /*
                String operation = '';
                if(os==EAW_Outbound_Utility.OutboundSystem.Repo){
                    operation = rd.Repo_DML_Type__c;
                }else if (os==EAW_Outbound_Utility.OutboundSystem.Galileo){
                    operation = rd.Galileo_DML_Type__c;
                }
                */
                    ReleaseDateJson rdJson = new ReleaseDateJson('' + rd.ID,'' + rd.Release_Date_Guideline__c,rd.Release_Date_Guideline__r.name,rd.Product_Type__c,
                                                                    rd.Title__r.FIN_PROD_ID__c,rd.Title__r.FOX_ID__c,rd.Title__r.Product_Version_Id__c,rd.Title__r.name,rd.EAW_Release_Date_Type__c,/*''+rdtMap.get(rd.EAW_Release_Date_Type__c).id*/null,
                                                                    rd.Territory__c,EAW_Outbound_Utility.getTerritoryIds(rd.Territory__c),rd.Language__c,EAW_Outbound_Utility.getLanguageIds(rd.Language__c),EAW_Outbound_Utility.getFormattedDate(rd.Release_Date__c),
                                                                    rd.Status__c,EAW_Outbound_Utility.getFormattedDate(rd.Feed_Date__c),rd.Feed_Date_Status__c,EAW_Outbound_Utility.getFormattedDate(rd.Manual_Date__c),rd.Temp_Perm__c,
                                                                    rdgTag, EAW_Outbound_Utility.getFormattedDateTime(rd.LastModifiedDate),rd.Repo_DML_Type__c,rd.Release_Date_Guideline__r.All_Rules__c);
                    rdJsonList.add(rdJson);
                    rdIdList.add(rd.id);
                /*if(rdJsonList.size()==EAW_Outbound_Utility.MAX_API_RECORDS_LIMIT){
                    rdJsonListOfList.add(rdJsonList);
                    rdIdListOfList.add(rdIdList);
                    rdJsonList = new List<ReleaseDateJson>();
                    rdIdList = new List<id>();
                }*/
            }
            /*
            if(rdJsonList!=null && !rdJsonList.isEmpty()){
                rdJsonListOfList.add(rdJsonList);
                rdIdListOfList.add(rdIdList);
            }*/
            if(rdJsonList!=null && !rdJsonList.isempty()){
                /*for(integer i=0;i<rdJsonListOfList.size();i++){
                    List<ReleaseDateJson> tempRdJsonList = rdJsonListOfList[i];
                    List<id> tempIdList = rdIdListOfList[i];*/
                    EAW_Outbound_Utility eouRepo = new EAW_Outbound_Utility(EAW_Outbound_Utility.OutboundDataType.ReleaseDate,os,rdIdList);
                    if(/*os==EAW_Outbound_Utility.OutboundSystem.Repo && */String.isNotBlank(repoUrl)){
                        eouRepo.sendDataThroughWebAPI( system.json.serialize(rdJsonList),repoUrl,repoErrorLogs,repoSuccessIds);
                    /*}else if (os==EAW_Outbound_Utility.OutboundSystem.Galileo  && String.isNotBlank(galileoUrl)){//To Do:Currently galileo api is not yet ready
                        eouRepo.sendDataThroughWebAPI( system.json.serialize(tempRdJsonList),galileoUrl,galileoErrorLogs,galileoSuccessIds);*/
                    }
                /*}*/
            }
            
        }catch(exception e){
            system.debug('EAW_ReleaseDates_Outbound Exception: '+e.getmessage());
        }
    }
    
    public void aggregateAllRds(List<Eaw_release_date__c> records){
        List<Eaw_release_date__c> repoRecords = new List<Eaw_release_date__c>();
        /*List<Eaw_release_date__c> galileoRecords = new List<Eaw_release_date__c>();*/
        
        List<Eaw_release_date__c> updateList = new List<Eaw_release_date__c>();
        List<Eaw_release_date__c> deleteList = new List<Eaw_release_date__c>();
        List<EAW_Error_Log__c> insertErrorList = new List<EAW_Error_Log__c>();
        
        if(records!=null && !records.isempty()){
            for(Eaw_release_date__c rd:records){
                boolean isSoftDeletedRecord = rd.Soft_Deleted__c=='TRUE'; //rd.Active__c;
                
                if(String.isBlank(rd.Repo_DML_Type__c)){
            		if(isSoftDeletedRecord){
                		rd.Repo_DML_Type__c='Delete';
                	} else if(rd.Sent_To_Repo__c){
    					rd.Repo_DML_Type__c='Update';
    				}else{
    					rd.Repo_DML_Type__c='Insert';
    				}
            	}
                
                if(rd.Sent_To_Repo__c || !isSoftDeletedRecord){
                    repoRecords.add(rd);
                }else if(!rd.Sent_To_Repo__c && isSoftDeletedRecord){
                    repoIgnoredIds.add(rd.id);
                }
                
                system.debug('repoRecords: '+system.JSON.serialize(repoRecords));
                system.debug('repoIgnoredIds: '+system.JSON.serialize(repoIgnoredIds));
                
                /*if(rd.Sent_To_Galileo__c || !isSoftDeletedRecord){
                    galileoRecords.add(rd);
                }else if(!rd.Sent_To_Galileo__c && isSoftDeletedRecord){
                    galileoIgnoredIds.add(rd.id);
                }*/
            }
            if(!repoRecords.isEmpty() && String.isNotBlank(repoUrl)){
                getReleaseDatesJson(EAW_Outbound_Utility.OutboundSystem.Repo,repoRecords);
            }
            /*
            if(!galileoRecords.isEmpty() && String.isNotBlank(galileoUrl)){
                getReleaseDatesJson(EAW_Outbound_Utility.OutboundSystem.Galileo,galileoRecords);
            }*/
            system.debug('repoSuccessIds: '+system.JSON.serialize(repoSuccessIds));
            for(Eaw_release_date__c rd:records){
                    boolean canBeDeleted = false;
                    boolean isSoftDeletedRecord = rd.Soft_Deleted__c=='TRUE';    
                    /*if((repoSuccessIds.contains(rd.id) && galileoSuccessIds.contains(rd.id)) //Both are success
                    || (repoSuccessIds.contains(rd.id) && galileoIgnoredIds.contains(rd.id)) // repo is success and galileo is failed even one time 
                        || (galileoSuccessIds.contains(rd.id) && repoIgnoredIds.contains(rd.id)) // galileo is success and repo is failed even one time 
                        || (repoIgnoredIds.contains(rd.id) && galileoIgnoredIds.contains(rd.id))//If record is inactive and didn't send to both api then we can delete without sending them
                        ){
                        rd.Send_To_Third_Party__c=false;
                        canBeDeleted = true; // Can be deleted
                    }*/
                    if(repoSuccessIds.contains(rd.id) || repoIgnoredIds.contains(rd.id)){
                    	rd.Send_To_Third_Party__c=false;
                        canBeDeleted = true; // Can be deleted
                    }
                    if(repoSuccessIds.contains(rd.id)){//Repo call is success then repo DML type is blank and sent to repo should be true
                        rd.Sent_To_Repo__c=true;
                        rd.Repo_DML_Type__c='';
                    }
                    /*
                    if(galileoSuccessIds.contains(rd.id)){//Galileo call is success then Galileo DML type is blank and sent to Galileo should be true
                        rd.Sent_To_Galileo__c=true;
                        rd.Galileo_DML_Type__c='';
                    }*/
                    if(isSoftDeletedRecord && canBeDeleted){
                        deleteList.add(rd);
                    }else{
                        updateList.add(rd);
                    }
            }
            
            if(repoErrorLogs!=null && !repoErrorLogs.isempty()){
                insertErrorList.addAll(repoErrorLogs);
            }
            system.debug('updateList: '+system.JSON.serialize(updateList));
            system.debug('deleteList: '+system.JSON.serialize(deleteList));
            system.debug('insertErrorList: '+system.JSON.serialize(insertErrorList));
            /*insertErrorList.addAll(galileoErrorLogs);*/
        }
        checkRecursiveData.bypassDateCalculation=true;//To bypass recursive calculation of downstream release dates
        if(updateList!=null && !updateList.isEmpty()){
            update updateList;
        }
        if(deleteList!=null && !deleteList.isEmpty()){
            delete deleteList;
        }
        if(insertErrorList!=null && !insertErrorList.isEmpty()){
            upsert insertErrorList;
        }
        //To do Skip downstream calculation of Release dates and Windows
        //if response 200 then Send_To_Third_Party - False, Dml_Type as blank
        //Sent_To_Repo-true
        //Sent_To_Galileo-true
    }
   
    
    @future(callout=true)
    public static void sendDataAsync(Set<id> releaseDateIds){
        EAW_ReleaseDates_Outbound ero = new EAW_ReleaseDates_Outbound();
        ero.sendData(releaseDateIds);
    }
    
    public void sendData(Set<id> releaseDateIds){
    	//If following query is modified make sure to change the query in EAW_ReleaseDate_API_BatchProcess batch class
        List<Eaw_release_date__c> eawRDList = [SELECT
													ID, 
													Release_Date_Guideline__c, 
													Release_Date_Guideline__r.name, 
													Product_Type__c, 
													Title__r.FIN_PROD_ID__c, 
													Title__r.FOX_ID__c,
													Title__r.Product_Version_Id__c, 
													Title__r.Name,    
													EAW_Release_Date_Type__c, 
													Territory__c, 
													Language__c, 
													Release_Date__c, 
													Status__c, 
													Feed_Date__c, 
													Feed_Date_Status__c, 
													Manual_Date__c, 
													Temp_Perm__c, 
													LastModifiedDate, 
													Release_Date_Guideline__r.All_Rules__c, 
													Galileo_DML_Type__c, 
													Repo_DML_Type__c, 
													Sent_To_Galileo__c, 
													Sent_To_Repo__c, 
													Send_To_Third_Party__c, 
													Soft_Deleted__c
												FROM
													Eaw_release_date__c
												WHERE
													 id in :releaseDateIds];
                                                
        aggregateAllRds(eawRDList);
    }
    
    public static void sendDataSync(Set<id> releaseDateIds){
        EAW_ReleaseDates_Outbound ero = new EAW_ReleaseDates_Outbound();
        ero.sendData(releaseDateIds);
    }    
    
}