public with sharing class EAWPlanAction {
	
    ApexPages.StandardSetController setController;
    MetadataService.MetadataPort metaService;
    PageReference planPageRef;
    
    public EAW_Plan_Guideline__c searchPlan{ get; set; }
    
    
    public EAWPlanAction(ApexPages.StandardSetController controller) {
		setController = controller;
        
        Schema.DescribeSObjectResult result = EAW_Plan_Guideline__c.SObjectType.getDescribe();
		planPageRef = new PageReference('/' + result.getKeyPrefix());
		planPageRef.setRedirect(true);
        
        searchPlan = new EAW_Plan_Guideline__c();
        metaService = MetadataServiceExamples.createService();
    }
    
    
    public PageReference deleteSelectedRecords() {
        
        // Take all selected records, and delete the set
        delete ((EAW_Plan_Guideline__c[])setController.getSelected());
        
        // Redirect back to the main record page
		return planPageRef;
    }
    
    public PageReference cloneSelectedRecords() {
        
        List<EAW_Plan_Guideline__c> newPlans = new List<EAW_Plan_Guideline__c>();
        
        // Take all selected records, and make a new record
        for(EAW_Plan_Guideline__c oldPlan : (EAW_Plan_Guideline__c[])setController.getSelected()) {
            
            // We don't have a copy constructor, so lets copy all the fields
			EAW_Plan_Guideline__c plan = new EAW_Plan_Guideline__c();
            plan.Name = oldPlan.Name + ' clone';
            //plan.Business_Division__c = oldPlan.Business_Division__c;
            plan.Media__c = oldPlan.Media__c;
            plan.Product_Type__c = oldPlan.Product_Type__c;
            plan.Territory__c = oldPlan.Territory__c;
            //plan.Lifecycle__c = oldPlan.Lifecycle__c;
            
            newPlans.add(plan);
        }
        
        if(newPlans.size() > 0) {
            insert newPlans;
        }
        
        // Redirect back to the main record page
		return planPageRef;
    }
    
    public PageReference deactivatePlans() {
        
        // Take all selected records, and update the record
        for(EAW_Plan_Guideline__c oldPlan : (EAW_Plan_Guideline__c[])setController.getSelected()) {
            //oldPlan.Status__c = 'Draft';
            update oldPlan;
        }
        
        // Redirect back to the main record page
		return planPageRef;
    }
    
    public PageReference searchForPlans() {
		
        // We search for plans by making a new listview called Search Results
        // and overriding it with each subsequent search
        
        // Create the ListView Object
        MetadataService.ListView listView = new MetadataService.ListView();
        
        listView.fullName = 'EAW_Plan_Guideline__c.MyListView';
        listView.label = 'Search Results';
        listView.filterScope = 'Everything';
        listView.columns = new List<String> {'NAME', 'Territory__c', 'Media__c', 'Product_Type__c', 'Lifecycle__c'};
        
        // Add the Filters
        List<MetadataService.ListViewFilter> filterList = new List<MetadataService.ListViewFilter>();
        listView.filters = filterList;
        
        if(searchPlan.Name != null && !searchPlan.Name.equals('')) {
            MetadataService.ListViewFilter nameFilter = new MetadataService.ListViewFilter();
            nameFilter.field = 'NAME';
            nameFilter.operation = 'contains';
            nameFilter.value = searchPlan.Name;
            filterList.add(nameFilter);
        }
        
        if(searchPlan.Territory__c != null) {
            MetadataService.ListViewFilter terrFilter = new MetadataService.ListViewFilter();
            terrFilter.field = 'Territory__c';
            terrFilter.operation = 'equals';
            terrFilter.value = searchPlan.Territory__c;
            filterList.add(terrFilter);
        }
        
        if(searchPlan.Media__c != null) {
            MetadataService.ListViewFilter mediaFilter = new MetadataService.ListViewFilter();
            mediaFilter.field = 'Media__c';
            mediaFilter.operation = 'equals';
            mediaFilter.value = searchPlan.Media__c;
            filterList.add(mediaFilter);
        }
        
        if(searchPlan.Product_Type__c != null) {
            MetadataService.ListViewFilter prodFilter = new MetadataService.ListViewFilter();
            prodFilter.field = 'Product_Type__c';
            prodFilter.operation = 'equals';
            prodFilter.value = searchPlan.Product_Type__c;
            filterList.add(prodFilter);
        }

		
        // Delete the Old ListView
        List<MetadataService.DeleteResult> deleteResults = metaService.deleteMetadata('ListView', new String[] { 'EAW_Plan_Guideline__c.MyListView' });

        // Save the listview now
        List<MetadataService.SaveResult> results = metaService.createMetadata(new MetadataService.Metadata[] { listView });
        
        return planPageRef;
    }
   
}