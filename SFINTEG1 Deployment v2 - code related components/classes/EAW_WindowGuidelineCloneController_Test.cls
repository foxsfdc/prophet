@isTest
public class EAW_WindowGuidelineCloneController_Test{ 
    
    @testSetup static void setupTestData() {
        // Call appropriate methods in EAW_TestDataFactory to create test records
        EAW_TestDataFactory.createWindowGuideLineStrand(1, true);
        EAW_TestDataFactory.createRuleDetail(2, True);
        List<EAW_Window_Guideline__c> windowGuideLine = EAW_TestDataFactory.createWindowGuideLine(1, false);
        windowGuideLine[0].Product_Type__c = 'Pilot';
        windowGuideLine[0].Window_Type__c = 'First-Run';
        windowGuideLine[0].Name = 'Test record on 21/01';
        insert windowGuideLine;
    }
    
    @isTest static void getWindowGuideline_Test() {
    
        List<EAW_Window_Guideline__c> windowGuideLineList = [SELECT Id FROM EAW_Window_Guideline__c LIMIT 1];
        EAW_Window_Guideline__c windowGuideLine = EAW_WindowGuidelineCloneController.getWindowGuideline(windowGuideLineList[0].Id);
        system.assert(windowGuideLine != null);
        
        try {
            EAW_WindowGuidelineCloneController.getWindowGuideline('');
        } catch (exception e) {
            system.assert(e.getMessage() != null);
        }
        
    }  
    
    @isTest static void cloneRuleAndRuleDetails_Test() {
        
        List<EAW_Window_Guideline__c> windowGuideLineList = [SELECT Id FROM EAW_Window_Guideline__c LIMIT 2];
        
        List <EAW_Rule__c> rulesList = [SELECT Id FROM EAW_Rule__c LIMIT 2];
        rulesList[0].Window_Guideline__c = windowGuideLineList[0].Id;
        update rulesList;
        
        List<EAW_Rule_Detail__c> edmRuleDetailList = [SELECT Id FROM EAW_Rule_Detail__c LIMIT 2];
        edmRuleDetailList[1].Parent_Rule_Detail__c = edmRuleDetailList[0].Id;
        update edmRuleDetailList;
        
        EAW_WindowGuidelineCloneController.cloneRuleAndRuleDetails(windowGuideLineList[0].Id, windowGuideLineList[1].Id);
        
        List<EAW_Window_Guideline_Strand__c> clonedStrands = [SELECT Id, EAW_Window_Guideline__c FROM EAW_Window_Guideline_Strand__c 
                                                              WHERE EAW_Window_Guideline__c =: windowGuideLineList[1].Id];
        system.assertEquals(1, clonedStrands.size());
        
        List <EAW_Rule__c> clonedRules = [SELECT Id, Window_Guideline__c FROM EAW_Rule__c WHERE Window_Guideline__c =: windowGuideLineList[1].Id];
        system.assertEquals(1, clonedRules.size());
        
        List <EAW_Rule_Detail__c> ruleDetails = [SELECT Id FROM EAW_Rule_Detail__c];
        system.assertEquals(4, ruleDetails.size());
    }  
}