public with sharing class EAW_TitleEDMBoxOfficesController {
    
    @AuraEnabled
    public static List<Object> getFields(Map<String,String> objFieldSetMap) {

        List<Object>fieldNames = new List<Object>();
        for(String strObjectName: objFieldSetMap.keySet())
        {
            fieldNames.add(EAW_FieldSetController.getFields(strObjectName, objFieldSetMap.get(strObjectName)));
        }
        return fieldNames;
        
    }
    public class titleNameAndBoxOfficeWrapper{
        @AuraEnabled
        public String titleAttribute;
        @AuraEnabled
        public List<Object> boxOffices;
    }
    @AuraEnabled
    public static titleNameAndBoxOfficeWrapper searchForTVDBoxOffices(String titleIdsString) {
        
        titleNameAndBoxOfficeWrapper newtitleNameAndBoxOfficeWrapper = new titleNameAndBoxOfficeWrapper();
        
        List<Object> fieldValues = new List<Object>();
        
        List<EAW_Title__c> eawTitle = [ Select Id, Name  From EAW_Title__c Where Id =: titleIdsString ];
        
        String queryCondition = ' WHERE Title_Attribute__c = \''+ eawTitle[0].Id+'\'';
        
        queryCondition += ' ORDER BY Name ASC';
        
        fieldValues.add(EAW_FieldSetController.getRecords('EAW_Box_Office__c', 'Box_Office_Table', queryCondition, null, null));
        
        system.debug('::::::::::: fieldValues ::::::::::::'+fieldValues);
        newtitleNameAndBoxOfficeWrapper.titleAttribute = eawTitle[0].Name;
        newtitleNameAndBoxOfficeWrapper.boxOffices = fieldValues;
        return newtitleNameAndBoxOfficeWrapper;
    }
    
    @AuraEnabled
    public static List<EAW_Box_Office__c> saveBoxOfficeRecords(String updatedDataList) {
        system.debug(Logginglevel.INFO,':::: modifiedboxOffice ::::'+updatedDataList);
        List<EAW_Box_Office__c> boxOfficeList = (List<EAW_Box_Office__c>)JSON.deserialize(updatedDataList, List<EAW_Box_Office__c>.class);
        system.debug(Logginglevel.INFO,':::: PLANS ::::'+boxOfficeList);
        if( boxOfficeList != NULL && boxOfficeList.size() > 0 ) update boxOfficeList;
        system.debug(Logginglevel.INFO,':::: PLANS ::::'+boxOfficeList);
        return boxOfficeList;
    }
    
    @AuraEnabled
    public static void deleteBoxOfficeRecords(List<String> recIds) {

        List<EAW_Box_Office__c> items = new List<EAW_Box_Office__c>();
        try {
            for (String recId : recIds) {
                EAW_Box_Office__c boxOffice = new EAW_Box_Office__c();
                boxOffice.Id = recId;
                items.add(boxOffice);
            }
            delete items;
        } catch (Exception e) {
            throw new AuraHandledException('Error : ' + e.getMessage());
        }
    }
    
    @AuraEnabled
    public static void massUpdateTVDBoxOfficeRecords(List<String> recIds, Decimal boxOfficeUSD, Decimal boxOfficeLocalCurrency, Decimal manualBoxOfficeUSD, String boxOfficeOverrideType ) {

        try {

            List<EAW_Box_Office__c> items = new List<EAW_Box_Office__c>();
           
            for (String recId : recIds) {
                
                EAW_Box_Office__c boxOffice = new EAW_Box_Office__c();
                boxOffice.Id = recId;

               if ( boxOfficeUSD != null)
                    boxOffice.TVD_US_Box_Office__c = boxOfficeUSD;
               if ( boxOfficeLocalCurrency != null)
                    boxOffice.TVD_Local_Currency_Value__c = boxOfficeLocalCurrency;
               if ( manualBoxOfficeUSD != null)
                    boxOffice.Manual_Box_Office_USD__c = manualBoxOfficeUSD;
                if ( boxOfficeOverrideType != null)
                    boxOffice.Box_Office_USD_Override_Type__c = boxOfficeOverrideType;
                
                System.debug('::::::boxOffice:::::'+boxOffice);
                items.add(boxOffice);
            }
            System.debug('::::::items:::::'+items);
            if (items.size() > 0) {
                update items;
            }
            //insertNotes(note, recIds, title);

        } catch (Exception e) {
            throw new AuraHandledException('Error : ' + e.getMessage());
        }
    }
}