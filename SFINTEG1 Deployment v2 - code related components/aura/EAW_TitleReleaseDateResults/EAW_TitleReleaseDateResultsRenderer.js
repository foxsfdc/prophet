({
    rerender : function (component, helper) { // LRCC - 1282
        this.superRerender();
        
        window.setTimeout($A.getCallback(function(){
            helper.setBodyWidth(component);
        }),100);
        
    },
    // LRCC - 1784
    afterRender : function(component,helper){
        /*let ele = component.find('tableElement').getElement();
    	  ele.addEventListener("scroll", function(evt) {
    		  // not the most exciting thing, but a thing nonetheless
    		 /* let theadElement = component.find('theadELement').getElement();
    		  theadElement.style.left = -(evt.target.scrollLeft) + 'px';
    		  
    		  let checkElement = component.find('checkElement').getElement();
    		  checkElement.style.left = evt.target.scrollLeft + 'px'
    		  
              //LRCC-1260
              let dailogIcons = component.find('dailogIcons').getElement();
    		  dailogIcons.style.left = (evt.target.scrollLeft) + 'px';
              
              
    		  let thElement = component.find('thElement');
    		  thElement.map((ele)=>{
    			  let elementth = ele.getElement();
    			  if(elementth.getAttribute('data-colIndx') == '0' ){
    				  elementth.style.left = evt.target.scrollLeft + 'px';
    			  }
    			  if(elementth.getAttribute('data-colIndx') == '1'){
    				  elementth.style.left = evt.target.scrollLeft + 'px';
    			  }
    		  });
        
        	//LRCC-1517
        	if(component.find('resultFilter')) {
                
                let filterHeader = component.find('resultFilter').find('filterHeader').getElement();
                filterHeader.style.left = -(evt.target.scrollLeft) + 'px';
                
                let filterCheckHeader = component.find('resultFilter').find('filterCheckHeader').getElement();
                filterCheckHeader.style.left = evt.target.scrollLeft + 'px'
                
                //LRCC-1260
                let filterDialogHeader = component.find('resultFilter').find('filterDialogHeader').getElement();
                filterDialogHeader.style.left = (evt.target.scrollLeft) + 'px';
                
                
                let filterthElement = component.find('resultFilter').find('filterthElement');
                filterthElement.map((ele)=>{
                    let elementth = ele.getElement();
                    if(elementth.getAttribute('data-colIndx') == '0' ){
                    	elementth.style.left = evt.target.scrollLeft + 'px';
                	}
                    if(elementth.getAttribute('data-colIndx') == '1'){
                    	elementth.style.left = evt.target.scrollLeft + 'px';
                	}
            	});
    		}
    		  
    		  var tableColumn = component.find('tableColumn');
    		  
    		  if(tableColumn && !Array.isArray(tableColumn)){
    		    tableColumn = [tableColumn];
    		  }
           /* if(tableColumn && tableColumn.length > 0){	
                tableColumn.map((ele)=>{
                    let elementtdcmp = ele;
                    if(elementtdcmp.find('inputTableID') && elementtdcmp.find('inputTableID').length > 0){
                    elementtdcmp.find('inputTableID')[0].find('itdEle').getElement().style.left = evt.target.scrollLeft + 'px';
                    elementtdcmp.find('inputTableID')[1].find('itdEle').getElement().style.left = evt.target.scrollLeft + 'px';
                }
                });
            }	 
            var tdcheck = component.find('tdcheck');
            if(tdcheck && !Array.isArray(tdcheck)){
                tdcheck = [tdcheck];
            }
            if(tdcheck && tdcheck.length > 0 ){
                tdcheck.map((ele)=>{
                    ele.getElement().style.left = evt.target.scrollLeft + 'px';
                });
            }
    		 
            //LRCC-1260
            var tddailogs = component.find('tddailogs');
            if(tddailogs && !Array.isArray(tddailogs)){
               tddailogs = [tddailogs];
            }
            if(tddailogs && tddailogs.length > 0 ){
                tddailogs.map((ele)=>{
                    ele.getElement().style.left = evt.target.scrollLeft + 'px';
                });
            }
		});*/
         helper.windowClick = $A.getCallback(function(event){
             if(component.isValid()){
                 helper.setBodyWidth(component);
             }
         });
         
         window.addEventListener('resize',helper.windowClick);
     }
})