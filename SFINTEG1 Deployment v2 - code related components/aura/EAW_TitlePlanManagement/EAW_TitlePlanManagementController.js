({
    doInit: function (component, event, helper) {
        component.set('v.titleColumns', [
            {label: 'Name', fieldName: 'Name', type: 'text',initialWidth: 200},
            {label: 'Intl Product Type', fieldName: 'Intl_Product_Type__c', type: 'text', initialWidth: 200}
        ]);
        
    },
    getFieldSet: function(component,event,helper){
        helper.fieldSet(component);
    },
    
    /** Methods below control Paste Title popup*/
    showPasteTitleModal : function(component, event, helper) {
        document.getElementById("EAW_TitlePasteSearchComponent").style.display = "block";
    },
    hidePasteTitleModal : function(component, event, helper){
        document.getElementById("EAW_TitlePasteSearchComponent").style.display = "none";
    },
    
    /** Methods below control Advanced Title Search popup*/
    showAdvancedTitleSearchModal : function(component, event, helper) {
        document.getElementById("EAW_TitleAdvancedSearchComponent").style.display = "block";
    },
    hideAdvancedTitleSearchModal : function(component, event, helper){
        document.getElementById("EAW_TitleAdvancedSearchComponent").style.display = "none";
    },
    
    /** Methods Below control SPINNER*/
    showSpinner:function(component){
        component.set('v.showSpinner',true);
    },
    hideSpinner:function(component){
        component.set('v.showSpinner',false);
    }
})