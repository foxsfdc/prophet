({	
    init : function(cmp,event,helper){
        console.log('::test::::',JSON.stringify(cmp.get('v.conditionBasedMap')));
        console.log(':::::::::::::::::::::::'+cmp.get('v.windowGuidelineList'));
        helper.getDependentFields(cmp,event,helper); 
    },
    deleteNodes : function(cmp, event, helper) {
        
        var conditionBasedMap = JSON.parse(JSON.stringify(cmp.get('v.conditionBasedMap')));
        var node = event.getSource().get('v.name');
        var newRuleDetailJSON = {
            'ruleDetail' : JSON.parse(JSON.stringify(cmp.get('v.ruleDetailObject'))),
            'dateCalcs' : [],
            'nodeCalcs' : []                
        }; 
        
        if(node == 'trueBased'){
            console.log(conditionBasedMap.TrueBased);
            if(conditionBasedMap.TrueBased.ruleDetailJSON && conditionBasedMap.TrueBased.ruleDetailJSON.ruleDetail.Id){
                helper.openModal(cmp,event,helper,'Delete this Rule and its Rule details?');
            	cmp.set('v.toDelete',true);
            } else{
                var newRuleDetail = {'ruleOrder' : 1,
                                     'ruleDetailJSON' : JSON.parse(JSON.stringify(newRuleDetailJSON))};
                conditionBasedMap.TrueBased = newRuleDetail;
                cmp.find('trueBased').set('v.toSelect',false);
            }   
        } else if(node == 'falseBased'){
            if(conditionBasedMap.FalseBased.ruleDetailJSON && conditionBasedMap.FalseBased.ruleDetailJSON.ruleDetail.Id){
                helper.openModal(cmp,event,helper,'Delete this Rule and its Rule details?');
                cmp.set('v.toDelete',false);
            } else{
                var newRuleDetail = {'ruleOrder' : 2,
                                     'ruleDetailJSON' : JSON.parse(JSON.stringify(newRuleDetailJSON))};
                conditionBasedMap.FalseBased = newRuleDetail;
                cmp.find('falseBased').set('v.toSelect',false);
            }  
        }
        cmp.set('v.conditionBasedMap',conditionBasedMap);
	},
    saveCondition : function(cmp,event,helper){
        var cmpEvent = cmp.getEvent("ruleDetailSave");
        cmpEvent.setParams({
            'ruleType' : 'condition'
        });
        if(cmp.find('trueBased'))cmp.find('trueBased').formEarliestLatestNodes();
        if(cmp.find('falseBased'))cmp.find('falseBased').formEarliestLatestNodes();
       // console.log(':::::::::::::::::::::::::::::'+JSON.stringify(cmp.get('v.conditionBasedMap')));
        cmpEvent.fire();
    },
    setDependentList : function(cmp,event,helper){
        var selectedValue = event.getSource().get('v.value');
        cmp.set('v.dependentList',cmp.get('v.conditionBasedDependentMap')[selectedValue]);
    },
    deleteRuleDetail : function(cmp,event,helper){
        event.stopPropagation();
        var conditionBasedMap = cmp.get('v.conditionBasedMap');        
        var newRuleDetailJSON = {
            'ruleDetail' : JSON.parse(JSON.stringify(cmp.get('v.ruleDetailObject'))),
            'dateCalcs' : [],
            'nodeCalcs' : []                
        }; 
        
        if(cmp.get('v.toDelete')){
            helper.deleteRuleDetailById(cmp,event,helper,conditionBasedMap.TrueBased.ruleDetailJSON.ruleDetail.Id,false);
            var newRuleDetail = {'ruleOrder' : 1,
                                 'ruleDetailJSON' : JSON.parse(JSON.stringify(newRuleDetailJSON))};
                conditionBasedMap.TrueBased = newRuleDetail;
                cmp.find('trueBased').set('v.toSelect',false);
        } else {
            helper.deleteRuleDetailById(cmp,event,helper, conditionBasedMap.FalseBased.ruleDetailJSON.ruleDetail.Id,false);
            var newRuleDetail = {'ruleOrder' : 2,
                                 'ruleDetailJSON' : JSON.parse(JSON.stringify(newRuleDetailJSON))};
                conditionBasedMap.FalseBased = newRuleDetail;
                cmp.find('falseBased').set('v.toSelect',false);
        }
        cmp.set('v.conditionBasedMap',conditionBasedMap);
    }
})