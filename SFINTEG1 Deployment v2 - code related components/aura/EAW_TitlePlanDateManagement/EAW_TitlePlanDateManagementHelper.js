({
    collectIds: function (selectedResults) {

        var collectedIds = [];

        if (selectedResults && selectedResults.length > 0) {
            for (var result of selectedResults) {
                if (result.id) {
                    collectedIds.push(result.id); //NOTE THIS IS LOWERCASE "i"
                } else {
                    collectedIds.push(result.Id); //NOTE THIS IS UPPERCASE "I'
                }
            }
        }
        return collectedIds;
    },

    collectNames: function (selectedResults) {

        var collectedNames = [];

        if (selectedResults && selectedResults.length > 0) {
            for (var result of selectedResults) {
                collectedNames.push(result.title); // The NAME is actually a column called title (it's weird)
            }
        }

        return collectedNames;
    },

    errorToast: function (message) {
        var toastEvent = $A.get('e.force:showToast');

        toastEvent.setParams({
            'title': 'Error!',
            'type': 'error',
            'message': message
        });

        toastEvent.fire();
    },

    successToast: function (message) {
        var toastEvent = $A.get('e.force:showToast');

        toastEvent.setParams({
            'title': 'Success!',
            'type': 'success',
            'message': message
        });

        toastEvent.fire();
    },

    showSpinner: function (component) {
        component.set('v.showSpinner', true);
    },

    hideSpinner: function (component) {
        component.set('v.showSpinner', false);
    },

    showReleaseDateResults: function (component) {
        component.set('v.showReleaseDateResults', true);
    },

    hideReleaseDateResults: function (component) {
        component.set('v.showReleaseDateResults', false);
    },
    setSearchCriteriaString: function (component,helper, headerList, valueList, titleList, isPlan) {

        let searchCriteriaString = '',
            nullString = '',
            columnDivider = ',',
            lineDivider = '\n';
        if (isPlan) {
            searchCriteriaString += 'Plan /Window Results' + lineDivider + lineDivider;
            nullString += 'Plan /Window Results' + lineDivider + lineDivider;
        } else {
            searchCriteriaString += 'Release Date Results' + lineDivider + lineDivider;
            nullString += 'Release Date Results' + lineDivider + lineDivider;
        }
		//LRCC-1885
        console.log('headerList:::before::',headerList);
        console.log('valueList:::before::',valueList);
        
        var searchHeader = (isPlan)?component.get('v.windowSearchReleaseDateHeader'):component.get('v.releaseDateSearchWindowHeader');
        var searchValues = (isPlan)?component.get('v.windowSearchReleaseDateValues'):component.get('v.releaseDateSearchWindowValues');
        var tempheaderList;
 		var tempvalueList;
        tempheaderList = headerList.concat(searchHeader);
        tempvalueList = valueList.concat(searchValues);
        headerList = tempheaderList;
		valueList = tempvalueList
        console.log('headerList:::after::',headerList);
        console.log('valueList:::after::',valueList);
        
        for (var i = 0; i < headerList.length; i++) {
            var temp = valueList[i];

            if (!$A.util.isEmpty(temp[0])) {
                if (!searchCriteriaString.includes('Search Criteria')) {
                    searchCriteriaString += 'Search Criteria';
                    searchCriteriaString += lineDivider;
                }
                searchCriteriaString += headerList[i];
                searchCriteriaString += columnDivider;
                for (var j = 0; j < temp.length; j++) {
                    if (headerList[i].endsWith('Date') && temp[j]) {
                        console.log(temp[j]);
                        var resultData = helper.getFormattedDate(temp[j]);
                    } else {
                        var resultData = temp[j];
                    }
                    searchCriteriaString += resultData + ';' || '';
                }
                searchCriteriaString += lineDivider;
            }
        }
		
        if (titleList.length) {
            searchCriteriaString += lineDivider;
            searchCriteriaString += 'Title Criteria';
            searchCriteriaString += lineDivider;
            searchCriteriaString += 'Title(s)' + columnDivider;
            for (var i = 0; i < titleList.length; i++) {
                searchCriteriaString += titleList[i].Title_Name.replace(',', '');
                searchCriteriaString += ';';
            }
        }
        searchCriteriaString += lineDivider;
        searchCriteriaString += lineDivider;
        nullString += lineDivider;
        nullString += lineDivider;
        if (nullString == searchCriteriaString) {
            return null;
        } else {
            return searchCriteriaString;
        }
    },
    getFormattedDate: function (dateToFormat) {
        var mydate = new Date(dateToFormat);
        var month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"
        ][mydate.getMonth()];
        return ' ' + mydate.getDate() + '-' + month + '-' + mydate.getFullYear() + ' ';
    },
    fetchReleaseDateIdFromReleaseDateTagJunction : function (component, event, helper,pagesPerRecord,currentPageNumber,pageIdMap,selectedFilterMap) {
         console.log('fetchReleaseDateIdFromReleaseDateTagJunction::::::');
        var releaseDateTags = helper.collectIds(component.get('v.selectedReleaseDateTags'));
          console.log('releaseDateTags::::::',releaseDateTags);
        var action = component.get('c.searchReleaseDateTagJunction');
        action.setParams({'tags': releaseDateTags});
        action.setCallback(this, function (response) {
             console.log('response.getReturnValue::::::',response.getReturnValue());
            var state = response.getState();
            if (state === 'SUCCESS') {
                
                var results = response.getReturnValue();
                console.log('releasedatetagJunction::::::',results);
                helper.searchReleaseDates(component, event, helper,pagesPerRecord,currentPageNumber,pageIdMap,selectedFilterMap,results);
        
            }
        });
        $A.enqueueAction(action);
    }, 
    //LRCC-1518
    fetchReleaseDateGuidelineIdWithWindowSearch : function (component, event, helper,pagesPerRecord,currentPageNumber,pageIdMap,selectedFilterMap)  {
        
        //LRCC-1885
		var releaseDateSearchWindowHeader = component.get('v.releaseDateSearchWindowHeader');
        var releaseDateSearchWindowValues = component.get('v.releaseDateSearchWindowValues');
	
        var windoParam;
        var windowTags = helper.collectIds(component.get('v.selectedWindowTags'));
        //LRCC-1885
		releaseDateSearchWindowHeader.push('Window Tags');
		releaseDateSearchWindowValues.push(helper.collectNames(component.get('v.selectedWindowTags')));

        var windowNames = helper.collectIds(component.get('v.selectedWindows'));//collectNames(component.get('v.selectedWindows'));
        //LRCC-1885
		releaseDateSearchWindowHeader.push('Window Names');
		releaseDateSearchWindowValues.push(helper.collectNames(component.get('v.selectedWindows')));

        var windowType = component.find('windowType') ? component.find('windowType').get('v.selectedList') : '';
        var tempwindowType = [];
		        
		helper.showSpinner(component);
        windowType.forEach(function (element) {
            tempwindowType.push(element.value);
        });
        windowType = tempwindowType;
		//LRCC-1885
		releaseDateSearchWindowHeader.push('Window Type');
		releaseDateSearchWindowValues.push([windowType]);
        
        var windowStatus = component.find('windowStatus') ? component.find('windowStatus').get('v.selectedList') : '';
        var tempWindowStatus = [];
        windowStatus.forEach(function (element) {
            tempWindowStatus.push(element.value);
        });
        windowStatus = tempWindowStatus;
        //LRCC-1885
		releaseDateSearchWindowHeader.push('Window Status');
		releaseDateSearchWindowValues.push([windowStatus]);
        
        var windowDateStart = component.find('windowDateStart') ? component.find('windowDateStart').get('v.value') : '';
        //LRCC-1885
		releaseDateSearchWindowHeader.push('Window Start From Date');
		releaseDateSearchWindowValues.push([windowDateStart]);
        
        var windowDateEnd = component.find('windowDateEnd') ? component.find('windowDateEnd').get('v.value') : '';
        //LRCC-1885
		releaseDateSearchWindowHeader.push('Window Start To Date');
		releaseDateSearchWindowValues.push([windowDateEnd]);
        component.set('v.releaseDateSearchWindowHeader',releaseDateSearchWindowHeader);
        component.set('v.releaseDateSearchWindowValues',releaseDateSearchWindowValues);
        
        if (windowDateEnd && windowDateStart > windowDateEnd) {
            console.log('con enter:::', windowDateStart, windowDateEnd);
            helper.errorToast('Window Start From must be earlier than Window Start To');
        }else {
             
            windoParam = {'windowTags': windowTags,
                          'windowNames': windowNames,
                          'windowType': windowType,
                          'windowStatus': windowStatus,
                          'windowDateStart': windowDateStart,
                          'windowDateEnd': windowDateEnd,
                         }
            
        }
        console.log('windoParam:::',windoParam);
          console.log('con enter:::', windowDateStart, windowDateEnd);
        if(windoParam != null){
            
            
            var action = component.get('c.getWGRelaseDateGuideline');
            action.setParams(windoParam);
            action.setCallback(this, function (response) {
                console.log('response.getReturnValue:::::get:',JSON.parse(response.getReturnValue()));
                var state = response.getState();
                if (state === 'SUCCESS') {
                    
                    var results =JSON.parse(response.getReturnValue());
                    console.log('releasedateGuideline ids::::::',results);
                    if(results && results.rdGuideLineIdSet.length){
                        
                       helper.searchReleaseDates(component, event, helper,pagesPerRecord,currentPageNumber,pageIdMap,selectedFilterMap,results.rdGuideLineIdSet); 
                    }else{
                        
                        helper.searchReleaseDates(component, event, helper,pagesPerRecord,currentPageNumber,pageIdMap,selectedFilterMap,null);
                    }
                    
                    
                }
            });
            $A.enqueueAction(action);
        }else{
            helper.searchReleaseDates(component, event, helper,pagesPerRecord,currentPageNumber,pageIdMap,selectedFilterMap,null);
        }
    },    
    searchReleaseDates: function (component, event, helper,pagesPerRecord,currentPageNumber,pageIdMap,selectedFilterMap,releasedateGuidelineIds) {


        var headerList = [];
        var valueList = [];
        var titleList = [];

        var medias = helper.collectNames(component.get('v.selectedMedias'));
        headerList.push('Media');
        valueList.push([medias]);

        var territories = helper.collectNames(component.get('v.selectedTerritories'));
        headerList.push('Territory');
        valueList.push([territories]);

        var languages = helper.collectNames(component.get('v.selectedLanguages'));
        headerList.push('Language');
        valueList.push([languages]);
        
        //LRCC-1557
        //var releaseDateTypes = component.find('releaseDateType') ? component.find('releaseDateType').get('v.value') : '';
        var releaseDateTypes = helper.collectNames(component.get('v.selectedReleaseDateTypes'));
        headerList.push('Release Date Type');
        valueList.push([releaseDateTypes]);

        var releaseDateTags = helper.collectIds(component.get('v.selectedReleaseDateTags'));
        headerList.push('Release Date Tags');
        valueList.push(helper.collectNames(component.get('v.selectedReleaseDateTags')));

        var titles = helper.collectIds(component.get('v.selectedTitles'));
        titleList = component.get('v.selectedTitles');

        var releaseDateStart = component.find('releaseDateStart') ? component.find('releaseDateStart').get('v.value') : '';
        headerList.push('Start Release Date');
        valueList.push([releaseDateStart]);

        var releaseDateEnd = component.find('releaseDateEnd') ? component.find('releaseDateEnd').get('v.value') : '';
        headerList.push('End Release Date');
        valueList.push([releaseDateEnd]);
        
        //LRCC:1022
        //LRCC-1731
        var releaseDateStatus = component.find('releaseDateStatus') ? component.find('releaseDateStatus').get('v.selectedList ') : '';
        var tempReleaseDateStatus = [];
        releaseDateStatus.forEach(function (element) {
            tempReleaseDateStatus.push(element.value);
        });
        releaseDateStatus = tempReleaseDateStatus;
        headerList.push('Release Date Status');
        valueList.push([releaseDateStatus]);
        var releasedateGuidelineId = releasedateGuidelineIds ? releasedateGuidelineIds : [];
        console.log('releasedateGuidelineId:::',releasedateGuidelineId);
        if (helper.setSearchCriteriaString(component,helper, headerList, valueList, titleList) != null || (releasedateGuidelineId.length)) {
			
            component.set('v.searchCriteriaString', helper.setSearchCriteriaString(component,helper, headerList, valueList, titleList));
            if (releaseDateEnd && releaseDateStart > releaseDateEnd) {
                helper.errorToast('Release Date From must be earlier than Release Date To');
            } else {
                var releaseDateSearchParam ;
                var action = component.get('c.searchReleaseDateByCriteria');
                action.setParams({
                    'medias': medias,
                    'territories': territories,
                    'languages': languages,
                    'types': releaseDateTypes,
                    'tags': releaseDateTags,
                    'titles': titles,
                    'stringDateStart': releaseDateStart,
                    'stringDateEnd': releaseDateEnd,
                    'status': releaseDateStatus,
                    'pagesPerRecord' : pagesPerRecord,
                    'currentPageNumber':currentPageNumber,
                    'pageIdMap':pageIdMap,
                    'selectedFilterMap':selectedFilterMap,
                    'totalReleaseDateCount':component.get('v.totalRecordCount'),
                    'exportFlag':null,
                    'lastRecordCreatedDate' : null,
                    'lastBatchIds' : null,
                    'recordLimit' : null ,
                    'releasedateGuidelineIds':releasedateGuidelineId
                    
                });
                
                releaseDateSearchParam = JSON.parse(JSON.stringify(action.getParams()));
                component.set('v.releaseDateSearchParam',releaseDateSearchParam);
                console.log('releaseDateSearchParam::::::',JSON.stringify(component.get('v.releaseDateSearchParam')));
                helper.showSpinner(component);

                action.setCallback(this, function (response) {
                    var state = response.getState();
                    
                    if (state === 'SUCCESS') {
                        console.log('response.getReturnValue():::',JSON.parse(JSON.stringify(response.getReturnValue())));
                        
                        component.set('v.resultData',[]);
                        var releaseDateData = response.getReturnValue().releaseDateData; 
                        var results = releaseDateData;
                        console.log('results:::',JSON.parse(JSON.stringify(results)));
                        component.set('v.SearchContainerState', false);
                        
                        if (document.getElementById("collapseandexpandBtn") && document.getElementById("collapseandexpandBtn")) {
                            var ceb = document.getElementById("collapseandexpandBtn");
                            ceb.style.display = "none";
                            
                            var cef = document.getElementById("collapseandexpandfilter");
                            cef.style.display = "none";
                        }
                        
                        if (results) {
                            //LRCC-1459
                            console.log('results::::releaseDate:::test:::',results);
                            console.log('results::::releaseDate:::test:::',JSON.stringify(results.releaseDates));
                            if(results.totalCount && results.totalCount[0].expr0){
                                console.log('results::::results.totalCount[0].expr0:::test:::',results.totalCount[0].expr0);    
                                component.set('v.totalRecordCount', results.totalCount[0].expr0); 
                                //LRCC-1517
                                if(response.getReturnValue().filterTotalCount) {
                                    component.set('v.totalRecordCount', response.getReturnValue().filterTotalCount);   
                                }
                            }
                            
                            if (Array.isArray(results.releaseDates)) {
                                for (var releaseDate of results.releaseDates) {
                                    //Tags string to object
                                    //upd
                                    if (results.releaseDateTagJunctions && results.releaseDateTagJunctions.length > 0) {
                                        releaseDate.releaseDateTags = results.releaseDateTagJunctions.filter(tag => tag.Tag__c && tag.Release_Date__c == releaseDate.Id)
                                        .map(x => x.Tag__r);
                                    } else {
                                        releaseDate.releaseDateTags = [];
                                    }
                                    
                                    if (results.titleTagJunctions && results.titleTagJunctions.length > 0) {
                                        releaseDate.titleTags = results.titleTagJunctions.filter(tag => tag.Tag__c && tag.Title_Attribute__c == releaseDate.Title__c)
                                        .map(x => x.Tag__r.Name)
                                        .join(', ');
                                    }
                                    
                                    if (releaseDate.Title__r) {
                                        //LRCC-1539
                                        releaseDate.Name = releaseDate.Title__r.Name__c; // LRCC - 1870
                                        //releaseDate.TitleName = releaseDate.Title__r.Name;
                                        releaseDate.titleLink = '/one/one.app?sObject/' + releaseDate.Title__r.Id + '/view#/sObject/' + releaseDate.Title__r.Id + '/view';
                                    }
                                    
                                    if (releaseDate.Title__r && releaseDate.Title__c) {
                                        releaseDate.edmTitleFinProdId = releaseDate.Title__r.FIN_PROD_ID__c;
                                        if (releaseDate.Title__r.PROD_TYP_CD_INTL__r) {
                                            releaseDate.edmTitleIntlProdType = releaseDate.Title__r.PROD_TYP_CD_INTL__r.Name;
                                        }
                                    }
                                    
                                    if (releaseDate.Customer__r) {
                                        releaseDate.customerName = releaseDate.Customer__r.Name;
                                    }
                                    
                                    if (releaseDate.EAW_Release_Date_Type__c) {
                                        releaseDate.releaseDateTypeName = releaseDate.EAW_Release_Date_Type__c;
                                    }
                                    //LRCC-1690
                                    if(releaseDate.releaseDateTags){
                                        releaseDate.releaseDateTagsSortColumn = releaseDate.releaseDateTags.map(x => x.Name).join(', ');
                                    }
                                    var indicationInstatnce = {'redcolor':[],
                                                               'tooltip':[],
                                                               'tooltipMsg':{}}
                                    
                                    if((releaseDate.Feed_Date__c != releaseDate.Release_Date__c && releaseDate.Status__c == 'Firm')){
                                        
                                        indicationInstatnce.redcolor.push('Feed_Date__c');
                                        indicationInstatnce.tooltip.push('Feed_Date__c');
                                        
                                        if(!indicationInstatnce.tooltipMsg['Feed_Date__c']){
                                            
                                            indicationInstatnce.tooltipMsg['Feed_Date__c'] = [];   
                                        }
                                        indicationInstatnce.tooltipMsg['Feed_Date__c'].push('The Feed Date has been changed since the Release Date was firmed');
                                        
                                    }
                                    if(releaseDate.Feed_Date__c  && releaseDate.Status__c == 'Not Released'){
                                        
                                        indicationInstatnce.redcolor.push('Feed_Date__c');
                                        indicationInstatnce.tooltip.push('Feed_Date__c');
                                        
                                        if(!indicationInstatnce.tooltipMsg['Feed_Date__c']){
                                            
                                            indicationInstatnce.tooltipMsg['Feed_Date__c'] = [];   
                                        }
                                        indicationInstatnce.tooltipMsg['Feed_Date__c'].push('A Feed Date exists while the Release Date Status is “Not Released”');
                                        
                                    }
                                    if(releaseDate.Feed_Date__c && releaseDate.Temp_Perm__c == 'Permanent' && releaseDate.Manual_Date__c && releaseDate.Status__c == 'Firm'){
                                        
                                        indicationInstatnce.redcolor.push('Feed_Date__c','Projected_Date__c');
                                        indicationInstatnce.tooltip.push('Feed_Date__c');
                                        
                                        if(!indicationInstatnce.tooltipMsg['Feed_Date__c']){
                                            
                                            indicationInstatnce.tooltipMsg['Feed_Date__c'] = [];   
                                        }
                                        if(indicationInstatnce.tooltipMsg['Feed_Date__c'].indexOf('The Feed Date has been changed since the Release Date was firmed') == -1){
                                            indicationInstatnce.tooltipMsg['Feed_Date__c'].push('The Feed Date has been changed since the Release Date was firmed');
                                     
                                        }
                                       }
                                    if(releaseDate.Projected_Date__c != releaseDate.Release_Date__c && releaseDate.Status__c == 'Firm'){
                                        
                                        indicationInstatnce.redcolor.push('Projected_Date__c');
                                        indicationInstatnce.tooltip.push('Projected_Date__c');
                                        
                                        if(!indicationInstatnce.tooltipMsg['Projected_Date__c']){
                                            
                                            indicationInstatnce.tooltipMsg['Projected_Date__c'] = [];   
                                        }
                                        indicationInstatnce.tooltipMsg['Projected_Date__c'].push('The Projected Date has been changed since the Release Date was firmed');
                                    }
                                    if(releaseDate.Release_Date__c && (releaseDate.EAW_Release_Date_Type__c == 'Initial HE Release' 
                                                                       || releaseDate.EAW_Release_Date_Type__c == 'Initial Physical Release' 
                                                                       || releaseDate.EAW_Release_Date_Type__c == 'Initial Digital Release')){
                                        
                                        indicationInstatnce.redcolor.push('Release_Date__c');
                                        indicationInstatnce.tooltip.push('Release_Date__c');
                                        
                                        if(!indicationInstatnce.tooltipMsg['Release_Date__c']){
                                            
                                            indicationInstatnce.tooltipMsg['Release_Date__c'] = [];   
                                        }
                                        indicationInstatnce.tooltipMsg['Release_Date__c'].push('An unfirmed date exists that is earlier than the firmed date');
                                    }
                                    releaseDate.indication = indicationInstatnce;
                                }
                                //tags string to objects
                                //upd
                                component.set('v.resultData', JSON.parse(JSON.stringify(results.releaseDates)));
                                console.log('result:::',JSON.stringify(component.get('v.resultData')));
                                component.set('v.myDataOriginal', JSON.parse(JSON.stringify(results.releaseDates)));
                                
                                
                                if (component.find('titlePlanDateResults')) {
                                    var childrefreshFlag =  component.get("v.childrefresh");
                                    component.find('titlePlanDateResults').setLookupFieldData(childrefreshFlag);
                                }
                                
                                helper.showReleaseDateResults(component);
                            } else {
                                component.set('v.resultData', []);
                                if (component.find('titlePlanDateResults')) {
                                    var childrefreshFlag =  component.get("v.childrefresh");
                                    component.find('titlePlanDateResults').setLookupFieldData(childrefreshFlag);
                                }
                                
                                helper.showReleaseDateResults(component);
                            }
                        } else {
                            component.set('v.resultData', []);
                            if (component.find('titlePlanDateResults')) {
                                var childrefreshFlag =  component.get("v.childrefresh");
                                component.find('titlePlanDateResults').setLookupFieldData(childrefreshFlag);
                            }
                            helper.showReleaseDateResults(component);
                        }
                        helper.hideSpinner(component);
                    } else if (state === 'ERROR') {
                        component.set('v.SearchContainerState', true);
                        if (document.getElementById("collapseandexpandBtn") && document.getElementById("collapseandexpandBtn")) {
                            var ceb = document.getElementById("collapseandexpandBtn");
                            ceb.style.display = "block";
                            
                            var cef = document.getElementById("collapseandexpandfilter");
                            cef.style.display = "block";
                        }
                        component.set('v.resultData', []);
                        helper.showReleaseDateResults(component);
                        helper.hideSpinner(component);
                    }
                    
                });
                $A.enqueueAction(action);
            }
        } else {
            //LRCC-1518
            helper.hideSpinner(component);
            if(releasedateGuidelineId.length){
              
               helper.errorToast('At least one criteria must be provided before a search is performed');
             
            }else if(releasedateGuidelineId.length == 0){
                
                component.set('v.resultData',[]);
                component.set('v.totalRecordCount',0);
                helper.showReleaseDateResults(component);

            }
            //LRCC-1012 - commented for this ticket
            /*if (component.get('v.resultData').length) {
                component.set('v.resultData', []);
            }*/
        }
    },
    checkChangesInTabelData: function (component, event, helper, searchFlag,pagesPerRecod,currentPageNumber,pageIdMap,selectedFilterMap) {

       
       //     <!-- LRCC:1459 -->
        component.set('v.SearchFlag', searchFlag);
        console.log('::::::::>:checkChangesInTabelData:::::::::',pagesPerRecod,currentPageNumber,pageIdMap,selectedFilterMap);
        if ('releasedate' == searchFlag) {
            //helper.fetchReleaseDateIdFromReleaseDateTagJunction(component, event, helper,pagesPerRecod,currentPageNumber,pageIdMap,selectedFilterMap);
            //LRCC-1518
            // helper.searchReleaseDates(component, event, helper,pagesPerRecod,currentPageNumber,pageIdMap,selectedFilterMap);
            // 
        	helper.fetchReleaseDateGuidelineIdWithWindowSearch(component, event, helper,pagesPerRecod,currentPageNumber,pageIdMap,selectedFilterMap);
        } else if ('plan' == searchFlag) {
            //helper.searchTitlePlansAndWindows(component, event, helper,pagesPerRecod,currentPageNumber,pageIdMap,selectedFilterMap);
            helper.fetchWindowGuidelineIdWithReleaseDateTypeSearch(component, event, helper,pagesPerRecod,currentPageNumber,pageIdMap,selectedFilterMap);
       
        }        
    },
    //LRCC-1518
    fetchWindowGuidelineIdWithReleaseDateTypeSearch : function (component, event, helper,pagesPerRecod,currentPageNumber,pageIdMap,selectedFilterMap) {
        //LRCC-1885
        var windowSearchReleaseDateHeader = component.get('v.windowSearchReleaseDateHeader');
        var windowSearchReleaseDateValues = component.get('v.windowSearchReleaseDateValues');
        
        var releaseDateParam;
        var releaseDateTypes = helper.collectNames(component.get('v.selectedReleaseDateTypes'));
        //LRCC-1885
        windowSearchReleaseDateHeader.push('Release Date Type');
        windowSearchReleaseDateValues.push([releaseDateTypes]);
        component.set('v.windowSearchReleaseDateHeader',windowSearchReleaseDateHeader);
        component.set('v.windowSearchReleaseDateValues',windowSearchReleaseDateValues);
        
        if(releaseDateTypes != null){
            
            releaseDateParam = {'types':releaseDateTypes}    
        }
         console.log('releaseDateParam:::',releaseDateParam);
        if(releaseDateParam != null){
            var action = component.get('c.getWGWithRelaseDateType');
            action.setParams(releaseDateParam);
            action.setCallback(this, function (response) {
                
                console.log('response.getReturnValue:::win::get:',JSON.parse(response.getReturnValue()));
                var state = response.getState();
                if (state === 'SUCCESS') {
                    
                    var results =JSON.parse(response.getReturnValue());
                    console.log('windowGuideLineAliasSet ids::::::',results);
                    if(results && results.windowGuideLineAliasSet.length){
                        
                        helper.searchTitlePlansAndWindows(component, event, helper,pagesPerRecod,currentPageNumber,pageIdMap,selectedFilterMap,results.windowGuideLineAliasSet);
                    }else{
                        
                        helper.searchTitlePlansAndWindows(component, event, helper,pagesPerRecod,currentPageNumber,pageIdMap,selectedFilterMap,null);
                    }
                }
            });
            $A.enqueueAction(action);
        }else{
            helper.searchTitlePlansAndWindows(component, event, helper,pagesPerRecod,currentPageNumber,pageIdMap,selectedFilterMap,null);
        }
    },    
    searchTitlePlansAndWindows: function (component, event, helper,pagesPerRecod,currentPageNumber,pageIdMap,selectedFilterMap,rdtypeWGNames) {
			
        var headerList = [];
        var valueList = [];
        var titleList = [];
		
		
        var retired = component.get('v.status');
        if(retired != '--None--') {
        	headerList.push('Retired');
        	valueList.push([retired]);    
        }
        
        var medias = helper.collectNames(component.get('v.selectedMedias'));
        headerList.push('Media');
        valueList.push([medias]);
		
        var territories = helper.collectNames(component.get('v.selectedTerritories'));
        headerList.push('Territoty');
        valueList.push([territories]);

        var languages = helper.collectNames(component.get('v.selectedLanguages'));
        headerList.push('Languages');
        valueList.push([languages]);

        var titles = helper.collectIds(component.get('v.selectedTitles'));
        titleList = component.get('v.selectedTitles');

        var plans = helper.collectIds(component.get('v.selectedPlans'));
        headerList.push('Plans');
        valueList.push(helper.collectNames(component.get('v.selectedPlans')));

        var windowTags = helper.collectIds(component.get('v.selectedWindowTags'));
        headerList.push('Window Tags');
        valueList.push(helper.collectNames(component.get('v.selectedWindowTags')));
        
        var windowNames = helper.collectNames(component.get('v.selectedWindows'));
        console.log('windowNames::be:',windowNames);
         
        //LRCC-1518
        if(rdtypeWGNames && rdtypeWGNames.length){
            
            for(var i=0 ; i < rdtypeWGNames.length; i++){
                
                if(windowNames.indexOf(rdtypeWGNames[i]) == -1){
                    
                    windowNames.push(rdtypeWGNames[i]);
                } 
            }    
        }
        
        headerList.push('Window Names');
        valueList.push(helper.collectNames(component.get('v.selectedWindows')));
        
        //LRCC:1519
        //LRCC-1731
        var windowType = component.find('windowType') ? component.find('windowType').get('v.selectedList') : '';
        var tempwindowType = [];
        windowType.forEach(function (element) {
            tempwindowType.push(element.value);
        });
        windowType = tempwindowType;
        headerList.push('Window Type');
        valueList.push([windowType]);
        

        console.log('windowType::::',windowType);
        //LRCC:1022
        //LRCC-1731
        var windowStatus = component.find('windowStatus') ? component.find('windowStatus').get('v.selectedList') : '';
        var tempWindowStatus = [];
        windowStatus.forEach(function (element) {
            tempWindowStatus.push(element.value);
        });
        windowStatus = tempWindowStatus;
        headerList.push('Window Status');
        valueList.push([windowStatus]);
		
        
        var windowDateStart = component.find('windowDateStart') ? component.find('windowDateStart').get('v.value') : '';
        headerList.push('Window Start From Date');
        valueList.push([windowDateStart]);
        component.set('v.selectedWindowStartDateStart', windowDateStart);
		
        
        var windowDateEnd = component.find('windowDateEnd') ? component.find('windowDateEnd').get('v.value') : '';
        headerList.push('Window Start To Date');
        valueList.push([windowDateEnd]);
        component.set('v.selectedWindowStartDateEnd', windowDateEnd);
		
        
        /****LRCC-783: Ability to Search by Window End Date****/
        var windowEndDateStart = component.find('windowEndDateStart') ? component.find('windowEndDateStart').get('v.value') : '';
        headerList.push('Window End From Date');
        valueList.push([windowEndDateStart]);
        component.set('v.selectedWindowEndDateStart', windowEndDateStart);

        var windowEndDateEnd = component.find('windowEndDateEnd') ? component.find('windowEndDateEnd').get('v.value') : '';
        headerList.push('Window End To Date');
        valueList.push([windowEndDateEnd]);
        component.set('v.selectedWindowEndDateEnd', windowEndDateEnd);

        var windowOutsideDateStart = component.find('windowOutsideDateStart') ? component.find('windowOutsideDateStart').get('v.value') : '';
        headerList.push('Window Outside Start Date');
        valueList.push([windowOutsideDateStart]);
        component.set('v.selectedWindowOutsideDateStart', windowOutsideDateStart);

        var windowOutsideDateEnd = component.find('windowOutsideDateEnd') ? component.find('windowOutsideDateEnd').get('v.value') : '';
        headerList.push('Window Outside End Date');
        valueList.push([windowOutsideDateEnd]);
        component.set('v.selectedWindowOutsideDateEnd', windowOutsideDateEnd);

        // LRCC:1022
        // LRCC-1731
        var windowRightStatus = component.find('windowRightStatus') ? component.find('windowRightStatus').get('v.selectedList') : '';
        var tempWindowRightStatus = [];
        windowRightStatus.forEach(function (element) {
            tempWindowRightStatus.push(element.value);
        });
        windowRightStatus = tempWindowRightStatus;
        headerList.push('Right Status');
        valueList.push([windowRightStatus]);

        // LRCC:1022	
        // LRCC-1731
        var windowLicenseInfoCodes = component.find('windowLicenseInfoCodes') ? component.find('windowLicenseInfoCodes').get('v.selectedList') : '';
        var tempWindowLicenseInfoCodes = [];
        windowLicenseInfoCodes.forEach(function (element) {
            
            tempWindowLicenseInfoCodes.push(element.value);
        });
        windowLicenseInfoCodes = tempWindowLicenseInfoCodes;
        headerList.push('License Info Code');
        valueList.push([windowLicenseInfoCodes]);
        
        //LRCC-1628
        var infoCodeSearchType = component.get('v.infoCodeSearchType');
         
        
        var releaseDateStart = component.find('releaseDateStart') ? component.find('releaseDateStart').get('v.value') : '';
        var releaseDateEnd = component.find('releaseDateEnd') ? component.find('releaseDateEnd').get('v.value') : '';
        
        //LRCC-1557
        //var releaseDateTypes = component.find('releaseDateType') ? component.find('releaseDateType').get('v.value') : '';
        var releaseDateTypes = component.get('v.selectedReleaseDateTypes');
        var releaseDateStatus = (component.find('releaseDateStatus')&& component.find('releaseDateStatus').get('v.value')) ? component.find('releaseDateStatus').get('v.value') : '';//component.find('releaseDateStatus') 
        var releaseDateTags = component.get('v.selectedReleaseDateTags');
        //console.log('releaseDateStatus:::::window:::',component.find('releaseDateStatus'));
        //console.log('releaseDateStatus:::::window::value:',(component.find('releaseDateStatus')&& component.find('releaseDateStatus').get('v.value')) ? component.find('releaseDateStatus').get('v.value') : 'test');
        if (windowDateEnd && windowDateStart > windowDateEnd) {
            console.log('con enter:::', windowDateStart, windowDateEnd);
            helper.errorToast('Window Start From must be earlier than Window Start To');
        }
        //LRCC-1097 added to check window start date is before end date. 
        else if (windowEndDateEnd && windowEndDateStart > windowEndDateEnd) {
            helper.errorToast('Window End date should not be earlier than window start date');
        } else if (windowDateStart && windowEndDateStart) {
            if (windowEndDateStart < windowDateStart) {
                helper.errorToast('Window End From must be earlier than Window End To');
            }
        } else if (windowOutsideDateEnd && windowOutsideDateStart > windowOutsideDateEnd) {
            helper.errorToast('Outside End From must be earlier than Outside Start To');
        } else {
            // Don't allow user to search if the search criteria is incomplete && give them an error msg
            if ((releaseDateStart === undefined || releaseDateStart == '') &&
                (releaseDateEnd === undefined || releaseDateEnd == '') &&
                releaseDateTags == '' && releaseDateStatus == '') {
                //LRCC-1518 -- && releaseDateTypes == '' 
				
                if (helper.setSearchCriteriaString(component,helper, headerList, valueList, titleList, true) != null || releaseDateTypes != '') {
                    
                    component.set('v.searchCriteriaString', helper.setSearchCriteriaString(component,helper, headerList, valueList, titleList, true));
                    console.log('plans::::::::::',plans);
                    var action = component.get('c.getTitlePlanAndWindows');
                    action.setParams({
                        'medias': medias,
                        'territories': territories,
                        'languages': languages,
                        'titles': titles,
                        'plans': plans,
                        'windowTags': windowTags,
                        'windowNames': windowNames,
                        'windowType': windowType,
                        'windowStatus': windowStatus,
                        'windowDateStart': windowDateStart,
                        'windowDateEnd': windowDateEnd,

                        'windowEndDateStart': windowEndDateStart,
                        'windowEndDateEnd': windowEndDateEnd,

                        'windowOutsideDateStart': windowOutsideDateStart,
                        'windowOutsideDateEnd': windowOutsideDateEnd,
                        'windowRightStatus': windowRightStatus,
                        'windowLicenseInfoCodes': windowLicenseInfoCodes,
                        'infoCodeSearchType' : infoCodeSearchType, // LRCC-1628
                        'retire': retired,
                        'pagesPerRecord' : pagesPerRecod,
                        'currentPageNumber':currentPageNumber,
                        'pageIdMapUI':pageIdMap,
                        'selectedFilterMap':selectedFilterMap
                    });
                    component.set('v.windowSearchParam',JSON.parse(JSON.stringify(action.getParams())));
                    console.log('windowSearchParam::::::',JSON.stringify(component.get('v.windowSearchParam')));
                    helper.showSpinner(component);

                    action.setCallback(this, function (response) {
                        var state = response.getState();
                        helper.hideSpinner(component);
                        var titlePlans = [];

                        if (state === 'SUCCESS') {
                            console.log('<<ressss::::>>',JSON.parse(JSON.stringify(response.getReturnValue())));
                            var numberOfWindows = response.getReturnValue().totalPlansAndNumberofWindows;
                            var results = response.getReturnValue().windowRecordList;
                            var titleTagMap = response.getReturnValue().titleTagMap;
                            //response.getReturnValue().titlePlanWindows;

                            //LRCC-1019
                            if (document.getElementById("collapseandexpandBtn") && document.getElementById("collapseandexpandBtn")) {
                                var ceb = document.getElementById("collapseandexpandBtn");
                                ceb.style.display = "none";

                                var cef = document.getElementById("collapseandexpandfilter");
                                cef.style.display = "none";
                            }
                            component.set('v.SearchContainerState', false);

                            if (results) {
                                
                                //LRCC-1459
                                if(response.getReturnValue().windowExportQueryString != null ){
                                    component.set('v.windowExportQuery',response.getReturnValue().windowExportQueryString);
                                }
                                if(response.getReturnValue().totalPlanCount != null){
                                    
                                    component.set('v.totalRecordCount',response.getReturnValue().totalPlanCount); 
                                }else{
                                    component.set('v.totalRecordCount',0);
                                }
                                component.set('v.initialTableShow',true);
                                var windows = [];
                                for (var i = 0; i <results.length; i++) {
                                    var window =results[i];
                                    
                                    if (results[i].EAW_Window_Guideline__c &&results[i].EAW_Window_Guideline__r) {
                                        window.windowGuidelineName =results[i].EAW_Window_Guideline__r.Window_Guideline_Alias__c; //LRCC-1493.
                                    }
                                    if (results[i].EAW_Plan__c &&results[i].EAW_Plan__r.EAW_Title__c &&results[i].EAW_Plan__r.EAW_Title__c &&results[i].EAW_Plan__r.EAW_Title__r.PROD_TYP_CD_INTL__c &&results[i].EAW_Plan__r.EAW_Title__r.PROD_TYP_CD_INTL__r.Name) {
                                        window.PROD_TYP_CD_INTL__c =results[i].EAW_Plan__r.EAW_Title__r.PROD_TYP_CD_INTL__r.Name;
                                    }
                                    if (results[i].EAW_Plan__c &&results[i].EAW_Plan__r.EAW_Title__c &&results[i].EAW_Plan__r.EAW_Title__c &&results[i].EAW_Plan__r.EAW_Title__r.FIN_DIV_CD__c &&results[i].EAW_Plan__r.EAW_Title__r.FIN_DIV_CD__r.Name) {
                                        window.FIN_DIV_CD__c =results[i].EAW_Plan__r.EAW_Title__r.FIN_DIV_CD__r.Name;
                                    }
                                    //LRCC-1551
                                    if (results[i].EAW_Plan__c &&results[i].EAW_Plan__r.EAW_Title__c &&results[i].EAW_Plan__r.EAW_Title__r.Name__c ) {
                                        window.titleName =results[i].EAW_Plan__r.EAW_Title__r.Name__c; // LRCC - 1870
                                    }
                                    //LRCC-1700
                                    if (results[i].EAW_Plan__c &&results[i].EAW_Plan__r.Plan_Guideline_Name__c ) {
                                        window.planGuidelineName =results[i].EAW_Plan__r.Plan_Guideline_Name__c;
                                    }
                                    window.Title =results[i].Name;
                                    window.link = '/one/one.app?sObject/' +results[i].EAW_Window_Guideline__c + '/view#/sObject/' +results[i].EAW_Window_Guideline__c + '/view';
                                    window.FIN_PROD_ID__c = results[i].EAW_Plan__r.EAW_Title__r.FIN_PROD_ID__c;
                                    window.PlanName =results[i].EAW_Plan__r.Name__c;//results[0].Acquired_Indicator__c;
                                   // window.TitleTags =results[i].Approx_Run_Time__c;
                                    window.TitleTags = '';
                                    if(titleTagMap[results[i].EAW_Plan__r.EAW_Title__c]){
                                        window.TitleTags = titleTagMap[results[i].EAW_Plan__r.EAW_Title__c];
                                    }
                                    //LRCC:1096 & LRCC-1010
                                    window.windowTags = JSON.parse(results[i].CurrencyIsoCode)||[];
                                    delete window.CurrencyIsoCode;
                                   //LRCC-1690
                                    if(window.windowTags){
                                        
                                      window.windowTagsSortColumn = window.windowTags.map(x => x.Name).join(', ');
       
                                    }
                                    //LRCC-1018
                                    var indicationInstatnce = {'redcolor':[],
                                                               'tooltip':[],
                                                               'tooltipMsg':{}}
                                    
                                    if(results[i].Start_Date__c && results[i].Outside_Date__c && (results[i].Start_Date__c > results[i].Outside_Date__c)){
                                        indicationInstatnce.redcolor.push('Start_Date__c');
                                        indicationInstatnce.tooltip.push('Start_Date__c');
                                        
                                        if(!indicationInstatnce.tooltipMsg['Start_Date__c']){
                                            
                                        	 indicationInstatnce.tooltipMsg['Start_Date__c'] = [];   
                                        }
                                        indicationInstatnce.tooltipMsg['Start_Date__c'].push('The Window Start Date is past the Outside Date');
                                        /*var indi = {'redcolor':'Start_Date__c',
                                                    'tooltip':'Outside_Date__c',
                                                    'tooltipMsg':'The Window Start Date is past the Outside Date'}
                                       */
                                    }
                                    if((results[i].Projected_Outside_Date__c != results[i].Outside_Date__c) && results[i].Status__c == 'Firm'){
                                        
                                        indicationInstatnce.redcolor.push('Outside_Date__c');
                                        indicationInstatnce.tooltip.push('Outside_Date__c');
                                        
                                        if(!indicationInstatnce.tooltipMsg['Outside_Date__c']){
                                            
                                            indicationInstatnce.tooltipMsg['Outside_Date__c'] = [];   
                                        }
                                        indicationInstatnce.tooltipMsg['Outside_Date__c'].push('The Projected Outside Date has changed since the Window Schedule was firmed');
                                        
                                    }
                                    if((results[i].Projected_Tracking_Date__c != results[i].Tracking_Date__c) && results[i].Status__c == 'Firm'){
                                        
                                        indicationInstatnce.redcolor.push('Tracking_Date__c');
                                        indicationInstatnce.tooltip.push('Tracking_Date__c');
                                        
                                        if(!indicationInstatnce.tooltipMsg['Tracking_Date__c']){
                                            
                                            indicationInstatnce.tooltipMsg['Tracking_Date__c'] = [];   
                                        }
                                        indicationInstatnce.tooltipMsg['Tracking_Date__c'].push('The Projected Tracking Date has changed since the Window Schedule was firmed');
                                    }
                                    window.indication = indicationInstatnce;
                                    windows.push(window);
                                }                
                                /*for (var key in results) {
                                    var i;
                                    var windows = [];

                                    // If WINDOWS are attached to Title-Plan
                                    //if (results[key].length > 1) {
                                        // Attach all title plan data to each window for ease of display
                                        //i = 1
                                        for (i = 0; i < results[key].length; i++) {
                                            var window = results[key][i];
                                            
                                            if (results[key][i].EAW_Window_Guideline__c && results[key][i].EAW_Window_Guideline__r) {
                                                window.windowGuidelineName = results[key][i].EAW_Window_Guideline__r.Window_Guideline_Alias__c; //LRCC-1493.
                                            }
                                            if (results[key][i].EAW_Plan__c && results[key][i].EAW_Plan__r.EAW_Title__c && results[key][i].EAW_Plan__r.EAW_Title__c && results[key][i].EAW_Plan__r.EAW_Title__r.PROD_TYP_CD_INTL__c && results[key][i].EAW_Plan__r.EAW_Title__r.PROD_TYP_CD_INTL__r.Name) {
                                                window.PROD_TYP_CD_INTL__c = results[key][i].EAW_Plan__r.EAW_Title__r.PROD_TYP_CD_INTL__r.Name;
                                            }
                                            if (results[key][i].EAW_Plan__c && results[key][i].EAW_Plan__r.EAW_Title__c && results[key][i].EAW_Plan__r.EAW_Title__c && results[key][i].EAW_Plan__r.EAW_Title__r.FIN_DIV_CD__c && results[key][i].EAW_Plan__r.EAW_Title__r.FIN_DIV_CD__r.Name) {
                                                window.FIN_DIV_CD__c = results[key][i].EAW_Plan__r.EAW_Title__r.FIN_DIV_CD__r.Name;
                                            }
                                            //LRCC-1551
                                            if (results[key][i].EAW_Plan__c && results[key][i].EAW_Plan__r.EAW_Title__c && results[key][i].EAW_Plan__r.EAW_Title__r.Name ) {
                                                window.titleName = results[key][i].EAW_Plan__r.EAW_Title__r.Name;
                                            }
                                            //LRCC-1700
                                            if (results[key][i].EAW_Plan__c && results[key][i].EAW_Plan__r.Plan_Guideline_Name__c ) {
                                                window.planGuidelineName = results[key][i].EAW_Plan__r.Plan_Guideline_Name__c;
                                            }
											window.Title = results[key][0].Name;
                                            window.link = '/one/one.app?sObject/' + results[key][i].EAW_Window_Guideline__c + '/view#/sObject/' + results[key][i].EAW_Window_Guideline__c + '/view';
                                            window.FIN_PROD_ID__c = results[key][0].FIN_PROD_ID__c;
                                            window.PlanName = results[key][0].Acquired_Indicator__c;
                                            window.TitleTags = results[key][0].Approx_Run_Time__c;
                                            //LRCC:1096 & LRCC-1010
                                            window.windowTags = JSON.parse(results[key][i].CurrencyIsoCode);
                                            delete window.CurrencyIsoCode;
                                            windows.push(window);
                                        }
                                    //}

                                    // Set Title and Plan IDs
                                  /*results[key][0].TitleId = results[key][0].Id;
                                   results[key][0].Id = key;
                                    var totalWindows = numberOfWindows[key] || 0;

                                    let titlePlanId = '';
                                    if (event.getParam('titlePlanId')) {
                                        titlePlanId = event.getParam("titlePlanId");
                                    }

                                    if (key && titlePlanId && key === titlePlanId) {
                                        if (!titlePlanId && titlePlans.indexOf(key)) {
                                            titlePlans.remove(key);
                                        } else if (event.getParam('titlePlanId')) {
                                            titlePlans.push({
                                                TitlePlan: results[key][0],
                                                Windows: windows,
                                                showWindows: true,
                                                selectedWindows: [],
                                                totalWindowSize: totalWindows
                                            });
                                        }
                                    } else {
                                        titlePlans.push({
                                            TitlePlan: results[key][0],
                                            Windows: windows,
                                            showWindows: false,
                                            selectedWindows: [],
                                            totalWindowSize: totalWindows
                                        });
                                    }
                                }*/
                                console.log('windows::::::',windows);
                                //titlePlans.push({Windows: windows});
                                 component.set('v.resultData', windows);
                                //component.set('v.resultData', titlePlans);
                                console.log('::::=re::::',component.get('v.resultData'));
                                //LRCC-1459
                                if (component.find('titlePlanDateResults')) {
                                    var childrefreshFlag =  component.get("v.childrefresh");
                                    component.find('titlePlanDateResults').reConstruct(childrefreshFlag);
                                }
                            } else {
                                component.set('v.resultData', []);
                            }
                            component.set('v.editMode', false);
                        } else if (state === 'ERROR') {
                           
                            component.set('v.windowObj', []);
                            
                            component.set('v.resultData', []);
                            
                            console.log("Error getting title-plan windows:::::");
                            //LRCC-1019
                            if (document.getElementById("collapseandexpandBtn") && document.getElementById("collapseandexpandBtn")) {
                                var ceb = document.getElementById("collapseandexpandBtn");
                                ceb.style.display = "block";

                                var cef = document.getElementById("collapseandexpandfilter");
                                cef.style.display = "block";
                            }
                            component.set('v.SearchContainerState', false);
                        }

                        helper.hideReleaseDateResults(component); // Hiding Release Dates = Show Title-Plan Windows (bc its the else)

                    });
                    $A.enqueueAction(action);
                } else {
                	//LRCC-1390
                    if( component.get("v.EventName") != 'cmpEvent'){
                    
                    	helper.errorToast('At least one criteria must be provided before a search is performed.');
                    }
                    //LRCC-1012 -commented for this ticket
                    //if (component.get('v.resultData').length) {
                    
                    	//component.set('v.resultData', []);
                    	//LRCC-1459
                        //if (component.find('titlePlanDateResults')) {
                            //component.find('titlePlanDateResults').reConstruct();
                        //}
                    //}
                }
            } else {
                helper.errorToast('Invalid Search Criteria Entered');
            }
        }
    },
    //LRCC-1675
    exportWindows:function (component, event, helper,pagesPerRecod,currentPageNumber,pageIdMap,selectedFilterMap) {
  		
        var totalRecordCount = component.get('v.totalRecordCount') > 32000 ? 32000 : component.get('v.totalRecordCount');
        //var totalRecordCount = component.get('v.totalRecordCount');
        console.log('totalRecordCount:::',totalRecordCount);
        var recordLimit = 2000;
        
        var numberOfLoop = Math.floor((totalRecordCount+recordLimit - 1 )/recordLimit);
        console.log('numberOfLoop:::',numberOfLoop);
        var allExportWindows = [];
        var lastBatchIds = [];
        var lastRecordCreatedDate ;
        
        var i = 1;
        var finalresult = helper.recursiveExportAction(component, event, helper,allExportWindows,lastBatchIds,lastRecordCreatedDate,recordLimit,numberOfLoop,i)
        console.log('return final ::ssddssd:',finalresult);
        
    },
    recursiveExportAction : function(component, event, helper,allExportWindows,lastBatchIds,lastRecordCreatedDate,recordLimit,numberOfLoop,index){
        
        var windowSearchParam = component.get('v.windowSearchParam');
            windowSearchParam.windowExportQuery = component.get('v.windowExportQuery');
            windowSearchParam.lastRecordCreatedDate = lastRecordCreatedDate;
            windowSearchParam.lastBatchIds = lastBatchIds;
            windowSearchParam.recordLimit = recordLimit;
            
            var action = component.get('c.exportWindows');
            action.setParams(windowSearchParam);
            helper.showSpinner(component);
            action.setCallback(this, function (response) {
                
                
               
                var state = response.getState();
                helper.hideSpinner(component);
                var titlePlans = [];
                console.log('index::::',index)
                if (state === 'SUCCESS') {
                    
                    var results = response.getReturnValue();
                    console.log(':::results:::',results);
                    console.log(':::results length:::',results.length);
                    if(results.length){
                        
                        
                        var windows = [];
                        for (var i = 0; i <results.length; i++) {
                            var window =results[i];
                            
                            if (results[i].EAW_Window_Guideline__c && results[i].EAW_Window_Guideline__r) {
                                window.windowGuidelineName =results[i].EAW_Window_Guideline__r.Window_Guideline_Alias__c; //LRCC-1493.
                            }
                            
                            if (results[i].EAW_Plan__c && results[i].EAW_Plan__r.EAW_Title__c &&results[i].EAW_Plan__r.EAW_Title__c &&results[i].EAW_Plan__r.EAW_Title__r.PROD_TYP_CD_INTL__c &&results[i].EAW_Plan__r.EAW_Title__r.PROD_TYP_CD_INTL__r.Name) {
                                window.PROD_TYP_CD_INTL__c =results[i].EAW_Plan__r.EAW_Title__r.PROD_TYP_CD_INTL__r.Name;
                            }
                            
                            if (results[i].EAW_Plan__c &&results[i].EAW_Plan__r.EAW_Title__c &&results[i].EAW_Plan__r.EAW_Title__c &&results[i].EAW_Plan__r.EAW_Title__r.FIN_DIV_CD__c &&results[i].EAW_Plan__r.EAW_Title__r.FIN_DIV_CD__r.Name) {
                                window.FIN_DIV_CD__c =results[i].EAW_Plan__r.EAW_Title__r.FIN_DIV_CD__r.Name;
                            }
                            
                            //LRCC-1551
                            if (results[i].EAW_Plan__c &&results[i].EAW_Plan__r.EAW_Title__c &&results[i].EAW_Plan__r.EAW_Title__r.Name__c ) { // LRCC - 1870
                                window.titleName =results[i].EAW_Plan__r.EAW_Title__r.Name__c;
                            }
                            
                            //LRCC-1700
                            if (results[i].EAW_Plan__c &&results[i].EAW_Plan__r.Plan_Guideline_Name__c ) {
                                window.planGuidelineName =results[i].EAW_Plan__r.Plan_Guideline_Name__c;
                            }
                            
                            window.Title =results[i].Name;
                            
                            window.link = '/one/one.app?sObject/' +results[i].EAW_Window_Guideline__c + '/view#/sObject/' +results[i].EAW_Window_Guideline__c + '/view';
                            
                            window.FIN_PROD_ID__c =results[i].EAW_Plan__r.EAW_Title__r.FIN_PROD_ID__c;
                            
                            window.PlanName =results[i].EAW_Plan__r.Name__c;//results[0].Acquired_Indicator__c;
                            //Export issue fix
                            if(results[i].EAW_Title_Attribute__r && results[i].EAW_Title_Attribute__r.Approx_Run_Time__c){
                                
                               window.TitleTags = results[i].EAW_Title_Attribute__r.Approx_Run_Time__c;
                            }
                            
                            //LRCC:1096 & LRCC-1010
                            window.windowTags = JSON.parse(results[i].CurrencyIsoCode)||[];
                            
                            delete window.CurrencyIsoCode;
                            
                            windows.push(window);
                            
                            if(results.length - 1 == i){
                                
                                lastRecordCreatedDate = results[i].CreatedDate ;
                            }
                            allExportWindows.push(results[i]);
                            lastBatchIds.push(results[i].Id);
                        }  
                        console.log('windows length:::',windows); 
                        //console.log('allExportWindows::before:',allExportWindows); 
                        //console.log('lastBatchIds::before:',lastBatchIds);
                        if(allExportWindows.length || windows.length){
                            //allExportWindows = allExportWindows.concat(windows);
                            console.log('allExportWindows::afetr:',allExportWindows);
                            var tagArray = windows.map(x => x.Id);
                            //lastBatchIds = tagArray;
                            console.log('numberOfLoop > index::afetr:',numberOfLoop , index);
                            console.log(':::lastBatchIds::::'+lastBatchIds);
                            if(numberOfLoop > index){
                                
                                index = index+1;
                                helper.recursiveExportAction(component, event, helper,allExportWindows,lastBatchIds,lastRecordCreatedDate,recordLimit,numberOfLoop,index);
                                
                            }else{
                                
                                let data = allExportWindows;
                                console.log(data); 
                                console.log('data length:::',data.length); 
                                if(data == null) {
                                    return;
                                }
                                
                                let csv = helper.convertArrayToCSV(component, data,helper);
                                var csvData = new Blob([csv], { type: 'text/plain' });
                                var csvUrl = URL.createObjectURL(csvData);
                                var link = document.createElement("a");
                                link.setAttribute('href', csvUrl);
                                link.setAttribute('download', 'Plans&Windows_Results.csv');
                                link.click();
                                helper.hideSpinner(component);
                                return allExportWindows;
                                
                            }
                            
                            
                        }
                    }else if(allExportWindows.length){
                        let data = allExportWindows;
                                console.log(data); 
                                console.log('data length:::',data.length); 
                                if(data == null) {
                                    return;
                                }
                                
                                let csv = helper.convertArrayToCSV(component, data,helper);
                                var csvData = new Blob([csv], { type: 'text/plain' });
                                var csvUrl = URL.createObjectURL(csvData);
                                var link = document.createElement("a");
                                link.setAttribute('href', csvUrl);
                                link.setAttribute('download', 'Plans&Windows_Results.csv');
                                link.click();
                        		helper.hideSpinner(component);
                                return allExportWindows;
                    }
                    
            } else if (state === 'ERROR') {
                console.log('Error ::::::',response.getReturnValue());
            }
        });
            $A.enqueueAction(action);
            
    },
        convertArrayToCSV : function(component,data,helper){
            
        var csvStringResult, counter, header, keys, columnDivider, lineDivider;
            
        if(data == null || !data.length) {
            return null;
        }
        
        // store ,[comma] in columnDivider variabel for sparate CSV values and
        // for start next line use '\n' [new line] in lineDivider varaible
        columnDivider = ',';
        lineDivider =  '\n';
        
        // in the keys variable store fields API Names as a key
        // this labels use in CSV file header
        console.log(component.get('v.resultColumns'));
        var columns = JSON.parse(JSON.stringify(component.get('v.resultColumns')));
        header = [];//['Title Name','Plan Name'];
        keys =[];
        
        csvStringResult = component.get('v.searchCriteriaString') || '';
        
        for(var i=0;i<columns.length;i++){
            header.push(columns[i].label); //this array is the 'pretty' column names for the csv
            keys.push(columns[i].fieldName); //this array is the actual column name
        }
        
        
            //csvStringResult += header.join(columnDivider);
            csvStringResult = component.get('v.searchCriteriaString') || '';
            csvStringResult += header.join(columnDivider);
            csvStringResult += lineDivider;
            var ivalue = 0;
            for(var i=0;i<data.length;i++){
                //ivalue = i;
                //csvStringResult += data[i][Id]+columnDivider;//ivalue+columnDivider;
                //csvStringResult += lineDivider;
                //csvStringResult += (data[i].TitlePlan.Name || '').replace(',','');
                //csvStringResult += columnDivider + (data[i].TitlePlan.Acquired_Indicator__c || '').replace(',','') + columnDivider;
                counter = 0;
               
                for(var sTempkey in keys) {
                     
                    var skey = keys[sTempkey];
                    
                    if(counter > 0){
                        
                        csvStringResult += columnDivider;
                    }
                    if(columns[sTempkey].type == 'date' && !$A.util.isEmpty(data[i][skey])){
                        
                        var resultData = helper.getFormattedDate(data[i][skey]);
                    } else {
                       
                        //LRCC-1014 - 'Fin'l Title Id' column data is displayed incorrectly
                        //LRCC-1675
                        
                        if(keys[sTempkey] == 'windowTags' && data[i][skey] && data[i][skey].length){
                            
                            var tagArray = data[i][skey].map(x => x.Name);
                            var resultData = tagArray.join(';');
                        }else{
                            if(skey == 'Is_Confidential_Window__c' || skey == 'Retired__c') { //LRCC-1828
                                var resultData = data[i][skey] ? 'Yes' : 'No';
                            } else {
                                var resultData = (skey=='FIN_PROD_ID__c')?(data[i][skey])?'=""'+data[i][skey]+'""':'': data[i][skey] || '';
                            }                            
                        }
                    }
                    csvStringResult += '"'+resultData+'"';
                    counter++;
                } 
                csvStringResult += lineDivider;
            }
        return csvStringResult.replace(/#/g, '');
    },
    getFormattedDate : function(dateToFormat){
        
        //LRCC-1675
        var mydate = dateToFormat.split('-');
        var month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                     "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"][mydate[1]-1];
        //LRCC-1691
        
        return ' '+mydate[2] + '-' + month + '-' +  mydate[0]+' ';//.trim();
    },
    exportReleaseDate: function (component, event, helper) {
        
        var totalRecordCount = component.get('v.totalRecordCount') > 32000 ? 32000 : component.get('v.totalRecordCount');
        //var totalRecordCount = component.get('v.totalRecordCount');
        console.log('totalRecordCount:::',totalRecordCount);
        var recordLimit = 2000;
        
        var numberOfLoop = Math.floor((totalRecordCount+recordLimit - 1 )/recordLimit);
        console.log('numberOfLoop:::',numberOfLoop);
        var allExportRecords = [];
        var lastBatchIds = [];
        var lastRecordCreatedDate ;
        var lastRecordId;
        var i = 1;
        helper.showSpinner(component);
        var finalresult = helper.recursiveReleaseDateExportAction(component, event, helper,allExportRecords,lastBatchIds,lastRecordCreatedDate,recordLimit,numberOfLoop,i,lastRecordId);
        console.log('return final ::releasedate:::',finalresult);
       
    },
    recursiveReleaseDateExportAction : function(component, event, helper,allExportRecords,lastBatchIds,lastRecordCreatedDate,recordLimit,numberOfLoop,index,lastRecordId){
        
        var releaseDateSearchParam = component.get('v.releaseDateSearchParam');
        releaseDateSearchParam.lastRecordCreatedDate = lastRecordCreatedDate;
        releaseDateSearchParam.lastBatchIds = lastBatchIds;
        releaseDateSearchParam.recordLimit = recordLimit;
        releaseDateSearchParam.exportFlag = 'export';
        releaseDateSearchParam.lastRecordId = lastRecordId;
        
        var action = component.get('c.searchReleaseDateByCriteria');
        var searchParam = component.get('v.releaseDateSearchParam');
       // console.log('releaseDateSearchParam:::export:::',JSON.stringify(component.get('v.releaseDateSearchParam')));
               
       
        action.setParams(component.get('v.releaseDateSearchParam'));
        action.setCallback(this, function (response) {
                          
            var state = response.getState();
            if (state === 'SUCCESS') {
                
                //console.log('export::results:',response.getReturnValue())
                var results = response.getReturnValue().releaseDateData;
               
                if (results) {
                    
                    if (Array.isArray(results.releaseDates)) {
                        console.log('index::',index,'export::results:', results.releaseDates)
                        for (var i = 0; i < results.releaseDates.length; i++) {
                            var releaseDate = results.releaseDates[i];
                            //Tags string to object
                            //upd
                            if (results.releaseDateTagJunctions && results.releaseDateTagJunctions.length > 0) {
                                releaseDate.releaseDateTags = results.releaseDateTagJunctions.filter(tag => tag.Tag__c && tag.Release_Date__c == releaseDate.Id)
                                .map(x => x.Tag__r);
                            } else {
                                releaseDate.releaseDateTags = [];
                            }
                            
                            if (results.titleTagJunctions && results.titleTagJunctions.length > 0) {
                                releaseDate.titleTags = results.titleTagJunctions.filter(tag => tag.Tag__c && tag.Title_Attribute__c == releaseDate.Title__c)
                                .map(x => x.Tag__r.Name)
                                .join(', ');
                            }
                            
                            if (releaseDate.Title__r) {
                                //LRCC-1539
                                releaseDate.Name = releaseDate.Title__r.Name__c; // LRCC - 1870
                                //releaseDate.TitleName = releaseDate.Title__r.Name__c;
                                releaseDate.titleLink = '/one/one.app?sObject/' + releaseDate.Title__r.Id + '/view#/sObject/' + releaseDate.Title__r.Id + '/view';
                            }
                            
                            if (releaseDate.Title__r && releaseDate.Title__c) {
                                releaseDate.edmTitleFinProdId = releaseDate.Title__r.FIN_PROD_ID__c;
                                if (releaseDate.Title__r.PROD_TYP_CD_INTL__r) {
                                    releaseDate.edmTitleIntlProdType = releaseDate.Title__r.PROD_TYP_CD_INTL__r.Name;
                                }
                            }
                            
                            if (releaseDate.Customer__r) {
                                releaseDate.customerName = releaseDate.Customer__r.Name;
                            }
                            
                            if (releaseDate.EAW_Release_Date_Type__c) {
                                releaseDate.releaseDateTypeName = releaseDate.EAW_Release_Date_Type__c;
                            }
                            
                            if(results.releaseDates.length - 1 == i){
                                console.log('results.releaseDates[i]::afetr:',results.releaseDates[i]);
                                lastRecordCreatedDate = releaseDate.CreatedDate ;
                                lastRecordId = releaseDate.Id ;
                                console.log('lastRecordId::afetr:',lastRecordId);
                                console.log('lastRecordCreatedDate::afetr:',lastRecordCreatedDate);
                            }
                            allExportRecords.push(releaseDate);
                            lastBatchIds.push(releaseDate.Id);
                        }
                        
                        if(allExportRecords.length || results.releaseDates.length){
 
                            //allExportRecords = allExportRecords.concat(results.releaseDates);
                            console.log('allExportRecords::afetr:',allExportRecords);
                            var tagArray = results.releaseDates.map(x => x.Id);
                            //lastBatchIds = tagArray;
                            console.log('numberOfLoop > index::afetr:',numberOfLoop , index);
                            console.log(':::lastBatchIds::::'+lastBatchIds);
                            if(numberOfLoop > index){
                                
                                index = index+1;
                                helper.recursiveReleaseDateExportAction(component, event, helper,allExportRecords,lastBatchIds,lastRecordCreatedDate,recordLimit,numberOfLoop,index,lastRecordId);
                                
                            }else{
                                
                                let data = allExportRecords;
                                console.log(data); 
                                console.log('data length:::',data.length); 
                                if(data == null) {
                                    return;
                                }
                                
                                let csv = helper.convertArrayToCSVForReleaseDate(component, data, helper);
                                var csvData = new Blob([csv], { type: 'text/plain' });
                                var csvUrl = URL.createObjectURL(csvData);
                                var link = document.createElement("a");
                                link.setAttribute('href', csvUrl);
                                link.setAttribute('download', 'ReleaseDateResults.csv');
                                link.click();
                                
                                helper.hideSpinner(component);
                                return allExportRecords;
                            }
                        }
                    } else if(allExportRecords.length) {
                        
                        let data = allExportRecords;
                        console.log(data); 
                        console.log('data length:::',data.length); 
                        if(data == null) {
                            return;
                        }
                        let csv = helper.convertArrayToCSVForReleaseDate(component, data, helper);
                        var csvData = new Blob([csv], { type: 'text/plain' });
                        var csvUrl = URL.createObjectURL(csvData);
                        var link = document.createElement("a");
                        link.setAttribute('href', csvUrl);
                        link.setAttribute('download', 'ReleaseDateResults.csv');
                        link.click();
                                
                    }
                } else {
                    
                }
                //helper.hideSpinner(component);
            } else if (state === 'ERROR') {
                console.log('Error::::csv:::::');
                helper.hideSpinner(component);
            }
        });
        $A.enqueueAction(action);
    },
     convertArrayToCSVForReleaseDate : function (component, data,helper) {

        // declare variables
        var csvStringResult, counter, header, keys, columnDivider, lineDivider;

        // check if 'objectRecords' parameter is null, then return from function
        if(data == null || !data.length) {
            return null;
        }

        // store ,[comma] in columnDivider variabel for sparate CSV values and
        // for start next line use '\n' [new line] in lineDivider varaible
        columnDivider = ',';
        lineDivider =  '\n';

        // in the keys variable store fields API Names as a key
        // this labels use in CSV file header
        var columns = component.get('v.resultColumns');
        header = [];
        keys =[];
        for(var i=0;i<columns.length;i++){
        	header.push(columns[i].label);// this array is the 'pretty' column names for the csv
        	keys.push(columns[i].fieldName);// this array is the actual column name
        }
        csvStringResult = component.get('v.searchCriteriaString') || '';
        csvStringResult += header.join(columnDivider);
        csvStringResult += lineDivider;
		
        for(let i=0; i < data.length; i++) {
            counter = 0;
            
            for(var sTempkey in keys) {
                
                var skey = keys[sTempkey];
                // add , [comma] after every String value (except first)
                if(counter > 0){
                    
                    csvStringResult += columnDivider;
                }
                if(columns[sTempkey].type == 'date' && !$A.util.isEmpty(data[i][skey])){
                    
                	var resultData = helper.getFormattedDate(data[i][skey]);
                } else {
                    //var resultData = data[i][skey] || '';
                    //LRCC-1014 - 'Fin'l Title Id' column data is displayed incorrectly
                    //LRCC-1675
                   
                    if(keys[sTempkey] == 'releaseDateTags' && data[i][skey] && data[i][skey].length){
                        
                        var tagArray = data[i][skey].map(x => x.Name);
                        var resultData = tagArray.join(';');
                    } else if(skey == 'Is_Confidential_Release_Date__c') { //LRCC-1828
                        var resultData = data[i][skey] ? 'Yes' : 'No';
                    } else {
                        var resultData = (skey=='edmTitleFinProdId')?(data[i][skey])?'=""'+data[i][skey]+'""':'': data[i][skey] || '';
                    }
                }
               	csvStringResult += '"'+ resultData +'"';
				counter++;
            }
            csvStringResult += lineDivider;
        }
		//return the CSV format String
		
        return csvStringResult;
    },
   
    
})