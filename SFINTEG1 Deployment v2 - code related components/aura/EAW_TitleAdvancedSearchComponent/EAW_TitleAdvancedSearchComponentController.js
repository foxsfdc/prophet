({
	doInit : function (component, event, helper) {
        component.set('v.spinner',true);
		component.set('v.titleColumns', [ 		// LRCC-1430 sortable : true is added to all fields
            {label: 'Fin\'l Title Id', fieldName: 'Title_Id', type: 'text', initialWidth: 150,sortable : true}, 
            {label: 'Title', fieldName: 'Name', type: 'url',
            typeAttributes: { label: {fieldName: 'Title_Name'}, target: '_blank'}, initialWidth: 200,sortable : true},
            {label: 'Intl Product Type', fieldName: 'PROD_TYP_CD_INTL', type: 'text', initialWidth: 150,sortable : true},
            {label: 'Product Type', fieldName: 'PROD_TYP_CD', type: 'text', initialWidth: 150,sortable : true},
            {label: 'Division', fieldName: 'FIN_DIV_CD', type: 'text', initialWidth: 150,sortable : true},
            {label: 'Title Tag(s)', fieldName: 'DIR_NM__c', type: 'text', initialWidth: 150,sortable : true},	// Title Tags field temporarily held in Director Name
            {label: 'Initial Release Date', fieldName: 'FRST_REL_DATE__c', type: 'Date', initialWidth: 150,sortable : true}
        ]);
        helper.resetAdvancedTitleSearchModal(component);
        component.set('v.spinner',false);
    },
    searchAdvancedTitles : function(component, event, helper){
    	
    	try {
            component.set('v.titleSearchResults', []); //LRCC-1430
            component.set('v.spinner',true);
            
             //LRCC-1430 
            var titleName = component.find('titleName') ? component.get('v.selectedTitlesTypeahead') : [];
            //var titleName = component.find('titleName') ? component.get('v.selectedTitles') : [];
            
            var selectedDivisions = component.find('division') ? component.get('v.selectedDivisions') : [];
            var finlTitleId = component.find('finlTitleId') ? component.find('finlTitleId').get('v.value') : '';
            
            //LRCC-1884
            var initialReleaseDateFrom = component.find('initialReleaseDateFrom') ? component.find('initialReleaseDateFrom').get('v.value') /*component.get('v.dateFrom')*/ : ''; 
            var initialReleaseDateTo = component.find('initialReleaseDateTo') ? component.find('initialReleaseDateTo').get('v.value') /*component.get('v.dateTo')*/ : ''; 
            
            var selectedIntlProductTypes = component.find('intlProductType') ? component.get('v.selectedIntlProductTypes') : [];
            var selectedProductTypes = component.find('productType') ? component.get('v.selectedProductTypes') : [];
            var usBoxOfficeFrom = component.find('usBoxOfficeFrom') ? component.find('usBoxOfficeFrom').get('v.value') : '';
            var usBoxOfficeTo = component.find('usBoxOfficeTo') ? component.find('usBoxOfficeTo').get('v.value') : '';
            var selectedTitleTags = component.find('titleTag') ? component.get('v.selectedTitleTags') : [];
            var genre = component.find('genre') ? component.get('v.selectedGenres') : [];
            var directorName = component.find('directorName') ? component.find('directorName').get('v.value') : '';
            var franchise = component.find('franchise') ? component.find('franchise').get('v.value') : '';
            var talent = component.find('talent') ? component.find('talent').get('v.value') : '';
            
            //LRCC-1884
            if(talent && 
               !titleName.length && !selectedDivisions.length && !finlTitleId 
                && !initialReleaseDateFrom && !initialReleaseDateTo && !selectedIntlProductTypes.length
                && !selectedProductTypes.length && !usBoxOfficeFrom && !usBoxOfficeTo
                && !selectedTitleTags.length && !genre.length && !directorName && !franchise) {
                
                helper.errorToast('You are searching only with Talent. Please Add some more criterias in Search Region');
                component.set('v.spinner',false);
            } else {
                
                var action = component.get("c.searchForAdvancedTitles");
                action.setParams({
                    'titleName': helper.collectNames(titleName),
                    'selectedDivisions': helper.collectNames(selectedDivisions),
                    'finlTitleId' : finlTitleId,
                    'initialReleaseDateFrom' : initialReleaseDateFrom.toString(),
                    'initialReleaseDateTo' : initialReleaseDateTo.toString(),
                    'selectedIntlProductTypes' : helper.collectNames(selectedIntlProductTypes),
                    'selectedProductTypes' : helper.collectNames(selectedProductTypes),
                    'usBoxOfficeFrom' : usBoxOfficeFrom,
                    'usBoxOfficeTo' : usBoxOfficeTo,
                    'genre' : helper.collectNames(genre),
                    'selectedTitleTags' : helper.collectNames(selectedTitleTags),
                    'directorName' : directorName,
                    'franchise' : franchise,
                    'talent' : talent
                });
                
                action.setCallback(this,function(response){
                    
                    var state = response.getState();
                    var results = response.getReturnValue();
                    
                    console.log("results: ", results);
                    
                    if(state === 'SUCCESS') {
                        //LRCC-1430 
                        if(results && results.length > 0) {
                            
                            if(results.length <= 5000) {
                                
                                let shortResults =[];
                                
                                for(let result of results.slice(0, 5000)) {
                                    //LRCC-1325 
                                    result.Title_Name = result.Name;
                                    result.Name = '/one/one.app?sObject/'+ result.Id + '/view#/sObject/' + result.Id + '/view';
                                    //LRCC-1098
                                    //1095 & 1250-Replace Title EDM with Title Attribute
                                    if(result.FIN_PROD_ID__c) {
                                        result.Title_Id = result.FIN_PROD_ID__c;
                                    }
                                    
                                    if(result.PROD_TYP_CD_INTL__r && result.PROD_TYP_CD_INTL__r.Name) {
                                        result.PROD_TYP_CD_INTL = result.PROD_TYP_CD_INTL__r.Name;
                                    }
                                    
                                    if(result.PROD_TYP_CD_INTL__r && result.PROD_TYP_CD_INTL__r.Name) {
                                        result.PROD_TYP_CD = result.PROD_TYP_CD_INTL__r.Name;
                                    }
                                    
                                    if(result.FIN_DIV_CD__r && result.FIN_DIV_CD__r.Name) {
                                        result.FIN_DIV_CD = result.FIN_DIV_CD__r.Name;
                                    }
                                    
                                    if(result.First_Release_Date__c) {
                                        result.FRST_REL_DATE__c = helper.getFormattedDate(result.First_Release_Date__c);//LRCC-1696
                                    }
                                    
                                    // if(result.Title_EDM__r && result.Title_EDM__r.DIR_NM__c) {
                                    //  result.DIR_NM__c = result.Title_EDM__r.DIR_NM__c;
                                    //  }
                                    shortResults.push(result);  
                                } 
                                component.set('v.titleSearchResults', shortResults);
                            } else {
                                helper.errorToast('Search results are more than 5000 records');
                            }
                            
                            //LRCC:1098
                            //component.set('v.titleSearchResults', shortResults);                       
                        } else {
                            helper.errorToast('We could not find any results from your search criteria.')
                        }
                    } else if (state === 'ERROR'){
                        console.log("error: "); 
                        console.log(response.getError());
                    }
                    component.set('v.spinner',false);
                });
                $A.enqueueAction(action);
            }
        } catch (e) {
            console.log(e);
        }
    },
    /** Methods below hide/show the popup*/
	showAdvancedTitleSearchModal : function(component, event, helper) {
        document.getElementById("EAW_TitleAdvancedSearchComponent").style.display = "block";
    },
    hideAdvancedTitleSearchModal : function(component, event, helper){
        document.getElementById("EAW_TitleAdvancedSearchComponent").style.display = "none";
    	helper.resetAdvancedTitleSearchModal(component);
        component.set('v.titleSearchResults', []);
	},
    clear: function(component, event, helper) {
    	helper.resetAdvancedTitleSearchModal(component);
    },
    addSelectedTitles : function(component, event, helper) {
		
        let selectedTitles = component.get("v.selectedTitles");
        
		let dataArr = component.get("v.data");
		let gridIds=[];

		if(selectedTitles.length > 0) {
	        for (let i = 0; i < dataArr.length; i++) {
	            gridIds.push(dataArr[i].Id);
	        }
            for (let i = 0; i < selectedTitles.length; i++) {
            	if(!gridIds.includes(selectedTitles[i].Id))dataArr.push(selectedTitles[i]);
            }
            component.set("v.data", dataArr);
            document.getElementById("EAW_TitleAdvancedSearchComponent").style.display = "none";

            helper.resetAdvancedTitleSearchModal(component);
            helper.successToast('Your selected title(s) has/have been added.');
            //LRCC-1430
            component.set('v.titleSearchResults', []);
        } else {
            helper.errorToast('Please select a title to be added.')
        }
	}
})