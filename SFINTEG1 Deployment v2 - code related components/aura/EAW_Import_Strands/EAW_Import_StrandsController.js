({
    importCategories : function(component, event, helper) {
        
        try {
            let apiRequest = component.get('v.apiRequest');
        	//let source = "810";
            //let idType = "2";
            //let idValue = "613193";  //"611723";- working
            
            var selectedIdType = component.find("import-strands-select-id-type").get("v.value");
    		var selectedEnvironment = component.find("import-strands-select-environment").get("v.value");
    		
    		apiRequest.idType = selectedIdType;
    		apiRequest.system = selectedEnvironment;
    		
    		helper.setInputId(component, event, helper);
            
			if(apiRequest && apiRequest.system && apiRequest.idType && (apiRequest.id && apiRequest.id != null)) {
                
                var action = component.get("c.sendIDValues");
                action.setParams({
                    "source": apiRequest.system,
                    "idType": apiRequest.idType,
                    "idValue": apiRequest.id
                });
                
                action.setCallback(this, function (response) {
                    var state = response.getState();
                    
                    if (state === 'SUCCESS') {
                        var categoryList = JSON.parse(response.getReturnValue());
                        component.set("v.categoryResponse",categoryList);
                    } 
                    else if (state === 'ERROR') {
                        let errors = response.getError();
                        let handleErrors = helper.handleErrors(errors, component, event, helper);
                        if(handleErrors) {
                            helper.errorToast(handleErrors);
                        }
                    }
                });
                $A.enqueueAction(action);
            } else {
                helper.errorToast('Please enter values to get catagories');
            }
        } catch (e) {
            console.log('Exception:' + e);
        }
    },
    
    importStrands : function(component, event, helper) {
    	helper.importStrands(component, event, helper);
    },
    
    updateSelectedEnvironment : function(component, event, helper) {
        var apiRequest = component.get("v.apiRequest");
        var selectedEnvironment = component.find("import-strands-select-environment").get("v.value");

        apiRequest.system = selectedEnvironment;
    },
    
    updateSelectedIdType : function(component, event, helper) {
        var apiRequest = component.get("v.apiRequest");
        var selectedIdType = component.find("import-strands-select-id-type").get("v.value");

        apiRequest.idType = selectedIdType;
    },
    
    updateInputtedId : function(component, event, helper) {
        helper.setInputId(component, event, helper);
    },
    
    updateSelectedCategory : function(component, event, helper) {
        var apiRequest = component.get("v.apiRequest");
        var selectedCategoryId = component.find("import-strands-select-category").get("v.value");
        apiRequest.categoryId = selectedCategoryId;
    },
    
    hideImportStrandsModal : function(component, event, helper) {
        helper.hideImportStrandsModal(component, event, helper);
    },
    
    showImportStrandsConfirmModal : function(component, event, helper) {
        let apiRequest = component.get('v.apiRequest');
		var option = apiRequest.option;
		if(option === 'append') {
			helper.importStrands(component, event, helper);
		} else {
			component.find("modalConfirmStrands").open();
		}
    },

	hideImportStrandsConfirmModal : function(component, event, helper) {
        component.find("modalConfirmStrands").close();		
    }
})