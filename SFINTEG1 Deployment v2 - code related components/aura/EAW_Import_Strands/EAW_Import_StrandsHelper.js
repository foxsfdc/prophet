({
    handleErrors: function(errors, component, event, helper) {
    	
    	let errorMessage = '';
        if (errors && Array.isArray(errors) && errors.length > 0) {
            errors.forEach(error => {
                if (error.message && errorMessage.includes(error.message) == false) {
	                errorMessage += error.message + "\n";
	            }
	            if (error.pageErrors && error.pageErrors.length > 0) {
	                error.pageErrors.forEach(pageError => {
	                    if (errorMessage.includes(error.message) == false)
	                        errorMessage += pageError.message + "\n";
	                });
	            } else if (error.fieldErrors) {
	                let fields = Object.keys(error.fieldErrors);
	                fields.forEach(fieldError => {
	                    if (Array.isArray(fieldError)) {
	                        fieldError.forEach(err => {
	                            if (errorMessage.includes(error.message) == false)
	                                errorMessage += err.message;
	                        });
	                    }
	                });
	            }
	        });
	    } else {
	        errorMessage = errors;
	    }
	    if (errorMessage.indexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION') !== -1) {
	        let errorMessageList = errorMessage.split('FIELD_CUSTOM_VALIDATION_EXCEPTION,');
	        if (errorMessageList.length > 1) {
	            let finalErroList = errorMessageList[1].split(': []');
	            if (finalErroList.length) {
	                errorMessage = finalErroList[0];
	            }
	        }
	    }
	    if (errorMessage.indexOf('REQUIRED_FIELD_MISSING') !== -1) {
	        
	        let errorMessageList = errorMessage.split('REQUIRED_FIELD_MISSING,');
	        if (errorMessageList.length > 1) {
	            errorMessage = errorMessageList[1];
	        }
	    }
	    return errorMessage;
	},
	
	errorToast : function(message) {
        
        var toastEvent = $A.get('e.force:showToast');
		toastEvent.setParams({
            'title': 'Error!',
            'type': 'error',
            'message': message
        });
		toastEvent.fire();
    },
    
    formatStrand : function(responseStrand, recordId) {
        return {
            EAW_Window_Guideline__c: recordId,
            License_Type__c: responseStrand.licenseType,
            Start_Date_Source__c: 'Start Date',
            Start_Date_Months_After__c: responseStrand.startDateMonths,
            Start_Date_Days_After__c: responseStrand.startDateDays,
            Start_Date_From__c: responseStrand.startDateFrom,
            End_Date_Source__c: 'End Date',
            End_Date_Months_After__c: responseStrand.endDateMonths,
            End_Date_Days_After__c: responseStrand.endDateDays,
            End_Date_From__c: responseStrand.endDateFrom,
            Media__c: responseStrand.medias,
            Territory__c: responseStrand.territories,
            Language__c: responseStrand.languages
        };
    },
    
    importStrands : function(component, event, helper) {
    	
        let apiRequest = component.get('v.apiRequest');
        let requestParam;
        let categoryList = component.get('v.categoryResponse');
        
        if(apiRequest.categoryId == null) {
        	var selectedCategoryId = component.find("import-strands-select-category").get("v.value");
        	apiRequest.categoryId = selectedCategoryId;
        }
        
        if((apiRequest.id && apiRequest.id != null) && (apiRequest.categoryId && apiRequest.categoryId != null) && apiRequest.option) {
            
            for(let category of categoryList) {
            	if(category.titleListCategoryId == apiRequest.categoryId) {
            		requestParam = JSON.stringify(category);
            	}
            } 
            
            var temp = requestParam;
            temp = temp.slice(0,-1);
            temp=temp+',"applicationId":'+apiRequest.system+'}';
        
            requestParam = temp;
            
            var windowGuidelineId = component.get("v.childAttribute");
            
            var action = component.get("c.sendCategories");
            action.setParams({
                "requestParamBody": requestParam,
                "actionType" : apiRequest.option,
                "windowGuidelineId" : windowGuidelineId
            });
			
            action.setCallback(this, function (response) {
                var state = response.getState();
                
                if (state === 'SUCCESS') {
                    $A.get('e.force:refreshView').fire();
                    component.set('v.showImportStrands',false);
                    component.set('v.apiRequest',{});
                    component.set('v.categoryResponse',{});
                    component.set('v.apiRequest.option','append');
                    this.successToast('Successfully created Window Guideline Strands!');
                } 
                else if(state == 'ERROR') {
                	let errors = response.getError();
                	let handleErrors = this.handleErrors(errors, component, event, helper);
                    if(handleErrors) {
                        this.errorToast(handleErrors);
                    }
                }
            });
            $A.enqueueAction(action);
            component.find("modalConfirmStrands").close();
            this.hideImportStrandsModal(component, event, helper); 
        } else {
            helper.errorToast('No ID or Category has been entered, please enter a valid ID or category to import strands.');
        }
    },
    
    setInputId : function(component, event, helper) {
        let apiRequest = component.get("v.apiRequest");
        let inputtedId = component.find("import-strands-input-id").get("v.value");

        if(inputtedId.length > 0) {
            let lastChar = inputtedId.slice(inputtedId.length - 1, inputtedId.length);

            if(!Number(lastChar) && lastChar !== '0') {
                component.find('import-strands-input-id').set('v.value', inputtedId.slice(0, inputtedId.length - 1))
                inputtedId = inputtedId.slice(0, inputtedId.length - 1);
            }
        }

        apiRequest.id = inputtedId;
    },
    
	hideImportStrandsModal : function(component, event, helper) {
        component.set("v.categoryResponse",{});
        component.set('v.apiRequest',{});
        component.set('v.apiRequest.option','append');
        document.getElementById("eaw-import-strands-modal").style.display = "none";
    },
    
    errorToast : function(message) {
    	var toastEvent = $A.get('e.force:showToast');
    	toastEvent.setParams({
			'title': 'Error!',
			'type': 'error',
			'message': message
		});
		toastEvent.fire();
    },
	
    successToast : function(message) {
    	var toastEvent = $A.get('e.force:showToast');
        toastEvent.setParams({
			'title': 'Success!',
			'type': 'success',
			'message': message
		});
		toastEvent.fire();
    },
    
    mockResponseCategories : function() {
        return [
            {
                categoryId: 123,
                name: "Library Pictures CIS"
            },
            {  
                categoryId: 456,
                name: "Library Pictures Russia"
            }
        ];
    },
    
    mockResponseStrands : function() {
        return {
            system: "Tuscany",
            idType: "titleListId",
            id: 12345,
            categoryId: 456,
            strands: [
                {
                    licenseType: "Carve-Out",
                    startDateMonths: 0,
                    startDateDays: 25,
                    startDateFrom: "Window Start Date",
                    endDateMonths: 2,
                    endDateDays: 13,
                    endDateFrom: "Window End Date",
                    medias: [
                        "Basic TV",
                        "Electronic Sell Through"
                    ],
                    territories: [
                        "Spain +"
                    ],
                    languages: [
                        "English"
                    ]
                },
                {
                    licenseType: "Exhibition License",
                    startDateMonths: 1,
                    startDateDays: 1,
                    startDateFrom: "Window Start Date",
                    endDateMonths: 1,
                    endDateDays: 1,
                    endDateFrom: "Window End Date",
                    medias: [
                        "Pay TV"
                    ],
                    territories: [
                        "Spain +"
                    ],
                    languages: [
                        "English"
                    ]
                }
            ]
        };
    }
})