({
	init: function (cmp, event, helper) {
		cmp.set('v.spinner', true);
        cmp.set('v.showModal', true);
        helper.setFieldValues(cmp, event, helper);
		cmp.set('v.spinner', false);
	},
	
    handleSubmit: function (component, event, helper) {
        component.set('v.spinner', true);
		event.preventDefault(); //stop form submission
		var eventFields = event.getParam("fields");
        component.find('recordForm').submit(eventFields);
		component.set('v.spinner', false);
	},
    
    handleErrors: function (cmp, event, helper) {
        var errors = event.getParams();
        
    },
    
	handleSuccess: function (cmp, event, helper) {
		
		var windowGuideline = event.getParams().response.id;
		cmp.set('v.spinner', true);
        
        //LRCC-1864
        var action = cmp.get("c.updateOriginWindowGuidelineId");
        action.setParams({
            'recordId': windowGuideline
        });
        action.setCallback(this, function (response) {
            
            let state = response.getState();
            console.log('state:::',state);
            if(state == 'SUCCESS') {
                var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId": windowGuideline
                });
                navEvt.fire();
            } 
            else if (state == 'ERROR') {
                let errors = response.getError();
                let handleErrors = helper.handleErrors(errors, cmp, event, helper);
                if(handleErrors) {
                    helper.toast('error',handleErrors);
                }
            }
        });
        $A.enqueueAction(action);	   
        
        cmp.set('v.spinner', false);
	},
	closeModal: function (cmp) {
		cmp.set('v.showModal', false);
        var base_url = window.location.origin;
        location.replace(base_url+"/lightning/o/EAW_Window_Guideline__c/list?filterName=Recent");
		dismissActionPanel.fire();
	}
})