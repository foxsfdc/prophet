({
	init : function (component, event, helper) {
   		let action = component.get("c.getWindowGuidelines");
        action.setParams({"recordId": component.get("v.recordId")});
        
        action.setCallback(this, function(response) {
        	let state = response.getState();
            
            if(state === 'SUCCESS') {
            	let results = response.getReturnValue();
            	
            	for(let result of results) {
            		
                    //LRCC-1493
                    if(result.Window_Guideline_Alias__c) {
                        result.Name = result.Window_Guideline_Alias__c;
                    }
            		result.link = '/one/one.app?sObject/'+ result.Id + '/view#/sObject/' + result.Id + '/view';
	            }
	            
            	component.set('v.windowGuidelineData', results);
            } else {
            	helper.errorToast('There was an issue loading the Window Guidelines, please refresh the page to load them.');
            }
        });
        $A.enqueueAction(action);
        //LRCC:1272
        component.set('v.myColumns', [
            {label: 'Window Guideline Alias', fieldName: 'link', type: 'url', // LRCC-1493
             		typeAttributes: { label: {fieldName: 'Name' }, target: '_blank'}, sortable: true},
            {label: 'Window Guideline Status', fieldName: 'Status__c', type: 'text'},
            /*{label: 'Media', fieldName: 'Media__c', type: 'text'},*/
            {label: 'Customer', fieldName: 'Customers__c', type: 'text'},
            {label: 'Product Type', fieldName: 'Product_Type__c', type: 'text'}, // LRCC-1493
            /*{label: 'Start Date', fieldName: 'Start_Date__c', type: 'date'},*/
            {label: 'Start Date Rule', fieldName: 'Start_Date_Rule__c', type: 'text'},
            /*{label: 'End Date', fieldName: 'End_Date__c', type: 'date'},*/
            {label: 'End Date Rule', fieldName: 'End_Date_Rule__c', type: 'text'},
            /*{label: 'Outside Date', fieldName: 'Outside_Date__c', type: 'date'},*/
            {label: 'Outside Date Rule', fieldName: 'Outside_Date_Rule__c', type: 'text'},
            /*{label: 'Tracking Date', fieldName: 'Tracking_Date__c', type: 'date'},*/
            {label: 'Tracking Date Rule', fieldName: 'Tracking_Date_Rule__c', type: 'text'}
        ]);
		
   		// Put this back in if we can think of a way to set planStatus every time 'status' is changed
        /*let statusAction = component.get('c.getPlanStatusFromRecordId');
        statusAction.setParams({'recordId': component.get('v.recordId')});

        statusAction.setCallback(this, function(response) {
            let state = response.getState();

            if(state === 'SUCCESS') {
                component.set('v.planStatus', response.getReturnValue());
            } else {
                helper.errorToast('There was an issue accessing the Plan Guideline status');
            }
        });
        $A.enqueueAction(statusAction);*/
    },
	
    createNewWindowGuidelineModal : function(component, event, helper) {

        let getStatusAction = component.get("c.getPlanStatusFromRecordId");
        getStatusAction.setParams({"recordId": component.get("v.recordId")});

        getStatusAction.setCallback(this, function(response) {
        	let state = response.getState();

            if(state === 'SUCCESS') {
                let planStatus = response.getReturnValue().status;
				var productType = response.getReturnValue().productType;
                if(productType && productType.length > 0){
                    var productTypeAttribute = [];
                    for(var i=0;i<productType.length;i++){
                        var attribute = {
                            'label' : productType[i],
                            'value' : productType[i]
                        };
                        productTypeAttribute.push(attribute);
                    }
                    component.set('v.productTypeAttribute',productTypeAttribute);
                }
                //Perform check for Plan's Status - only allow module to show if Draft  planStatus === 'Draft'
                if (planStatus === 'Draft') {
                    helper.openModal(component, event, helper);
                	/*let createRecordEvent = $A.get('e.force:createRecord');

	                createRecordEvent.setParams({
    	                'entityApiName': 'EAW_Window_Guideline__c',
        	            'panelOnDestroyCallback': function (event) {
            	            let planId = component.get("v.recordId");
                	        let href = window.location.href.split("/");
                    	    let windowGuidelineId = href[6];

                        	if (planId != windowGuidelineId) {
                            	let action = component.get('c.createPlanWindowGuidelineJunction');
                            	action.setParams({planId: planId, windowGuidelineId: windowGuidelineId});
                            	action.setCallback(this, function (response) {
                                	let state = response.getState();

	                                if (state === 'SUCCESS') {
    	                                let results = response.getReturnValue();
        	                            let windowGuidelines = component.get('v.windowGuidelineData');

        	                            if (results.length > 1) {
                                        	for (let result of results) {
                                            	if (result.Customer__r) {
                                                	result.customerName = result.Customer__r.Name;
                                            	}

                                            	result.link = '/one/one.app?sObject/' + result.Id + '/view#/sObject/' + result.Id + '/view';
                                            	windowGuidelines.push(result);
                                        	}
                                    	} else {
                                        	let result = results[0];

	                                        if (result.Customer__r) {
    	                                        result.customerName = result.Customer__r.Name;
        	                                }

            	                            result.link = '/one/one.app?sObject/' + result.Id + '/view#/sObject/' + result.Id + '/view';
                	                        windowGuidelines.push(result);
                    	                }

                        	            component.set('v.windowGuidelineData', windowGuidelines);

                            	    } else {
                                	    helper.errorToast('There was an issue creating a window guideline or the window guidelines association to the plan guideline.');
                                	}
                            	});
                            	$A.enqueueAction(action);
                        	}
                    	}
                	});
	                createRecordEvent.fire();*/
                } else {
                	helper.errorToast('Plan must be Draft to add Window Guidelines.');
                }

            } else {
            	helper.errorToast('There was an issue accessing the Plan Guideline.');
            }
        });
        $A.enqueueAction(getStatusAction);
	},

	showCreateNewWindowGuidelineModal : function(component, event, helper) {
        let action = component.get("c.getPlanStatusFromRecordId");
        action.setParams({"recordId": component.get("v.recordId")});

        action.setCallback(this, function(response) {
            let state = response.getState();

            if(state === 'SUCCESS') {
			
                let planStatus = response.getReturnValue().status;
			
                // Perform check for Plan's Status - only allow module to show if DRAFT
                if (planStatus === 'Draft') {
                    document.getElementById("eaw-create-plan-window-guideline-junction-modal").style.display = "block";
                } else {
                    helper.errorToast('Plan must be Draft to add Window Guidelines.');
                }
            } else {
                helper.errorToast('There was an issue accessing the Plan Guideline.');
            }
        });
        $A.enqueueAction(action);
	},
    showAddExistingWindowGuidelinesModal : function(component, event, helper) {
		
        let action = component.get("c.getPlanStatusFromRecordId");
        action.setParams({"recordId": component.get("v.recordId")});
		
        action.setCallback(this, function(response) {
            let state = response.getState();
			
            if(state === 'SUCCESS') {
				console.log('click',response.getReturnValue());
                let planStatus = response.getReturnValue().status;
                var productTypeAttribute = response.getReturnValue().productType;
                if(productTypeAttribute && productTypeAttribute.length > 0){
                    component.set('v.productTypeAttribute',productTypeAttribute);
                }
                // Perform check for Plan's Status - only allow module to show if DRAFT
                console.log('::',planStatus);
                if (planStatus === 'Draft') {
                    //$A.util.removeClass(component.find('existingModal'),'slds-hide');
                    component.find('existingModal').openModal();
                    //document.getElementById("eaw-add-plan-window-guideline-junction-modal").style.display = "block";
                } else {
                    helper.errorToast('Plan must be Draft to add Window Guidelines.');
                }
            } else {
                helper.errorToast('There was an issue accessing the Plan Guideline.');
            }
        });
        $A.enqueueAction(action);

    },

    showRemovePlanWindowGuidelinesModal : function(component, event, helper) {
		
        let action = component.get("c.getPlanStatusFromRecordId");
        action.setParams({"recordId": component.get("v.recordId")});
        
        action.setCallback(this, function(response) {
            let state = response.getState();
			if(state === 'SUCCESS') {
                
                let planStatus = response.getReturnValue().status;
                
                //Perform check for Plan's Status - only allow module to show if DRAFT
                //LRCC-1565 commented lines for LRCC-510 are removed
                let selectedRows = component.get('v.selectedRows');
                if(selectedRows.length > 0) {
					
					if(planStatus === 'Draft') {
                        document.getElementById("eaw-remove-plan-window-guideline-junction-modal").style.display = "block";
                    } else {
                        helper.errorToast('Plan must be Draft to remove Window Guidelines.');
                    }
                } else { 
                    helper.errorToast('There are no Window Guidelines selected for removal from this plan.');
                }
            } else {
                helper.errorToast('There was an issue accessing the Plan Guideline.');
            }
        });
        $A.enqueueAction(action);
	},

    cloneWindowGuidelines : function(component, event, helper) {

        let action = component.get("c.getPlanStatusFromRecordId");
        action.setParams({"recordId": component.get("v.recordId")});

        action.setCallback(this, function(response) {

            let state = response.getState();

            if(state === 'SUCCESS') {

                let planStatus = response.getReturnValue().status;

                /*if(planStatus === 'Draft') { LRCC-1612*/
                    let selectedRows = component.get('v.selectedRows');
                    if (selectedRows.length === 1) {
                        document.getElementById("eaw-clone-plan-window-guideline-modal").style.display = "block";
                    } else {
                        helper.errorToast('You can only clone one window guideline at a time.');
                    }
                /*} else { LRCC-1612
                    helper.errorToast('Plan must be Draft to clone Window Guidelines.');
                }*/

            } else {
                helper.errorToast('There was an issue accessing the Plan Guideline.');
            }
        });
        $A.enqueueAction(action);
    },
    addNewWindowGuideline : function(component,event,helper){
        helper.openModal(component,event,helper);
    },
    createJunction : function(component,event,helper){
        var windowGuidelineId = event.getParam("windowGuidelineId");
		var isSaveAndNew = event.getParam("isSaveAndNew");
        component.set('v.addWindow',false);
        helper.planAndWindowJunction(component,event,helper,component.get('v.recordId'),windowGuidelineId,isSaveAndNew);
    }
})