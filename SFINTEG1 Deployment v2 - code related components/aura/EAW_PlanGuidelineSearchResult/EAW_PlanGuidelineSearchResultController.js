({
    init: function (component, event, helper) {
    	
    	//LRCC-1633
		if(component.find('header-checkbox') && component.find('header-checkbox').getElement() && component.find('header-checkbox').getElement().checked == true) {
			component.find('header-checkbox').getElement().checked = false;
		}
		
    //LRCC-1459
        if(component.get('v.initialTableShow')){
            
            helper.generateAlphabets(component, event, helper);
            //LRCC-1795
            component.set("v.pagesPerRecod",component.find('pagePerRecord').get("v.value"));
            let pagesPerRecord = parseInt(component.find('pagePerRecord').get("v.value"));
            component.set("v.maxPage", Math.floor((component.get('v.totalRecordCount')+pagesPerRecord - 1 )/pagesPerRecord)); 
        }
    },
    exportToCsv : function(component,event,helper){
        //LRCC-1675
        console.log('exportToCsv::::');
        let exportSearchResultEvt = component.getEvent("EAW_ExportSearchResult");
        exportSearchResultEvt.setParams({"resultColumns" : component.get("v.myColumns"),
                                         "eventFlag":'planguideline'});
        exportSearchResultEvt.fire();
        
    	/*let data = component.get('v.originalPlanData');
        if(data == null) {
            return;
        }
        let csv = helper.convertArrayToCSV(component, data,helper);
		let hiddenElement = document.createElement('a');
        hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
        hiddenElement.target = '_self'; //
        hiddenElement.download = 'PlanGuidelines_Results.csv';  // CSV file Name* you can change it.[only name not .csv]
        document.body.appendChild(hiddenElement); // Required for FireFox browser
        hiddenElement.click();*/
    },
    clone : function(component, event, helper) {
        var cloneVal = component.get("v.cloneValue");
		helper.clearUnsavedChanges(component, event, helper);
        if(cloneVal === 'cloneWindowGuidelines') {
            helper.clone('c.clonePlanAndWindows', component);
        } else if (cloneVal === 'useSameWindowGuidelines') {
            helper.clone('c.clonePlanSameWindows', component);
        } else if (cloneVal === 'cloneWithoutWindowGuidelines') {
            helper.clone('c.clonePlanNoWindows', component);
        }

        helper.hideCloneModal();
    },

    deactivate : function(component, event, helper) {
        var planData = component.get("v.planData");
        var selectedRows = component.get("v.selectedRows");
        var action = component.get("c.deactivatePlans");
        action.setParams({"deactivatedPlans" : JSON.stringify(selectedRows)});
		
        action.setCallback(this, function(response) {
            var state = response.getState();
            var results = response.getReturnValue();

            if(state === 'SUCCESS') {
                for(var result of results) {
                    var index = planData.findIndex(item => item.Id == result.Id);
                    if(index != -1) {
                        planData[index] = result;
                    }
                    
                    result.link = '/one/one.app?sObject/'+ result.Id + '/view#/sObject/' + result.Id + '/view';
                }

                component.set("v.planData", planData);
                helper.successToast('The plan(s) has/have been deactivated.');
            } else {
                helper.errorToast('The plan(s) has/have not been deactivated.');
            }
        });
        $A.enqueueAction(action);

        helper.hideDeactivateModal();
    },

    deleteDraft : function(component, event, helper) {
        
        var planData = component.get("v.planData");
        var selectedRows = component.get("v.selectedRows");
        var action = component.get("c.deleteDraftPlans");
        action.setParams({"deletedDraftPlans" : JSON.stringify(selectedRows)});

        action.setCallback(this, function(response) {
            var state = response.getState();
            var results = response.getReturnValue();
            console.log('state::',state);
            console.log('results:::',results);
            
            if(state === 'SUCCESS') {
                
                if(results && results.length > 0) {
                    
                	for(var result of results) {
                        var index = planData.findIndex(item => item.Id == result.Id);
    
                        if(index != -1) {
                            planData.splice(index, 1);
                        }
                    }
                    component.set("v.planData", planData);
                    helper.handleCheckBoxValues(component); //LRCC:1199
                    helper.successToast('The plan(s) with a status of draft has/have been deleted.');  
                    //LRCC-1459
                    component.set('v.checked',false);
                  
                    var editTableEvent = $A.get("e.c:EAW_EditTableEvent");
                    editTableEvent.setParams({"editMode":false});
                    editTableEvent.fire();
                    
                    helper.reinitializePageMap(component,component.get("v.pageNumber"));
  					component.set('v.pageNumber', 1);
                    var pageConfigEvt = component.getEvent("pageConfigEvt");
                    pageConfigEvt.setParams({"pagesPerRecord" : component.get("v.pagesPerRecod"),
                                             "currentPageNumber":component.get("v.pageNumber"),
                                             "isDeleted":true});
                    pageConfigEvt.fire();
                      
                } else {
            	    helper.errorToast('Only Plan Guidelines with statuses of Draft will be deleted.');
            	}
            }
            else if (state === 'ERROR') {
                let errors = response.getError();
                let handleErrors = helper.handleErrors(errors, component, event, helper);
                if(handleErrors) {
                    helper.errorToast(handleErrors);
                }
            }
        });
        $A.enqueueAction(action);

        helper.hideDeleteModal();
    },

    hideDeactivateModal : function(component, event, helper) {
        helper.hideDeactivateModal();
    },

    hideCloneModal : function(component, event, helper) {
        helper.hideCloneModal();
    },

    hideDeleteModal : function(component, event, helper) {
        helper.hideDeleteModal();
    },

    showDeactivateModal : function(component, event, helper) {
        var selectedRows = component.get("v.selectedRows");

        if(selectedRows.length > 0) {
            helper.showDeactivateModal();
        } else {
            helper.errorToast('You have to select plan(s) to deactivate.');
        }
    },

    showCloneModal : function(component, event, helper) {
    	
        var selectedRows = component.get("v.selectedRows");
        console.log('selectedRows:::',selectedRows);
        if(selectedRows.length === 1) {
            helper.showCloneModal();
        } else {
            helper.errorToast('You can only clone one plan at a time.');
        }
    },
	
    showDeleteModal : function(component, event, helper) {
        var selectedRows = component.get("v.selectedRows");

        if(selectedRows.length > 0) {
            helper.showDeleteModal();
        } else {
            helper.errorToast('You have to select plan(s) to delete.');
        }
    },
    
    //Following methods are related to table setup to show the search results
    
    // We are setting the width which is just changed by the mouse move event     
    resetColumn: function(component, event, helper) {
        // Get the component which was used for the mouse move
        if( component.get("v.currentEle") !== null ) {
            var newWidth = component.get("v.newWidth"); 
            var currentEle = component.get("v.currentEle").parentNode.parentNode; // Get the DIV
            var parObj = currentEle.parentNode; // Get the TH Element
            parObj.style.width = newWidth+'px';
            currentEle.style.width = newWidth+'px';
            console.log(newWidth);
            component.get("v.currentEle").style.right = 0; // Reset the column divided 
            component.set("v.currentEle", null); // Reset null so mouse move doesn't react again
        }
	},
	
	checkAll : function (component, event, helper) {
        
        var self = event.target;
        var checkboxes = component.find("row-checkbox");
        
        if(!Array.isArray(checkboxes)){
            checkboxes = [checkboxes];
        }
        if(checkboxes) {
            for(let check of checkboxes) {
                check.getElement().checked = self.checked;
            }
            if(self.checked) {
                component.set("v.selectedRows",component.get("v.planData"));
            } else {
            	component.set("v.selectedRows",[]);
            }
        }
    },
    
    setNewWidth: function(component, event, helper) {
        var currentEle = component.get("v.currentEle");
        if( currentEle != null && currentEle.tagName ) {
            var parObj = currentEle;
            while(parObj.parentNode.tagName != 'TH') {
                if( parObj.className == 'slds-resizable__handle')
                    currentEle = parObj;    
                parObj = parObj.parentNode;
                count++;
            }
            var count = 1;
            var mouseStart = component.get("v.mouseStart");
            var oldWidth = parObj.offsetWidth;  // Get the width of DIV
            var newWidth = oldWidth + (event.clientX - parseFloat(mouseStart));
            component.set("v.newWidth", newWidth);
            if((oldWidth - newWidth) < 0){
            currentEle.style.right = ( oldWidth - newWidth ) +'px';
            component.set("v.currentEle", currentEle);
            if(currentEle.id){
                var myColumns = component.get('v.myColumns');
                for(var col of myColumns){
                     console.log(currentEle.id,':::current::',oldWidth - newWidth);
                    col.width = 100 + '%';
                }
                helper.setFixedHeader(component);
                component.set('v.myColumns',myColumns);
                //component.set('v.bodyWidth',((component.get('v.myColumns').length + 1) * 209 ) + newWidth );
            }
            }
        }
    },
    
    calculateWidth: function(component, event, helper) {
        
        var childObj = event.target
        var mouseStart=event.clientX;
        component.set("v.currentEle", childObj);
        component.set("v.mouseStart",mouseStart);
        // Stop text selection event so mouse move event works perfectlly.
        if(event.stopPropagation) event.stopPropagation();
        if(event.preventDefault) event.preventDefault();
        event.cancelBubble=true;
        event.returnValue=false;  
    },
	
	sort: function(component, event, helper) {
		//LRCC-1690

        console.log('::: Event Current Target:::'+event.currentTarget);
        var sortVal = event.currentTarget.dataset.value;
         //LRCC-1690
        component.set('v.selectedSortingParam',{'sortVal':sortVal});
        console.log('sortparam::',JSON.stringify(component.get('v.selectedSortingParam')));
        console.log('old value:'+sortVal);
        helper.sortBy(component,sortVal,false);
		//LRCC-1459
        component.set('v.colWithAlpha',{'selectedCol':null,'selectedAlpha':null});	
        var target = event.currentTarget;
        var rowIndex = target.getAttribute("data-colIndex");
        console.log("Row No : " + rowIndex);
        console.log('::: selected :::', JSON.stringify(component.get("v.myColumns")[rowIndex]));
        
        if((component.get("v.myColumns")[rowIndex].type == "string" || 
           component.get("v.myColumns")[rowIndex].type == "picklist") && 
           ((component.get("v.myColumns")[rowIndex].fieldName.indexOf("__c") > -1)||
           component.get("v.myColumns")[rowIndex].fieldName == "Name")){
            
            var colAplpha = component.get('v.colWithAlpha');
            colAplpha.selectedCol = component.get("v.myColumns")[rowIndex].fieldName;
            component.set('v.colWithAlpha',colAplpha)
        }else{
            helper.showErrorToast(component,'Selecting column is not valid for alphabet sort');
            component.set('v.colWithAlpha',{'selectedCol':null,'selectedAlpha':null});	
            return;
        }
        console.log('::: selected :::',JSON.stringify(component.get('v.colWithAlpha')));
        console.log('::: selected :myColumns::',JSON.stringify( component.get("v.myColumns")));
        
        //end-LRCC-1459
		
    },
    
    getSelectedIndex : function(component,event,helper){
    	console.log(event.currentTarget.value);
    	var selectedValues = component.get('v.selectedRows');
    	if(event.currentTarget.checked){
    		selectedValues.push(component.get('v.planData')[event.currentTarget.value]);
    	} else{
		     selectedValues.splice(selectedValues.indexOf(component.get('v.planData')[event.currentTarget.value]),1); 	
    	}
    	console.log('selectedValues:::',selectedValues);
    	component.set('v.selectedRows',selectedValues);
        
        //LRCC-1633
        let checkboxes = component.find('header-checkbox');
        let check = Array.isArray(checkboxes) ? checkboxes[0] : checkboxes;
        
        if(component.get('v.planData').length == component.get('v.selectedRows').length) {            
            check.getElement().checked = true;
        } else {
            check.getElement().checked = false;
        }
    },
    
    editGrid: function(component, event, helper) {         
        helper.handleCheckBoxValues(component); //LRCC:1199
    	var originalPlanData = component.get("v.originalPlanData");
        if(component.get('v.checked')) {
        	component.set("v.originalPlanData", JSON.parse(JSON.stringify(originalPlanData)));
        }
        var checked = component.get("v.checked");
        var editTableEvent = $A.get("e.c:EAW_EditTableEvent");
        editTableEvent.setParams({"editMode":checked});
        editTableEvent.fire();
        //LRCC-1295
        if(checked == false && component.get("v.checkSaveRecords") == false) {
            	
            component.find("modalCmp").open();
        } 
        helper.makeUnsavedChanges(component, event, helper);
    },
    closeModal: function(component, event, helper) {
    	component.find("modalCmp").close();
        component.set('v.checked', true);
        component.set('v.displayEditButtons', true);
        var editTableEvent = $A.get("e.c:EAW_EditTableEvent");
        editTableEvent.setParams({"editMode":true});
        editTableEvent.fire();
       
    },
    saveModal : function(component, event, helper) {
        component.find("modalCmp").close();
        component.set('v.checked', false);        
        component.set("v.planData", JSON.parse(JSON.stringify(component.get("v.originalPlanData"))));  
        helper.clearUnsavedChanges(component, event, helper);
	},
    
    savePlanRecords: function(component, event, helper){
    
    	event.stopPropagation();
    	
    	var myColumns = component.get("v.myColumns");
        var myData = component.get("v.planData");

        var myDataOriginal = component.get("v.originalPlanData");

        var updatedData = new Array();
        
        for(var i=0 ; i<myData.length ; i++){
            for(let column of myColumns){
                var data = myData[i];
                var dataOriginal = myDataOriginal[i];
                if(data[column.fieldName] != dataOriginal[column.fieldName]){
                    updatedData.push(data);
                    break;
                }
            }
        }
    	
    	console.log('Records going for update : ' + JSON.stringify(updatedData));
        
        if( updatedData && updatedData.length > 0 ) {
        	
        	var action = component.get("c.saveRecords");
	        action.setParams({"plans" : JSON.stringify(updatedData)});
	        
	        action.setCallback(this, function(response) {
	            var state = response.getState();
	            var results = response.getReturnValue();
	
	            if(state === 'SUCCESS') {
	            	component.set("v.originalPlanData", JSON.parse(JSON.stringify(component.get("v.planData"))));
	                helper.successToast('The plan(s) has been updated successfully.');
	                
                    //component.set('v.checked',false); //LRCC-1668
                    helper.handleCheckBoxValues(component); //LRCC:1199
	                
	                /*
	                 * LRCC-1668
	                var editTableEvent = $A.get("e.c:EAW_EditTableEvent");
	                editTableEvent.setParams({"editMode":false});
	                editTableEvent.fire();
                    */
	                component.set('v.checkSaveRecords',true);
                    helper.clearUnsavedChanges(component, event, helper);    
	            } else {
	                //helper.errorToast('Error has occurred. Please contact administrator');
	            	let errors = response.getError();
                	let handleErrors = helper.handleErrors(errors, component, event, helper);
                    if(handleErrors) {
                        helper.errorToast(handleErrors);
                    }
                }
			}); 
	        $A.enqueueAction(action);
        } else {
        	//LRCC-892
        	var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title: "Info!",
                message: $A.get("$Label.c.EAW_RecordsNotEdited"),
                type: "info"
            });
            toastEvent.fire();
        }
    },
    cancel: function(component, event, helper) {
        component.set("v.planData", JSON.parse(JSON.stringify(component.get("v.originalPlanData"))));
        component.set("v.checked",false);        
        helper.handleCheckBoxValues(component); //LRCC:1199
        var checked = component.get("v.checked");
        var editTableEvent = $A.get("e.c:EAW_EditTableEvent");
        editTableEvent.setParams({"editMode":checked});
        editTableEvent.fire(); 
        helper.clearUnsavedChanges(component, event, helper);
    },
    setBodyWidth : function(component){
        
    },
     //LRCC-1459
    renderPage : function(component, event, helper) {
        
        console.log('click::::',component.get("v.pagesPerRecods"),':::>::',component.get("v.pageNumber"),'<<>>',component.get('v.colWithAlpha'))
        let isAlphaFiltered = component.get("v.isAlphaFiltered");
        
        let pageConfigEvt = component.getEvent("pageConfigEvt");
  		
        pageConfigEvt.setParams({"pagesPerRecord" : component.get("v.pagesPerRecod"),
                                 "currentPageNumber":component.get("v.pageNumber"),
                                 "selectedFilterMap" : isAlphaFiltered ? component.get('v.colWithAlpha') : null
                                 });
        pageConfigEvt.fire();
		
	},
	handleChange: function(component, event, helper) {
        
         if(component.get("v.isEditable")){
            helper.showErrorToast(component);
            component.find('pagePerRecord').set("v.value",component.get("v.oldPagesPerRecord"));
            return;
        } 
         
        component.set('v.pageNumber', 1);
        let pagesPerRecord = parseInt(component.find('pagePerRecord').get("v.value"));
         
        component.set("v.pagesPerRecod",component.find('pagePerRecord').get("v.value"));
        component.set("v.oldPagesPerRecord",component.find('pagePerRecord').get("v.value"));
        component.set("v.maxPage", Math.floor((component.get('v.totalRecordCount')+pagesPerRecord - 1 )/pagesPerRecord)); 
         
         var pageConfigEvt = component.getEvent("pageConfigEvt");
            pageConfigEvt.setParams({"pagesPerRecord" : component.get("v.pagesPerRecod"),
                                     "currentPageNumber":component.get("v.pageNumber")});
            pageConfigEvt.fire();
    },    
    chooseAlphabet : function(component, event, helper) {
        
       if(component.get("v.isEditable")){
            helper.showErrorToast(component);
            return;
        } 
        
        var target = event.currentTarget;
        var rowIndex = target.getAttribute("data-rowIndex");
       
        if(component.get("v.alphabets")[rowIndex]){
            
            var colAplpha = component.get('v.colWithAlpha');
            colAplpha.selectedAlpha = component.get("v.alphabets")[rowIndex];
            component.set('v.colWithAlpha',colAplpha);
        }
        let colWithAlpha = component.get('v.colWithAlpha');
        if( colWithAlpha.selectedCol != null ||   colWithAlpha.selectedAlpha == 'All' ){
            
            if(colWithAlpha.selectedAlpha == 'All'){
                
                colWithAlpha.selectedCol == null;
                component.set('v.colWithAlpha',colWithAlpha);
                component.set("v.isAlphaFiltered",false);
            }
            component.set("v.isAlphaFiltered",true);
            component.set('v.pageNumber', 1);
            component.set("v.pageIdMap",{});
            var pageConfigEvt = component.getEvent("pageConfigEvt");
            
            pageConfigEvt.setParams({"pagesPerRecord" : component.get("v.pagesPerRecod"),
                                     "currentPageNumber":1,
                                     "selectedFilterMap" : component.get('v.colWithAlpha') });
            pageConfigEvt.fire();

        }
        if(colWithAlpha.selectedAlpha == 'All'){
            
            component.set('v.colWithAlpha',{'selectedCol':null,'selectedAlpha':null});	
        }
        
         console.log('::: selected :::', component.get('v.colWithAlpha'));
    },
})