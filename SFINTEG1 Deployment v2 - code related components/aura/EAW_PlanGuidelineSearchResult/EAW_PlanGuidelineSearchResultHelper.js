({
    handleErrors: function(errors, component, event, helper) {
    	
    	console.log(':::::: errors :::::::'+errors);
	    let errorMessage = '';
        
        if (errors && Array.isArray(errors) && errors.length > 0) {
            
	        errors.forEach(error => {
                
	            if (error.message && errorMessage.includes(error.message) == false) {
	                errorMessage += error.message + "\n";
	            }
	            if (error.pageErrors && error.pageErrors.length > 0) {
	                error.pageErrors.forEach(pageError => {
	                    if (errorMessage.includes(error.message) == false)
	                        errorMessage += pageError.message + "\n";
	                });
	            } else if (error.fieldErrors) {
	                let fields = Object.keys(error.fieldErrors);
	                fields.forEach(fieldError => {
	                    if (Array.isArray(fieldError)) {
	                        fieldError.forEach(err => {
	                            if (errorMessage.includes(error.message) == false)
	                                errorMessage += err.message;
	                        });
	                    }
	                });
	            }
	        });
	    } else {
	        errorMessage = errors;
	    }
	    if (errorMessage.indexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION') !== -1) {
	        
	        let errorMessageList = errorMessage.split('FIELD_CUSTOM_VALIDATION_EXCEPTION,');
	        if (errorMessageList.length > 1) {
	            let finalErroList = errorMessageList[1].split(': []');
	            if (finalErroList.length) {
	                errorMessage = finalErroList[0];
	            }
	        }
	    }
	    if (errorMessage.indexOf('REQUIRED_FIELD_MISSING') !== -1) {
	        
	        let errorMessageList = errorMessage.split('REQUIRED_FIELD_MISSING,');
	        if (errorMessageList.length > 1) {
	            errorMessage = errorMessageList[1];
	        }
	    }
	    console.log('***** errorMessage-->', errorMessage);
	    return errorMessage;
	},
    clone : function(type, component) {
        var planData = component.get("v.planData");
        var selectedRows = component.get("v.selectedRows");
        var action = component.get(type);
        action.setParams({'clonePlan': JSON.stringify(selectedRows)});
		
        console.log('selectedRows:::::',selectedRows);
        action.setCallback(this, function(response) {
            var state = response.getState();
            var results = response.getReturnValue();
			
            if(state === 'SUCCESS') {
                var result = results[0];
                var navigateEvent = $A.get('e.force:navigateToSObject');

                navigateEvent.setParams({
                    'recordId': result.Id
                });
                
                navigateEvent.fire();
            } else if (state === 'ERROR') {
                let errors = response.getError();
                let handleErrors = helper.handleErrors(errors, component, event, helper);
                if(handleErrors) {
                    helper.errorToast(handleErrors);
                }
            } else {
                this.errorToast('The plan has not been cloned.');
            }
        });
        $A.enqueueAction(action);
    },
	convertArrayToCSV : function (component, data,helper) {

        // declare variables
        var csvStringResult, counter, header, keys, columnDivider, lineDivider;

        // check if 'objectRecords' parameter is null, then return from function
        if(data == null || !data.length) {
            return null;
        }

        // store ,[comma] in columnDivider variabel for sparate CSV values and
        // for start next line use '\n' [new line] in lineDivider varaible
        columnDivider = ',';
        lineDivider =  '\n';

        // in the keys variable store fields API Names as a key
        // this labels use in CSV file header
        var columns = component.get('v.myColumns');
        header = [];
        keys =[];
        for(var i=0;i<columns.length;i++){
        	header.push(columns[i].label);// this array is the 'pretty' column names for the csv
        	keys.push(columns[i].fieldName);// this array is the actual column name
        }
        
        csvStringResult = component.get('v.searchCriteriaString') || '';
        csvStringResult += header.join(columnDivider);
        csvStringResult += lineDivider;

        for(let i=0; i < data.length; i++) {
            counter = 0;

            for(var sTempkey in keys) {
            
                var skey = keys[sTempkey];

                // add , [comma] after every String value (except first)
                if(counter > 0){
                    csvStringResult += columnDivider;
                }
                if(columns[sTempkey].type == 'date' && !$A.util.isEmpty(data[i][skey])){
                	
            		var resultData = helper.getFormattedDate(data[i][skey]);
            	} else{
           		 	var resultData = data[i][skey] || '';
            	}
               
                csvStringResult += '"'+ resultData +'"';

                counter++;
            }
            csvStringResult += lineDivider;
        }

        // return the CSV format String
        return csvStringResult;
    },
    getFormattedDate : function(dateToFormat) {
    	var mydate = new Date(dateToFormat);
		var month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
		"Jul", "Aug", "Sept", "Oct", "Nov", "Dec"][mydate.getMonth()];
		return ' '+mydate.getDate() + ' - ' + month + ' - ' +  mydate.getFullYear()+' ';
    },
    errorToast : function(message) {
        var toastEvent = $A.get('e.force:showToast');

        toastEvent.setParams({
            'title': 'Error!',
            'type': 'error',
            'message': message
        });

        toastEvent.fire();
    },

    successToast : function(message) {
        var toastEvent = $A.get('e.force:showToast');

        toastEvent.setParams({
            'title': 'Success!',
            'type': 'success',
            'message': message
        });

        toastEvent.fire();
    },

    hideDeactivateModal : function() {
        document.getElementById("eaw-deactivate-popup").style.display = "none";
    },

    hideCloneModal : function() {
        document.getElementById("eaw-clone-popup").style.display = "none";
    },

    hideDeleteModal : function() {
        document.getElementById("eaw-delete-popup").style.display = "none";
    },

    showDeactivateModal : function() {
        document.getElementById("eaw-deactivate-popup").style.display = "block";
    },

    showCloneModal : function() {
        document.getElementById("eaw-clone-popup").style.display = "block";
    },

    showDeleteModal : function() {
        document.getElementById("eaw-delete-popup").style.display = "block";
    },
    
    sortBy:  function (component, field,nextFlag) {
        //LRCC-1690
        var sortAsc = component.get("v.sortAsc"),
            sortField = component.get("v.sortField"),
            
            records = component.get("v.rowsToDisplay");
        if(!nextFlag){
            
             sortAsc = field == sortField ? !sortAsc : true;
        }
        /*records.sort(function (a, b) {
            var t1 = a[field] == b[field],
                t2 = a[field] > b[field];
            return t1 ? 0 : (sortAsc ? -1 : 1) * (t2 ? -1 : 1);
        });*/
         records.sort(function(a, b) {
            if(sortAsc) {
                if(a[field] == undefined) {
                    return -1;
                } else if(b[field] == undefined) {
                    return 1;
                }
                
                return (new String(a[field]).toUpperCase() > new String(b[field]).toUpperCase()) ? 1 : ((new String(b[field]).toUpperCase() > new String(a[field]).toUpperCase()) ? -1 : 0);
            } else {
                if(a[field] == undefined) {
                    
                    return 1;
                } else if(b[field] == undefined) {
                    return -1;
                }
                
                return (new String(a[field]).toUpperCase() < new String(b[field]).toUpperCase()) ? 1 : ((new String(b[field]).toUpperCase() < new String(a[field]).toUpperCase()) ? -1 : 0);
            }
        });
        
        component.set("v.sortAsc", sortAsc);
        component.set("v.sortField", field);
        component.set("v.planData", records);
        component.set("v.rowsToDisplay", records);
        
        /****LRCC:887-Sorting indicator applied logic****/
        component.set("v.selectedTabsoft", field);
        if (sortAsc == true) {
            component.set("v.arrowDirection", 'arrowup');
        } else {
            component.set("v.arrowDirection", 'arrowdown');
        }
    },
         
     /****LRCC:1199-Edit toggle Action Logic****/
	handleCheckBoxValues: function(component) {
	
        if(component.get('v.checked') == false) {
    
            component.set("v.selectedRows",[]);
            component.set('v.selectedValues',[]);
    
            component.find("header-checkbox").getElement().checked = false;
            var checkboxes = component.find("row-checkbox");
            console.log('checkboxes:::',checkboxes);
            
            if(!Array.isArray(checkboxes)){
                checkboxes = [checkboxes];
            }
            if(checkboxes) {
                for(let check of checkboxes) {
                    if(check.getElement() != null)
                    check.getElement().checked = false;
                }
            }
        }
    },
    makeUnsavedChanges: function(cmp, evt, helper) {
        var unsaved = cmp.find("unsaved");
        unsaved.setUnsavedChanges(true, { label: 'Plan Guideline Results' });
    },
    clearUnsavedChanges: function(cmp, evt, helper) {
        var unsaved = cmp.find("unsaved");
        unsaved.setUnsavedChanges(false);
    },
        setFixedHeader : function(component){
            if(component.get('v.planData') && component.get('v.planData').length){  // LRCC - 1282
                const globalId = component.getGlobalId(),
                    parentnode = document.getElementById(globalId),
                    thElements = parentnode.getElementsByTagName('th'),
                    tdElements = parentnode.getElementsByTagName('td');
                console.log('::::::::::',thElements);
                var bodyWidth = 0;
                
                for (let i = 0; i < thElements.length; i++) {
                    console.log(thElements[i].offsetWidth+':::::::::::::::'+tdElements[i]);
                    if(tdElements[i] && thElements[i])tdElements[i].style.width = thElements[i].offsetWidth + 'px';
                    bodyWidth += thElements[i].offsetWidth;
                }
              //  console.log(':::::::bodyWidth:::::::::::'+bodyWidth);
                component.set('v.bodyWidth',bodyWidth + 20);
            }
        },
 //LRCC-1459
    generateAlphabets : function(component, event, helper) {
        
        var alphabets = [];
        for(var i= 65 ; i < 91 ; i++){
            
            alphabets.push(String.fromCharCode(i));
        }
        alphabets.push('All');
        component.set('v.alphabets',alphabets);
          //LRCC-1459
		 helper.renderPage(component, event, helper)
        
    },
    //LRCC-1459
    formPageIdMap : function(component, event, helper) {
      console.log('formPageIdMap:::::');
        const pageNumber = component.get("v.pageNumber"),
            rowsToDisplay = component.get("v.rowsToDisplay");
        
        let pageIdMap = component.get("v.pageIdMap") || {};
        
        const recordIds = rowsToDisplay.map((rec) =>  rec.Id);
        
        pageIdMap[pageNumber] = recordIds;
        
        component.set("v.pageIdMap",pageIdMap);
        console.log('pageIdMap:::::test:::::::',JSON.stringify(pageIdMap));
        var selctsort = component.get('v.selectedSortingParam');
        console.log('tosort::',selctsort);
        if(selctsort){
            this.sortBy(component, selctsort.sortVal,true);  
        }
       
    },
    //LRCC-1459
    reinitializePageMap : function(component,pageNumber) {
    
        let pageIdMap = component.get("v.pageIdMap");
        
        for(let i = pageNumber ; i <= Object.keys(pageIdMap).length ; i++ ){
            pageIdMap[i] = [];
        } 
        component.set("v.pageIdMap",pageIdMap);
    },
      //LRCC-1459
     renderPage: function(component, event, helper) {
         console.log('call render page:::1::',component.get("v.planData"));
         console.log('call render page:::2::',component.get("v.pageNumber"));
         console.log('call render page:::3::',component.find('pagePerRecord'));
         console.log('call render page:::::',component.find('pagePerRecord').get("v.value"));
         let records = component.get("v.planData") || [];
        //var pageNumber = component.get("v.pageNumber");
        //var pagesPerRecord = parseInt(component.find('pagePerRecord').get("v.value"));
       ///var  rowsToDisplay = records;
      
        component.set("v.rowsToDisplay", records);
        console.log('rowsToDisplay:::::',records);
        helper.formPageIdMap(component);
        
    },
        //LRCC-1459
        showErrorToast : function(component,msg) {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title: "Info!",
                message:msg ,
                type: "info"
            });
            toastEvent.fire();
        },
    
})