({
	doInit : function(component, event, helper) {
    /*LRCC-1041*/
    component.set('v.isEditable', false);
        helper.getPromotionAuditHistoryData(component, event, helper);
    },

    resetColumn: function(component, event, helper) {
        let currentEle = component.get('v.currentEle');

        if(component.get("v.currentEle") !== null) {
            let newWidth = component.get("v.newWidth");
            let currentEleParent = currentEle.parentNode.parentNode;
            let parObj = currentEleParent.parentNode;

            parObj.style.width = newWidth + 'px';
            currentEleParent.style.width = newWidth + 'px';

            component.get("v.currentEle").style.right = 0;
            component.set("v.currentEle", null);
        }
    },

    calculateWidth: function(component, event, helper) {
        let childObj = event.target;
        let mouseStart = event.clientX;

        component.set("v.currentEle", childObj);
        component.set("v.mouseStart", mouseStart);

        if (event.stopPropagation) {
            event.stopPropagation();
        }

        if (event.preventDefault) {
            event.preventDefault();
        }

        event.cancelBubble = true;
        event.returnValue = false;
    },

    setNewWidth: function(component, event, helper) {
        let currentEle = component.get("v.currentEle");

        if (currentEle != null && currentEle.tagName) {
            let parObj = currentEle;

            while (parObj.parentNode.tagName != 'TH') {
                if (parObj.className == 'slds-resizable__handle') {
                    currentEle = parObj;
                }

                parObj = parObj.parentNode;
            }

            let mouseStart = component.get("v.mouseStart");
            let oldWidth = parObj.offsetWidth;
            let newWidth = oldWidth + (event.clientX - parseFloat(mouseStart));

            component.set("v.newWidth", newWidth);
            currentEle.style.right = (oldWidth - newWidth) + 'px';
            component.set("v.currentEle", currentEle);
        }
    },

    sort: function(component, event, helper) {
        let sortVal = event.currentTarget.dataset.value;

        helper.sortBy(component, sortVal);
    }
})