({
    /*LRCC-1041*/
    getPromotionAuditHistoryData: function (component, event, helper) {

        let action = component.get("c.getAuditHistory");
        action.setParams({"releaseDateId": component.get("v.recordId")});
        
        action.setCallback(this, function(response) {
            
            let state = response.getState();
			if (state === 'SUCCESS') {
                
                let results = response.getReturnValue();
                for (let result of results) {
                    result.link = '/lightning/r/sObject/'+ result.Id + '/view';
                }
                
                component.set('v.resultData', results);
            } else {
                helper.errorToast('There was an issue loading the Promotion Audit History, Please refresh the page to load them.');
            }
        });
        $A.enqueueAction(action);
    }
})