({
	searchData: function(component,event,helper){
        helper.search(component);
    },
    clearSearch: function(component,event,helper){
        helper.clearSearch(component);
    }
})