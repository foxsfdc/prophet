({
    init : function(cmp, event, helper) {
        console.log('record::',cmp.get('v.dateCalc'));
        if(cmp.get('v.operandDateAndStatus')){
        	helper.setModifiedOperandDateAndStatus(cmp,event,helper,cmp.get('v.operandDateAndStatus'));
        }
        cmp.set('v.validreleaseWindowDateWrapper',JSON.parse(JSON.stringify(cmp.get('v.releaseWindowDateWrapper'))));
        var validreleaseWindowDateWrapper = cmp.get('v.validreleaseWindowDateWrapper');
        if((cmp.get('v.ruleDate') == 'Start Date' && !cmp.get('v.previousVersion'))|| (!cmp.get('v.validForAllDates') && !cmp.get('v.previousVersion') && (cmp.get('v.ruleDate') == 'Outside Date' || cmp.get('v.ruleDate') == 'Tracking Date'))){      // LRCC - 1276
            for(var i in validreleaseWindowDateWrapper){
                if(validreleaseWindowDateWrapper[i].value == cmp.get('v.recordId')){
                    validreleaseWindowDateWrapper.splice(i,1);
                    break;
                }
            }
        }
        if(cmp.get('v.ruleDate') == 'End Date'){
            validreleaseWindowDateWrapper.push({'label' : 'Rights End Date','value' : 'Rights End Date'});
        }
        if(validreleaseWindowDateWrapper)validreleaseWindowDateWrapper.splice(0, 0, {'label': '--None--','value':''});//LRCC - 1333
        //console.log(':::',validreleaseWindowDateWrapper);
        cmp.set('v.validreleaseWindowDateWrapper',validreleaseWindowDateWrapper);
        
        if(cmp.get('v.dateCalc') && cmp.get('v.dateCalc').Conditional_Operand_Id__c){
            var operandId = cmp.get('v.dateCalc').Conditional_Operand_Id__c;
            cmp.set('v.mediaAndTerritoryValue',operandId);
            if((operandId == cmp.get('v.recordId')) || (cmp.get('v.previousVersion') && operandId == cmp.get('v.previousVersion'))){ // LRCC - 1276
                helper.setWindowDatesByRulesDate(cmp,event,helper,operandId);
            }
        } 
        if(cmp.get('v.windowGuidelineList') && !cmp.get('v.isReleaseDate')){
            // For Window Guideline Rule Builder
            //operator
            if(cmp.get('v.windowGuidelineList').find(function(element){
                return element.value == operandId})){
                cmp.set('v.isWindowDate',true);
            } else{
                cmp.set('v.isWindowDate',false);
            }
        }
        if(cmp.get('v.dateCalc') && !helper.isActiveConditionId(cmp,cmp.get('v.dateCalc').Conditional_Operand_Id__c)){
            cmp.get('v.dateCalc').Conditional_Operand_Id__c = '';
        }
        if(!cmp.get('v.dateCalc').Conditional_Operand_Id__c && cmp.get('v.ruleDate') == 'End Date' && cmp.get('v.dateCalc').Condition_Operand_Details__c && cmp.get('v.dateCalc').Condition_Operand_Details__c == 'Rights End Date'){ // LRCC - 1752
            cmp.set('v.mediaAndTerritoryValue','Rights End Date');
            cmp.set('v.isWindowDate',false);
        }
        if( !cmp.get('v.dateCalc').Conditional_Operand_Id__c && cmp.get('v.ruleDate') == 'End Date' && cmp.get('v.validForAllDates')  && cmp.get('v.dateCalc').Condition_Operand_Details__c != 'Rights End Date'){
            cmp.set('v.mediaAndTerritoryValue',cmp.get('v.recordId'));
            cmp.set('v.isWindowDate',true);
        }
        /*LRCC-1359*/
        cmp.set('v.dayAlignment',cmp.get('v.dateCalc').Condition_Date_Duration__c);
    },
    setMediaAndTerritoryValue : function(cmp,event,helper){
        var mediaAndTerritoryValue = cmp.get('v.mediaAndTerritoryValue');
        var selectedLabel;
        if(mediaAndTerritoryValue){
            console.log(cmp.get('v.recordId')+':::::mediaAndTerritoryValue::',mediaAndTerritoryValue);
            if((mediaAndTerritoryValue == cmp.get('v.recordId')) || (cmp.get('v.previousVersion') && mediaAndTerritoryValue == cmp.get('v.previousVersion'))){
                helper.setWindowDatesByRulesDate(cmp,event,helper,mediaAndTerritoryValue);
            } else{
                if(cmp.get('v.ruleDate') == 'Start Date' || cmp.get('v.ruleDate') == 'End Date'){
                    cmp.set('v.windowDates',[{'label': 'Start Date', 'value': 'Start Date'},
                                             {'label': 'End Date', 'value': 'End Date'}]);
                } else{
                    cmp.set('v.windowDates',[{'label': 'Start Date', 'value': 'Start Date'},
                                             {'label': 'End Date', 'value': 'End Date'},
                                             {'label': 'Outside Date', 'value': 'Outside Date'},
                                             {'label': 'Tracking Date', 'value': 'Tracking Date'}]);
                }
            }
            if(cmp.get('v.releaseWindowDateWrapper') && cmp.get('v.isReleaseDate') && mediaAndTerritoryValue != 'Rights End Date'){
                selectedLabel = cmp.get('v.releaseWindowDateWrapper').find(function(element){          		
                    if(element.value == mediaAndTerritoryValue){	                    
                        return element.label;
                    }});
            } else {   
                console.log('::::');
                // For Window Guideline Rule Builder
                if(cmp.get('v.windowGuidelineList')  && cmp.get('v.windowGuidelineList').find(function(element){
                    if(element.value == mediaAndTerritoryValue){
                        selectedLabel = element.label;
                        return true;
                    }})){
                    cmp.set('v.isWindowDate',true);
                } else{
                    cmp.set('v.isWindowDate',false);
                    if(cmp.get('v.releaseDateGuidelineList') && cmp.get('v.releaseDateGuidelineList').find(function(element){
                        if(element.value == mediaAndTerritoryValue){
	                         if(cmp.get('v.dateCalc').Window_Guideline_Date__c){
                                cmp.get('v.dateCalc').Window_Guideline_Date__c = '';
                            }
		            		/*cmp.get('v.dateCalc').Condition_Date_Duration__c = '';
		            		cmp.get('v.dateCalc').Condition_Criteria_Amount__c = '';
		            		cmp.get('v.dateCalc').Day_of_the_Week__c = '';*/
                            selectedLabel = element.label;
                            return true;
                        }})){
                    }
                }
                 console.log('::::'+mediaAndTerritoryValue);
                if(mediaAndTerritoryValue == 'Rights End Date'){
                    selectedLabel = mediaAndTerritoryValue;
                    cmp.get('v.dateCalc').Window_Guideline_Date__c = '';
                    cmp.set('v.isWindowDate',false);
                }
            }
            console.log('selectedLabel:::::',selectedLabel);
            if(selectedLabel){
                if(selectedLabel.label){
                    
                    cmp.get('v.dateCalc').Condition_Operand_Details__c = selectedLabel.label;
                } else{
                    
                    cmp.get('v.dateCalc').Condition_Operand_Details__c = selectedLabel;
                }
            }
            if(mediaAndTerritoryValue != 'Rights End Date')cmp.get('v.dateCalc').Conditional_Operand_Id__c = mediaAndTerritoryValue;
        } else { //LRCC - 1333
            cmp.get('v.dateCalc').Condition_Operand_Details__c = null;
            cmp.get('v.dateCalc').Conditional_Operand_Id__c = '';
        }
    },
    
    handleAlignment : function(cmp, event, helper) {
        
        var isDay = event.getParam("isDay"); 
       	cmp.set("v.dayAlignment", isDay); 
    }
})