({
    onblur : function(component,event,helper){
        // on mouse leave clear the listOfSeachRecords & hide the search result component 
        //component.set("v.listOfSearchRecords", null );
         console.log('onblur::hideOuterdd:',component.find('hideOuterdd'));
       // var element = component.find('hideOuterdd').getElement();
        // console.log('onblur::element:',element);
        // console.log('onblur:::',window.getComputedStyle(element).getPropertyValue('z-index'));
        
       // if(window.getComputedStyle(element).getPropertyValue('z-index') == 11){
            
            component.set("v.SearchKeyWord", '');
            var forOpen = component.find("pill-container");
            //$A.util.addClass(forOpen, 'slds-show');
            //$A.util.removeClass(forOpen, 'pillClose');
            var forclose = component.find("searchRes");
            $A.util.addClass(forclose, 'slds-is-close');
            $A.util.removeClass(forclose, 'slds-is-open');
       // }
    },
    onfocus : function(component,event,helper){
        console.log(':::::::::::::::::::')
        // show the spinner,show child search result component and call helper function
        $A.util.addClass(component.find("mySpinner"), "slds-show");
        //component.set("v.listOfSearchRecords", null ); 
        
        // Get Default 5 Records order by createdDate DESC 
        var getInputkeyWord = '';
        helper.searchHelper(component,event,getInputkeyWord);
        var forclose = component.find("pill-container");
        //$A.util.addClass(forclose, 'pillClose');
       // $A.util.removeClass(forclose, 'slds-show');
        var forOpen = component.find("searchRes");
        $A.util.addClass(forOpen, 'slds-is-open');
        $A.util.removeClass(forOpen, 'slds-is-close');
    },
    
    keyPressController : function(component, event, helper) {
        $A.util.addClass(component.find("mySpinner"), "slds-show");
        console.log('::::::::')
        // get the search Input keyword   
        var getInputkeyWord = component.get("v.SearchKeyWord");
        // check if getInputKeyWord size id more then 0 then open the lookup result List and 
        // call the helper 
        // else close the lookup result List part.   
        if(getInputkeyWord.length > 0){
            var forOpen = component.find("searchRes");
            $A.util.addClass(forOpen, 'slds-is-open');
            $A.util.removeClass(forOpen, 'slds-is-close');
            var forclose = component.find("lookup-pill");
            //$A.util.addClass(forclose, 'slds-hide');
            //$A.util.removeClass(forclose, 'slds-show');
            helper.searchHelper(component,event,getInputkeyWord);
        }
        else{  
            component.set('v.filteredRecords',JSON.parse(JSON.stringify(component.get('v.listOfSearchRecords'))));
            /*var forclose = component.find("searchRes");
            $A.util.addClass(forclose, 'slds-is-open');
            $A.util.removeClass(forclose, 'slds-is-close');*/
        }
    },
    
    // function for clear the Record Selaction 
    clear :function(component,event,helper){
        var selectedPillId = event.getSource().get("v.name");
        if(selectedPillId){
            var AllPillsList = component.get("v.lstSelectedRecords"); 
            var searchList = component.get('v.listOfSearchRecords');
            
            for(var i = 0; i < AllPillsList.length; i++){
                if(AllPillsList[i].value == selectedPillId){
                    searchList.push(AllPillsList[i]);
                    AllPillsList.splice(i, 1);
                    component.set("v.lstSelectedRecords", AllPillsList);
                    component.set('v.listOfSearchRecords',searchList);
                }  
            }
            helper.setSelectedValue(component,event,helper,AllPillsList);
        }
        component.set("v.SearchKeyWord",null);
       // component.set("v.listOfSearchRecords", null );      
    },
    
    // This function call when the end User Select any record from the result list.   
    handleComponentEvent : function(component, event, helper) {
        component.set("v.SearchKeyWord",null);
        // get the selected object record from the COMPONENT event 	 
        var listSelectedItems =  component.get("v.lstSelectedRecords");
        var selectedAccountGetFromEvent = event.getParam("recordByEvent");
        if(selectedAccountGetFromEvent && selectedAccountGetFromEvent.value){
            listSelectedItems.push(selectedAccountGetFromEvent);
            component.set("v.lstSelectedRecords" , listSelectedItems); 
            var searchList = component.get('v.listOfSearchRecords');
            for(var i = 0; i < searchList.length; i++){
                if(searchList[i].value.toLowerCase() == selectedAccountGetFromEvent.value.toLowerCase()){
                    
                    searchList.splice(i, 1);
                    component.set('v.listOfSearchRecords',searchList);
                }  
            }
            helper.setSelectedValue(component,event,helper,listSelectedItems);
        }
        
        $A.util.addClass(component.find("searchRes"), 'slds-is-close');
        $A.util.removeClass(component.find("searchRes"), 'slds-is-open'); 
        
        $A.util.addClass(component.find("lookup-pill"), 'slds-show');
        $A.util.removeClass(component.find("lookup-pill"), 'slds-hide');
        
       // $A.util.addClass(component.find("pill-container"), 'slds-show');
        //$A.util.removeClass(component.find("pill-container"), 'pillClose');
        
    },
    init : function(cmp,event,helper){
        event.stopPropagation();
        //cmp.set('v.filteredRecords',JSON.parse(JSON.stringify(cmp.get('v.listOfSearchRecords'))));
        const originalSearchRecords = (cmp.get('v.searchRecords') && cmp.get('v.searchRecords').length) ? JSON.parse(JSON.stringify(JSON.parse(JSON.stringify(cmp.get('v.searchRecords'))))) : JSON.parse(JSON.stringify(cmp.get('v.listOfSearchRecords')));
        cmp.set('v.listOfSearchRecords',JSON.parse(JSON.stringify(originalSearchRecords)));
        var searchRecords = JSON.parse(JSON.stringify(cmp.get('v.listOfSearchRecords')));
        //console.log('::::searchRecords:::'+JSON.stringify(searchRecords));
        if(cmp.get('v.value') || cmp.get('v.selectedList')){
            var selectedJSONList = [];
            var selectedValueList = cmp.get('v.value') ? cmp.get('v.value').split(';') : cmp.get('v.selectedList');
            console.log(':::selectedValueList:::::',selectedValueList)
            for(var i of selectedValueList){
                for(var j in searchRecords){
                    if(i.value){
                        if(searchRecords[j].value == i.value){
                            selectedJSONList.push(searchRecords[j]);
                            searchRecords.splice(j,1);
                            break;
                        }
                    } else{
                        if((searchRecords[j].value.toLowerCase() == i.toLowerCase()) || (searchRecords[j].value == i)){
                            selectedJSONList.push(searchRecords[j]);
                            searchRecords.splice(j,1);
                            break;
                        }
                    }
                    
                }
            }
            console.log('::selectedJSONList::'+JSON.stringify(selectedJSONList));
            cmp.set('v.listOfSearchRecords',searchRecords);
            cmp.set('v.filteredRecords',JSON.parse(JSON.stringify(searchRecords)));
            cmp.set('v.lstSelectedRecords',selectedJSONList);
            var forclose = cmp.find("lookup-pill");
            $A.util.addClass(forclose, 'slds-show');
            $A.util.removeClass(forclose, 'slds-hide');
            
            var forclose = cmp.find("searchRes");
            $A.util.addClass(forclose, 'slds-is-close');
            $A.util.removeClass(forclose, 'slds-is-open'); 
        }
    }
 })