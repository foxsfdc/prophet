({
    removePlanWindowGuidelineJunctions : function(component, event, helper) {
        //LRCC-1565
        component.set('v.showSpinner', true);
    	var windowGuidelineSelect = component.get('v.windowGuidelineSelect');
    	var planId = component.get('v.planRecordId');
    	
    	var removeSelectedJunctions = component.get('c.removeSelectedJunctions');
    	removeSelectedJunctions.setParams({windowGuidelines: windowGuidelineSelect, planId: planId});
    	
    	removeSelectedJunctions.setCallback(this, function(response) {
    		var state = response.getState();
    		
    		if(state === 'SUCCESS') {
	    		var invalidWindowGuidelines = response.getReturnValue().invalidWindowGuidelines;
	            var windowGuidelines = response.getReturnValue().windowGuidelines;
	            
	            for(var windowGuideline of windowGuidelines) {
            		if(windowGuideline.Customer__r) {
            			windowGuideline.customerName = windowGuideline.Customer__r.Name;
            		}
                    //LRCC-1493
            		if(windowGuideline.Window_Guideline_Alias__c != undefined) {
                        windowGuideline.Name = windowGuideline.Window_Guideline_Alias__c;
                    } else {
                        windowGuideline.Name = ' ';
                    }
	            	windowGuideline.link = '/one/one.app?sObject/'+ windowGuideline.Id + '/view#/sObject/' + windowGuideline.Id + '/view';
	            }

	            component.set("v.windowGuidelineList", windowGuidelines);
	            
                /*LRCC-1680
	            if(invalidWindowGuidelines && invalidWindowGuidelines.length > 0) {
                    component.set("v.invalidWindowGuidelines", invalidWindowGuidelines);
                    helper.hideRemoveModalSection();
                    helper.showInvalidateModalSection();
                } else {
                */
                helper.hideRemovePlanWindowGuidelineModal();
                //}
    		} else if (state === 'ERROR') {
                let errors = response.getError();
                let handleErrors = helper.handleErrors(errors, component, event, helper);
                if(handleErrors) {
                    helper.hideRemovePlanWindowGuidelineModal();
                    helper.errorToast(handleErrors);
                }
            } else {
            	helper.errorToast('The was an issue removing the window guideline from this plan.');
            }
            //LRCC-1565
            component.set('v.showSpinner', false);
    	});
    	$A.enqueueAction(removeSelectedJunctions);
	},
	
    invalidateVersionWindowGuidelines : function(component, event, helper) {
        var invalidWindowGuidelines = component.get("v.invalidWindowGuidelines");
        var invalidateWindowGuidelines = component.get("c.invalidateWindowGuidelines");
        invalidateWindowGuidelines.setParams({windowGuidelines: invalidWindowGuidelines});
        
        invalidateWindowGuidelines.setCallback(this, function(response) {
        	var state = response.getState();
        	
        	if(state === 'SUCCESS') {
		    	helper.hideInvalidateModalSection();
		        helper.showRemoveModalSection();
		        helper.hideRemovePlanWindowGuidelineModal();
        	} else {
            	this.errorToast('The was an issue invalidating the window guideline.');
        	}
            
        });
        $A.enqueueAction(invalidateWindowGuidelines);
    },
    
    hideRemovePlanWindowGuidelineModal : function(component, event, helper) {
        helper.hideRemovePlanWindowGuidelineModal();
    },
    
    hideInvalidateModal : function(component, event, helper) {
        helper.hideInvalidateModalSection();
        helper.showRemoveModalSection();
        helper.hideRemovePlanWindowGuidelineModal();
    }
})