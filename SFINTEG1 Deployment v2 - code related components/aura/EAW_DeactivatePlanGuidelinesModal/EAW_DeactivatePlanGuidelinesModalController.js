({
    deactivate : function(component, event, helper) {
        var planData = component.get("v.planData");
        var selectedRows = component.get("v.selectedRows");
        var action = component.get("c.deactivatePlans");
        action.setParams({"plans" : selectedRows});

        action.setCallback(this, function(response) {
            var state = response.getState();

            if(state === 'SUCCESS') {
                var results = response.getReturnValue();

                for(var result of results) {
                    var index = planData.findIndex(item => item.Id == result.Id);
                    if(index != -1) {
                        planData[index] = result;
                    }

                    result.link = '/one/one.app?sObject/'+ result.Id + '/view#/sObject/' + result.Id + '/view';
                }

                component.set("v.planData", planData);
                helper.successToast('The plan(s) has/have been deactivated.');
            } else {
                helper.errorToast('The plan(s) has/have not been deactivated.');
            }
        });
        $A.enqueueAction(action);

        helper.hideDeactivateModal();
    },

    hideDeactivateModal : function(component, event, helper) {
        helper.hideDeactivateModal();
    }
})