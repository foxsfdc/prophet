({
    init : function(cmp, event, helper) {
        
        console.log(cmp.get('v.ruleDetailStatusMap')+':::'+cmp.get('v.ruleDetailDateMap'));
        console.log(cmp.get('v.dateCalc').Id+'::::::::::::::'+cmp.get('v.ruleDetailDateMap')[cmp.get('v.dateCalc').Id])
        var operand = '';
        if(cmp.get('v.dateCalc').Condition_Operand_Details__c){
            var operandList = (cmp.get('v.dateCalc').Condition_Operand_Details__c.split('/'));
            operand = operandList[0] + ' in ' + operandList[1];
        }
        var dateCalcSet = {
            'operand' : operand,
            'date' : ''
        };
        if(cmp.get('v.dateCalc') && cmp.get('v.dateCalc').Id && cmp.get('v.ruleDetailDateMap')[(cmp.get('v.dateCalc').Id)]){
            dateCalcSet['date']  =  helper.setDate(cmp.get('v.ruleDetailDateMap')[(cmp.get('v.dateCalc').Id)],event,helper);
            if(cmp.get('v.ruleDetailStatusMap')[cmp.get('v.dateCalc').Id]){
                dateCalcSet['status'] =  cmp.get('v.ruleDetailStatusMap')[cmp.get('v.dateCalc').Id];
            } else{
                dateCalcSet['status'] = null;
            }
        }
        cmp.set('v.dateCalcSet',dateCalcSet);
    },
    
 })