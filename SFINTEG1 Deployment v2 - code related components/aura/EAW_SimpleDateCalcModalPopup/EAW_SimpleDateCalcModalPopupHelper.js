({
	setDate : function(dateValue,event,helper) {
		var dateList = dateValue.split('-');
        var date = dateList[2] + ' - ' +  helper.getMonth(dateList[1]) + ' - ' + dateList[0];
        return date;
	},
    getMonth : function(monthValue){
        var month = {
            '01' : 'Jan',
            '02' : 'Feb',
            '03' : 'Mar',
            '04' : 'Apr',
            '05' : 'May',
            '06' : 'Jun',
            '07' : 'Jul',
            '08' : 'Aug',
            '09' : 'Sep',
            '10' : 'Oct',
            '11' : 'Nov',
            '12' : 'Dec'
        };
		return month[monthValue];
    }
})