({
	openPopUp : function(cmp, event, helper) {
		var params = event.getParam('arguments');
        if(params && params.recordId){
            var action = cmp.get("c.getWindowHistory");
                    
            action.setParams({
                windowId   : params.recordId
            });
            
            action.setCallback(this,function(response){
                
                if(response.getState() === 'SUCCESS'){
                    //LRCC-1787
                    var result = response.getReturnValue();
                    console.log(';;;;;;;',result);
                    if(result && (result.records[0].LicenseInfoCodes_OldValue__c || result.records[0].OldTags__c || result.records[0].OldNotes__c || result.records[0].Histories.length)){

                        var histories = result.records[0].Histories || [];
                        if(result.records[0].LicenseInfoCodes_OldValue__c) {
                            var infoCodesHistory = [];
                            var OldInfoCodes = result.records[0].LicenseInfoCodes_OldValue__c.split('&&&');
                            var NewInfoCodes = result.records[0].License_Info_Codes__c;
                            var infoCodeChangedDates = result.records[0].LicenseInfoCodes_Changed_Dates__c ? result.records[0].LicenseInfoCodes_Changed_Dates__c.split('&&&') : [];
                            var infoCodeChangedUsers = result.records[0].LicenseInfoCodes_Changed_Users__c ? result.records[0].LicenseInfoCodes_Changed_Users__c.split('&&&') : [];
                            
                            for(var i=0; i < OldInfoCodes.length; i++) {
                                
                                if(i < OldInfoCodes.length && i+1 < OldInfoCodes.length) {
                                    infoCodesHistory.push({
                                        "Field" : "License Info Codes",
                                        "OldValue" : OldInfoCodes[i],
                                        "NewValue" : OldInfoCodes[i+1],
                                        "CreatedDate" : infoCodeChangedDates[i] ? infoCodeChangedDates[i].replace(' ', 'T') : "",
                                        "CreatedBy" : { "Name" : infoCodeChangedUsers[i] }
                                    });
                                } else {
                                    infoCodesHistory.push({
                                        "Field" : "License Info Codes",
                                        "OldValue" : OldInfoCodes[i],
                                        "NewValue" : NewInfoCodes ? NewInfoCodes : '',
                                        "CreatedDate" : infoCodeChangedDates[i] ? infoCodeChangedDates[i].replace(' ', 'T') : "",
                                        "CreatedBy" : { "Name" : infoCodeChangedUsers[i] }
                                    });
                                }
                            }
                            infoCodesHistory = infoCodesHistory.reverse();
                            histories = histories.concat(infoCodesHistory);
                        } 
                        
                        if(result.records[0].OldTags__c) {
                            var tagsHistory = [];
                            var OldTags = result.records[0].OldTags__c.split('&&&');
                            var NewTags = result.tags;
                            var TagsChangedDates = result.records[0].Tags_Changed_Dates__c ? result.records[0].Tags_Changed_Dates__c.split('&&&') : [];
                            var TagsChangedUsers = result.records[0].Tags_Changed_Users__c ? result.records[0].Tags_Changed_Users__c.split('&&&') : [];
                            
                            for(var i=0; i < OldTags.length; i++) {
                                
                                if(i < OldTags.length && i+1 < OldTags.length) {
                                    tagsHistory.push({
                                        "Field" : "Window Tags",
                                        "OldValue" : OldTags[i],
                                        "NewValue" : OldTags[i+1],
                                        "CreatedDate" : TagsChangedDates[i] ? TagsChangedDates[i].replace(' ', 'T') : "",
                                        "CreatedBy" : { "Name" : TagsChangedUsers[i] }
                                    });
                                } else {
                                    tagsHistory.push({
                                        "Field" : "Window Tags",
                                        "OldValue" : OldTags[i],
                                        "NewValue" : NewTags.length ? NewTags.join(';') : '',
                                        "CreatedDate" : TagsChangedDates[i] ? TagsChangedDates[i].replace(' ', 'T') : "",
                                        "CreatedBy" : { "Name" : TagsChangedUsers[i] }
                                    });
                                }
                            }
                            tagsHistory = tagsHistory.reverse();
                            histories = histories.concat(tagsHistory);
                        } 
                        
                        if(result.records[0].OldNotes__c) {
                            var notesHistory = [];
                            var OldNotes = result.records[0].OldNotes__c.split('&&&');
                            var NewNotes = result.notes;
                            var NotesChangedDates = result.records[0].Notes_Changed_Dates__c ? result.records[0].Notes_Changed_Dates__c.split('&&&') : [];
                            var NotesChangedUsers = result.records[0].Notes_Changed_Users__c ? result.records[0].Notes_Changed_Users__c.split('&&&') : [];
                            
                            for(var i=0; i < OldNotes.length; i++) {
                                if(i < OldNotes.length && i+1 < OldNotes.length) {
                                    notesHistory.push({
                                        "Field" : "Notes",
                                        "OldValue" : OldNotes[i],
                                        "NewValue" : OldNotes[i+1],
                                        "CreatedDate" : NotesChangedDates[i] ? NotesChangedDates[i].replace(' ', 'T') : "",
                                        "CreatedBy" : { "Name" : NotesChangedUsers[i] }
                                    });
                                } else {
                                    notesHistory.push({
                                        "Field" : "Notes",
                                        "OldValue" : OldNotes[i],
                                        "NewValue" : NewNotes.length ? NewNotes.join(';') : '',
                                        "CreatedDate" : NotesChangedDates[i] ? NotesChangedDates[i].replace(' ', 'T') : "",
                                        "CreatedBy" : { "Name" : NotesChangedUsers[i] }
                                    });
                                }
                            }
                            notesHistory = notesHistory.reverse();
                            histories = histories.concat(notesHistory);
                        } 
                        helper.setHistoryList(cmp,event,helper,histories);
                    } 
                    cmp.set('v.showModal',true);
                    $A.util.addClass(cmp.find('modal'),'slds-fade-in-open');
                    $A.util.addClass(cmp.find('backDrop'),'slds-backdrop_open');
                }
            });
            $A.enqueueAction(action);  
        }
	},
    closeModal : function(cmp){
        $A.util.removeClass(cmp.find('modal'),'slds-fade-in-open');
        $A.util.removeClass(cmp.find('backDrop'),'slds-backdrop_open');
        cmp.set('v.showModal',false);
    }
})