({
	setHistoryList : function(cmp,event,helper,historyList) {
        var validHistoryList = [];
        var created = {};
        for(var history of historyList){
            //LRCC-1787
            history.CreatedDate = history.CreatedDate ? helper.setDate(cmp,event,helper,history.CreatedDate) : '';
            history.OldValue = (history.OldValue == 'null' ? '' : (history.Field == 'Notes' ? history.OldValue.split(';') : history.OldValue));
            history.NewValue = (history.NewValue == 'null' ? '' : (history.Field == 'Notes' ? history.NewValue.split(';') : history.NewValue));
            history.type = 'String';
            if(history.Field == 'Name'){
                history.Field = 'Window Name';
            }
            if(history.Field == 'Status__c'){
                history.Field = 'Window Status';
            }
            if(history.Field == 'License_Info_Codes__c'){
                history.Field = 'Licence Info codes';
            }
            //LRCC-1787
            if(history.Field == 'Retired__c'){
                history.Field = 'Retired';
                history.OldValue = history.OldValue ? 'Yes' : 'No';
                history.NewValue = history.NewValue ? 'Yes' : 'No';
            }
            if(history.Field == 'Right_Status__c'){
                history.Field = 'Right Status';
            }
            //LRCC-1729
            if(history.Field == 'Start_Date_Text__c'){
                history.Field = 'Start Date Text';
            }
            if(history.Field == 'End_Date_Text__c'){
                history.Field = 'End Date Text';
            }
            if(history.Field.includes('Date__c')){ //LRCC-1787 'Date' to 'Date__c'
                history.type = 'Date';
                if(history.Field == 'Start_Date__c'){
                    history.Field = 'Start Date';
                } else if(history.Field == 'End_Date__c'){
                    history.Field = 'End Date';
                } else if(history.Field == 'Outside_Date__c'){
                    history.Field = 'Outside Date';
                } else if(history.Field == 'Tracking_Date__c'){
                    history.Field = 'Tracking Date';
                }
            }
            if(history.Field == 'created'){
                history.Field = 'Created';
                created = history;
            } else if(history.Field != 'EAW_Window_Guideline__c'){
                validHistoryList.push(history);
            }
        }
        if(created.Field){
            validHistoryList.splice(validHistoryList.length,0,created);
        }
        console.log('::::::::'+validHistoryList);
        cmp.set('v.historyList',validHistoryList);
	},
    setDate : function(cmp,event,helper,dateValue) {
        var dateAndTime = dateValue.split('T');
		var dateList = dateAndTime[0].split('-');
        var time = helper.setTime(dateAndTime[1].split(':'));
        var date = dateList[2] + '-' +  helper.getMonth(dateList[1]) + '-' + dateList[0];
        return date +' '+ time;
	},
    getMonth : function(monthValue){
        var month = {
            '01' : 'Jan',
            '02' : 'Feb',
            '03' : 'Mar',
            '04' : 'Apr',
            '05' : 'May',
            '06' : 'Jun',
            '07' : 'Jul',
            '08' : 'Aug',
            '09' : 'Sep',
            '10' : 'Oct',
            '11' : 'Nov',
            '12' : 'Dec'
        };
		return month[monthValue];
    },
    setTime : function(date) {
        //var d = new Date(date);
        var hh = date[0];
        var m = date[1];
       // var s = d.getSeconds();
        var dd = "AM";
        var h = hh;
        if(hh >= 7){
            h = hh - 7;
        }
        if (h >= 12) {
            h = hh - 12;
            dd = "PM";
        }
        if (h == 0) {
            h = 12;
        } else if(h < 10 && new String(h).length < 2){
            h = '0' + h;
        }
        if(m < 10  && new String(m).length < 2){
            m = '0' + m;
        }
        var replacement = h + ":" + m;
        replacement += " " + dd;
        return replacement;
    }
    
})