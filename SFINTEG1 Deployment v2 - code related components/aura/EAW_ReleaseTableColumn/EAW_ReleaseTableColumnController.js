({
    //LRCC-1445
	handleTempPermValue : function(component, event, helper) {
        
		var columnsCmp = component.find('inputTableID');
        var manualDate = false; // LRCC - 1082
        var istemp = true;
        for(var i=0; i < columnsCmp.length; i++) {
            console.log('columnsCmp:::',columnsCmp[i].get('v.column').fieldName+'::::::::::::::'+columnsCmp[i].get('v.displayValue'));
           
            if(columnsCmp[i].get('v.column').fieldName == 'Status__c' && columnsCmp[i].get('v.displayValue')){
                if(columnsCmp[i].get('v.displayValue') == 'Firm'){
                    istemp = false;
                } 
            }
            if(columnsCmp[i].get('v.column').fieldName == 'Manual_Date__c' && columnsCmp[i].get('v.displayValue')){
                manualDate = true;
            }
            console.log(manualDate+':::::'+istemp)
            if(columnsCmp[i].get('v.column').fieldName == 'Temp_Perm__c') {
                if(manualDate){
                    if(istemp){
                        columnsCmp[i].set('v.displayValue', 'Temporary');
                    } else{
                        columnsCmp[i].set('v.displayValue', 'Permanent');
                    }
                } else{
                     columnsCmp[i].set('v.displayValue', 'None');
                }
            }            
        }
	},
    handleDataModifyConfirmation : function(cmp,event,helper){ // LRCC - 1638
        var columnsCmp = cmp.find('inputTableID');
        var params = event.getParams();
        if(params.field){
            if(params.field == 'Manual_Date__c' && !params.confirmation){
                for(var i=0; i < columnsCmp.length; i++) {
                    if(columnsCmp[i].get('v.column').fieldName == 'Manual_Date__c' ){
                        columnsCmp[i].set('v.displayValue', params.valueToChange);
                    }
                }
            }/* else if(!params.confirmation && (params.field == 'Start_Date__c' || params.field == 'End_Date__c' || params.field == 'Outside_Date__c' ||params.field == 'Tracking_Date__c')) { //LRCC-1447
            
                    for(var i=0; i < columnsCmp.length; i++) {
                        if(columnsCmp[i].get('v.column').fieldName == params.field ){
                            columnsCmp[i].set('v.displayValue', params.valueToChange);
                        }
                    }
                }*/
        }
    },
    doInit : function(cmp,event,helper) { // LRCC - 1459
        var columns = cmp.get('v.resultColumns');
        var body = cmp.get("v.body");
        var recordMap = cmp.getReference('v.recordMap');
        var record = cmp.getReference('v.record');
        for(var i=0;i<columns.length;i++){
            $A.createComponent(
                "c:EAW_PlanDateResultsCell", // LRCC - 1459
                {
                    "aura:id": "inputTableID",
                    "recordMap": recordMap,
                    "column": columns[i],
                    "data" : record,
                    "colIndx" : i
                },
                function(newButton, status, errorMessage){
                    //Add the new button to the body array
                    if (status === "SUCCESS") {
                        console.log(':new::'+newButton);
                        body.push(newButton);
                    }
                    else if (status === "INCOMPLETE") {
                        console.log("No response from server or client is offline.")
                        // Show offline error
                    }
                        else if (status === "ERROR") {
                            console.log("Error: " + errorMessage);
                            // Show error message
                        }
                }
            );
        }
        cmp.set("v.body", body);
      //  console.log('::body:'+body);
        
    },
})