({
    init : function(cmp, event, helper) {
        
        cmp.set('v.spinner', true);
        cmp.set('v.showModal', true);
        
        if(cmp.get('v.windowGuidelineId')) {
            var windowGuideline = {'icon':'standard:account','id':cmp.get('v.windowGuidelineId'),'sObjectType':'EAW_Window_Guideline__c','title':cmp.get('v.windowGuidelineName')};
            var selectedWindowGuidelines = cmp.get('v.selectedWindowGuidelines');
            selectedWindowGuidelines.push(windowGuideline);
            cmp.set('v.selectedWindowGuidelines',selectedWindowGuidelines);
        }
        helper.setFieldValues(cmp, event, helper);
        cmp.set('v.spinner', false);
    },
    
    handleSubmit : function(component ,event,helper) {
        
        component.set('v.spinner',true);
        event.preventDefault(); //stop form submission
        
        var canSubmit;
        var eventFields = event.getParam("fields");
        var inputFields = component.find('inputFields');
        
        if(component.get('v.selectedWindowGuidelines') && component.get('v.selectedWindowGuidelines').length == 0) {
            console.log('::success::');
            canSubmit = false;
            $A.util.addClass(component.find('genericCmp'), ' requiredBorder'); 
        }
        else {
            canSubmit = true;
        }
        
        if(canSubmit) {
            console.log('::submit::');
            
            let data = JSON.parse(JSON.stringify(eventFields));
            data['EAW_Window_Guideline__c'] = component.get('v.selectedWindowGuidelines')[0].id;
            console.log('data:::',data);
            component.find('recordForm').submit(data);
        } 
        component.set('v.spinner',false);        
    },
    
    handleError: function(component, event) {
        var errors = event.getParams();
        console.log("response", JSON.stringify(errors));
    },
    
    handleSuccess : function(cmp,event,helper) { 
        
        var windowId = event.getParams().response.id;
        
        var base_url = window.location.origin;
        if(cmp.get('v.windowGuidelineId')) {
            location.replace(base_url+"/lightning/r/EAW_Window_Guideline__c/"+cmp.get('v.windowGuidelineId')+"/view");
        } else {
            location.replace(base_url+"/lightning/r/EAW_Window__c/"+windowId+"/view");
        }
        cmp.set('v.spinner',false);        
    },
    
    closeModal : function(cmp) {
        
        cmp.set('v.showModal',false);
        var base_url = window.location.origin;
        if(cmp.get('v.windowGuidelineId')) {
            location.replace(base_url+"/lightning/r/EAW_Window_Guideline__c/"+cmp.get('v.windowGuidelineId')+"/view");
        } else if(cmp.get('v.addWindowModal')) {
            var addWindowModalEvent = cmp.getEvent("addWindowModalEvent");                        
            addWindowModalEvent.fire(); 
        } else {
            location.replace(base_url+"/lightning/o/EAW_Window__c/list?filterName=Recent");
        }
    }
})