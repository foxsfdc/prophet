//This file was referenced from https://github.com/pozil/sfdc-ui-lookup
({
    search : function(component, event, helper) {
        var searchTerm = component.get('v.searchTerm');
        if(searchTerm.length >= 1) {
        	//LRCC:1151
            var totalRecords =  helper.duplicateFilter(component);//component.get('v.totalRecords');
            //var selectedIds = helper.getSelectedIds(component);
            var selectedIds = component.get("v.selectedIds");
            //console.log('call from controller',searchTerm);
            var filteredRecords = totalRecords.filter((record) => {
                if(record.title.toLowerCase().indexOf(searchTerm.toLowerCase()) >= 0 && !(selectedIds.indexOf(record.id) >= 0)) {
                	return record;
            	}
            });
            //console.log('serch::::')
           /* var filteredRecords = [];
            if(component.get("v.checkconflict")) {
                for(var i = 0; i < totalRecords.length; i++) {
                    if(selectedIds.indexOf(totalRecords[i].id) == -1) {
                        filteredRecords.push(totalRecords[i]);
                    }
                }
            }*/
            helper.generateFilteredRecordView(component, helper, filteredRecords);
        }
    },
    
    //LRCC:1232 start
    onInput : function(component, event, helper) {
       // console.log('oninput..',component.get('v.inputSpinner'));
        if(helper.isSelectionAllowed(component)) {
            //LRCC-1286
            component.set('v.hasFocus', true);
        	const newSearchTerm = event.target.value;
            const genericType = component.get('v.genericType');     
           // console.log('newSearchTerm::::',newSearchTerm);    
            //LRCC-1534
            if(genericType && genericType == 'EAW_Title__c'){
                if( newSearchTerm && newSearchTerm.length > 2 ) {
                    component.set('v.inputSpinner', true);
                    var searchEvent = component.getEvent("searchEvent");
                    searchEvent.setParams({
                        "searchTerm" : newSearchTerm
                    });
                    searchEvent.fire();
                } else {
                    component.set('v.inputSpinner', false);
                    component.set('v.totalRecords',[]);
                }
            } else {
                component.set('v.inputSpinner', true);
                var searchEvent = component.getEvent("searchEvent");
                searchEvent.setParams({
                    "searchTerm" : newSearchTerm
                });
                searchEvent.fire();
            }
           // console.log('enter::::',component.get('v.inputSpinner'));
        }
    },
    
    //LRCC:1232 end
    handleTotalRecordsLoaded : function(component, event, helper) {
        
        var totalRecords = JSON.parse(JSON.stringify(component.get('v.totalRecords')));
        
        var selection = JSON.parse(JSON.stringify(component.get('v.selection')));
        var filteredRecords = [];
        var selectedIds = [];
        
        for (var i of selection) {
            selectedIds.push(i.id);
        }
        filteredRecords = totalRecords.filter(totalRecord => ! selectedIds.includes(totalRecord.id));
        helper.generateFilteredRecordView(component, helper, filteredRecords);
    },
    
    onComboboxClick : function(component, event, helper) {
    	//console.log('selectionsDisplayInTable:::::::',component.get('v.selectionsDisplayInTable'));
        if(component.get('v.selectionsDisplayInTable')){
            helper.search(component);
        }
        const blurTimeout = component.get('v.blurTimeout');
        if(blurTimeout) {
            clearTimeout(blurTimeout);
        }
        //console.log('oncombobox:::::testing:::;');
       // component.set('v.hasFocus', true);
       // component.find("searchInput").getElement().focus();
    },
    
    /*LRCC:1232 start
    onFocus : function(component, event, helper) {
        if(helper.isSelectionAllowed(component)) {
            component.set('v.hasFocus', true);
            
            console.log('onFocus')
            
            /*document.onkeydown = function(e) {
                switch(e.keyCode) {
                    case 38: // if the UP key is pressed
                        if (document.activeElement == (maininput || first)) {
                            break;
                        } else {
                            document.activeElement.parentNode.previousSibling.firstChild.focus();
                        }

                        break;
                    case 40: // if the DOWN key is pressed
                        if (document.activeElement == maininput) {
                            first.firstChild.focus();
                        } else {
                            document.activeElement.parentNode.nextSibling.firstChild.focus();
                        }
                        break;
                }
            }; 
            
            var totalRecords = component.get('v.totalRecords');
            //var selectedIds = helper.getSelectedIds(component);\
            var selectedIds = component.get("v.selectedIds");
            var searchTerm = component.get('v.searchTerm');
            var existingList = component.get('v.tableData');
            
            if(totalRecords.length > 0 && searchTerm.length == 0) {
                var filteredRecords = totalRecords.filter((record) => {
                    if(!(selectedIds.indexOf(record.id) >= 0)) {                    	
                        return record;// LRCC- 1098 commented
                    }
                });
    			
                console.log('component.get("v.checkconflict")::::::::::',component.get("v.checkconflict"));
                    
                var filteredRecords = [];
                if(component.get("v.checkconflict")) {
                    for(var i = 0; i < totalRecords.length; i++) {
                        var isExisting = false;
                        if(existingList.length) {
                         
                            existingList.find(function(element) {
                                if(element.id == totalRecords[i].id) { 
                                    
                                    isExisting = true;                                
                                }                          
                            });
                        } 
                        if(!isExisting) {
                            filteredRecords.push(totalRecords[i]);    
                        }
                    }
                  console.log('serch::ofocus::',filteredRecords);
                  helper.generateFilteredRecordView(component, helper, filteredRecords);
                }
                //LRCC:1150
                else {
                
                	var fliteredList = helper.duplicateFilter(component);
                    console.log('else::filteredList::',fliteredList);
					helper.generateFilteredRecordView(component, helper, fliteredList);
					
            	}
            }
        }
    }, LRCC:1232 end*/ 

    onBlur : function(component, event, helper) {
        var element = component.find('listBoxUL').getElement();
        if(window.getComputedStyle(element).getPropertyValue('z-index') != 2){
            if(helper.isSelectionAllowed(component)) {
                const blurTimeout = window.setTimeout(function(){
                    component.set('v.hasFocus', false);
                    component.set('v.blurTimeout', null);
                }, 300);
                
                component.set('v.blurTimeout', blurTimeout);
            }
        }
    },
    onRemoveSelectedItem : function(component, event, helper) {
        const itemId = event.getSource().get('v.name');
        helper.removeSelectedItem(component, itemId);
    },
    onClearSelection : function(component, event, helper) {
        helper.clearSelection(component);
    },
})