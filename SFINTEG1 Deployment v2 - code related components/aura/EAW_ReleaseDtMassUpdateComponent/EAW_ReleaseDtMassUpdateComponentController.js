({
    doMassUpdate : function(component, event, helper) {
                
        var newTerritoryName = component.get('v.selectedTerritory')[0]==null?null:component.get('v.selectedTerritory')[0].title;
        var newLanguageName = component.get('v.selectedLanguage')[0]==null?null:component.get('v.selectedLanguage')[0].title;
        var newCustomerName = component.get('v.selectedCustomer')[0]==null?null:component.get('v.selectedCustomer')[0].title;
        var newProductName = component.get('v.selectedProduct')[0]==null?null:component.get('v.selectedProduct')[0].title;
        var newReleaseDateName = component.get('v.selectedReleaseDate')[0]==null?null:component.get('v.selectedReleaseDate')[0].title;
        
        var selectedRows = component.get('v.selectedRows');
        var recIds = new Array();
        for(let row of selectedRows){
            recIds.push(row.Id);
        }
        
        let subject = component.get("v.title");
        let body = component.get("v.body");
        
        if(subject && subject != null) {
            
            component.set("v.showSpinner",true);
            var action = component.get("c.massUpdateReleaseDateGuidelines");
            action.setParams({
                "recIds" : selectedRows, "newTerritoryName" : newTerritoryName, 
                "newLanguageName":newLanguageName, "newCustomerName":newCustomerName, 
                "newProductName":newProductName, "newReleaseDateName":newReleaseDateName,
                "note":component.get("v.body"),
                "title":component.get("v.title")
            });
            
            action.setCallback(this, function(response){
                if(response.getState() === "SUCCESS"){
                    var selectedRows = component.get('v.selectedRows');
                    var recIds = new Array();
                    for(let row of selectedRows){
                        recIds.push(row.Id);
                    }
                    
                    var dataTableData = component.get("v.myData");
                    for(var i=0 ; i < dataTableData.length ; i++){
                        if(selectedRows.indexOf(dataTableData[i].Id) > -1){
                            dataTableData[i].Territory__c = newTerritoryName==null?dataTableData[i].Territory__c:newTerritoryName;
                            dataTableData[i].Language__c = newLanguageName==null?dataTableData[i].Language__c:newLanguageName; 
                            dataTableData[i].Customer__c = newCustomerName==null?dataTableData[i].Customer__c:newCustomerName; 
                            dataTableData[i].Product_Type__c = newProductName==null?dataTableData[i].Product_Type__c:newProductName;
                            dataTableData[i].Release_Date_Type__c = newReleaseDateName==null?dataTableData[i].Release_Date_Type__c:newReleaseDateName;
                        }
                    } 
                    component.set("v.myData",dataTableData);
                    
                    var massUpdateTableEvent = $A.get("e.c:EAW_MassUpdateTableEvent");
                    massUpdateTableEvent.fire();
                    
                    //hide spinner
                    component.set("v.showSpinner",false);
                    //hide modal 
                    component.set('v.selectedReleaseDate', []);
                    component.set('v.selectedLanguage', []);
                    component.set('v.selectedTerritory', []);
                    component.set('v.selectedProduct', []);
                    component.set('v.selectedCustomer',[]);
                    component.set('v.body','');
                    component.set('v.title','');
                    component.set('v.displayComponent',false);
                }
                else{
                    console.log("Error EAW_ReleaseDtMassUpdateComponent.doMassUpdate " + JSON.stringify(response.getError()));
                    component.set("v.showSpinner",false);
                }
            });
            $A.enqueueAction(action);
        } else {
            let label = $A.get("$Label.c.EAW_NotesSubjectValidation");
            helper.errorToast(label);
        }
    },
	hideMassUpdate : function(component, event, helper) {
        component.set('v.displayComponent',false);
        component.set('v.selectedReleaseDate', []);
        component.set('v.selectedLanguage', []);
        component.set('v.selectedTerritory', []);
        component.set('v.selectedProduct', []);
        component.set('v.selectedCustomer',[]);
        component.set('v.body','');
        component.set('v.title','');
    }
})