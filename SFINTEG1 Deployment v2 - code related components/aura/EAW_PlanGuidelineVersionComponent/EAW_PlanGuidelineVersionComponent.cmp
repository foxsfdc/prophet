<aura:component controller="EAW_PlanController" implements="force:appHostable,flexipage:availableForAllPageTypes,force:hasRecordId">
    
    <!-- Ref for current record -->
    <aura:attribute name="recordId" type="Id" />
    <aura:attribute name="planObj" type="Object"/>
    
    <!-- list of all versions -->
    <aura:attribute name="versionList" type="Object" />
    
    <!-- init vars of components -->
    <aura:handler name="init" value="{!this}" action="{!c.doInit}"/>
	
    <!-- used to label and create datatable -->
    <aura:attribute name="versionHistoryColumns" type="List"/>
    
    <aura:attribute name="title" type="string" />
    <aura:attribute name="body" type="string" />
    <!--LRCC-1296-->
    <aura:attribute name="showVersion" type="boolean"/>
    <aura:attribute name="showVersionHistory" type="boolean"/>
    
    <force:recordData aura:id="recordLoader"
                      recordId="{!v.recordId}"
                      fields="Version__c,Status__c"
                      targetFields="{!v.planObj}"
                      targetError="{!v.recordLoadError}"
                      />
    
    <div class="slds-grid">
        <div class="slds-col">
            <article class="slds-card">
                <div class="slds-card__body slds-card__body_inner">
                    
                    <!-- Versioning -->
                    <br/>
                    <div class="slds-grid">
                        <div class="slds-col slds-size_3-of-12 slds-grid">
                            <div class="slds-col slds-size_2-of-5 p-t-20">
                                <div class="label-align-center">Version</div>
                            </div>
                            <div class="display-inline slds-col slds-size_3-of-5">
                                <div class="label-align-center">{!v.planObj.Version__c}</div>
                            </div>
                        </div>
                        
                        <div class="slds-col slds-size_3-of-12 slds-grid">
                            <div class="slds-col slds-size_2-of-5 p-t-20">
                                <div class="label-align-center">Version Status: </div>
                            </div>
                            <div class="display-inline slds-col slds-size_3-of-5">
                                {!v.planObj.Status__c}
                            </div>
                        </div>
                        
                        <div class="slds-col slds-size_6-of-12 slds-grid">
                            <div class="slds-col">
                                <div class="slds-float_right">
                                    <lightning:button variant="neutral" label="View Version History" onclick="{!c.showVersionModal}"/>
                                    <aura:if isTrue="{!equals(v.planObj.Status__c,'Active')}">
                                    	<lightning:button variant="neutral" label="New Version" onclick="{!c.showConfirmModal}"/>
                                    </aura:if>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>   
        </div>
    </div>
    
    <!-- Version History Popup -->
    <aura:if isTrue="{!v.showVersionHistory}">
        <div class="popup" id="eaw-plan-version-popup">
            <section role="dialog" tabindex="-1" aria-labelledby="modal-heading-01" aria-modal="true" aria-describedby="modal-content-id-1" class="slds-modal slds-fade-in-open">
                <div id="eaw-plan-version-container" class="slds-modal__container">
                    <!-- Title for Modal + Close button -->
                    <header class="slds-modal__header">
                        <button class="slds-button slds-button_icon slds-modal__close slds-button_icon-inverse" title="Close" onclick="{!c.hideVersionModal}" aria-hidden="true">
                            <lightning:icon iconName="utility:close" size="small" />
                            <span class="slds-assistive-text">Plan Guideline Version History</span>
                        </button>
                        <h2 id="modal-heading-01" class="slds-text-heading_medium slds-hyphenate">Plan Guideline Version History</h2>
                    </header>
                    <div class="slds-modal__content slds-p-around_medium" id="modal-content-id-1">
                        <!-- Modal main content goes here -->
                        <c:EAW_FieldSetComponent myData="{! v.versionList }" myColumns="{! v.versionHistoryColumns }"/>
                    </div>
                    
                    <!-- Footer Buttons -->
                    <footer class="slds-modal__footer">
                        <lightning:button variant="neutral" label="Cancel" onclick="{!c.hideVersionModal}" />
                    </footer>
                </div>
            </section>
            <div class="slds-backdrop slds-backdrop_open"></div>
        </div>
    </aura:if>
    
    <!-- Confirmation Popup -->
    <aura:if isTrue="{!v.showVersion}">
        <div class="popup" id="eaw-plan-confirm-popup">
            <section role="dialog" tabindex="-1" aria-labelledby="modal-heading-01" aria-modal="true" aria-describedby="modal-content-id-1" class="slds-modal slds-fade-in-open">
                <div class="slds-modal__container">
                    <!-- Title for Modal + Close button -->
                    <header class="slds-modal__header">
                        <button class="slds-button slds-button_icon slds-modal__close slds-button_icon-inverse" title="Close" onclick="{!c.hideConfirmModal}" aria-hidden="true">
                            <lightning:icon iconName="utility:close" size="small" />
                            <span class="slds-assistive-text">New Version</span>
                        </button>
                        <h2 id="modal-heading-01" class="slds-text-heading_medium slds-hyphenate">New Version</h2>
                    </header>
                    <div class="slds-modal__content slds-p-around_medium" id="modal-content-id-1">
                        <p>"Create New Version" will create a new draft identical to the latest version</p>
                    </div>
                    
                    <div class="slds-modal__content" style="padding-top:5px;">
                        <h3 class="slds-section__title slds-m-around_medium">
                            <button aria-controls="expando-unique-id" aria-expanded="true" class="slds-button slds-section__title-action">
                                <span class="slds-truncate" title="Section Title">Notes</span>
                            </button>
                        </h3>
                        <div aria-hidden="false" class="slds-m-around_medium" id="expando-unique-id">
                            <div class="slds-grid" style="padding-top: 5px">
                                <div class="slds-col slds-size_1-of-4">
                                    <label class="label-align-center">
                                        Version Notes<abbr class="slds-required" title="required">*</abbr>
                                    </label>
                                </div>
                                <div class="slds-col slds-size_3-of-4">
                                    <ui:inputText value="{!v.title}"/>
                                </div>
                            </div>
                            <!--LRCC-1571-->
                            <!--div class="slds-grid" style="padding-top: 5px">
                                <div class="slds-col slds-size_1-of-4">
                                    <label class="label-align-center">Body</label>
                                </div>
                                <div class="slds-col slds-size_3-of-4">
                                    <ui:inputTextArea value="{!v.body}"/>
                                </div>
                            </div-->
                        </div>
                    </div>
                                    
                    <!-- Footer Buttons -->
                    <footer class="slds-modal__footer">
                        <lightning:button variant="neutral" label="OK" onclick="{!c.createNewVersion}" />
                        <lightning:button variant="neutral" label="Cancel" onclick="{!c.hideConfirmModal}" />
                    </footer>
                </div>
            </section>
            <div class="slds-backdrop slds-backdrop_open"></div>
        </div>
    </aura:if>
</aura:component>