({
	doInit : function(component, event, helper) {
        
        //Set column header names for data table
        component.set('v.versionHistoryColumns', [
            {label: 'Version', fieldName: 'Version__c', type: 'number', sortable: true, initialWidth: 50},
            {label: 'Status', fieldName: 'Status__c', type: 'text', initialWidth: 100},
            {label: 'Name', fieldName: 'Plan_Guideline', type: 'url',
            	typeAttributes: { label: {fieldName: 'Name'}, target: '_blank'}, initialWidth: 200},
            {label: 'Notes', fieldName: 'VersionNotes__c', type: 'text', initialWidth: 200},
            {label: 'Created Date', fieldName: 'CreatedDate', type: 'date', initialWidth: 200},
            {label: 'Created By', fieldName: 'CreatedBy', type: 'text', initialWidth: 200},
            {label: 'Activated Date', fieldName: 'ActivatedDate__c', type: 'text', initialWidth: 200},
            {label: 'Activated By', fieldName: 'ActivatedBy', type: 'text', initialWidth: 200}
        ]);
        
        //Fetch the version history
        var action2 = component.get("c.getAllVersions");
        action2.setParams({"recordId" : component.get("v.recordId")});
        action2.setCallback(this, function(response) {
            let results = response.getReturnValue();
            if(results != null) {
            	
            	for(let result of results){
                    // Set associated link with each name of result
                    result.Plan_Guideline = '/one/one.app?sObject/'+ result.Id + '/view#/sObject/' + result.Id + '/view';
                    result.ActivatedBy = result.ActivatedBy__r == null ? '' : result.ActivatedBy__r.Name;
                    result.CreatedBy = result.CreatedBy == null ? '' : result.CreatedBy.Name;
                }
                
                // Sorts version history results in ascending order
                results.sort((a, b) => (a.Version__c - b.Version__c));
                
                component.set("v.versionList", results);
            } 
        });
        
        $A.enqueueAction(action2);
	},                       
    hideVersionModal : function(component, event, helper) {
        component.set("v.showVersionHistory",false);
    },
    showVersionModal : function(component, event, helper) {
        component.set("v.showVersionHistory",true);
    },
    hideConfirmModal : function(component, event, helper) {
        component.set("v.showVersion",false);
        helper.clearData(component);
    },
    showConfirmModal : function(component, event, helper) {
        component.set("v.showVersion",true);
    },
    createNewVersion : function(component, event, helper) {
        
        if($A.util.isEmpty(component.get('v.title'))) {
            let label = $A.get("$Label.c.EAW_NotesSubjectValidation");
            helper.errorToast(label);
        }
        else {
            if(component.get('v.planObj').Status__c !== 'Draft') {
                
                let title = component.get('v.title');
                let body = component.get('v.body');
                
                //call Apex Controller to get version page to redirect to
                var action = component.get("c.getNewVersionId");
                action.setParams({
                    'recordId': component.get("v.recordId"), 
                    'title': title,
                    'body': body
                });
                action.setCallback(this, function (response) {
                    
                    let state = response.getState();
                    if (state == 'SUCCESS') {
                    	let results = response.getReturnValue();
                        
                        if(results != null) {
                            window.parent.location = '/one/one.app#/sObject/' + results + '/view'
	                    }
                        helper.clearData(component);
                	} 
                    else if (state == 'ERROR') {
                        let errors = response.getError();
                        
                        if (errors && Array.isArray(errors) && errors.length > 0) {
                            helper.errorToast(errors[0].message);
                        }
                    }
                });
                $A.enqueueAction(action);
            } 
            else {
                helper.errorToast('Draft of current Plan Guideline already exists');
            }
        }
    }
})