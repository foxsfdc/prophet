({
    clearData : function(component) {
        component.set('v.title', '');
        component.set('v.body', '');
    },
    errorToast : function(message) {
        var toastEvent = $A.get('e.force:showToast');
	
        toastEvent.setParams({
            'title': 'Error!',
            'type': 'error',
            'message': message
        });
	    toastEvent.fire();
    },
    successToast : function(message) {
        var toastEvent = $A.get('e.force:showToast');
	
        toastEvent.setParams({
            'title': 'Success!',
            'type': 'success',
            'message': message
        });
        toastEvent.fire();
    }
})