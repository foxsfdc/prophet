({
    updateWindows : function(component, event, helper) {

        // Validate Inputs
        // Blank Start Date/End Date == clear them
        // Blank Tags == Clear them
        // Status MUST be there

        let windowList = component.get('v.windowList') ? component.get('v.windowList') : [];
        let windowStatus = component.find('windowStatus') ? component.find('windowStatus').get('v.value') : '';
        let startDate = component.find('startDateInput').get('v.value') ? component.find('startDateInput').get('v.value') : '';
        let endDate = component.find('endDateInput').get('v.value') ? component.find('endDateInput').get('v.value') : '';
        let notes = component.find('pastedTitleInput') ? component.find('pastedTitleInput').get('v.value') : '';
        let selectedTags = helper.collectIds(component.get('v.selectedTags'));

        console.log(windowStatus);
        console.log(startDate);
        console.log(endDate);
        console.log(selectedTags);
        console.log(notes);

        if(windowStatus == '') {
            helper.errorToast('Must Select a Window Status');
            return; // Don't make call to update
        } else if(windowList.length === 0) {
            helper.errorToast('No Windows Selected');
            return;
        }

        // Send the APEX call
        try {
            let action = component.get("c.massUpdateWindows");
            action.setParams({
                'windowIds' : windowList,
                'tagIds' : selectedTags,
                'windowStatus' : windowStatus,
                'startDate' : startDate,
                'endDate' : endDate,
                'noteText' : notes
            });
            action.setCallback(this, function(response) {
                let state = response.getState();
                let results = response.getReturnValue();

                if (state === 'SUCCESS') {
                    //console.log(results);
                    helper.successToast(results.length + ' Windows Updated');
                } else if (state === 'ERROR') {
                    console.log(response.getError());
                    helper.error('Failed to Update Windows');
                }
            });
            $A.enqueueAction(action);
        } catch (e) {
            console.log(e);
        }
    },
    /** Methods below hide/show the popup*/
    showPopup : function(component, event, helper) {
        document.getElementById("EAW_TitlePlanDateMassUpdateComponent").style.display = "block";
    },
    hidePopup : function(component, event, helper){
        document.getElementById("EAW_TitlePlanDateMassUpdateComponent").style.display = "none";
        component.set('v.windowList', []);
        component.set('v.startDate', '');
        component.set('v.endDate', '');
        component.set('v.selectedTags', []);
    }
})