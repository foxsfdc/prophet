({    
    clearSearch : function(component) {
        component.set('v.selectedCustomers', []);
        component.set('v.selectedPlanGuidelines', []);
        component.set('v.selectedMedias', []);
        component.set('v.selectedTerritories', []);
        component.set('v.selectedLanguages', []);
        //component.find('windowGuidelineName').set('v.value', '');
        component.set('v.selectedWindowGuidelines', []);
        component.find('windowType').set('v.value', '');
        component.find('versionStatus').set('v.value', '');
        //component.find('contentOwner').set('v.value','');
        //LRCC-1511 - Remove Lifecycle
        //component.find('lifeCycle').set('v.value','');
        //LRCC-1568
        component.set('v.selectedProductType', []);
        //LRCC:1200
        //component.set('v.windowData', []); 
    },
    errorToast : function(message) {
    	var toastEvent = $A.get('e.force:showToast');
    		
		toastEvent.setParams({
			'title': 'Error!',
			'type': 'error',
			'message': message
		});
		toastEvent.fire();
    },
    search : function(component, event, helper,pagesPerRecord,currentPageNumber,pageIdMap,selectedFilterMap) {
        try {	
            var customers = component.get('v.selectedCustomers');
            var plans = component.get('v.selectedPlanGuidelines');
            var medias = component.get('v.selectedMedias');
            var territories = component.get('v.selectedTerritories');
            var languages = component.get('v.selectedLanguages');
            var windowGuidelines = component.get('v.selectedWindowGuidelines');
            //var windowGuidelineName = component.find('windowGuidelineName') && component.find('windowGuidelineName').get('v.value') != null ? component.find('windowGuidelineName').get('v.value') : '';
            var contentOwner = component.find('contentOwner') && component.find('contentOwner').get('v.value') != null ? component.find('contentOwner').get('v.value') : '';            
             //LRCC-1012
            var windowType = (component.find('windowType') == '--None--') ? component.find('windowType').get('v.value') : '';
             var versionStatus = component.find('versionStatus') ? component.find('versionStatus').get('v.value') : '';
            //LRCC-1511 - Remove Lifecycle
            //var lifeCycle = component.find('lifeCycle') ? component.find('lifeCycle').get('v.value') : '';
            var numWindowsToReturn = component.get('v.numRowsDisplayed');
            //LRCC-1568
            var productType = component.get('v.selectedProductType');
            console.log('<<productType>>'+this.collectNames(productType));
			//LRCC-1511 - Remove Lifecycle
            if(customers.length > 0 || plans.length > 0 || medias.length > 0 || territories.length > 0 || languages.length > 0 ||
                windowGuidelines.length > 0 || contentOwner || windowType || versionStatus || 
                //lifeCycle || 
                productType.length >0) {
                
                var action = component.get("c.searchWindows");
                action.setParams({
                    "customerIds": this.collectCustomers(customers),//this.collectIds(customers),//LRCC-1236
                    "planGuidelineIds": this.collectIds(plans),
                    "mediaNames": this.collectNames(medias),
                    "territoryNames": this.collectNames(territories),
                    "languageNames": this.collectNames(languages),
                    "windowGuidelineName": this.collectNames(windowGuidelines), // LRCC - 1484
                    "contentOwner" : contentOwner,
                    "windowType": windowType,
                    "versionStatus": versionStatus,
                   //LRCC-1511 - Remove Lifecycle
                   // "lifeCycle" : lifeCycle,
                    "numWindowsToReturn": numWindowsToReturn,
                    'pagesPerRecord' : pagesPerRecord,
                    'currentPageNumber':currentPageNumber,
                    'pageIdMapUI':pageIdMap,
                    'selectedFilterMap':selectedFilterMap,
                    "productType" : this.collectNames(productType)
                    
                });
                
                this.showSpinner(component);
                
                action.setCallback(this, function (response) {
                    var state = response.getState();
                    var results = response.getReturnValue();
                    
                    if (state === 'SUCCESS') {
                        if (results) {
                            //LRCC-1459
                            console.log('results::::test:::test:::',results);
                            if(results.totalCount && results.totalCount[0].expr0){
                                
                                console.log('results::::results.totalCount[0].expr0:::test:::',results.totalCount[0].expr0);    
                                component.set('v.totalRecordCount', results.totalCount[0].expr0);    
                            }else {
                                component.set('v.totalRecordCount',0);
                            }
                            component.set('v.initialTableShow',true);
                            console.log('results::::initialTableShow:::test:::',component.get('v.initialTableShow'));
                            this.addPlanNames(component, results.WindowGuideline);
                        } 
                        else {
                            component.set('v.windowData', []);
                            //LRCC-1459
                            if (component.find('windowGuidelineSearch')) {
                                component.find('windowGuidelineSearch').reConstruct();
                                component.set('v.initialTableShow',true);
                            }
                        }
                    } else if (state === 'ERROR') {
                        component.set('v.windowData', []);
                        //LRCC-1459
                        if (component.find('windowGuidelineSearch')) {
                            component.find('windowGuidelineSearch').reConstruct();
                            component.set('v.initialTableShow',true);
                        }
                    }
                });
                $A.enqueueAction(action);
            } else {
                //LRCC-1012 - commented below code for this ticket.
                //component.set('v.windowData', []);
                //LRCC-1459
                //if (component.find('windowGuidelineSearch')) {
                    //component.find('windowGuidelineSearch').reConstruct();
                //}
                helper.errorToast('At least one criteria must be provided before a search is performed.');
            }
        } catch(e) {
            console.log(e);
        }
    },

    collectIds : function(selectedResults) {
        var collectedIds = [];
        
        if(selectedResults && selectedResults.length > 0) {
            for(var result of selectedResults) {
                collectedIds.push(result.id);
            }
        }
        
        return collectedIds;
    },
    collectNames : function(selectedResults) {
        var collectedNames = [];
        
        if(selectedResults && selectedResults.length > 0) {
            for(var result of selectedResults) {
                collectedNames.push(result.title);
            }
        }
        
        return collectedNames;
    },
    showToast : function(type,message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "type": type,
            "message": message
        });
        toastEvent.fire();
    },
    addPlanNames : function(component, windowGuidelines) {
    		
        var windowGuidelineIds = [];
        for(var windowGuideline of windowGuidelines) {
            
            windowGuidelineIds.push(windowGuideline.Id);
        	windowGuideline.link = '/one/one.app?sObject/' + windowGuideline.Id + '/view#/sObject/' + windowGuideline.Id + '/view';
            
            //LRCC-1560 - Replace Customer lookup field with Customer text field
            /*if (windowGuideline.Customer__r) {
                windowGuideline.customerName = windowGuideline.Customer__r.Name;
                windowGuideline.Customer__c = windowGuideline.Customer__r.Name;
            }**/
            if (windowGuideline.Customers__c) {
                windowGuideline.customerName = windowGuideline.Customers__c;
                //windowGuideline.Customer__c = windowGuideline.Customer__r.Name;
            }
            //LRCC-1493
            windowGuideline.Name = windowGuideline.Window_Guideline_Alias__c;
        }
        
        if(windowGuidelineIds.length > 0) {
        
            var action = component.get("c.getRelatedPlanGuidelines");
            action.setParams({"windowGuidelineIds": windowGuidelineIds});
            
            action.setCallback(this, function(response) {
                var state = response.getState();
                var results = response.getReturnValue();
                
                if(state === 'SUCCESS' && results) {
                    
                    for(var windowGuideline of windowGuidelines) {
                    	
                    	var filteredResults = results.filter(result => result.Window_Guideline__c == windowGuideline.Id);
                        
                        if(filteredResults.length > 0) {
                            
                            //var resultNames = "";
                            for(var filteredResult of filteredResults) {
                                //resultNames += filteredResult.Plan__r.Name + ', ';
                                windowGuideline.planName = filteredResult.Plan__r.Name;
                                windowGuideline.planStatus = filteredResult.Plan__r.Status__c;
                                windowGuideline.planVersion = filteredResult.Plan__r.Version__c;
                                windowGuideline.planSalesRegion = filteredResult.Plan__r.Sales_Region__c;
                                windowGuideline.planReleaseType = filteredResult.Plan__r.Release_Type__c;
                            }
                            //windowGuideline.planName = resultNames.substr(0,resultNames.length-1);
                        }
                    }
                    //rechecks any selected rows when infinite scrolling reloads everything
			        component.get('v.selectedRows').length > 0 ? this.recheckAllSelectedRows(component, windowGuidelines) : null;
			        component.set('v.windowData', windowGuidelines);
                    //LRCC-1459
                    if (component.find('windowGuidelineSearch')) {
                        
                        component.find('windowGuidelineSearch').reConstruct();
                    }
			    	component.set('v.isEditable',false);
			    	component.set('v.isChecked',false);
			    	this.hideSpinner(component);
	    		} 
                else if(state === 'ERROR') {
                    console.log(response.getError());
                    this.hideSpinner(component);
                }
            });
            $A.enqueueAction(action);
        }
        else {
    		//rechecks any selected rows when infinite scrolling reloads everything
	        component.get('v.selectedRows').length > 0 ? this.recheckAllSelectedRows(component, windowGuidelines) : null;
	        component.set('v.windowData', windowGuidelines);
            //LRCC-1459
            if (component.find('windowGuidelineSearch')) {
                component.find('windowGuidelineSearch').reConstruct();
            }
	    	component.set('v.isEditable',false);
	    	component.set('v.isChecked',false);
            this.hideSpinner(component);
	    }
        return windowGuidelines;
    },

    // Check all previously checked checkboxes on rerender
    recheckAllSelectedRows : function(component, results) {

        let selectedRows = component.get('v.selectedRows');
        console.log('selectedRows: ' + selectedRows);
        console.log('results: ' + results);

        for(let result of results) {
            if(selectedRows.includes(result.Id)) {
                result.isChecked = true;
            }
        }
	},

    showSpinner : function(component) {
        component.set('v.showSpinner', true);
    },
    
    hideSpinner : function(component) {
        component.set('v.showSpinner', false);
    },
    
    //LRCC-1236
    collectCustomers: function(selectedResults) {
        console.log('selectedResults::::',selectedResults);
        var customerNames = [];
        for(var result of selectedResults) {
            var customerName = 	result.title.split(':');
            if(customerName != null) {
                 customerNames.push(customerName[1].trim().toLowerCase()); //LRCC-1667
            }
        }
        console.log('customerNames::::',customerNames);
        return customerNames;
    },
})