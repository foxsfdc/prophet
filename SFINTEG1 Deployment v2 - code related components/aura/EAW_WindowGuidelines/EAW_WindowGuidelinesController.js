({
    searchData: function(component, event, helper) {
        component.set('v.clearRowsFlag',true);
        component.set('v.numRowsDisplayed', 50);
        component.set('v.scrollY', 0);
        component.set('v.selectedRows', []);
        //LRCC-1459
        let selectedFilterMap = null;
        if(event.getParam('selectedFilterMap') && event.getParam('selectedFilterMap').selectedCol != '' && event.getParam('selectedFilterMap').selectedAlpha != ''){
            selectedFilterMap = JSON.stringify(event.getParam('selectedFilterMap'));
        }
        console.log('window:::Guide::::::::',event.getParam('currentPageNumber'),event.getParam('pagesPerRecord'),JSON.stringify(component.get("v.pageIdMap")));
        
        //LRCC-1459
        if(event.getParam('currentPageNumber') && event.getParam('pagesPerRecord')){
            
            helper.search(component, event, helper,event.getParam('pagesPerRecord'),event.getParam('currentPageNumber'),JSON.stringify(component.get("v.pageIdMap")),selectedFilterMap);
        } else {
            //LRCC-1795
            helper.search(component, event, helper,50,1,JSON.stringify({}));
        }
        //helper.search(component, event, helper);
       
    },
	clearSearch: function(component,event,helper) {
        helper.clearSearch(component);
    },
    createRecord : function (component, event, helper) {
    	//LRCC-1556
    	component.set('v.modalpopup', true);
    }
})