({
	setLookup : function(component, event, helper) {
        if(component.find('titleReleaseDateResults') && component.find('titleReleaseDateResults').get('v.resultColumns')){
            
            var params = event.getParam('arguments');
            var param1 = params.childrefresh;
            console.log('param1:::tresult::',param1);
            component.find('titleReleaseDateResults').setlookup(param1);
        }
	},
	 //LRCC-1459
     
	reConstruct : function(component, event, helper) {
        if(component.find('planWindowResults')){
            var params = event.getParam('arguments');
            var param1 = params.childrefresh;
            console.log('param1:::tresult::',param1);
            component.find('planWindowResults').reConstruct(param1);
        }
	},
    //LRCC:1019
    checkChanges: function(component, event, helper) {
       
        if(component.find('titleReleaseDateResults')){
            
			return component.find('titleReleaseDateResults').checkChanges();
        }else {
            
            return false;
        }
	},
    checkForChange: function(cmp, event, helper) {
        var isChange = event.getParam("isChange");
        var name = event.getParam('cmpName');
        var unsaved = cmp.find("unsaved");
        if(isChange){
            unsaved.setUnsavedChanges(true, { label: name });
        } else{
            unsaved.setUnsavedChanges(false);
        }
    },
    saveResults : function(cmp,event,helper){
    	if(cmp.get('v.isDateSearch') && cmp.find('titleReleaseDateResults')){
    		cmp.find('titleReleaseDateResults').unSavedSave();
    	} else if(cmp.find('planWindowResults')){
    		cmp.find('planWindowResults').unSavedSave();
    	}
    }
})