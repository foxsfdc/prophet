({
    collectIds : function(selectedResults) {
        let collectedIds = [];

        if(selectedResults && selectedResults.length > 0) {
            for(let result of selectedResults) {
                if(result.id) {
                    collectedIds.push(result.id); // NOTE THIS IS LOWERCASE "i"
                } else {
                    collectedIds.push(result.Id); // NOTE THIS IS UPPERCASE "I'
                }
            }
        }

        return collectedIds;
    }
})