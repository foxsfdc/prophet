({
    applyLatest : function(component, event, helper){

        // // We're passed in a list of Window Ids
        let planItem = component.get('v.titlePlan') == null ? [] : component.get('v.titlePlan');
        let windowIds = helper.collectIds(planItem.Windows);
        console.log(planItem);
        console.log(windowIds);

        let action = component.get("c.applyLatestVersion");
        action.setParams({
            'windowIds' : windowIds,
            'planId' : planItem.TitlePlan.Id
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            let results = response.getReturnValue();

            if (state === 'SUCCESS' && results != null) {

                console.log(results);

                let displayData = component.get('v.displayData') == null ? [] : component.get('v.displayData');
                let planResult = results.plan == null ? '' : results.plan;
                let windowResults = results.windows == null ? [] : results.windows;

                // We don't need to update Plans since the only field we display from it is the name
                // but code is commented out below just in case
                // TODO: uncomment this an update as needed if we need title data
                // if(planResult !== '') {
                //     displayData.map(item => {
                //         if(item.TitlePlan.Id === planResult.Id) {
                //             item.TitlePlan.Name = planResult.Name;
                //         }
                //     });
                // }

                // Find the Windows and Update them
                if(windowResults.length > 0) {
                    displayData.map(item => {
                        item.Windows.map(window => {
                            let windowIndex = windowResults.findIndex(x => x.Id === window.Id);
                            if(windowIndex !== -1) {
                                // Set the window data
                                window.Start_Date__c = windowResults[windowIndex].Start_Date__c ;
                                window.End_Date__c = windowResults[windowIndex].End_Date__c;
                                window.Tracking_Date__c = windowResults[windowIndex].Tracking_Date__c;
                                window.Outside_Date__c = windowResults[windowIndex].Outside_Date__c;
                                window.Media__c = windowResults[windowIndex].Media__c;
                                window.Window_Type__c = windowResults[windowIndex].Window_Type__c;
                            }
                        });
                    });
                }

                // Set the data so the UI updates
                component.set('v.displayData', displayData);

            } else if (state === 'ERROR') {
                console.log(response.getError());
            }
        });
        $A.enqueueAction(action);

        // Close popup when we're done
        document.getElementById("EAW_TitlePlanWindowVersionUpdatePopup").style.display = "none";
    },

    /** Methods below hide/show the popup*/
    showPopup : function(component, event, helper) {
        document.getElementById("EAW_TitlePlanWindowVersionUpdatePopup").style.display = "block";
    },
    hidePopup : function(component, event, helper){
        document.getElementById("EAW_TitlePlanWindowVersionUpdatePopup").style.display = "none";
    }
})