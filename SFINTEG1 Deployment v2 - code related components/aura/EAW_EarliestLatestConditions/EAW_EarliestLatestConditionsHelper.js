({
    setParentNode : function(cmp,event,helper,nodeCalc) {
        //nodeCalc = JSON.parse(JSON.stringify(nodeCalc));
        var addedNodeRuleDetail = cmp.get('v.addedNodeRuleDetail') || [];
        var ruleDetailList = cmp.get('v.ruleDetailList');
        var childRuleDetails = [];
        for(var i=0;i<ruleDetailList.length;i++){
            if(nodeCalc.ruleDetail.Id == ruleDetailList[i].Parent_Rule_Detail__c){
                if(addedNodeRuleDetail.indexOf(ruleDetailList[i]) == -1){
                    addedNodeRuleDetail.push(ruleDetailList[i]);
                    if(childRuleDetails.indexOf(ruleDetailList[i]) == -1)childRuleDetails.push(ruleDetailList[i]);
                }
            }
        }
        console.log('::::childRuleDetails::::'+JSON.stringify(childRuleDetails));
        if(childRuleDetails.length){
            if(nodeCalc.ruleDetail.Parent_Condition_Type__c == 'Condition'){
                var trueBased =  childRuleDetails.find(function(element) {
                    return element.Condition_Type__c == 'If';
                });
                var falseBased = childRuleDetails.find(function(element) {
                    return element.Condition_Type__c == 'Else/If';
                });
                console.log(trueBased,falseBased)
                helper.setConditionBasedNode(cmp,event,helper,trueBased,falseBased,nodeCalc);
                
                cmp.set('v.selectedContext','condition');
            } else{
                var whenBasedList = childRuleDetails;
                var otherBased = whenBasedList.find(function(element) {
                    return element.Condition_Type__c == 'Otherwise';
                });
                console.log(otherBased);
                if(otherBased){
                    whenBasedList.splice(whenBasedList.indexOf(otherBased),1);
                }
                var caseNodeList = helper.setNodeRuleDetailForCase(cmp,event,helper,whenBasedList,cmp.get('v.ruleDetailList'));
                if(otherBased){
                    otherBased = helper.setNodeRuleDetailForCase(cmp,event,helper,[otherBased],cmp.get('v.ruleDetailList'));
                } else{
                    otherBased = [otherBased];
                }
                console.log('otherBased:::::',otherBased)
                helper.setCaseBasedNode(cmp,event,helper,caseNodeList,otherBased[0],nodeCalc);
                cmp.set('v.selectedContext','multidate');
            }
        } else {
            if(nodeCalc.ruleDetail.Parent_Condition_Type__c == 'Condition'){
                cmp.set('v.selectedContext','condition');
            } else{
                cmp.set('v.selectedContext','multidate');
            }
        }
    },
    setConditionBasedNode : function(cmp,event,helper,trueBased,falseBased,nodeCalc) {
        
        var trueBasedRuleDetailObject = JSON.parse(JSON.stringify(cmp.get('v.ruleDetailObject')));
        trueBasedRuleDetailObject.Rule_Order__c = 1;
        trueBasedRuleDetailObject.Nest_Order__c = 1;
        
        trueBased = trueBased || trueBasedRuleDetailObject;
        var trueNewRuleDetailJSON = {
            'ruleDetail' : trueBased,
            'dateCalcs' : [],
            'nodeCalcs' : []                
        };   
        var falseBasedRuleDetailObject = JSON.parse(JSON.stringify(cmp.get('v.ruleDetailObject')));
        falseBasedRuleDetailObject.Rule_Order__c = 2;
        falseBasedRuleDetailObject.Nest_Order__c = 1;
        
        falseBased = falseBased || falseBasedRuleDetailObject;
        
        var falseNewRuleDetailJSON  = {
            'ruleDetail' : falseBased,
            'dateCalcs' : [],
            'nodeCalcs' : []                
        }; 
        var conditionBased = {
            'TrueBased' : {'ruleOrder' : 1,
                           'ruleDetailJSON' :  trueNewRuleDetailJSON},
            'FalseBased' : {'ruleOrder' : 2,
                            'ruleDetailJSON' : falseNewRuleDetailJSON}
        };
        cmp.set('v.conditionBasedMap',conditionBased);
        console.log(':;;;end-nodeCalc::::'+JSON.stringify(nodeCalc));
    },
    setCaseBasedNode : function(cmp,event,helper,whenBasedList,otherBased,nodeCalc) {
        
        var newRuleDetail = JSON.parse(JSON.stringify(cmp.get('v.ruleDetailObject')));
        newRuleDetail.Rule_Order__c = 1;
        newRuleDetail.Nest_Order__c = 1;
        newRuleDetail.Condition_Type__c = 'Case';
        
        var newRuleDetailJSON = {
            'ruleDetail' : newRuleDetail,
            'dateCalcs' : [],
            'nodeCalcs' :  []               
        };   
        newRuleDetail = JSON.parse(JSON.stringify(newRuleDetail));
        newRuleDetail.Nest_Order__c = 2;
        var nodeRuleDetail = {
            'ruleDetail' : newRuleDetail,
            'dateCalcs' : [],
            'nodeCalcs' :  [] 
        }
        newRuleDetailJSON.nodeCalcs = [nodeRuleDetail];
        whenBasedList = whenBasedList || [JSON.parse(JSON.stringify(newRuleDetailJSON))];
        otherBased = otherBased || JSON.parse(JSON.stringify(newRuleDetailJSON));
      
        var caseRuleDetailMap = {
            'whenBased' : whenBasedList ,
            'otherBased' : otherBased
        }
        cmp.set('v.caseRuleDetailMap',caseRuleDetailMap);
       // console.log('caseRuleDetailMap::',caseRuleDetailMap);
    },
    setNodeRuleDetailForCase : function(cmp,event,helper,basedNode,ruleDetailList){
        var basedList = [];
        for(var i=0;i<basedNode.length;i++){
            var trueNewRuleDetailJSON = {
                'ruleDetail' : basedNode[i],
                'dateCalcs' : [],
                'nodeCalcs' : []                
            };  
            basedList.push(trueNewRuleDetailJSON);
        }
        console.log(basedList);
        return basedList;
    },
})