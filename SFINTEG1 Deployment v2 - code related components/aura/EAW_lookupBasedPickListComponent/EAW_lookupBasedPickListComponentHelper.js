({
	setpickListValues : function(cmp,objectList) {
        var pickListValues = [];
        for(var i in objectList){
            var temp = {};
            temp.label = objectList[i].Name;
            temp.value = objectList[i].Name;
            pickListValues.push(temp);
        }
        cmp.set('v.objectRecordsValues',pickListValues);
	},
})