({
    //LRCC-1528
    openModal : function(cmp, event, helper) {
        $A.util.addClass(cmp.find("modal"),' slds-fade-in-open');
        $A.util.addClass(cmp.find("modal-backdrop"),' slds-backdrop--open');
    },
    //LRCC-1528
    closeModal : function(cmp, event, helper) {
        $A.util.removeClass(cmp.find("modal"),' slds-fade-in-open');
        $A.util.removeClass(cmp.find("modal-backdrop"),' slds-backdrop--open');
    }
})