({
    init : function(component, event, helper) {
        
        var action = component.get('c.getObjectNames');
        var isReleaseDateResults = component.get('v.isReleaseDateResults');
        var objects = isReleaseDateResults ? ['EDM_REF_PRODUCT_TYPE__c'/*,'Title Tag'*/,'Release Date Tag'] : ['EDM_REF_DIVISION__c','EDM_REF_PRODUCT_TYPE__c','License_Info_Codes__c'/*,'Title Tag'*/,'Window Tag'];
        
        action.setParams({
            'objects' : objects
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state == 'SUCCESS') {
                var records = response.getReturnValue();
                console.log(';;;;;',JSON.parse(JSON.stringify(records)));
                if(! isReleaseDateResults) {
                    component.set('v.allInfoCodes', records.License_Info_Codes__c.picklistValues);                
                }
                var columns = JSON.parse(JSON.stringify(component.get('v.resultColumns')));        
                for(let col of columns) {
                    console.log('col.fieldName;;;;;',col.fieldName);
                    if(col.fieldName == 'Name') {
                        col.fieldName = 'Title__r.Name';
                    } else if(col.fieldName == 'titleName') {
                        col.fieldName = 'EAW_Plan__r.EAW_Title__r.Name';
                    } else if(col.fieldName == 'edmTitleFinProdId') {
                        col.fieldName = 'Title__r.FIN_PROD_ID__c';
                    } else if(col.fieldName == 'FIN_PROD_ID__c') {
                        col.fieldName = 'EAW_Plan__r.EAW_Title__r.FIN_PROD_ID__c';
                    } else if(col.fieldName == 'windowGuidelineName') {
                        col.fieldName = 'EAW_Window_Guideline__r.Window_Guideline_Alias__c';
                    } else if(col.fieldName == 'planGuidelineName') {
                        col.fieldName = 'EAW_Plan__r.Plan_Guideline_Name__c';
                    } else if(col.fieldName == 'PlanName') {
                        col.fieldName = 'EAW_Plan__r.Name';
                    } else if(col.fieldName == 'edmTitleIntlProdType') {
                        col.fieldName = 'Title__r.PROD_TYP_CD_INTL__r.Name';
                        col.type = 'picklist';
                        col.picklistValues = helper.collectNames(records.EDM_REF_PRODUCT_TYPE__c.sObjectRecords, false, false);
                    } else if(col.fieldName == 'PROD_TYP_CD_INTL__c') {
                        col.fieldName = 'EAW_Plan__r.EAW_Title__r.PROD_TYP_CD_INTL__r.Name';
                        col.type = 'picklist';
                        col.picklistValues = helper.collectNames(records.EDM_REF_PRODUCT_TYPE__c.sObjectRecords, false, false);
                    } else if(col.fieldName == 'FIN_DIV_CD__c') {
                        col.fieldName = 'EAW_Plan__r.EAW_Title__r.FIN_DIV_CD__r.Name';
                        col.type = 'picklist';
                        col.picklistValues = helper.collectNames(records.EDM_REF_DIVISION__c.sObjectRecords, false, false);
                    } else if(col.fieldName.includes('Tag') && col.fieldName != 'titleTags' && col.fieldName != 'TitleTags') {
                        col.type = 'picklist';
                        var label = col.label.replace('(s)','');
                        label = label.split(' ').join('');
                        col.picklistValues = helper.collectNames(records[label].sObjectRecords, true, false);
                    } else if(col.fieldName == 'Retired__c' || col.fieldName == 'Is_Confidential_Release_Date__c' || col.fieldName == 'Is_Confidential_Window__c') {
                        col.type = 'picklist';
                        col.picklistValues = [{'label' : 'Any', 'value' : ''},
                                             {'label' : 'Yes', 'value' : 'true'},
                                             {'label' : 'No', 'value' : 'false'}];
                    //} else if(col.fieldName == 'License_Info_Codes__c') {
                        //col.type = 'picklist';
                        //col.picklistValues = helper.collectNames(records.License_Info_Codes__c.picklistValues, false, true);
                    } else if(col.fieldName == 'Start_Date_Text__c') {
                        col.picklistValues = [{label: "TBD", value: "TBD"}];
                    } else if(col.fieldName == 'End_Date_Text__c') {
                        col.picklistValues = [{label: "TBD", value: "TBD"},
                                             {label: "Perpetuity", value: "Perpetuity"},
                                             {label: "Rights End Date", value: "Rights End Date"}];
                    }
                    
                    if(col.type == 'picklist' && col.fieldName != 'Retired__c' && col.fieldName != 'Is_Confidential_Release_Date__c' && col.fieldName != 'Is_Confidential_Window__c') {
                        col.picklistValues.splice(0, 0, {'label' : 'All', 'value' : ''});
                    }
                }        
                helper.setFilterActions(columns);
                console.log(';;;;;;',JSON.parse(JSON.stringify(columns)));
                component.set('v.inputColumns',columns);     
                
            }
        });
        $A.enqueueAction(action);
    },
    
    handlechange : function(component, event, helper) {
        
        var col = event.getSource().get('v.name');        
        var inputColumns = component.get('v.inputColumns');
        
        for(let column of inputColumns) {
            
            if(column.fieldName == col) {
                column.selectedAction = event.getParam("value");
            }
        }
        component.set('v.inputColumns', inputColumns);
        helper.applyFilter(component, event);
    },
    
    handleClear : function(component, event, helper) {
        
        var fieldName = event.getSource().get('v.name');        
        var allFilterElements = component.find('filterElement');
        var applyFilter = false;
        
        for(let filterElement of allFilterElements) {
            
            var selectedCol = filterElement.get('v.label') ? filterElement.get('v.label').fieldName : filterElement.get('v.infoCodeObject').fieldName;
            
            if(selectedCol == fieldName && selectedCol != 'License_Info_Codes__c') {
                filterElement.set('v.value', '');
                applyFilter = true;                
            } else if(selectedCol == 'License_Info_Codes__c') {
                filterElement.set('v.value', '');
                filterElement.set('v.selectedList', []);
            }          
        }
        if(applyFilter) {
            helper.applyFilter(component, event);
        }
    },
    
    applyFilter : function(component, event, helper) {
        
        if (event.which == 13){
            helper.applyFilter(component, event);
        }
    },
    
    onSelect : function(component, event, helper) {
        
        if (event.getSource().get('v.label').type == 'date' || event.getSource().get('v.label').type == 'picklist'){
            helper.applyFilter(component, event);
        }
    },
    
    infoCodesChange : function(component, event, helper) {
        console.log('infoCodesChange');
        helper.applyFilter(component, event);
    }
})