({
    init : function(cmp, event, helper) {
        
        window.setTimeout(function() {
            cmp.set('v.spinner',false);
            cmp.set('v.isSaveAndNew',false);
        },2000);
	},
    
    handleSubmit : function(component, event, helper) {
        
        event.preventDefault(); //stop form submission
        var eventFields = event.getParam("fields");
        
        //LRCC-1556
        if(!$A.util.isEmpty(component.get('v.productType'))){
            eventFields['Product_Type__c'] = component.get('v.productType');
            component.find('recordForm').submit(eventFields);	//LRCC-1469
        }
        else if($A.util.isEmpty(component.get('v.productType'))) {
            helper.toast('Error', 'Please select product type value it is mandatory')
        }
    },
    
    saveAndNew : function(cmp,event) {
        cmp.set('v.isSaveAndNew',true);
    },
    
    handleSuccess : function(cmp,event,helper) {
        
        var windowGuidelineId = event.getParams().response.id;
        
        //LRCC-1864
        var action = cmp.get("c.updateOriginWindowGuidelineId");
        action.setParams({
            'recordId': windowGuidelineId
        });
        action.setCallback(this, function (response) {
            
            let state = response.getState();
            console.log('state:::',state);
            if(state == 'SUCCESS') {
                helper.closeModal(cmp);
                var cmpEvent = cmp.getEvent("cmpEvent");
                cmpEvent.setParams({
                    "windowGuidelineId" : windowGuidelineId,
                    "isSaveAndNew" : cmp.get('v.isSaveAndNew')
                });
                cmpEvent.fire();
            } 
            else if (state == 'ERROR') {
                let errors = response.getError();
                let handleErrors = helper.handleErrors(errors, cmp, event, helper);
                if(handleErrors) {
                    helper.toast('error',handleErrors);
                }
            }
        });
        $A.enqueueAction(action);	
    },
    
    openModal : function(cmp) {
        $A.util.addClass(cmp.find('modal'),'slds-fade-in-open');
        $A.util.addClass(cmp.find('backDrop'),'slds-backdrop_open');
        cmp.set('v.showModal',true);
    },
    
    closeModal : function(cmp,event,helper){
        cmp.set('v.showModal',false);
        helper.closeModal(cmp);
    }
})