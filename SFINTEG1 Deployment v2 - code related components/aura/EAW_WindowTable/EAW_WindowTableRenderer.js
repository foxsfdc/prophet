({
    rerender : function (component, helper) { // LRCC - 1282
        this.superRerender();
        window.setTimeout($A.getCallback(function(){
            helper.setBodyWidth(component);
        },300));
  
    },
	afterRender : function(component,helper){
    	/*let ele = component.find('tableElement').getElement();
    	  ele.addEventListener("scroll", function(evt) {
    		  // not the most exciting thing, but a thing nonetheless
    		  let theadElement = component.find('theadELement').getElement();
    		  theadElement.style.left = -(evt.target.scrollLeft) + 'px';
              console.log('::::::theadElement:::'+theadElement);
    		  let checkElement = component.find('checkElement').getElement();
    		  checkElement.style.left = evt.target.scrollLeft + 'px'
    		  
              let dailogIcons = component.find('dailogIcons').getElement();
    		  dailogIcons.style.left = (evt.target.scrollLeft) + 'px';
              
    		  let thElement = component.find('thElement');
    		  thElement.map((ele)=>{
    			  let elementth = ele.getElement();
    			  if(elementth.getAttribute('data-colIndx') == '0' ){
    				  elementth.style.left = evt.target.scrollLeft + 'px';
    			  }
    			  if(elementth.getAttribute('data-colIndx') == '1'){
    				  elementth.style.left = evt.target.scrollLeft + 'px';
    			  }
    		  });
    		  
    		  var tableColumn = component.find('tableColumn');
    		  
    		  if(tableColumn && !Array.isArray(tableColumn)){
    		    tableColumn = [tableColumn];
    		  }
           /* if(tableColumn && tableColumn.length > 0){	
                tableColumn.map((ele)=>{
                    let elementtdcmp = ele;
                    if(elementtdcmp.find('inputTableID') && elementtdcmp.find('inputTableID').length > 0){
                    elementtdcmp.find('inputTableID')[0].find('itdEle').getElement().style.left = evt.target.scrollLeft + 'px';
                    elementtdcmp.find('inputTableID')[1].find('itdEle').getElement().style.left = evt.target.scrollLeft + 'px';
                }
                });
            }	 
                 var tdcheck = component.find('tdcheck');
            if(tdcheck ){
                if(tdcheck.length > 0){
                    tdcheck.map((ele)=>{
                        ele.getElement().style.left = evt.target.scrollLeft + 'px';
                	});
                } else{
                     tdcheck.getElement().style.left = evt.target.scrollLeft + 'px';    
                }
           }
           var tddailogs = component.find('tddailogs');
           if(tddailogs && !Array.isArray(tddailogs)){
           		tddailogs = [tddailogs];
            }
             if(tddailogs && tddailogs.length > 0 ){
                tddailogs.map((ele)=>{
                     ele.getElement().style.left = evt.target.scrollLeft + 'px';
                });
            }
		});*/
        helper.windowClick = $A.getCallback(function(event){
            if(component.isValid()){
            	helper.setBodyWidth(component);
            }
        });
        window.addEventListener('resize',helper.windowClick);
                        
    	window.setTimeout($A.getCallback(function(){
            component.set('v.setIcons',true);
        }),500);
    }
})