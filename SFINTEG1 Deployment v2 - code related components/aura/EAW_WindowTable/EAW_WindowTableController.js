({
	resetColumn: function(component, event, helper) {
        let currentEle = component.get('v.currentEle');

        if(component.get("v.currentEle") !== null) {
            let newWidth = component.get("v.newWidth");
            let currentEleParent = currentEle.parentNode.parentNode;
            let parObj = currentEleParent.parentNode;

            parObj.style.width = newWidth + 'px';
            currentEleParent.style.width = newWidth + 'px';

            component.get("v.currentEle").style.right = 0;
            component.set("v.currentEle", null);
        }
    },
    setNewWidth: function(component, event, helper) {

        /*let currentEle = component.get("v.currentEle");

        if(currentEle != null && currentEle.tagName) {
            let parObj = currentEle;
			
            while(parObj.parentNode.tagName != 'TH') {
                if(parObj.className == 'slds-resizable__handle') {
                    currentEle = parObj;
                }
			    parObj = parObj.parentNode;
            }

            let mouseStart = component.get("v.mouseStart");
            let oldWidth = parObj.offsetWidth;
            let newWidth = oldWidth + (event.clientX - parseFloat(mouseStart));

            component.set("v.newWidth", newWidth);
            currentEle.style.right = ( oldWidth - newWidth ) + 'px';
            component.set("v.currentEle", currentEle);
        }*/
        var currentEle = component.get("v.currentEle");
        if( currentEle != null && currentEle.tagName ) {
            var parObj = currentEle;
            while(parObj.parentNode.tagName != 'TH') {
                if( parObj.className == 'slds-resizable__handle')
                    currentEle = parObj;    
                parObj = parObj.parentNode;
                count++;
            }
            var count = 1;
            var mouseStart = component.get("v.mouseStart");
            var oldWidth = parObj.offsetWidth;  // Get the width of DIV
            var newWidth = oldWidth + (event.clientX - parseFloat(mouseStart));
            component.set("v.newWidth", newWidth);
            currentEle.style.right = ( oldWidth - newWidth ) +'px';
            component.set("v.currentEle", currentEle);
            if(currentEle.id){ // LRCC - 1282
                var myColumns = component.get('v.myColumns');
                for(var col of myColumns){
                    if(col.fieldName == currentEle.id){
                        console.log(currentEle.id,':::current::',oldWidth - newWidth);
                        col.width = newWidth;
                    }
                }
                component.set('v.myColumns',myColumns);
                component.set('v.bodyWidth',((component.get('v.myColumns').length + 1) * 140 ) + newWidth );
            }
        }
    },
    calculateWidth: function(component, event, helper) {
        let childObj = event.target;
        let mouseStart = event.clientX;

        component.set("v.currentEle", childObj);
        component.set("v.mouseStart", mouseStart);

        if(event.stopPropagation){
            event.stopPropagation();
        }

        if(event.preventDefault){
            event.preventDefault();
        }

        event.cancelBubble = true;
        event.returnValue = false;
    },
    sort: function(component, event, helper) {
        
        //LRCC-1517
        if(component.get("v.buttonstate")){
            helper.showErrorToast(component,'Filter Search Option is Enabled');
            return;
        } 
        
        let sortVal = event.currentTarget.dataset.value;
        let sortIdx = event.currentTarget.dataset.idx;
        //LRCC-1690
        component.set('v.selectedSortingParam',{'sortVal':sortVal,'sortIdx':sortIdx});
        console.log('sortparam::',JSON.stringify(component.get('v.selectedSortingParam')));
        helper.sortBy(component, sortVal, sortIdx,false);
        
        //LRCC-1700 - pagination
        component.set('v.colWithAlpha',{'selectedCol':null,'selectedAlpha':null});	
        var target = event.currentTarget;
        var rowIndex = target.getAttribute("data-colIndex");
        console.log("Row No ::: " + rowIndex);
        console.log('::: selected :::', JSON.stringify(component.get("v.resultColumns")[rowIndex]));
        
        
        //LRCC-1012-- add condition for PROD_TYP_CD_INTL__c,FIN_PROD_ID__c
        if((component.get("v.resultColumns")[rowIndex].fieldName != 'PROD_TYP_CD_INTL__c')&&
           (component.get("v.resultColumns")[rowIndex].fieldName != 'FIN_PROD_ID__c')&&
           (component.get("v.resultColumns")[rowIndex].type == "string" || 
            component.get("v.resultColumns")[rowIndex].type == "picklist") && 
           ((component.get("v.resultColumns")[rowIndex].fieldName.indexOf("__c") > -1)||
            component.get("v.resultColumns")[rowIndex].fieldName == "Name")){
            
            var colAplpha = component.get('v.colWithAlpha');
            colAplpha.selectedCol = component.get("v.resultColumns")[rowIndex].fieldName;
            component.set('v.colWithAlpha',colAplpha)
        }else{
             helper.showErrorToast(component,'Selecting column is not valid for alphabet sort');
             component.set('v.colWithAlpha',{'selectedCol':null,'selectedAlpha':null});	
             return;
        }
         console.log('::: selected :::',JSON.stringify(component.get('v.colWithAlpha')));
        console.log('::: selected :::',JSON.stringify( component.get("v.resultColumns")));
        
         //end-LRCC-1459
        
        
    },
    checkAll : function(component, event, helper) {
    	
    	event.stopImmediatePropagation();
    	
        let selectAllIdx = event.getSource().get('v.name');
        let checkboxes = component.find('row-checkbox');
        let self = event.getSource().get('v.checked');
        let selectedRows  = component.get('v.selectedRows');
        
        //LRCC-1459
        let resultData =  component.get("v.rowsToDisplay");//component.get("v.resultData");
        console.log('resultData::::::',resultData);
        //resultData[selectAllIdx];
        let windows = resultData;
        
        console.log(windows,selectAllIdx);
        // add/delete each window from selectedRows
        if(Array.isArray(windows)) {
            for(let window of windows) {
                window.isChecked = self;
                if(self && !selectedRows.includes(window.Id)) {
                    selectedRows.push(window.Id);
                } else if(!self && selectedRows.includes(window.Id)) {
                    selectedRows.splice(selectedRows.indexOf(window.Id), 1);
                }
            }
        } else {
            windows.isChecked = self;
            if(self && !selectedRows.includes(windows.Id)) {
                selectedRows.push(windows.Id);
            } else if(!self && selectedRows.includes(windows.Id)) {
                selectedRows.splice(selectedRows.indexOf(windows.Id), 1);
            }
        }
		
        // check/uncheck each window
        if(Array.isArray(checkboxes)) {
            for(let check of checkboxes) {
                check.getElement().checked = self;
            }
        } else {
            checkboxes.getElement().checked = self;
        }
       //LRCC-1459
        component.set('v.rowsToDisplay',resultData);
        //component.set('v.resultData',resultData);
        component.set('v.selectedWindows', selectedRows);
    },
    updateSelectedRows : function(component, event, helper) {
        let selectedRows  = component.get('v.selectedRows');
        let self = event.target;
        
        if(self.checked) {
            selectedRows.push(self.value);
        } else {
            selectedRows.splice(selectedRows.indexOf(self.value), 1);
        }
        component.set('v.selectedWindows', selectedRows);
        
        var checkboxes = component.find("row-checkbox");
        var allselected = true;
        
        if(checkboxes){
            if(Array.isArray(checkboxes)) {
                for(let check of checkboxes){
                    var ele = check.getElement();
                    if(! ele.checked){
                        allselected = false;
                        break;
                    }
                }
            } else {
                var ele = checkboxes.getElement();
                if(! ele.checked){
                    allselected = false;
                }
            }
        }
        console.log('allselected:::',allselected);
        let headerCheckbox = component.find('header-checkbox');
        let headercheck = Array.isArray(headerCheckbox) ? headerCheckbox[0] : headerCheckbox;
        
        if(allselected) {
            headercheck.set('v.checked', true);
        } else {
            headercheck.set('v.checked', false);
        }
    },
    openWindowRuleModal : function(cmp,event,helper){ // LRCC - 1259
        var windowId = event.getSource().get('v.name');
        console.log(windowId);
        if(windowId && cmp.find('windowRuleModal')){
            cmp.find('windowRuleModal').openModal(windowId);
        }
    },
    openWindowStrandModal : function(cmp,event,helper){ // LRCC - 1259
        var windowId = event.getSource().get('v.name');
        console.log(windowId);
        if(windowId && cmp.find('windowStrandModal')){
            cmp.find('windowStrandModal').openModal(windowId);
        }
    },
    rowAction: function(component, event, helper) {
        
        var recId = event.getSource().get('v.name');
        var notesEvent = $A.get("e.c:EAW_NotesEvent");
        notesEvent.setParams({
            
            "parentId": recId
        });
        notesEvent.fire();
    },
    openHistoryModal : function(cmp,event,helper){
        var windowId = event.getSource().get('v.name');
        console.log(windowId);
        if(windowId && cmp.find('windowHistory')){
            cmp.find('windowHistory').openModal(windowId);
        }
    },//LRCC-1690
    toSort: function(component, event, helper){
        
        var selctsort = component.get('v.selectedSortingParam');
        console.log('tosort::',selctsort);
        if(selctsort){
          helper.sortBy(component, selctsort.sortVal, selctsort.sortIdx,true);  
        }
        
    },
    setBodyWidth : function(cmp,event,helper){ // LRCC - 1701
       // console.log(':::::::edit Width::')
        window.setTimeout(
            $A.getCallback(function() {
                helper.setBodyWidth(cmp);
            }),1000);
    }
    
})