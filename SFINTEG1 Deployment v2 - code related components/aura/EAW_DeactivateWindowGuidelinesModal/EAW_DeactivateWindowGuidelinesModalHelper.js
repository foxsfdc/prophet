({
	errorToast : function(message) {
    	var toastEvent = $A.get('e.force:showToast');
    		
		toastEvent.setParams({
			'title': 'Error!',
			'type': 'error',
			'message': message
		});
		
		toastEvent.fire();
    },
    
    successToast : function(message) {
    	var toastEvent = $A.get('e.force:showToast');
    		
		toastEvent.setParams({
			'title': 'Success!',
			'type': 'success',
			'message': message
		});
		
		toastEvent.fire();
    },
	
    hideDeactivateModal : function() {
        document.getElementById("deactivate-window-guideline-modal").style.display = "none";
	}
})