({
	deactivate : function(component, event, helper) {
		var windowData = component.get("v.windowData");
        var selectedWindows = component.get('v.selectedRows');
		var action = component.get("c.deactivateWindows");
        action.setParams({"windows": selectedWindows});

        action.setCallback(this, function(response) {
        	var state = response.getState();
                    
            if(state === 'SUCCESS') {
		        var results = response.getReturnValue();

		        for(var result of results) {
		        	result.link = '/one/one.app?sObject/'+ result.Id + '/view#/sObject/' + result.Id + '/view';
		            
		            var index = windowData.findIndex(item => item.Id == result.Id);
		            if(index != -1) {
		                windowData[index] = result;
		            }
		        }
		        
		    	component.set('v.windowData', windowData);
		        component.set('v.selectedRows', []);
		        helper.hideDeactivateModal();
		        helper.successToast('Title successfully deactivated.');
            } else {
            	helper.errorToast('There was an error while trying to deactivate this title.');
            }
        });
        
        $A.enqueueAction(action);
	},
    
    hideDeactivateModal : function(component, event, helper) {
        helper.hideDeactivateModal();
    }
})