({
    showExistingObjectList : function(genericSearchResults) {
        document.getElementById(genericSearchResults).style.display = "block";
    },
    
    hideExistingObjectList : function(genericSearchResults) {
        document.getElementById(genericSearchResults).style.display = "none";
    },
    
    showSpinner : function(component) {
        component.set('v.showLookupSpinner', true);
    },
    
    hideSpinner : function(component) {
        component.set('v.showLookupSpinner', false);
    }
})