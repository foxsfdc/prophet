({
	sortBy: function (component, field) {
        var sortAsc = component.get("v.sortAsc"),
            sortField = component.get("v.sortField"),
            records = component.get("v.boxOfficeData");
        sortAsc = field == sortField ? !sortAsc : true;
        records.sort(function (a, b) {
            var t1 = a[field] == b[field],
                t2 = a[field] > b[field];
            return t1 ? 0 : (sortAsc ? -1 : 1) * (t2 ? -1 : 1);
        });
        component.set("v.sortAsc", sortAsc);
        component.set("v.sortField", field);
        component.set("v.boxOfficeData", records);
        
        /****LRCC:887-Sorting indicator applied logic****/
        component.set("v.selectedTabsoft", field);
        if (sortAsc == true) {
            component.set("v.arrowDirection", 'arrowup');
        } else {
            component.set("v.arrowDirection", 'arrowdown');
        }
    },
    errorToast : function(message) {
        var toastEvent = $A.get('e.force:showToast');

        toastEvent.setParams({
            'title': 'Error!',
            'type': 'error',
            'message': message
        });

        toastEvent.fire();
    },
    successToast : function(message) {
        var toastEvent = $A.get('e.force:showToast');

        toastEvent.setParams({
            'title': 'Success!',
            'type': 'success',
            'message': message
        });

        toastEvent.fire();
    },
    readOnlyMode : function(component,event,helper){
    	component.set("v.checked",false);
        var checked = component.get("v.checked");
    	component.set('v.displayEditButtons', checked);
        
        var editTableEvent = $A.get("e.c:EAW_EditTableEvent");
        editTableEvent.setParams({"editMode":checked});
        editTableEvent.fire();
    }
})