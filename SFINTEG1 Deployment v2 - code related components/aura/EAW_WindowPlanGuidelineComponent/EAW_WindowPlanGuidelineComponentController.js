({
    init : function (component, event, helper) {
        
        let action = component.get("c.getPlanGuidelines");
        action.setParams({"recordId": component.get("v.recordId")});
        
        action.setCallback(this, function(response) {
            
            let state = response.getState();
			if (state === 'SUCCESS') {
                
                let results = response.getReturnValue();
                for (let result of results) {
                    result.link = '/lightning/r/sObject/'+ result.Id + '/view';
                }
                
                component.set('v.planGuidelineData', results);
            } else {
                helper.errorToast('There was an issue loading the Plan Guidelines, Please refresh the page to load them.');
            }
        });
        $A.enqueueAction(action);
        
        component.set('v.myColumns', [
            {label: 'Plan Guideline Name', fieldName: 'link', type: 'url', 
             typeAttributes: { label: {fieldName: 'Name' }, target: '_blank'}, sortable: true},
            {label: 'Plan Guideline Status', fieldName: 'Status__c', type: 'text'},
            {label: 'PG Version', fieldName: 'Version__c', type: 'number'},
            //{label: 'Territory', fieldName: 'Territory__c', type: 'text'},
            //{label: 'Language', fieldName: 'Language__c', type: 'text'},
            {label: 'Product Type', fieldName: 'Product_Type__c', type: 'text'},
            //{label: 'Release Type', fieldName: 'Release_Type__c', type: 'text'}
        ]);
    }
})