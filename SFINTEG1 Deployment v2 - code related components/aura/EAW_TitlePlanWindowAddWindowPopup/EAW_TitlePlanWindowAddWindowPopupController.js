({
    addWindow : function(component, event, helper) {
        let selectedTitles = component.get('v.selectedTitles');
        let selectedTitlePlans = component.get('v.selectedTitlePlans');
        let selectedWindowGuidelines = component.get('v.selectedWindowGuidelines');
        let title = component.get('v.title');
        let body = component.get('v.body');
        //LRCC-1459
        let pageNumber = component.get('v.pageNumber');
        let colWithAlpha = JSON.stringify(component.get('v.colWithAlpha'));
        let pageIdMapUI = component.get('v.pageIdMapUI')|| JSON.stringify({});

        console.log(selectedTitles)
        console.log(selectedTitlePlans)
        console.log(selectedWindowGuidelines)

        if(selectedWindowGuidelines.length === 1 && title != null && title != '') {
            
            let action = component.get('c.createWindow');
            action.setParams({titleId: selectedTitles[0],
                              planId: selectedTitlePlans[0],
                              windowGuidelineId: selectedWindowGuidelines[0].id,
                              title: title,
                              body: body,
                              pagesPerRecord:100,
                              currentPageNumber :pageNumber,
                              pageIdMapUI :pageIdMapUI,
                              selectedFilterMap : colWithAlpha
                             });

            action.setCallback(this, function (response) {
                let state = response.getState();

                console.log(state)

                if(state === 'SUCCESS') {
                    let results = response.getReturnValue();

                    console.log(results);

                    if(results && results != null) {
                        let resultData = component.get('v.resultData');
                        let titlePlans = [];

                        for(let key in results) {
                            let i;
                            let windows = [];

                            if(results[key].length > 1) {
                                for(i = 1; i < results[key].length; i++){
                                    let window = results[key][i];
                                    window.Title = results[key][0].Name;
                                    window.link = '/one/one.app?sObject/'+ window.Id + '/view#/sObject/' + window.Id + '/view';
                                    window.FIN_PROD_ID__c = results[key][0].FIN_PROD_ID__c;
                                    window.PROD_TYP_CD_INTL__c = results[key][0].CurrencyIsoCode;
                                    window.FIN_DIV_CD__c = results[key][0].DIR_NM__c;
                                    window.PlanName = results[key][0].Acquired_Indicator__c;
                                    window.TitleTags = results[key][0].Approx_Run_Time__c;
                                    window.isChecked = false;

                                    windows.push(window);
                                }

                                if(windows.length > 0) {
                                    results[key][0].TitleId = results[key][0].Id;
                                    results[key][0].Id = key;
                                    titlePlans.push({TitlePlan: results[key][0], Windows: windows, showWindows: false});
                                }
                            }
                        }

                        if(titlePlans.length > 0) {
                            for(let titlePlan of titlePlans) {
                                resultData.find(function(element) {
                                    if(element.TitlePlan.Id === titlePlan.TitlePlan.Id) {
                                        element.Windows.push(titlePlan.Windows[0]);
                                    }
                                });
                            }
                            component.set('v.resultData', resultData);
                        }
                        /**LRCC:787**/
                        let cmpEvent = component.getEvent("cmpEvent");
                        cmpEvent.setParams({
            				"titlePlanId" : component.get('v.titlePlanId')
            			});
                        cmpEvent.fire();
                        
                        helper.clearInputsAndHide(component);
                        helper.successToast('Window successfully added to the Title Plan.');
                    } else {
                        helper.errorToast('You can not add duplicate Windows to a Title Plan.');
                    }
            	}
                else if(state == 'ERROR') {
                	let errors = response.getError();
                	let handleErrors = helper.handleErrors(errors, component, event, helper);
                    if(handleErrors) {
                        helper.errorToast(handleErrors);
                    }
                } 
            	else {
            		helper.errorToast('There was an issue creating the plan, window, and window strands for the selected title plan.');
				}
            });
            $A.enqueueAction(action);
        } else {
            if(selectedWindowGuidelines.length === 0) {
            	helper.errorToast('You must select a Window Guideline to generate a Window for the Title Plan.');
            } else {
            	let label = $A.get("$Label.c.EAW_NotesSubjectValidation");
            	helper.errorToast(label);
            }
        }
    },

    hidePopup : function(component, event, helper) {
        helper.clearInputsAndHide(component);
    }
})