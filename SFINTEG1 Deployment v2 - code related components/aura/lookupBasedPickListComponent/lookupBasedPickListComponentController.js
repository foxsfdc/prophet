({
	init : function(cmp, event, helper) {
        
        cmp.set('v.spinner',true);
        event.stopPropagation();
        console.log(cmp.get('v.objectRecordsValues').length,cmp.get('v.objectName'));
        if(!cmp.get('v.objectRecordsValues').length){
            var action = cmp.get('c.getLookupRecords');
            action.setParams({
                'sObjectName' : cmp.get('v.objectName'),
                'whereCondition' : cmp.get('v.whereCondition')
            });
            action.setCallback(this,function(response){
                var state = response.getState();
                if(state == 'SUCCESS'){
                    console.log('objectRecordsValues:::',response.getReturnValue());
                    helper.setpickListValues(cmp,response.getReturnValue());
                } else if(state == 'ERROR'){
                    console.log('error::',response.getError()[0].message);
                }
                cmp.set('v.spinner',false);
            });
            $A.enqueueAction(action);
        }
	}
})