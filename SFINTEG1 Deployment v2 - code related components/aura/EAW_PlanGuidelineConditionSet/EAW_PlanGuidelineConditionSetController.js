({
	checkForValid : function(cmp, event, helper) {
		var inputCmp = cmp.find('inputCmp');
        if(!Array.isArray(inputCmp)){
            inputCmp = [inputCmp];
        }
        var valid = true;
        for(var i=0;i<inputCmp.length;i++){
            if(inputCmp[i].checkForValues()){
                valid = false;
            }
        }
        return valid;
	},
    init : function(cmp){
        var changedFieldset = JSON.parse(JSON.stringify(cmp.get('v.fieldSet')));
        if(cmp.get('v.fieldItems') && cmp.get('v.fieldItems').indexOf('Condition_Status__c') != -1){
            var operator = changedFieldset.find(function(element) {
                return element.fieldName == 'Condition_Status__c';
            });
            if(cmp.get('v.conditionField') == 'Line of Business'){
                operator.pickListValues = [
                    {label: "Current", value: "Current"},
                    {label: "Library", value: "Library"}
                ];
            } else{
                if(cmp.get('v.releaseDateStatusList').length){
                    operator.pickListValues = cmp.get('v.releaseDateStatusList');
                } else{
                   operator.pickListValues = [
                    {label: "Firm", value: "Firm"},
                    {label: "Estimated", value: "Estimated"},
                    {label: "Tentative", value: "Tentative"},
                    {label: "Not Released", value: "Not Released"}];
                }
            }
        }
        if(cmp.get('v.fieldItems') && cmp.get('v.fieldItems').indexOf('Condition_Operator__c') != -1){
            if(cmp.get('v.fieldItems').indexOf('Condition_Criteria_Amount__c') != -1 || cmp.get('v.fieldItems').indexOf('Condition_Date__c') != -1 || cmp.get('v.fieldItems').indexOf('Condition_Year__c') != -1){
                var operatorElement = changedFieldset.find(function(element) {
                    return element.fieldName == 'Condition_Operator__c';
                });
                operatorElement.pickListValues = [
                    {label: ">", value: ">"},
                    {label: "<", value: "<"},
                    {label: ">=", value: ">="},
                    {label: "<=", value: "<="},
                    {label: "=", value: "="}];
            } else if(cmp.get('v.fieldItems').indexOf('Condition_Status__c') != -1 || cmp.get('v.fieldItems').indexOf('Territory__c')){
                var operatorElement = changedFieldset.find(function(element) {
                    return element.fieldName == 'Condition_Operator__c';
                });
                operatorElement.pickListValues = [
                    {label: "IN", value: "IN"},
                    {label: "NOT", value: "NOT"}];
            } 
            cmp.set('v.changedFieldset',changedFieldset);
        } else{
            cmp.set('v.changedFieldset',changedFieldset);
        }
    }
})