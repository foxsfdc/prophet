({
    init : function(cmp,event,helper){
        //console.log('init::::',cmp.get('v.ruleDetail')['Condition_Date_Duration__c'])
        if(cmp.get('v.ruleDetail')){
            if(!$A.util.isEmpty(cmp.get('v.ruleDetail')['Condition_Date_Duration__c'])){
                cmp.set('v.dateOption',cmp.get('v.ruleDetail')['Condition_Date_Duration__c']);
            } else{
                cmp.set('v.dateOption','');
            }
            if(!$A.util.isEmpty(cmp.get('v.ruleDetail')['Condition_Criteria_Amount__c'])){
                cmp.set('v.dateValue',cmp.get('v.ruleDetail')['Condition_Criteria_Amount__c']);
            }
            if(!$A.util.isEmpty(cmp.get('v.ruleDetail')['Day_of_the_Week__c'])){
                cmp.set('v.dayValue',cmp.get('v.ruleDetail')['Day_of_the_Week__c']);
            }
        }
    },
	handleChange : function(cmp, event, helper) {
        console.log(event.getSource().get('v.value'));
        if(!$A.util.isEmpty(event.getSource().get('v.value'))){
            if(!$A.util.isEmpty(cmp.get('v.ruleDetail')['Condition_Criteria_Amount__c'])){
                cmp.set('v.dateValue','');
                cmp.get('v.ruleDetail')['Condition_Criteria_Amount__c'] = '';
            }
            if(!$A.util.isEmpty(cmp.get('v.ruleDetail')['Day_of_the_Week__c'])){
                cmp.set('v.dayValue','');
                cmp.get('v.ruleDetail')['Day_of_the_Week__c'] = '';
            }
            cmp.get('v.ruleDetail')['Condition_Date_Duration__c'] = event.getSource().get('v.value');
            
            //LRCC-1359
            var cmpEvent = cmp.getEvent("cmpEvent"); 
            cmpEvent.setParams({"isDay" : event.getSource().get('v.value')}); 
   			cmpEvent.fire(); 
            
        } else{
            cmp.get('v.ruleDetail')['Condition_Date_Duration__c'] = '';
            cmp.get('v.ruleDetail')['Condition_Criteria_Amount__c'] = '';
            cmp.get('v.ruleDetail')['Day_of_the_Week__c'] = '';
        }
    },
    setValue : function(cmp,event,helper){
        if(event.getSource().get('v.value')){
            cmp.get('v.ruleDetail')['Condition_Criteria_Amount__c'] = event.getSource().get('v.value');
        } else{
            cmp.get('v.ruleDetail')['Condition_Criteria_Amount__c'] = '';
        }
        if(event.getSource().get('v.value') != 'Day' && !$A.util.isEmpty(cmp.get('v.ruleDetail')['Day_of_the_Week__c'])){
            cmp.set('v.dayValue','');
            cmp.get('v.ruleDetail')['Day_of_the_Week__c'] = '';
        }
        
    },
    setDayValue : function(cmp,event,helper){
        if(event.getSource().get('v.value')){
            cmp.get('v.ruleDetail')['Day_of_the_Week__c'] = event.getSource().get('v.value');
        } else{
             cmp.get('v.ruleDetail')['Day_of_the_Week__c'] = '';
        }
    }
})