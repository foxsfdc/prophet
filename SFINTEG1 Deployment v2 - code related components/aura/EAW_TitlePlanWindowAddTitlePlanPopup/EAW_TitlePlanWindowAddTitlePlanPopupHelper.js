({
    handleErrors: function(errors, component, event, helper) {
	    
	    let errorMessage = '';
	    	
    	if (errors && Array.isArray(errors) && errors.length > 0) {
    		
	        errors.forEach(error => {
        		
	            if (error.message && errorMessage.includes(error.message) == false) {
	                errorMessage += error.message + "\n";
	            }
	            if (error.pageErrors && error.pageErrors.length > 0) {
	                error.pageErrors.forEach(pageError => {
	                    if (errorMessage.includes(error.message) == false)
	                        errorMessage += pageError.message + "\n";
	                });
	            } else if (error.fieldErrors) {
	                let fields = Object.keys(error.fieldErrors);
	                fields.forEach(fieldError => {
	                    if (Array.isArray(fieldError)) {
	                        fieldError.forEach(err => {
	                            if (errorMessage.includes(error.message) == false)
	                                errorMessage += err.message;
	                        });
	                    }
	                });
	            }
	        });
	    } else {
	        errorMessage = errors;
	    }
	    if (errorMessage.indexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION') !== -1) {
	        
	        let errorMessageList = errorMessage.split('FIELD_CUSTOM_VALIDATION_EXCEPTION,');
	        if (errorMessageList.length > 1) {
	            let finalErroList = errorMessageList[1].split(': []');
	            if (finalErroList.length) {
	                errorMessage = finalErroList[0];
	            }
	        }
	    }
	    if (errorMessage.indexOf('REQUIRED_FIELD_MISSING') !== -1) {
	        
	        let errorMessageList = errorMessage.split('REQUIRED_FIELD_MISSING,');
	        if (errorMessageList.length > 1) {
	            errorMessage = errorMessageList[1];
	        }
	    }
	    console.log('***** errorMessage-->', errorMessage);
	    return errorMessage;
	},
	
    clearInputsAndHide : function(component) {
        document.getElementById("EAW_TitlePlanWindowAddTitlePlanPopup").style.display = "none";
        component.set('v.selectedTitles', []);
        component.set('v.selectedPlanGuidelines', []);
        component.set('v.title', '');
        component.set('v.body', '');
    },

    errorToast : function(message) {
        var toastEvent = $A.get('e.force:showToast');

        toastEvent.setParams({
            'title': 'Error!',
            'type': 'error',
            'message': message
        });

        toastEvent.fire();
    },

    successToast : function(message) {
        var toastEvent = $A.get('e.force:showToast');

        toastEvent.setParams({
            'title': 'Success!',
            'type': 'success',
            'message': message
        });

        toastEvent.fire();
    }
})