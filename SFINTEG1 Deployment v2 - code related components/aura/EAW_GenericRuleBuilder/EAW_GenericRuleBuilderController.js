({	
    init : function(cmp,event,helper){     
        console.log('recordId:::',cmp.get('v.recordId'));
        helper.getRulesAndDetails(cmp,event,helper);
    },
    handleSelect : function(cmp, event, helper) {
        if(event.getParam("value") == 'earliestLatest'){
            helper.setEarliestLatestBasedNode(cmp,event,helper);
        }
        if(event.getParam("value") == 'condition'){
            helper.setConditionBasedNode(cmp,event,helper);
        }
        if(event.getParam("value") == 'date'){
             helper.setDateBasedNode(cmp,event,helper);
        }
        if(event.getParam("value") == 'multidate'){
            helper.setCaseBasedNode(cmp,event,helper);          
        }
        cmp.set('v.selectedContext',event.getParam("value"));
        //cmp.set('v.rule',{'sObjectType' : 'EAW_Rule__c'});
    },
    removeWholeNode : function(cmp,event,helper){
        if(cmp.get('v.rule').Id){
            helper.openModal(cmp,event,helper,'Delete this Rule and its Rule details?');
        } else{
            cmp.set('v.rule',{'sObjectType' : 'EAW_Rule__c'});
            cmp.set('v.selectedContext','');
        }        
        
    },
    saveRuleDetails : function(cmp,event,helper){
        var conditionType = event.getParam("ruleType");
        var rule = cmp.get('v.rule');
       
        /**LRCC:769**/
        rule.Nested__c = true;
        if(conditionType == 'condition'){
            rule.Condition_Type__c = 'If'; 
            cmp.set('v.rule',rule);
            
            var conditionBasedMap = cmp.get('v.conditionBasedMap');
            console.log(conditionBasedMap);
            var conditionList = [];
            conditionBasedMap.TrueBased.ruleDetailJSON.ruleDetail.Condition_Type__c = 'If';
            conditionList.push(conditionBasedMap.TrueBased.ruleDetailJSON);
            conditionBasedMap.FalseBased.ruleDetailJSON.ruleDetail.Condition_Type__c = 'Else/If';
            conditionList.push(conditionBasedMap.FalseBased.ruleDetailJSON);
            console.log('::conditionList::'+conditionList);
            var notValid = false;
            for(var i=0;i<conditionList.length;i++){
                conditionList[i].ruleDetail.Rule_Order__c = i+1;
                var temp = helper.checkForNullNodes(cmp,event,helper,conditionList[i]);
                if(temp){
                    notValid = true;
                    break;
                }
            }
             //LRCC-1454 conditionList.length > 1 &&
            if(conditionList.length && (conditionList[0].ruleDetail.hasOwnProperty('Conditional_Operand_Id__c') ||conditionList[0].ruleDetail.hasOwnProperty('Condition_Date_Text__c') || conditionList[0].ruleDetail.hasOwnProperty('Condition_Timeframe__c') ) && !notValid && !helper.checkForRule(cmp,event,helper,cmp.get('v.rule'))){
                if((!conditionList[1].ruleDetail.hasOwnProperty('Conditional_Operand_Id__c') && !conditionList[1].ruleDetail.hasOwnProperty('Condition_Date_Text__c') && !conditionList[1].ruleDetail.hasOwnProperty('Condition_Timeframe__c'))){
                	
                   var tempConditionList = conditionList;
                    conditionList.splice(1,1);
                    conditionList = tempConditionList;
                }
                console.log(':::Valid::')
                helper.saveRuleAndRuleDetailsNodes(cmp,event,helper,conditionList);
            } else{
                 helper.showToast(cmp,event,helper,'warning','Please provide valid values');
            }
            
        } else if(conditionType == 'earliestLatest'){
            cmp.set('v.rule',rule);
            if(!helper.checkForNullNodes(cmp,event,helper,cmp.get('v.newRuleDetailJSON')) && (cmp.get('v.newRuleDetailJSON').dateCalcs.length || cmp.get('v.newRuleDetailJSON').nodeCalcs.length)){
                console.log(':::::::Valid::::::');
                helper.saveRuleAndRuleDetailsNodes(cmp,event,helper,[cmp.get('v.newRuleDetailJSON')]);           
            } else{
                helper.showToast(cmp,event,helper,'warning','Please provide valid values');
            }
        } else if(conditionType == 'case'){
            var caseList = JSON.parse(JSON.stringify(cmp.get('v.caseRuleDetailMap').whenBased));
            //console.log('caseList:::::',caseList);
            for(var i=0;i<caseList.length;i++){
                caseList[i].ruleDetail.Condition_Type__c ='Case then';
            }
            cmp.get('v.caseRuleDetailMap').otherBased.ruleDetail.Rule_Order__c = cmp.get('v.caseRuleDetailMap').whenBased.length + 1;
            cmp.get('v.caseRuleDetailMap').otherBased.ruleDetail.Condition_Type__c = 'Otherwise';
            if(cmp.get('v.caseRuleDetailMap').otherBased.nodeCalcs.length && ((cmp.get('v.caseRuleDetailMap').otherBased.nodeCalcs[0].ruleDetail.hasOwnProperty('Condition_Type__c') && (cmp.get('v.caseRuleDetailMap').otherBased.nodeCalcs[0].ruleDetail.Condition_Type__c != 'Case') && cmp.get('v.caseRuleDetailMap').otherBased.nodeCalcs[0].ruleDetail.Condition_Type__c != 'OtherWise')|| cmp.get('v.caseRuleDetailMap').otherBased.nodeCalcs[0].ruleDetail.hasOwnProperty('Conditional_Operand_Id__c') || cmp.get('v.caseRuleDetailMap').otherBased.nodeCalcs[0].ruleDetail.hasOwnProperty('Condition_Timeframe__c'))){ // LRCC - 1555
                caseList.push(cmp.get('v.caseRuleDetailMap').otherBased);
            } else{
                if(cmp.get('v.caseRuleDetailMap').otherBased.ruleDetail.Id){
                    var ruleDetailsToDelete = cmp.get('v.ruleDetailstoDelete');
                    ruleDetailsToDelete.push(cmp.get('v.caseRuleDetailMap').otherBased.ruleDetail.Id);
                    cmp.set('v.ruleDetailstoDelete',ruleDetailsToDelete);
                }
            }
            cmp.get('v.rule').Condition_Type__c = 'case';
            console.log('caseList:::::',caseList);
            var notValid = false;
            for(var i=0;i<caseList.length;i++){
                caseList[i].ruleDetail.Rule_Order__c = i+1;
                var temp = helper.validForCase(cmp,event,helper,caseList[i],notValid);
                if(temp){
                    notValid = true;
                    break;
                }
            }
            //console.log(notValid)
            if(caseList.length >= 1 && !notValid){
                console.log('valid')
                helper.saveRuleAndRuleDetailsNodes(cmp,event,helper,caseList);
            } else{
                helper.showToast(cmp,event,helper,'warning','Please provide valid values');
            }
        }
        
    },
    saveDateCalc : function(cmp,event,helper){
        var rule = cmp.get('v.rule');
        rule.Nested__c = false;
        console.log(cmp.get('v.dateCalcBasedMap'));
        var newRuleDetailJSON = {
            'ruleDetail' : cmp.get('v.dateCalcBasedMap'),
            'dateCalcs' : [],
            'nodeCalcs' : []                
        };  
        if(!helper.validNodes(cmp,event,helper,false,[cmp.get('v.dateCalcBasedMap')],[])){
            helper.saveRuleAndRuleDetailsNodes(cmp,event,helper,[newRuleDetailJSON]);
        } else{
            helper.showToast(cmp,event,helper,'warning','Please provide valid values');
        }
        
    },
    deleteRuleDetailById : function(cmp,event,helper){
        var ruleDetailId = event.getParam("ruleDetailId");
        var isDateCalc = event.getParam("isDateCalc");
        //helper.deleteRuleDetailById(cmp,event,helper,ruleDetailId,isDateCalc,false);  
        var toDeleteList = cmp.get('v.ruleDetailstoDelete') || [];
        toDeleteList.push(ruleDetailId);
        cmp.set('v.ruleDetailstoDelete',toDeleteList);
    },
    deleteRuleDetail : function(cmp,event,helper){
        event.stopPropagation();
        helper.deleteRuleDetailById(cmp,event,helper,cmp.get('v.rule').Id,false,true);     
        cmp.set('v.rule',{'sObjectType' : 'EAW_Rule__c'});
        cmp.set('v.selectedContext','');
        cmp.set('v.ruleDetailstoDelete',[]);
    },
    getTest : function(cmp){
        console.log(cmp.get('v.newRuleDetailJSON'));
    }
})