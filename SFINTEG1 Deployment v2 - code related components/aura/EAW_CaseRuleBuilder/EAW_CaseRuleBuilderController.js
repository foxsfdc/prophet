({
    addWhenContext : function(cmp, event, helper) {
        var thenBasedRuleDetailJSON = cmp.get('v.thenBasedRuleDetailJSON');
        var newRuleDetail = JSON.parse(JSON.stringify(cmp.get('v.ruleDetailObject')));
        newRuleDetail.Rule_Order__c = thenBasedRuleDetailJSON.length + 1;
        newRuleDetail.Nest_Order__c = 1;
        newRuleDetail.Condition_Type__c = 'Case';
        var newRuleDetailJSON = {
            'ruleDetail' : JSON.parse(JSON.stringify(newRuleDetail)),
            'dateCalcs' : [],
            'nodeCalcs' : []               
        };  
        newRuleDetail.Nest_Order__c = 2;
        newRuleDetailJSON.nodeCalcs.push({'ruleDetail' : JSON.parse(JSON.stringify(newRuleDetail)),
                                          'dateCalcs' : [],
                                          'nodeCalcs' : []});
        thenBasedRuleDetailJSON.push(JSON.parse(JSON.stringify(newRuleDetailJSON)));
        console.log(thenBasedRuleDetailJSON);
        cmp.set('v.thenBasedRuleDetailJSON',thenBasedRuleDetailJSON);	
    },
    init : function(cmp,event,helper){
        console.log(cmp.get('v.thenBasedRuleDetailJSON'));
        console.log(cmp.get('v.otherBasedRuleDetailJSON'));
        cmp.set('v.spinner',true);
        var action = cmp.get('c.getDependentFieldSet');
        action.setParams({
            'recordTypeName' : cmp.get('v.recordTypeName'),
            'fieldName' : 'Condition_Criteria__c'
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state == 'SUCCESS'){
                console.log('otherBasedRuleDetailJSON:::',cmp.get('v.otherBasedRuleDetailJSON'));
                helper.setNodeForBasedNodes(cmp,event,helper,response.getReturnValue());
               // cmp.set('v.conditionBasedField',response.getReturnValue().controllingPicklistValues);
               // cmp.set('v.conditionBasedDependentMap',response.getReturnValue().dependentPicklistValues);
                cmp.set('v.spinner',false);
            } else if(state == 'ERROR'){
                console.log(response.getError()[0].message);
            }
            cmp.set('v.spinner',false);
        });
        $A.enqueueAction(action);
    },
     saveCondition : function(cmp,event,helper){
        var cmpEvent = cmp.getEvent("ruleDetailSave");
         cmpEvent.setParams({
             'ruleType' : 'case'
         });
         if(cmp.find('whenEarliest')){
             var whenEarliest = cmp.find('whenEarliest');
             if(!Array.isArray(whenEarliest)){
                 whenEarliest = [whenEarliest];
             }
             for(var i of whenEarliest){
                 i.formEarliestLatestNodes();
             }
         }
         if(cmp.find('otherEarliest')){
             var otherEarliest = cmp.find('otherEarliest');
             if(!Array.isArray(otherEarliest)){
                 otherEarliest = [otherEarliest];
             }
             for(var i of otherEarliest){
                 i.formEarliestLatestNodes();
             }
         }
         cmpEvent.fire();
     },
    removeWhenContext : function(cmp,event,helper){
        var thenBasedRuleDetailJSON = cmp.get('v.thenBasedRuleDetailJSON');
        var indexPosition = event.getSource().get('v.name');
        var nodeToDelete = {};
        nodeToDelete.isWhen = true;
        nodeToDelete.isChild = false;
        nodeToDelete.index = indexPosition;
        cmp.set('v.nodeToDelete',nodeToDelete);
        if(thenBasedRuleDetailJSON[indexPosition].ruleDetail && thenBasedRuleDetailJSON[indexPosition].ruleDetail.Id){
            helper.openModal(cmp,event,helper,'Delete this Case When Rule details?');
        } else{
            if(thenBasedRuleDetailJSON.length != 1){
                thenBasedRuleDetailJSON.splice(indexPosition,1);
                cmp.set('v.thenBasedRuleDetailJSON',thenBasedRuleDetailJSON);
            }  
        }
    },
    deleteNodes : function(cmp,event,helper){
        var newRuleDetail = JSON.parse(JSON.stringify(cmp.get('v.ruleDetailObject')));
        var newRuleDetailJSON = {
            'ruleDetail' : newRuleDetail,
            'dateCalcs' : [],
            'nodeCalcs' : []
        };
        var thenBasedRuleDetailJSON = cmp.get('v.thenBasedRuleDetailJSON');
        var indexPosition = event.getSource().get('v.name');
        var nodeToDelete = {};
        nodeToDelete.isWhen = true;
        nodeToDelete.isChild = true;
        nodeToDelete.index = indexPosition;
         cmp.set('v.nodeToDelete',nodeToDelete);
        if(thenBasedRuleDetailJSON[indexPosition].nodeCalcs[0] && thenBasedRuleDetailJSON[indexPosition].nodeCalcs[0].ruleDetail.Id){
            helper.openModal(cmp,event,helper,'Delete this Case then Rule details?');
        } else{
            thenBasedRuleDetailJSON[indexPosition].nodeCalcs[0] = newRuleDetailJSON;
            cmp.set('v.thenBasedRuleDetailJSON',thenBasedRuleDetailJSON);
            console.log(cmp.find('whenEarliest'),indexPosition);
            var whenEarliest = cmp.find('whenEarliest');
            if(!Array.isArray(whenEarliest)){
                whenEarliest = [whenEarliest];
            }
            if(whenEarliest[indexPosition]){
                whenEarliest[indexPosition].set('v.toSelect',false);
            }
        }  
        
    },
    deleteRuleDetail : function(cmp,event,helper){
        event.stopPropagation();
        var newRuleDetail = JSON.parse(JSON.stringify(cmp.get('v.ruleDetailObject')));
        var newRuleDetailJSON = {
            'ruleDetail' : newRuleDetail,
            'dateCalcs' : [],
            'nodeCalcs' : []
        };
        var thenBasedRuleDetailJSON = cmp.get('v.thenBasedRuleDetailJSON');
        var otherBased = cmp.get('v.otherBasedRuleDetailJSON');
        if(cmp.get('v.nodeToDelete').isWhen){
            if(cmp.get('v.nodeToDelete').isChild){
                helper.deleteRuleDetailById(cmp,event,helper, thenBasedRuleDetailJSON[cmp.get('v.nodeToDelete').index].nodeCalcs[0].ruleDetail.Id,false);
                thenBasedRuleDetailJSON[cmp.get('v.nodeToDelete').index].nodeCalcs[0] = newRuleDetailJSON;
            } else{
                helper.deleteRuleDetailById(cmp,event,helper, thenBasedRuleDetailJSON[cmp.get('v.nodeToDelete').index].ruleDetail.Id,false);
                if(thenBasedRuleDetailJSON.length != 1){
                    thenBasedRuleDetailJSON.splice(cmp.get('v.nodeToDelete').index,1);
                } else{
                    newRuleDetail.Condition_Type__c = 'Case then';
                    newRuleDetail.Nest_Order__c = 1;
                    newRuleDetailJSON.nodeCalcs.push({'ruleDetail' : JSON.parse(JSON.stringify(newRuleDetail)),
                                                      'dateCalcs' : [],
                                                      'nodeCalcs' : []});
                    thenBasedRuleDetailJSON = [newRuleDetailJSON];
                }
                
            }
            cmp.set('v.thenBasedRuleDetailJSON',thenBasedRuleDetailJSON);
            var whenEarliest = cmp.find('whenEarliest');
            if(!Array.isArray(cmp.find('whenEarliest'))){
                whenEarliest = [whenEarliest];
            }
            whenEarliest[cmp.get('v.nodeToDelete').index].set('v.toSelect',false);
        } else{
            if(cmp.get('v.nodeToDelete').isChild){
                helper.deleteRuleDetailById(cmp,event,helper, otherBased.nodeCalcs[0].ruleDetail.Id,false);
                otherBased.nodeCalcs = [newRuleDetailJSON];
                cmp.set('v.otherBasedRuleDetailJSON',otherBased);
                cmp.find('otherEarliest').set('v.toSelect',false);
            }else{
                helper.deleteRuleDetailById(cmp,event,helper, otherBased.ruleDetail.Id,false);
                newRuleDetail.Condition_Type__c = 'OtherWise';
                var newRuleDetailJSON = {
                    'ruleDetail' : JSON.parse(JSON.stringify(newRuleDetail)),
                    'dateCalcs' : [],
                    'nodeCalcs' : []               
                };  
                newRuleDetail.Nest_Order__c = 2;
                newRuleDetailJSON.nodeCalcs.push({'ruleDetail' : JSON.parse(JSON.stringify(newRuleDetail)),
                                                  'dateCalcs' : [],
                                                  'nodeCalcs' : []});
                cmp.set('v.otherBasedRuleDetailJSON',newRuleDetailJSON);
                cmp.find('otherEarliest').set('v.toSelect',false);
            }           
        }
    },
    removeOtherContext : function(cmp,event,helper){
        var otherBasedRuleDetailJSON = cmp.get('v.otherBasedRuleDetailJSON');
        var newRuleDetail = JSON.parse(JSON.stringify(cmp.get('v.ruleDetailObject')));
        var newRuleDetailJSON = {
            'ruleDetail' : newRuleDetail,
            'dateCalcs' : [],
            'nodeCalcs' : []
        };
        var otherBased = event.getSource().get('v.name');
        if(otherBased == 'parentOtherWise'){
            var nodeToDelete = {};
            nodeToDelete.isWhen = false;
            nodeToDelete.isChild = false;
            cmp.set('v.nodeToDelete',nodeToDelete);
            if(otherBasedRuleDetailJSON.ruleDetail.Id){
                helper.openModal(cmp,event,helper,'Delete this Case otherwise Rule details?');
            } else{
                newRuleDetail.Condition_Type__c = 'OtherWise';
                var newRuleDetailJSON = {
                    'ruleDetail' : JSON.parse(JSON.stringify(newRuleDetail)),
                    'dateCalcs' : [],
                    'nodeCalcs' : []               
                };  
                newRuleDetail.Nest_Order__c = 2;
                newRuleDetailJSON.nodeCalcs.push({'ruleDetail' : JSON.parse(JSON.stringify(newRuleDetail)),
                                                  'dateCalcs' : [],
                                                  'nodeCalcs' : []});
                cmp.set('v.otherBasedRuleDetailJSON',newRuleDetailJSON);
                cmp.find('otherEarliest').set('v.toSelect',false);
            }
        } else{
             var nodeToDelete = {};
            nodeToDelete.isWhen = false;
            nodeToDelete.isChild = true;
            cmp.set('v.nodeToDelete',nodeToDelete);
            if(otherBasedRuleDetailJSON.nodeCalcs[0].ruleDetail.Id){
              helper.openModal(cmp,event,helper,'Delete this Case otherwise Rule details?');
            } else{
                otherBasedRuleDetailJSON.nodeCalcs = [newRuleDetailJSON];
                cmp.set('v.otherBasedRuleDetailJSON',otherBasedRuleDetailJSON);
                cmp.find('otherEarliest').set('v.toSelect',false);
            }
        }  
    }
    
})