({
	setNodeForBasedNodes : function(cmp,event,helper,response) {
		var thenBasedRuleDetailJSON = cmp.get('v.thenBasedRuleDetailJSON');
        var otherBasedRuleDetailJSON = cmp.get('v.otherBasedRuleDetailJSON');
        var ruleDetailList = cmp.get('v.ruleDetailList');
        var insertList = [];
        console.log(JSON.stringify(otherBasedRuleDetailJSON));
        if(thenBasedRuleDetailJSON.length && thenBasedRuleDetailJSON[0].ruleDetail.Id){
            for(var j=0;j<ruleDetailList.length;j++){
                for(var i=0;i<thenBasedRuleDetailJSON.length;i++){
                   // console.log(thenBasedRuleDetailJSON[i]);
                   // console.log(otherBasedRuleDetailJSON.ruleDetail);
                    if(thenBasedRuleDetailJSON[i].ruleDetail.Id == ruleDetailList[j].Parent_Rule_Detail__c &&  insertList.indexOf(ruleDetailList[j]) == -1){
                        var temp = {
                            'ruleDetail' : ruleDetailList[j],
                            'dateCalcs' : [],
                            'nodeCalcs' : []
                        };
                        thenBasedRuleDetailJSON[i].nodeCalcs.push(temp);
                        insertList.push(ruleDetailList[j]);
                    } else if(otherBasedRuleDetailJSON.ruleDetail && otherBasedRuleDetailJSON.ruleDetail.Id == ruleDetailList[j].Parent_Rule_Detail__c &&  insertList.indexOf(ruleDetailList[j]) == -1){
                        var temp = {
                            'ruleDetail' : ruleDetailList[j],
                            'dateCalcs' : [],
                            'nodeCalcs' : []
                        };
                        otherBasedRuleDetailJSON.nodeCalcs.push(temp);
                        insertList.push(ruleDetailList[j]);
                    }
                  //  console.log(thenBasedRuleDetailJSON[i].ruleDetail.Id,ruleDetailList[j].Parent_Rule_Detail__c);
                }
            }
            console.log('thenBasedRuleDetailJSON::',thenBasedRuleDetailJSON);
            console.log('otherBasedRuleDetailJSON::',otherBasedRuleDetailJSON);
            
         //   window.setTimeout(function(){
            
           // },5000);
        }
        cmp.set('v.thenBasedRuleDetailJSON',thenBasedRuleDetailJSON);
        cmp.set('v.otherBasedRuleDetailJSON',otherBasedRuleDetailJSON);
        cmp.set('v.conditionBasedField',response.controllingPicklistValues);
        cmp.set('v.conditionBasedDependentMap',response.dependentPicklistValues);
    },
    deleteRuleDetailById : function(cmp,event,helper,ruleDetailId,isDateCalc){
        console.log(ruleDetailId,isDateCalc);
        var cmpEvent = cmp.getEvent("ruleDetailDelete");
        cmpEvent.setParams({
            'ruleDetailId' : ruleDetailId,
            'isDateCalc' : isDateCalc
        });
        cmpEvent.fire();
    },
     openModal : function(cmp,event,helper,message){
        cmp.find('promt').set('v.content',message);
        cmp.find('promt').openModal();
    },
})