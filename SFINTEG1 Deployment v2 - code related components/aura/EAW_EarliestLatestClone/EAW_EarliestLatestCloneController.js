({
	init : function(cmp, event, helper) {
        
       // console.log('::::::::::::'+JSON.stringify(cmp.get('v.newRuleDetailJSON')));
        if(cmp.get('v.newRuleDetailJSON')) helper.setRuleDetailNodes(cmp,event,helper);
        if(cmp.get('v.newRuleDetailJSON') && cmp.get('v.newRuleDetailJSON').ruleDetail && cmp.get('v.newRuleDetailJSON').ruleDetail.Id && !cmp.get('v.newRuleDetailJSON').nodeCalcs.length){
           // console.log(cmp.get('v.newRuleDetailJSON').ruleDetail);
            if(cmp.get('v.newRuleDetailJSON').ruleDetail.Condition_Met_Days__c || !cmp.get('v.newRuleDetailJSON').ruleDetail.Condition_Timeframe__c){
            	cmp.get('v.newRuleDetailJSON').ruleDetail.Condition_Type__c = ''; 
            }
            //helper.setRuleDetailNodes(cmp,event,helper);
            if(cmp.get('v.newRuleDetailJSON') && cmp.get('v.newRuleDetailJSON').ruleDetail.Rule_Order__c && cmp.get('v.newRuleDetailJSON').ruleDetail.Nest_Order__c){
                cmp.set('v.ruleOrder',cmp.get('v.newRuleDetailJSON').ruleDetail.Rule_Order__c);
                cmp.set('v.nestOrder',cmp.get('v.newRuleDetailJSON').ruleDetail.Nest_Order__c);
            }
        } else{
            if(cmp.get('v.newRuleDetailJSON') && cmp.get('v.newRuleDetailJSON').ruleDetail && cmp.get('v.newRuleDetailJSON').ruleDetail.Rule_Order__c && cmp.get('v.newRuleDetailJSON').ruleDetail.Nest_Order__c){
                cmp.set('v.ruleOrder',cmp.get('v.newRuleDetailJSON').ruleDetail.Rule_Order__c);
                cmp.set('v.nestOrder',cmp.get('v.newRuleDetailJSON').ruleDetail.Nest_Order__c);
            }
        }
        
        if(cmp.get('v.conditionTimeFrame')){
            cmp.set('v.options',[{'label': 'Earliest', 'value':'True_Earliest'},
                                  {'label': 'Latest', 'value': 'True_Latest'},]);
        } else {
             cmp.set('v.options',[{'label': 'Earliest', 'value':'False Earliest'},
                                  {'label': 'Latest', 'value': 'False Latest'},]);
        }
        
	},
    addContext : function(cmp,event,helper){
        var selectedMenuItemValue = event.getParam("value");
        var newRuleDetailJSON = cmp.get('v.newRuleDetailJSON');
        var newRuleDetail = JSON.parse(JSON.stringify(cmp.get('v.newRuleDetail')));
        
        if(cmp.get('v.parentNode') && !cmp.get('v.toSelect') && newRuleDetailJSON && newRuleDetailJSON.dateCalcs && newRuleDetailJSON.nodeCalcs && !newRuleDetailJSON.dateCalcs.length && !newRuleDetailJSON.nodeCalcs.length){
           //console.log('inside');
            if(selectedMenuItemValue == 'Add Date Calc'){ 
                newRuleDetailJSON.ruleDetail.Condition_Type__c = '';
                
            } else if(selectedMenuItemValue == 'TBD' || selectedMenuItemValue == 'Perpetuity'){ // LRCC - 1154
                newRuleDetailJSON.ruleDetail.Condition_Type__c = '';
                newRuleDetailJSON.ruleDetail.Condition_Date_Text__c = selectedMenuItemValue;
               
            } else{
                newRuleDetailJSON.ruleDetail.Condition_Type__c = 'Else/If';
                cmp.set('v.toSelect',true);
            }
        } else{
            if(selectedMenuItemValue == 'Add Date Calc'){            
                
                newRuleDetail.Rule_Order__c = cmp.get('v.ruleOrder');
                newRuleDetail.Nest_Order__c = cmp.get('v.nestOrder');
                if(!newRuleDetailJSON.dateCalcs){
                    newRuleDetailJSON.dateCalcs = [];
                }
                newRuleDetailJSON.dateCalcs.push(newRuleDetail);
            } else if(selectedMenuItemValue == 'TBD' || selectedMenuItemValue == 'Perpetuity'){ // LRCC - 1154
                newRuleDetail.Rule_Order__c = cmp.get('v.ruleOrder');
                newRuleDetail.Nest_Order__c = cmp.get('v.nestOrder');
                newRuleDetail.Condition_Date_Text = selectedMenuItemValue;
                 if(!newRuleDetailJSON.dateCalcs){
                    newRuleDetailJSON.dateCalcs = [];
                }
                newRuleDetailJSON.dateCalcs.push(newRuleDetail);
            } else if(selectedMenuItemValue == 'case' || selectedMenuItemValue == 'if'){
                var parentConditionNode = {
                    'ruleDetail' :  JSON.parse(JSON.stringify(cmp.get('v.newRuleDetail'))),
                    'dateCalcs' : [],
                    'nodeCalcs' : []  
                };
                if(selectedMenuItemValue == 'if'){
                    parentConditionNode.ruleDetail.Parent_Condition_Type__c = 'Condition';
                    parentConditionNode.ruleDetail.Condition_Type__c = 'If';
                } else {
                    parentConditionNode.ruleDetail.Parent_Condition_Type__c = 'Case';
                    parentConditionNode.ruleDetail.Condition_Type__c = 'Case';
                }
                if(!newRuleDetailJSON.nodeCalcs){
                    newRuleDetailJSON.nodeCalcs = [];
                }
                newRuleDetailJSON.nodeCalcs.push(parentConditionNode);
            } else {
                newRuleDetail.Rule_Order__c = cmp.get('v.ruleOrder');
                newRuleDetail.Nest_Order__c = cmp.get('v.nestOrder')+ 1;
                var nodeRuleDetail = {
                    'ruleDetail' : newRuleDetail,
                    'dateCalcs' : [],
                    'nodeCalcs' : []                
                }; 
                if(!newRuleDetailJSON.nodeCalcs){
                    newRuleDetailJSON.nodeCalcs = [];
                }
                newRuleDetailJSON.nodeCalcs.push(nodeRuleDetail);
            } 
        }
        console.log(newRuleDetailJSON);
        cmp.set('v.newRuleDetailJSON',newRuleDetailJSON);
    },
    removeNodeCalc : function(cmp,event,helper){
        var newRuleDetailJSON = cmp.get('v.newRuleDetailJSON');
        var indexPosition = event.getSource().get('v.name');
        var nodeToDelete = cmp.get('v.nodeToDelete');
        nodeToDelete.isDateCalc = false;
        nodeToDelete.index = indexPosition;
        cmp.set('v.nodeToDelete',nodeToDelete);
        
        if(newRuleDetailJSON.nodeCalcs[indexPosition] && newRuleDetailJSON.nodeCalcs[indexPosition].ruleDetail.Id){
            helper.openModal(cmp,event,helper,'Delete this Rule and its Rule details?');
        } else{
            //console.log(newRuleDetailJSON.nodeCalcs[indexPosition]);
            newRuleDetailJSON.nodeCalcs.splice(indexPosition,1);
            cmp.set('v.newRuleDetailJSON',newRuleDetailJSON);
        }              
    },
    removeDateCalc : function(cmp,event,helper){
        var newRuleDetailJSON = cmp.get('v.newRuleDetailJSON');
        var indexPosition = event.getSource().get('v.name');
        var nodeToDelete = cmp.get('v.nodeToDelete');
        nodeToDelete.isDateCalc = true;
        nodeToDelete.index = indexPosition;
        cmp.set('v.nodeToDelete',nodeToDelete);
        if(newRuleDetailJSON.dateCalcs[indexPosition] && newRuleDetailJSON.dateCalcs[indexPosition].Id){
            helper.openModal(cmp,event,helper,'Delete this Rule details?');
        } else{
            //console.log(newRuleDetailJSON.dateCalcs[indexPosition]);
            newRuleDetailJSON.dateCalcs.splice(indexPosition,1);
            cmp.set('v.newRuleDetailJSON',newRuleDetailJSON);
        }  
    },
    setEarliestLatestNodes : function(cmp,event,helper){
        //console.log('::::::::::::::::;'+JSON.stringify(cmp.get('v.newRuleDetailJSON')));
        if(cmp.find('conditions')){
            var conditions = cmp.find('conditions');
           // console.log(Array.isArray(conditions));
            if(!Array.isArray(conditions)){
                conditions = [conditions];
            }
            let currentNode = cmp.get('v.newRuleDetailJSON');
            for(var i in conditions){
                
               // console.log(i+':::::Node::::::'+conditions[i].get('v.selectedContext'));
                if(conditions[i].get('v.selectedContext') == 'condition' && conditions[i].get('v.conditionBasedMap')){
                    var conditionList = [];
                    conditions[i].get('v.conditionBasedMap').TrueBased.ruleDetailJSON.ruleDetail.Condition_Type__c = 'If';
                    conditions[i].get('v.conditionBasedMap').FalseBased.ruleDetailJSON.ruleDetail.Condition_Type__c = 'Else/If';
                    conditionList.push(conditions[i].get('v.conditionBasedMap').TrueBased.ruleDetailJSON);
                    conditionList.push(conditions[i].get('v.conditionBasedMap').FalseBased.ruleDetailJSON);
                    if((!conditionList[1].ruleDetail.hasOwnProperty('Conditional_Operand_Id__c') && !conditionList[1].ruleDetail.hasOwnProperty('Condition_Date_Text__c') && !conditionList[1].ruleDetail.hasOwnProperty('Condition_Timeframe__c'))){
                        
                        var tempConditionList = conditionList;
                        conditionList.splice(1,1);
                        conditionList = tempConditionList;
                    }
                    currentNode.nodeCalcs[i].nodeCalcs = conditionList;
                    
                } else if(conditions[i].get('v.selectedContext') == 'multidate' && conditions[i].get('v.caseRuleDetailMap')){
                  //  console.log(':::'+conditions[i].get('v.caseRuleDetailMap'));
                    var caseList = [];
                    var whenList = conditions[i].get('v.caseRuleDetailMap').whenBased;
                    for(var j=0;j<whenList.length;j++){
                        whenList[j].ruleDetail.Condition_Type__c ='Case then';
                        caseList.push(whenList[j]);
                    }
                    (conditions[i].get('v.caseRuleDetailMap')).otherBased.ruleDetail.Condition_Type__c = 'Otherwise';
                    (conditions[i].get('v.caseRuleDetailMap')).otherBased.ruleDetail.Rule_Order__c = 2;
                    if(conditions[i].get('v.caseRuleDetailMap').otherBased.nodeCalcs.length && ((conditions[i].get('v.caseRuleDetailMap').otherBased.nodeCalcs[0].ruleDetail.hasOwnProperty('Condition_Type__c') && (conditions[i].get('v.caseRuleDetailMap').otherBased.nodeCalcs[0].ruleDetail.Condition_Type__c != 'Case') && conditions[i].get('v.caseRuleDetailMap').otherBased.nodeCalcs[0].ruleDetail.Condition_Type__c != 'OtherWise')|| conditions[i].get('v.caseRuleDetailMap').otherBased.nodeCalcs[0].ruleDetail.hasOwnProperty('Conditional_Operand_Id__c') || conditions[i].get('v.caseRuleDetailMap').otherBased.nodeCalcs[0].ruleDetail.hasOwnProperty('Condition_Timeframe__c'))){ // LRCC - 1555
                        // console.log('::::::::::::::::::::::'+JSON.stringify(cmp.get('v.caseRuleDetailMap').otherBased));
                        caseList.push(conditions[i].get('v.caseRuleDetailMap').otherBased);
                    } else{
                        if(conditions[i].get('v.caseRuleDetailMap').otherBased.ruleDetail.Id){

                        }
                    }
                    //caseList.push((conditions[i].get('v.caseRuleDetailMap')).otherBased);
                    currentNode.nodeCalcs[i].nodeCalcs = caseList;
                }
             //  console.log('::::::::::::::::;'+JSON.stringify(cmp.get('v.newRuleDetailJSON')));
                //cmp.set('v.newRuleDetailJSON').nodeCalcs[i] = currentNode;
            }
            //cmp.set('v.newRuleDetailJSON',currentNode);
        }
    },
    deleteRuleDetail : function(cmp,event,helper){
        event.stopPropagation();
        var newRuleDetailJSON = cmp.get('v.newRuleDetailJSON');
        if(cmp.get('v.nodeToDelete').isDateCalc){
            helper.deleteRuleDetailById(cmp,event,helper, newRuleDetailJSON.dateCalcs[cmp.get('v.nodeToDelete').index].Id,true);
            newRuleDetailJSON.dateCalcs.splice(cmp.get('v.nodeToDelete').index,1);
        } else{
            helper.deleteRuleDetailById(cmp,event,helper, newRuleDetailJSON.nodeCalcs[cmp.get('v.nodeToDelete').index].ruleDetail.Id,false);
            newRuleDetailJSON.nodeCalcs.splice(cmp.get('v.nodeToDelete').index,1);
        }
        cmp.set('v.newRuleDetailJSON',newRuleDetailJSON);
    },
    saveRuleDetails : function(cmp,event,helper){
        helper.formNestedNodes(cmp,event,helper);
        var cmpEvent = cmp.getEvent("ruleDetailSave");
        cmpEvent.setParams({
            'ruleType' : 'earliestLatest'
        });
        console.log('::::::::final::::::::;'+JSON.stringify(cmp.get('v.newRuleDetailJSON')));
        cmpEvent.fire();
    },
    formNestedNodesForCondition : function(cmp,event,helper){
        console.log('::formNestedNodesForCondition::');
        helper.formNestedNodes(cmp,event,helper);
    }
})