({
	setRuleDetailNodes : function(cmp,event,helper) {
		cmp.set('v.spinner',true);
        var newRuleDetailJSON = cmp.get('v.newRuleDetailJSON');
        var currentRuleDetail = newRuleDetailJSON.ruleDetail;
        var ruleDetailList = cmp.get('v.ruleDetailList');
        var insertList = [];
        var addedNodeRuleDetail = cmp.get('v.addedNodeRuleDetail') || [];
      //  console.log(':::ruleDetailList:::::::'+ruleDetailList);
        
        for(var i=0;i<ruleDetailList.length;i++){
            if(ruleDetailList[i].Parent_Rule_Detail__c == currentRuleDetail.Id){
                if($A.util.isEmpty(ruleDetailList[i].Condition_Timeframe__c) && $A.util.isEmpty(ruleDetailList[i].Parent_Condition_Type__c)){
                    if(newRuleDetailJSON.dateCalcs.indexOf(ruleDetailList[i]) == -1){
                        newRuleDetailJSON.dateCalcs.push(ruleDetailList[i]);
                        insertList.push(i);
                    }  
                } else if(ruleDetailList[i].Condition_Timeframe__c == 'TBD' || ruleDetailList[i].Condition_Timeframe__c == 'Perpetuity'){ // LRCC - 1154
                    if(newRuleDetailJSON.dateCalcs.indexOf(ruleDetailList[i]) == -1){
                        newRuleDetailJSON.dateCalcs.push(ruleDetailList[i]);
                        insertList.push(i);
                    }  
                }else if(addedNodeRuleDetail.indexOf(ruleDetailList[i]) == -1){
                    var nodeRuleDetail = {
                        'ruleDetail' : ruleDetailList[i],
                        'dateCalcs' : [],
                        'nodeCalcs' : []                
                    };
                    if(newRuleDetailJSON.nodeCalcs.indexOf(nodeRuleDetail) == -1){
                        newRuleDetailJSON.nodeCalcs.push(nodeRuleDetail);
                        addedNodeRuleDetail.push(ruleDetailList[i]);
                    }                   
                }
            }
        }   
       // console.log(':::newRuleDetailJSON:::'+JSON.stringify(newRuleDetailJSON));
        cmp.set('v.addedNodeRuleDetail',addedNodeRuleDetail);      
        cmp.set('v.ruleDetailList',ruleDetailList);
        window.setTimeout(function(){
            cmp.set('v.newRuleDetailJSON',newRuleDetailJSON);
            cmp.set('v.spinner',false);
        },1500);
    },
    openModal : function(cmp,event,helper,message){
        cmp.find('promt').set('v.content',message);
        cmp.find('promt').openModal();
    },
    deleteRuleDetailById : function(cmp,event,helper,ruleDetailId,isDateCalc){
        console.log(ruleDetailId,isDateCalc);
        var cmpEvent = cmp.getEvent("ruleDetailDelete");
        cmpEvent.setParams({
            'ruleDetailId' : ruleDetailId,
            'isDateCalc' : isDateCalc
        });
        cmpEvent.fire();
    },
    formNestedNodes : function(cmp,event,helper){
        var appEvent = $A.get("e.c:EAW_EarliestLatestNodes");
        appEvent.fire();
    }
})