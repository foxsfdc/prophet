({
	init : function(cmp, event, helper) {
        var params = event.getParam('arguments');
        if(params){
            cmp.set('v.recordId',params.windowId);
            helper.getWindowStrands(cmp,event,helper,false);
        }
    },
    saveStrands : function(cmp,event,helper){
        var isNull = helper.checkForNull(cmp,event,helper);
        if(!isNull){
            console.log('::::',cmp.get('v.windowStrandList'));
            cmp.set('v.spinner',true);
            var windowStrandsToDelete = cmp.find('windowStrandContainer').get('v.windowStrandsToDelete') || null;
            var action = cmp.get('c.saveWindowStrands');
            action.setParams({
                windowStrandList : JSON.stringify(cmp.get('v.windowStrandList')),
                windowStrandListToDelete : windowStrandsToDelete
            });
            action.setCallback(this,function(response){
                var state = response.getState();
                if(state == 'SUCCESS'){
                    cmp.set('v.windowRecord',{'sObjectType':'EAW_Window__c'});
                    helper.showToast(cmp,event,helper,'success','Window Strands are updated.');
                    helper.getWindowStrands(cmp,event,helper,false);
                } else if(state == 'ERROR'){
                    console.log('error::',response.getError());
                }
            });
            $A.enqueueAction(action);
        }
    },
    restoreWindowStrands : function(cmp,event,helper){
        cmp.set('v.spinner',true);
        var action = cmp.get('c.restoreOverriddenWindowStrands');
        action.setParams({
            'windowId' : cmp.get('v.recordId')
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state == 'SUCCESS'){
                cmp.set('v.windowRecord',{'sObjectType':'EAW_Window__c'});
                helper.showToast(cmp,event,helper,'success','Default Window Strands Overridden');
                helper.getWindowStrands(cmp,event,helper,true);
            } else if(state == 'ERROR'){
                console.log('error::',response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    closeModal : function(cmp){
        cmp.get('v.windowRecord').Id = null;
        cmp.set('v.windowRecord',{'sObjectType':'EAW_Window__c'});
        $A.util.removeClass(cmp.find('modal'),'slds-fade-in-open');
        $A.util.removeClass(cmp.find('backDrop'),'slds-backdrop_open');
        
    }
})