({
    getWindowStrands : function(cmp,evnet,helper,isOverridden) {
        cmp.set('v.spinner',true);
        var action = cmp.get('c.getWindowStrands');
        action.setParams({
            'windowId' : cmp.get('v.recordId'),
            'fieldList' : ['License_Type__c','Media__c','Territory__c','Language__c']
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state == 'SUCCESS'){
                console.log('::',response.getReturnValue());
                cmp.set('v.windowStrandList',response.getReturnValue().windowStrandList);
                cmp.set('v.fieldWrapper',response.getReturnValue().fieldWrapperSet);
                cmp.set('v.windowRecord',response.getReturnValue().window);
                if(response.getReturnValue().windowStrandList.length){
                    if(!isOverridden)cmp.set('v.edit',false);
                } else{
                    cmp.set('v.edit',true);
                }
                $A.util.addClass(cmp.find('modal'),'slds-fade-in-open');
                $A.util.addClass(cmp.find('backDrop'),'slds-backdrop_open');
            } else if(state == 'ERROR'){
                console.log('error ::',response.getError());
            }
            cmp.set('v.spinner',false);
        });
        $A.enqueueAction(action);
    },
    checkForNull : function(cmp,event,helper){
        var isNull = false;
        var windowStrandContainer = cmp.find('windowStrandContainer');
        if(!Array.isArray(windowStrandContainer)){
            windowStrandContainer = [windowStrandContainer];
        }
        for(var i of windowStrandContainer){
            if(i.checkForNull()){
                isNull = true;
            }
        }
        return isNull;
    },
    showToast : function(cmp, event, helper, type, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "type": type,
            "message": message
        });
        toastEvent.fire();
    },
})