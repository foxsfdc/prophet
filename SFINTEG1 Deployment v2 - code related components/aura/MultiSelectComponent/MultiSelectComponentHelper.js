({
	helperMethod : function(cmp, event) {
        event.stopPropagation(); 
                
        var optionsList = cmp.find("optionList");
        $A.util.addClass(optionsList, 'slds-hide');
    },
    checkForNullValues: function(cmp,event,helper){
        
        if(cmp.get('v.selectedvalue').length == 0){
            $A.util.addClass(cmp.find('multiSelectInput'),'slds-has-error');
            return true;
        } else{
            helper.clearError(cmp,event,helper);
            return false;
        }
    },
    clearError : function(cmp,event,helper){
        if($A.util.hasClass(cmp.find('multiSelectInput'),'slds-has-error')){
            $A.util.removeClass(cmp.find('multiSelectInput'),'slds-has-error');
        }
    },
    setSelectedValueString : function(component,event,helper,selectedValues){
        var selectedValueString = '';
        if(selectedValues.length){
            for(var i=0;i<selectedValues.length;i++){
               // console.log('selectedValues::',selectedValues[i].value);
                if(selectedValues[i].value){
                     selectedValueString += selectedValues[i].value + ';';
                }
            }
        }
        component.set('v.value',selectedValueString);
    },
    setSelectedValue : function(component,event,helper,selectedValueString){
        //console.log('multiSelect',selectedValueString);
        var selectedList = selectedValueString.split(";");
        var selectedValueList = [];
        for(var i=0;i<selectedList.length;i++){
            if(selectedList[i]){
                var temp = {
                    'label' : selectedList[i],
                    'value' : selectedList[i]
                };
                selectedValueList.push(temp);
            }
        }
        component.set('v.selectedvalue',selectedValueList);
    }
})