({	
    doInit : function(component, event, helper) {
        if(component.get('v.value')){
            helper.setSelectedValue(component,event,helper,component.get('v.value'));
        } else{
            component.set('v.selectedvalue',[]);
        }
        var optionsList = component.get("v.optionsList") || [],
            selectedList = JSON.parse(JSON.stringify(component.get("v.selectedvalue"))) || [],
        	options = [];
        var selectedValues = [];
        console.log('selectedList::',selectedList);
        if(selectedList.length){
            for(var i=0;i<selectedList.length;i++){
                selectedValues.push(selectedList[i].label.toLowerCase());
            }
        }
        console.log('selectedValues::',selectedValues)
        var hasValue = false;
        if( Array.isArray(optionsList)){
            for(var i=0;i<optionsList.length;i++){
                var opt = {};
                
                opt.label = optionsList[i];
                console.log(':::',optionsList[i].label);
                if(optionsList[i].label != '' && selectedValues.indexOf(optionsList[i].label.toLowerCase()) != -1){
                    opt.selected = 'true';
                    hasValue = true;
                }
                else{
                     opt.selected = 'false';
                }
                options.push(opt);
            }
        }
        console.log('options::',options);
        component.set("v.options", options);
        if(!hasValue){
            component.set('v.selectedvalue',[]);
        }
        if(selectedList.length){
            helper.setSelectedValueString(component,event,helper,selectedList);
        }
    },
	showList : function(component, event, helper) {
		event.stopPropagation(); 
        var optionsList = component.find("optionList");
        $A.util.toggleClass(optionsList, 'slds-hide');
        helper.clearError(component,event,helper);
    },
    selectOption : function(component, event, helper) {
        
        event.stopPropagation(); 
        var index = event.currentTarget.getAttribute("data-index"),
        	options = component.get("v.options"),
       		selectedvalue = component.get("v.selectedvalue");
        if(options[index].selected == 'true'){
            options[index].selected = 'false';
            for(var i in selectedvalue){
                console.log(selectedvalue[i].label,options[index].label.label);
                if((selectedvalue[i].label).toLowerCase() == (options[index].label.label).toLowerCase()){
                    selectedvalue.splice(i, 1);
                }
            }
        }
        else{
            options[index].selected = 'true';
            if(options[index].label){
                selectedvalue.push(options[index].label);
            }
        }
        component.set("v.options", options);
        component.set("v.selectedvalue", selectedvalue);
        helper.setSelectedValueString(component,event,helper,selectedvalue);
    },
    checkForValue: function(cmp,event,helper){
        return helper.checkForNullValues(cmp,event,helper);
    }
})