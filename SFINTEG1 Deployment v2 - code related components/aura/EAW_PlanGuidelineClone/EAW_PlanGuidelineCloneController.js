({
	init: function (cmp, event, helper) {
		
        cmp.set('v.spinner', true);
		let action = cmp.get('c.getPlanGuideline');
		action.setParams({
			'recordId': cmp.get('v.recordId')
		});
		action.setCallback(this, function (response) {
					
            var state = response.getState();
			
            if (state == 'SUCCESS') {
				let planGuideline = response.getReturnValue();
                if (planGuideline.Status__c) planGuideline.Status__c = 'Draft';
                if (planGuideline.Version__c) planGuideline.Version__c = 1;
                cmp.set('v.planGuideline', planGuideline);
                cmp.set('v.planList', planGuideline)
				helper.setFieldValues(cmp, event, helper);
				cmp.set('v.showModal', true);
            } 
            else if (state == 'ERROR') {
				let errors = response.getError();
                let handleErrors = helper.handleErrors(errors, component, event, helper);
                if(handleErrors) {
                    helper.errorToast(handleErrors);
                }
			}
		});
		cmp.set('v.spinner', false);
		$A.enqueueAction(action);
	},
	
	clone: function (cmp, event, helper) {
		
		cmp.set('v.spinner', true);
        let action = cmp.get('c.clonePlanAndWindows');
		action.setParams({
			'clonePlan': JSON.stringify(cmp.get('v.planList'))
		});
		action.setCallback(this, function (response) {
			
            let state = response.getState();
		
            if (state == 'SUCCESS') {
                
                let results = response.getReturnValue();
                let message = cmp.get('v.planGuideline').Name + ' was cloned sucessfully';
				
                helper.closeModal(cmp);
				helper.toast(state, message);
            
                let navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId": results[0].Id
                });
                navEvt.fire();
            } 
            else if(state == 'ERROR') {
                helper.closeModal(cmp);
				let errors = response.getError();
                let handleErrors = helper.handleErrors(errors, cmp, event, helper);
                if(handleErrors) {
                    helper.errorToast(handleErrors);
                }
			}
            cmp.set('v.spinner', false);
		});
		$A.enqueueAction(action);
	},
    close: function (cmp, event, helper) {
        helper.closeModal(cmp);
	},
})