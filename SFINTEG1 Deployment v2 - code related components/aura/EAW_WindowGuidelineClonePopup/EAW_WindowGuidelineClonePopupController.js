({
    cloneRecords : function(component, event, helper) {
        try {
            //let windowGuidelines = component.get('v.windowGuidelineSelect');
            let windowGuidelines = component.get('v.selectedRows');
			
            if(windowGuidelines.length === 1) {

                let cloneWindowGuidelines = component.get("c.cloneWindowGuidelines");
                cloneWindowGuidelines.setParams({windowGuidelineIds: windowGuidelines});
                console.log(windowGuidelines);
                cloneWindowGuidelines.setCallback(this, function(response) {
                    let windowData = component.get('v.windowData');
                    let state = response.getState();

                    if(state === 'SUCCESS') {
                        let results = response.getReturnValue();
                        console.log(results);

                        for(let clonedWindowGuideline of results) {
                            clonedWindowGuideline.link = '/one/one.app?sObject/'+ clonedWindowGuideline.Id + '/view#/sObject/' + clonedWindowGuideline.Id + '/view';
                            windowData.push(clonedWindowGuideline);
                        }

                        component.set('v.windowData', windowData);

                        let navigateEvent = $A.get('e.force:navigateToSObject');

                        navigateEvent.setParams({
                            'recordId': results[0].Id
                        });

                        navigateEvent.fire();
                    } else if (state === 'ERROR') {
                        let errors = response.getError();
                        let handleErrors = helper.handleErrors(errors, component, event, helper);
                        if(handleErrors) {
                            helper.errorToast(handleErrors);
                        }
                    } else {
                        helper.errorToast('There was an error while trying to clone this title.');
                    }
                });
                $A.enqueueAction(cloneWindowGuidelines);
            } else {
                helper.errorToast('You can only clone one Window Guideline at a time.');
            }
        } catch(e) {
            console.log(e);
            helper.errorToast('There was an error while trying to clone this title.');
        }
    },

    /** Methods below hide/show the popup*/
    showPopup : function(component, event, helper) {
        document.getElementById("clone-window-guideline-modal").style.display = "block";
    },
    hidePopup : function(component, event, helper){
        document.getElementById("clone-window-guideline-modal").style.display = "none";
    }
})