({
	doInit : function(component, event, helper) {
		
        // Set column header names for data table
        /*component.set('v.titleAttributeColumns', [
            {label: 'Int\'l Title ID', fieldName: 'Title_Id', type: 'url',
             typeAttributes: { label: {fieldName: 'FIN_PROD_ID__c'}, target: '_blank'}, initialWidth: 200},
            {label: 'Title', fieldName: 'Name', type: 'text', initialWidth: 200},
            {label: 'Int\'l Product Type', fieldName: 'PROD_TYP_CD_INTL', type: 'text', initialWidth: 200},	
            {label: 'Product Type', fieldName: 'PROD_TYP_CD', type: 'text', initialWidth: 200},            
            {label: 'Division', fieldName: 'FIN_DIV_CD', type: 'text', initialWidth: 200},
            {label: 'Title Tag(s)', fieldName: 'DIR_NM__c', type: 'text', initialWidth: 200},	// Title Tags field temporarily held in Director Name
            {label: 'Release Date', fieldName: 'FRST_REL_DATE__c', type: 'text', initialWidth: 200}
        ]);*/
        helper.fieldSet(component);
	},
    
    searchData: function(component, event, helper) {
        helper.search(component);
    },
    
    clearSearch: function(component, event, helper) {
    	component.set('v.titleAttributeList', []);
        component.set('v.selectedTitles', []);
    },
    
    /** Methods below control Paste Title popup*/
    showPasteTitleModal : function(component, event, helper) {
        document.getElementById("EAW_TitlePasteSearchComponent").style.display = "block";
    },
    hidePasteTitleModal : function(component, event, helper){
        document.getElementById("EAW_TitlePasteSearchComponent").style.display = "none";
    },
    
    /** Methods below control Advanced Title Search popup*/
    showAdvancedTitleSearchModal : function(component, event, helper) {
        document.getElementById("EAW_TitleAdvancedSearchComponent").style.display = "block";
    },
    hideAdvancedTitleSearchModal : function(component, event, helper){
        document.getElementById("EAW_TitleAdvancedSearchComponent").style.display = "none";
    },
    
    /** Methods Below control SPINNER*/
    showSpinner:function(component){
        component.set('v.showSpinner',true);
    },
    hideSpinner:function(component){
        component.set('v.showSpinner',false);
    },
    dataChangeHandler: function( component, event, helper ){
    	helper.retrieveTVDBoxOfficeData(component);
    }
})