({
    search: function(component) {
    	// Fetch title data off of inputted titles
        var selectedTitles = component.get("v.selectedTitles");
        var action = component.get("c.getTitleAttributes");
        
        action.setParams({"titleIdsString" : this.collectIds(selectedTitles)});
        
        action.setCallback(this, function(response) {
            
            let results = response.getReturnValue();
            
            if(results != null) {
                for(let result of results) {
                	result.Title_Id = '/one/one.app?sObject/'+ result.Id + '/view#/sObject/' + result.Id + '/view';
                    
                    if(result.PROD_TYP_CD_INTL__r && result.PROD_TYP_CD_INTL__r.Name) {
                    	result.PROD_TYP_CD_INTL = result.PROD_TYP_CD_INTL__r.Name;
                    }
                    		
                    if(result.PROD_TYP_CD__r && result.PROD_TYP_CD__r.Name) {
                    	result.PROD_TYP_CD = result.PROD_TYP_CD__r.Name;
                    }
                    
                    if(result.FIN_DIV_CD__r && result.FIN_DIV_CD__r.Name) {
                        result.FIN_DIV_CD = result.FIN_DIV_CD__r.Name;
                    }
                }
                console.log(results);
            	component.set("v.titleAttributeList", results);
            } 
        });
        $A.enqueueAction(action);    
    },
    
	collectIds : function(selectedResults) {
        var collectedIds = [];
        var collectedIdsString = "";
        
        if(selectedResults && selectedResults.length > 0) {
            for(var result of selectedResults) {
                collectedIds.push(result.id);
            }
        }
        console.log("collect ids: " + collectedIds[0]);
        
        for(var id of collectedIds){
            console.log(id);
            collectedIdsString += id + ' ';
        }
        
        console.log("collect id string: " + collectedIdsString);
        
        return collectedIdsString; 
    },
    
    retrieveTVDBoxOfficeData : function( component ){
    	var titleIds = [];
    	var titleIdFormattedString = '';
    	var titleRecords = component.get("v.data");
    	
    	if( titleRecords.length > 0 ){
    		
    		for( var titleRec of titleRecords ){
    			titleIds.push('\''+titleRec.Id+'\'');
    		}
    		titleIdFormattedString = titleIds.join(",");
    		
    		var action = component.get("c.searchForTVDBoxOffices");
        
	        action.setParams({"titleIdsString" : titleIdFormattedString});
	        
	        action.setCallback(this, function(response) {
                var state = response.getState();
                var results= response.getReturnValue();
                //this.hideSpinner(component)
            	
            	console.log('state :::'+state);
            	console.log('results :::');
            	console.dir(results);
            	
                if(state === 'SUCCESS') {
                	
                	var data = new Array();
                    console.dir(response.getReturnValue());
                    for(let col of response.getReturnValue())
                    {
                    	for(let colValue of col)
                    		data.push(colValue);
                    }
                    
                    var col = component.get('v.boxOfficeColumns');
                    component.set("v.boxOfficeList", this.getLookupFields(col, data, 'Name'));
                    component.set("v.myDataOriginal", JSON.parse(JSON.stringify(component.get("v.boxOfficeList"))));
                    
                    console.log(' Search Results Data :::');
                	console.dir(component.get("v.boxOfficeList"));
                	
                	console.log(' Columns $$$ :::');
                	console.dir(col);
                	console.dir(component.get('v.boxOfficeColumns'));
                	
                } else if(state === 'ERROR') {
                    //component.set('v.planData', []);
                    component.set('v.boxOfficeList', []);
                }
            });
	        $A.enqueueAction(action);        
    		
    	} else {
    	
    	}
    	
    	console.log(':::::: TitleIds ::::::::'+titleIds);
    	console.log(':::::: titleIdFormattedString :::::::'+titleIdFormattedString);
    },
    
    getLookupFields: function (col, data, fName) {
    
        for (var i = 0, clen = col.length; i < clen; i++) {
        
            if (col[i]['type'] == 'reference') {
            
                for (var j = 0, rlen = data.length; j < rlen; j++) {
                
                    var str = col[i]['fieldName'];
                    
                    if( str.indexOf('__c') != -1 ){
                    	str =  str.replace('__c', '__r');
                    } else{
                    	str =  str.replace('Id', '');
                    }
                    
                    
                    var fValue = data[j][str];
                    if (fValue != undefined) {
                        // console.dir(fValue[fName]);
                        data[j][col[i]['fieldName']] = fValue[fName];
                    }
                }
            }
        }
        return data;
    },
    
    fieldSet: function (component) {
        try {
            
            var action = component.get("c.getFields");
            action.setParams(
	            {
	                "objFieldSetMap": {
	                    "TVD_Box_Office__c": "Territory_Attributes_Table_Fields"
	                }
	            }
	        );

            action.setCallback(this, function (response) {
                var state = response.getState();

                if (state === 'SUCCESS') 
                {
                    var columns = new Array();
                    console.dir(response.getReturnValue());
                    for(let col of response.getReturnValue())
                    {
                    	for(let colName of col)
                    		columns.push(colName);
                    }
                    console.log('Columns:');
                    console.dir(columns);
                    
                    component.set("v.boxOfficeColumns", columns);
                } 
                else if (state === 'ERROR') {
                    var errors = response.getError();

                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " +
                                errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                } else {
                    console.log('Something went wrong, Please check with your admin');
                }
            });
            $A.enqueueAction(action);
        } catch (e) {
            console.log('Exception:' + e);
        }
    },
})