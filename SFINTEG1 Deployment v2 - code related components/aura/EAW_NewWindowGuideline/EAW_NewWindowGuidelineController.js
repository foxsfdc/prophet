({
    init : function(component, event, helper) {
        var collectPicklistsValues = component.get("c.collectPicklistsValues");

        collectPicklistsValues.setCallback(this, function(response) {
			var picklistsValues = response.getReturnValue();
            console.log(picklistsValues);
            var licenseTypes = picklistsValues[0];
			var medias = picklistsValues[1];
			var territories = picklistsValues[2];
			var languages = picklistsValues[3];
            
            component.set("v.medias", medias);
        });
		$A.enqueueAction(collectPicklistsValues);
        
        helper.resetNewWindowGuidelineFields(component);
	},
    saveNewWindowGuideline : function(component, event, helper) {
        var formattedWindowGuideline = component.get("v.newWindowGuideline");
        var planId = component.get('v.planRecordId');
        
        if(formattedWindowGuideline.Name.length > 0) {
            var newWindowGuideline = component.get("c.insertWindowGuideline");
            newWindowGuideline.setParams({windowGuideline: formattedWindowGuideline, planRecordId: planId});
            
            newWindowGuideline.setCallback(this, function(response) {
                var planWindowGuidelineJunction = helper.formatPlanWindowGuidelineJunction(response.getReturnValue());
                var windowGuidelineList = component.get("v.windowGuidelineList");
                
                windowGuidelineList.push(planWindowGuidelineJunction);
                component.set("v.windowGuidelineList", windowGuidelineList);
            });
            $A.enqueueAction(newWindowGuideline);
            
            $A.get('e.force:refreshView').fire();
            helper.hideNewWindowGuidelineModal(component, event, helper);
        }
    },
    updateInputtedName : function(component, event, helper) {
        var newWindowGuideline = component.get("v.newWindowGuideline");
        var inputtedName = component.find("new-window-guideline-input-name").get("v.value");

        newWindowGuideline.Name = inputtedName;
    },
    updateSelectedMedia : function(component, event, helper) {
        var newWindowGuideline = component.get("v.newWindowGuideline");
        var selectedMedia = component.find("new-window-guideline-select-media").get("v.value");

        newWindowGuideline.Media__c = selectedMedia;
    },
    updateLookupCustomer : function(component, event, helper) {
        var newWindowGuideline = component.get("v.newWindowGuideline");
        var lookupCustomer = component.find("new-window-guideline-lookup-customer").get("v.value");
        
        //LRCC-1560 - Replace Customer lookup field with Customer text field
        //newWindowGuideline.Customer__c = lookupCustomer;
        newWindowGuideline.Customers__c = lookupCustomer;
    },
    updateTextAreaDescription : function(component, event, helper) {
        var newWindowGuideline = component.get("v.newWindowGuideline");
        var textAreaDescription = component.find("new-window-guideline-textarea-description").get("v.value");

        newWindowGuideline.Description__c = textAreaDescription;
    },
	updateSelectedWindowType : function(component, event, helper) {
        var newWindowGuideline = component.get("v.newWindowGuideline");
        var selectedWindowType = component.find("new-window-guideline-select-window-type").get("v.value");

        newWindowGuideline.Window_Type__c = selectedWindowType;
    },
	updateInputtedVersion : function(component, event, helper) {
        var newWindowGuideline = component.get("v.newWindowGuideline");
        var inputtedVersion = component.find("new-window-guideline-input-version").get("v.value");

        newWindowGuideline.Version__c = inputtedVersion;
    },
	updatedSelectedVersionStatus : function(component, event, helper) {
        var newWindowGuideline = component.get("v.newWindowGuideline");
        var selectedVersionStatus = component.find("new-window-guideline-select-version-status").get("v.value");

        newWindowGuideline.Version_Status__c = selectedVersionStatus;
    },
	updateTextAreaVersionNotes : function(component, event, helper) {
        var newWindowGuideline = component.get("v.newWindowGuideline");
        var textAreaVersionNotes = component.find("new-window-guideline-textarea-version-notes").get("v.value");

        newWindowGuideline.Version_Notes__c = textAreaVersionNotes;
    },
    hideNewWindowGuidelineModal : function(component, event, helper) {
        helper.hideNewWindowGuidelineModal(component);
    }
})