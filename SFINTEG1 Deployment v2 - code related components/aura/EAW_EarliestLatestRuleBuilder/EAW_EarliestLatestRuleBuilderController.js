({
	init : function(cmp, event, helper) {
               
        if(cmp.get('v.newRuleDetailJSON') && cmp.get('v.newRuleDetailJSON').ruleDetail && cmp.get('v.newRuleDetailJSON').ruleDetail.Id && !cmp.get('v.newRuleDetailJSON').nodeCalcs.length){
           // console.log(cmp.get('v.newRuleDetailJSON').ruleDetail);
            if(cmp.get('v.newRuleDetailJSON').ruleDetail.Condition_Met_Days__c || !cmp.get('v.newRuleDetailJSON').ruleDetail.Condition_Timeframe__c){
            	cmp.get('v.newRuleDetailJSON').ruleDetail.Condition_Type__c = ''; 
            }
            helper.setRuleDetailNodes(cmp,event,helper);
            if(cmp.get('v.newRuleDetailJSON') && cmp.get('v.newRuleDetailJSON').ruleDetail.Rule_Order__c && cmp.get('v.newRuleDetailJSON').ruleDetail.Nest_Order__c){
                cmp.set('v.ruleOrder',cmp.get('v.newRuleDetailJSON').ruleDetail.Rule_Order__c);
                cmp.set('v.nestOrder',cmp.get('v.newRuleDetailJSON').ruleDetail.Nest_Order__c);
            }
        } else{
            if(cmp.get('v.newRuleDetailJSON') && cmp.get('v.newRuleDetailJSON').ruleDetail && cmp.get('v.newRuleDetailJSON').ruleDetail.Rule_Order__c && cmp.get('v.newRuleDetailJSON').ruleDetail.Nest_Order__c){
                cmp.set('v.ruleOrder',cmp.get('v.newRuleDetailJSON').ruleDetail.Rule_Order__c);
                cmp.set('v.nestOrder',cmp.get('v.newRuleDetailJSON').ruleDetail.Nest_Order__c);
            }
        }
        
        if(cmp.get('v.conditionTimeFrame')){
            cmp.set('v.options',[{'label': 'Earliest', 'value':'True_Earliest'},
                                  {'label': 'Latest', 'value': 'True_Latest'},]);
        } else {
             cmp.set('v.options',[{'label': 'Earliest', 'value':'False Earliest'},
                                  {'label': 'Latest', 'value': 'False Latest'},]);
        }
        
	},
    addContext : function(cmp,event,helper){
        var selectedMenuItemValue = event.getParam("value");
        var newRuleDetailJSON = cmp.get('v.newRuleDetailJSON');
        var newRuleDetail = JSON.parse(JSON.stringify(cmp.get('v.newRuleDetail')));
        
        if(cmp.get('v.parentNode') && !cmp.get('v.toSelect') && newRuleDetailJSON.dateCalcs && newRuleDetailJSON.nodeCalcs && !newRuleDetailJSON.dateCalcs.length && !newRuleDetailJSON.nodeCalcs.length){
           console.log('inside');
            if(selectedMenuItemValue == 'Add Date Calc'){ 
                newRuleDetailJSON.ruleDetail.Condition_Type__c = '';
                
            } else if(selectedMenuItemValue == 'TBD' || selectedMenuItemValue == 'Perpetuity'){ // LRCC - 1154
                newRuleDetailJSON.ruleDetail.Condition_Type__c = '';
                newRuleDetailJSON.ruleDetail.Condition_Date_Text__c = selectedMenuItemValue;
               
            } else{
                newRuleDetailJSON.ruleDetail.Condition_Type__c = 'Else/If';
                cmp.set('v.toSelect',true);
            }
        } else{
            if(selectedMenuItemValue == 'Add Date Calc'){            
                
                newRuleDetail.Rule_Order__c = cmp.get('v.ruleOrder');
                newRuleDetail.Nest_Order__c = cmp.get('v.nestOrder');
                newRuleDetailJSON.dateCalcs.push(newRuleDetail);
            } else if(selectedMenuItemValue == 'TBD' || selectedMenuItemValue == 'Perpetuity'){ // LRCC - 1154
                newRuleDetail.Rule_Order__c = cmp.get('v.ruleOrder');
                newRuleDetail.Nest_Order__c = cmp.get('v.nestOrder');
                newRuleDetail.Condition_Date_Text = selectedMenuItemValue;
                newRuleDetailJSON.dateCalcs.push(newRuleDetail);
            } else {
                newRuleDetail.Rule_Order__c = cmp.get('v.ruleOrder');
                newRuleDetail.Nest_Order__c = cmp.get('v.nestOrder')+ 1;
                var nodeRuleDetail = {
                    'ruleDetail' : newRuleDetail,
                    'dateCalcs' : [],
                    'nodeCalcs' : []                
                }; 
                newRuleDetailJSON.nodeCalcs.push(nodeRuleDetail);
            } 
        }
        console.log(newRuleDetailJSON);
        cmp.set('v.newRuleDetailJSON',newRuleDetailJSON);
    },
    get : function(cmp){
        console.log(cmp.get('v.newRuleDetailJSON'));
    },
    removeNodeCalc : function(cmp,event,helper){
         var newRuleDetailJSON = cmp.get('v.newRuleDetailJSON');
        var indexPosition = event.getSource().get('v.name');
        var nodeToDelete = cmp.get('v.nodeToDelete');
        nodeToDelete.isDateCalc = false;
        nodeToDelete.index = indexPosition;
        cmp.set('v.nodeToDelete',nodeToDelete);
       
        if(newRuleDetailJSON.nodeCalcs[indexPosition] && newRuleDetailJSON.nodeCalcs[indexPosition].ruleDetail.Id){
             helper.openModal(cmp,event,helper,'Sure want to delete this RuleDetail and it\'s DateCalcs ?');
        } else{
            console.log(newRuleDetailJSON.nodeCalcs[indexPosition]);
            newRuleDetailJSON.nodeCalcs.splice(indexPosition,1);
            cmp.set('v.newRuleDetailJSON',newRuleDetailJSON);
        }              
    },
    removeDateCalc : function(cmp,event,helper){
        var newRuleDetailJSON = cmp.get('v.newRuleDetailJSON');
        var indexPosition = event.getSource().get('v.name');
        var nodeToDelete = cmp.get('v.nodeToDelete');
        nodeToDelete.isDateCalc = true;
        nodeToDelete.index = indexPosition;
        cmp.set('v.nodeToDelete',nodeToDelete);
        if(newRuleDetailJSON.dateCalcs[indexPosition] && newRuleDetailJSON.dateCalcs[indexPosition].Id){
            helper.openModal(cmp,event,helper,'Sure want to delete this DateCalc ?');
        } else{
            console.log(newRuleDetailJSON.dateCalcs[indexPosition]);
            newRuleDetailJSON.dateCalcs.splice(indexPosition,1);
            cmp.set('v.newRuleDetailJSON',newRuleDetailJSON);
        }  
    },
    saveRuleDetails : function(cmp,event,helper){
        var cmpEvent = cmp.getEvent("ruleDetailSave");
        cmpEvent.setParams({
            'ruleType' : 'earliestLatest'
        });
        cmpEvent.fire();
    },
    deleteRuleDetail : function(cmp,event,helper){
        event.stopPropagation();
        var newRuleDetailJSON = cmp.get('v.newRuleDetailJSON');
        if(cmp.get('v.nodeToDelete').isDateCalc){
            helper.deleteRuleDetailById(cmp,event,helper, newRuleDetailJSON.dateCalcs[cmp.get('v.nodeToDelete').index].Id,true);
            newRuleDetailJSON.dateCalcs.splice(cmp.get('v.nodeToDelete').index,1);
        } else{
            helper.deleteRuleDetailById(cmp,event,helper, newRuleDetailJSON.nodeCalcs[cmp.get('v.nodeToDelete').index].ruleDetail.Id,false);
            newRuleDetailJSON.nodeCalcs.splice(cmp.get('v.nodeToDelete').index,1);
        }
        cmp.set('v.newRuleDetailJSON',newRuleDetailJSON);
    }
})