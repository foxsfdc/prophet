({
    doInit : function(component, event, helper) {
       // console.log('column:::;;;',component.get('v.column').fieldName);
       // console.log('inputTable:::;;;',component.get('v.inputTable'));
       // console.log('updateable:::;;;',component.get('v.updateable'));
       // component.set('v.spinner',true);
        var column = component.get("v.column");
        var data = component.get("v.data"); 
         console.log('data:::;tooltip;;',data);
        // Tags string to object
        if(!$A.util.isEmpty(column) && !$A.util.isEmpty(column.fieldName)){
            component.set("v.canShowData",true);
            
            if (data[column.fieldName] && (typeof (data[column.fieldName]) == 'object') && data[column.fieldName].length > 0) {
                //console.log('datavalue:::',data[column.fieldName]);
                component.set('v.selectedRecords',data[column.fieldName]);
                var temp = [];
                data[column.fieldName].forEach(function(element) {
                    var title;
                    if (element.Name) { title = element.Name; }
                    else { title = element.title; }
                    temp.push(title);
                });
                component.set("v.displayValue",temp.join(', '));
            } else  {
                //LRCC-1018
                component.set("v.tooltip",data['indication']);
                component.set("v.idWithcol",data['Id']+'~'+column.fieldName);
                console.log('idwith col:::',component.get("v.idWithcol"));
                component.set('v.genericTooltipCol',data['indication'].tooltip.indexOf(column.fieldName) > -1);
                component.set('v.genericShowRedCol',data['indication'].redcolor.indexOf(column.fieldName) > -1);
               
                
                component.set("v.displayValue",data[column.fieldName]);     
            }
            component.set('v.updateable',column.updateable);    
        }
    },
    getBaseUrl : function(component,event,helper) {
        var action = component.get("c.getBaseUrl");
        action.setCallback(this, function(response) {
            var state = response.getState();
            var result = response.getReturnValue();
            if (state === 'SUCCESS') 
            {
                component.set("v.baseUrl",result);
                
            } else if (state === 'ERROR') {
                console.log(JSON.stringify(response.getError()));
            }
        });
        $A.enqueueAction(action);
    },
    editTable : function(component, event, helper) {
        //console.log('editTable:::',event.getParam("editMode"));
        component.set('v.inputTable',event.getParam("editMode"));
    },
    massUpdateTable : function(component, event, helper) {
        var column = component.get("v.column");
        var data = component.get('v.data');
        component.set('v.displayValueEdit[0].title',data[column.fieldName]);
    },
    getTypeAheadData : function(component, event, helper) {
        //upd
        
        var column = component.get("v.column");
        var data = component.get("v.data");
         //console.log('getTypeAheadData:::');
        if(!$A.util.isEmpty(component.get("v.displayValueEdit"))){
            
            // LRCC:1096 start
            var tagTitles = [];
            for (var i=0;i<component.get("v.displayValueEdit").length;i++) {
                tagTitles.push(component.get("v.displayValueEdit")[i].title);
            }
            var title = tagTitles.join(', ');
           // console.log('title:::',title);
            
             //console.log('data:::',JSON.parse(JSON.stringify(data)));
             //console.log('column.fieldName:::',column.fieldName);
            // console.log('data[column.fieldName]:::',data[column.fieldName]);
            //upd
            //LRCC-1327 - issue 1 
            if ((data[column.fieldName] && (typeof (data[column.fieldName]) == 'object') && data[column.fieldName].length != undefined)||component.get("v.displayValueEdit").length) {
                //console.log('tag;;;;');
                data[column.fieldName] = component.get("v.displayValueEdit");
            } else {
                data[column.fieldName] = title;   
            }
            component.set("v.displayValue",title);
            component.set("v.data",data);
            // LRCC:1096 end
            
        } else if(event.getParam("value")[0] != event.getParam("oldValue")[0]){            
            
            //upd
            if (data[column.fieldName] && (typeof (data[column.fieldName]) == 'object') && data[column.fieldName].length != undefined) {
                data[column.fieldName] = [];   
            } else {
                data[column.fieldName] = '';   
            }
            component.set("v.displayValue",'');            
            component.set("v.data",data);
        }
        
        // LRCC:1096 start
        //console.log('rec::::', component.get("v.recordMap"));
        //console.log('displayValueEdit::::', component.get("v.displayValueEdit"));
        
        if (data[column.fieldName] && (typeof (data[column.fieldName]) == 'object') && data[column.fieldName].length != undefined) {
            
            var recordMap = component.get("v.recordMap") || {};
            var tagIds = [];
            for(var i=0;i<component.get("v.displayValueEdit").length;i++) {
                
                if (component.get("v.displayValueEdit")[i].id) {
                    tagIds.push(component.get("v.displayValueEdit")[i].id);
                }
            }
            
            if(data && data.Id) {
                
                var dataId = data.Id;
                recordMap[dataId] = tagIds;
                //console.log(';;recordMap::',JSON.parse(JSON.stringify(recordMap)));
                component.set("v.recordMap", recordMap);  
            }
        }
        // LRCC:1096 end
    },
    //LRCC - 1285 and LRCC - 1015
    showInfo : function(component,event,helper) {
        var column = component.get("v.column");
        var data = component.get("v.data");
        if(column.fieldName == 'Allow_Manual__c' && data[column.fieldName] == false) {
            helper.showToast('info', 'There may be manual dates entered for this release date. These will not be overwritten or removed.');
        }
    },
    getNonTypeaheadData:function(component,event,helper) {
        //Tags string to object
        //console.log('getNonTypeaheadData::::');
        if(!$A.util.isEmpty(component.get("v.displayValue"))){
            var column = component.get("v.column");
            var title = component.get("v.displayValue");
            var data = component.get("v.data");
            
            //upd
            //LRCC-1327 ||data[column.fieldName] == null removed
            if (data[column.fieldName] && (typeof (data[column.fieldName]) == 'object') && data[column.fieldName].length != undefined) {
                
                if (component.get('v.displayValueEdit').length > 0) {
                    data[column.fieldName] = component.get('v.displayValueEdit');
                } else {
                    data[column.fieldName] = component.get("v.selectedRecords");
                }
            } else {
                data[column.fieldName] = title; 
                if(event.getParam("value") != event.getParam("oldValue")){ // LRCC - 1082
                    if(column.fieldName == 'Manual_Date__c' && data['Status__c']) { // LRCC - 1638
                        if(data['Status__c'] == 'Firm'){
                            data['Temp_Perm__c'] = 'Permanent'; 
                            var manualDateChangeEvent = component.getEvent("manualDateChangeEvent");                     
                            manualDateChangeEvent.fire(); 
                           // var message = 'The Release Date Status is Firm. Updating the Manual date will update the Release Date.Do you want to continue ?';
                            //component.find('modifierPrompt').openModal(event.getParam("oldValue"),event.getParam("value"),message,'Manual_Date__c');
                        } else if(data['Status__c'] == 'Not Released' || data['Status__c'] == 'Estimated'){
                            data['Temp_Perm__c'] = 'Temporary';
                            var manualDateChangeEvent = component.getEvent("manualDateChangeEvent");                     
                            manualDateChangeEvent.fire(); 
                        }
                        //console.log('data:::',JSON.parse(JSON.stringify(data)));
                        
                    }
                    /*
                    //LRCC-1447
                    if(data['Status__c'] == 'Firm' && (column.fieldName == 'Start_Date__c' || column.fieldName == 'End_Date__c' || column.fieldName == 'Outside_Date__c' || column.fieldName == 'Tracking_Date__c')) {
                        
                        var message = 'The window schedule status is Firm. Updating the date will update the window schedule date. Do you want to continue?';
                        component.find('modifierPrompt').openModal(event.getParam("oldValue"),event.getParam("value"),message,column.fieldName);
                    }
                    */
                }
            }            
            console.log('::::::::::::;data::::'+JSON.stringify(data));
            component.set("v.data",data);
            
        } else if(event.getParam("value") != event.getParam("oldValue")){
            
            component.set("v.displayValue",'');
            var column = component.get("v.column");
            var data = component.get("v.data");
           // console.log('column:::',JSON.parse(JSON.stringify(column)));
            //console.log('data:::',JSON.parse(JSON.stringify(data)));
            //upd
            if (data[column.fieldName] && (typeof (data[column.fieldName]) == 'object') && data[column.fieldName].length != undefined) {
                data[column.fieldName] = [];
            } else {
                data[column.fieldName] = ''; 
                //LRCC-1445
                if(column.fieldName == 'Manual_Date__c') {
                    data['Temp_Perm__c'] = '';
                   // console.log('data:::',JSON.parse(JSON.stringify(data)));
                    var manualDateChangeEvent = component.getEvent("manualDateChangeEvent");                     
                    manualDateChangeEvent.fire(); 
                }
            }
            //LRCC-1447
            if(column.fieldName == 'Start_Date__c' || column.fieldName == 'End_Date__c'){
                
                var dateValidationObj = {oldValue : event.getParam("oldValue"),
                                         newValue : event.getParam("value"),
                                         ErrorMessage: '',
                                         columnName :component.get("v.column").fieldName }
                    
               component.set("v.dateValidationObj",dateValidationObj); 
                
            }else{
               // console.log('data:::',JSON.parse(JSON.stringify(data)));
                component.set("v.data",data);  
            }
            
        }
    },
    proceedToUpdate :function(component, event, helper) {
       // console.log('dateChanged:::::::::');
          /*var WindowDateChangeEvent = component.getEvent("EAW_WindowDateChangeEvent");
                WindowDateChangeEvent.setParams({"columnName" : component.get("v.column").fieldName,
                                                 "newRecord":event.getParam("value"),
                                                 "oldRecord":event.getParam("oldValue")});
                WindowDateChangeEvent.fire();*/
        
    },
    removeDateConfrimModalCmp :function(component, event, helper) {
       // console.log('dateChanged:::::::::');
        
    },
    handleDataModifyConfirmation : function(cmp,event,helper){ // LRCC - 1638
       // console.log(':::::'+JSON.stringify(event.getParams()));
        
        var params = event.getParams();
        if(params.field){
            var data = cmp.get("v.data");
            if(params.field == 'Manual_Date__c'){
                data['Manual_Date__c'] = params.valueToChange;
                if(params.confirmation){
                    data['Temp_Perm__c'] = 'Permanent';
                    var manualDateChangeEvent = cmp.getEvent("manualDateChangeEvent");                     
                    manualDateChangeEvent.fire(); 
                }
            }/* else if(params.field == 'Start_Date__c' || params.field == 'End_Date__c' || params.field == 'Outside_Date__c' ||params.field == 'Tracking_Date__c') { //LRCC-1447
                data[params.field] = params.valueToChange;
            }*/
            cmp.set('v.data',data);
        }
    },
    checkTootip  :function(component, event, helper) {
        
        console.log('overcell::::::::',component.get("v.overcell")); 
        console.log(event.currentTarget.getAttribute("data-attriVal")); 
        if(component.get("v.overcell") && component.get("v.overcell") != event.currentTarget.getAttribute("data-attriVal")){
            component.set("v.overcell",''); 
        }else{
           
            component.set("v.overcell",event.currentTarget.getAttribute("data-attriVal")); 
            var tooltip = component.get("v.tooltip");
            var overcell=  component.get("v.overcell");
            var colname;
            if(overcell){
                
               colname = overcell.split("~")[1];
            }
            
            console.log('colname::',colname);
           
            if(colname){
                console.log('tooltip.tooltipMsg[colname]',tooltip.tooltipMsg[colname]);
                console.log('tooltip.tooltip.indexOf(colname)',tooltip.tooltip.indexOf(colname));
                component.set('v.genericTooltipCol',tooltip.tooltip.indexOf(colname) > -1);
                component.set('v.genericTooltipMSg',tooltip.tooltipMsg[colname]);
            }
           
            
        }
    },
    removeTooltip :function(component, event, helper) {
        
        console.log('out:::::',event.currentTarget.getAttribute("data-attriVal")); 
        component.set("v.overcell",''); 
        //component.set('v.genericTooltipCol',null);
        component.set('v.genericTooltipMSg',null);
    },

})