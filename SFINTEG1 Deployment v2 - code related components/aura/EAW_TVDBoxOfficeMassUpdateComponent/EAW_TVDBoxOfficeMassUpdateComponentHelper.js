({
	hideMassUpdate : function(component) {
		//hide spinner
        component.set("v.showSpinner",false);
        //hide modal 
        component.set('v.boxOfficeUSD', null);
        component.set('v.boxOfficeLocalCurrency', null);
        component.set('v.manualBoxOfficeUSD', null);
        component.set('v.boxOfficeOverrideType', '');
        component.set('v.displayComponent',false);
	},
	
	errorToast : function(message) {
        var toastEvent = $A.get('e.force:showToast');

        toastEvent.setParams({
            'title': 'Error!',
            'type': 'error',
            'message': message
        });

        toastEvent.fire();
    },

    successToast : function(message) {
        var toastEvent = $A.get('e.force:showToast');

        toastEvent.setParams({
            'title': 'Success!',
            'type': 'success',
            'message': message
        });

        toastEvent.fire();
    }
})