({
	doMassUpdate : function(component, event, helper) {
		
		var boxOfficeUsdVal = component.find('tvdBoxOffice') ? component.find('tvdBoxOffice').get('v.value') : '';
        var manualBoxOfficeUsdVal = component.find('tvdManaulBoxOffice') ? component.find('tvdManaulBoxOffice').get('v.value') : '';
        var boxOfficeOverrideTypeVal = component.find('tvdBoxOfficeLocal') ? component.find('tvdBoxOfficeOverrideType').get('v.value') : '';
        var boxOfficeLocalCurrencyVal = component.find('tvdBoxOfficeOverrideType') ? component.find('tvdBoxOfficeLocal').get('v.value') : '';
        
        if((boxOfficeUsdVal && boxOfficeUsdVal != '') || (manualBoxOfficeUsdVal && manualBoxOfficeUsdVal != '') ||
            (boxOfficeOverrideTypeVal && boxOfficeOverrideTypeVal != '') || (boxOfficeLocalCurrencyVal && boxOfficeLocalCurrencyVal != '') ) {
            
	        var selectedRows = component.get('v.selectedRows');
			var recIds = new Array();
		    for(let row of selectedRows){
		        recIds.push(row.Id);
		    }
		    
		    component.set("v.showSpinner",true);
		    
		    
		    var action = component.get("c.massUpdateTVDBoxOfficeRecords");
	        action.setParams({"recIds" : selectedRows, "boxOfficeUSD" : boxOfficeUsdVal, 
	        				"boxOfficeLocalCurrency":boxOfficeLocalCurrencyVal, "manualBoxOfficeUSD":manualBoxOfficeUsdVal, 
	        				"boxOfficeOverrideType":boxOfficeOverrideTypeVal
	        				});
	        				
			action.setCallback(this, function(response){
			
		        if(response.getState() === "SUCCESS"){
		            
		            var dataTableData = component.get("v.myData");
		            for(var i=0 ; i < dataTableData.length ; i++){
		                if(selectedRows.indexOf(dataTableData[i].Id) > -1){
		                    dataTableData[i].TVD_US_Box_Office__c = boxOfficeUsdVal==null?dataTableData[i].TVD_US_Box_Office__c:boxOfficeUsdVal;
		                    dataTableData[i].TVD_Local_Currency_Value__c = boxOfficeLocalCurrencyVal==null?dataTableData[i].TVD_Local_Currency_Value__c:boxOfficeLocalCurrencyVal; 
		                    dataTableData[i].Manual_Box_Office_USD__c = manualBoxOfficeUsdVal==null?dataTableData[i].EAW_Manual_Box_Office_USD__c:manualBoxOfficeUsdVal; 
		                    dataTableData[i].Box_Office_USD_Override_Type__c = boxOfficeOverrideTypeVal==null?dataTableData[i].EAW_Box_Office_USD_Override_Type__c:boxOfficeOverrideTypeVal;
	                    }
		            } 
		            component.set("v.myData",dataTableData);
		            component.set("v.myDataOriginal", JSON.parse(JSON.stringify(dataTableData)));
		            
		            var massUpdateTableEvent = $A.get("e.c:EAW_MassUpdateTableEvent");
		            massUpdateTableEvent.fire();
		            
		            helper.hideMassUpdate(component);
		            
		            helper.successToast('Successfully mass updated!');
		        }
		        else{
		            helper.errorToast('There was an error while performing the mass update.  Please try again or contact your system administrator if the problem persists.');
		            component.set("v.showSpinner",false);
		        }
	        });
	        
	        $A.enqueueAction(action);
	        
        } else{
        	helper.errorToast('Please update values for mass updating.');
        }
	},
	
	hideMassUpdate : function(component, event, helper) {
        helper.hideMassUpdate(component);
    }
})