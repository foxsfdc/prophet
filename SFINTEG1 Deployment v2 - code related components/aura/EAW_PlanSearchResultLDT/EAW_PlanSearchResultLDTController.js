({
    init: function (component, event, helper) {
   		component.set('v.myColumns', [
            {label: 'Name', fieldName: 'link', type: 'url', 
             		typeAttributes: { label: {fieldName: 'Name' }, target: '_blank'}, sortable: true,
             		initialWidth: 200},
            {label: 'Product Type', fieldName: 'Product_Type__c', type: 'text', initialWidth: 100},
            {label: 'Status', fieldName: 'Status__c', type: 'text'},
            {label: 'Version', fieldName: 'Version__c', type: 'number'},
            {label: 'Other Qualifiers', fieldName: 'otherQualifiers', type: 'text'},
            {label: 'Sales Region', fieldName: 'Sales_Region__c', type: 'text', initialWidth: 150},
            {label: 'Territory', fieldName: 'Territory__c', type: 'text', initialWidth: 150},
            {label: 'Language', fieldName: 'Language__c', type: 'text', initialWidth: 150},
            {label: 'Release Type', fieldName: 'Release_Type__c', type: 'text', initialWidth: 150},
            {label: 'Created By', fieldName: 'CreatedBy_Name__c', type: 'text'},
            {label: 'Created At', fieldName: 'Created_Date__c', type: 'date', initialWidth: 150},
            {label: 'Title Count', fieldName: 'titleCount', type: 'integer'}
		]);
    },
    
    createRecord : function (component, event, helper) {
        var createRecordEvent = $A.get("e.force:createRecord");
        
        createRecordEvent.setParams({
            "entityApiName": "EAW_Plan_Guideline__c"
        });
        
        createRecordEvent.fire();
    },
    
	clone : function(component, event, helper) {
        var cloneVal = component.get("v.cloneValue");
        
        if(cloneVal === 'cloneWindowGuidelines') {
            helper.clone('c.clonePlanAndWindows', component);
        } else if (cloneVal === 'useSameWindowGuidelines') {
            helper.clone('c.clonePlanSameWindows', component);
        } else if (cloneVal === 'cloneWithoutWindowGuidelines') {
            helper.clone('c.clonePlanNoWindows', component);
        }
        
        helper.hideCloneModal();
	},
    
    deactivate : function(component, event, helper) {
        var planData = component.get("v.planData");
        var selectedRows = component.get("v.selectedRows");
		var action = component.get("c.deactivatePlans");
        action.setParams({"plans" : selectedRows});
        
        action.setCallback(this, function(response) {
        	var state = response.getState();
            var results = response.getReturnValue();
            
            if(state === 'SUCCESS') {
	            for(var result of results) {
	            	var index = planData.findIndex(item => item.Id == result.Id);
	            	
	            	if(index != -1) {
	            		planData[index] = result;
	            	}
	            
	            	result.link = '/one/one.app?sObject/'+ result.Id + '/view#/sObject/' + result.Id + '/view';
	            }
	            
	        	component.set("v.planData", planData);
	        	helper.successToast('The plan(s) has/have been deactivated.');
            } else {
            	helper.errorToast('The plan(s) has/have not been deactivated.');
            }
        });
        $A.enqueueAction(action);
        
        helper.hideDeactivateModal();
	},
    
    deleteDraft : function(component, event, helper) {
        var planData = component.get("v.planData");
        var selectedRows = component.get("v.selectedRows");
		var action = component.get("c.deleteDraftPlans");
        action.setParams({"plans" : selectedRows});
        
        action.setCallback(this, function(response) {
        	var state = response.getState();
            var results = response.getReturnValue();
            
            if(state === 'SUCCESS') {
            	for(var result of results) {
                	var index = planData.findIndex(item => item.Id == result.Id);
                	
                	if(index != -1) {
                    	planData.splice(index, 1);
                	}
        		}
        		
            	component.set("v.planData", planData);
            	helper.successToast('The plan(s) with a status of draft has/have been deleted.');
            } else {
            	helper.errorToast('The plan(s) with a status of draft has/have not been deleted.');
            }
            
        });
        $A.enqueueAction(action);
        
        helper.hideDeleteModal();
    },
    
    hideDeactivateModal : function(component, event, helper) {
        helper.hideDeactivateModal();
    },
    
    hideCloneModal : function(component, event, helper) {
        helper.hideCloneModal();
    },
    
    hideDeleteModal : function(component, event, helper) {
        helper.hideDeleteModal();
    },
    
    showDeactivateModal : function(component, event, helper) {
    	var selectedRows = component.get("v.selectedRows");
    
        if(selectedRows.length > 0) {
        	helper.showDeactivateModal();
    	} else {
    		helper.errorToast('You have to select plan(s) to deactivate.');
    	}
    },
    
    showCloneModal : function(component, event, helper) {
        var selectedRows = component.get("v.selectedRows");
        
        if(selectedRows.length === 1) {
        	helper.showCloneModal();
    	} else {
    		helper.errorToast('You can only clone one plan at a time.');
    	}
    },
    
    showDeleteModal : function(component, event, helper) {
        var selectedRows = component.get("v.selectedRows");
    
        if(selectedRows.length > 0) {
        	helper.showDeleteModal();
    	} else {
    		helper.errorToast('You have to select plan(s) to delete.');
    	}
    }
    
})