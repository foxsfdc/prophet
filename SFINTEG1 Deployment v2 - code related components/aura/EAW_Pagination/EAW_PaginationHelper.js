({
	showErrorToast : function(component) {
		 var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title: "Info!",
                message: "Save Or cancel Unsaved changes before navigating across the page",
                type: "info"
            });
            toastEvent.fire();
	}
})