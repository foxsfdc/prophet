({
    doinit : function(component, event, helper) {
         console.log('releaseDateTags:::',component.get('v.selectedReleaseDateTags'));
         console.log('myData:::',component.get('v.myData'));
         console.log('selectedRows:::',component.get('v.selectedRows'));
        console.log('component.find(releaseDateStatus):::',component.find('releaseDateStatus'));
        //if(component.find('releaseDateStatus')){
            //component.find('releaseDateStatus').set('v.value','');
        //}
        
    },
    
    manualdateChange : function(component, event, helper) {
        
        if(!event.getSource().get("v.value")) {
            component.set('v.tempOrPerm', '');
        }
    },
    
    doMassUpdate : function(component, event, helper) {
        
        let selectedRows = component.get('v.selectedRows');
        let releaseDateStatus = component.find('releaseDateStatus') ? component.find('releaseDateStatus').get('v.value') : '';
        let releaseDateTempPerm = component.find('releaseDateTempPerm') ? component.find('releaseDateTempPerm').get('v.value') : '';
        let releaseDateManualDate = component.find('releaseDateManualDate') ? component.find('releaseDateManualDate').get('v.value') : '';
        let releaseDateTags = component.get('v.selectedReleaseDateTags');
        let notesTitle = component.get('v.title');
        let notesBody = component.get('v.body');
        console.log('releaseDateTempPerm:::::',releaseDateTempPerm);
        console.log('title:::::',notesTitle);
        console.log('body:::::',notesBody);
        
        //LRCC:1010
        let clearReleaseDateTagReleaseDateIds = [];
        let clearMaualDateReleaseDateIds = [];
         //<!-- LRCC-1010 APPEND,REPLACE & CLEAR radio button for notes  -->
        let applyNoteValue = component.get('v.applyNoteValue') ;
        let massUpdateNotes = {title:notesTitle,Body:notesBody,applyNoteValue:applyNoteValue};
        console.log('action:::::',component.find('radioAction').get('v.value'));
        var notesAttachmentEvent = component.getEvent("notesAttachmentEvent");
        notesAttachmentEvent.setParams({
            "title" : notesTitle,
            "body" : notesBody,
            "action" : component.find('radioAction').get('v.value')
        });
        notesAttachmentEvent.fire();
        
        /**LRCC:1010 - Add a check box to clear the Manual Date && Release Date Junctions**/
        let clearManualDate = component.find('clearManualDateCheckbox').getElement().checked;
        let clearTags = component.find('clearTagsCheckbox').getElement().checked;
        
        console.log(releaseDateStatus);
        console.log(releaseDateTempPerm);
        console.log(releaseDateManualDate);
        console.log(releaseDateTags);
        console.log(clearManualDate);
        console.log(clearTags);
        console.log('myData:::',component.get('v.myData'));
        console.log('selectedRows:::',component.get('v.selectedRows'));
       
        
        if((releaseDateManualDate && releaseDateManualDate != 'undefined' && releaseDateManualDate != '') 
        		&& (releaseDateTempPerm == 'undefined' ||releaseDateTempPerm == null || releaseDateTempPerm == '')) {
        	 
        	 helper.errorToast('Release Date Mass Update.Please select both Manual date and Temp/Perm');
        	 
        } else{
        	//LRCC-1642
            if((releaseDateStatus && releaseDateStatus != '') || (releaseDateTempPerm && releaseDateTempPerm != '') ||
               (releaseDateManualDate && releaseDateManualDate != 'undefined' && releaseDateManualDate != '') || (releaseDateTags && releaseDateTags.length > 0)
               ||(notesTitle && notesTitle != '')||(notesBody && notesBody != '') || (clearManualDate || clearTags)) {
               //LRCC-1732
                //if(notesTitle && notesTitle != '') {
                    
                    let formattedDate = '';
                    let tagIds = [];
                    
                    if(releaseDateManualDate && releaseDateManualDate != 'undefined' && releaseDateManualDate != '') {
                        let date = new Date(releaseDateManualDate);
                        formattedDate = (date.getUTCMonth() + 1) + '/' + date.getUTCDate() + '/' + date.getUTCFullYear();
                    }
                     
                    for(let tag of releaseDateTags) {
                        tagIds.push(tag.Id);
                    }
                    
                    //LRCC-1010
                    var originalTableData = component.get('v.myData');
                    for(var i = 0 ;i < originalTableData.length ; i++){
                    
                        if(component.get('v.selectedRows').indexOf(originalTableData[i].Id) > -1){
                            
                            if(releaseDateStatus)originalTableData[i].Status__c = releaseDateStatus;
                            if(!formattedDate) formattedDate = originalTableData[i].Manual_Date__c;
                            originalTableData[i].Manual_Date__c = (!clearManualDate) ? formattedDate : null;
                            originalTableData[i].Temp_Perm__c = (!clearManualDate) ? (releaseDateTempPerm ? releaseDateTempPerm  : originalTableData[i].Temp_Perm__c) : null;
                            
                            if(clearReleaseDateTagReleaseDateIds.indexOf(originalTableData[i].Id) == -1 && clearTags == true){
                            
                                clearReleaseDateTagReleaseDateIds.push(originalTableData[i].Id);
                            }
                            if(clearMaualDateReleaseDateIds.indexOf(originalTableData[i].Id) == -1 && clearManualDate == true){
                            
                                clearMaualDateReleaseDateIds.push(originalTableData[i].Id);
                            }
                            if(originalTableData[i] != null && originalTableData[i].releaseDateTags != null){
                            
                                var releaseTagNameList=[];
                                var temprealeseDateList = originalTableData[i].releaseDateTags;
                                if(!Array.isArray(temprealeseDateList)){
                                
                                    temprealeseDateList = [temprealeseDateList];
                                }
                                for(var temprealeseDate of temprealeseDateList){
                                
                                    if(releaseTagNameList.indexOf(temprealeseDate.title) == -1){
                                        releaseTagNameList.push(temprealeseDate.title);
                                    }
                                }      
                            }
                            originalTableData[i].releaseDateTags = [];	
                            for(var j = 0;j < releaseDateTags.length ; j++){
                                    
                                if(Array.isArray(originalTableData[i].releaseDateTags)){
                                    
                                     originalTableData[i].releaseDateTags.push(releaseDateTags[j]);
                                }
                            }
                            console.log('clearTags:test::::',clearTags);
                            if(clearTags){
                            
                                 originalTableData[i].releaseDateTags = [];
                                   console.log('originalTableData[i]:::',originalTableData[i].releaseDateTags);
                            }
                        }
                    }
                    component.set('v.myData',[]);
                    console.log('originalTableData:::',originalTableData);
                    component.set('v.myData',JSON.parse(JSON.stringify(originalTableData)));
                    console.log('myData::after:',component.get('v.myData'));
                    var indicationFlag = component.get('v.ChangesIndicationFlag');
                    indicationFlag = !indicationFlag;
                    
                    var clearReleaseDateMap = {};
                    if(!clearReleaseDateMap['clearReleaseDateTagReleaseDateIds']){
                    
                        clearReleaseDateMap['clearReleaseDateTagReleaseDateIds'] = [];
                    }
                    clearReleaseDateMap['clearReleaseDateTagReleaseDateIds'] = clearReleaseDateTagReleaseDateIds;
                    if(!clearReleaseDateMap['clearMaualDateReleaseDateIds']){
                    
                        clearReleaseDateMap['clearMaualDateReleaseDateIds'] = [];
                    }
                    clearReleaseDateMap['clearMaualDateReleaseDateIds'] = clearMaualDateReleaseDateIds;
                    component.set('v.clearReleaseDateMap',clearReleaseDateMap);
                    
                    console.log('maasss clear:::::::: ',JSON.stringify( component.get('v.clearReleaseDateMap')));
                    component.set('v.MassUpdatechagesSourceData',originalTableData);
                    //LRCC-1010 APPEND,REPLACE & CLEAR radio button for notes 
                    component.set('v.massUpdateNotes',massUpdateNotes);
                    //LRCC:1010
                    helper.hideMassUpdate(component);                 
                    var editTableEvent = $A.get('e.c:EAW_EditTableEvent');
                    editTableEvent.setParams({
                        'editMode': true
                    });
                    editTableEvent.fire();
                
                    //component.get('v.callToggle')();
                    
                  /*  let action = component.get('c.massUpdate');
                    action.setParams({
                        'selectedIds': selectedRows,
                        'status': releaseDateStatus,
                        'tempPerm': releaseDateTempPerm,
                        'manualDate': formattedDate,
                        'tags': tagIds,
                        'clearManualDate': clearManualDate,
                        'clearTags': clearTags,
                        'notesTitle': notesTitle,
                        'notesBody': notesBody
                    });
    
                    action.setCallback(this, function(response) {
                        if(response.getState() === 'SUCCESS') {
                            let tableData = component.get('v.myData');
                            let updatedReleaseDates = response.getReturnValue();
                            let combinedTagNames = '';
                            console.log('releaseDateTags::',releaseDateTags);
                            for(let releaseDateTag of releaseDateTags) {
                                combinedTagNames += releaseDateTag.title;
                            }
                            console.log('combinedTagNames::',combinedTagNames);
                            for(let releaseDate of updatedReleaseDates.releaseDates) {
                                let rowData = tableData.find(row => row.Id === releaseDate.Id);
    
                                rowData.titleTags = combinedTagNames;
                                rowData.Status__c = releaseDate.Status__c;
                                rowData.Manual_Date__c = releaseDate.Manual_Date__c;
                                rowData.Temp_Perm__c = releaseDate.Temp_Perm__c;
                                
                                if(updatedReleaseDates.releaseDateTagJunctions && updatedReleaseDates.releaseDateTagJunctions.length > 0) {
                                    rowData.releaseDateTags = updatedReleaseDates.releaseDateTagJunctions.filter(tag => tag.Release_Date__c == releaseDate.Id)
                                                                                                    .map(x => x.Tag__r.Name)
                                                                                                    .join(', ');
                                }
                            }
    
                            component.set('v.myData', tableData);
                            helper.hideMassUpdate(component);
                            helper.successToast('Successfully updated Release Dates!');
                        } 
                        else if (response.getState() === 'ERROR') {
                            let errors = response.getError();
                            let handleErrors = helper.handleErrors(errors, component, event, helper);
                            if(handleErrors) {
                                helper.errorToast(handleErrors);
                            }
                        } 
                        else {
                            helper.errorToast('There was an error while performing the mass update.  Please try again or contact your system administrator if the problem persists.');
                        }
                    });
                    $A.enqueueAction(action);*/
                //} else {
                   // let label = $A.get("$Label.c.EAW_NotesSubjectValidation");
                    //helper.errorToast(label);
                //}
            //LRCC-1642   
            } else {
                helper.errorToast('Please select at least one field before save');
            }
            
        }
    },

    hideMassUpdate : function(component, event, helper) {
        helper.hideMassUpdate(component);
    },
    //LRcc-1327
    clearReleaseDateTags : function(component, event, helper) {
        
       console.log('component.find(releaseDateStatus):::',component.find('releaseDateStatus').get('v.value'));
        console.log('clear:::',component.find('clearTagsCheckbox').getElement().checked);
        
        if(component.find('clearTagsCheckbox').getElement().checked){
        
        	component.set('v.disalbleReleaseDateTags',true);
        	component.set('v.selectedReleaseDateTags',[]);
        }else{
        	component.set('v.disalbleReleaseDateTags',false);
        }
         console.log('disableWindowFlag:::',component.get('v.disalbleReleaseDateTags'));
    },
    clearManualDateAndTemp : function(component, event, helper) {
    
        console.log('clear:disalbleManualDateAndTemp::',component.find('clearManualDateCheckbox').getElement().checked);
        
        if(component.find('clearManualDateCheckbox').getElement().checked){
        
        	
        	component.set('v.tempOrPerm','');
        	component.set('v.releaseDate',null);        	
            console.log('manuladatex:::',component.get('v.releaseDate'));
        	component.set('v.disalbleManualDateAndTemp',true);
        }else{
        	component.set('v.disalbleManualDateAndTemp',false);
        }
         console.log('disalbleManualDateAndTemp:::',component.get('v.disalbleManualDateAndTemp'));
    },
    checkForManualDateOverridden : function(cmp,event,helper){
        
        let releaseDateStatus = component.find('releaseDateStatus') ? component.find('releaseDateStatus').get('v.value') : '';
        let releaseDateTempPerm = component.find('releaseDateTempPerm') ? component.find('releaseDateTempPerm').get('v.value') : '';
        let releaseDateManualDate = component.find('releaseDateManualDate') ? component.find('releaseDateManualDate').get('v.value') : '';
        
        if(releaseDateStatus == 'Firm' && releaseDateManualDate){
            
        } else{
            helper.checkForManualDateUpdate(cmp,event,helper);
        }
    }
})