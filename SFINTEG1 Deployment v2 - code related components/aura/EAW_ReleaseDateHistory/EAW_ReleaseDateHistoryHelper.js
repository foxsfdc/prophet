({
	setHistoryList : function(cmp,event,helper,historyList) {
        var validHistoryList = []; 
        var created = {};
        for(var history of historyList){
            //LRCC-1787
            history.CreatedDate = history.CreatedDate ? helper.setDate(cmp,event,helper,history.CreatedDate) : '';
            history.OldValue = (history.OldValue == 'null' ? '' : (history.Field == 'Notes' ? history.OldValue.split(';') : history.OldValue));
            history.NewValue = (history.NewValue == 'null' ? '' : (history.Field == 'Notes' ? history.NewValue.split(';') : history.NewValue));
            history.type = 'String';
            if(history.Field == 'Temp_Perm__c'){
                history.Field = 'Temp/Perm';
            } else if(history.Field == 'Status__c'){
                history.Field = 'Release Date Status';
            }else if(history.Field == 'Allow_Manual__c'){
                history.Field = 'Allow Manual';
            } else if(history.Field == 'Active__c'){
                history.Field = 'Active';
                history.OldValue = new String(history.OldValue).charAt(0).toUpperCase() + new String(history.OldValue).slice(1);
                history.NewValue = new String(history.NewValue).charAt(0).toUpperCase() + new String(history.NewValue).slice(1);
            } else if(history.Field == 'Is_Confidential__c'){
                history.Field = 'Is Confidential';
            } else if(history.Field == 'EAW_Release_Date_Type__c'){
                history.Field = 'Release Date Type';
            } else if(history.Field == 'Title__c'){
                history.Field = 'Title';
            }
            if(history.Field.includes('Date__c')){ //LRCC-1787 'Date' to 'Date__c'
                history.type = 'Date';
                if(history.Field == 'Projected_Date__c'){
                    history.Field = 'Projected Date';
                } else if(history.Field == 'Feed_Date__c'){
                    history.Field = 'Feed Date';
                } else if(history.Field == 'Manual_Date__c'){
                    history.Field = 'Manual Date';
                } else if(history.Field == 'Release_Date__c'){
                    history.Field = 'Release Date';
                }
            }
			if(history.Field == 'created'){
                history.Field = 'Created';
                created = history;
            } else{
                validHistoryList.push(history);
            }          
        }
        if(created.Field){
            validHistoryList.splice(validHistoryList.length,0,created);
        }
        console.log('::::::::'+JSON.stringify(validHistoryList));
        cmp.set('v.historyList',validHistoryList);
    },
    setDate : function(cmp,event,helper,dateValue) {
        console.log('::::::::::::::'+dateValue);
        var dateAndTime = dateValue.split('T');
        var dateList = dateAndTime[0].split('-');
        var time = helper.setTime(dateAndTime[1].split(':'));
        var date = dateList[2] + '-' +  helper.getMonth(dateList[1]) + '-' + dateList[0];
        return date +' '+ time;
	},
    getMonth : function(monthValue){
        var month = {
            '01' : 'Jan',
            '02' : 'Feb',
            '03' : 'Mar',
            '04' : 'Apr',
            '05' : 'May',
            '06' : 'Jun',
            '07' : 'Jul',
            '08' : 'Aug',
            '09' : 'Sep',
            '10' : 'Oct',
            '11' : 'Nov',
            '12' : 'Dec'
        };
		return month[monthValue];
    },
    setTime : function(date) {
        //var d = new Date(date);
        var hh = date[0];
        var m = date[1];
       // var s = d.getSeconds();
        var dd = "AM";
        var h = hh;
        if(hh >= 7){
            h = hh - 7;
        }
        if (h >= 12) {
            h = hh - 12;
            dd = "PM";
        }
        if (h == 0) {
            h = 12;
        } else if(h < 10 && new String(h).length < 2){
            h = '0' + h;
        }
        if(m < 10  && new String(m).length < 2){
            m = '0' + m;
        }
        var replacement = h + ":" + m;
        replacement += " " + dd;
        return replacement;
    }
    
})