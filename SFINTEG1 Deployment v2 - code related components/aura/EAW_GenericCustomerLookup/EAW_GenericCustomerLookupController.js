({
    doInit : function(component, event, helper) {
        
        component.set('v.showSpinner',true);
        var action = component.get("c.getCustomer");
        action.setParams({objectName:component.get("v.sobjecttype"),objectId:component.get("v.recordId")});
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            var results = response.getReturnValue();
            if(state === 'SUCCESS') {
                if(results && results.length > 0){
                    for(let value of results) {
                        value.title = value.titleId + ': ' + value.title;
                    }
                    component.set('v.selectedCustomers', results);
                    component.set('v.showSpinner',false);
                } else {
                    component.set('v.showSpinner',false);
                }
            }
        });
        $A.enqueueAction(action);
    },
    save : function(component, event, helper) {
        
        var selectedCustomersList = component.get('v.selectedCustomers');
        var customerIdList = [];
        var customerNames;
        component.set('v.showSpinner',true);
        
        for(var i = 0; i < selectedCustomersList.length ; i++){
            
            var customer = selectedCustomersList[i];
            var customerName = 	customer.title.split(':')
            if(customerNames == null){
                customerNames = customerName[1].trim();
            } else {
			    customerNames = customerNames+';'+customerName[1].trim();
            }
        }
            var action = component.get("c.updateCustomerField");
            action.setParams({customerIds:customerNames,objectId:component.get("v.recordId")});
            
            action.setCallback(this, function(response) {
                
                var state = response.getState();
                var results = response.getReturnValue();
                
                if(state === 'SUCCESS' && results) {
                
                    component.set('v.showSpinner',false);
                    window.setTimeout($A.getCallback(function() {
                        $A.get("e.force:closeQuickAction").fire();
                        location.reload();
                    }),1);
                } 
                else if (state == 'ERROR') {
                    component.set('v.showSpinner',false);
                    window.setTimeout($A.getCallback(function() {
                        $A.get("e.force:closeQuickAction").fire();
                    }),1);
                    let errors = response.getError();
                	let handleErrors = helper.handleErrors(errors, component, event, helper);
                    if(handleErrors) {
                        helper.errorToast(handleErrors);
                    }
                }
            });
            $A.enqueueAction(action);
    }
})