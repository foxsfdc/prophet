({
    onEdit : function(component, event, helper) {
        let rule = component.get('v.rules');

        component.set('v.monthsRule', rule.months);
        component.set('v.daysRule', rule.days);
        
        if(rule.windowName != '' && rule.dateType != '') {
            component.set('v.basedOnDateRuleValue', rule.media + ' - ' + rule.windowName + ' / ' + rule.dateType);
        } else {
            component.set('v.basedOnDateRuleValue', rule.media + ' / ' + rule.territory);
        }
    },

    updateRulesList : function(component, event, helper) {
        let sObjectType = component.get('v.sObjectType');
        let auraId = event.getSource().getLocalId();
        let selectedComponent = component.find(auraId);
        let rules = component.get('v.rules');

        if(selectedComponent.get('v.name') == 'months') {
            component.set('v.monthsRule', selectedComponent.get('v.value'));
            rules.months = selectedComponent.get('v.value');
        } else if(selectedComponent.get('v.name') == 'days') {
            component.set('v.daysRule', selectedComponent.get('v.value'));
            rules.days = selectedComponent.get('v.value');
        } else if(selectedComponent.get('v.name') == 'basedOnDate') {
            let basedOnDate = selectedComponent.get('v.value');

            component.set('v.basedOnDateRuleValue', basedOnDate);

            if(basedOnDate.indexOf('/') > 0 && basedOnDate.indexOf('-') > 0) {
                let index = basedOnDate.lastIndexOf('/');
                let dateType = basedOnDate.slice(index + 2, basedOnDate.length);
                let mediaWindow = basedOnDate.slice(0, index - 1);
                let secondIndex = mediaWindow.indexOf('-');
                let windowName = mediaWindow.slice(secondIndex + 2, mediaWindow.length);
                let media = mediaWindow.slice(0, secondIndex - 1);

                rules.media = media;
                rules.windowName = windowName;
                rules.dateType = dateType;
                rules.territory = '';
                rules.basedOnDate = media + ' - ' + windowName + ' / ' + dateType;
            } else {
                let index = basedOnDate.indexOf('/');
                let media = basedOnDate.slice(0, index - 1);
                let territory = basedOnDate.slice(index + 2, basedOnDate.length);

                rules.media = media;
                rules.windowName = '';
                rules.dateType = '';
                rules.territory = territory;
                rules.basedOnDate = media + ' / ' + territory;
            }
        }

        component.set('v.rules', rules);
    }
})