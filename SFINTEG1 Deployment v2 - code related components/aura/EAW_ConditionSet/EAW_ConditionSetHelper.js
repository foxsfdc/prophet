({
	isActiveConditionalId : function(cmp,idList){
		var idValidList = [];
		
		cmp.get('v.releaseWindowDateWrapper').find(function(element){
            if(idList.includes(element.value)){
                idValidList.push(element);
            }
        });
        return idValidList; 
	}
})