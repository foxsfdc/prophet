({	
    getRulesAndDetails : function(cmp,event,helper){
        cmp.set('v.spinner',true);
    },
    setConditionBasedNode : function(cmp,event,helper,trueBased,falseBased) {
        
        var trueBasedRuleDetailObject = JSON.parse(JSON.stringify(cmp.get('v.ruleDetailObject')));
        trueBasedRuleDetailObject.Rule_Order__c = 1;
        trueBasedRuleDetailObject.Nest_Order__c = 1;
        
        var trueNewRuleDetailJSON = {
            'ruleDetail' : trueBased || JSON.parse(JSON.stringify(trueBasedRuleDetailObject)),
            'dateCalcs' : [],
            'nodeCalcs' : []                
        };   
        var falseBasedRuleDetailObject = JSON.parse(JSON.stringify(cmp.get('v.ruleDetailObject')));
        falseBasedRuleDetailObject.Rule_Order__c = 2;
        falseBasedRuleDetailObject.Nest_Order__c = 1;
        
        var falseNewRuleDetailJSON  = {
            'ruleDetail' : falseBased || JSON.parse(JSON.stringify(falseBasedRuleDetailObject)),
            'dateCalcs' : [],
            'nodeCalcs' : []                
        }; 
     	
        var conditionBased = {
            'TrueBased' : {'ruleOrder' : 1,
                           'ruleDetailJSON' :  JSON.parse(JSON.stringify(trueNewRuleDetailJSON))},
            'FalseBased' : {'ruleOrder' : 2,
                            'ruleDetailJSON' : JSON.parse(JSON.stringify(falseNewRuleDetailJSON))}
        };
        cmp.set('v.conditionBasedMap',conditionBased);
    },
    setCaseBasedNode : function(cmp,event,helper,whenBasedList,otherBased) {
        
        var newRuleDetail = JSON.parse(JSON.stringify(cmp.get('v.ruleDetailObject')));
        newRuleDetail.Rule_Order__c = 1;
        newRuleDetail.Nest_Order__c = 1;
        newRuleDetail.Condition_Type__c = 'Case';
       
        var newRuleDetailJSON = {
            'ruleDetail' : newRuleDetail,
            'dateCalcs' : [],
            'nodeCalcs' :  []               
        };   
        newRuleDetail = JSON.parse(JSON.stringify(newRuleDetail));
        newRuleDetail.Nest_Order__c = 2;
        var nodeRuleDetail = {
            'ruleDetail' : newRuleDetail,
            'dateCalcs' : [],
            'nodeCalcs' :  [] 
        }
        newRuleDetailJSON.nodeCalcs = [nodeRuleDetail];
        var caseRuleDetailMap = {
            'whenBased' : whenBasedList ||  [JSON.parse(JSON.stringify(newRuleDetailJSON))],
            'otherBased' : otherBased || JSON.parse(JSON.stringify(newRuleDetailJSON))
        }
        
        cmp.set('v.caseRuleDetailMap',caseRuleDetailMap);
     //   console.log('caseRuleDetailMap::',caseRuleDetailMap);
    },
    setEarliestLatestBasedNode : function(cmp,event,helper,ruleDetail) {
        var newRuleDetailJSON = {
            'ruleDetail' : ruleDetail || JSON.parse(JSON.stringify(cmp.get('v.ruleDetailObject'))),
            'dateCalcs' : [],
            'nodeCalcs' : []                
        };   
        cmp.set('v.newRuleDetailJSON',newRuleDetailJSON);
    },
    setDateBasedNode : function(cmp,event,helper,dateNode) {
        var newRuleDetail = dateNode || JSON.parse(JSON.stringify(cmp.get('v.ruleDetailObject')));
        newRuleDetail.Rule_Order__c = 1;
        newRuleDetail.Nest_Order__c = 1;
        cmp.set('v.dateCalcBasedMap',newRuleDetail);
    },
    setRuleDetailByRules : function(cmp,event,helper,parentRuleDetailList){
        //LRCC-1454
        if(parentRuleDetailList.length == 1 && parentRuleDetailList[0].Condition_Type__c != 'If' && parentRuleDetailList[0].Condition_Type__c != 'Case then'){
            var isEarliestLatest = cmp.get('v.ruleDetailList').find(function(element){
                return element.Parent_Rule_Detail__c == parentRuleDetailList[0].Id; 
            });
            if(isEarliestLatest){
                helper.setEarliestLatestBasedNode(cmp,event,helper,parentRuleDetailList[0]);   
                cmp.set('v.selectedContext','earliestLatest');
            } else{
                helper.setDateBasedNode(cmp,event,helper,parentRuleDetailList[0]);
                cmp.set('v.selectedContext','date');
            }
            //LRCC-1454 for change the length size
        } else if(parentRuleDetailList.length > 0 && (parentRuleDetailList[0].Condition_Type__c == 'If' || parentRuleDetailList[0].Condition_Type__c == 'Else/If')){
            var trueBased =  parentRuleDetailList.find(function(element) {
                return element.Condition_Type__c == 'If';
            });
            var falseBased = parentRuleDetailList.find(function(element) {
                return element.Condition_Type__c == 'Else/If';
            });
          //  console.log(trueBased,falseBased)
            helper.setConditionBasedNode(cmp,event,helper,trueBased,falseBased);
            cmp.set('v.selectedContext','condition');
        } else{
            var whenBasedList = parentRuleDetailList;
            var otherBased = whenBasedList.find(function(element) {
                return element.Condition_Type__c == 'Otherwise';
            });
            //console.log(otherBased);
            if(otherBased){
            	whenBasedList.splice(whenBasedList.indexOf(otherBased),1);
            }
            var caseNodeList = helper.setNodeRuleDetailForCase(cmp,event,helper,whenBasedList,cmp.get('v.ruleDetailList'));
            if(otherBased){
                otherBased = helper.setNodeRuleDetailForCase(cmp,event,helper,[otherBased],cmp.get('v.ruleDetailList'));
            } else{
                otherBased = [otherBased];
            }
            // console.log('otherBased:::::',otherBased)
            helper.setCaseBasedNode(cmp,event,helper,caseNodeList,otherBased[0]);
            cmp.set('v.selectedContext','multidate');
        }
    },
    showToast : function(cmp, event, helper, type, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "type": type,
            "message": message
        });
        toastEvent.fire();
    },
    buildErrorMsg : function(cmp, event, helper, errors){
        var errorMsg = "";
        
        if(errors[0] && errors[0].message){  // To show other type of exceptions
            errorMsg = errors[0].message;
        }else if(errors[0] && errors[0].pageErrors.length) {  // To show DML exceptions
            errorMsg = errors[0].pageErrors[0].message; 
        }else if(errors[0] && errors[0].fieldErrors){  // To show field exceptions
            var fields = Object.keys(errors[0].fieldErrors);
            var field = fields[0];
            errorMsg = errors[0].fieldErrors[field];
            errorMsg = errorMsg[0].message;
        }else if(errors[0] && errors[0].duplicateResults.length){ // To show duplicateResults exceptions
            errorMsg = errors[0].duplicateResults[0].message;
        }
        
        return errorMsg;
        
    },
    deleteRuleDetailById : function(cmp,event,helper,ruleDetailId,isDateCalc,isRule){
        
        var action = cmp.get('c.deleteRuleAndDetails');
        var windowRecordId = cmp.get('v.isWindow') ? cmp.get('v.windowRecordId') : null;
        var windowGuidelineId = cmp.get('v.isWindow') ? null : cmp.get('v.recordId');
       // console.log('action');
        action.setParams({
            'isRule' : isRule,
            'isDateCalc' : isDateCalc,
            'ruleOrRuleDetailId' : ruleDetailId,
            'currentWindowGuidelineId' : windowGuidelineId,
            'windowId' : windowRecordId
            
        });
        cmp.set('v.spinner',true);
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state == 'SUCCESS'){
               // console.log(response.getReturnValue());                
            } else if(state == 'ERROR'){
                var errorMessage = helper.buildErrorMsg(cmp,event,helper,response.getError());
                helper.showToast(cmp,event,helper,state,errorMessage);              
            }
            cmp.set('v.spinner',false);
        });
        $A.enqueueAction(action);            
    },
    openModal : function(cmp,event,helper,message){
        cmp.find('promt').set('v.content',message);
        cmp.find('promt').openModal();
    },
    checkForNullNodes : function(cmp,event,helper,ruleDetailJSON){
        var notValid = false;
       // console.log('JSON:::test::',JSON.stringify(ruleDetailJSON));
        if(ruleDetailJSON.dateCalcs.length || ruleDetailJSON.nodeCalcs.length){
            if(ruleDetailJSON.ruleDetail && !ruleDetailJSON.ruleDetail.Condition_Timeframe__c){
                notValid = true;
                return notValid;
            }
            return helper.validNodes(cmp,event,helper,notValid,ruleDetailJSON.dateCalcs,ruleDetailJSON.nodeCalcs);
        } else{
            //LRCC-1454
            if(ruleDetailJSON.ruleDetail && (ruleDetailJSON.ruleDetail.hasOwnProperty('Conditional_Operand_Id__c') ||ruleDetailJSON.ruleDetail.hasOwnProperty('Condition_Date_Text__c') || ruleDetailJSON.ruleDetail.hasOwnProperty('Condition_Timeframe__c'))){
                
            	return helper.validNodes(cmp,event,helper,notValid,[ruleDetailJSON.ruleDetail],[]);    
            }else {
               
                return false;
            }
        }
    },
    validNodes : function(cmp,event,helper,notValid,dateCalcs,nodeCalcs){

        if(dateCalcs.length){
            for(var i=0;i<dateCalcs.length;i++){
              //  console.log(dateCalcs[i])
                //dateCalcs[i].Condition_Met_Media__c = '';
                var isWindowDate = false;
                var selectedValue = dateCalcs[i].Conditional_Operand_Id__c;
                isWindowDate = cmp.get('v.windowGuidelineList').find(function(element){
                    return element.value == selectedValue
                });
                helper.setWindowIdForRuleDetail(cmp,dateCalcs[i]);
                //LRCC-1454
                if($A.util.isEmpty(dateCalcs[i].Condition_Date_Text__c) && ($A.util.isEmpty(dateCalcs[i].Condition_Met_Months__c) ||  $A.util.isEmpty(dateCalcs[i].Condition_Met_Days__c) || ($A.util.isEmpty(dateCalcs[i].Conditional_Operand_Id__c) && (/*!$A.util.isEmpty(dateCalcs[i].Condition_Operand_Details__c) && */ dateCalcs[i].Condition_Operand_Details__c != 'Rights End Date'))  || (!$A.util.isEmpty(dateCalcs[i].Condition_Date_Duration__c) && ($A.util.isEmpty(dateCalcs[i].Condition_Criteria_Amount__c) || (dateCalcs[i].Condition_Date_Duration__c == 'Day' && $A.util.isEmpty(dateCalcs[i].Day_of_the_Week__c))) || (isWindowDate && $A.util.isEmpty(dateCalcs[i].Window_Guideline_Date__c))))){  // LRCC - 1477
                  	console.log('error on date',dateCalcs[i]);
                    notValid = true;
                    break;
                }
            } 
        } else if(!nodeCalcs.length){
            return true;
        }
        if(!notValid &&  nodeCalcs.length){
            for(var i=0;i<nodeCalcs.length;i++){
                //console.log('nodeCalcs::::',nodeCalcs[i])
                if((!nodeCalcs[i].ruleDetail.Condition_Timeframe__c)){
                    if(nodeCalcs[i].ruleDetail.Parent_Condition_Type__c){
                        console.log('::Parent_Condition_Type__c:'+nodeCalcs[i].ruleDetail.Parent_Condition_Type__c);
                        if(helper.checkForNestedNodes(cmp,event,helper,nodeCalcs[i])){
                            notValid = true;
                            return notValid;
                        }
                    } else if(nodeCalcs[i].ruleDetail.Condition_Type__c){
                        console.log(':::here::'+JSON.stringify(nodeCalcs[i].ruleDetail));
                        if(helper.validNodes(cmp,event,helper,notValid,[nodeCalcs[i].ruleDetail],[])){
                            notValid = true;
                            return notValid;
                        }
                    } else{
                        notValid = true;
                        return notValid;
                    }
                } else{
                    helper.setWindowIdForRuleDetail(cmp,nodeCalcs[i].ruleDetail);
                    if(helper.validNodes(cmp,event,helper,notValid,nodeCalcs[i].dateCalcs,nodeCalcs[i].nodeCalcs)){
                        return true;
                    }
                }
            }
        } else{
            return notValid;
        }       
        
    },
    checkForRule : function(cmp,event,helper,rule){
        //console.log(rule);
        if(!rule.Operator__c  || !rule.Condition_Field__c || (!rule.Condition_Operand_Details__c && rule.Condition_Field__c != 'Title Tag') || !rule.Criteria__c){
            return true;
        } else{
            return false;
        }
    },
    setNodeRuleDetailForCase : function(cmp,event,helper,basedNode,ruleDetailList){
        var basedList = [];
        for(var i=0;i<basedNode.length;i++){
            var trueNewRuleDetailJSON = {
                'ruleDetail' : basedNode[i],
                'dateCalcs' : [],
                'nodeCalcs' : []                
            };  
            basedList.push(trueNewRuleDetailJSON);
        }
       // console.log(basedList);
        return basedList;
    },
    validForCase : function(cmp,event,helper,caseNode,isValid){
        if(caseNode.ruleDetail){
            //console.log(caseNode.nodeCalcs)
             helper.setWindowIdForRuleDetail(cmp,caseNode.ruleDetail);
            if(caseNode.nodeCalcs[0] && caseNode.nodeCalcs[0].nodeCalcs.length){
                isValid = helper.validNodes(cmp,event,helper,false,caseNode.dateCalcs,caseNode.nodeCalcs) || helper.validParentRuleDetail(cmp,event,helper,caseNode.ruleDetail);
            } else{
                if(caseNode.nodeCalcs[0] && caseNode.nodeCalcs[0].dateCalcs && caseNode.nodeCalcs[0].dateCalcs.length){
                    isValid = helper.validParentRuleDetail(cmp,event,helper,caseNode.ruleDetail) || helper.validNodes(cmp,event,helper,false,[],caseNode.nodeCalcs);
                } else if(caseNode.nodeCalcs[0]){
                    isValid = helper.validDateCalcs(cmp,event,helper,caseNode.nodeCalcs[0].ruleDetail) || helper.validParentRuleDetail(cmp,event,helper,caseNode.ruleDetail);
                }
            }
            return isValid;
        } else{
            return true;
        }
    },
    validParentRuleDetail : function(cmp,event,helper,ruleDetail){
        if(ruleDetail.Condition_Type__c == 'Case then'){
            //console.log('ruleDetail:::',ruleDetail)
            if($A.util.isEmpty(ruleDetail.Condition_Set_Operator__c) || $A.util.isEmpty(ruleDetail.Condition_Field__c) ||  ($A.util.isEmpty(ruleDetail.Condition_Operand_Details__c) && ruleDetail.Condition_Field__c != 'Title Tag') || $A.util.isEmpty(ruleDetail.Condition_Criteria__c)){
                return true;
            } else{
                helper.setWindowIdForRuleDetail(cmp,ruleDetail);
                return false;
            }
        } else{
            helper.setWindowIdForRuleDetail(cmp,ruleDetail);
            return false;
        }
        
    },
    validDateCalcs : function(cmp,event,helper,dateCalcs){
        //console.log(':::here::',dateCalcs);
        if(dateCalcs  && dateCalcs.Condition_Type__c == ''){
            delete dateCalcs.Condition_Type__c;
        }
        var isWindowDate = false;
        isWindowDate = cmp.get('v.windowGuidelineList').find(function(element){
            var selectedValue = dateCalcs.Conditional_Operand_Id__c;
            return element.value == selectedValue
        });
        if($A.util.isEmpty(dateCalcs.Condition_Date_Text__c) && ($A.util.isEmpty(dateCalcs.Condition_Met_Months__c) ||  $A.util.isEmpty(dateCalcs.Condition_Met_Days__c) || ($A.util.isEmpty(dateCalcs.Conditional_Operand_Id__c) && (/*!$A.util.isEmpty(dateCalcs.Condition_Operand_Details__c) &&*/ dateCalcs.Condition_Operand_Details__c != 'Rights End Date'))  || (isWindowDate && $A.util.isEmpty(dateCalcs.Window_Guideline_Date__c)) || (!$A.util.isEmpty(dateCalcs.Condition_Date_Duration__c) && ($A.util.isEmpty(dateCalcs.Condition_Criteria_Amount__c) || (dateCalcs.Condition_Date_Duration__c == 'Day' && $A.util.isEmpty(dateCalcs.Day_of_the_Week__c)))))){ // LRCC - 1477
        	return true;
        } else{
            helper.setWindowIdForRuleDetail(cmp,dateCalcs);
            return false;
        }
           
    },
    checkForOverridden : function(overriddenRule,checkingList){
        var keyList = Object.keys(overriddenRule);
        var overRidden = false; 
        //console.log(checkingList);
       // console.log('overriddenRule::',overriddenRule);
        for(var element of checkingList){
            //console.log(element.Id == overriddenRule.Id)
            if(element.Id == overriddenRule.Id){
                overRidden = false;
                for(var i of keyList){ 
                    if(element[i] != overriddenRule[i]){
                        //console.log('here::',element[i], overriddenRule[i]);
                        overRidden = true;
                        return overRidden;
                    }
                }
                if(!overRidden){
                    break;
                }
            } else{
                overRidden = true;
            }
        }
        return overRidden;
       // console.log('overridden:::::::::::::::::::::',overRidden);
    },
    earliestLatestOverridden : function(cmp,event,helper,JSON,isOverridden){
        var checkingList = cmp.get('v.originalParentRuleDetailList').concat(cmp.get('v.originalRuleDetailList'));
        if(JSON.ruleDetail){
            //console.log(helper.checkForOverridden(JSON.ruleDetail,checkingList));
            if(!helper.checkForOverridden(JSON.ruleDetail,checkingList)){
                helper.setWindowIdForRuleDetail(cmp,JSON.ruleDetail);
                if(JSON.dateCalcs.length){
                    for(var i of JSON.dateCalcs){
                        if(helper.checkForOverridden(i,checkingList)){
                            isOverridden = true;
                        }
                    }
                } if(JSON.nodeCalcs.length){
                    for(var i of JSON.nodeCalcs){
                        isOverridden = helper.earliestLatestOverridden(cmp,event,helper,i,isOverridden);
                    }
                }
            } else{
                isOverridden = true;
            }
        }
        return isOverridden;
    },
    conditionOverRidden : function(cmp,event,helper,JSON){
        var ruleDetailList = [JSON.ruleDetail];
        if(JSON.nodeCalcs.length){
            for(var i of JSON.nodecalcs){
                ruleDetailList.push(i);
            }
        }
       // console.log('ruleDetailList:::',ruleDetailList);
    },
    setWindowIdForRuleDetail : function(cmp,ruleDetail){
        if(cmp.get('v.isWindow')){
            ruleDetail.WindowName__c = cmp.get('v.windowRecordId');
          //  ruleDetail.Window_Guideline__c = null;
        }
    },
    checkForNestedNodes : function(cmp,event,helper,node){
        var notValid = false;
        if(node.ruleDetail && node.ruleDetail.Parent_Condition_Type__c){
            if(node.ruleDetail.Parent_Condition_Type__c == 'Condition'){
                if($A.util.isEmpty(node.ruleDetail.Condition_Field__c) || ($A.util.isEmpty(node.ruleDetail.Conditional_Operand_Id__c) && node.ruleDetail.Condition_Field__c != 'Title Tag') || $A.util.isEmpty(node.ruleDetail.Condition_Criteria__c) || $A.util.isEmpty(node.ruleDetail.Condition_Set_Operator__c)){
                    console.log('errorOnparentRuleDetail:::'+node.ruleDetail);
                    notValid = true;
                } else{
                    if(helper.validNodes(cmp,event,helper,false,node.dateCalcs,node.nodeCalcs)){
                        console.log('errorOnthisNode:::'+JSON.stringify(node));
                        notValid = true;
                    }
                }
            } else if(node.ruleDetail.Parent_Condition_Type__c == 'Case'){
                if(node.nodeCalcs.length){
                    for(var i of node.nodeCalcs){
                        if(helper.validForCase(cmp,event,helper,i,false)){
                            notValid = true;
                            break;
                        }
                    }
                }
            }
        } else{
            notValid = true;
        }
        return notValid;
    }
})