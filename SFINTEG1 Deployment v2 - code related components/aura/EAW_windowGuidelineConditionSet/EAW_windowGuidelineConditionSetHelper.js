({
	setOperandList : function(cmp,event,helper,selectedValue,notInit) {
        if(!$A.util.isEmpty(selectedValue)){
            if(notInit){
                if(cmp.get('v.fromCmp') == 'Condition'){
                	cmp.get('v.rule').Conditional_Operand_Id__c = '';
                } else{
                    cmp.get('v.ruleDetail').Conditional_Operand_Id__c = '';
                }
            }
            if(selectedValue == 'Release Date Status'){
                cmp.set('v.operandList',cmp.get('v.releaseDateGuidelineList'));
            } else if(selectedValue == 'Window Tag' || selectedValue == 'Window Schedule Status'){
                var operandList = JSON.parse(JSON.stringify(cmp.get('v.windowGuidelineList'))); // LRCC - 1096
                for(var i in operandList){
                    if(operandList[i].value == cmp.get('v.recordId')){
                        //operandList.splice(i,1);
                        break;
                    }
                }
                cmp.set('v.operandList',operandList);
            }
        }
	},
    isActiveConditionId : function(cmp,idList){
        /*  return cmp.get('v.operandList').find(function(element){
            if(element.value == id){
                return true;
            } else{
                return false;
            }
        }); */
        var idValidList = [];
        
        cmp.get('v.operandList').find(function(element){
            if(idList.includes(element.value)){
                idValidList.push(element);
            }
        });
        return idValidList; 
    }
})