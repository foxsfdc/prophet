({
	init: function (component, event, helper) {
        
        var action = component.get("c.getFields");
        action.setParams (
            {
                "objFieldSetMap": {
                    "EAW_Release_Date__c": "Related_List_Fields"
                }
            }
        );
        
        action.setCallback(this, function (response) {
            
            var state = response.getState();
            if(state === 'SUCCESS') {
                
                var columns = new Array();
                
                for(let col of response.getReturnValue()) {
                    for(let colName of col) {
                
                        if(colName.fieldName == 'Name') {
                            columns.push(
                                	{label: 'Release Date Number', fieldName: 'link', type: 'url',
                                          typeAttributes: { label: {fieldName: 'Name' }, target: '_blank'}, sortable: true});
            			}
                        else if(colName.fieldName == 'Title__c') {
                            columns.push({label: 'Title Name', fieldName: 'titleLink', type: 'url',
                                          typeAttributes: { label: {fieldName: 'titleName' }, target: '_blank'}, sortable: true});
                        }
                        else if(colName.fieldName == 'Release_Date__c') {
                            columns.push({label: 'Release Date', fieldName: 'Release_Date__c', type: 'date-local'})
                        }
                        else {
                            columns.push(colName);
                        }
                   	}
                }
                component.set('v.myColumns', columns);
                helper.getReleaseDates(component, event, helper);
            } 
            else if(state === 'ERROR') {
                let errors = response.getError();
                let handleErrors = helper.handleErrors(errors, component, event, helper);
                if(handleErrors) {
                    helper.errorToast(handleErrors);
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    addReleaseDate: function(component, event, helper) {
        
        let createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            "entityApiName": "EAW_Release_Date__c",
            "defaultFieldValues": {
                'Release_Date_Guideline__c' : component.get("v.recordId")
            }
        });
        createRecordEvent.fire();
    },
})