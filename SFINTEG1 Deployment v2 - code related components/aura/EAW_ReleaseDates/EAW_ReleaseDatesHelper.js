({
	getReleaseDates : function(component, event, helper) {
        
        let action = component.get("c.getReleaseDates");
        action.setParams({"recordId": component.get("v.recordId")});
        
        action.setCallback(this, function(response) {
            
            let state = response.getState();
            if(state === 'SUCCESS') {
            
                let results = response.getReturnValue();
            	for(let result of results) {
            		if(result.Title__r) {
                        result.titleName = result.Title__r.Name;
            		}
                    result.link = '/one/one.app?sObject/'+ result.Id + '/view#/sObject/' + result.Id + '/view';
                    result.titleLink = '/one/one.app?sObject/'+ result.Title__c + '/view#/sObject/' + result.Title__c + '/view';
	            }
	            component.set('v.releaseDateData', results);
                component.set('v.showSpinner', false);
            } 
            else if(state === 'ERROR') {
                let errors = response.getError();
                let handleErrors = helper.handleErrors(errors, component, event, helper);
                if(handleErrors) {
                    helper.errorToast(handleErrors);
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    handleErrors: function(errors, component, event, helper) {
		
        let errorMessage = '';
        if (errors && Array.isArray(errors) && errors.length > 0) {
    		
            errors.forEach(error => {
        	    if (error.message && errorMessage.includes(error.message) == false) {
	                errorMessage += error.message + "\n";
	            }
	            if (error.pageErrors && error.pageErrors.length > 0) {
	                error.pageErrors.forEach(pageError => {
	                    if (errorMessage.includes(error.message) == false)
	                        errorMessage += pageError.message + "\n";
	                });
	            } else if (error.fieldErrors) {
	                let fields = Object.keys(error.fieldErrors);
	                fields.forEach(fieldError => {
	                    if (Array.isArray(fieldError)) {
	                        fieldError.forEach(err => {
	                            if (errorMessage.includes(error.message) == false)
	                                errorMessage += err.message;
	                        });
	                    }
	                });
	            }
	        });
	    } else {
            errorMessage = errors;
        }
	    if (errorMessage.indexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION') !== -1) {
	        
            let errorMessageList = errorMessage.split('FIELD_CUSTOM_VALIDATION_EXCEPTION,');
	        if (errorMessageList.length > 1) {
	            let finalErroList = errorMessageList[1].split(': []');
	            if (finalErroList.length) {
	                errorMessage = finalErroList[0];
	            }
	        }
	    }
	    if (errorMessage.indexOf('REQUIRED_FIELD_MISSING') !== -1) {
	        
            let errorMessageList = errorMessage.split('REQUIRED_FIELD_MISSING,');
            if (errorMessageList.length > 1) {
                let finalErrorList = errorMessageList[1].split(': []');
	            if (finalErrorList.length) {
                	errorMessage = finalErrorList[0];
                }
	        }
	    } else {
            let errMsg = errorMessage.split(",").join("\n");
            errorMessage = errMsg;
        }
		console.log('***** errorMessage-->', errorMessage);
        return errorMessage;
	},
    
    errorToast : function(message) {
    	var toastEvent = $A.get('e.force:showToast');
    	toastEvent.setParams({
			'title': 'Error!',
			'type': 'error',
			'message': message
		});
		toastEvent.fire();
    },
    
    successToast : function(message) {
    	var toastEvent = $A.get('e.force:showToast');
    	toastEvent.setParams({
			'title': 'Success!',
			'type': 'success',
			'message': message
		});
		toastEvent.fire();
    }
})