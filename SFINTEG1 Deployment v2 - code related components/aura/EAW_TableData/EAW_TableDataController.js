({
    doInit : function(component, event, helper) {
        
        component.set('v.spinner',true);
        var column = component.get("v.column");
        var data = component.get("v.data"); 
       // console.log('col:::',column.fieldName);
       // console.log('dataval:::',data[column.fieldName]);
        //console.log('column:::::',column)
       // console.log('data:::ss::',JSON.stringify(data));
        // Tags string to object
        if(!$A.util.isEmpty(column) && !$A.util.isEmpty(column.fieldName)){
            //component.set("v.canShowData",true);
            
            if (data[column.fieldName] && (typeof (data[column.fieldName]) == 'object') && data[column.fieldName].length > 0) {
                
                component.set('v.selectedRecords',data[column.fieldName]);
                var temp = [];
                data[column.fieldName].forEach(function(element) {
                    var title;
                    if (element.Name) { title = element.Name; }
                    else { title = element.title; }
                    temp.push(title);
                });
                component.set("v.displayValue",temp.join(', '));
            } else  {
                component.set("v.displayValue",data[column.fieldName]);     
            }
            component.set('v.updateable',column.updateable);    
           // var timeout = component.get('v.isWindow') ? 150 : 0;
           // window.setTimeout($A.getCallback(function(){
                component.set("v.canShowData",true);
                component.set('v.spinner',false);
            //}),timeout);
        }
    },
    getBaseUrl : function(component,event,helper) {
        var action = component.get("c.getBaseUrl");
        action.setCallback(this, function(response) {
            var state = response.getState();
            var result = response.getReturnValue();
            if (state === 'SUCCESS') 
            {
                component.set("v.baseUrl",result);
                
            } else if (state === 'ERROR') {
                console.log(JSON.stringify(response.getError()));
            }
        });
        $A.enqueueAction(action);
    },
    editTable : function(component, event, helper) {
        component.set('v.inputTable',event.getParam("editMode"));
    },
    massUpdateTable : function(component, event, helper) {
        var column = component.get("v.column");
        var data = component.get('v.data');
        component.set('v.displayValueEdit[0].title',data[column.fieldName]);
    },
    getTypeAheadData : function(component, event, helper) {
        //upd
        
        var column = component.get("v.column");
        var data = component.get("v.data");
        
        if(!$A.util.isEmpty(component.get("v.displayValueEdit"))){
            
            // LRCC:1096 start
            var tagTitles = [];
            for (var i=0;i<component.get("v.displayValueEdit").length;i++) {
                tagTitles.push(component.get("v.displayValueEdit")[i].title);
            }
            var title = tagTitles.join(', ');
            //console.log('title:::',title);
            component.set("v.displayValue",title);
            
            //upd
            //LRCC-1327 - issue 1 
            if ((data[column.fieldName] && (typeof (data[column.fieldName]) == 'object') && data[column.fieldName].length != undefined)||component.get("v.displayValueEdit").length) {
                console.log('tag;;;;');
                data[column.fieldName] = component.get("v.displayValueEdit");
            } else {
                data[column.fieldName] = title;   
            }
            
            component.set("v.data",data);
            // LRCC:1096 end
            
        } else if(event.getParam("value")[0] != event.getParam("oldValue")[0]){            
            
            //upd
            if (data[column.fieldName] && (typeof (data[column.fieldName]) == 'object') && data[column.fieldName].length != undefined) {
                data[column.fieldName] = [];   
            } else {
                data[column.fieldName] = '';   
            }
            component.set("v.displayValue",'');            
            component.set("v.data",data);
        }
        
        // LRCC:1096 start
        //console.log('rec::::', component.get("v.recordMap"));
        //console.log('displayValueEdit::::', component.get("v.displayValueEdit"));
        
        if (data[column.fieldName] && (typeof (data[column.fieldName]) == 'object') && data[column.fieldName].length != undefined) {
            
            var recordMap = component.get("v.recordMap") || {};
            var tagIds = [];
            for(var i=0;i<component.get("v.displayValueEdit").length;i++) {
                
                if (component.get("v.displayValueEdit")[i].id) {
                    tagIds.push(component.get("v.displayValueEdit")[i].id);
                }
            }
            
            if(data && data.Id) {
                
                var dataId = data.Id;
                recordMap[dataId] = tagIds;
                //console.log(';;recordMap::',JSON.parse(JSON.stringify(recordMap)));
                component.set("v.recordMap", recordMap);  
            }
        }
        // LRCC:1096 end
    },
    //LRCC - 1285 and LRCC - 1015
    showInfo : function(component,event,helper) {
        var column = component.get("v.column");
        var data = component.get("v.data");
        if(column.fieldName == 'Allow_Manual__c' && data[column.fieldName] == false) {
            helper.showToast('info', 'There may be manual dates entered for this release date. These will not be overwritten or removed.');
        }
    },
    getNonTypeaheadData:function(component,event,helper) {
        //Tags string to object
       // console.log('getNonTypeaheadData::::');
        if(!$A.util.isEmpty(component.get("v.displayValue"))){
            var column = component.get("v.column");
            var title = component.get("v.displayValue");
            var data = component.get("v.data");
            
            //upd
            if (data[column.fieldName] && (typeof (data[column.fieldName]) == 'object') && data[column.fieldName].length != undefined) {
                
                if (component.get('v.displayValueEdit').length > 0) {
                    data[column.fieldName] = component.get('v.displayValueEdit');
                } else {
                    data[column.fieldName] = component.get("v.selectedRecords");
                }
            } else {
                data[column.fieldName] = title; 
                if(event.getParam("value") != event.getParam("oldValue")){ // LRCC - 1082
                    if(column.fieldName == 'Manual_Date__c' && data['Status__c']) { // LRCC - 1638
                        if(data['Status__c'] == 'Firm'){
                            data['Temp_Perm__c'] = 'Permanent';
                            var manualDateChangeEvent = component.getEvent("manualDateChangeEvent");                     
                            manualDateChangeEvent.fire(); 
                            //var message = 'The Release Date Status is Firm. Updating the Manual date will update the Release Date.Do you want to continue ?';
                            //component.find('modifierPrompt').openModal(event.getParam("oldValue"),event.getParam("value"),message,'Manual_Date__c');
                        } else if(data['Status__c'] == 'Not Released' || data['Status__c'] == 'Estimated'){
                            data['Temp_Perm__c'] = 'Temporary';
                            var manualDateChangeEvent = component.getEvent("manualDateChangeEvent");                     
                            manualDateChangeEvent.fire(); 
                        }
                        //console.log('data:::',JSON.parse(JSON.stringify(data)));
                    }
                }
            }            
            
            component.set("v.data",data);
            
        } else if(event.getParam("value") != event.getParam("oldValue")){
            
            component.set("v.displayValue",'');
            var column = component.get("v.column");
            var data = component.get("v.data");
            //upd
            if (data[column.fieldName] && (typeof (data[column.fieldName]) == 'object') && data[column.fieldName].length != undefined) {
                data[column.fieldName] = [];
            } else {
                data[column.fieldName] = '';
                //LRCC-1445
                if(column.fieldName == 'Manual_Date__c') {
                    data['Temp_Perm__c'] = 'None';
                    var manualDateChangeEvent = component.getEvent("manualDateChangeEvent");                     
                    manualDateChangeEvent.fire(); 
                }
            }
            
            component.set("v.data",data);
        }
    },
    handleDataModifyConfirmation : function(cmp,event,helper){ // LRCC - 1638
      //  console.log(':::::'+JSON.stringify(event.getParams()));
        
        var params = event.getParams();
        if(params.field){
            var data = cmp.get("v.data");
            if(params.field == 'Manual_Date__c'){
                data['Manual_Date__c'] = params.valueToChange;
                if(params.confirmation){
                    data['Temp_Perm__c'] = 'Permanent';
                    var manualDateChangeEvent = cmp.getEvent("manualDateChangeEvent");                     
                    manualDateChangeEvent.fire(); 
                }
            }
            cmp.set('v.data',data);
        }
    }
})