({
    doInit : function(component, event, helper) {
        component.set('v.tagColumns', [
            {label: 'Name', fieldName: 'Name', type: 'text'},
            {label: 'Tag Type', fieldName: 'Tag_Type__c', type: 'picklist'},
            {label: 'Active', fieldName: 'isActive', type: 'text'}
        ]);
        
        var selectedTags = component.get('v.selectedTags');
        var action = component.get("c.getTagInfo");
        action.setParams({
            'tagIds': selectedTags
        });
        
        console.log('ma tags: ' + selectedTags);
        action.setCallback(this, function(response) {
            var state = response.getState();
            var results= response.getReturnValue();
            
            // Loop through results and add a column 'isActive' so it can be shown in the datatable
            if(results != null) {
                for(let i = 0; i < results.length; i++) {
                    results[i].isActive = results[i].Active__c ? 'true' : 'false';              
                }    
            }
            
            component.set('v.showSpinner', false);
            component.set('v.tagList', results);
        });
        $A.enqueueAction(action);
    },
    downloadCsv : function(component,event,helper) {
        console.log('download clicked');
        var tagList = component.get('v.tagList');
        if(tagList == null) {
            return;
        }
        
        var csv = helper.convertArrayToCSV(component, tagList);
        
        console.log('ma csv= ' + csv);
        var hiddenElement = document.createElement('a');
        hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
        hiddenElement.target = '_self'; // 
        hiddenElement.download = 'exported-tags.csv';  // CSV file Name* you can change it.[only name not .csv] 
        document.body.appendChild(hiddenElement); // Required for FireFox browser
        hiddenElement.click(); // using click() js function to download csv file   
    }
})