({
    //LRCC:1010
    notesAttachmentAction : function(component, event, helper) {
        var params = event.getParam('arguments');
        if (params) {
            component.set('v.massUpdatedRows', params.selectedRows);
            var title = params.notesTitle;
            var body = params.notesBody;
            //LRCC-1010
            component.set('v.notesAction', params.notesAction);
            console.log('title::',title);
            console.log('body::',body);            
            var action = component.get("c.fetchUser");
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    console.log('res::',response.getReturnValue());
                    var storeResponse = response.getReturnValue();   
                    console.log(';;;;',storeResponse);
                    component.set('v.notesAttachement', {
                        'CreatedBy' : {'Name' : storeResponse.userDetail.Name, 'Id' : storeResponse.userDetail.id},
                        'LastModifiedDate' : storeResponse.timeZone,
                        'Title' : title,
                        'Body' : body
                    });
                }
            });
            $A.enqueueAction(action);    
        }
    },   
    
    displayNotes : function(component, event, helper) {
        
        var recId = event.getParam("parentId");
        //LRCC:1010
        let selectedRows = component.get('v.massUpdatedRows');
        // set the handler attributes based on event data
        component.set("v.parentId", recId);
        var action = component.get("c.getNotes");
        action.setParams({
            "recId" : recId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            var result = response.getReturnValue();
            if (state === 'SUCCESS') {
                
                console.log('Results:',result);
                //LRCC:1010
                console.log(';;;;;',component.get('v.notesAttachement'));
                if (component.get('v.notesAttachement') && Object.keys(component.get('v.notesAttachement')).length > 0 && selectedRows.indexOf(recId) != -1) {
                    console.log('testtttt');
                    var notesAction = component.get('v.notesAction');
                    console.log('notesAction::',notesAction);
                    if(notesAction == 'clear') {
                        result.attachmentList = [];
                    } else if(notesAction == 'replace') {
                        result.attachmentList = [component.get('v.notesAttachement')];                        
                    } else {
                        result.attachmentList.push(component.get('v.notesAttachement'));
                    }
                }                
                component.set('v.notes',result.attachmentList);                
                console.log('notes::::',component.get('v.notes'));
                
            } else if (state === 'ERROR') {
                console.log(JSON.stringify(response.getError()));
            }
        });
        $A.enqueueAction(action);
        component.set('v.displayModal',true);
    },
    saveNotes : function(component, event, helper) {
        
        var newNote = component.get('v.body');
        var action = component.get("c.insertNotes");
        var title = component.get('v.title');
        var titleField = component.find("titleValue");
        var recIds = new Array();
        recIds.push(component.get("v.parentId"));
        
        if(title!=null && title.trim()!='') {
            
            action.setParams({
                "parentIds": recIds,
                "note":newNote,
                "title":title
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                var result = response.getReturnValue();
                if (state === 'SUCCESS') {
                    var newNotes = component.get("v.notes");
                    newNotes.push(response.getReturnValue()[0]);
                    component.set("v.notes", newNotes);
                    
                } else if (state === 'ERROR') {
                    console.log(JSON.stringify(response.getError()));
                }
            });
            $A.enqueueAction(action);
            component.set('v.body','');
            component.set('v.title','');
        } else {
            let label = $A.get("$Label.c.EAW_NotesSubjectValidation");
            helper.errorToast(label);  
        }
    },
    hideNotesModal : function(component, event, helper) {
        component.set('v.displayModal',false);
        component.set('v.displayAddNote', false);
        component.set('v.body','');
        component.set('v.title','');
        //LRCC-1015
        var customNotesEvent = component.getEvent("customNotesEvent");
        customNotesEvent.fire();
    },
    addNotesModal : function(component, event, helper) {
        component.set('v.displayAddNote', true);
        component.set('v.body','');
        component.set('v.title','');
    }
})