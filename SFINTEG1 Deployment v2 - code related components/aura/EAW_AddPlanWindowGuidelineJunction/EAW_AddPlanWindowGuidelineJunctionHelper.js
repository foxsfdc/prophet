({
    searchPromise : function(component, helper) {
        var searchText = component.get('v.searchText');
        var action = component.get('c.searchForIds');
        
        return new Promise(function(resolve, reject) {
            action.setParams({searchText: searchText,planId: component.get('v.recordId')});
            
            if(searchText.length > 1) {
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    
                    if(state === 'SUCCESS') {
                        var windowGuidelines = response.getReturnValue();
                        var windowGuidelinesWithProductType = [];
                        
                        if(windowGuidelines.length > 0 && component.get('v.productTypeAttribute')) {
                            for(var i=0;i<windowGuidelines.length;i++) {
                                if(windowGuidelines[i].Product_Type__c) {
                                    if(component.get('v.productTypeAttribute').indexOf(windowGuidelines[i].Product_Type__c) != -1) {
                                        windowGuidelinesWithProductType.push(windowGuidelines[i]);
                                    }
                                }
                            }
                        } 
                        if(windowGuidelinesWithProductType.length > 0){
                            component.set("v.searchWindowGuidelineResults",windowGuidelinesWithProductType);
                        } else {
                            //component.set("v.searchWindowGuidelineResults", windowGuidelines);
                        }
                        
                        if(windowGuidelinesWithProductType.length > 0) {
                            helper.hideErrorForWindowGuidelinesList();
                            helper.showExistingWindowGuidelinesList(component);
                        } else {
                            helper.hideExistingWindowGuidelinesList(component);
                            helper.showErrorForWindowGuidelinesList();
                        }
                    } else {
                        helper.hideExistingWindowGuidelinesList(component);
                        helper.showErrorForWindowGuidelinesList();
                    }
                });
                $A.enqueueAction(action);
            } else {
                helper.hideExistingWindowGuidelinesList(component);
                helper.showErrorForWindowGuidelinesList();
            }
        });
    },
    
    showErrorForWindowGuidelinesList : function() {
        document.getElementById("error-window-guideline-results").style.display = "block";
    },
    
    hideErrorForWindowGuidelinesList : function() {
        document.getElementById("error-window-guideline-results").style.display = "none";
    },
    
    showExistingWindowGuidelinesList : function(component) {
        console.log('show');
        if(component.find('existingWindowGuidelineModal')) $A.util.removeClass(component.find('existingWindowGuidelineModal'),'slds-hide');
       // document.getElementById("existing-window-guideline-results").style.display = "block";
    },
    
    hideExistingWindowGuidelinesList : function(component) {
        
        if(component.find('existingWindowGuidelineModal') && !$A.util.hasClass(component.find('existingWindowGuidelineModal'),'slds-hide')){
            component.set('v.searchText','');
            component.set("v.searchWindowGuidelineResults",[]);
            $A.util.addClass(component.find('existingWindowGuidelineModal'),'slds-hide');
        }
       // document.getElementById("existing-window-guideline-results").style.display = "none";
    },
    
    hideAddPlanWindowGuidelineModal : function(component,helper) {
        helper.hideExistingWindowGuidelinesList(component);
        $A.util.removeClass(component.find('modal'),'slds-fade-in-open');
        $A.util.removeClass(component.find('backdrop'),'slds-backdrop_open');
        //document.getElementById("eaw-add-plan-window-guideline-junction-modal").style.display = "none";
    },
    
    errorToast : function(message) {
    	var toastEvent = $A.get('e.force:showToast');
    		
		toastEvent.setParams({
			'title': 'Error!',
			'type': 'error',
			'message': message
		});
		
		toastEvent.fire();
    },
    
    successToast : function(message) {
    	var toastEvent = $A.get('e.force:showToast');
    		
		toastEvent.setParams({
			'title': 'Success!',
			'type': 'success',
			'message': message
		});
		 
		toastEvent.fire();
    },
    openModal : function(cmp){
        $A.util.addClass(cmp.find('modal'),'slds-fade-in-open');
        $A.util.addClass(cmp.find('backdrop'),'slds-backdrop_open');
    }
})