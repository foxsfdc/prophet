({
	setFieldValues: function (cmp, event, helper) {
		var fieldValueList = [];
		for (var field of cmp.get('v.recordFields')) {
			var fieldNode = {
				'fieldName': field,
			};
			fieldValueList.push(fieldNode);
		}
		cmp.set('v.fieldValueList', fieldValueList);
	},
    
	toast: function (type, message) {
		var toastEvent = $A.get('e.force:showToast');
		toastEvent.setParams({
			'type': type,
			'message': message
		});
		toastEvent.fire();
	}
})