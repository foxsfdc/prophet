({
	openPopUp : function(component,event,helper) {
        
        var params = event.getParam('arguments');
        if(params && params.recordId){
            var action = component.get("c.getUsedByRecords");
                    
            action.setParams({
                recordId   : params.recordId,
                objectName : component.get("v.objectName")
            });
            
            action.setCallback(this,function(response){
                
                if(response.getState() === 'SUCCESS'){
                    
                   // component.set("v.usedGuidelines", response.getReturnValue());
                    if(response.getReturnValue().wgDateMap){
                        component.set('v.windowGuidelineDateMap',response.getReturnValue().wgDateMap);
                    }
                    console.log('records : ', JSON.stringify(response.getReturnValue()));
                    helper.setData(component,response.getReturnValue());
                    $A.util.addClass(component.find('modal'),'slds-fade-in-open');
                    $A.util.addClass(component.find('backDrop'),'slds-backdrop_open');
                    component.set('v.showModal',true);
                    //component.find("modalCmp").openModal();
                }
            });
            $A.enqueueAction(action);  
        }
    },
    
    closeModal : function(cmp,event,helper){
        cmp.set('v.showModal',false);
        $A.util.removeClass(cmp.find('modal'),'slds-fade-in-open');
        $A.util.removeClass(cmp.find('backDrop'),'slds-backdrop_open');
    }
})