({
	showSpinner:function(component){
        component.set('v.showSpinner',true);
    },
    hideSpinner:function(component){
        component.set('v.showSpinner',false);
    }
})