({
	init : function(cmp, event, helper) {
        cmp.set('v.spinner',true);
        var params = event.getParam('arguments');
        if(params){
            cmp.set('v.recordId',params.windowId);
            
            var action = cmp.get('c.getWindowDetails');
            action.setParams({
                'recordId' : cmp.get('v.recordId')
            });
            action.setCallback(this,function(response){
                var state = response.getState();
                if(state == 'SUCCESS'){
                    console.log(':::::::::::'+JSON.stringify(response.getReturnValue()));
                    if(response.getReturnValue().Title_Name__c){
                        cmp.set('v.titleName',response.getReturnValue().Title_Name__c);
                    } else if(response.getReturnValue().Name && (response.getReturnValue().Name).split('-').length > 1){
                        cmp.set('v.titleName',(response.getReturnValue().Name).split('-')[1]);
                    }
                    cmp.set('v.windowRecord',response.getReturnValue());
                    $A.util.addClass(cmp.find('modal'),'slds-fade-in-open');
                    $A.util.addClass(cmp.find('backDrop'),'slds-backdrop_open');
                } else {
                    console.log('error:::',response.getError());
                }
                cmp.set('v.spinner',false);
            });
            $A.enqueueAction(action);
        }
    },
    closeModal : function(cmp){
        cmp.get('v.windowRecord').Id = null;
        cmp.set('v.windowRecord',{'sObjectType':'EAW_Window__c'});
        $A.util.removeClass(cmp.find('modal'),'slds-fade-in-open');
        $A.util.removeClass(cmp.find('backDrop'),'slds-backdrop_open');
        
    }
})