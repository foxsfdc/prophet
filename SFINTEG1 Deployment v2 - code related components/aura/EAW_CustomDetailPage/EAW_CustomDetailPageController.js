({
    doInit : function(component, event, helper) {
        helper.fieldSet(component, event, helper);
    },
    
    saveModalforfirm : function(component, event, helper) {
        
        component.set('v.spinner',true); 
        let data = JSON.parse(JSON.stringify(component.get('v.changedRecord'))); 
        
        if(component.get('v.sObjectName') == 'EAW_Release_Date__c') {
            if(data.Manual_Date__c) {
            	data.Release_Date__c = data.Manual_Date__c;
            } else if(! data.Feed_Date__c && data.Status__c != 'Not Released') {                
                data.Status__c = 'Estimated';
                data.Temp_Perm__c = '';
            } else {
                data.Release_Date__c = data.Feed_Date__c;
            }
        }
        
        console.log('data::::',data);
        component.find('recordForm').submit(data);
        component.find("modalCmpforfirm").close();
    },
    
    closeModalforfirm : function(component, event, helper) {
        
        component.find("modalCmpforfirm").close();
        component.set('v.spinner',false); 
    },
    
    handleValidationError : function(component ,event,helper) {
        
        if(component.find('myDiv')) {            
            let myDiv = component.find('myDiv').getElement();
            myDiv.style.display = myDiv.style.display == 'block' ? 'none' : 'block';      
        }
    },
    
    handleLoad : function(component ,event,helper) {
        
        component.set('v.spinner',true);
        var record = event.getParam("recordUi").record.fields;
        component.set('v.originalRecord', JSON.parse(JSON.stringify(record)));
        component.set('v.spinner',false);
    },
    
    handleSubmit : function(component ,event,helper) {
        
        console.log('hiiii');
        component.set('v.spinner',true);
        event.preventDefault(); // stop form submission
        var eventFields = event.getParam("fields");
        console.log('eventFields::',JSON.parse(JSON.stringify(eventFields)));        
        var canSubmit = true;
        var inputFields = component.find('inputFields');
        
        for(var i=0; i< inputFields.length; i++) {
            
            if((inputFields[i].get('v.fieldName') == 'Name' || inputFields[i].get('v.fieldName') == 'Release_Date_Guideline__c' || inputFields[i].get('v.fieldName') == 'Title__c') && ! inputFields[i].get('v.value')) {
                canSubmit = false;
                console.log('fffff');
                $A.util.addClass(inputFields[i], ' requiredBorder');                
            }
        }
        
        if(canSubmit) {
            
            var modalOpen = false;            
            let data = JSON.parse(JSON.stringify(eventFields));    
            var record = component.get('v.originalRecord');
            console.log('record::',JSON.parse(JSON.stringify(record))); 
            
            if(component.get('v.sObjectName') == 'EAW_Window__c') {
                
                if(record.Status__c.value == 'Firm' &&
                   (record.Start_Date__c.value != data.Start_Date__c || 
                    record.End_Date__c.value != data.End_Date__c ||
                    record.Outside_Date__c.value != data.Outside_Date__c ||
                    record.Tracking_Date__c.value != data.Tracking_Date__c)) {
                    //&& ! (! data.Start_Date__c && ! data.End_Date__c && record.Status__c.value != data.Status__c)
                    
                    component.set('v.changedRecord', data);
                    component.set('v.popupMessage','The window schedule status is Firm. Updating the date will update the window schedule date. Do you want to continue?');
                    component.find("modalCmpforfirm").open(); 
                    modalOpen = true;
                    component.set('v.spinner',false); 
                }
            } else {
            
                if(record.Status__c.value == 'Firm' && record.Manual_Date__c.value != data.Manual_Date__c) {
                    
                    component.set('v.changedRecord', data);
                    component.set('v.popupMessage','The release date status is Firm. Updating the manual date will update the release date. Do you want to continue?');
                    component.find("modalCmpforfirm").open();   
                    modalOpen = true;
                    component.set('v.spinner',false); 
                }
            }
            
            if(! modalOpen) {
                component.find('recordForm').submit(data);
            }
            component.set('v.errorList', []);
        } else {
            component.set('v.spinner',false); 
        }
    },
    
    handleError : function(component ,event,helper) {
        
        var errorList = [];
        
        console.log('error:::',JSON.parse(JSON.stringify(event.getParam("error"))));
        
        if(event.getParam("error") && event.getParam("error").body && event.getParam("error").body.output) {
            
            for(let error of event.getParam("error").body.output.errors) {
                errorList.push(error.message);
            }
        }
        component.set('v.errorList', errorList);
        
        component.set('v.spinner',false);    
    },
    
    handleSuccess : function(component,event,helper) { 
        
        console.log(';;;;',event.getParams().response);
        var base_url = window.location.origin;
        if(component.get('v.showEditModal')) {
            component.set('v.showEditModal',false);
        } else if(component.get('v.recordId')) {
                location.replace(base_url+"/lightning/r/"+component.get('v.sObjectName')+"/"+component.get('v.recordId')+"/view");
        }
        component.set('v.spinner',false);            
    },
    
    handleCancel : function(component,event,helper) { 
        
        var base_url = window.location.origin;
        if(component.get('v.showEditModal')) {
            component.set('v.showEditModal',false);
        } else if(component.get('v.recordId')) {
                location.replace(base_url+"/lightning/r/"+component.get('v.sObjectName')+"/"+component.get('v.recordId')+"/view");
        }
    }
})