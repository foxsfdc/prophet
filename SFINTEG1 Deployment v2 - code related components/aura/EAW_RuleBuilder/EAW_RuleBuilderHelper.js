({
    findBasedOnDateOptions : function(sObjectType) {
        return [
            {label: 'EST - UK (1R) / Start Date', value: 'EST - UK (1R) / Start Date'},
            {label: 'EST - UK (1R) / End Date', value: 'EST - UK (1R) / End Date'},
            {label: 'Pay TV - UK (1R) / Start Date', value: 'Pay TV - UK (1R) / Start Date'},
            {label: 'Pay TV - UK (1R) / End Date', value: 'Pay TV - UK (1R) / End Date'},
            {label: 'Pay TV - UK (1st Cycle Library) / Start Date', value: 'Pay TV - UK (1st Cycle Library) / Start Date'},
            {label: 'Pay TV - UK (1st Cycle Library) / End Date', value: 'Pay TV - UK (1st Cycle Library) / End Date'},
            {label: 'Free / Basic TV - UK (1R) / Start Date', value: 'Free / Basic TV - UK (1R) / Start Date'},
            {label: 'Free / Basic TV - UK (1R) / End Date', value: 'Free / Basic TV - UK (1R) / End Date'},
            {label: 'VOD - UK (1R) / Start Date', value: 'VOD - UK (1R) / Start Date'},
            {label: 'VOD - UK (1R) / End Date', value: 'VOD - UK (1R) / End Date'},
            {label: 'Free / Basic TV - UK (1st Cycle Library) / Start Date', value: 'Free / Basic TV - UK (1st Cycle Library) / Start Date'},
            {label: 'Free / Basic TV - UK (1st Cycle Library) / End Date', value: 'Free / Basic TV - UK (1st Cycle Library) / End Date'},
            {label: 'Hotel PPV - UK (1R) / Start Date', value: 'Hotel PPV - UK (1R) / Start Date'},
            {label: 'Hotel PPV - UK (1R) / End Date', value: 'Hotel PPV - UK (1R) / End Date'},
            {label: 'VOD - UK (Lib) / Start Date', value: 'VOD - UK (Lib) / Start Date'},
            {label: 'VOD - UK (Lib) / End Date', value: 'VOD - UK (Lib) / End Date'},
            {label: 'SVOD - UK (1R, Netflix) / Start Date', value: 'SVOD - UK (1R, Netflix) / Start Date'},
            {label: 'SVOD - UK (1R, Netflix) / End Date', value: 'SVOD - UK (1R, Netflix) / End Date'},
            {label: 'SVOD - UK (2R, Netflix) / Start Date', value: 'SVOD - UK (2R, Netflix) / Start Date'},
            {label: 'SVOD - UK (2R, Netflix) / End Date', value: 'SVOD - UK (2R, Netflix) / End Date'},
            {label: 'Home PPV - UK (1R) / Start Date', value: 'Home PPV - UK (1R) / Start Date'},
            {label: 'Home PPV - UK (1R) / End Date', value: 'Home PPV - UK (1R) / End Date'},
            {label: 'Pay TV - Spain (1R) / Start Date', value: 'Pay TV - Spain (1R) / Start Date'},
            {label: 'Pay TV - Spain (1R) / End Date', value: 'Pay TV - Spain (1R) / End Date'},
            {label: 'EST - Spain (1R) / Start Date', value: 'EST - Spain (1R) / Start Date'},
            {label: 'EST - Spain (1R) / End Date', value: 'EST - Spain (1R) / End Date'},
            {label: 'Theatrical / United States', value: 'Theatrical / United States'},
            {label: 'Theatrical / United Kingdom Of Great Britain And Northern Ireland', value: 'Theatrical / United Kingdom Of Great Britain And Northern Ireland'},
            {label: 'EST / United Kingdom Of Great Britain And Northern Ireland', value: 'EST / United Kingdom Of Great Britain And Northern Ireland'},
            {label: 'HV / United Kingdom Of Great Britain And Northern Ireland', value: 'HV / United Kingdom Of Great Britain And Northern Ireland'},
            {label: 'VOD / United Kingdom Of Great Britain And Northern Ireland', value: 'VOD / United Kingdom Of Great Britain And Northern Ireland'}
        ];
    },

    errorToast : function(message) {
        var toastEvent = $A.get('e.force:showToast');

        toastEvent.setParams({
            'title': 'Error!',
            'type': 'error',
            'message': message
        });

        toastEvent.fire();
    },

    successToast : function(message) {
        var toastEvent = $A.get('e.force:showToast');

        toastEvent.setParams({
            'title': 'Success!',
            'type': 'success',
            'message': message
        });

        toastEvent.fire();
    }
})