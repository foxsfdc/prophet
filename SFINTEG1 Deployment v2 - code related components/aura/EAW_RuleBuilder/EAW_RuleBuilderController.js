({
    doInit : function(component, event, helper) {
        let sObjectType = component.get('v.sObjectType');
        let recordId = component.get('v.recordId');
        let action = component.get('c.getRules');

        component.set('v.basedOnDateRuleOptions', helper.findBasedOnDateOptions(sObjectType));

        action.setParams({
            'id': recordId,
            'sObjectType': sObjectType
        });

        action.setCallback(this, function(response) {
            let state = response.getState();
            
            if(state === 'SUCCESS') {
                let results = response.getReturnValue();
                let startDateRules = component.get('v.startDateRules');
                let endDateRules = component.get('v.endDateRules');

                if(results && results.ruleDetails.length > 0) {
                    for (let rule of results.ruleDetails) {
                        if (rule.Condition_Set_Field__c == 'Window Start Date') {
                            startDateRules.months = rule.Condition_Met_Months__c;
                            startDateRules.days = rule.Condition_Met_Days__c;
                            startDateRules.territory = rule.Condition_Met_Territory__c ? rule.Condition_Met_Territory__c : '';
                            startDateRules.media = rule.Condition_Met_Media__c;
                            startDateRules.id = rule.Id;
                            startDateRules.windowName = rule.WindowName__r ? rule.WindowName__r.Name : '';
                            
                            if(rule.Start_Date__c) {
                                startDateRules.dateType = 'Start Date';
                                
                                startDateRules.basedOnDate = startDateRules.media + ' - ' + startDateRules.windowName + ' / ' + startDateRules.dateType;
                            } else if(rule.End_Date__c) {
                                startDateRules.dateType = 'End Date';
                                
                                startDateRules.basedOnDate = startDateRules.media + ' - ' + startDateRules.windowName + ' / ' + startDateRules.dateType;
                            } else {
                                startDateRules.basedOnDate = startDateRules.media + ' / ' + startDateRules.territory;
                            }
                        } else if(rule.Condition_Set_Field__c == 'Window End Date') {
                            endDateRules.months = rule.Condition_Met_Months__c;
                            endDateRules.days = rule.Condition_Met_Days__c;
                            endDateRules.territory = rule.Condition_Met_Territory__c;
                            endDateRules.media = rule.Condition_Met_Media__c;
                            endDateRules.id = rule.Id;
                            endDateRules.windowName = rule.WindowName__r ? rule.WindowName__r.Name : '';
                            
                            if(rule.Start_Date__c) {
                                endDateRules.dateType = 'Start Date';
                                
                                endDateRules.basedOnDate = endDateRules.media + ' - ' + endDateRules.windowName + ' / ' + endDateRules.dateType;
                            } else if(rule.End_Date__c) {
                                endDateRules.dateType = 'End Date';
                                
                                endDateRules.basedOnDate = endDateRules.media + ' - ' + endDateRules.windowName + ' / ' + endDateRules.dateType;
                            } else {
                                endDateRules.basedOnDate = endDateRules.media + ' / ' + endDateRules.territory;
                            }
                        }
                    }

                    component.set('v.startDateRules', startDateRules);
                    component.set('v.endDateRules', endDateRules);
                }
            } else {
                helper.errorToast('There was an issue collecting this records rules.')
            }
        });
        $A.enqueueAction(action);
    },

    saveRules : function (component, event, helper) {
        let sObjectType = component.get('v.sObjectType');
        let startDateRules = component.get('v.startDateRules');
        let endDateRules = component.get('v.endDateRules');
        let recordId = component.get('v.recordId');
        let action = component.get('c.setRules');
        
        console.log(startDateRules.dateType)
        console.log(endDateRules.dateType)

        action.setParams({
            id: recordId,
            sObjectType: sObjectType,
            startRuleId: startDateRules.id,
            startMonths: startDateRules.months,
            startDays: startDateRules.days,
            startTerritory: startDateRules.territory,
            startMedia: startDateRules.media,
            startWindowName: startDateRules.windowName,
            startDateType: startDateRules.dateType,
            endRuleId: endDateRules.id,
            endMonths: endDateRules.months,
            endDays: endDateRules.days,
            endTerritory: endDateRules.territory,
            endMedia: endDateRules.media,
            endWindowName: endDateRules.windowName,
            endDateType: endDateRules.dateType
        });

        action.setCallback(this, function(response) {
            let state = response.getState();

            if(state === 'SUCCESS') {
                console.log(response.getReturnValue());

                helper.successToast('The rules were saved.')
            } else {
                helper.errorToast('There was an issue saving the rules you have created / updated.')
            }
        });
        $A.enqueueAction(action);
    }
})