({
    createRecord : function (component, event, helper) {
        
        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            "entityApiName": "EAW_Plan_Guideline__c"
        });
	    createRecordEvent.fire();
    },
    
	getFieldSet: function(component, event, helper) {
        helper.fieldSet(component);
    },
    
    searchData : function(component, event, helper) {
        var checked = component.get("v.checked");
        var childCheckRecords = component.find("planGuideLine").get("v.checkSaveRecords");
        //LRCC-1459
        let selectedFilterMap = null;
        if(event.getParam('selectedFilterMap') && event.getParam('selectedFilterMap').selectedCol != '' && event.getParam('selectedFilterMap').selectedAlpha != ''){
            selectedFilterMap = JSON.stringify(event.getParam('selectedFilterMap'));
        }
        
        if(!event.getParam('isDeleted') && checked == true && childCheckRecords == false) {
            component.find("modalCmp").open();
        }
        else {
        	//helper.search(component, event, helper);
            console.log('Plan:::Guide::window:::::::',event.getParam('currentPageNumber'),event.getParam('pagesPerRecord'),JSON.stringify(component.get("v.pageIdMap")));
        
        	//LRCC-1459
            if(event.getParam('currentPageNumber') && event.getParam('pagesPerRecord')){
                
                helper.search(component, event, helper,event.getParam('pagesPerRecord'),event.getParam('currentPageNumber'),JSON.stringify(component.get("v.pageIdMap")),selectedFilterMap);
            } else {
                //LRCC-1795
                helper.search(component, event, helper,50,1,JSON.stringify({}));
            }
        }       
    },
    closeModal: function(component, event, helper) {
    	component.find("modalCmp").close();
        component.set('v.checked', true);
        var editTableEvent = $A.get("e.c:EAW_EditTableEvent");
        editTableEvent.setParams({"editMode":true});
        editTableEvent.fire();
       
	},
    saveModal : function(component, event, helper) {
        component.find("modalCmp").close();
        component.set('v.checked', false);        
        component.set("v.myData", JSON.parse(JSON.stringify(component.get("v.myDataOriginal"))));
        var editTableEvent = $A.get("e.c:EAW_EditTableEvent");
        editTableEvent.setParams({"editMode":false});
        editTableEvent.fire();
       
	},
	
    clearSearch: function(component, event, helper){
        helper.clearSearch(component);
    },
    
    /** LRCC:1045 **/
    itemsChange: function(component, event, helper) {
        helper.autoSelectTerritory(component, event, helper);
    },
    //LRCC-1675
    exportSearchResult: function(component, event, helper) {
        console.log('export::plan');
        helper.exportPlanGuideline(component, event, helper);
    },
})