({
    search : function(component, event, helper,pagesPerRecord,currentPageNumber,pageIdMap,selectedFilterMap) {
        
        try {
            var headerList = [];
    		var valueList = [];
            var mediaList = [];
    		var territoryList = [];
            var languageList = [];
            var productList = [];
            var salesRegionList = [];
            var planNamesList = [];
						
            var planNames = component.get('v.selectedPlanGuidelines');
            	planNamesList = component.get('v.selectedPlanGuidelines');
            
            var status = component.find('status') ? component.find('status').get('v.value') : '';
            	headerList.push('Status');
	    		valueList.push([status]);
            
            var releaseType = component.find('releaseType') ? component.find('releaseType').get('v.value') : '';
            	headerList.push('Release Type');
	    		valueList.push([releaseType]);
            
            var salesRegions =  component.get('v.selectedSalesRegions');
            	salesRegionList = component.get('v.selectedSalesRegions');
            
            var medias = component.get('v.selectedMedias');
            	mediaList = component.get('v.selectedMedias');	
            
            var territories = component.get('v.selectedTerritories');
            	territoryList = component.get('v.selectedTerritories');	
            
            var languages = component.get('v.selectedLanguages');
            	languageList = component.get('v.selectedLanguages');	
            
            var productTypes = component.get('v.selectedProductTypes');
            	productList = component.get('v.selectedProductTypes');	
            
            if(planNames.length > 0 || medias.length > 0 || territories.length > 0 || languages.length > 0 || productTypes.length > 0 ||
                status || releaseType || salesRegions.length > 0) {
                
                component.set('v.searchCriteriaString',helper.setSearchCriteriaString(helper, headerList, valueList, mediaList, territoryList, languageList, productList, salesRegionList, planNamesList));	
				
                var action = component.get("c.searchPlanByCriteria");
                action.setParams({
                	"objFieldSetMap": {
	                    "EAW_Plan_Guideline__c": "Plan_Guideline_Name"
	                },
                    'status': status,
                    'releaseType': releaseType,
                    'planNames': this.collectIds(planNames), //LRCC-1280 
                    'salesRegions': this.collectNames(salesRegions),
                    'territoryNames': this.collectNames(territories),
                    'languageNames': this.collectNames(languages),
                    'productTypeNames': this.collectNames(productTypes),
                    'mediaNames': this.collectNames(medias),
                    'pagesPerRecord' : pagesPerRecord,
                    'currentPageNumber':currentPageNumber,
                    'pageIdMapUI':pageIdMap,
                    'selectedFilterMap':selectedFilterMap,
                    'exportFlag':null
                });
				
                this.showSpinner(component);
				
                component.set('v.planGuidelineSearchParam',JSON.parse(JSON.stringify(action.getParams())));
                
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    var results= response.getReturnValue();
                    console.log('results::',results)
                    
                	if(state === 'SUCCESS') {
                        //LRCC-1459
                        console.log('results:::::::test:::',results);
                        if(results.totalCount && results.totalCount[0].expr0){
                            
                            console.log('results::::results.totalCount[0].expr0:::test:::',results.totalCount[0].expr0);    
                            component.set('v.totalRecordCount', results.totalCount[0].expr0);    
                        }else {
                            component.set('v.totalRecordCount',0);
                        }
                        component.set('v.initialTableShow',true);
                        console.log('results::::initialTableShow:::test:::',component.get('v.initialTableShow'));
                        
                        if(results && results.Plan[0]) {
                            this.addWindowNames(component,helper, results.Plan[0],false);
                            
                        } else {
                            component.set('v.myData', []);
                            this.hideSpinner(component);
                        }
                    } else if(state === 'ERROR') {
                        component.set('v.myData', []);
                        this.hideSpinner(component);
                    }
                });
                $A.enqueueAction(action);
            } else {
                //LRCC-1012 - commented for this ticket
                //component.set('v.myData', []);
                helper.errorToast('At least one criteria must be provided before a search is performed.');
    		}
		} catch(e) {
            console.log(e);
        }
    },
    setSearchCriteriaString : function(helper, headerList, valueList, mediaList, territoryList, languageList, productList, salesRegionList, planNamesList) {
    	
    	let searchCriteriaString = '',
    		columnDivider = ',',
    		lineDivider =  '\n';
    		
        searchCriteriaString += 'Plan Guidelines Results' +  lineDivider +  lineDivider;
    	
		for(var i=0;i<headerList.length;i++) {
			
            var temp = valueList[i];
			
			if(!$A.util.isEmpty(temp[0])){
				if(!searchCriteriaString.includes('Search Criteria')){
					searchCriteriaString += 'Search Criteria';
					searchCriteriaString += lineDivider;
				}
				searchCriteriaString += headerList[i];
				searchCriteriaString += columnDivider;
				for(var j=0;j<temp.length;j++){
					if(headerList[i].endsWith('Date') && temp[j]){
						console.log(temp[j]);
						var resultData = helper.getFormattedDate(temp[j]);
					} else{
						var resultData = temp[j];
					}
					searchCriteriaString += resultData + ';' || '';
				}
				searchCriteriaString += lineDivider;
			}
        }
		
        if(mediaList.length) {
			searchCriteriaString += lineDivider;
			searchCriteriaString += 'Media Criteria';
			searchCriteriaString += lineDivider;
			searchCriteriaString += 'Media(s)' +  columnDivider;
			for(var i=0;i<mediaList.length;i++) {
                searchCriteriaString += mediaList[i].title.replace(',','');	
				searchCriteriaString += ';';			
			}		
        }
        
		if(territoryList.length) {
			searchCriteriaString += lineDivider;
			searchCriteriaString += 'Territory Criteria';
			searchCriteriaString += lineDivider;
			searchCriteriaString += 'Territories(s)' +  columnDivider;
			for(var i=0;i<territoryList.length;i++) {
                searchCriteriaString += territoryList[i].title.replace(',','');	
				searchCriteriaString += ';';			
			}		
        }
        
        if(languageList.length) {
			searchCriteriaString += lineDivider;
			searchCriteriaString += 'Language Criteria';
			searchCriteriaString += lineDivider;
			searchCriteriaString += 'Languages(s)' +  columnDivider;
			for(var i=0;i<languageList.length;i++) {
                searchCriteriaString += languageList[i].title.replace(',','');	
				searchCriteriaString += ';';			
			}		
        }
        
        if(productList.length){
            searchCriteriaString += lineDivider;
			searchCriteriaString += 'Product Type Criteria';
			searchCriteriaString += lineDivider;
			searchCriteriaString += 'Product Type(s)' +  columnDivider;
			for(var i=0;i<productList.length;i++) {
                searchCriteriaString += productList[i].title.replace(',','');	
				searchCriteriaString += ';';			
			}
        }
        
        if(salesRegionList.length){
            searchCriteriaString += lineDivider;
			searchCriteriaString += 'Sales Region Criteria';
			searchCriteriaString += lineDivider;
			searchCriteriaString += 'Sales Region(s)' +  columnDivider;
			for(var i=0;i<salesRegionList.length;i++) {
				console.log('salesReg::',salesRegionList[i]);
                searchCriteriaString += salesRegionList[i].title.replace(',','');	
				searchCriteriaString += ';';			
			}
        }
        
        if(planNamesList.length) {
            searchCriteriaString += lineDivider;
			searchCriteriaString += 'Plan Guideline Name Criteria';
			searchCriteriaString += lineDivider;
			searchCriteriaString += 'Plan Guideline Name (s)' +  columnDivider;
			for(var i=0;i<planNamesList.length;i++) {
				searchCriteriaString += planNamesList[i].title.replace(',','');	
				searchCriteriaString += ';';			
			}
        }
        
        searchCriteriaString += lineDivider;
		searchCriteriaString += lineDivider;
        return searchCriteriaString;
    },
    getFormattedDate : function(dateToFormat) {
    	/*var mydate = new Date(dateToFormat);
		var month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
		"Jul", "Aug", "Sept", "Oct", "Nov", "Dec"][mydate.getMonth()];
		return ' '+mydate.getDate() + ' - ' + month + ' - ' +  mydate.getFullYear()+' ';*/
        //LRCC-1675
        var mydate = dateToFormat.split('-');
        var month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                     "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"][mydate[1]-1];
        //LRCC-1691
        
        return ' '+mydate[2] + '-' + month + '-' +  mydate[0]+' ';//.trim();
    },
    addWindowNames : function(component,helper,planGuidelines,exportflag,allExportRecords,lastBatchIds,lastRecordCreatedDate,recordLimit,numberOfLoop,index){
        
        console.log('addWindowNames:::::',planGuidelines);
        var planGuidelineIds = [];
        for(var i = 0; i < planGuidelines.length ; i++) {
            
            var planGuideline = planGuidelines[i];
            planGuidelineIds.push(planGuideline.Id);
            if(exportflag){
                //LRCC-1675
                if(planGuidelines.length - 1 == i){
                    
                    lastRecordCreatedDate = planGuideline.CreatedDate ;
                    
                    console.log('lastRecordCreatedDate::afetr:',lastRecordCreatedDate);
                }
                allExportRecords.push(planGuideline);
                lastBatchIds.push(planGuideline.Id);
            }
        }	
        
        if(planGuidelineIds.length > 0) {
            
            var action = component.get("c.getRelatedWindowGuidelines");
            action.setParams({"planGuidelineIds": planGuidelineIds});
            
            action.setCallback(this, function(response) {
                
                var state = response.getState();
                var results = response.getReturnValue();
                 console.log('exportflag:::results:windows:',results);
                console.log('exportflag:::&&:windows:',state === 'SUCCESS' && results && results.length > 0);
                if(state === 'SUCCESS' && results && results.length > 0) {
                    
                    for(var planGuideline of planGuidelines) {
                        
                        var filteredResults = results.filter(result => result.Plan__c == planGuideline.Id);
                        
                        if(filteredResults.length > 0) {
                            var resultNames = '';
                            var duplicateCheck = [];
                            for(var filteredResult of filteredResults) {
                                
                                if(duplicateCheck.indexOf(filteredResult.Window_Guideline__c) == -1) {
                                    if(resultNames) {
                                        resultNames += ', ' + filteredResult.Window_Guideline__r.Name;
                                    } else {
                                        resultNames += filteredResult.Window_Guideline__r.Name;
                                    }
                                    duplicateCheck.push(filteredResult.Window_Guideline__c);
                                }
                            }
                            planGuideline.windowName = resultNames;
                        }
                    }
                    //LRCC-1675
                    console.log('exportflag:::::',exportflag);
                    if(exportflag){
                        
                        if(allExportRecords.length || planGuidelines.length){
 
                            
                            console.log('allExportRecords::afetr:',allExportRecords);
                            var tagArray = planGuidelines.map(x => x.Id);
                            console.log('numberOfLoop > index::afetr:',numberOfLoop , index);
                            console.log(':::lastBatchIds::::'+lastBatchIds);
                            if(numberOfLoop > index){
                                
                                index = index+1;
                                helper.recursivePlanGuidelineExportAction(component, event, helper,allExportRecords,lastBatchIds,lastRecordCreatedDate,recordLimit,numberOfLoop,index);
                                
                            }else{
                                
                                let data = allExportRecords;
                                console.log(data); 
                                console.log('data length:::',data.length); 
                                if(data == null) {
                                    return;
                                }
                                let csv = helper.convertArrayToCSV(component, data,helper);
                                console.log('csv:::::',csv);
                                let hiddenElement = document.createElement('a');
                                hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
                                hiddenElement.target = '_self'; //
                                hiddenElement.download = 'PlanGuidelines_Results.csv';  // CSV file Name* you can change it.[only name not .csv]
                                document.body.appendChild(hiddenElement); // Required for FireFox browser
                                hiddenElement.click();
                                this.hideSpinner(component);
                                
                                helper.hideSpinner(component);
                                return allExportRecords;
                            }
                        }
                        /*let data = planGuidelines;
                        if(data == null) {
                            return;
                        }
                        let csv = helper.convertArrayToCSV(component, data,helper);
                        console.log('csv:::::',csv);
                        let hiddenElement = document.createElement('a');
                        hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
                        hiddenElement.target = '_self'; //
                        hiddenElement.download = 'PlanGuidelines_Results.csv';  // CSV file Name* you can change it.[only name not .csv]
                        document.body.appendChild(hiddenElement); // Required for FireFox browser
                        hiddenElement.click();
                        this.hideSpinner(component);*/
                    }else{
                        
                        component.set('v.myData', planGuidelines);
                        component.set("v.myDataOriginal", JSON.parse(JSON.stringify(component.get("v.myData"))));
                        this.hideSpinner(component);
                        //LRCC-1459
                        if (component.find('planGuideLine')) {
                            component.find('planGuideLine').reConstruct();
                        }
                    }
                    
                } else if(state === 'ERROR') {
                    
                    console.log(response.getError());
                    this.hideSpinner(component);
                } else {
                	console.log('exportflag:::erre:windows:',exportflag);
                    if(exportflag){
                        
                        if(allExportRecords.length || planGuidelines.length){
 
                            
                            console.log('allExportRecords::afetr:',allExportRecords);
                            var tagArray = planGuidelines.map(x => x.Id);
                            
                            console.log('numberOfLoop > index::afetr:',numberOfLoop , index);
                            console.log(':::lastBatchIds::::'+lastBatchIds);
                            if(numberOfLoop > index){
                                
                                index = index+1;
                                helper.recursivePlanGuidelineExportAction(component, event, helper,allExportRecords,lastBatchIds,lastRecordCreatedDate,recordLimit,numberOfLoop,index);
                                
                            }else{
                                
                                let data = allExportRecords;
                                console.log(data); 
                                console.log('data length:::',data.length); 
                                if(data == null) {
                                    return;
                                }
                                let csv = helper.convertArrayToCSV(component, data,helper);
                                console.log('csv:::::',csv);
                                let hiddenElement = document.createElement('a');
                                hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
                                hiddenElement.target = '_self'; //
                                hiddenElement.download = 'PlanGuidelines_Results.csv';  // CSV file Name* you can change it.[only name not .csv]
                                document.body.appendChild(hiddenElement); // Required for FireFox browser
                                hiddenElement.click();
                                this.hideSpinner(component);
                                
                                helper.hideSpinner(component);
                                return allExportRecords;
                            }
                        }
                        /*console.log('exportflag::planGuideline:',planGuidelines);
                        let data = planGuidelines;
                        if(data == null) {
                            return;
                        }
                        let csv = helper.convertArrayToCSV(component, data,helper);
                        console.log('csv:::::',csv);
                        let hiddenElement = document.createElement('a');
                        hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
                        hiddenElement.target = '_self'; //
                        hiddenElement.download = 'PlanGuidelines_Results.csv';  // CSV file Name* you can change it.[only name not .csv]
                        document.body.appendChild(hiddenElement); // Required for FireFox browser
                        hiddenElement.click();
                        this.hideSpinner(component);*/
                    }else{
                        component.set('v.myData', planGuidelines);
                        component.set("v.myDataOriginal", JSON.parse(JSON.stringify(component.get("v.myData"))));
                        this.hideSpinner(component);
                        //LRCC-1459
                        if (component.find('planGuideLine')) {
                            component.find('planGuideLine').reConstruct();
                        }
                    }
                }
            });
            $A.enqueueAction(action);
        } else {
        
            component.set('v.myData', planGuidelines);
            component.set("v.myDataOriginal", JSON.parse(JSON.stringify(component.get("v.myData"))));
        	this.hideSpinner(component);
        	//LRCC-1459
        	if (component.find('planGuideLine')) {
                component.find('planGuideLine').reConstruct();
            }
        }
		return planGuidelines;
    },
    
    //This is method currently not valid. Currently PG field set don't have any reference fields.
    getLookupFields: function (col, data, fName) {
        for (var i = 0, clen = col.length; i < clen; i++) {
            if (col[i]['type'] == 'reference') {
                for (var j = 0, rlen = data.length; j < rlen; j++) {
                    //var str = (col[i]['fieldName']).replace('__c', '__r');
                    var str = col[i]['fieldName'];
                    
                    if( str.indexOf('__c') != -1 ){
                    	str =  str.replace('__c', '__r');
                    } else{
                    	str =  str.replace('Id', '');
                    }
                    
                    var fValue = data[j][str];
                    if (fValue != undefined) {
                        data[j][col[i]['fieldName']] = fValue[fName];
                    }
                }
            } 
        }
        return data;
    },
    
    clearSearch: function(component) {
        
        if(component.find('status')) {
        	component.find('status').set('v.value', '');
        }
        if(component.find('releaseType')) {
            component.find('releaseType').set('v.value', '');
        }
        component.set('v.selectedMedias', []);
        component.set('v.selectedPlanGuidelines', []);
        component.set('v.selectedTerritories', []);
        component.set('v.selectedLanguages', []);
        component.set('v.selectedProductTypes', []);
        component.set('v.selectedSalesRegions',[]);
    },
    
    fieldSet: function (component) {
        try {
            var strObjectName = component.get("v.strObjectName");
            var strFieldSetName = component.get("v.strFieldSetName");
            var strObjFieldSetMap = new Map();
			
		    strObjFieldSetMap.set(strObjectName, strFieldSetName);
		    
		    console.log('Field Set Map:');
            console.dir(strObjFieldSetMap);
			
            var action = component.get("c.getFields");
            action.setParams(
	            {
	                "objFieldSetMap": {
	                    "EAW_Plan_Guideline__c": "Plan_Guideline_Name"
	                }
	            }
	        );
			
            action.setCallback(this, function (response) {
                var state = response.getState();
                
                if (state === 'SUCCESS') 
                {
                    var columns = new Array();
                    console.dir(response.getReturnValue());
                    for(let col of response.getReturnValue()) {
                        for(let colName of col) {
                            if(colName.fieldName === 'Name') {
                                colName.width = '199';
                    			columns.push(colName);        
                        	}
                        }
                    }
                    columns.push({fieldName: "Status__c", label: "Plan Guideline Status", type: "picklist", updateable: false});
                    columns.push({fieldName: "Version__c", label: "PG Version", type: "double", updateable: false});
                    columns.push({fieldName: "Product_Type__c", label: "Product Type", type: "multipicklist", updateable: false});
                    columns.push({fieldName: "Sales_Region__c", label: "Sales Region", type: "multipicklist", updateable: false});
                    columns.push({fieldName: "All_Rules__c", label: "All Rules", type: "string", updateable: false});
                    //columns.push({fieldName: "Release_Type__c", label: "Release Type", type: "picklist", updateable: false});
                    columns.push({fieldName: "windowName", label: "Window Guideline Name", type: "string", updateable: false});
                    console.log('columns:',columns);
                    component.set('v.myColumns', columns);
                } 
                else if (state === 'ERROR') {
                    var errors = response.getError();

                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " +
                                errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                } else {
                    console.log('Something went wrong, Please check with your admin');
                }
            });
            $A.enqueueAction(action);
        } catch (e) {
            console.log('Exception:' + e);
        }
    },
    
    collectNames : function(selectedResults) {
        var collectedNames = [];
		
        if(selectedResults && selectedResults.length > 0) {
            for(var result of selectedResults) {
                collectedNames.push(result.title);
            }
        }
        return collectedNames;
    },
	
	//LRCC-1280
	collectIds : function(selectedResults) {
        var collectedNames = [];
		
        if(selectedResults && selectedResults.length > 0) {
            for(var result of selectedResults) {
                collectedNames.push(result.id);
            }
        }
        return collectedNames;
    },
    
    showSpinner : function(component) {
        component.set('v.showSpinner', true);
    },

    hideSpinner : function(component) {
        component.set('v.showSpinner', false);
    },
    errorToast : function(message) {
    	var toastEvent = $A.get('e.force:showToast');
    		
		toastEvent.setParams({
			'title': 'Error!',
			'type': 'error',
			'message': message
		});
		toastEvent.fire();
    },

    /** LRCC:1045 **/
    autoSelectTerritory : function(component, event, helper) {
        
        console.log('v.selectedSalesRegions:::cc:',component.get('v.selectedSalesRegions'));
        console.log('v.alreadySelectedTerritory:::cc:',component.get('v.alreadySelectedTerritory'));
        var temp ;
        
        var action = component.get("c.autoSelected");
        if(component.get('v.selectedSalesRegions').length > 0 ){
            
            for(let i = 0;i < component.get('v.selectedSalesRegions').length;i++){
                if(!temp){
                    
                    temp = component.get('v.selectedSalesRegions')[i].title;
                }else {
                    temp =temp+','+ component.get('v.selectedSalesRegions')[i].title;
                }
            }
        } else {
            temp = '';
        }
        action.setParams({"selectedSalesRegionStr" :temp});
        action.setCallback(this, function(response) {
            let results = response.getReturnValue();
            
            if(results != null) {
                
                var selectedTerritories = component.get('v.selectedTerritories');
                var choosedOnlyInTerritoryNames = [];
                var alreadySelectedTerritory = component.get('v.alreadySelectedTerritory');
                
                var tempSelectedTerritories = selectedTerritories;
                
                for(let i=0 ; i < selectedTerritories.length ; i++){
                    for(let j=0 ; j < alreadySelectedTerritory.length ; j++){
                        if( selectedTerritories[i] && alreadySelectedTerritory[j] && alreadySelectedTerritory[j].title == selectedTerritories[i].title){
                            tempSelectedTerritories.splice(i,1);
                        }
                    }
                    
                }
                
                for(let i=0 ; i<results.length ; i++){
                    selectedTerritories.push(results[i]);
                }
                component.set('v.selectedTerritories',selectedTerritories);
                component.set('v.alreadySelectedTerritory',results);
            } 
        });
        $A.enqueueAction(action);
    },
    //LRCC-1675
    exportPlanGuideline : function(component, event, helper) {
        console.log('plan::::: export:');
        var totalRecordCount = component.get('v.totalRecordCount') > 32000 ? 32000 : component.get('v.totalRecordCount');
        //var totalRecordCount = component.get('v.totalRecordCount');
        console.log('totalRecordCount:::',totalRecordCount);
        var recordLimit = 2000;
        
        var numberOfLoop = Math.floor((totalRecordCount+recordLimit - 1 )/recordLimit);
        console.log('numberOfLoop:::',numberOfLoop);
        var allExportRecords = [];
        var lastBatchIds = [];
        var lastRecordCreatedDate ;
        var lastRecordId;
        var i = 1;
        var finalresult = helper.recursivePlanGuidelineExportAction(component, event, helper,allExportRecords,lastBatchIds,lastRecordCreatedDate,recordLimit,numberOfLoop,i,lastRecordId);
        console.log('return final ::releasedate:::',finalresult);
    },
    recursivePlanGuidelineExportAction :  function(component, event, helper,allExportRecords,lastBatchIds,lastRecordCreatedDate,recordLimit,numberOfLoop,index) {
        
        try {
            var planGuidelineSearchParam = component.get('v.planGuidelineSearchParam');
            planGuidelineSearchParam.exportFlag = 'export';
            planGuidelineSearchParam.lastRecordCreatedDate = lastRecordCreatedDate;
            planGuidelineSearchParam.lastBatchIds = lastBatchIds;
            planGuidelineSearchParam.recordLimit = recordLimit;
            
            var action = component.get("c.searchPlanByCriteria");
            action.setParams(planGuidelineSearchParam);
            
            this.showSpinner(component);
            
            action.setCallback(this, function(response) {
                var state = response.getState();
                var results= response.getReturnValue();
                console.log('results: export:',results)
                
                if(state === 'SUCCESS') {
                    console.log('SUCCESS: export:',results)
                    console.log('results && results.Plan[0]):', results.Plan[0])
                    if(results && results.Plan[0]) {
                        
                        helper.addWindowNames(component,helper, results.Plan[0],true,allExportRecords,lastBatchIds,lastRecordCreatedDate,recordLimit,numberOfLoop,index);
                    } else {
                        
                        this.hideSpinner(component);
                    }
                } else if(state === 'ERROR') {
                    
                    this.hideSpinner(component);
                }
            });
            $A.enqueueAction(action);
            
        } catch(e) {
            console.log(e);
        }
    },
    convertArrayToCSV : function (component, data,helper) {
        
        // declare variables
        var csvStringResult, counter, header, keys, columnDivider, lineDivider;
        
        // check if 'objectRecords' parameter is null, then return from function
        if(data == null || !data.length) {
            return null;
        }
        
        // store ,[comma] in columnDivider variabel for sparate CSV values and
        // for start next line use '\n' [new line] in lineDivider varaible
        columnDivider = ',';
        lineDivider =  '\n';
        
        // in the keys variable store fields API Names as a key
        // this labels use in CSV file header
        var columns = component.get('v.myColumns');
        header = [];
        keys =[];
        for(var i=0;i<columns.length;i++){
            header.push(columns[i].label);// this array is the 'pretty' column names for the csv
            keys.push(columns[i].fieldName);// this array is the actual column name
        }
        
        csvStringResult = component.get('v.searchCriteriaString') || '';
        csvStringResult += header.join(columnDivider);
        csvStringResult += lineDivider;
        
        for(let i=0; i < data.length; i++) {
            counter = 0;
            
            for(var sTempkey in keys) {
                
                var skey = keys[sTempkey];
                
                // add , [comma] after every String value (except first)
                if(counter > 0){
                    csvStringResult += columnDivider;
                }
                if(columns[sTempkey].type == 'date' && !$A.util.isEmpty(data[i][skey])){
                    
                    var resultData = helper.getFormattedDate(data[i][skey]);
                } else{
                    var resultData = data[i][skey] || '';
                }
                
                csvStringResult += '"'+ resultData +'"';
                
                counter++;
            }
            csvStringResult += lineDivider;
        }
        
        // return the CSV format String
        return csvStringResult;
    },
})