<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Populate_Plan_Name</fullName>
        <description>Populate plan name based on Title Name - Plan Guideline Name</description>
        <field>Name</field>
        <formula>EAW_Title__r.Name &amp; &quot;/&quot; &amp; Plan_Guideline__r.Name</formula>
        <name>Populate Plan Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Auto Populate Plan Name</fullName>
        <actions>
            <name>Populate_Plan_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Plan Name populates based on Title Name - Plan Guideline Name</description>
        <formula>AND(NOT(ISBLANK(Plan_Guideline__c)),NOT(ISBLANK( EAW_Title__c )))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
