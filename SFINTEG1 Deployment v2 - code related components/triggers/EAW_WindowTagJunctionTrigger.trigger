trigger EAW_WindowTagJunctionTrigger on EAW_Window_Tag_Junction__c (before insert, before update, before delete, after insert, after update, after delete) {
     
     EAW_WindowTagJunctionTriggerHandler handler = new EAW_WindowTagJunctionTriggerHandler();
     
     //LRCC-1787
     if( Trigger.isBefore ) {
    
        if( trigger.isInsert) {
            handler.updateOldTags(trigger.new);
        } else if( trigger.isUpdate ){
            handler.updateOldTags(trigger.old);
        } else if( trigger.isDelete ) {
            handler.updateOldTags(trigger.old);
        }
        
     } else if( Trigger.isAfter ) {
         
         if( trigger.isInsert ){
             handler.reQualificationOfRules(trigger.new);
         } else if( trigger.isUpdate ){
             handler.reQualificationOfRules(trigger.new, trigger.old);
         } else if( trigger.isDelete ){
             handler.reQualificationOfRules(trigger.old);
         }
     }
}