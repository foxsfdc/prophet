trigger EAW_Release_Date_Tag_Junction_Trigger on EAW_Release_Date_Tag_Junction__c (before insert, before update, before delete, after insert, after update, after delete ) {
    
    EAW_ReleaseDateTagJunction_Handler handler = new EAW_ReleaseDateTagJunction_Handler();
    //LRCC-1787
    if( Trigger.isBefore ) {
    
        if( trigger.isInsert) {
            handler.updateOldTags(trigger.new);
        } else if( trigger.isUpdate ){
            handler.updateOldTags(trigger.old);
        } else if( trigger.isDelete ) {
            handler.updateOldTags(trigger.old);
        }
    
    } else if( Trigger.isAfter ) {
    
        if( Trigger.isInsert ){
            handler.reQualificationOfRules(trigger.new);
        } else if ( Trigger.isUpdate ){
            handler.reQualificationOfRules(trigger.old, trigger.new);
        } else if( Trigger.isDelete ){
            handler.reQualificationOfRules(trigger.old);
        }
    
    } 

}