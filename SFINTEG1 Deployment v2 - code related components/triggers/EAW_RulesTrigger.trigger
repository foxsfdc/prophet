trigger EAW_RulesTrigger on EAW_Rule__c (after delete, after insert, after undelete, after update, before delete, before insert, before update)
{
    EAW_RulesHandler rHdlr = new EAW_RulesHandler();
    if(Trigger.isBefore)
    {
        //rHdlr.applyRules(Trigger.new); 
    }  
    
    if( Trigger.isAfter ){
        if( Trigger.isDelete ){
            rHdlr.handleRuleRecordDelete(trigger.old);
        }
    }
}