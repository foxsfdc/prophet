trigger EAW_PlanGuidelineTrigger on EAW_Plan_Guideline__c (before insert, before update, before delete, after update, after insert) {

    EAW_PlanGuidelineTrigger_Handler handler = new EAW_PlanGuidelineTrigger_Handler();

    if(trigger.isBefore) {
        
        // Code below is executed BEFORE a Plan is updated/Changed

        if(trigger.isDelete) {
            //LRCC-716
            handler.preventDeletionofPG(trigger.old);
        } else if(trigger.isUpdate) {
            handler.ensureValidationIsNotBypassed(trigger.old, trigger.new);
            handler.validateStatusUpdateToActive(trigger.new, trigger.old);
            handler.preventChangesWhenActiveStatus(trigger.new, trigger.old, trigger.oldMap); //LRCC-1338 
            handler.cloneRuleDetailsToNewVersion(trigger.old, trigger.new); // LRCC - 1357
            handler.validateStatusUpdateToInactive(trigger.oldMap, trigger.new); //LRCC-1745
            
        } else if(trigger.isInsert){
            handler.validatePlanGuidelineStatus(trigger.new);
        }
        
    } else if( Trigger.isAfter ) {
        if(trigger.isInsert) {
            handler.cloneRuleDetailsToNewVersion( trigger.new); // LRCC - 1357
         }
         else if(trigger.isUpdate) { 
            handler.updateAssociatedPlans(trigger.old, trigger.new);
            handler.planReQualificationCheckForActivePG(trigger.old, trigger.new);
            //handler.createEAWWindows(trigger.old, trigger.new);
         }
    } 
}