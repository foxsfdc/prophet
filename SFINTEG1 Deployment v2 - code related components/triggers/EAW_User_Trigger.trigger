trigger EAW_User_Trigger on User (after insert) {
    
    EAW_User_Handler handler = new EAW_User_Handler();
    if(trigger.isafter && trigger.isInsert) {
        handler.addNewUserToGroup(Trigger.new);
    }
}