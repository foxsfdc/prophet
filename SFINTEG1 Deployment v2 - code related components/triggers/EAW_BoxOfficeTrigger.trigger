trigger EAW_BoxOfficeTrigger on EAW_Box_Office__c (after insert, after update) {
    
    EAW_BoxOfficeHandler tvdHanlder = new EAW_BoxOfficeHandler();
    
    if(Trigger.isBefore) {
        
    } 
    else if(Trigger.isAfter) {
        
        if(Trigger.isInsert) {
            Set<Id> qualifiedTitleIdSet = tvdHanlder.processTVDBoxOfficeToQualifyTitleForInsert(Trigger.new, null);
            //tvdHanlder.sentNotifcationsToTPVReviewGroup(null, trigger.new); //LRCC-1048
        } 
        else if(Trigger.isUpdate) {
            tvdHanlder.processTVDBoxOfficeToQualifyTitleForUpdate(Trigger.new, Trigger.old);
            //tvdHanlder.sentNotifcationsToTPVReviewGroup(trigger.old, trigger.new); //LRCC-1048
        }
    }
}