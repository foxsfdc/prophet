trigger EAW_TVDBoxOfficeTrigger on TVD_Box_Office__c ( after Insert, after update ) {

    EAW_TVDBoxOfficeHandler tvdHanlder = new EAW_TVDBoxOfficeHandler();
    
    if( Trigger.isBefore ){
    
    } else if( Trigger.isAfter ){
        
        if( Trigger.isInsert ){
            Set<Id> qualifiedTitleIdSet = tvdHanlder.processTVDBoxOfficeToQualifyTitleForInsert(Trigger.new, null);
        } else if( Trigger.isUpdate ){
            tvdHanlder.processTVDBoxOfficeToQualifyTitleForUpdate(Trigger.new, Trigger.old);
        }
        
    }

}