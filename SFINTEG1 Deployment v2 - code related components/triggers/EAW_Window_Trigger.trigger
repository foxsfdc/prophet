trigger EAW_Window_Trigger on EAW_Window__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    
    EAW_Window_Handler reHandler = new EAW_Window_Handler();
    
    if(Trigger.isBefore) {
        
        if(Trigger.isInsert) {
            
            //LRCC-1601
            rehandler.populateDateValuesFromProjectedDates(trigger.new, NULL);
            //LRCC:623, LRCC-1271
            reHandler.populateWindowName(Trigger.new);           
            //LRCC-1236
            rehandler.populatecustomersValue(Trigger.new);
            
        } else if(trigger.isUpdate) {
            //LRCC-1838
            reHandler.populateRightsStatusOldValue(trigger.new, trigger.old);
            //LRCC-1601
            rehandler.populateDateValuesFromProjectedDates(trigger.new, trigger.old);
            //LRCC-1864
            reHandler.firmedWindowsProcess(Trigger.new, Trigger.oldMap);
            //LRCC:623, LRCC-1271
            reHandler.populateWindowName(Trigger.new);
            //LRCC-631
            reHandler.updateValuePreventRightsCheckRecursiveCallout(Trigger.new, Trigger.old); 
            reHandler.reQualificationOfRules(trigger.new, trigger.old);
             //LRCC-1787
            reHandler.trackLicenseInfoCodes(trigger.new, trigger.oldMap);
        }
    } 
    else if(trigger.isAfter) {
        
        if(trigger.isInsert) {
            reHandler.calculateDateForNewWindows(trigger.new);
           // reHandler.cloneRuleAndRuleDetailsFromWGL(Trigger.new, null);//LRCC-1009 // Commented for LRCC - 1259
            //LRCC-1009
            //LRCC-1653 - -In this ticket once passed QA it with remove the commented code.
            //reHandler.createWindowStrand(trigger.new);
            reHandler.sentNotificationToRReviewGroup(null, Trigger.new); //LRCC-1050
        } 
        else if(trigger.isUpdate) {
            //LRCC-631
            //reHandler.initiateRightsCheckCallout(trigger.new);
            reHandler.sentNotificationToRReviewGroup(Trigger.old, Trigger.new); //LRCC-1050
        }
    }
}