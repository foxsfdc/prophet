trigger EAW_ReleaseDateType_Trigger on EAW_Release_Date_Type__c (after delete, after insert, after undelete, after update, before delete, before insert, before update)
{
    EAW_ReleaseDateType_Handler rdHandler = new EAW_ReleaseDateType_Handler();
    
    if(Trigger.isBefore)
    {
        if(Trigger.isDelete)
        {
            rdHandler.preventDelete(trigger.old);     
        }
    }//LRCC-611
    else if(Trigger.isAfter) {
    
        if(Trigger.isInsert) {
        
           rdHandler.calculateAutoFirm(trigger.new);
            
        } else if(Trigger.isUpdate) {
        
           rdHandler.calculateAutoFirm(trigger.new);
        }
    }

}