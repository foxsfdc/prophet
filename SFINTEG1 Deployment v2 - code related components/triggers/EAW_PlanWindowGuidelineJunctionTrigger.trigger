trigger EAW_PlanWindowGuidelineJunctionTrigger on EAW_Plan_Window_Guideline_Junction__c (before insert, after Insert, before delete, after delete) {
    
    EAW_PlanWindowGuidelineJNTrigger_Handler handler = new EAW_PlanWindowGuidelineJNTrigger_Handler();
    
    if(trigger.isBefore) {
        if(trigger.isDelete) {
            //LRCC-510
            //handler.checkToRemoveJunction(trigger.old); //Move to after delete.
        }
    } else if(trigger.isAfter) {
        /*if( trigger.isInsert ){
            handler.createEAWWindows(trigger.New);
        }*/
        if(trigger.isDelete) { //Before delete will not getting parent child query '__r' so I have put logics in after trigger.
            //LRCC-510
            handler.checkToRemoveJunction(trigger.old);
        }
    }
}