trigger EAW_TitleTrigger on EAW_Title__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
        
    EAW_Title_Handler titleHandler = new EAW_Title_Handler();
    
    if(Trigger.isBefore) {
    
        if(Trigger.isDelete) {
            titleHandler.deleteRelatedReleaseDates(trigger.old); //LRCC-1187
        }
        else if(Trigger.isUpdate) {
            titleHandler.findTitleName(trigger.new); //LRCC-1567
        }
        else if(Trigger.isInsert) {
            titleHandler.findTitleName(trigger.new); //LRCC-1567
        }
    } 
    else if(Trigger.isAfter) {
            
        if(Trigger.isInsert) {
            titleHandler.processTitleToCheckForPlanQualification(trigger.new, null, false);
            titleHandler.sentNotificationTAReviewGroup(null, trigger.new); //LRCC-1048
        } 
        else if(Trigger.isUpdate) {
            titleHandler.filterTitleToCheckForPlanQualification(trigger.new, trigger.old);
            titleHandler.sentNotificationTAReviewGroup(trigger.old, trigger.new); //LRCC-1048
        }
    }
}