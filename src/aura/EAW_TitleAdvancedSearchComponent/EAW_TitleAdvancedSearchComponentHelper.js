({
	resetAdvancedTitleSearchModal : function(component) {
        //LRCC-1884
        //component.find('titleName').set('v.value', '');        
        component.set('v.selectedTitlesTypeahead', []);
        
        component.set('v.selectedDivisions', []);
        component.find('finlTitleId').set('v.value', '');
        
        //LRCC-1884
        component.find('initialReleaseDateFrom').set('v.value', '');
        component.find('initialReleaseDateTo').set('v.value', '');
        //component.set('v.dateFrom',[]);
        //component.set('v.dateTo',[]);
        
        component.find('usBoxOfficeFrom').set('v.value', '');
        component.find('usBoxOfficeTo').set('v.value', '');
        component.set('v.selectedProductTypes', []);
        component.set('v.selectedIntlProductTypes', []);
       	component.set('v.selectedGenres', []);
        //component.find('usAdmissions').set('v.value','');
        //component.find('usScreens').set('v.value','');
        component.set('v.selectedTitleTags',[]);        
        component.set('v.selectedTitles', []);
        
        component.find('directorName').set('v.value', '');
        component.find('franchise').set('v.value', '');
        component.find('talent').set('v.value', '');
	},
    
    //LRCC-1696
    getFormattedDate : function(dateToFormat){
        /*
        var mydate = new Date(dateToFormat);
        var month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                     "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"][mydate.getMonth()];
        return ' '+mydate.getDate() + '-' + month + '-' +  mydate.getFullYear()+' ';
        */
        //LRCC-1675
        var mydate = dateToFormat.split('-');
        var month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                     "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"][mydate[1]-1];
        return ' '+mydate[2] + '-' + month + '-' +  mydate[0]+' ';
    },

    collectNames : function(selectedResults) {
        var collectedNames = [];
        
        if(selectedResults && selectedResults.length > 0) {
            for(var result of selectedResults) {
                collectedNames.push(result.title);
            }
        }
        console.log('<<collectNames>>'+collectedNames);
        return collectedNames;
    },

    errorToast : function(message) {
        var toastEvent = $A.get('e.force:showToast');

        toastEvent.setParams({
            'title': 'Error!',
            'type': 'error',
            'message': message
        });

        toastEvent.fire();
    },

    successToast : function(message) {
        var toastEvent = $A.get('e.force:showToast');

        toastEvent.setParams({
            'title': 'Success!',
            'type': 'success',
            'message': message
        });

        toastEvent.fire();
    },
    //LRCC:1098
    duplicateFilter : function(component,result,selectedRec){
    
			var existingList = result;
			var selectedList = selectedRec;
			var fliteredList = [];
			var selectedListNames = [];
			
			for(let j = 0;j < selectedList.length; j++){
			
				selectedListNames.push(selectedList[j].Id)
			}
			
			for(let i =0 ;i < existingList.length;i++){
			  var element = existingList[i]; 
			  	
				if(selectedListNames.indexOf( existingList[i].Id) == -1){
					
					fliteredList.push(existingList[i]); 
				}
			}
			return fliteredList;
    }
})