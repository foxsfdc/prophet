({    
    clearSearch: function(component){
        component.find('tag').set('v.value','');
        component.find('tagType').set('v.value','');
        
        component.set('v.data',[]);
    },
    search : function(component) {
        try {
            //var obj = component.find('territory') ? component.find('territory').get('v.value') : '';
            
            var action=component.get("c.search");
            action.setParams({
                //"field": obj
            });
            this.showSpinner(component)
            action.setCallback(this,function(response){
                var state = response.getState();
                var result= response.getReturnValue();
                this.hideSpinner(component)
                if(state === 'SUCCESS'){
                    console.log(result);
                    if(result){
                        component.set('v.data',result);
                    }
                    else{
                        component.set('v.data',[]);
                    }
                }
                else if(state === 'ERROR'){
                    console.log(response.getError());   
                    component.set('v.data',[]);
                }
            });
            $A.enqueueAction(action);
        }catch(e){
            console.log(e);
        }
    },
    showSpinner:function(component){
        component.set('v.showSpinner',true);
    },
    hideSpinner:function(component){
        component.set('v.showSpinner',false);
    }
})