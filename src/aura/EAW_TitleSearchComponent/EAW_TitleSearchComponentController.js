({
	doInit : function(component, event, helper) {
        
        //Set column header names for data table
        component.set('v.titleColumns', [
            {label: 'Int\'l Title ID', fieldName: 'Title_Id', type: 'text', initialWidth: 150},             
            {label: 'Title', fieldName: 'Name__c', type: 'url',
            typeAttributes: { label: {fieldName: 'Title_Name'}, target: '_blank'}, initialWidth: 200},
            {label: 'Int\'l Product Type', fieldName: 'PROD_TYP_CD_INTL', type: 'text', initialWidth: 200},	
            {label: 'Product Type', fieldName: 'PROD_TYP_CD', type: 'text', initialWidth: 200},            
            {label: 'Division', fieldName: 'FIN_DIV_CD', type: 'text', initialWidth: 200},
            {label: 'Title Tag(s)', fieldName: 'DIR_NM__c', type: 'text', initialWidth: 200},	// Title Tags field temporarily held in Director Name
            {label: 'Release Date', fieldName: 'FRST_REL_DATE__c', type: 'text', initialWidth: 200}
        ]);
	},
    removeItems: function(component, event, helper) {

	    // find the items && remove them
	    let data = component.get('v.data');
    	let selectedItems = component.get('v.selectedItems');
    	for(let item of selectedItems) {
            
            console.log('item::::'+item.Id);
    	    let index = data.indexOf(item);
    	    if(index !== -1) {
                data.splice(index,1);
            }
        }

        // update the component w/ the pruned list
        component.set('v.data', data);
        
    },
    /** Methods below control Paste Title popup*/
    showPasteTitleModal : function(component, event, helper) {
        document.getElementById("EAW_TitlePasteSearchComponent").style.display = "block";
    },
    hidePasteTitleModal : function(component, event, helper){
        document.getElementById("EAW_TitlePasteSearchComponent").style.display = "none";
    },
    
    /** Methods below control Advanced Title Search popup*/
    showAdvancedTitleSearchModal : function(component, event, helper) {
        document.getElementById("EAW_TitleAdvancedSearchComponent").style.display = "block";
    },
    hideAdvancedTitleSearchModal : function(component, event, helper){
        document.getElementById("EAW_TitleAdvancedSearchComponent").style.display = "none";
    }
})