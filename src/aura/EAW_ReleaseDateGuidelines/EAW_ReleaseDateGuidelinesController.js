({
    searchData: function(component, event, helper) {
        var checked = component.get("v.checked");
        
        if(checked == true && component.get("v.checkSaveRecords") == false) {
            
            component.find("modalCmp").open();
        }         
        else if(component.get("v.selectedTerritories").length || 
                component.get("v.selectedLanguages").length || 
                component.get("v.selectedProductTypes").length || 
                component.get("v.selectedCustomers").length || 
                component.get("v.selectedReleaseDateTypes").length ) {            
            
            //LRCC-1459
            //helper.search(component,false);
            //LRCC-1795
             if(component.find('pagePerRecord')){
                
                component.set("v.pagesPerRecod",component.find('pagePerRecord').get("v.value"));
            }
            console.log('page per record :::',component.find('pagePerRecord'));
            helper.search(component,false,event, helper,component.get("v.pagesPerRecod"),1,JSON.stringify({}));
            component.set('v.editDisable',false);
            
        } else { 
            helper.showToast('error','At least one criteria must be provided before a search is performed');
            //LRCC-1012- commented code for this ticket
            //if(component.get('v.myData')){
                //component.set('v.myData', []);
            //}
        }
    },
    //LRCC-1633 method added
    checkHeaderCheckbox: function(component, event, helper) {
        
        var checkboxes = component.find("row-checkbox");
        var allselected = true;
        
        if(checkboxes){
            if(Array.isArray(checkboxes)) {
                for(let check of checkboxes){
                    var ele = check.getElement();
                    if(! ele.checked){
                        allselected = false;
                        break;
                    }
                }
            } else {
                var ele = checkboxes.getElement();
                if(! ele.checked){
                    allselected = false;
                }
            }
        }
        console.log('allselected:::',allselected);
        let headerCheckbox = component.find('header-checkbox');
        let headercheck = Array.isArray(headerCheckbox) ? headerCheckbox[0] : headerCheckbox;
        
        if(allselected) {
            headercheck.getElement().checked = true;
        } else {
            headercheck.getElement().checked = false;
        }
    },
    getFieldSet: function(component, event, helper) {
        helper.fieldSet(component);
        
        //LRCC-1459
        console.log('initialTableShow  ::::',component.get('v.initialTableShow'));
        if(component.get('v.initialTableShow')){
        
            helper.generateAlphabets(component, event, helper);
                //LRCC-1795
            if(component.find('pagePerRecord')){
                
                component.set("v.pagesPerRecod",component.find('pagePerRecord').get("v.value"));
            }
       }
    },
    clearSearch: function(component, event, helper) {
        component.set('v.selectedReleaseDateTypes', []);
        component.set('v.selectedLanguages', []);
        component.set('v.selectedTerritories', []);
        component.set('v.selectedProductTypes', []);
        component.set('v.selectedCustomers',[]);
        //component.find('releaseDateType').set('v.value','');
        //LRCC:1200
        //component.set('v.myData', []);
        component.set('v.checked',false);
        helper.handleCheckBoxValues(component); //LRCC:1199
        component.set('v.displayEditButtons', false);
        var editTableEvent = $A.get("e.c:EAW_EditTableEvent");
        editTableEvent.setParams({"editMode":false});
        editTableEvent.fire();
        component.set('v.editDisable',true);
        component.set('v.displayEditButtons', false);
    },
    displayNotes: function(component, event, helper) {
        component.set('v.parentId', event.getParams('parentId'));
    },
    editGrid: function(component, event, helper) {
        helper.handleCheckBoxValues(component); //LRCC:1199
        var checked = component.get("v.checked");
        component.set('v.displayEditButtons', checked);
        var editTableEvent = $A.get("e.c:EAW_EditTableEvent");
        editTableEvent.setParams({"editMode":checked});
        editTableEvent.fire();
        helper.makeUnsavedChanges(component, event, helper);
        if(checked == false && component.get("v.checkSaveRecords") == false) {
            
            component.find("modalCmp").open();           
            
        }
        
    },
    closeModal: function(component, event, helper) {
        component.find("modalCmp").close();
        component.set('v.checked', true);
        component.set('v.displayEditButtons', true);
        var editTableEvent = $A.get("e.c:EAW_EditTableEvent");
        editTableEvent.setParams({"editMode":true});
        editTableEvent.fire();
        var checked = component.get("v.checked");
        if(checked == false && component.get("v.checkSaveRecords") == false) {
            
            component.find("modalCmp").open();
        }        
    },
    saveModal : function(component, event, helper) {
        component.find("modalCmp").close();
        component.set('v.checked', false);
        component.set('v.displayEditButtons',false);
        component.set("v.myData", JSON.parse(JSON.stringify(component.get("v.myDataOriginal")))); 
        //LRCC-1668
        component.set("v.rowsToDisplay", JSON.parse(JSON.stringify(component.get("v.myDataOriginal"))));
        helper.clearUnsavedChanges(component, event, helper);
    },
    displayMassUpdate: function(component,event,helper){
        
        var selectedIds = new Array();
        var checkboxes = component.find("row-checkbox");
        
        if(checkboxes) {
            for(let check of checkboxes){
                var ele = check.getElement();
                if(ele.checked){
                    selectedIds.push(ele.value);
                }
            }
        }
        
        if(selectedIds.length==0) {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title: "Error!",
                message: $A.get("$Label.c.NoRecordsSelected"),
                type: "warning"
            });
            toastEvent.fire();
        }
        else {
            component.set("v.dataTableSelectedRows",selectedIds);
            component.set('v.displayMassUpdate',true);
        }
    },
    saveRecords: function(component,event,helper){
        
        var myColumns = component.get("v.myColumns");
         //LRCC-1459
        component.set("v.myData",component.get("v.rowsToDisplay"));
        var resultData = component.get("v.myData");
		
        
        var myDataOriginal = component.get("v.myDataOriginal");
        console.log('resultData:::',resultData);
        console.log('myDataOriginal:::',myDataOriginal);
        var updatedData = new Array();
        
        for (var i = 0; i < resultData.length; i++) {
            for (let column of myColumns) {
                var data = JSON.parse(JSON.stringify(resultData[i]));
                var dataOriginal = JSON.parse(JSON.stringify(myDataOriginal[i]));
                
                if (data[column.fieldName] && (typeof (data[column.fieldName]) == 'object') && data[column.fieldName].length != undefined && (updatedData.indexOf(data[i]).Id == -1)) {
                    
                    var temp = true;
                    for (let dataObj of data[column.fieldName]) {
                        var originalObjIds = [];
                        for (let originalObj of dataOriginal[column.fieldName]) {
                            var originalObjId;
                            if (originalObj.Id) {originalObjId=originalObj.Id;} else {originalObjId=originalObj.id;}
                            originalObjIds.push(originalObjId);
                        }
                        var dataObjId;
                        if (dataObj.id) {dataObjId=dataObj.id} else {dataObjId=dataObj.Id}
                        if ((! originalObjIds.includes(dataObjId))) {
                            console.log('111::::');
                            updatedData.push(data);
                            temp = false;
                            break;
                        }
                    }
                    if (temp) {
                        for (let originalObj of dataOriginal[column.fieldName]) {
                            var dataObjIds = [];
                            for (let dataObj of data[column.fieldName]) {
                                var dataObjId;
                                if (dataObj.Id) {dataObjId=dataObj.Id;} else {dataObjId=dataObj.id;}
                                dataObjIds.push(dataObjId);
                            }
                            var originalObjId;
                            if (originalObj.id) {originalObjId=originalObj.id} else {originalObjId=originalObj.Id}
                            if ((! dataObjIds.includes(originalObjId))) {
                                console.log('222::::');
                                updatedData.push(data);
                                break;
                            }
                        }
                    }
                } else {
                    if (data[column.fieldName] != dataOriginal[column.fieldName]) {
                        console.log('333::::');
                        updatedData.push(data);
                        break;
                    }
                } 
            }
        }
        
        
        
        console.log(updatedData.length);
        
        if(updatedData && updatedData.length > 0) {
            //LRCC-1668
            for (var i = 0; i < updatedData.length; i++) {
                for (let column of myColumns) {
                    var data = updatedData[i];
                    if(Array.isArray(data[column.fieldName])) {
                        console.log('colllll:::',column.fieldName);
                        if(data[column.fieldName].length) {
                            data[column.fieldName] = data[column.fieldName][0].title;
                        } else {
                            data[column.fieldName] = '';
                        }
                    }
                }
            }
            console.log('Records going for update : ' + JSON.stringify(updatedData));
            component.set("v.showSpinner",true);
            var action = component.get("c.saveReleaseDateGuidelinesChanges");
            action.setParams({
                "updatedDataList" : updatedData
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                var result = response.getReturnValue();
                if (state === 'SUCCESS') {
                    console.log(';;;;',JSON.parse(JSON.stringify(component.get("v.myData"))));
                    component.set("v.myDataOriginal", JSON.parse(JSON.stringify(component.get("v.myData"))));
                    
                    //component.set("v.checked",false); //LRCC-1668
                    helper.handleCheckBoxValues(component); //LRCC:1199
                    var checked = component.get("v.checked");
                    component.set('v.displayEditButtons', checked);
                    /*
                     * LRCC-1668
                    var editTableEvent = $A.get("e.c:EAW_EditTableEvent");
                    editTableEvent.setParams({"editMode":checked});
                    editTableEvent.fire();
                    */
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title: "Success!",
                        message: $A.get("$Label.c.RecordsSavedSuccessfully"),
                        type: "success"
                    });
                    toastEvent.fire();
                    component.set('v.checkSaveRecords',true);
                    helper.clearUnsavedChanges(component, event, helper);
                } else if (state === 'ERROR') {
                    let errors = response.getError();
                    let handleErrors = helper.handleErrors(errors, component, event, helper);
                    if(handleErrors) {
                        console.log(handleErrors);
                        helper.errorToast(component, handleErrors);
                    }
                }
            });
            $A.enqueueAction(action);
            component.set("v.showSpinner",false);
        } else {
            //LRCC-892
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title: "Info!",
                message: $A.get("$Label.c.EAW_RecordsNotEdited"),
                type: "info"
            });
            toastEvent.fire();
        }
    },
    checkAll : function (component, event, helper) {
        
        var self = event.target;
        var checkboxes = component.find("row-checkbox");
        
        if(!Array.isArray(checkboxes)){
            checkboxes = [checkboxes];
        }
        if(checkboxes){
            for(let check of checkboxes){
                check.getElement().checked = self.checked;
            }
        }
    },
    sort: function(component, event, helper) {
        //LRCC-1690
        var sortVal = event.currentTarget.dataset.value;
        //LRCC-1690
        component.set('v.selectedSortingParam',{'sortVal':sortVal});
        console.log('sortparam::',JSON.stringify(component.get('v.selectedSortingParam')));
        console.log('old value:'+sortVal);
        helper.sortBy(component,sortVal,false);
    	//LRCC-1459
        component.set('v.colWithAlpha',{'selectedCol':null,'selectedAlpha':null});	
        var target = event.currentTarget;
        var rowIndex = target.getAttribute("data-colIndex");
        console.log("Row No : " + rowIndex);
        console.log('::: selected :::', JSON.stringify(component.get("v.myColumns")[rowIndex]));
        
        if((component.get("v.myColumns")[rowIndex].type == "string" || 
           component.get("v.myColumns")[rowIndex].type == "picklist") && 
           ((component.get("v.myColumns")[rowIndex].fieldName.indexOf("__c") > -1)||
           component.get("v.myColumns")[rowIndex].fieldName == "Name")){
            
            var colAplpha = component.get('v.colWithAlpha');
            colAplpha.selectedCol = component.get("v.myColumns")[rowIndex].fieldName;
            component.set('v.colWithAlpha',colAplpha)
        }else{
            helper.showErrorToast(component,'Selecting column is not valid for alphabet sort');
            component.set('v.colWithAlpha',{'selectedCol':null,'selectedAlpha':null});	
            return;
        }
        console.log('::: selected :::',JSON.stringify(component.get('v.colWithAlpha')));
        console.log('::: selected :myColumns::',JSON.stringify( component.get("v.myColumns")));
       
    },
    cancel: function(component, event, helper) {
        
        component.set("v.myData", JSON.parse(JSON.stringify(component.get("v.myDataOriginal"))));
        //LRCC-1668
        component.set("v.rowsToDisplay", JSON.parse(JSON.stringify(component.get("v.myDataOriginal"))));
        component.set("v.checked", false);
        helper.handleCheckBoxValues(component); //LRCC:1199
        var checked = component.get("v.checked");
        component.set('v.editDisable',false);
        component.set('v.displayEditButtons', checked);
        
        var editTableEvent = $A.get("e.c:EAW_EditTableEvent");
        editTableEvent.setParams({"editMode":checked});
        editTableEvent.fire();
        helper.clearUnsavedChanges(component, event, helper);
    },
    deleteRecords: function(component, event, helper) {
        
        var selectedIds = new Array();
        console.log('hi');
        var checkboxes = component.find("row-checkbox");
        console.dir(checkboxes);        
        if(checkboxes){
            if(Array.isArray(checkboxes)) {
                for(let check of checkboxes){
                    var ele = check.getElement();
                    if(ele.checked){
                        selectedIds.push(ele.value);
                    }
                }
            } else {
                var ele = checkboxes.getElement();
                if(ele.checked){
                    selectedIds.push(ele.value);
                }
            }
        }
        if(selectedIds.length==0)
        {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title: "Error!",
                message: $A.get("$Label.c.NoRecordsSelected"),
                type: "warning"
            });
            toastEvent.fire();
        }
        else
        {
            var action = component.get("c.deleteReleaseDateGuidelines");
            action.setParams({"recIds" : selectedIds});
            action.setCallback(this, function(response)
                               {
                                   if(response.getState() === "SUCCESS")
                                   {
                                      //helper.search(component,true);
                                      //LRCC-1459
                                      
					                    var selectedFilterMap = component.get("v.isAlphaFiltered") ? component.get('v.colWithAlpha') : null;
					                    let selectedFilterMapTemp = null;
					                    if(selectedFilterMap && selectedFilterMap.selectedCol != '' && selectedFilterMap.selectedAlpha != ''){
					                        selectedFilterMapTemp = JSON.stringify(selectedFilterMap);
					                    }
					                    
					                    //LRCC-1459
					                    if(component.get("v.pageNumber") && component.get("v.pagesPerRecod")){
					                        
					                        helper.search(component,true, event, helper,component.get("v.pagesPerRecod"),component.get("v.pageNumber"),JSON.stringify(component.get("v.pageIdMap")),selectedFilterMapTemp);
					                    } else {
					                        
					                        helper.search(component,true, event, helper,50,1,JSON.stringify({}));
					                    }
                                       var toastEvent = $A.get("e.force:showToast");
                                       toastEvent.setParams({
                                           title: "Success!",
                                           message: $A.get("$Label.c.RecordsDeletedSuccessfully"),
                                           type: "success"
                                       });
                                       toastEvent.fire();
                                   }
                                   else if (response.getState() === 'ERROR') 
                                   {
                                       console.log(JSON.stringify(response.getError()));
                                       let errors = response.getError();
                                       let handleErrors = helper.handleErrors(errors, component, event, helper);
                                       if(handleErrors) {
                                           //LRCC-1663
                                           helper.errorToast(component, handleErrors);
                                       }
                                   }
                               });
            $A.enqueueAction(action);
        }
    },
    rowSelection: function(component, event, helper) {
        var dataTableSelectedRows = event.getParam('selectedRows');
        component.set("v.dataTableSelectedRows", dataTableSelectedRows);
    },
    createRecord : function (component, event, helper) 
    {
        //LRCC-1556
        component.set('v.modalpopup', true);
    },
    calculateWidth: function(component, event, helper) {
        
        var childObj = event.target
        var mouseStart=event.clientX;
        component.set("v.currentEle", childObj);
        component.set("v.mouseStart",mouseStart);
        // Stop text selection event so mouse move event works perfectlly.
        if(event.stopPropagation) event.stopPropagation();
        if(event.preventDefault) event.preventDefault();
        event.cancelBubble=true;
        event.returnValue=false;  
    },
    
    setNewWidth: function(component, event, helper) {
        var currentEle = component.get("v.currentEle");
        if( currentEle != null && currentEle.tagName ) {
            var parObj = currentEle;
            while(parObj.parentNode.tagName != 'TH') {
                if( parObj.className == 'slds-resizable__handle')
                    currentEle = parObj;    
                parObj = parObj.parentNode;
                count++;
            }
            var count = 1;
            var mouseStart = component.get("v.mouseStart");
            var oldWidth = parObj.offsetWidth;  // Get the width of DIV
            var newWidth = oldWidth + (event.clientX - parseFloat(mouseStart));
            component.set("v.newWidth", newWidth);
            currentEle.style.right = ( oldWidth - newWidth ) +'px';
            component.set("v.currentEle", currentEle);
            if(currentEle.id){ // LRCC - 1282
                var myColumns = component.get('v.myColumns');
                for(var col of myColumns){
                    if(col.fieldName == currentEle.id){
                        console.log(currentEle.id,':::current::',oldWidth - newWidth);
                        col.width = newWidth;
                    }
                }
                component.set('v.myColumns',myColumns);
                component.set('v.bodyWidth',((component.get('v.myColumns').length + 1) * 140 ) + newWidth );
            }
        }
    },
    // We are setting the width which is just changed by the mouse move event     
    resetColumn: function(component, event, helper) {
        // Get the component which was used for the mouse move
        if(component.get("v.currentEle") !== null) {
            var newWidth = component.get("v.newWidth"); 
            var currentEle = component.get("v.currentEle").parentNode.parentNode; // Get the DIV
            var parObj = currentEle.parentNode; // Get the TH Element
            parObj.style.width = newWidth+'px';
            currentEle.style.width = newWidth+'px';
            console.log(newWidth);
            component.get("v.currentEle").style.right = 0; // Reset the column devided 
            component.set("v.currentEle", null); // Reset null so mouse move doesn't react again
        }
    },
    rowAction : function(component, event, helper) {
        var recId = event.getSource().get('v.name');
        
        var notesEvent = $A.get("e.c:EAW_NotesEvent");
        notesEvent.setParams({"parentId":recId});
        notesEvent.fire();
    },
    //LRCC-1459
    renderPage : function(component, event, helper) {
        
        console.log('click::::',component.get("v.pagesPerRecods"));
        console.log('click::22::',component.get("v.pagesPerRecod"));
        //':::>::',component.get("v.pageNumber"),'<<>>',component.get('v.colWithAlpha'))
        
        let isAlphaFiltered = component.get("v.isAlphaFiltered");
        let pageConfigEvt = component.getEvent("pageConfigEvt");
        var selectedFilterMap = isAlphaFiltered ? component.get('v.colWithAlpha') : null;
        
        helper.fetchReleaseDateDate(component, event, helper,selectedFilterMap,component.get("v.pageNumber"),component.get("v.pagesPerRecod"));
       
		
	},
	handleChange: function(component, event, helper) {
        
         if(component.get("v.isEditable")){
            helper.showErrorToast(component);
            component.find('pagePerRecord').set("v.value",component.get("v.oldPagesPerRecord"));
            return;
        } 
         
        component.set('v.pageNumber', 1);
        let pagesPerRecord = parseInt(component.find('pagePerRecord').get("v.value"));
         
        component.set("v.pagesPerRecod",component.find('pagePerRecord').get("v.value"));
        component.set("v.oldPagesPerRecord",component.find('pagePerRecord').get("v.value"));
        component.set("v.maxPage", Math.floor((component.get('v.totalRecordCount')+pagesPerRecord - 1 )/pagesPerRecord)); 
         
      
        helper.fetchReleaseDateDate(component, event, helper,null,component.get("v.pageNumber"),component.get("v.pagesPerRecod"));
    },    
    chooseAlphabet : function(component, event, helper) {
        
       if(component.get("v.isEditable")){
            helper.showErrorToast(component);
            return;
        } 
        
        var target = event.currentTarget;
        var rowIndex = target.getAttribute("data-rowIndex");
       
        if(component.get("v.alphabets")[rowIndex]){
            
            var colAplpha = component.get('v.colWithAlpha');
            colAplpha.selectedAlpha = component.get("v.alphabets")[rowIndex];
            component.set('v.colWithAlpha',colAplpha);
        }
        let colWithAlpha = component.get('v.colWithAlpha');
        if( colWithAlpha.selectedCol != null ||   colWithAlpha.selectedAlpha == 'All' ){
            
            if(colWithAlpha.selectedAlpha == 'All'){
                
                colWithAlpha.selectedCol == null;
                component.set('v.colWithAlpha',colWithAlpha);
                component.set("v.isAlphaFiltered",false);
            }
            component.set("v.isAlphaFiltered",true);
            component.set('v.pageNumber', 1);
            component.set("v.pageIdMap",{});
            var pageConfigEvt = component.getEvent("pageConfigEvt");
            
           /* pageConfigEvt.setParams({"pagesPerRecord" : component.get("v.pagesPerRecod"),
                                     "currentPageNumber":1,
                                     "selectedFilterMap" : component.get('v.colWithAlpha') });
            pageConfigEvt.fire();*/
            helper.fetchReleaseDateDate(component, event, helper,component.get('v.colWithAlpha'),component.get("v.pageNumber"),component.get("v.pagesPerRecod"));
   

        }
        if(colWithAlpha.selectedAlpha == 'All'){
            
            component.set('v.colWithAlpha',{'selectedCol':null,'selectedAlpha':null});	
        }
        
         console.log('::: selected :::', component.get('v.colWithAlpha'));
    },
    displayUsedBy : function(cmp,event,helper){
        if(event.getSource().get('v.name')){
            //console.log(event.getSource().get('v.name')+'::::::::::'+cmp.find('usedByModal'));
            //cmp.find('modalCmp').set('v.recordId',event.getSource().get('v.name'));
            cmp.find('usedByModal').openModal(event.getSource().get('v.name'));
        }
    }
})