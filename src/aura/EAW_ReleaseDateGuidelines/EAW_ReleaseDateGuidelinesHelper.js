({
    search: function (component,isEditMode,event, helper,pagesPerRecord,currentPageNumber,pageIdMap,selectedFilterMap) {
        
        
        try {
            //ADD SPINNER
            console.log(':::: Inside-Search ::::');
            component.set('v.showSpinner',true);
            var strObjectName = component.get("v.strObjectName");
            var strFieldSetName = component.get("v.strFieldSetName");
            //var releaseDateType = component.find('releaseDateType') ? component.find('releaseDateType').get('v.value') : '';
            
            var action = component.get("c.getRecords");
            
            action.setParams({
                "objFieldSetMap": {
                    "EAW_Release_Date_Guideline__c": "Search_Result_Fields"
                },
                "territory": JSON.stringify(component.get("v.selectedTerritories")),
                "language": JSON.stringify(component.get("v.selectedLanguages")),
                "productType": JSON.stringify(component.get("v.selectedProductTypes")),
                "customer": JSON.stringify(component.get("v.selectedCustomers")),
                "releaseDateType": JSON.stringify(component.get("v.selectedReleaseDateTypes")),
                'pagesPerRecord' : pagesPerRecord,
                'currentPageNumber':currentPageNumber,
                'pageIdMapUI':pageIdMap,
                'selectedFilterMap':selectedFilterMap,
            });
            action.setCallback(this, function (response) {
                
                var state = response.getState();
                var result = response.getReturnValue();
                
                if (state === 'SUCCESS') {
                    //LRCC-1459
                    console.log('result::::test:::test:::',result);
                    if(result.totalCount && result.totalCount[0].expr0){
                        
                        console.log('result::::result.totalCount[0].expr0:::test:::',result.totalCount[0].expr0);    
                        component.set('v.totalRecordCount', result.totalCount[0].expr0);    
                    }else {
                        component.set('v.totalRecordCount',0);
                    }
                    component.set('v.initialTableShow',true);
                    console.log('result::::initialTableShow:::test:::',component.get('v.initialTableShow'));
                    console.dir(response.getReturnValue());
                    console.log('::::::::::::',JSON.parse(JSON.stringify(response.getReturnValue())));
                    var data = new Array();
                    console.dir(response.getReturnValue());
                    
                    for(let col of response.getReturnValue().ReleaseDate) {
                        
                        if(col.length > 1) {
                            for(let colValue of col)
                        		data.push(colValue);
                        } else {	
                            console.log(col)
                            data.push(col);
                    	}
                    }
                    var col = component.get('v.myColumns');
                    component.set("v.myData", this.getLookupFields(col, data, 'Name'));
                    component.set("v.myDataOriginal", JSON.parse(JSON.stringify(component.get("v.myData"))));
                    component.set('v.enableInfiniteLoading', true);
                    
                    if(component.find("header-checkbox") && component.find("header-checkbox").getElement()){
                        component.find("header-checkbox").getElement().checked = false;
                    }
                    
                    if(isEditMode){
                        component.set("v.checked",true);
                        var checked = component.get("v.checked");
                        component.set('v.displayEditButtons', checked);
                        
                        var editTableEvent = $A.get("e.c:EAW_EditTableEvent");
                        editTableEvent.setParams({"editMode":checked});
                        editTableEvent.fire();
                    }else{
                        component.set("v.checked",false);
                        var checked = component.get("v.checked");
                        component.set('v.displayEditButtons', checked);
                        
                        var editTableEvent = $A.get("e.c:EAW_EditTableEvent");
                        editTableEvent.setParams({"editMode":checked});
                        editTableEvent.fire();
                    }
                    //LRCC-1459
					helper.generateAlphabets(component, event, helper);
                } else if (state === 'ERROR') {
                    console.log(JSON.stringify(response.getError()));
                    component.set('v.myData', []);
                    //LRCC-1459
					helper.generateAlphabets(component, event, helper);
                }
                component.set('v.showSpinner',false);
            });
            $A.enqueueAction(action);
        } catch (e) {
            console.log(e);
        }
    },
    sortBy: function (component, field,nextFlag) {
        //LRCC-1690
        let sortAsc = component.get("v.sortAsc"),
            sortField = component.get("v.sortField"),
            records = component.get("v.rowsToDisplay");
        if(!nextFlag){
            
             sortAsc = field == sortField ? !sortAsc : true;
        }
        /*records.sort(function (a, b) {
            var t1 = a[field] == b[field],
                t2 = a[field] > b[field];
            return t1 ? 0 : (sortAsc ? -1 : 1) * (t2 ? -1 : 1);
        });*/
		records.sort(function(a, b) {
            if(sortAsc) {
                if(a[field] == undefined) {
                    return -1;
                } else if(b[field] == undefined) {
                    return 1;
                }
                
                return (new String(a[field]).toUpperCase() > new String(b[field]).toUpperCase()) ? 1 : ((new String(b[field]).toUpperCase() > new String(a[field]).toUpperCase()) ? -1 : 0);
            } else {
                if(a[field] == undefined) {
                    
                    return 1;
                } else if(b[field] == undefined) {
                    return -1;
                }
                
                return (new String(a[field]).toUpperCase() < new String(b[field]).toUpperCase()) ? 1 : ((new String(b[field]).toUpperCase() < new String(a[field]).toUpperCase()) ? -1 : 0);
            }
        });
        component.set("v.sortAsc", sortAsc);
        component.set("v.sortField", field);
        component.set("v.myData", records);
        component.set("v.rowsToDisplay", records);
        
        /****LRCC:887-Sorting indicator applied logic****/
        component.set("v.selectedTabsoft", field);
        if (sortAsc == true) {
            component.set("v.arrowDirection", 'arrowup');
        } else {
            component.set("v.arrowDirection", 'arrowdown');
        }
    },
    
    getLookupFields: function (col, data, fName) {
        for (var i = 0, clen = col.length; i < clen; i++) {
            if (col[i]['type'] == 'reference') {
                for (var j = 0, rlen = data.length; j < rlen; j++) {
                    var str = (col[i]['fieldName']).replace('__c', '__r');
                    var fValue = data[j][str];
                    if (fValue != undefined) {
                        // console.dir(fValue[fName]);
                        data[j][col[i]['fieldName']] = fValue[fName];
                    }
                }
            }
        }
        return data;
    },
    
    fieldSet: function (component) {
        try {
            var strObjectName = component.get("v.strObjectName");
            var strFieldSetName = component.get("v.strFieldSetName");
            var strObjFieldSetMap = new Map();
            
            strObjFieldSetMap.set(strObjectName, strFieldSetName);
            
            console.log('Field Set Map:');
            console.dir(strObjFieldSetMap);
            
            var action = component.get("c.getFields");
            action.setParams(
                {
                    "objFieldSetMap": {
                        "EAW_Release_Date_Guideline__c": "Search_Result_Fields"
                    }
                }
            );
            
            action.setCallback(this, function (response) {
                var state = response.getState();
                
                if(state === 'SUCCESS') {
                    
                    var columns = new Array();
                    console.dir(response.getReturnValue());
                    for(let col of response.getReturnValue()) {
                        for(let colName of col) {
                            colName.width = '134'; // LRCC - 1282
                            if(colName.fieldName == 'Customers__c') {
                                columns.push({fieldName: "Customers__c", label: "Customers", picklistValues: Array(0), type: "textarea", updateable: false, width : '134'});
                            }
                            else if(colName.fieldName == 'Name') {
                                //LRCC-1694
                                columns.push({fieldName: "Name", label: "Release Date Guideline Name", picklistValues: Array(0), type: "string", updateable: false,width:'396'});
                            }
                            else if(colName.fieldName == 'All_Rules__c') {
                                columns.push({fieldName: "All_Rules__c", label: "All Rules", picklistValues: Array(0), type: "textarea", updateable: false,width : '134'});
                            }
                            else {
                                columns.push(colName);
                            }
                        }
                    }
                    console.log('Columns:', columns);
                    console.dir(columns);
                    
                    component.set('v.myColumns', columns);
                    //this.setBodyWidth(component); // LRCC - 1282
                } 
                else if (state === 'ERROR') {
                    var errors = response.getError();
                    
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " +
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                } else {
                    console.log('Something went wrong, Please check with your admin');
                }
            });
            $A.enqueueAction(action);
        } catch (e) {
            console.log('Exception:' + e);
        }
    },
    showToast : function(type,message){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "type": type,
            "message": message
        });
        toastEvent.fire();
    },
	
    handleErrors: function(errors, component, event, helper) {
		
        let errorMessage = '';
        if (errors && Array.isArray(errors) && errors.length > 0) {
    		
            errors.forEach(error => {
        	    if (error.message && errorMessage.includes(error.message) == false) {
	                errorMessage += error.message + "\n";
	            }
	            if (error.pageErrors && error.pageErrors.length > 0) {
	                error.pageErrors.forEach(pageError => {
	                    if (errorMessage.includes(error.message) == false)
	                        errorMessage += pageError.message + "\n";
	                });
	            } else if (error.fieldErrors) {
	                let fields = Object.keys(error.fieldErrors);
	                fields.forEach(fieldError => {
	                    if (Array.isArray(fieldError)) {
	                        fieldError.forEach(err => {
	                            if (errorMessage.includes(error.message) == false)
	                                errorMessage += err.message;
	                        });
	                    }
	                });
	            }
	        });
	    } else {
            errorMessage = errors;
        }
	    if (errorMessage.indexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION') !== -1) {
	        
            let errorMessageList = errorMessage.split('FIELD_CUSTOM_VALIDATION_EXCEPTION,');
	        if (errorMessageList.length > 1) {
	            let finalErroList = errorMessageList[1].split(': []');
	            if (finalErroList.length) {
	                errorMessage = finalErroList[0];
	            }
	        }
	    }
	    if (errorMessage.indexOf('REQUIRED_FIELD_MISSING') !== -1) {
	        
            let errorMessageList = errorMessage.split('REQUIRED_FIELD_MISSING,');
            if (errorMessageList.length > 1) {
                let finalErrorList = errorMessageList[1].split(': []');
	            if (finalErrorList.length) {
                	errorMessage = finalErrorList[0];
                }
	        }
	    } else {
            let errMsg = errorMessage.split(",").join("\n");
            errorMessage = errMsg;
        }
		console.log('***** errorMessage-->', errorMessage);
        return errorMessage;
	},
    
    errorToast : function(component, message) {
        component.find('notifLib').showNotice({
            "variant": "error",
            "header": "Error!",
            "message": message
        });
    },
        	
    successToast : function(message) {
        var toastEvent = $A.get('e.force:showToast');
	
        toastEvent.setParams({
            'title': 'Success!',
            'type': 'success',
            'message': message
        });
        toastEvent.fire();
    },
        
    //LRCC:1199-Edit toggle Action Logic   
    handleCheckBoxValues: function(component) {
        
        if(component.get('v.checked') == false) {
            
            component.set("v.selectedRows",[]);
           
            if(component.find("header-checkbox") != undefined ) {
            	component.find("header-checkbox").getElement().checked = false;
            }
            
            if(component.find("row-checkbox") != undefined ) {
                
                var checkboxes = component.find("row-checkbox");
                
                if(!Array.isArray(checkboxes)){
                    checkboxes = [checkboxes];
                }
                
                if(checkboxes) {
                    for(let check of checkboxes){
                        if(check.getElement() != null)
                            check.getElement().checked = false;
                    }
                }
            }
        }
    },
        
    makeUnsavedChanges: function(cmp, evt, helper) {
        var unsaved = cmp.find("unsaved");
        unsaved.setUnsavedChanges(true, { label: 'Release Date Guideline Results' });
    },
        
    clearUnsavedChanges: function(cmp, evt, helper) {
        var unsaved = cmp.find("unsaved");
        unsaved.setUnsavedChanges(false);
    },
    setBodyWidth : function(component){
        if(component.get('v.myData') && component.get('v.myData').length){  // LRCC - 1282
            console.log(':::::::::::::::;',component.getGlobalId());
            const globalId = component.getGlobalId(),
                parentnode = document.getElementById(globalId),
                thElements = parentnode.getElementsByTagName('th'),
                tdElements = parentnode.getElementsByTagName('td');
            var bodyWidth = 0;
            
            for (let i = 0; i < thElements.length; i++) {
               if(tdElements[i] && thElements[i]) tdElements[i].style.width = thElements[i].offsetWidth + 'px';
                bodyWidth += thElements[i].offsetWidth;
            }
            console.log(':::::::bodyWidth:::::::::::'+bodyWidth);
            component.set('v.bodyWidth',bodyWidth);
        }  else{
            component.set('v.bodyWidth', 250);
        }
    },
    //LRCC-1459
    generateAlphabets : function(component, event, helper) {
          console.log('generateAlphabets:::1::');
        var alphabets = [];
        for(var i= 65 ; i < 91 ; i++){
            
            alphabets.push(String.fromCharCode(i));
        }
        alphabets.push('All');
        component.set('v.alphabets',alphabets);
          //LRCC-1459
		 helper.renderPage(component, event, helper)
        
    },
    //LRCC-1459
    formPageIdMap : function(component, event, helper) {
      console.log('formPageIdMap:::::');
        const pageNumber = component.get("v.pageNumber"),
            rowsToDisplay = component.get("v.rowsToDisplay");
        
        let pageIdMap = component.get("v.pageIdMap") || {};
        
        const recordIds = rowsToDisplay.map((rec) =>  rec.Id);
        
        pageIdMap[pageNumber] = recordIds;
        
        component.set("v.pageIdMap",pageIdMap);
        console.log('pageIdMap:::::test:::::::',JSON.stringify(pageIdMap));
        
        let pagesPerRecord = parseInt(component.find('pagePerRecord').get("v.value"));
        console.log('pagesPerRecord count ::::',pagesPerRecord);
        console.log('total count ::::',component.get('v.totalRecordCount'));
        console.log('total count ::::',component.get('v.totalRecordCount'));
        console.log('calc ::::',(component.get('v.totalRecordCount') + pagesPerRecord - 1 )/pagesPerRecord);
        component.set("v.maxPage", Math.floor((component.get('v.totalRecordCount') + pagesPerRecord - 1 )/pagesPerRecord)); 
        console.log('maxPage count ::::',component.get('v.maxPage'));
        
        var selctsort = component.get('v.selectedSortingParam');
        console.log('tosort::',selctsort);
        if(selctsort){
            this.sortBy(component, selctsort.sortVal,true);  
        }
       
    },
    //LRCC-1459
    reinitializePageMap : function(component,pageNumber) {
        
        let pageIdMap = component.get("v.pageIdMap");
        
        for(let i = pageNumber ; i <= Object.keys(pageIdMap).length ; i++ ){
            pageIdMap[i] = [];
        } 
        component.set("v.pageIdMap",pageIdMap);
    },
    //LRCC-1459
    renderPage: function(component, event, helper) {
        
        console.log('call render page:::1::',component.get("v.myData"));
       
        
        let records = component.get("v.myData") || [];
        
         //var pageNumber = component.get("v.pageNumber");
        //var pagesPerRecord = parseInt(component.find('pagePerRecord').get("v.value"));
       //var  rowsToDisplay = records;
        
        component.set("v.rowsToDisplay", records);
        console.log('rowsToDisplay::test:::',records);
        helper.formPageIdMap(component);
        
    },
    //LRCC-1459
    showErrorToast : function(component,msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: "Info!",
            message:msg ,
            type: "info"
        });
        toastEvent.fire();
    },
        //LRCC-1459
    fetchReleaseDateDate: function(component, event, helper,selectedFilterMap,currentPageNumber,pagesPerRecord) {
        
        
        //LRCC-1459
        let selectedFilterMapTemp = null;
        if(selectedFilterMap && selectedFilterMap.selectedCol != '' && selectedFilterMap.selectedAlpha != ''){
            selectedFilterMapTemp = JSON.stringify(selectedFilterMap);
        }
        console.log('window:::Guide::::::::',currentPageNumber,pagesPerRecord,JSON.stringify(component.get("v.pageIdMap")));
        
        //LRCC-1459
        if(currentPageNumber && pagesPerRecord){
            
            helper.search(component,false, event, helper,pagesPerRecord,currentPageNumber,JSON.stringify(component.get("v.pageIdMap")),selectedFilterMapTemp);
        } else {
            
            helper.search(component,false, event, helper,100,1,JSON.stringify({}));
        }
        
    },
});