({
	sortBy :  function (component, field, index,nextFlag) {
        
        let sortAsc = component.get("v.sortAsc");
        let sortField = component.get("v.sortField");
        //LRCC-1459
        //LRCC-1690
        let resultData =  component.get("v.item");
            //component.get("v.resultData");
        let records = resultData;//[index];
        console.log(resultData)
         //LRCC-1690
        if(field == 'windowTags'){
          
            field ='windowTagsSortColumn';
        }
         if(!nextFlag){
            
             sortAsc = field == sortField ? !sortAsc : true;
        }
        records.sort(function(a, b) {
            if(sortAsc) {
                if(a[field] == undefined) {
                    return -1;
                } else if(b[field] == undefined) {
                    return 1;
                }
                
                return (new String(a[field]).toUpperCase() > new String(b[field]).toUpperCase()) ? 1 : ((new String(b[field]).toUpperCase() > new String(a[field]).toUpperCase()) ? -1 : 0);
            } else {
                if(a[field] == undefined) {
                    
                    return 1;
                } else if(b[field] == undefined) {
                    return -1;
                }
                
                return (new String(a[field]).toUpperCase() < new String(b[field]).toUpperCase()) ? 1 : ((new String(b[field]).toUpperCase() < new String(a[field]).toUpperCase()) ? -1 : 0);
            }
        });
        
        resultData[index] = records;
        
        component.set("v.sortAsc", sortAsc);
        component.set("v.sortField", field);
        component.set("v.rowsToDisplay", resultData);
         //LRCC-1690
         component.set("v.item", resultData);
        
        /****LRCC:887-Sorting indicator applied logic****/
        if(field == 'windowTagsSortColumn'){
          
            field ='windowTags';
        }
        component.set("v.selectedTabsoft", field);
        if (sortAsc == true) {
            component.set("v.arrowDirection", 'arrowup');
        } else {
            component.set("v.arrowDirection", 'arrowdown');
        }
    },
    setBodyWidth : function(component){
        
        if(component.get('v.resultColumns') && component.get('v.resultColumns').length){  
            console.log(':::::::::::::::;',component.getGlobalId());
            const globalId = component.getGlobalId(),
                parentnode = document.getElementById(globalId),
                thElements = parentnode.getElementsByTagName('th'),
                tdElements = parentnode.getElementsByTagName('td');
            var bodyWidth = 0;
            
            for (let i = 0; i < thElements.length; i++) {
                 if(thElements[i] && tdElements[i])tdElements[i].style.width = thElements[i].offsetWidth   + 'px';
                bodyWidth += thElements[i].offsetWidth;
            }
            console.log(':::::::bodyWidth:::::::::::'+bodyWidth);
            if(component.get('v.isEditable')){
                //LRCC-1517
                if(component.get('v.buttonstate')) {
                    component.set('v.bodyWidth', (bodyWidth / 2) + 35);
                } else {
                    component.set('v.bodyWidth',bodyWidth + 35);
                }
            } else{
                //LRCC-1517
                if(component.get('v.buttonstate')) {
                    component.set('v.bodyWidth', (bodyWidth / 2));
                } else {
                    component.set('v.bodyWidth',bodyWidth);
                }
            }
        }
    },
    //LRCC-1459
        showErrorToast : function(component,msg) {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title: "Info!",
                message:msg ,
                type: "info"
            });
            toastEvent.fire();
        },
})