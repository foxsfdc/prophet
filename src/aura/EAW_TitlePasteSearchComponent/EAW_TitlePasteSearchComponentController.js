({
	doInit: function (component, event, helper) { // Add coloumns for LRCC-1007
	//1095 & 1250-Replace Title EDM with Title Attribute
        component.set('v.titleColumns', [
            {label: 'Fin\'l Title Id', fieldName: 'Title_Id', type: 'text', initialWidth: 150}, 
            {label: 'Title', fieldName: 'Name', type: 'url',
             typeAttributes: { label: {fieldName: 'Title_Name'}, target: '_blank'}, initialWidth: 200},
            {label: 'Intl Product Type', fieldName: 'PROD_TYP_CD_INTL', type: 'text', initialWidth: 150},
            {label: 'Product Type', fieldName: 'PROD_TYP_CD', type: 'text', initialWidth: 150},
            {label: 'Division', fieldName: 'FIN_DIV_CD', type: 'text', initialWidth: 150},
            {label: 'Title Tag(s)', fieldName: 'DIR_NM__c', type: 'text', initialWidth: 150},	// Title Tags field temporarily held in Director Name
            {label: 'Initial Release Date', fieldName: 'FRST_REL_DATE__c', type: 'Date', initialWidth: 150}
        ]);
        component.find('pastedTitleInput').set('v.value', '');
        component.set('v.selectedTitles', []);
        component.set('v.titleSearchResults', []);
    },
	parsePastedTitles : function(component, event, helper) {
		try {
			var titles = component.find('pastedTitleInput') ? component.find('pastedTitleInput').get('v.value') : [];
			var action = component.get("c.searchForPastedTitles");
			action.setParams({
				"pastedTitles" : titles
			});
			console.log('my titles=' + titles);
			action.setCallback(this, function(response) {
				var state = response.getState();
				var results = response.getReturnValue();

				if (state === 'SUCCESS') {
					console.log(results);
                    
                    for(let result of results) {
                    	//LRCC-1325 
                      	result.Title_Name = result.Name;
						result.Name = '/one/one.app?sObject/'+ result.Id + '/view#/sObject/' + result.Id + '/view';
						
            			//result.Title_Id = '/one/one.app?sObject/'+ result.Id + '/view#/sObject/' + result.Id + '/view';
                        
                       //LRCC-1098
							//1095 & 1250-Replace Title EDM with Title Attribute
                            if(result.FIN_PROD_ID__c) {
                                result.Title_Id = result.FIN_PROD_ID__c;
                            }
                            
                            if(result.PROD_TYP_CD_INTL__r && result.PROD_TYP_CD_INTL__r.Name) {
                                result.PROD_TYP_CD_INTL = result.PROD_TYP_CD_INTL__r.Name;
                            }
                            
                            if(result.PROD_TYP_CD_INTL__r && result.PROD_TYP_CD_INTL__r.Name) {
                                result.PROD_TYP_CD = result.PROD_TYP_CD_INTL__r.Name;
                            }
                            
                            if(result.FIN_DIV_CD__r && result.FIN_DIV_CD__r.Name) {
                                result.FIN_DIV_CD = result.FIN_DIV_CD__r.Name;
                            }
                            
                            if(result.First_Release_Date__c) {
                                result.FRST_REL_DATE__c = result.First_Release_Date__c;
                            }
        			}
                    console.log('::results:::',results);
					component.set('v.titleSearchResults', results);
				} else if (state === 'ERROR') {
					console.log(response.getError());
				}
			});
			$A.enqueueAction(action);
		} catch (e) {
			console.log(e);
		}
	},
	addSelectedTitles : function(component, event, helper) {
	
		// add selected items
        if(!component.get('v.titleSearchResults').length){
            helper.showToast('error','No Search Results has listed.');
        } else if(!component.get('v.selectedTitles').length){
            helper.showToast('error','No Titles has been selected.');
        } else{
            var selectedTitles = component.get("v.selectedTitles");
            var dataArr = component.get("v.data");
            let gridIds=[];
            
            for (let i = 0; i < dataArr.length; i++) {
                gridIds.push(dataArr[i].Id);
            }
            
            for(let i = 0; i < selectedTitles.length; i++) {
                if(!gridIds.includes(selectedTitles[i].Id))dataArr.push(selectedTitles[i]);
            }
            console.log(dataArr);
            
            // close popup
            document.getElementById("EAW_TitlePasteSearchComponent").style.display = "none";
            
            // update data with new selected items
            component.set('v.data', dataArr);
            
            // Reset inputs
            component.find('pastedTitleInput').set('v.value', '');
            component.set('v.selectedTitles', []);
            component.set('v.titleSearchResults', []);
        }
    },
    /** Methods below hide/show the popup*/
    showPasteTitleModal : function(component, event, helper) {
        document.getElementById("EAW_TitlePasteSearchComponent").style.display = "block";
    },
    hidePasteTitleModal : function(component, event, helper){
        document.getElementById("EAW_TitlePasteSearchComponent").style.display = "none";
        
        // Reset inputs
        component.find('pastedTitleInput').set('v.value', '');
        component.set('v.selectedTitles', []);
        component.set('v.titleSearchResults', []);
    },
    clear: function(component, event, helper) {
        // Reset inputs
        component.find('pastedTitleInput').set('v.value', '');
        component.set('v.selectedTitles', []);
        component.set('v.titleSearchResults', []);
    }
})