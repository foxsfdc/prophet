({
    clone : function(component, event, helper) {
        var cloneVal = component.get("v.cloneValue");

        if(cloneVal === 'cloneWindowGuidelines') {
            helper.clone('c.clonePlanAndWindows', component);
        } else if (cloneVal === 'useSameWindowGuidelines') {
            helper.clone('c.clonePlanSameWindows', component);
        } else if (cloneVal === 'cloneWithoutWindowGuidelines') {
            helper.clone('c.clonePlanNoWindows', component);
        }

        helper.hideCloneModal();
    },

    hideCloneModal : function(component, event, helper) {
        helper.hideCloneModal();
    }

})