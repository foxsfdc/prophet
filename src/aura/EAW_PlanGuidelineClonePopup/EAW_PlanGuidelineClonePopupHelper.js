({
    errorToast : function(message) {
        var toastEvent = $A.get('e.force:showToast');

        toastEvent.setParams({
            'title': 'Error!',
            'type': 'error',
            'message': message
        });

        toastEvent.fire();
    },

    successToast : function(message) {
        var toastEvent = $A.get('e.force:showToast');

        toastEvent.setParams({
            'title': 'Success!',
            'type': 'success',
            'message': message
        });

        toastEvent.fire();
    },

    clone : function(type, component) {
        var planData = component.get("v.planData");
        var selectedRows = component.get("v.selectedRows");
        var action = component.get(type);
        action.setParams({'plans': selectedRows});

        action.setCallback(this, function(response) {
            var state = response.getState();
            var results = response.getReturnValue();

            if(state === 'SUCCESS') {
                var result = results[0];
                var navigateEvent = $A.get('e.force:navigateToSObject');

                navigateEvent.setParams({
                    'recordId': result.Id
                });

                navigateEvent.fire();
            } else {
                this.errorToast('The plan has not been cloned.');
            }
        });
        $A.enqueueAction(action);
    }

})