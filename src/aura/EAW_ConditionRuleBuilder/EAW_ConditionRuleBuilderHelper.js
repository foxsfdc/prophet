({
	getDependentFields : function(cmp,event,helper){
        console.log('recordTypeName:::::',cmp.get('v.recordTypeName'))
        cmp.set('v.spinner',true);
        var action = cmp.get('c.getDependentFieldSet');
        action.setParams({
            'fieldName' : 'Condition_Criteria__c',
            'recordTypeName' : cmp.get('v.recordTypeName')
        });
        action.setCallback(this,function(response){
            console.log(response);
            var state = response.getState();
            if(state == 'SUCCESS'){
                console.log(response.getReturnValue());
                cmp.set('v.conditionBasedField',response.getReturnValue().controllingPicklistValues);
                cmp.set('v.conditionBasedDependentMap',response.getReturnValue().dependentPicklistValues);
              //  helper.setDependentFields(cmp,event,helper);
                cmp.set('v.spinner',false);
            } else if(state == 'ERROR'){
                console.log(response.getError()[0].message);
            }
        });
        $A.enqueueAction(action);
    },
    setDependentFields : function(cmp,event,helper){
        if(!$A.util.isEmpty(cmp.get('v.rule').Condition_Field__c)){
            var selectedValue  = cmp.get('v.rule').Condition_Field__c;
            cmp.set('v.dependentList',cmp.get('v.conditionBasedDependentMap')[selectedValue]);
        }    	
    },
    openModal : function(cmp,event,helper,message){
        cmp.find('promt').set('v.content',message);
        cmp.find('promt').openModal();
    },
    deleteRuleDetailById : function(cmp,event,helper,ruleDetailId,isDateCalc){
        console.log(ruleDetailId,isDateCalc);
        var cmpEvent = cmp.getEvent("ruleDetailDelete");
        cmpEvent.setParams({
            'ruleDetailId' : ruleDetailId,
            'isDateCalc' : isDateCalc
        });
        cmpEvent.fire();
    }
})