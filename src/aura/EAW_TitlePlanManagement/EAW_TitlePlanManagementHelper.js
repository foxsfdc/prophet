({
	fieldSet: function(component) {
        try {
            var action = component.get("c.getFields");
            var strObjectName = component.get("v.strObjectName");
            var strFieldSetName = component.get("v.strFieldSetName");
            action.setParams({
                strObjectName: strObjectName,
                strFieldSetName: strFieldSetName
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === 'SUCCESS') {
                    var columns = [];

                    columns = response.getReturnValue().lstDataTableColumns;
                    component.set('v.myColumns', columns);
                    console.log('Columns:' + columns);
                    
                } else if (state === 'ERROR') {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " +
                                errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                } else {
                    console.log('Something went wrong, Please check with your admin');
                }
            });
            $A.enqueueAction(action);
        } catch (e) {
            console.log('Exception:' + e);
        }
    }
})