({
    handleSearchChange : function(component, event, helper) {
        var timer = component.get('v.timer');
        clearTimeout(timer);
        
        timer = setTimeout(function() {
            var newList = [];

            // Basically an event listener for searchPromise function
            Promise.all([helper.searchPromise(component, helper)])
                    .then(function(results) {
                        console.log(results);
                    }).catch(function(error) {
                        console.log(error);
                    });
            
            clearTimeout(timer);
            component.set('v.timer', null);
        }, 350);
        
        component.set('v.timer', timer);
    },
    
    addExistingPlanWindowGuidelines : function(component, event, helper) {
        
    	var planId = component.get('v.recordId');
        var allRecords = component.find("windowGuidelineSelect");
        var selectedWindowGuidelines = [];
        console.log('allRecords:::',allRecords);
        if(!Array.isArray(allRecords)){
            allRecords = [allRecords];
        }
        for(var key in allRecords) {
            if(allRecords[key].get("v.value")) {
                selectedWindowGuidelines.push(allRecords[key].get("v.text"));
            }
        }
        
        if(selectedWindowGuidelines.length > 0) {

            // List of Selected Window Guidelines are joined in a EAW_Plan_Window_Guideline_Junction__c to Plan
            var addSelectedWindowGuidelines = component.get("c.addSelectedExistingWindowGuidelines");
            addSelectedWindowGuidelines.setParams({windowGuidelineIds: selectedWindowGuidelines, planId: planId});
            
            addSelectedWindowGuidelines.setCallback(this, function(response) {
                var state = response.getState();
                
                if(state === 'SUCCESS') {
                    var results = response.getReturnValue();
                    
                    for(var result of results) {
	            		if(result.Customer__r) {
	            			result.customerName = result.Customer__r.Name;
	            		}
                        //LRCC-1493
	            		if(result.Window_Guideline_Alias__c != undefined) {
                            result.Name = result.Window_Guideline_Alias__c;
                        } else {
                            result.Name = ' ';
                        }
                        result.link = '/one/one.app?sObject/'+ result.Id + '/view#/sObject/' + result.Id + '/view';
		            }
                    
                    component.set("v.windowGuidelineList", results);

                    helper.hideAddPlanWindowGuidelineModal(component,helper);

                } else {
                	helper.errorToast('There was an issue saving the existing window guideline(s) to this plan.');
                }
            });
            $A.enqueueAction(addSelectedWindowGuidelines);
        } else {
            helper.errorToast('There are no existing window guidelines selected.');
        }
    },
    
    hideAddPlanWindowGuidelineModal : function(component, event, helper) {
        helper.hideAddPlanWindowGuidelineModal(component,helper);
    },
    openModal : function(cmp,event,helper){
        helper.openModal(cmp);
    }
})