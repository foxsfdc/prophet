({
	
	doInit : function(component, event, helper) {
		helper.fieldSet(component, helper); // In this method after the columns retrieved successfully we are calling for the records retrieve with in this helper method
    },
	
	checkItem : function(component, event, helper){
        
    	var id = event.getSource().get("v.text");
        
    },
    
    editGrid: function(component, event, helper) {

        var checked = component.get("v.checked");
    	component.set('v.displayEditButtons', checked);
        var editTableEvent = $A.get("e.c:EAW_EditTableEvent");
        editTableEvent.setParams({"editMode":checked});
        editTableEvent.fire();
    },
    
    resetColumn: function(component, event, helper) {
        // Get the component which was used for the mouse move
        if( component.get("v.currentEle") !== null ) {
            var newWidth = component.get("v.newWidth"); 
            var currentEle = component.get("v.currentEle").parentNode.parentNode; // Get the DIV
            var parObj = currentEle.parentNode; // Get the TH Element
            parObj.style.width = newWidth+'px';
            currentEle.style.width = newWidth+'px';
            console.log(newWidth);
            component.get("v.currentEle").style.right = 0; // Reset the column divided 
            component.set("v.currentEle", null); // Reset null so mouse move doesn't react again
        }
	},
	
	checkAll : function (component, event, helper) {
        var self = event.target;
        var checkboxes = component.find("row-checkbox");
        var selectedIds = new Array();
        if(checkboxes){
            if( Array.isArray(checkboxes)) {
        		for( var check of checkboxes){ 
	                check.getElement().checked = self.checked;
	                if( self.checked ) selectedIds.push(check.getElement().getAttribute('id'));
	            }
            } else {
            	checkboxes.getElement().checked = self.checked;
            	if( self.checked ) selectedIds.push(checkboxes.getElement().getAttribute('id'));
            }
            console.log('::::::selectedIds::::::::::'+selectedIds);
            if( self.checked ){
            	component.set("v.dataTableSelectedRows",selectedIds);
            } else {
            	component.set("v.dataTableSelectedRows",[]);
            }
        }
    },
    
    setNewWidth: function(component, event, helper) {
        var currentEle = component.get("v.currentEle");
        if( currentEle != null && currentEle.tagName ) {
            var parObj = currentEle;
            while(parObj.parentNode.tagName != 'TH') {
                if( parObj.className == 'slds-resizable__handle')
                    currentEle = parObj;    
                parObj = parObj.parentNode;
                count++;
            }
            var count = 1;
            var mouseStart = component.get("v.mouseStart");
            var oldWidth = parObj.offsetWidth;  // Get the width of DIV
            var newWidth = oldWidth + (event.clientX - parseFloat(mouseStart));
            component.set("v.newWidth", newWidth);
            currentEle.style.right = ( oldWidth - newWidth ) +'px';
            component.set("v.currentEle", currentEle);
        }
    },
    
    calculateWidth: function(component, event, helper) {
        
        var childObj = event.target
        var mouseStart=event.clientX;
        component.set("v.currentEle", childObj);
        component.set("v.mouseStart",mouseStart);
        // Stop text selection event so mouse move event works perfectlly.
        if(event.stopPropagation) event.stopPropagation();
        if(event.preventDefault) event.preventDefault();
        event.cancelBubble=true;
        event.returnValue=false;  
    },
	
	sort: function(component, event, helper) {
		console.log('::: Event Current Target:::'+event.currentTarget);
        var sortVal = event.currentTarget.dataset.value;
    	console.log('old value:'+sortVal);
        helper.sortBy(component,sortVal);
    },
    displayMassUpdate: function(component,event,helper){
        var selectedIds = new Array();
        
        var checkboxes = component.find("row-checkbox");
        console.dir(checkboxes);  
          
        if( checkboxes ){
        	if( Array.isArray(checkboxes)) {
        		for( var check of checkboxes){ 
	            	if( check.getElement().checked  ) {
	                	selectedIds.push(check.getElement().getAttribute('id'));
                	}
	            }
            } else {
            	if( checkboxes.getElement().checked  ) selectedIds.push(checkboxes.getElement().getAttribute('id'));
            }
        }
        console.log('::::::: SelectedIds ::::::::::');
        console.dir(selectedIds);
        if(selectedIds.length==0)
		{
			var toastEvent = $A.get("e.force:showToast");
			toastEvent.setParams({
			    title: "Error!",
			    message: $A.get("$Label.c.NoRecordsSelected"),
			    type: "warning"
				});
			toastEvent.fire();
		}
		else
		{
			component.set("v.dataTableSelectedRows",selectedIds);
			component.set('v.displayMassUpdate',true);
		}
    },
    createRecord : function (component, event, helper) 
    {
    	var createRecordEvent = $A.get("e.force:createRecord");
    	createRecordEvent.setParams({
    		"entityApiName": "EAW_Box_Office__c"
    	});
    	createRecordEvent.fire();
    },
    saveRecords: function(component,event,helper){
    	
    	component.set("v.showSpinner",true);
    	var myColumns = component.get("v.myColumns");
        var myData = component.get("v.boxOfficeData");

        var myDataOriginal = component.get("v.myDataOriginal");

        var updatedData = new Array();
        
        for(var i=0 ; i<myData.length ; i++){
            for(let column of myColumns){
                var data = myData[i];
                var dataOriginal = myDataOriginal[i];
                if(column.fieldName=='Territory__c'){
                	if(data[column.fieldName] && helper.collectNames(data[column.fieldName]) && helper.collectNames(data[column.fieldName])[0] && 
                				helper.collectNames(data[column.fieldName])[0]!= dataOriginal[column.fieldName]){
                		data.Territory__c=helper.collectNames(data[column.fieldName])[0];
                		updatedData.push(data);
	                    break;
                	}
                }else{
	                	if(data[column.fieldName] != dataOriginal[column.fieldName]){
	                	data.Territory__c=helper.collectNames(data['Territory__c'])[0];
	                    updatedData.push(data);
	                    break;
                	}
                }
                
            }
        }
		
        console.log('Records going for update : ' + JSON.stringify(updatedData));
        
        if( updatedData && updatedData.length > 0 ) {
        
        	var action = component.get("c.saveBoxOfficeRecords");
        	
            action.setParams({
                "updatedDataList" : JSON.stringify(updatedData)
            });
            
            action.setCallback(this, function(response) {
                var state = response.getState();
                var result = response.getReturnValue();
                if (state === 'SUCCESS') {
            		component.set("v.myDataOriginal", JSON.parse(JSON.stringify(component.get("v.boxOfficeData"))));
            		component.set("v.checked",false);
                    var checked = component.get("v.checked");
                    component.set('v.displayEditButtons', checked);
                    
                    var editTableEvent = $A.get("e.c:EAW_EditTableEvent");
                    editTableEvent.setParams({"editMode":checked});
                    editTableEvent.fire();
                    
                    var toastEvent = $A.get("e.force:showToast");
					toastEvent.setParams({
					    title: "Success!",
					    message: $A.get("$Label.c.RecordsSavedSuccessfully"),
					    type: "success"
					});
					toastEvent.fire();
                    
                } else if (state === 'ERROR') {
                	let mess = '';
                    console.log(JSON.stringify(response.getError()));
                    if(response.getError()){
                    	if(response.getError() && response.getError().length) {
                            for(let iresult of response.getError()) {
                                mess +=iresult.message;
                            }
                        }
                    }
                    helper.errorToast(mess);
                }
            });
            $A.enqueueAction(action);
            component.set("v.showSpinner",false);
        } else {
        	var toastEvent = $A.get("e.force:showToast");
				toastEvent.setParams({
				    title: "Info!",
				    message: $A.get("$Label.c.EAW_RecordsNotEdited"),
				    type: "info"
				});
				toastEvent.fire();
        }
    },
    cancel: function(component, event, helper) {
        
        component.set("v.boxOfficeData", JSON.parse(JSON.stringify(component.get("v.myDataOriginal"))));
        var checkAll = component.find('header-checkbox');
        if( checkAll ){
        	checkAll.getElement().checked = false;
        }
        helper.readOnlyMode(component, event, helper);
    }, 
    deleteRecords : function(component, event, helper){
    	var selectedIds = new Array();
    	var selectedPositions = new Array();
        var checkboxes = component.find("row-checkbox");
        console.dir(checkboxes);        
        if(checkboxes){
        	if( Array.isArray(checkboxes)) {
	        	for(let check of checkboxes){
	                var ele = check.getElement();
	                console.dir(ele);
	                if(ele.checked){
	                    selectedIds.push(ele.id);
	                    selectedPositions.push(ele.value);
	                }
	            }
            } else {
            	if( checkboxes.getElement().checked  ) {
            		selectedIds.push(checkboxes.getElement().getAttribute('id'));
            		selectedPositions.push(checkboxes.getElement().getAttribute('value'));
        		}
            }
        }
        console.log(selectedIds);
		if(selectedIds.length==0)
		{
			var toastEvent = $A.get("e.force:showToast");
			toastEvent.setParams({
			    title: "Error!",
			    message: $A.get("$Label.c.NoRecordsSelected"),
			    type: "warning"
				});
			toastEvent.fire();
		}
		else
		{
	        var action = component.get("c.deleteBoxOfficeRecords");
	        action.setParams({"recIds" : selectedIds});
	        action.setCallback(this, function(response)
	        {
	        	if(response.getState() === "SUCCESS")
	        	{
	        		
	        		selectedPositions.sort();
	        		var myDataOriginal = component.get('v.myDataOriginal');
	        		for(var i=selectedPositions.length-1;i>=0;i--){
	        			myDataOriginal.splice(selectedPositions[i],1);
	        		}
	        		if(myDataOriginal != undefined && myDataOriginal.length>0){
	        			component.set('v.boxOfficeData',JSON.parse(JSON.stringify(myDataOriginal)));
	        			component.set("v.myDataOriginal", JSON.parse(JSON.stringify(myDataOriginal)));
	        		}else{
	        			component.set('v.boxOfficeData','');
	        			component.set("v.myDataOriginal",'');
	        		}
	        		
	        		
	        		helper.readOnlyMode(component, event, helper);
	        		var toastEvent = $A.get("e.force:showToast");
					toastEvent.setParams({
					    title: "Success!",
					    message: $A.get("$Label.c.RecordsDeletedSuccessfully"),
					    type: "success"
						});
					toastEvent.fire();
	        	}
	        	else if (response.getState() === 'ERROR') 
	        	{
	                console.log(JSON.stringify(response.getError()));
	            }
	            console.log('success');
	        });
	        $A.enqueueAction(action);
        }
    },
    
    exportToCSV : function(component,event,helper){ // LRCC - 231
    	let data = component.get('v.myDataOriginal');
        if(data == null) {
            return;
        }
        let csv = helper.convertArrayToCSV(component, data,helper);

        var hiddenElement = document.createElement('a');
        hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
        hiddenElement.target = '_self'; //
        hiddenElement.download = 'Title Attribute-Boxoffice results.csv';  // CSV file Name* you can change it.[only name not .csv]
        document.body.appendChild(hiddenElement); // Required for FireFox browser
        hiddenElement.click();
    },
})