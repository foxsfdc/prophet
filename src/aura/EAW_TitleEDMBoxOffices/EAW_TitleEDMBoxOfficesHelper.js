({
	sortBy: function (component, field) {
        var sortAsc = component.get("v.sortAsc"),
            sortField = component.get("v.sortField"),
            records = component.get("v.boxOfficeData");
        sortAsc = field == sortField ? !sortAsc : true;
        records.sort(function (a, b) {
            var t1 = a[field] == b[field],
                t2 = a[field] > b[field];
            return t1 ? 0 : (sortAsc ? -1 : 1) * (t2 ? -1 : 1);
        });
        component.set("v.sortAsc", sortAsc);
        component.set("v.sortField", field);
        component.set("v.boxOfficeData", records);
        
        /****LRCC:887-Sorting indicator applied logic****/
        component.set("v.selectedTabsoft", field);
        if (sortAsc == true) {
            component.set("v.arrowDimyDataOriginalrection", 'arrowup');
        } else {
            component.set("v.arrowDirection", 'arrowdown');
        }
    },
    errorToast : function(message) {
        var toastEvent = $A.get('e.force:showToast');

        toastEvent.setParams({
            'title': 'Error!',
            'type': 'error',
            'message': message
        });

        toastEvent.fire();
    },
    successToast : function(message) {
        var toastEvent = $A.get('e.force:showToast');

        toastEvent.setParams({
            'title': 'Success!',
            'type': 'success',
            'message': message
        });

        toastEvent.fire();
    },
    readOnlyMode : function(component,event,helper){
    	component.set("v.checked",false);
        var checked = component.get("v.checked");
    	component.set('v.displayEditButtons', checked);
        
        var editTableEvent = $A.get("e.c:EAW_EditTableEvent");
        editTableEvent.setParams({"editMode":checked});
        editTableEvent.fire();
    },
    
    fieldSet: function (component, helper) {
        try {
            component.set("v.spinner", true);
            var action = component.get("c.getFields");
            action.setParams(
	            {
	                "objFieldSetMap": {
	                    "EAW_Box_Office__c": "Box_Office_Table"
	                }
	            }
	        );

            action.setCallback(this, function (response) {
                var state = response.getState();

                if (state === 'SUCCESS') 
                {
                    var columns = new Array();
                    console.dir(response.getReturnValue());
                    for(let col of response.getReturnValue())
                    {
                    	for(let colName of col){
                            //LRCC-1803
                            //if(colName.fieldName == "Territory__c"){
                                var colNameTemp =colName;
                                colNameTemp.width = '190';
                                columns.push(colNameTemp);   
                            //}else{
                               // columns.push(colName); 
                            //}
                            
                        }
                    }
                    component.set("v.myColumns", columns);
                    helper.retrieveTVDBoxOfficeData(component, helper);
                    component.set("v.spinner", false);
                } else if (state === 'ERROR') {
                    var errors = response.getError();

                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " +
                                errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                    component.set("v.spinner", false);
                } else {
                    console.log('Something went wrong, Please check with your admin');
                    component.set("v.spinner", false);
                }
                
            });
            $A.enqueueAction(action);
        } catch (e) {
            console.log('Exception:' + e);
            component.set("v.spinner", false);
        }
    },
    
    retrieveTVDBoxOfficeData : function( component, helper ){
    	
    	component.set("v.showSpinner",true);
    	component.set('v.checked',false);
    	var titleIds = [];
    	var titleIdFormattedString = '';
    	var titleRecordId = component.get("v.recordId");
    	console.log(titleRecordId);
    	
    	if( titleRecordId != null && titleRecordId != '' ){
    		
    		var action = component.get("c.searchForTVDBoxOffices");
        
	        action.setParams({"titleIdsString" : titleRecordId});
	        
	        action.setCallback(this, function(response) {
                var state = response.getState();
                var results= response.getReturnValue();
            	
                if(state === 'SUCCESS') {
                	
                	var data = new Array();
                    console.dir(response.getReturnValue());
                    component.set('v.titleAttributeName',response.getReturnValue().titleAttribute);
                    for(let col of response.getReturnValue().boxOffices)
                    {
                    	for(let colValue of col)
                    		data.push(colValue);
                    }
                    
                    var col = component.get('v.myColumns');
                    data = helper.getLookupFields(col, data, 'Name');
                    component.set("v.boxOfficeData", data);
                    component.set("v.myDataOriginal", JSON.parse(JSON.stringify(component.get("v.boxOfficeData"))));
                    
                } else if(state === 'ERROR') {
                	console.log('error');
                    //component.set('v.planData', []);
                    component.set('v.boxOfficeData', []);
                    component.set('v.myDataOriginal', []);
                }
                component.set("v.showSpinner",false);
            });
	        $A.enqueueAction(action);        
    		
    	} else {
    	
    	}
    },
    
    getLookupFields: function (col, data, fName) {
    
        for (var i = 0, clen = col.length; i < clen; i++) {
        
            if (col[i]['type'] == 'reference') {
            
                for (var j = 0, rlen = data.length; j < rlen; j++) {
                
                    var str = col[i]['fieldName'];
                    
                    if( str.indexOf('__c') != -1 ){
                    	str =  str.replace('__c', '__r');
                    } else{
                    	str =  str.replace('Id', '');
                    }
                    
                    var fValue = data[j][str];
                    
                    if (fValue != undefined) {
                        data[j][col[i]['fieldName']] = fValue[fName];
                    }
                }
            }
        }
        return data;
    },
    convertArrayToCSV : function(component,data,helper){
    	 var csvStringResult, counter, header, keys, columnDivider, lineDivider;

        // check if 'objectRecords' parameter is null, then return from function
        if(data == null || !data.length) {
            return null;
        }

        // store ,[comma] in columnDivider variabel for sparate CSV values and
        // for start next line use '\n' [new line] in lineDivider varaible
        columnDivider = ',';
        lineDivider =  '\n';

        // in the keys variable store fields API Names as a key
        // this labels use in CSV file header
        var columns = component.get('v.myColumns');
        header = [];
        keys =[];
        
        csvStringResult = 'Title Attribute Name : ' + component.get('v.titleAttributeName') + lineDivider  || '';
        
        for(var i=0;i<columns.length;i++){
        	header.push(columns[i].label);// this array is the 'pretty' column names for the csv
        	keys.push(columns[i].fieldName);// this array is the actual column name
        }
       
		 if(data.length) {
		 
			csvStringResult += lineDivider + 'Box Office Results' + lineDivider;
			
			csvStringResult += header.join(columnDivider);
			console.log(data[0])
			for(let j=0; j < data.length; j++) {
				csvStringResult += lineDivider ;
				counter = 0;
				
				for(var sTempkey in keys) {
				    var skey = keys[sTempkey];
				
				    // add , [comma] after every String value (except first)
					if(counter > 0){
					    csvStringResult += columnDivider;
					}
					if(columns[sTempkey].type == 'date' && !$A.util.isEmpty(data[j][skey])){
	            		var resultData = helper.getFormattedDate(data[j][skey]);
	            	} else{
	            		 var resultData = data[j][skey] || '';
	            	}
					csvStringResult += '"'+ resultData +'"';
			        counter++;
			    }
				//csvStringResult += lineDivider;
			}
		} else{
			csvStringResult +=  columnDivider +  'No Window Records' + lineDivider;
		}
		    	
        
        // return the CSV format String
        return csvStringResult;
    },
    getFormattedDate : function(dateToFormat){
    	var mydate = new Date(dateToFormat);
		var month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
		"Jul", "Aug", "Sept", "Oct", "Nov", "Dec"][mydate.getMonth()];
		return mydate.getDate() + ' - ' + month + ' - ' +  mydate.getFullYear();
    },
    collectNames: function (selectedResults) {

        var collectedNames = [];

        if (selectedResults && selectedResults.length > 0) {
            for (var result of selectedResults) {
                collectedNames.push(result.title); // The NAME is actually a column called title (it's weird)
            }
        }

        return collectedNames;
    }
})