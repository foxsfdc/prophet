({
    errorToast : function(message) {
    	var toastEvent = $A.get('e.force:showToast');
    		
		toastEvent.setParams({
			'title': 'Error!',
			'type': 'error',
			'message': message
		});
		
		toastEvent.fire();
    },
    
    successToast : function(message) {
    	var toastEvent = $A.get('e.force:showToast');
    		
		toastEvent.setParams({
			'title': 'Success!',
			'type': 'success',
			'message': message
		});
		
		toastEvent.fire();
    },
    planAndWindowJunction : function(component,event,helper,planId,windowGuidelineId,isSaveAndNew) {
        
        let action = component.get('c.createPlanWindowGuidelineJunction');
        action.setParams({planId: planId, windowGuidelineId: windowGuidelineId});
        action.setCallback(this, function (response) {
            let state = response.getState();
            
            if (state === 'SUCCESS') {
                let results = response.getReturnValue();
                let windowGuidelines = component.get('v.windowGuidelineData');
                
                if (results.length > 1) {
                    for (let result of results) {
                        
                        //LRCC-1493
                        if(result.Window_Guideline_Alias__c) {
                            result.Name = result.Window_Guideline_Alias__c;
                        }
                        result.link = '/one/one.app?sObject/' + result.Id + '/view#/sObject/' + result.Id + '/view';
                        windowGuidelines.push(result);
                    }
                } else {
                    let result = results[0];
                    
                    //LRCC-1493
                    if(result.Window_Guideline_Alias__c) {
                        result.Name = result.Window_Guideline_Alias__c;
                    }
                    
                    result.link = '/one/one.app?sObject/' + result.Id + '/view#/sObject/' + result.Id + '/view';
                    windowGuidelines.push(result);
                }
                
                component.set('v.windowGuidelineData', windowGuidelines);
                if(isSaveAndNew){
                    helper.openModal(component,event,helper);
                }
            } else {
                helper.errorToast('There was an issue creating a window guideline or the window guidelines association to the plan guideline.');
            }
        });
        $A.enqueueAction(action);
    },
    openModal : function(component,event,helper) {
        
        if(!component.get('v.addWindow')) {
            component.set('v.addWindow',true);
        }
        //window.setTimeout(function() {
            if(component.get('v.addWindow') && component.find('addWGModal')) {
                component.find('addWGModal').openModal();
            }
        //},1000);
    }
})