({
    getRulesAndDetails : function(cmp,event,helper,isOverridden){
        cmp.set('v.spinner',true);
        if(cmp.get('v.setBody')) cmp.set('v.setBody',false);
        var action = cmp.get('c.getRulesAndDetails');
        action.setParams({
            windowGuidelineId : cmp.get('v.recordId'),
            windowId : cmp.get('v.isWindow') ? cmp.get('v.windowRecordId') : null
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state == 'SUCCESS'){
                console.log(response.getReturnValue());
                cmp.set('v.isActive',response.getReturnValue().isActive);
                if(response.getReturnValue().previousVersion){ // LRCC - 1276
                    cmp.set('v.previousVersion',response.getReturnValue().previousVersion);
                }
                if(response.getReturnValue().ruleDetailList.length){
                    cmp.set('v.ruleDetailList',response.getReturnValue().ruleDetailList);
                }
                helper.setRulesBasedOnDates(cmp,event,helper,response.getReturnValue());
                if(response.getReturnValue().ruleList.length){
                    cmp.set('v.isNew',false);
                    if(!isOverridden){
                         cmp.set('v.edit',false);
                    }
                } else{
                    cmp.set('v.isNew',true);
                    if(!cmp.get('v.isWindow') && response.getReturnValue().isActive){
                        cmp.set('v.edit',false);
                    }
                }
                if(response.getReturnValue().releaseWindowDateWrapper.releaseDateGuidelines && response.getReturnValue().releaseWindowDateWrapper.windowGuidelines){
                	cmp.set('v.releaseDateGuidelineList',response.getReturnValue().releaseWindowDateWrapper.releaseDateGuidelines);
                    cmp.set('v.windowGuidelineList',response.getReturnValue().releaseWindowDateWrapper.windowGuidelines);
                    var releaseWindowDateWrapper = (response.getReturnValue().releaseWindowDateWrapper.releaseDateGuidelines).concat(response.getReturnValue().releaseWindowDateWrapper.windowGuidelines);
                    console.log('releaseWindowDateWrapper::',releaseWindowDateWrapper);
                    cmp.set('v.releaseWindowDateWrapper',releaseWindowDateWrapper);
                }
                if(cmp.get('v.isWindow') && response.getReturnValue().operandDateAndStatus){ // LRCC - 1259
                    cmp.set('v.operandDateAndStatus',response.getReturnValue().operandDateAndStatus);
                }
                cmp.set('v.setBody',true);
            	helper.setFOTM(cmp);
                if(cmp.get('v.isWindow') && cmp.find('accordion')){
                    cmp.find('accordion').set('v.activeSectionName',['startDate','endDate','trackingDate','outsideDate']);
                }
                cmp.set('v.spinner',false);
            } else if(state == 'ERROR'){
                console.log(response.getError()[0].message);
                cmp.set('v.spinner',false);
            }          
        });
        action.setBackground();
        $A.enqueueAction(action);
    },
    setFOTM : function(cmp){
       // console.log('::::::endDateRule:::::',cmp.get('v.windowGuidelineJSON').endDateRule)
    	if(cmp.get('v.windowGuidelineJSON').startDateRule && cmp.get('v.windowGuidelineJSON').startDateRule.First_of_the_Month__c && cmp.find('startDateFOTM')){
    		cmp.find('startDateFOTM').set('v.checked',true);
        } else if(cmp.find('startDateFOTM')){
            cmp.find('startDateFOTM').set('v.checked',false);
        }
    	if(cmp.get('v.windowGuidelineJSON').endDateRule && cmp.get('v.windowGuidelineJSON').endDateRule.First_of_the_Month__c && cmp.find('endDateFOTM')){
    		cmp.find('endDateFOTM').set('v.checked',true);
        } else if(cmp.find('endDateFOTM')){
            cmp.find('endDateFOTM').set('v.checked',false);
        }
    },
    setRulesBasedOnDates : function(cmp,event,helper,response){
        var ruleList = response.ruleList;
        var startDateRule =  response.ruleList.find(function(element) {
            return element.Date_To_Modify__c == 'Start Date';
        });
        var endDateRule =  response.ruleList.find(function(element) {
            return element.Date_To_Modify__c == 'End Date';
        });
        var trackingDateRule =  response.ruleList.find(function(element) {
            return element.Date_To_Modify__c == 'Tracking Date';
        });
        var outsideDateRule =  response.ruleList.find(function(element) {
            return element.Date_To_Modify__c == 'Outside Date';
        });
        var windowGuidelineJSON = {};
       // console.log(startDateRule,endDateRule)
        if(startDateRule){
            var startDateParentRuleDetailList = helper.setDetailsBasedOnRules(cmp,startDateRule,response.parentRuleDetailList);
            windowGuidelineJSON.startDateRule = startDateRule;
        } else{
            windowGuidelineJSON.startDateRule = JSON.parse(JSON.stringify(cmp.get('v.rule')));
        }
        windowGuidelineJSON.startDateRuleDetail = startDateParentRuleDetailList;
        
        if(endDateRule){
            var endDateRuleDetail = helper.setDetailsBasedOnRules(cmp,endDateRule,response.parentRuleDetailList);
        	windowGuidelineJSON.endDateRule = endDateRule;
        } else{
            windowGuidelineJSON.endDateRule = JSON.parse(JSON.stringify(cmp.get('v.rule')));
        }
        windowGuidelineJSON.endDateRuleDetail = endDateRuleDetail;

        if(trackingDateRule){
            var trackingDateRuleDetail = helper.setDetailsBasedOnRules(cmp,trackingDateRule,response.parentRuleDetailList);
       		windowGuidelineJSON.trackingDateRule = trackingDateRule;
        } else{
            windowGuidelineJSON.trackingDateRule = JSON.parse(JSON.stringify(cmp.get('v.rule')));
        }
        windowGuidelineJSON.trackingDateRuleDetail = trackingDateRuleDetail;

        if(outsideDateRule){
            var outsideDateRuleDetail = helper.setDetailsBasedOnRules(cmp,outsideDateRule,response.parentRuleDetailList);
        	windowGuidelineJSON.outsideDateRule = outsideDateRule;
        } else{
            windowGuidelineJSON.outsideDateRule = JSON.parse(JSON.stringify(cmp.get('v.rule')));
        }
        windowGuidelineJSON.outsideDateRuleDetail = outsideDateRuleDetail;
        console.log('windowGuidelineJSON:::',windowGuidelineJSON);
        cmp.set('v.windowGuidelineJSON',windowGuidelineJSON);
    },
    setDetailsBasedOnRules : function(cmp,ruleToSet,parentRuleDetailList){
        var ruleDetailList = [];
        var parentList = JSON.parse(JSON.stringify(parentRuleDetailList));
        for(var i in parentList){
            if(parentList[i].Rule_No__c == ruleToSet.Id){
                 ruleDetailList.push(parentList[i]);
            }
        }
        if(!Array.isArray(ruleDetailList)){
            ruleDetailList = [ruleDetailList];
        }
        return ruleDetailList;
    },
    saveRuleAndRuleDetailsNodes : function(cmp,event,helper,ruleList,ruleDetailContentList,ruleDetailToDelete,DetailJSONmap){
        cmp.set('v.spinner',true);
        var action = cmp.get('c.saveWindowRuleAndRuleDetails');
        cmp.get('v.rule').Release_Date_Guideline__c = cmp.get('v.recordId');
        //console.log(ruleDetailContent);
        var windowRecord = cmp.get('v.windowRecord').Id ? cmp.get('v.windowRecord') : null;
        var windowGuidelineId = cmp.get('v.isWindow') ? null : cmp.get('v.recordId');
        action.setParams({
            'ruleList' : JSON.stringify(ruleList),
            'ruleAndRuleDetailsNodes' : JSON.stringify(ruleDetailContentList),
            'ruleDetilsToDelete' : ruleDetailToDelete || null,
            'DetailJSONmap' : JSON.stringify(DetailJSONmap) || null,
            'currentWindowGuidelineId' : windowGuidelineId,
            'windowRecord' : windowRecord
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state == 'SUCCESS'){
                console.log('sucess');
                var message = 'Window Guideline was updated successfully.'
                if(cmp.get('v.isWindow')){
                     message = 'Window was updated successfully.'
                }
                if(ruleList.length){
                    var message = 'Window Rules were updated successfully';
                    if(cmp.get('v.isNew')){
                        var message = 'Window Rules were inserted succesfully';
                    }
                }
                helper.showToast(cmp,event,helper,state,message);
                cmp.set('v.edit',false);
                cmp.set('v.isNew',false);
                cmp.set('v.spinner',false);
                if(!cmp.get('v.isWindow')){
                    location.reload();
                } else{
                    cmp.set('v.setBody',false);
                    helper.getRulesAndDetails(cmp,event,helper,false);
                }
                
            } else if(state == 'ERROR'){
                console.log('error:::',response.getError());
                var errorMessage = helper.buildErrorMsg(cmp,event,helper,response.getError());
                helper.showToast(cmp,event,helper,state,errorMessage);
                cmp.set('v.spinner',false);
            }
        });
        $A.enqueueAction(action);
    },
    showToast : function(cmp, event, helper, type, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "type": type,
            "message": message
        });
        toastEvent.fire();
    },
    buildErrorMsg : function(cmp, event, helper, errors){
        var errorMsg = "";
        
        if(errors[0] && errors[0].message){  // To show other type of exceptions
            errorMsg = errors[0].message;
        }else if(errors[0] && errors[0].pageErrors.length) {  // To show DML exceptions
            errorMsg = errors[0].pageErrors[0].message; 
        }else if(errors[0] && errors[0].fieldErrors){  // To show field exceptions
            var fields = Object.keys(errors[0].fieldErrors);
            var field = fields[0];
            errorMsg = errors[0].fieldErrors[field];
            errorMsg = errorMsg[0].message;
        }else if(errors[0] && errors[0].duplicateResults.length){ // To show duplicateResults exceptions
            errorMsg = errors[0].duplicateResults[0].message;
        }
        
        return errorMsg;
    },
    setRuleDateToModify : function(cmp,rule,dateToModify){
        if(dateToModify == 'startDate'){
            rule.Date_To_Modify__c = 'Start Date';
            if(!cmp.get('v.isWindow')) rule.First_of_the_Month__c = cmp.find('startDateFOTM').get('v.checked');
        } else if(dateToModify == 'endDate'){
            rule.Date_To_Modify__c = 'End Date';
            console.log(':::endDate',cmp.find('endDateFOTM').get('v.checked'));
            rule.First_of_the_Month__c = cmp.find('endDateFOTM').get('v.checked');
        } else if(dateToModify == 'outsideDate'){
            rule.Date_To_Modify__c = 'Outside Date';
            rule.First_of_the_Month__c = cmp.find('outsideDateFOTM').get('v.checked');
        } else{
            rule.Date_To_Modify__c = 'Tracking Date';
            rule.First_of_the_Month__c = cmp.find('trackingDateFOTM').get('v.checked');
        }
        if(!cmp.get('v.isWindow')){
        	rule.Window_Guideline__c = cmp.get('v.recordId');
        } else{
        	rule.Window__c = cmp.get('v.windowRecordId');
        	if(!$A.util.isEmpty(rule.Id) && cmp.find(dateToModify) && (cmp.find(dateToModify).get('v.isOverridden') || rule.First_of_the_Month__c != cmp.find(dateToModify).get('v.originalRule').Rule_Overridden__c)){
        		rule.Rule_Overridden__c	 = true;
        	} else if($A.util.isEmpty(rule.Id)){
        		rule.Rule_Overridden__c	 = true;
        	}
        	rule.Window_Guideline__c = null;
        }
        return rule;
    },
    openModal : function(cmp,event,helper,message){
        cmp.find('promt').set('v.content',message);
        cmp.find('promt').openModal();
    },
    setDetailJSONString : function(date,ruleDetailJSON,rule,helper,DetailJSONmap){
        var JSONString = '';
        if(rule.Condition_Type__c ){
            if(rule.Condition_Type__c == 'if'){
                JSONString = 'Condition : '+ helper.setConditionNode(rule.Condition_Field__c,rule.Condition_Operand_Details__c,rule.Operator__c,rule.Criteria__c);
                for(var ruleDetailNode of ruleDetailJSON){
                    if(ruleDetailNode.ruleDetail.Condition_Type__c == 'If'){
                        JSONString += 'If TRUE then, ';
                    } else{
                        JSONString += 'If FALSE then, ';
                    }
                    if(ruleDetailNode.ruleDetail.Condition_Timeframe__c){
                        JSONString += helper.setEarliestLatestNode(ruleDetailNode,helper);
                    } else{
                        JSONString += helper.setDateNode(ruleDetailNode.ruleDetail);
                    }
                }
            } else if(rule.Condition_Type__c == 'case'){
                JSONString + 'Case Condition : ';
                for(var ruleDetailNode of ruleDetailJSON){
                    if(ruleDetailNode.ruleDetail.Condition_Type__c == 'Case then'){
                        JSONString += 'When - '+ helper.setCaseNode(ruleDetailNode,helper,true) + ' ';
                    } else {
                        JSONString += 'Otherwise - '+ helper.setCaseNode(ruleDetailNode,helper,false) + ' ';
                    }
                }
            } else if(rule.Condition_Type__c == 'TBD' || rule.Condition_Type__c == 'Perpetuity' || rule.Condition_Type__c == 'Rights End Date' ){
                JSONString += rule.Condition_Type__c +'; ';
            }
        }else{
            for(var ruleDetailNode of ruleDetailJSON){
                if(ruleDetailNode.ruleDetail.Condition_Timeframe__c){
                    JSONString += helper.setEarliestLatestNode(ruleDetailNode,helper);
                } else{
                    JSONString += 'Date Calculation : ' + helper.setDateNode(ruleDetailNode.ruleDetail);
                }
            }
        }
        if(rule.First_of_the_Month__c){
            JSONString += ' - FOTM;';
        }
        console.log(date+':::::'+JSONString);
        helper.setDateMap(date,JSONString,DetailJSONmap);
    },
    setConditionNode : function(conditionField,operandDetails,operator,criteria){
        return conditionField +' on '+operandDetails + ' '+operator +' '+ criteria ;
    },
    setCaseNode : function(node,helper,thenCase){
        var thisString = '';
        if(thenCase){
        	thisString += helper.setConditionNode(node.ruleDetail.Condition_Field__c,node.ruleDetail.Condition_Operand_Details__c,node.ruleDetail.Condition_Set_Operator__c,node.ruleDetail.Condition_Criteria__c) + ' Then :';
        }
        for(var detailNode of node.nodeCalcs){
            if(detailNode.ruleDetail.Condition_Timeframe__c){
                thisString += helper.setEarliestLatestNode(detailNode,helper);
            } else{
                thisString += helper.setDateNode(detailNode.ruleDetail);
            }
        }   
        return thisString;
    },
    setDateNode : function(node){
        if(!node.Condition_Date_Text__c){
            var thisString =  node.Condition_Met_Months__c + ' Months '+ node.Condition_Met_Days__c + ' Days of '+ node.Condition_Operand_Details__c;
            if(node.Window_Guideline_Date__c){
                thisString += ' on ' + node.Window_Guideline_Date__c;
            }
            if(node.Condition_Date_Duration__c){
                thisString += ' in ' + node.Condition_Date_Duration__c + ' ';
                if(node.Condition_Criteria_Amount__c){
                    thisString +=  node.Condition_Criteria_Amount__c + ' ';
                    if(node.Day_of_the_Week__c){
                        thisString += ' of ' + node.Day_of_the_Week__c + ' ';
                    }
                }
            }
        } else{
            var thisString = node.Condition_Date_Text__c;
        }
        return thisString + '; ';
    },
    setEarliestLatestNode : function(node,helper){
        var thisString = '';
        if(node.ruleDetail.Condition_Timeframe__c){
            if(node.ruleDetail.Condition_Timeframe__c == 'True_Earliest' || node.ruleDetail.Condition_Timeframe__c == 'False Earliest'){
                thisString = 'Earliest Of {';
            } else{
                thisString = 'Latest Of {';
            }
            if(node.dateCalcs.length){
                for(var dateCalc of node.dateCalcs){
                    thisString += helper.setDateNode(dateCalc);
                }
            }
            if(node.nodeCalcs.length){
                for(var detailNode of node.nodeCalcs){
                    thisString += helper.setEarliestLatestNode(detailNode,helper);
                }
            }
        } else if(node.ruleDetail.Parent_Condition_Type__c){
            if(node.ruleDetail.Parent_Condition_Type__c == 'Condition'){
                thisString = 'Condition : '+ helper.setConditionNode(node.ruleDetail.Condition_Field__c,node.ruleDetail.Condition_Operand_Details__c,node.ruleDetail.Condition_Set_Operator__c,node.ruleDetail.Condition_Criteria__c);
                for(var ruleDetailNode of node.nodeCalcs){
                    if(ruleDetailNode.ruleDetail.Condition_Type__c == 'If'){
                        thisString += 'If TRUE then, ';
                    } else{
                        thisString += 'If FALSE then, ';
                    }
                    if(ruleDetailNode.ruleDetail.Condition_Timeframe__c){
                        thisString += helper.setEarliestLatestNode(ruleDetailNode,helper);
                    } else{
                        thisString += helper.setDateNode(ruleDetailNode.ruleDetail);
                    }
                }
            } else if(node.ruleDetail.Parent_Condition_Type__c == 'Case'){
                thisString + 'Case Condition : ';
                for(var ruleDetailNode of node.nodeCalcs){
                    if(ruleDetailNode.ruleDetail.Condition_Type__c == 'Case then'){
                        thisString += 'When - '+ helper.setCaseNode(ruleDetailNode,helper,true) + ' ';
                    } else {
                        thisString += 'Otherwise - '+ helper.setCaseNode(ruleDetailNode,helper,false) + ' ';
                    }
                }
            }
        } 
        return thisString + '}';
    },
    setDateMap : function(date,JSONString,dateMap){
        if(date == 'startDate'){
            dateMap['Start Date'] = JSONString;
        } else if(date == 'endDate'){
            dateMap['End Date'] = JSONString;
        } else if(date == 'trackingDate'){
            dateMap['Tracking Date'] = JSONString;
        } else {
            dateMap['Outside Date'] = JSONString;
        }
    },
    getRuleId : function(cmp,ruleDate){
        if(ruleDate == 'Start Date'){
            return cmp.get('v.windowGuidelineJSON').startDateRule.Id;
        } else if(ruleDate == 'End Date'){
           return cmp.get('v.windowGuidelineJSON').endDateRule.Id;
        } else if(ruleDate == 'Outside Date'){
           return cmp.get('v.windowGuidelineJSON').outsideDateRule.Id;
        } else if(ruleDate == 'Tracking Date'){
           return cmp.get('v.windowGuidelineJSON').trackingDateRule.Id;
        }
    },
    overrideRule : function(cmp,event,helper,ruleDate,windowId,windowGuidelineId){
        cmp.set('v.spinner',true);
        var action = cmp.get('c.overriddenWindowRules');
        action.setParams({
            'windowGuidelineId' : windowGuidelineId,
            'windowId' : windowId,
            'dateToModify' : ruleDate
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state == 'SUCCESS'){
                var message =  'Default Window Rules are Overridden';
                helper.showToast(cmp,event,helper,state,message);
                cmp.set('v.windowGuidelineList',{});
                cmp.set('v.setBody',false);
                helper.getRulesAndDetails(cmp,event,helper,true);
            } else if(state == 'ERROR'){
                console.log('error:::',response.getError());
                var errorMessage = helper.buildErrorMsg(cmp,event,helper,response.getError());
                helper.showToast(cmp,event,helper,state,errorMessage);
                cmp.set('v.spinner',false);
            }
        });
		action.setBackground();
        $A.enqueueAction(action);
    },
    setNullDateRules : function(cmp,event,helper,JSONMap){
        var dateList = ['Start Date','End Date','Outside Date','Tracking Date'];
        for (var i of dateList){
            if(!JSONMap[i]){
                JSONMap[i] = '';
            }
        }
    },
    setNestedNodes : function(cmp,event,helper){
        var appEvent = $A.get("e.c:EAW_EarliestLatestNodes");
        appEvent.fire();
    }
})