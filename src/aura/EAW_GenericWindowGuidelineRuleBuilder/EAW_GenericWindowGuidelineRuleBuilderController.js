({
    init : function(cmp,event,helper){   
        helper.getRulesAndDetails(cmp,event,helper,false);
    },
    saveWindowRules : function(cmp,event,helper){
        var dateList = ['startDate','endDate','outsideDate','trackingDate'];
        var ruleList = [];
        var ruleDetailList = [];
        var ruleDetailToDelete = [];
        var temp = true;
        var validOne = false;
        var DetailJSONmap = {};
        var activeTabs = cmp.find('accordion').get('v.activeSectionName') || [];
        helper.setNestedNodes(cmp,event,helper);
        for(var i of dateList){
            var ruleDetailJSON = cmp.find(i).validRuleAndRuleDetails();
            console.log(i+':::::::::::::::'+ruleDetailJSON);
            if(ruleDetailJSON){
                console.log(i,':::::::valid::',ruleDetailJSON);
                if(ruleDetailJSON != true && ruleDetailJSON.length ){
                    var rule = helper.setRuleDateToModify(cmp,cmp.find(i).get('v.rule'),i);
                    ruleList.push(rule);
                    ruleDetailList.push(ruleDetailJSON);
                    helper.setDetailJSONString(i,JSON.parse(JSON.stringify(ruleDetailJSON)),JSON.parse(JSON.stringify(rule)),helper,DetailJSONmap); // LRCC - 1256
                    if(cmp.find(i).get('v.ruleDetailstoDelete').length){
                        if(ruleDetailToDelete.length){
                            ruleDetailToDelete.concat(cmp.find(i).get('v.ruleDetailstoDelete'));
                        }
                        ruleDetailToDelete = cmp.find(i).get('v.ruleDetailstoDelete');
                    }
                } 
                validOne = true;
            } else{
                temp = false;
                if(activeTabs.indexOf(i) == -1){
                    activeTabs.push(i);	
                }
            }
        }
        if(temp){
            console.log('ruleList:::',ruleList);
            console.log('ruleDetailList::::',ruleDetailList);
            console.log('ruleDetailToDelete::::',ruleDetailToDelete);
            helper.setNullDateRules(cmp,event,helper,DetailJSONmap);
            console.log('DetailJSONmap::::::::::',DetailJSONmap);
            console.log('window:::',cmp.get('v.windowRecord'));
            helper.saveRuleAndRuleDetailsNodes(cmp,event,helper,ruleList,ruleDetailList,ruleDetailToDelete,DetailJSONmap);
            
        } else{
            if(validOne){
                cmp.set('v.ruleListToSave',ruleList);
                cmp.set('v.ruleDetailListAToSave',ruleDetailList);
                cmp.set('v.ruleDetailToDelete',ruleDetailToDelete);
                console.log(activeTabs)
                cmp.find('accordion').set('v.activeSectionName',activeTabs);
                helper.showToast(cmp,event,helper,'Warning','Provide valid values for Rules');
               // helper.saveRuleAndRuleDetailsNodes(cmp,event,helper,ruleList,ruleDetailList,ruleDetailToDelete);
                //helper.openModal(cmp,event,helper,'There are some InValid Rules. Want to Save the Valid Rules ?');
            } else{
                console.log('toast');
                cmp.find('accordion').set('v.activeSectionName',dateList);
                helper.showToast(cmp,event,helper,'error','No Rules are Valid');
            }
        }
    },
    proceedToSave : function(cmp,event,helper){
        console.log('ruleList:::',cmp.get('v.ruleListToSave'));
        console.log('ruleDetailList::::',cmp.get('v.ruleDetailListAToSave'));
        console.log('ruleDetailToDelete::::',cmp.get('v.ruleDetailToDelete'));
        helper.saveRuleAndRuleDetailsNodes(cmp,event,helper,cmp.get('v.ruleListToSave'),cmp.get('v.ruleDetailListAToSave'),cmp.get('v.ruleDetailToDelete'));
        cmp.set('v.ruleListToSave',[]);
        cmp.set('v.ruleDetailListAToSave',[]);
        cmp.set('v.ruleDetailToDelete',[]);
    },
    restoreRule : function(cmp,event,helper){
        var ruleDate = event.getSource().get('v.name');
        helper.overrideRule(cmp,event,helper,ruleDate,cmp.get('v.windowRecordId'),cmp.get('v.recordId'));
    },
    setFirstOfMonth : function(cmp,event,helper){
        console.log('::::::',event.getSource().get('v.checked'));
        console.log('::::::',event.getSource().get('v.name') );
    	if(event.getSource().get('v.name') == 'Start Date'){
            if(event.getSource().get('v.checked')){
                cmp.get('v.windowGuidelineJSON').startDateRule.First_of_the_Month__c = true;
            } else {
                cmp.get('v.windowGuidelineJSON').startDateRule.First_of_the_Month__c = false;
            }
    	} else if(event.getSource().get('v.name') == 'End Date'){
            if(event.getSource().get('v.checked')){
                cmp.get('v.windowGuidelineJSON').endDateRule.First_of_the_Month__c = true;
            } else{
                cmp.get('v.windowGuidelineJSON').endDateRule.First_of_the_Month__c = false;
            }
        }
       //cmp.set('v.windowGuidelineJSON',windowGuideline);
        console.log(cmp.get('v.windowGuidelineJSON').startDateRule.First_of_the_Month__c);
    },
    closeWindowModal : function(cmp, event) {
        
        var cmpEvent = cmp.getEvent("closeModal");
        cmpEvent.fire();
    },
    setDependentManuals : function(cmp,event){
        var windowRecord = JSON.parse(JSON.stringify(cmp.get('v.windowRecord')));
        var manual = event.getSource().get('v.name');
        if(manual == 'manualStartDate'){
            windowRecord.Start_Date_Rule__c = '';
        } else if(manual == 'startDateText'){
            windowRecord.Manual_Start_Date__c = null;
        } else if(manual == 'manualEndDate'){
            windowRecord.End_Date_Rule__c = '';
        } else if(manual == 'endDateText'){
            windowRecord.Manual_End_Date__c = null;
        }
        cmp.set('v.windowRecord',windowRecord);
    },
   // <!--LRCC:707-->
    rowAction : function(component, event, helper) {
    	if(component.get('v.edit')){
	    	var recId = event.currentTarget.dataset.value;
	        
	        var notesEvent = $A.get("e.c:EAW_NotesEvent");
	        notesEvent.setParams({"parentId":recId});
	        notesEvent.fire();
	    }
    }
})