({
    addPlan : function(component, event, helper) {
        
        let selectedTitles = component.get('v.selectedTitles');
        let selectedPlanGuidelines = component.get('v.selectedPlanGuidelines');
		let newNote = component.get('v.body');
        let title = component.get('v.title');
        console.log('title::::',title);
        //LRCC-1459
        let pageNumber = component.get('v.pageNumber');
        let colWithAlpha = component.get('v.colWithAlpha');
        let pageIdMapUI = component.get('v.pageIdMapUI')|| JSON.stringify({});
        
        
        if(selectedTitles.length === 1 && selectedPlanGuidelines.length === 1 && title != null && title != '') {
        
            let titleId = selectedTitles[0].id;
            let planGuidelineId = selectedPlanGuidelines[0].id;
            let action = component.get('c.createPlan');
            action.setParams({
                titleId: titleId,
                planGuidelineId: planGuidelineId,
                title: title,
                body: newNote,
                flag: true,
                pagesPerRecord:100,
                currentPageNumber :pageNumber,
                pageIdMapUI :pageIdMapUI,
                selectedFilterMap :  JSON.stringify(colWithAlpha)
                
            });
			
            action.setCallback(this, function (response) {
                
                let state = response.getState();
                
                if(state === 'SUCCESS') {
                	
                    let results = response.getReturnValue();

                    if(results && results != null) {
                    
                        let resultData = component.get('v.resultData');
                        let titlePlans = [];

                        //TODO: Refactor this list view setup
                        let something = results.length - 1;
                        console.log(results)
                        console.log(resultData)

                        for(let key in results) {
                            let isNewPlan = resultData.findIndex(titlePlan => titlePlan.TitlePlan.Id != key) !== -1;

                            if(results[key].length > 1 && isNewPlan) {
                            	let windows = [];

                                for(let i = 1; i < results[key].length; i++) {
                                    let window = results[key][i];
                                    window.Title = results[key][0].Name;
                                    window.link = '/one/one.app?sObject/'+ window.Id + '/view#/sObject/' + window.Id + '/view';
                                    window.FIN_PROD_ID__c = results[key][0].FIN_PROD_ID__c;
                                    window.PROD_TYP_CD_INTL__c = results[key][0].CurrencyIsoCode;
                                    window.FIN_DIV_CD__c = results[key][0].DIR_NM__c;
                                    window.PlanName = results[key][0].Acquired_Indicator__c;
                                    window.TitleTags = results[key][0].Approx_Run_Time__c;
                                    window.isChecked = false;

                                    windows.push(window);
                                }

                                if(windows.length > 0) {
                                    results[key][0].TitleId = results[key][0].Id;
                                    results[key][0].Id = key;
                                    
                                    titlePlans.push({TitlePlan: results[key][0], Windows: windows, showWindows: false});
                                }
                            } else {
                                results[key][0].TitleId = results[key][0].Id;
                                results[key][0].Id = key;

                                titlePlans.push({TitlePlan: results[key][0], Windows: [], showWindows: false});
                            }
                        }
                        
                        if(titlePlans.length > 0) {
                            for(let titlePlan of titlePlans) {
                                resultData.push(titlePlan);
                            }

                            console.log(resultData)

                            component.set('v.resultData', resultData);
                        }
                        /**LRCC:787**/
                        let cmpEvent = component.getEvent("cmpEvent");
                        cmpEvent.fire();
                        
                        helper.clearInputsAndHide(component);
                        helper.successToast('Title Plan successfully created.');
                    } else {
                        helper.errorToast('You can not add a duplicate Plan or Windows to a Title.');
                	}
    			} 
            	else if(state == 'ERROR') {
            		let errors = response.getError();
            		var handleErrors = helper.handleErrors(errors, component, event, helper);
                    if(handleErrors) {
                        helper.errorToast(handleErrors);
                    }
				} 
            	else {
                    helper.errorToast('There was an issue creating the plan, window, and window strands for the selected title plan.');
				}
			});
            $A.enqueueAction(action);
        } 
        else {
            if (selectedTitles.length === 0 && selectedPlanGuidelines.length === 0) {
                helper.errorToast('You must select a Title and Plan Guideline to generate a Title Plan.');
            } else {
                let label = $A.get("$Label.c.EAW_NotesSubjectValidation");
				helper.errorToast(label);
            }
        }
    },

    hidePopup : function(component, event, helper) {
        helper.clearInputsAndHide(component);
    }
})