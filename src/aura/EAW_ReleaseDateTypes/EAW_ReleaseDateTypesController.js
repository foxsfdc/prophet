({
	init : function(component, event, helper) {
		var action=component.get("c.getAllTypes");  
        action.setCallback(this,function(response){
            var state = response.getState();
            var result= response.getReturnValue();
            if(state === 'SUCCESS'){
                console.log(result);
                if(result){
                    component.set('v.data',result);
                }
                else{
                    component.set('v.data',[]);
                }
            }
            else if(state === 'ERROR'){
                console.log(response.getError());   
                component.set('v.data',[]);
            }
        });
        $A.enqueueAction(action);
	},
    createRecord : function (component, event, helper) {
        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            "entityApiName": "EAW_Release_Date_Type__c"
        });
        createRecordEvent.fire();
    }
    
})