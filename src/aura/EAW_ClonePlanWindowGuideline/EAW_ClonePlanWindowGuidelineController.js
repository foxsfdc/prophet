({
    cloneWindowGuideline : function(component, event, helper) {
        
        let selectedRows = component.get('v.windowGuidelineSelect');

        if(selectedRows.length === 1) {
            let planId = component.get('v.planRecordId');
            //LRCC-1560 - Replace Customer lookup field with Customer text field
            //let customerName = 'Customer__c' in selectedRows ?  selectedRows[0].Customer__r.Name : null;
            let customerName = 'Customers__c' in selectedRows ?  selectedRows[0].Customers__c : null;
            let cloneSelectedWindowGuidelines = component.get("c.cloneSelectedWindowGuidelines");
            cloneSelectedWindowGuidelines.setParams({oldWindowGuideline: selectedRows[0], planId: planId});

            cloneSelectedWindowGuidelines.setCallback(this, function(response) {
                let state = response.getState();
                
                if(state === 'SUCCESS') {
                    let result = response.getReturnValue();
                    let windowGuidelines = component.get('v.windowGuidelineList');

                    if(customerName) {
                        result.customerName = customerName;
                    }
                    //LRCC-1493
                    if(result.Window_Guideline_Alias__c != undefined) {
                        result.Name = result.Window_Guideline_Alias__c;
                    } else {
                        result.Name = ' ';
                    }
                    
                    result.link = '/one/one.app?sObject/'+ result.Id + '/view#/sObject/' + result.Id + '/view';
                    windowGuidelines.push(result);

                    component.set('v.windowGuidelineList', windowGuidelines);

                    let navigateEvent = $A.get('e.force:navigateToSObject');
                    navigateEvent.setParams({
                        'recordId': result.Id
                    });

                    navigateEvent.fire();
                } 
                else if(state === 'ERROR') {
                	let errors = response.getError();
                	let handleErrors = helper.handleErrors(errors, component, event, helper);
                    if(handleErrors) {
                        helper.errorToast(handleErrors);
                    }
                }
                else {
                    helper.errorToast('There was an issue cloning the selected Window Guideline.');
                }
            });
            $A.enqueueAction(cloneSelectedWindowGuidelines);
        } else if(selectedRows.length === 0) {
            helper.errorToast('There are no Window Guidelines selected for cloning for this plan.')
        } else {
            helper.errorToast('You can only clone one Window Guideline at a time.');
        }
    },

    /** Methods below hide/show the popup*/
    showPopup : function(component, event, helper) {
        document.getElementById("eaw-clone-plan-window-guideline-modal").style.display = "block";
    },
    hidePopup : function(component, event, helper){
        document.getElementById("eaw-clone-plan-window-guideline-modal").style.display = "none";
    }
})