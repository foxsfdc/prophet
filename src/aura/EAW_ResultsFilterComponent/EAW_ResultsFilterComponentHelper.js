({    
    collectNames : function(selectedResults, isTag, isPicklist) {
        
        var collectedNames = [];
        
        if(selectedResults && selectedResults.length > 0) {
            for(var result of selectedResults) {
                if(isTag) {
                    collectedNames.push({'label' : result.Name, 'value' : result.Id});
                } else if(isPicklist) {
                    collectedNames.push({'label' : result, 'value' : result});
                } else {
                    collectedNames.push({'label' : result.Name, 'value' : result.Name});
                }
            }
        }
        console.log('collectNames:::',collectedNames);
        return collectedNames;
    },
    
    setFilterActions : function(columns) {
        
        for(let col of columns) {
            
            var filterActions = [];
            
            if(col.type == 'date') {
                filterActions.push({'label' : '= equal', 'value' : ' = '});
                filterActions.push({'label' : '< less', 'value' : ' < '});
                filterActions.push({'label' : '> greater', 'value' : ' > '});
                filterActions.push({'label' : '! not equal', 'value' : ' != '});
            } else if(col.fieldName.includes('Tag')) {
                filterActions.push({'label' : '~ contains', 'value' : ' ~ '});
            } else if(col.fieldName == 'License_Info_Codes__c') {
                filterActions.push({'label' : '~ contains', 'value' : ' ~ '});
                filterActions.push({'label' : '= equal', 'value' : ' = '});
            } else if(col.type == 'picklist') {
                filterActions.push({'label' : '= equal', 'value' : ' = '});
            } else {
                filterActions.push({'label' : '~ contains', 'value' : ' ~ '});
                filterActions.push({'label' : '= equal', 'value' : ' = '});
                filterActions.push({'label' : '^ begins with', 'value' : ' ^ '});
            }
            col.filterActions = filterActions;
            col.selectedAction = filterActions[0].value;
        }
    },
    
    applyFilter : function(component, event) {
        
        var selectedCol;
        var type;
        var selectedAction;
        var value;
        var colAplpha = {};
        var allFilterElements = component.find('filterElement');
        var invalidDate = false;
        
        for(let filterElement of allFilterElements) {
            
            selectedCol = filterElement.get('v.label') ? filterElement.get('v.label').fieldName : filterElement.get('v.infoCodeObject').fieldName;
            type = filterElement.get('v.label') ? filterElement.get('v.label').type : filterElement.get('v.infoCodeObject').type;
            selectedAction = filterElement.get('v.label') ? filterElement.get('v.label').selectedAction : filterElement.get('v.infoCodeObject').selectedAction;
            value = (selectedCol == 'License_Info_Codes__c' ? component.get('v.selectedList') : filterElement.get('v.value'));
            
            if((selectedCol == 'License_Info_Codes__c' && value.length) || (selectedCol != 'License_Info_Codes__c' && value)) {                
                if(selectedAction == ' ~ ') {
                    if(selectedCol.includes('Tag')) {
                        value = '\'' + value + '\'';
                    } else if(selectedCol == 'Customers__c') {
                        value = 'contains,' + value;
                    } else if(selectedCol == 'License_Info_Codes__c') {                        
                        value = this.infoCodesChange(component);                        
                        value = ' INCLUDES (\'' + value + '\')';
                    } else {
                        if(value.indexOf("'") > -1){
                            var tempvalue = escape(value);
                           value =  tempvalue//.replace(/%27/g, "~~");
                            console.log("valueLLL",value)
                        }
                        value = ' LIKE \'%' + value + '%\'';
                    }
                } else if(selectedAction == ' ^ ') {
                    if(selectedCol == 'Customers__c') {
                        value = 'begins,' + value;
                    } else {
                        value = ' LIKE \'' + value + '%\'';
                    }
                } else {
                    if(type == 'date' || selectedCol == 'Retired__c' || selectedCol == 'Is_Confidential_Release_Date__c' || selectedCol == 'Is_Confidential_Window__c') {
                        if(type == 'date' && (new Date(value) == 'Invalid Date')) {
                            invalidDate = true;
                        } else {
                            value = '' + selectedAction + value;
                        }
                    } else if(selectedCol == 'Customers__c') {
                        value = 'equal,' + value;
                    } else if(selectedCol == 'License_Info_Codes__c') { 
                        value = this.infoCodesChange(component);
                        value = '' + selectedAction + '\'' + value + '\'';
                    } else {
                        value = '' + selectedAction + '\'' + value + '\'';
                    }
                }
                colAplpha[selectedCol] = value; 
            } else {
                delete colAplpha[selectedCol];
            }            
        }
        
        component.set('v.colWithAlpha',colAplpha);
        component.set('v.pageNumber', 1);
        component.set("v.pageIdMap",{});
        
        var isReleaseDateResults = component.get('v.isReleaseDateResults');
        var pageConfigEvt = isReleaseDateResults ? component.getEvent("pageConfigEvt") : component.getEvent("pageConfigEvtPlans");
        console.log('before event call::::',JSON.stringify(component.get('v.colWithAlpha')));  
        console.log(';;;;',component.get("v.pagesPerRecod"));
        pageConfigEvt.setParams({"pagesPerRecord" : component.get("v.pagesPerRecod"),
                                 "currentPageNumber":1,
                                 "selectedFilterMap" : component.get('v.colWithAlpha') });
        if(invalidDate) {
            
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Error!",
                "message": "Please Enter Valid Date Format.",
                "type": "error"
            });
            toastEvent.fire();
        } else {            
            pageConfigEvt.fire();
        }
        
    },
    
    infoCodesChange : function(component) {
        
        var windowLicenseInfoCodes = component.get('v.selectedList'); 
        var colWithAlpha = component.get('v.colWithAlpha');        
        var tempWindowLicenseInfoCodes = [];
        
        windowLicenseInfoCodes.forEach(function (element) {            
            tempWindowLicenseInfoCodes.push(element.value);
        });
        return tempWindowLicenseInfoCodes.join(';');
    }
})