({
clearSearch: function (component, event, helper) {
		
        component.set('v.selectedMedias', []);
        component.set('v.selectedTerritories', []);
        component.set('v.selectedLanguages', []);
        component.set('v.status', '--None--');
		
        component.set('v.selectedPlans', []);
        component.set('v.selectedWindows', []);
        component.find('windowType').set('v.value', '');
        component.find('windowStatus').set('v.value', '');
        //LRCC-1731
        component.find('windowStatus').set('v.selectedList', []);
        //component.find('windowStatus').reload();
        component.set('v.selectedWindowTags', []);
        
         //LRCC-1519
        component.find('windowType').set('v.value', '');
        //LRCC-1731
        component.find('windowType').set('v.selectedList', []);
        //component.find('windowType').reload();

        component.find('windowDateStart').set('v.value', '');
        component.find('windowDateEnd').set('v.value', '');

        component.find('windowEndDateStart').set('v.value', '');
        component.find('windowEndDateEnd').set('v.value', '');

        component.find('windowOutsideDateStart').set('v.value', '');
        component.find('windowOutsideDateEnd').set('v.value', '');
        component.find('windowRightStatus').set('v.value', '');
        //LRCC-1731
        component.find('windowRightStatus').set('v.selectedList', []);
        //component.find('windowRightStatus').reload();
        component.find('windowLicenseInfoCodes').set('v.value', '');
        //LRCC-1731
        component.find('windowLicenseInfoCodes').set('v.selectedList', []);
        //component.find('windowLicenseInfoCodes').reload();

        component.find('releaseDateStart').set('v.value', '');
        component.find('releaseDateEnd').set('v.value', '');
        //LRCC-1557
        component.set('v.selectedReleaseDateTypes', []);
        //component.find('releaseDateType').set('v.value', '');
        component.find('releaseDateStatus').set('v.value', '');
        //LRCC-1731
        component.find('releaseDateStatus').set('v.selectedList', []);
        //component.find('releaseDateStatus').reload();
        component.set('v.selectedReleaseDateTags', []);
        //component.set('v.resultData',[]); // To clear the results.// LRCC - 1208
        component.set('v.selectedTitles', []); // LRCC - 1208
    },
    
    //LRCC-1628
    onSelectInfoCodeSearchType : function (component, event, helper) {
        component.set('v.infoCodeSearchType',event.getSource().get("v.label"));
    },
    
    //LRCC:1019
    toggelSearchCritira: function (component, event, helper) {

        var buttonstate = component.get('v.SearchContainerState');
        console.log('buttonstate:::', buttonstate);
        component.set('v.SearchContainerState', !buttonstate);
        var ceb = document.getElementById("collapseandexpandBtn");
        if (ceb.style.display === "none") {
            ceb.style.display = "block";
        } else {
            ceb.style.display = "none";
        }
        var cef = document.getElementById("collapseandexpandfilter");
        if (cef.style.display === "none") {
            cef.style.display = "block";
        } else {
            cef.style.display = "none";
        }
    },
    doInit: function (component, event, helper) {

        //LRCC:1022 start
        helper.showSpinner(component);
        var action = component.get("c.getAllPickListValues");

        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.pickListMap", JSON.parse(response.getReturnValue()));
                console.log('pickListMap:::', JSON.parse(JSON.stringify(component.get("v.pickListMap"))));
                component.set("v.callChild", true);
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

        $A.enqueueAction(action);
        helper.hideSpinner(component);
        // LRCC:1022 end

        component.set('v.SearchContainerState', true);
        if (document.getElementById("collapseandexpandBtn") && document.getElementById("collapseandexpandBtn")) {

            var ceb = document.getElementById("collapseandexpandBtn");
            ceb.style.display = "block";

            var cef = document.getElementById("collapseandexpandfilter");
            cef.style.display = "block";
        }
	},
    //LRCC:1262
    searchTitlePlansAndWindows: function (component, event, helper) {
        helper.searchTitlePlansAndWindows(component, event, helper);
    },
    /*
    proceedToSearch: function (component, event, helper) {

        let searchFlag = component.get('v.SearchFlag');

        if (event.getParam("componentFlag") == 'EAW_TitlePlanDateManagement') {
            let flag = event.getParam("isProceed");
            if (flag) {
                if ('releasedate' == searchFlag) {
                    helper.searchReleaseDates(component, event, helper);
                } else if ('plan' == searchFlag) {
                    helper.searchTitlePlansAndWindows(component, event, helper);
                }
                component.set('v.showConfrimBox', false);
                component.find('showconfirmmodal').close();
            } else {
                component.find('showconfirmmodal').close();
                component.set('v.showConfrimBox', flag);
            }
        }
    },
    */
    checkChangesForReleaseDateSearch: function (component, event, helper) {
         //LRCC-1885
        component.set('v.releaseDateSearchWindowHeader',[]);
        component.set('v.releaseDateSearchWindowValues',[]);
        component.set('v.windowSearchReleaseDateHeader',[]);
        component.set('v.windowSearchReleaseDateValues',[]);
        
       //     <!-- LRCC:1459 -->
		console.log('event.getParam(selectedFilterMap)::::::',event.getParam());
        component.set('v.SearchFlag', 'releasedate');
        console.log(event.getParam('currentPageNumber'),event.getParam('pagesPerRecord'),component.get("v.pageIdMap"));
        var editable = component.get('v.editMode');
        var saved = component.get('v.saveMode');
        let selectedFilterMap = null;
        
        if(event.getParam('selectedFilterMap') && event.getParam('selectedFilterMap').selectedCol != '' && event.getParam('selectedFilterMap').selectedAlpha != ''){
             selectedFilterMap = JSON.stringify(event.getParam('selectedFilterMap'));
        }
        if(!event.getParam('isDeleted') && editable == true && saved == false) {            	
            component.find("modalCmpparent").open();
        }
        else {
            if(event.getParam('currentPageNumber') && event.getParam('pagesPerRecord')){
                
                 component.set("v.childrefresh",event.getParam('currentPageNumber'));
                 //LRCC-1517 && event.getParam('selectedFilterMap').selectedCol != null is removed from below condition
                 if(event.getParam('selectedFilterMap') != null ||event.getParam('isDeleted')){
                  component.set('v.totalRecordCount',null);
                 }
                  helper.checkChangesInTabelData(component, event, helper, 'releasedate',event.getParam('pagesPerRecord'),event.getParam('currentPageNumber'),JSON.stringify(component.get("v.pageIdMap")),selectedFilterMap);
            } else {
                
                //LRCC-1517
                if(component.find('titlePlanDateResults') && component.find('titlePlanDateResults').find('titleReleaseDateResults')) {
                    component.find('titlePlanDateResults').find('titleReleaseDateResults').set('v.buttonstate', false);
                }
            	
                component.set("v.childrefresh",1);
                component.set('v.totalRecordCount',null);
                //LRCC-1795
                helper.checkChangesInTabelData(component, event, helper, 'releasedate',50,1,JSON.stringify({}));
            }
        }        
    },
    closeModalParent: function(component, event, helper) {
    	component.find("modalCmpparent").close();
        component.set('v.editMode', true);
        var editTableEvent = $A.get("e.c:EAW_EditTableEvent");
        editTableEvent.setParams({"editMode":true});
        editTableEvent.fire();
       
	},
    saveModalParent : function(component, event, helper) {
        var myDataOriginal = component.get('v.myDataOriginal');
        component.find("modalCmpparent").close();
        component.set('v.editMode', false);
        var editTableEvent = $A.get("e.c:EAW_EditTableEvent");
        editTableEvent.setParams({"editMode":false});        
        editTableEvent.fire();
        component.set('v.resultData', JSON.parse(JSON.stringify(myDataOriginal))); 
            
	},
    checkChangesForPlansAndWindows: function (component, event, helper) {
         //LRCC-1885
        component.set('v.releaseDateSearchWindowHeader',[]);
        component.set('v.releaseDateSearchWindowValues',[]);
        component.set('v.windowSearchReleaseDateHeader',[]);
        component.set('v.windowSearchReleaseDateValues',[]);
        
        component.set('v.SearchFlag', 'plan');
        var editable = component.get('v.editMode');
        var saved = component.get('v.titlesavemode');
        
        //LRCC-1459
        let selectedFilterMap = null;
        if(event.getParam('selectedFilterMap') && 
           event.getParam('selectedFilterMap').selectedCol != '' && 
           event.getParam('selectedFilterMap').selectedAlpha != '') {
            
            selectedFilterMap = JSON.stringify(event.getParam('selectedFilterMap'));
        }
       
        
        //LRCC-1390 
        component.set("v.EventName",event.getName());
        //
        if(!event.getParam('isDeleted') && editable == true && saved == false && (event.getName() != 'cmpEvent') && (event.getName() != 'pageConfigEvtPlans')) {   
           
            component.find("modalCmpparent").open();
        } else {
            if(event.getParam('currentPageNumber') && event.getParam('pagesPerRecord')){
                
                component.set("v.childrefresh",event.getParam('currentPageNumber'));
                helper.checkChangesInTabelData(component, event, helper, 'plan',event.getParam('pagesPerRecord'),event.getParam('currentPageNumber'),JSON.stringify(component.get("v.pageIdMap")),selectedFilterMap);
            } else {
                
                //LRCC-1517
                if(component.find('titlePlanDateResults') && component.find('titlePlanDateResults').find('planWindowResults')) {
                    component.find('titlePlanDateResults').find('planWindowResults').set('v.buttonstate', false);
                }
                
                component.set("v.childrefresh",1);
                //LRCC-1795
                helper.checkChangesInTabelData(component, event, helper, 'plan',50 , 1, JSON.stringify({}));
            }
            //helper.checkChangesInTabelData(component, event, helper, 'plan');
        }        
    },
     //LRCC-1675
    exportSearchResult: function (component, event, helper) {
        
        console.log('resultColumns::::::::',event.getParam('resultColumns'));
        console.log('eventFlag::::::::',event.getParam('eventFlag'));
        
        component.set('v.resultColumns',event.getParam('resultColumns'));
        var eventflag = event.getParam('eventFlag');
        if(eventflag == 'window'){
            
             helper.exportWindows(component, event, helper);  
        }else if(eventflag == 'releasedate'){
              console.log('eventFlag::::export:cc:::',event.getParam('eventFlag'));
         	 helper.exportReleaseDate(component, event, helper);     
        }
        
    },

})