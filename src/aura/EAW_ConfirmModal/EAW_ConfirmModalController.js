({
    cancel : function(component, event, helper) {
        
        
       if (component.get('v.componentFlag') == 'EAW_TitleReleaseDateResult') {
       
    	   	let changeEventTitleReleaseDateResult = component.getEvent('checkChangesConfirmEventTitleReleasedate');
	        changeEventTitleReleaseDateResult.setParams({'isProceed':false,'componentFlag':component.get('v.componentFlag')});
	        changeEventTitleReleaseDateResult.fire();
        }else{
        
        	let changeEvent = component.getEvent('checkChangesEvent');
	        changeEvent.setParams({'isProceed':false,'componentFlag':component.get('v.componentFlag')});
	        changeEvent.fire();
        }
	        
        
         		
    },
    ok : function(component, event, helper) {
    
    	console.log('ok::xx:::::::::::::::::::',component.get('v.componentFlag'));
        if (component.get('v.componentFlag') == 'EAW_TitleReleaseDateResult') {
       
    	   	let changeEventTitleReleaseDateResult = component.getEvent('checkChangesConfirmEventTitleReleasedate');
	        changeEventTitleReleaseDateResult.setParams({'isProceed':true,'componentFlag':component.get('v.componentFlag')});
	        changeEventTitleReleaseDateResult.fire();
        }else{
        
	        console.log('ok:::::::::::::::::::::',component.get('v.componentFlag'));
	        let changeEvent = component.getEvent('checkChangesEvent');
	        changeEvent.setParams({'isProceed':true,'componentFlag':component.get('v.componentFlag')});
	        changeEvent.fire();
        }
    },
     openModal : function(component, event, helper) {
     
    	 helper.openModal(component, event, helper);
        
    },
    closeModal : function(component, event, helper) {
        
       helper.closeModal(component, event, helper);
        
    },
})