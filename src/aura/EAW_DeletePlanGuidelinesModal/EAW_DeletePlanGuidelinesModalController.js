({
    deleteDraft : function(component, event, helper) {
        var planData = component.get("v.planData");
        var selectedRows = component.get("v.selectedRows");
        var action = component.get("c.deleteDraftPlans");
        action.setParams({"plans" : selectedRows});

        action.setCallback(this, function(response) {
            var state = response.getState();
            var results = response.getReturnValue();

            if(state === 'SUCCESS') {
                for(var result of results) {
                    var index = planData.findIndex(item => item.Id == result.Id);

                    if(index != -1) {
                        planData.splice(index, 1);
                    }
                }

                component.set("v.planData", planData);
                helper.successToast('The plan(s) with a status of draft has/have been deleted.');
            } else {
                helper.errorToast('The plan(s) with a status of draft has/have not been deleted.');
            }

        });
        $A.enqueueAction(action);

        helper.hideDeleteModal();
    },
    
    hideDeleteModal : function(component, event, helper) {
        helper.hideDeleteModal();
    }
})