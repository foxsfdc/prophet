({
	setFieldValues : function(cmp,event,helper) {
        console.log('jh')
        var fieldValueList = [];
        for(var field of cmp.get('v.recordFields')){
            var fieldNode = {
                'fieldName' : field,
                'value' : cmp.get('v.releaseDateGuideline')[field]
            };
            fieldValueList.push(fieldNode);
        }
        console.log('::',fieldValueList);
        cmp.set('v.fieldValueList',fieldValueList);
	},
     toast : function(type,message) {
    	var toastEvent = $A.get('e.force:showToast');
    	toastEvent.setParams({
			'type': type,
			'message': message
		});
		toastEvent.fire();
    }, 
})