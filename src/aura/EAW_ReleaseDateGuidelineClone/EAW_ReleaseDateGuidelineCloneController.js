({
	init : function(cmp, event, helper) {
		cmp.set('v.spinner',true);
        var action = cmp.get('c.getReleaseDateGuideline');
        action.setParams({
            'recordId' : cmp.get('v.recordId')
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state == 'SUCCESS'){
                var RGrecord = response.getReturnValue();
                if(RGrecord.Id)delete RGrecord.Id;
                 if(RGrecord.Active__c)RGrecord.Active__c = true; // LRCC - 1580
                cmp.set('v.releaseDateGuideline',RGrecord);
                helper.setFieldValues(cmp,event,helper);
                cmp.set('v.showModal',true);
            } else if(state == 'ERROR'){
                console.log('error::',response.getError().message);
            }
        });
        cmp.set('v.spinner',false);
        $A.enqueueAction(action);
	},
    handleLoad : function(cmp){
        
    },
     handleSubmit : function(component ,event,helper) {
        component.set('v.spinner',true);
        event.preventDefault(); // stop form submission
        var eventFields = event.getParam("fields");
         if(!$A.util.isEmpty(eventFields['Name'])) {
             //eventFields['Name'] = component.get('v.releaseDateGuideline').Name + ' clone';
             eventFields['All_Rules__c'] = component.get('v.releaseDateGuideline').All_Rules__c;
             console.log(eventFields);
             component.find('recordForm').submit(eventFields);
         } else {
             helper.toast('error','Release Date Guideline Name was required');
         }
          component.set('v.spinner',false);
    },
    handleSuccess : function(cmp,event,helper) {
        
        var newReleaseDateGuideline = event.getParams().response.id;
        console.log('::::::::',newReleaseDateGuideline);
        cmp.set('v.spinner',true);
        var action = cmp.get('c.cloneRuleAndRuleDetails');
        action.setParams({
            'oldRG' : cmp.get('v.recordId'),
            'newRG' : newReleaseDateGuideline
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state == 'SUCCESS'){
                console.log('success');
                var message = cmp.get('v.releaseDateGuideline').Name + ' was cloned sucessfully';
                helper.toast(state,message);
            } else {
                helper.toast(state,response.getError()[0].message);
            }
            var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId": newReleaseDateGuideline
            });
            navEvt.fire();
            cmp.set('v.spinner',false);
        });
        $A.enqueueAction(action);
    },
    closeModal : function(cmp){
        cmp.set('v.showModal',false);
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    }
})