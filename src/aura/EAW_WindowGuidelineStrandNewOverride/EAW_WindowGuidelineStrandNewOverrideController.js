({
    init : function(cmp, event, helper) {
        cmp.set('v.spinner', true);
        cmp.set('v.showModal', true);
        if(cmp.get('v.windowGuidelineId')) {
            var windowGuideline = {'icon':'standard:account','id':cmp.get('v.windowGuidelineId'),'sObjectType':'EAW_Window_Guideline__c','title':cmp.get('v.windowGuidelineName')};
            var selectedWindowGuidelines = cmp.get('v.selectedWindowGuidelines');
            selectedWindowGuidelines.push(windowGuideline);
            cmp.set('v.selectedWindowGuidelines',selectedWindowGuidelines);
        }
        helper.setFieldValues(cmp, event, helper);
        cmp.set('v.spinner', false);
    },
    handleSubmit : function(component ,event,helper) {
        component.set('v.spinner',true);
        event.preventDefault(); // stop form submission
        var eventFields = event.getParam("fields");
        console.log('eventFields::',JSON.parse(JSON.stringify(eventFields)));        
        var canSubmit = true;
        var inputFields = component.find('inputFields');
        
        if(component.get('v.selectedWindowGuidelines') && component.get('v.selectedWindowGuidelines').length == 0) {
            canSubmit = false;
            $A.util.addClass(component.find('genericCmp'), ' requiredBorder'); 
        }
        
        for(var i=0; i< inputFields.length; i++) {
            console.log('fF::',inputFields[i].get('v.fieldName'));
            if(inputFields[i].get('v.fieldName') == 'License_Type__c' && ! eventFields.License_Type__c) {
                canSubmit = false;
                console.log('fffff');
                $A.util.addClass(inputFields[i], ' requiredBorder');                
            }
        }
        
        if(canSubmit) {
            let data = JSON.parse(JSON.stringify(eventFields));
            data['EAW_Window_Guideline__c'] = component.get('v.selectedWindowGuidelines')[0].id;
            component.find('recordForm').submit(data);
        } 
        component.set('v.spinner',false);        
    },
    handleSuccess : function(cmp,event,helper) { 
        
        var newWindowGuidelineStrand = event.getParams().response.id;
        
        var base_url = window.location.origin;
        if(cmp.get('v.windowGuidelineId')) {
            //sforce.one.navigateToSObject(cmp.get('v.windowGuidelineId'), 'related');
            location.replace(base_url+"/lightning/r/EAW_Window_Guideline__c/"+cmp.get('v.windowGuidelineId')+"/view");
        } else {
            //sforce.one.navigateToSObject(newWindowGuidelineStrand, 'related');
            location.replace(base_url+"/lightning/r/EAW_Window_Guideline_Strand__c/"+newWindowGuidelineStrand+"/view");
        }
        cmp.set('v.spinner',false);        
    },
    closeModal : function(cmp){
        cmp.set('v.showModal',false);
        var base_url = window.location.origin;
        if(cmp.get('v.windowGuidelineId')) {
            //sforce.one.navigateToSObject(cmp.get('v.windowGuidelineId'));
            location.replace(base_url+"/lightning/r/EAW_Window_Guideline__c/"+cmp.get('v.windowGuidelineId')+"/view");
        } else {
            //sforce.one.navigateToSObject(newWindowGuidelineStrand);
            location.replace(base_url+"/lightning/o/EAW_Window_Guideline_Strand__c/list?filterName=Recent");
        }
    }
})