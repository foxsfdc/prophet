({
	init : function(cmp, event, helper) {
        event.stopPropagation();
        if(cmp.get('v.node')){
            var nodeCalc = cmp.get('v.node');
            console.log(JSON.stringify(nodeCalc));
            if(nodeCalc.ruleDetail && nodeCalc.ruleDetail.Id && nodeCalc.ruleDetail.Parent_Condition_Type__c){
                helper.setParentNode(cmp,event,helper,nodeCalc);
            } else if(nodeCalc.ruleDetail.Parent_Condition_Type__c == 'Condition'){
                helper.setConditionBasedNode(cmp,event,helper,null,null,nodeCalc);
                cmp.set('v.selectedContext','condition');
            } else{
                 helper.setCaseBasedNode(cmp,event,helper,null,null,nodeCalc);
                cmp.set('v.selectedContext','multidate');
            }
            console.log('::node::'+JSON.stringify(nodeCalc));
            //cmp.set('v.node',nodeCalc);
        }
	},
})