({
	saveSalesRegionRecord : function(component, event, helper) {
        console.log('helper::::');
        
        let territoryList = component.get('v.selectedTerritories');
        let territoryStr;
        
        for(let i = 0 ; i < territoryList.length;i++){
            
        	let str = territoryList[i];
            
            if(!territoryStr){
            
                territoryStr  = str.title + ';';
            }else{
                if(i < territoryList.length -1){
                    
                    territoryStr  = territoryStr + str.title+';';
                }else {
                    
                     territoryStr  = territoryStr + str.title;
                }
            }
        }
        
		 var action = component.get("c.saveSalesRegions");
        action.setParams({"salesRegion" :component.get('v.selectedSalesRegions')[0].title,
                          "territoryList": territoryStr});
        action.setCallback(this, function(response) {
            
            let results = response.getReturnValue();
            console.log('inserted::',results);
            if(results != null) {
                if(results.indexOf('Error :') == -1){
                    
                	helper.successToast(results+' is successfully Created.');
                }else{
                    helper.errorToast(results);
                }
                component.set('v.selectedTerritories',[]);
                component.set('v.selectedSalesRegions',[]);
                
            } 
        });
        $A.enqueueAction(action);

	}, 
    errorToast : function(message) {
        
        var toastEvent = $A.get('e.force:showToast');

        toastEvent.setParams({
            'title': 'Error!',
            'type': 'error',
            'message': message
        });

        toastEvent.fire();
    },
     successToast : function(message) {
        var toastEvent = $A.get('e.force:showToast');

        toastEvent.setParams({
            'title': 'Success!',
            'type': 'success',
            'message': message
        });

        toastEvent.fire();
    },
    init : function(component, event, helper) {
    
       
        var temp ;
	    var action = component.get("c.autoSelected");
	    if(component.get('v.selectedSalesRegions').length > 0 ){
	    
	    	temp = component.get('v.selectedSalesRegions')[0].title;
	    	
	    }else {
	    
	    	temp = '';
	    }
	    
        action.setParams({"selectedSalesRegionStr" :temp});
        action.setCallback(this, function(response) {
            
            let results = response.getReturnValue();
            console.log('inserted:cc:',results);
            
            if(results != null) {
            
                component.set('v.selectedTerritories',results);
            } 
        });
        $A.enqueueAction(action);

	}, 
})