//This file was referenced from https://github.com/pozil/sfdc-ui-lookup
({
    generateFilteredRecordView : function(component, helper, filteredRecords) {
        var label = component.get('v.label');
        var unorderedList = component.find('listBoxUL').getElement();
        component.set('v.searchResults', filteredRecords.length);
        unorderedList.innerHTML = '';
        console.log('fff::::',filteredRecords);
        for(var filteredRecord of filteredRecords) {
            console.log(';;filteredRecord.title;;',filteredRecord.title);
            var spanText = document.createElement('span');
            spanText.setAttribute('class', 'slds-listbox__option-text slds-listbox__option-text_entity');
            spanText.setAttribute('tabindex', '1');
            spanText.innerHTML = filteredRecord.title;
            spanText.title = filteredRecord.title;
            
            var spanMeta = document.createElement('span');
            spanMeta.setAttribute('class', 'slds-listbox__option-meta slds-listbox__option-meta_entity');
            if(filteredRecord.subtitle){
                spanMeta.innerHTML = filteredRecord.subtitle;
            }
            
            var spanBody = document.createElement('span');
            spanBody.setAttribute('class', 'slds-media__body');
            spanBody.appendChild(spanMeta);
            spanBody.appendChild(spanText);
            
            var spanMedia = document.createElement('span');
            spanMedia.setAttribute('id', filteredRecord.id);
            spanMedia.setAttribute('role', 'option');
            spanMedia.setAttribute('class', 'slds-media slds-listbox__option slds-listbox__option_entity slds-listbox__option_has-meta');
            spanMedia.onclick = (component, helper, filteredRecords, function(e) {
                const recordId = e.currentTarget.id;
                helper.selectResult(component, recordId, filteredRecords);
            });
            spanMedia.appendChild(spanBody);
            
            var listItem = document.createElement('li');
            listItem.setAttribute('role', 'presentation');
            listItem.setAttribute('class', 'slds-listbox__item');
            listItem.appendChild(spanMedia);
            
            unorderedList.appendChild(listItem);
        }
    },
    
    updateSearchTerm : function(component, helper, searchTerm) {
        const updatedSearchTerm = searchTerm.trim().replace(/\*/g);
        const curSearchTerm = component.get('v.searchTerm');
        if(curSearchTerm !== updatedSearchTerm) {
            var totalRecords = component.get('v.totalRecords');
            //var selectedIds = helper.getSelectedIds(component); LRCC-1098
            var selectedIds = component.get("v.selectedIds");
            var obtainedIds = component.get("v.checkedIds");
            console.log('<<obtainedIds>>'+obtainedIds);
            component.set('v.searchTerm', updatedSearchTerm);
            var existingList = component.get('v.tableData');
            console.log('updatedSearchTerm::',updatedSearchTerm,updatedSearchTerm.length);
            if(updatedSearchTerm.length < 1) {
                console.log('here')
                var filteredRecords = [];
                
                if(component.get("v.checkconflict")) {
                    for(var i = 0; i < totalRecords.length; i++) {
                        var isExisting = false;
                        /*if(selectedIds != 'undefined') { 
                            if(selectedIds.indexOf(totalRecords[i].id) == -1) {
                                filteredRecords.push(totalRecords[i]);
                            }
                        } else {
                             filteredRecords.push(totalRecords[i]);
                        }*/
                        
                        if(existingList.length) {
                            existingList.find(function(element) {
                                if(element.Id == totalRecords[i].id) { 
                                    isExisting = true;                                
                                }                          
                            });
                        } 
                        if(!isExisting) {
                            filteredRecords.push(totalRecords[i]);    
                        }
                    }
                }
                else {
                //LRCC:1151
                    var  totalRecords = helper.duplicateFilter(component);
                    var filteredRecords = totalRecords.filter((record) => {
                        if(!(selectedIds.indexOf(record.id) >= 0)) {
                        return record;
                    }
                  });
                }
                console.log('filter in searching::',filteredRecords);
               helper.generateFilteredRecordView(component, helper, filteredRecords);
            } else {
                console.log('Event fire');
                const searchEvent = component.getEvent('onSearch');
                searchEvent.fire();
            }
        }
    },
    
    selectResult : function(component, recordId, filteredRecords) {
        const selectedResult = filteredRecords.filter(result => result.id === recordId);
        console.log('selectedResult:::::::',selectedResult,':::',selectedResult[0]);
        if(selectedResult.length > 0) {
            const selection = component.get('v.selection');
            selection.push(selectedResult[0]);
            component.set('v.selection', selection);
        }
        console.log('selection:::::::::',component.get('v.selection'));
        
        const searchInput = component.find('searchInput');
        
        var label = component.get('v.label');
        //change made for Mass Update
        //var unorderedList = document.getElementById(label + '_listBoxIteration');
        var unorderedList = component.find('listBoxUL').getElement();
        unorderedList.innerHTML = '';
        
        searchInput.getElement().value = '';
        component.set('v.searchTerm', '');
        //LRCC:1232 start
        var searchEvent = component.getEvent("searchEvent");
        searchEvent.setParams({
            "searchTerm" : ''
        });
        searchEvent.fire();
        //LRCC:1232 end
    },
    
    getSelectedIds : function(component) {
        const selection = component.get('v.selection');  
        return selection.map(element => element.id);
    },
    
    removeSelectedItem : function(component, removedItemId) {
        const selection = component.get('v.selection');        
        const updatedSelection = selection.filter(item => item.id !== removedItemId);
        component.set('v.selection', updatedSelection);
    },
    
    clearSelection : function(component, itemId) {
        component.set('v.selection', []);
    },
    
    isSelectionAllowed : function(component) {
        return component.get('v.isMultiEntry') || component.get('v.selection').length === 0;
    },
    
    search : function(component) {
        // Fetch title data off of inputted titles
        var selectedTitles = component.get("v.selection");
        var action = component.get("c.getTitleAttributes");
        
        action.setParams({"titleIdsString" : this.collectIds(selectedTitles)});
        action.setCallback(this, function(response) {
            
            let results = response.getReturnValue();
            console.log('Response in helper::',results);
            if(results != null) {
                
                var existingList = component.get('v.tableData');
                console.log(':::***:::existingList:::',existingList.length);
                var idsSelected = component.get("v.selectedIds");
                console.log(idsSelected);
                console.log('results::::',results);
                for(let result of results) {
                    
                    result.Title_Name = result.Name__c;
                    result.Name__c = '/one/one.app?sObject/'+ result.Id + '/view#/sObject/' + result.Id + '/view';
                    //1095 & 1250-Replace Title EDM with Title Attribute
                    if(result.FIN_PROD_ID__c) {
                    	result.Title_Id = result.FIN_PROD_ID__c;
                    }
                    
                    if(result.PROD_TYP_CD_INTL__r && result.PROD_TYP_CD_INTL__r.Name) {
                        result.PROD_TYP_CD_INTL = result.PROD_TYP_CD_INTL__r.Name;
                    }
                    
                    if(result.PROD_TYP_CD_INTL__r && result.PROD_TYP_CD_INTL__r.Name) {
                        result.PROD_TYP_CD = result.PROD_TYP_CD_INTL__r.Name;
                    }
                    
                    if(result.FIN_DIV_CD__r && result.FIN_DIV_CD__r.Name) {
                        result.FIN_DIV_CD = result.FIN_DIV_CD__r.Name;
                    }
                    
                    if(result.First_Release_Date__c) {
                        result.FRST_REL_DATE__c = result.First_Release_Date__c;
                    }
                    
                    //if(result.Title_EDM__r && result.Title_EDM__r.DIR_NM__c) {
                        //result.DIR_NM__c = result.Title_EDM__r.DIR_NM__c;
                    //}
                    
                    var isExisting = false;
                    if(existingList.length) {
                        existingList.find(function(element) {
                            if(element.Id == result.Id) { 
                                isExisting = true;                                
                            }                          
                        });
                    } 
                    if(!isExisting){
                        existingList.push(result);                        
                    }
                    console.log('<<*idsSelected*>>'+idsSelected);
                }
                console.log('existingList:XXyyyy::',JSON.stringify(existingList));
                component.set("v.selectedIds", idsSelected);
                
                component.set("v.tableData", existingList);
                component.set('v.selection',[]);
            } 
        });
        $A.enqueueAction(action);
    },
    
    collectIds : function(selectedResults) {
        var collectedIds = [];
        var collectedIdsString = "";
        
        if(selectedResults && selectedResults.length > 0) {
            for(var result of selectedResults) {
                collectedIds.push(result.id);
            }
        }
        
        for(var id of collectedIds){
            collectedIdsString += id + ' ';
        }
        
        return collectedIdsString; 
    },
    //LRCC:1045
    duplicateFilter : function(component){
    
        var existingList = component.get('v.totalRecords');
        var selectedList = component.get('v.selection');
        
			var fliteredList = [];
			var selectedListNames = [];
			var isExisting = false;
			for(let j = 0;j < selectedList.length; j++){
			    selectedListNames.push(selectedList[j].id); 
			}
			for(let i =0 ;i < existingList.length;i++){
			  var element = existingList[i]; 
			  	if(selectedListNames.indexOf( existingList[i].id) == -1) { 
					fliteredList.push(existingList[i]); 
				}
			}
			return fliteredList;
    }
})