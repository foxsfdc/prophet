({
	afterRender: function (cmp,helper) {
        
        this.superAfterRender();
        
        helper.windowClick = $A.getCallback(function(event){
            if(cmp.isValid()){
                if(event && event.target && event.target.value == undefined){
                	 cmp.set('v.hasFocus',false);    
                }
            }
        });
        
        document.addEventListener('click',helper.windowClick);
    }
})