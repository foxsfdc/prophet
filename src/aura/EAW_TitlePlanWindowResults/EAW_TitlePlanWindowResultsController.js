({
    fireComponentEvent : function(component, event, helper) {
        var cmpEvent = component.getEvent("cmpEvent");
        cmpEvent.fire();
	},
    
    //LRCC-1517
    handleFilter : function(component, event, helper) {
        
        if(component.get("v.colWithAlpha").selectedCol){
            helper.showErrorToast(component,'Alphabetical Search Option is Enabled');
            return;
        } 
        
        var buttonstate = component.get('v.buttonstate');
        component.set('v.buttonstate', !buttonstate);
        
        if(! component.get('v.buttonstate')) {
            var pageConfigEvtPlans = component.getEvent("pageConfigEvtPlans");
            pageConfigEvtPlans.setParams({"pagesPerRecord" : component.get("v.pagesPerRecod"),
                                     "currentPageNumber": 1,
                                     "selectedFilterMap" : null});
            pageConfigEvtPlans.fire();
        }
    },
    
	init : function(component, event, helper) {
        console.log(':::::::FieldSet Start Time::::'+new Date());
		//component.set('v.initialTableShow',true);
		//LRCC-1633
		if(component.find('select-all-plans-checkbox') && component.find('select-all-plans-checkbox').getElement() && component.find('select-all-plans-checkbox').getElement().checked == true) {
			component.find('select-all-plans-checkbox').getElement().checked = false;
		}
		if(component.find('select-all-windows-checkbox') && component.find('select-all-windows-checkbox').getElement() && component.find('select-all-windows-checkbox').getElement().checked == true){
			component.find('select-all-windows-checkbox').getElement().checked = false;
		}
		
    	component.set('v.showSpinner',true); 
        
        var params = event.getParam('arguments');
        if(params){
            var param1 = params.childrefresh;
            component.set("v.pageNumber", param1);
        }
        
        
        
        helper.fieldSet(component,event, helper);
    	
        //helper.clearUnsavedChanges(component, event, helper);
    },
    
    toggleWindowVisibility : function(component, event, helper) {
		console.log('called');
        var noOfOpenWindows = component.get('v.noOfOpenWindows') || 0;
        if(event.getSource().get('v.alternativeText') == 'right'){
            noOfOpenWindows = noOfOpenWindows + 1;
        } else{
            noOfOpenWindows = noOfOpenWindows - 1;
        }
        component.set('v.noOfOpenWindows',noOfOpenWindows);
        //LRCC-1459
        let titlePlans = component.get('v.rowsToDisplay') == null ? [] : component.get('v.rowsToDisplay');
            //component.get('v.resultData') == null ? [] : component.get('v.resultData');
        let titlePlanId = event.target.id == null ? '' : event.target.id;
        
        //TODO: simplify this logic into like a two liner with an arrow function
        for(let i = 0; i < titlePlans.length; i++) {
            if(titlePlans[i].TitlePlan.Id === titlePlanId) {
                titlePlans[i].showWindows = !titlePlans[i].showWindows;
                break;
            }
        }
        console.log('titlePlans',titlePlans)
        //component.set('v.resultData', titlePlans);
        //LRCC-1459
        component.set('v.rowsToDisplay', titlePlans);
        component.set('v.titlePlanId',titlePlanId);
        
        let isEditable = component.get('v.isEditable'); 					// LRCC - 1120
        let editTableEvent = $A.get('e.c:EAW_EditTableEvent');
        editTableEvent.setParams({
            'editMode': isEditable
        });
        editTableEvent.fire();
    },
	
    checkAllPlans : function(component, event, helper) {
        helper.selectAllPlans( component, event, helper );
    },
	
    checkAllWindows : function(component, event, helper) {
    	component.set('v.showSpinner',true);
        helper.selectAllWindows( component, event, helper );
        let isEditable = component.get('v.isEditable'); 					// LRCC - 1120
        let editTableEvent = $A.get('e.c:EAW_EditTableEvent');
        editTableEvent.setParams({
            'editMode': isEditable
        });
        editTableEvent.fire();
        component.set('v.showSpinner',false);
    },
	
    updateSelectedTitlePlans : function(component, event, helper) {
        let selectedTitlePlans = component.get('v.selectedTitlePlans');
        let self = event.target;
		
	    if(self.checked) {
	        selectedTitlePlans.push(self.value);
        } else {
            selectedTitlePlans.splice(selectedTitlePlans.indexOf(self.value), 1);
        }
	
        component.set('v.selectedTitlePlans', selectedTitlePlans);
    },

    updateSelectedWindows : function(component, event, helper) {
        let selectedWindows = component.get('v.selectedWindows');
        let self = event.target;
		
        if(self.checked) {
            selectedWindows.push(self.value);
        } else {
            selectedWindows.splice(selectedWindows.indexOf(self.value), 1);
        }
		
        component.set('v.selectedWindows', selectedWindows);
    },

    showAddTitlePlan : function(component, event, helper) {
        document.getElementById("EAW_TitlePlanWindowAddTitlePlanPopup").style.display = "block";
    },
	
    showWindowMassUpdate : function(component, event, helper) {
        
        let selectedWindowsFlag = component.get('v.selectedWindows');
        
        if(selectedWindowsFlag && selectedWindowsFlag.length > 0) {
            document.getElementById("EAW_TitlePlanWindowMassUpdatePopup").style.display = "block";
        } else {
        	helper.errorToast('You must select at least one Windows to update.');    
        }
    },

    showMassDelete : function(component, event, helper) {
        //selectedTitlePlans
	    let selectedTitlePlans = component.get('v.selectedWindows');
		
	    if(selectedTitlePlans && selectedTitlePlans.length > 0) {
            component.find("deletemodalCmp").open();
        } else {
            helper.errorToast('You must select at least one Title Plan to delete.');
        }
    },
    //LRCC-1573
    proceedToDelete:function(component, event, helper) {
        
        component.find("deletemodalCmp").close();
        let selectedTitlePlans = component.get('v.selectedTitlePlans');
        
        if(selectedTitlePlans && selectedTitlePlans.length > 0) {
            document.getElementById("EAW_TitlePlanWindowDeletePopup").style.display = "block";
        } else {
            helper.errorToast('You must select at least one Title Plan to delete.');
        }
    },
    removeDeleteConfirmation:function(component, event, helper) {
        
       component.find("deletemodalCmp").close();
    },

    showAddWindow : function(component, event, helper) {
        let selectedTitlePlans = component.get('v.selectedTitlePlans');

        if(selectedTitlePlans.length === 1) {
            let checkboxs = component.find('title-plan-checkbox');
            let selectedTitles = [];
            
            if(checkboxs.length) {
            	for(let checkbox of checkboxs) {
            		selectedTitles = helper.collectSelectedTitles(component, checkbox, selectedTitles);
	            }
            } else {
            	selectedTitles = helper.collectSelectedTitles(component, checkboxs, selectedTitles);
            }

            component.set('v.selectedTitles', selectedTitles);
            document.getElementById("EAW_TitlePlanWindowAddWindowPopup").style.display = "block";
        } else {
            helper.errorToast('You must select a single Title Plan to add a Window to.');
        }
    },

    applyLatestVersion : function(component, event, helper) {

         //LRCC-1459
        let titlePlans =  component.get('v.rowsToDisplay') == null ? [] : component.get('v.rowsToDisplay');
            //component.get('v.resultData') == null ? [] : component.get('v.resultData');
        let titlePlanId = event.target.id == null ? '' : event.target.id;
        let selectedTitlePlan = null;

        titlePlans.map(title => (title.TitlePlan.Id === titlePlanId) ? selectedTitlePlan = title : false);

        // set the data and then open the popup
        component.set('v.selectedPlan', selectedTitlePlan);
        document.getElementById("EAW_TitlePlanWindowVersionUpdatePopup").style.display = "block";
    },
	
    sort: function(component, event, helper) {
        let sortVal = event.currentTarget.dataset.value;
        let sortIdx = event.currentTarget.dataset.idx;
        helper.sortBy(component, sortVal, sortIdx);
    },
    
    resetColumn: function(component, event, helper) {
        let currentEle = component.get('v.currentEle');

        if(component.get("v.currentEle") !== null) {
            let newWidth = component.get("v.newWidth");
            let currentEleParent = currentEle.parentNode.parentNode;
            let parObj = currentEleParent.parentNode;

            parObj.style.width = newWidth + 'px';
            currentEleParent.style.width = newWidth + 'px';

            component.get("v.currentEle").style.right = 0;
            component.set("v.currentEle", null);
        }
    },
    setNewWidth: function(component, event, helper) {

        let currentEle = component.get("v.currentEle");

        if(currentEle != null && currentEle.tagName) {
            let parObj = currentEle;
			
            while(parObj.parentNode.tagName != 'TH') {
                if(parObj.className == 'slds-resizable__handle') {
                    currentEle = parObj;
                }
			    parObj = parObj.parentNode;
            }

            let mouseStart = component.get("v.mouseStart");
            let oldWidth = parObj.offsetWidth;
            let newWidth = oldWidth + (event.clientX - parseFloat(mouseStart));

            component.set("v.newWidth", newWidth);
            currentEle.style.right = ( oldWidth - newWidth ) + 'px';
            component.set("v.currentEle", currentEle);
        }
    },
    calculateWidth: function(component, event, helper) {
        let childObj = event.target;
        let mouseStart = event.clientX;

        component.set("v.currentEle", childObj);
        component.set("v.mouseStart", mouseStart);

        if(event.stopPropagation){
            event.stopPropagation();
        }

        if(event.preventDefault){
            event.preventDefault();
        }

        event.cancelBubble = true;
        event.returnValue = false;
    },
    updateSelectedRows : function(component, event, helper) {
        let selectedRows  = component.get('v.selectedRows');
        let self = event.target;
        
        if(self.checked) {
            selectedRows.push(self.value);
        } else {
            selectedRows.splice(selectedRows.indexOf(self.value), 1);
        }
        console.log(';;;;selectedRows',selectedRows);
        component.set('v.selectedWindows', selectedRows);
    },
    checkAll : function(component, event, helper) {
    	
    	event.stopImmediatePropagation();
    	
        let selectAllIdx = event.getSource().get('v.name');
        let checkboxes = component.find('row-checkbox');
        let self = event.getSource().get('v.checked');
        let selectedRows  = component.get('v.selectedRows');
        
        //LRCC-1459
        let resultData =  component.get("v.rowsToDisplay");//component.get("v.resultData");

        let windows = resultData[selectAllIdx];
        console.log(windows,selectAllIdx);
        // add/delete each window from selectedRows
        if(Array.isArray(windows)) {
            for(let window of windows) {
                window.isChecked = self;
                if(self && !selectedRows.includes(window.Id)) {
                    selectedRows.push(window.Id);
                } else if(!self && selectedRows.includes(window.Id)) {
                    selectedRows.splice(selectedRows.indexOf(window.Id), 1);
                }
            }
        } else {
            windows.isChecked = self;
            if(self && !selectedRows.includes(windows.Id)) {
                selectedRows.push(windows.Id);
            } else if(!self && selectedRows.includes(windows.Id)) {
                selectedRows.splice(selectedRows.indexOf(windows.Id), 1);
            }
        }
		
        // check/uncheck each window
        if(Array.isArray(checkboxes)) {
            for(let check of checkboxes) {
                check.getElement().checked = self;
            }
        } else {
            checkboxes.getElement().checked = self;
        }
       //LRCC-1459
        component.set('v.rowsToDisplay',resultData);
        //component.set('v.resultData',resultData);
        component.set('v.selectedWindows', selectedRows);
    },
    exportToCSV : function(component,event,helper){
        //LRCC-1675
        let exportSearchResultEvt = component.getEvent("EAW_ExportSearchResult");
        exportSearchResultEvt.setParams({"resultColumns" : component.get("v.resultColumns"),"eventFlag":'window'});
        exportSearchResultEvt.fire();
        
       /* 
    	let data = component.get('v.resultData');
    	console.log(data); 
        console.log('data length:::',data); 
        if(data == null) {
            return;
        }
        let csv = helper.convertArrayToCSV(component, data,helper);

        var hiddenElement = document.createElement('a');
        hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
        hiddenElement.target = '_self'; //
        hiddenElement.download = 'Plans&Windows_Results.csv';  // CSV file Name* you can change it.[only name not .csv]
        document.body.appendChild(hiddenElement); // Required for FireFox browser
        hiddenElement.click();*/
    },
    setSearchBasedType : function(component,event,helper){
        //LRCC-1518
    	if(component.get('v.searchCriteriaString') && component.get('v.searchCriteriaString').includes('Search')){
    		component.set('v.searchBased',true);
    	} else{
    		component.set('v.searchBased',false);
    	}
    },
    getAllWindows : function(component,event,helper){ // Related to LRCC - 239
    	console.log('all Windows',event.currentTarget.dataset.index);
    	if(event.currentTarget.dataset.index != null){
    		component.find('modalWindow').set('v.planId',event.currentTarget.dataset.index);
    		component.find('modalWindow').openModal();
    	}
    	
    },
    
    /****LRCC-1199-Edit toggle Action Logic****/
    editGrid: function(component, event, helper) {
        //LRCC-1120
        component.set('v.showSpinner', true);
        var checked = component.get('v.isEditable');
        //LRCC-1295
        if(checked == false && component.get("v.checkSaveRecords") == false) {
            	
            component.find("modalCmp").open();
        }
        helper.makeUnsavedChanges(component, event, helper);
        window.setTimeout(
            $A.getCallback(function() {
                if(component.isValid()){
                    helper.editGrid(component, event, helper);
                }
                else{
                    console.log('Component is Invalid');
                }
            }), 100
        );
    },
    
    saveChanges: function(component, event, helper) {
        helper.updateChangedRecord(component, event, helper);
    },
	openWindowRuleModal : function(cmp,event,helper){ // LRCC - 1259
        var windowId = event.getSource().get('v.name');
        console.log(windowId);
        if(windowId && cmp.find('windowRuleModal')){
            cmp.find('windowRuleModal').openModal(windowId);
        }
    },
    closeModal: function(component, event, helper) {
        component.find("modalCmp").close();
        component.set('v.isEditable', true);        
        var editTableEvent = $A.get("e.c:EAW_EditTableEvent");
        editTableEvent.setParams({"editMode":true});
        editTableEvent.fire();
       
	},
    saveModal : function(component, event, helper) {
        
        var myDataOriginal = component.get('v.myDataOriginal');
        component.find("modalCmp").close();
        component.set('v.isEditable', false);
        component.set('v.resultData', JSON.parse(JSON.stringify(myDataOriginal)));
        //LRCC-1010 & LRCC-1459
        component.set('v.rowsToDisplay', JSON.parse(JSON.stringify(myDataOriginal)));
        //component.set("v.resultData", resultset);
        //LRCC-1459
        //component.set("v.rowsToDisplay", resultset);
        
        //LRCC-1633
		if(component.find('select-all-plans-checkbox') && component.find('select-all-plans-checkbox').getElement() && component.find('select-all-plans-checkbox').getElement().checked == true) {
			component.find('select-all-plans-checkbox').getElement().checked = false;
		}
		if(component.find('select-all-windows-checkbox') && component.find('select-all-windows-checkbox').getElement() && component.find('select-all-windows-checkbox').getElement().checked == true){
			component.find('select-all-windows-checkbox').getElement().checked = false;
		}
        
        helper.clearUnsavedChanges(component, event, helper);
        
	},
    openWindowStrandModal : function(cmp,event,helper){ // LRCC - 1259
        var windowId = event.getSource().get('v.name');
        console.log(windowId);
        if(windowId && cmp.find('windowStrandModal')){
            cmp.find('windowStrandModal').openModal(windowId);
        }
    },
    //LRCC-631
    rowAction: function(component, event, helper) {
        
        var recId = event.currentTarget.dataset.value;
        var notesEvent = $A.get("e.c:EAW_NotesEvent");
        notesEvent.setParams({
            
            "parentId": recId
        });
        notesEvent.fire();
    },
    
    setNewWidth: function(component, event, helper) {
        let currentEle = component.get("v.currentEle");
        
        if (currentEle != null && currentEle.tagName) {
            let parObj = currentEle;
            
            while (parObj.parentNode.tagName != 'TH') {
                if (parObj.className == 'slds-resizable__handle') {
                    currentEle = parObj;
                }
                
                parObj = parObj.parentNode;
            }
            
            let mouseStart = component.get("v.mouseStart");
            let oldWidth = parObj.offsetWidth;
            let newWidth = oldWidth + (event.clientX - parseFloat(mouseStart));
            
            component.set("v.newWidth", newWidth);
            currentEle.style.right = (oldWidth - newWidth) + 'px';
            component.set("v.currentEle", currentEle);
        }
    },
    //LRCC-1327
    callHandleCheckBoxValues : function(component, event, helper){
    	console.log('callHandleCheckBoxValues::::::::::::::::::::');
    	helper.handleCheckBoxValues(component);
    },
    //LRCC-1459
    renderPage : function(component, event, helper) {
        
        console.log('click::::',component.get("v.pagesPerRecods"),':::>::',component.get("v.pageNumber"),'<<>>',component.get('v.colWithAlpha'))
        let isAlphaFiltered = component.get("v.isAlphaFiltered");
        let isColFiltered = component.get("v.buttonstate"); //LRCC-1517
        
        let pageConfigEvt = component.getEvent("pageConfigEvtPlans");
         if(component.find('pagePerRecord')){
            
            component.set("v.pagesPerRecod",component.find('pagePerRecord').get("v.value"));
        }
  
        pageConfigEvt.setParams({"pagesPerRecord" : component.get("v.pagesPerRecod"),
                                 "currentPageNumber":component.get("v.pageNumber"),
                                 "selectedFilterMap" : isAlphaFiltered || isColFiltered ? component.get('v.colWithAlpha') : null //LRCC-1517
                                 });
        pageConfigEvt.fire();
	},
	handleChange: function(component, event, helper) {
        
         if(component.get("v.isEditable")){
            helper.showErrorToast(component);
            component.find('pagePerRecord').set("v.value",component.get("v.oldPagesPerRecord"));
            return;
        } 
         
        component.set('v.pageNumber', 1);
        let pagesPerRecord = parseInt(component.find('pagePerRecord').get("v.value"));
         
        component.set("v.pagesPerRecod",component.find('pagePerRecord').get("v.value"));
        component.set("v.oldPagesPerRecord",component.find('pagePerRecord').get("v.value"));
        component.set("v.maxPage", Math.floor((component.get('v.totalRecordCount')+pagesPerRecord - 1 )/pagesPerRecord)); 
         
         var pageConfigEvt = component.getEvent("pageConfigEvtPlans");
            pageConfigEvt.setParams({"pagesPerRecord" : component.get("v.pagesPerRecod"),
                                     "currentPageNumber":component.get("v.pageNumber"),
                                     "selectedFilterMap" : component.get('v.colWithAlpha')}); //LRCC-1517
            pageConfigEvt.fire();
    },    
    chooseAlphabet : function(component, event, helper) {
        
       if(component.get("v.isEditable")){
            helper.showErrorToast(component);
            return;
        } 
        
        var target = event.currentTarget;
        var rowIndex = target.getAttribute("data-rowIndex");
       
        if(component.get("v.alphabets")[rowIndex]){
            
            var colAplpha = component.get('v.colWithAlpha');
            colAplpha.selectedAlpha = component.get("v.alphabets")[rowIndex];
            component.set('v.colWithAlpha',colAplpha);
        }
        let colWithAlpha = component.get('v.colWithAlpha');
        if( colWithAlpha.selectedCol != null ||   colWithAlpha.selectedAlpha == 'All' ){
            
            if(colWithAlpha.selectedAlpha == 'All'){
                
                colWithAlpha.selectedCol == null;
                component.set('v.colWithAlpha',colWithAlpha);
                component.set("v.isAlphaFiltered",false);
            }
            component.set("v.isAlphaFiltered",true);
            component.set('v.pageNumber', 1);
            component.set("v.pageIdMap",{});
            var pageConfigEvt = component.getEvent("pageConfigEvtPlans");
            
            pageConfigEvt.setParams({"pagesPerRecord" : component.get("v.pagesPerRecod"),
                                     "currentPageNumber":1,
                                     "selectedFilterMap" : component.get('v.colWithAlpha') });
            pageConfigEvt.fire();

        }
        if(colWithAlpha.selectedAlpha == 'All'){
            
            component.set('v.colWithAlpha',{'selectedCol':null,'selectedAlpha':null});	
        }
        
         console.log('::: selected :::', component.get('v.colWithAlpha'));
    },
    //LRCC:1636
    handleNotesAttachmentEvent : function(component, event, helper) {
        component.set('v.title', event.getParam("title"));
        component.set('v.body', event.getParam("body"));
        component.find('notesComponent').notesAttachmentAction(component.get('v.selectedRows'), event.getParam("title"), event.getParam("body"), event.getParam("action"));
    },
    deleteWindowsUI :function(component, event, helper) {
         console.log(':::delete::selectedWindows:',component.get('v.selectedWindows'));
        
        console.log(':::delete:::',component.get('v.toDeleteWindows'));
        var delWindowList = [];
        var selectedWindowsIds = component.get('v.selectedWindows');
        for(var i=0 ; i < component.get('v.rowsToDisplay').length ; i++){
            console.log(':::index:::',selectedWindowsIds.indexOf(component.get('v.rowsToDisplay')[i].Id));
            if( selectedWindowsIds.indexOf(component.get('v.rowsToDisplay')[i].Id) > -1){
                delWindowList.push(component.get('v.rowsToDisplay')[i]);
                //LRCC-1018
                if(component.get('v.rowsToDisplay') && component.get('v.rowsToDisplay')[i].indication){
                     delete component.get('v.rowsToDisplay')[i].indication;
                }
            }
        }
         
        component.set('v.toDeleteWindows',delWindowList);
        console.log(':::delete:::',component.get('v.toDeleteWindows'));
        var action = component.get("c.deleteWindows");
        action.setParams({'windowsIdList':delWindowList});
        action.setCallback(this, function(response) {
            var state = response.getState();
             console.log(':::response:::',response.getReturnValue());
            var data = response.getReturnValue();
            
            if (state === "SUCCESS") {
                
                component.find("deletemodalCmp").close();
                var resultData = component.get("v.rowsToDisplay");
                var tempResultData = new Array();
                for (var data of resultData) {
                    if (selectedWindowsIds.indexOf(data.Id) == -1) {
                        tempResultData.push(data);
                    }
                }
                
                //LRCC-1459
                component.set("v.rowsToDisplay", tempResultData);
                let pagesPerRecord = parseInt(component.find('pagePerRecord').get("v.value"));
                component.set("v.maxPage", Math.floor((component.get('v.totalRecordCount')+pagesPerRecord - 1 )/pagesPerRecord)); 
                
                
                helper.successToast($A.get("$Label.c.RecordsDeletedSuccessfully"));
                helper.reinitializePageMap(component,component.get("v.pageNumber"));
                component.set('v.pageNumber', 1);
                var pageConfigEvt = component.getEvent("pageConfigEvtPlans");
                pageConfigEvt.setParams({"pagesPerRecord" : component.get("v.pagesPerRecod"),
                                         "currentPageNumber":component.get("v.pageNumber"),
                                         "isDeleted":true});
                pageConfigEvt.fire();
               component.set('v.isDelete', true);
            }else if(state === "ERROR"){
                
            }
        });
         $A.enqueueAction(action);
    },
    addwindows: function(component, event, helper) {
        $A.util.addClass(component.find('modal'),'slds-fade-in-open');
        $A.util.addClass(component.find('backDrop'),'slds-backdrop_open');
        component.set('v.isShown', true);
    },
    closeModalWindow : function(cmp,event,helper){
        $A.util.removeClass(cmp.find('modal'),'slds-fade-in-open');
        $A.util.removeClass(cmp.find('backDrop'),'slds-backdrop_open');
        cmp.set('v.isShown', false);
    },
    
    //LRCC-1447
    saveModalforfirm : function(component, event, helper) {
        
        component.find("modalCmpforfirm").close();
        
        var firmModalData = component.get("v.firmModalData");
        var recordMap = component.get("v.recordMap");
        var notes = component.get('v.massUpdateNotesParent');
        
        //LRCC-1018
        for(var i = 0;i<firmModalData.length;i++){
            delete firmModalData[i].indication;
        }
        
        if(firmModalData.length || notes){
            var action = component.get("c.updateWindowRecords");
            action.setParams({ windowList : firmModalData, 
                              recordsMap : JSON.stringify(recordMap),
                              notes:JSON.stringify(notes),
                              noteWindows:component.get("v.noteschangedWindows")});
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    
                    //component.set('v.isEditable',false); //LRCC-1668
                    helper.successToast('The plan(s) has been updated successfully.');
                    //LRCC-1732
                    component.set('v.massUpdateNotesParent',{});
                    component.set('v.noteschangedWindows',[]);
                    //LRCC-1327 - Issue 
                    helper.handleCheckBoxValues(component, event, helper);
                    //LRCC-1327 - Issue1
                    
                    component.set('v.rowsToDisplay', JSON.parse(JSON.stringify( component.get('v.rowsToDisplay'))));                    
                    helper.editGrid(component, event, helper); //LRCC-1668
                    component.set('v.checkSaveRecords',true);
                    helper.clearUnsavedChanges(component, event, helper);
                    component.set('v.massUpdateNotes',{});
                    //LRCC-1636
                    component.set('v.disableSave',false);
                    component.set('v.massUpdateNotesParent',{});
                    component.find('notesComponent').set('v.notesAttachement', {});
                }else if (state === "ERROR") {
                    //LRCC-1376
                    let mess = '';
                    for(let result of response.getError()) {
                        if(result.pageErrors && result.pageErrors.length) {
                            for(let iresult of result.pageErrors) {
                                mess +=iresult.statusCode+' '+iresult.message;
                            }
                        }
                        if(result.fieldErrors && result.fieldErrors.length) {
                            for(let iresult of result.fieldErrors){
                                if(iresult.CurrencyIsoCode){
                                    var error =  iresult.CurrencyIsoCode;
                                    if(!Array.isArray(error)){
                                        error = [error]
                                    }
                                    for(var ierror of error){
                                        mess +=ierror.statusCode+' '+ierror.message;
                                    }
                                }
                            }
                        }
                    }
                    helper.errorToast(mess);
                    console.log('error::: '+response.getError());
                    
                    console.log('error::: '+mess);
                    //LRCC-1636
                    component.set('v.disableSave',false);
                }
            });
            $A.enqueueAction(action);
        }else {
            //LRCC-1636
            component.set('v.disableSave',false);
            helper.infoToast($A.get("$Label.c.EAW_RecordsNotEdited"));
        }
    },
    //LRCC-1447
    closeModalforfirm : function(component, event, helper) {
        
        component.set('v.disableSave',false);
        component.find("modalCmpforfirm").close();
        component.set("v.firmModalData", []);
        component.set('v.firmModalOpen', false);
    }
})