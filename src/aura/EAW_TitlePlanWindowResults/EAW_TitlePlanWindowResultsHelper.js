({
    sortBy : function (component, field, index) {
        
        let sortAsc = component.get("v.sortAsc");
        let sortField = component.get("v.sortField");
        //LRCC-1459
        let resultData =  component.get("v.rowsToDisplay");
            //component.get("v.resultData");
        let records = resultData[index];
        //LRCC-1690
        if(field == 'windowTags'){
            
           //sortField = 'releaseDateTagsSortColumn';
            field ='windowTagsSortColumn';
        }
        sortAsc = field == sortField ? !sortAsc : true;
        records.sort(function(a, b) {
            if(sortAsc) {
                if(a[field] == undefined) {
                    return -1;
                } else if(b[field] == undefined) {
                    return 1;
                }
                
                return (a[field] > b[field]) ? 1 : ((b[field] > a[field]) ? -1 : 0);
            } else {
                if(a[field] == undefined) {
                    return 1;
                } else if(b[field] == undefined) {
                    return -1;
                }
                
                return (a[field] < b[field]) ? 1 : ((b[field] < a[field]) ? -1 : 0);
            }
        });
        
        resultData[index] = records;
        
        component.set("v.sortAsc", sortAsc);
        component.set("v.sortField", field);
        component.set("v.resultData", resultData);
        
        /****LRCC:887-Sorting indicator applied logic****/
        component.set("v.selectedTabsoft", field);
        if (sortAsc == true) {
            component.set("v.arrowDirection", 'arrowup');
        } else {
            component.set("v.arrowDirection", 'arrowdown');
        }
    },
    collectSelectedTitles : function(component, checkbox, selectedTitles) {
        let isChecked = checkbox.getElement().checked;
        let planId = checkbox.getElement().value;
        
        if(isChecked) {
            let resultData = component.get('v.resultData');
            let titlePlan = resultData.find(item => item.TitlePlan.Id === planId);
            
            if(selectedTitles.indexOf(titlePlan.TitlePlan.TitleId) === -1) {
                selectedTitles.push(titlePlan.TitlePlan.TitleId);
            }
        }
        
        return selectedTitles;
    },
    
    errorToast : function(message) {
        var toastEvent = $A.get('e.force:showToast');
        
        toastEvent.setParams({
            'title': 'Error!',
            'type': 'error',
            'message': message
        });
        
        toastEvent.fire();
    },
    
    successToast : function(message) {
        var toastEvent = $A.get('e.force:showToast');
        
        toastEvent.setParams({
            'title': 'Success!',
            'type': 'success',
            'message': message
        });
        
        toastEvent.fire();
    },
    
    selectAllWindows : function(component, event, helper) {
        
        //When click all windows uncheck all plans
        let checkboxes = component.find('title-plan-checkbox');
        let selectedTitlePlans = component.get('v.selectedTitlePlans');
        let parentCheckBox = component.find('select-all-plans-checkbox').getElement().checked = false;
        
        if(Array.isArray(checkboxes)) {
            for (let check of checkboxes) {
                console.log('check:::',check);
                check.getElement().checked = false;
            }
        }
        component.set('v.selectedTitlePlans', selectedTitlePlans); 
        //end
        
        component.set('v.showSpinner',true);
        let self = event.target;
        let data = component.get('v.rowsToDisplay') == null ? [] : component.get('v.rowsToDisplay'); // LRCC - 1459
        let selectedWindows = [];
        
        if(self.checked){
            component.set('v.noOfOpenWindows',data.length);
        } else{ 
            component.set('v.noOfOpenWindows',0);
        }
        for(let i=0; i < data.length; i++) {
            data[i].showWindows = self.checked;     // Expand all Window rows in event that they're collapsed
        }
        component.set('v.rowsToDisplay', data);
        
        var windowTable = component.find('windowTable');
        console.log('windowTable:::',JSON.stringify(windowTable));
        if(windowTable) {
            if(!Array.isArray(windowTable)){
                windowTable = [windowTable];
            }
        }
        
        var wtIndex = 0;
        for(let i=0; i < data.length; i++) {
            
            if( data[i] ){
                // Add each window to selectedWindows for individual title-plans
                for(let window1 of data[i]) {	
                console.log('window1;;;;;;;',window1);            		            	
                    
                    window1.isChecked = self.checked;
                    
                    if(self.checked) {
                        data[i].selectedWindows.push(window1.Id);   // NOT SURE what i'm supposed to push? 
                        selectedWindows.push(window1.Id);
                    } else {
                        data[i].selectedWindows.splice(data[i].selectedWindows.indexOf(window1.Id),1);
                        selectedWindows.splice(data[i].selectedWindows.indexOf(window1.Id),1);
                    }
                }
            }
            if( data[i].selectedWindows && data[i].selectedWindows.length > 0) {
                windowTable[wtIndex].set("v.selectedRows", data[i].selectedWindows);
                wtIndex++;
            }
        }	     
        
        //Window header check
        console.log('windowTable:::',windowTable);
	     
		setTimeout(function() {
		
			for(let table of windowTable) {
			console.log('table::',table);
			
			    /*
			     * LRCC-1633
			     let windowParentChk = table.find('header-checkbox');
			    
			    if(Array.isArray(windowParentChk)) {
			        for (let windowCheck of windowParentChk) {
			            if(windowCheck){
			                windowCheck.set('v.checked',self.checked);
			            }
			        }
			    } 
			    else if(windowParentChk) {
			    	windowParentChk.set('v.checked',self.checked);
			    }
			    
			    */
			    
			    let rowChk = table.find('row-checkbox');
			    
			    if(Array.isArray(rowChk)) {
			        for (let row of rowChk) {
			            if(row){
			                row.set('v.checked',self.checked);
			            }
			        }
			    } 
			    else if(rowChk) {
			    	rowChk.set('v.checked',self.checked);
			    }
		    }
		},200);
        console.log('selectedWindows::::::::::::',selectedWindows);
        component.set('v.selectedWindows', selectedWindows);
        console.log('data;;;;',JSON.parse(JSON.stringify(data)));
        component.set('v.rowsToDisplay', data);
        component.set('v.showSpinner',false);
    },
    selectAllPlans : function(component, event, helper) {
        
        //when click show all plans uncheck the all windows
        let parentCheckBox = component.find('select-all-windows-checkbox').getElement().checked = false;
        //LRCC-1633
        let data = component.get('v.rowsToDisplay') == null ? [] : component.get('v.rowsToDisplay');
        
        for(let i=0; i < data.length; i++) {
            data[i].showWindows = false;     // Expand all Window rows in event that they're collapsed
        }
        for(let i=0; i < data.length; i++) {
            if( data[i] ){
                for(let window1 of data[i]) {	            		            	
                    window1.isChecked = false;
                }
            }
        }
        component.set('v.rowsToDisplay', data);
        //end
        
        let checkboxes = component.find('title-plan-checkbox');
        let self = event.target;
        let selectedTitlePlans = component.get('v.selectedTitlePlans');
        
        if(Array.isArray(checkboxes)) {
            for (let check of checkboxes) {
                
                check.getElement().checked = self.checked;
                if (check.getElement().checked && !selectedTitlePlans.includes(check.getElement().value)) {
                    selectedTitlePlans.push(check.getElement().value);
                } 
                else if (!check.getElement().checked) {
                    selectedTitlePlans.splice(selectedTitlePlans.indexOf(check.getElement().value), 1);
                }
            }
        } else {
            checkboxes.getElement().checked = self.checked;
            checkboxes.getElement().checked ? selectedTitlePlans.push(checkboxes.getElement().value) : selectedTitlePlans.pop();
        }
        
        component.set('v.selectedTitlePlans', selectedTitlePlans);    
    },
    fieldSet: function (component, event, helper) {
        try {
            var action = component.get("c.getFields");
            action.setParams(
                {
                    "objFieldSetMap": {
                        "EAW_Window__c": "Search_Result_Fields"
                    }
                }
            );
            
            action.setCallback(this, function(response) {
                var state = response.getState();
                
                if(state === "SUCCESS") {
                    console.log(':::::::FieldSet End Time::::'+new Date());
                    helper.clearUnsavedChanges(component, event, helper);
                    var columns = new Array();
                    
                    columns.push({fieldName: 'FIN_PROD_ID__c', label: 'Fin\'l Title Id', type: 'string', updateable: false});
                    columns.push({label: 'Title', fieldName: 'titleName', type: 'string', updateable: false }); // LRCC-1551
                    columns.push({fieldName: 'PROD_TYP_CD_INTL__c', label: 'Int\'l Product Type', type: 'string', updateable: false});
                    columns.push({fieldName: 'FIN_DIV_CD__c', label: 'Division', type: '', updateable: false});
                    columns.push({fieldName: 'TitleTags', label: 'Title Tag(s)', type: 'string', updateable: false});
                    columns.push({fieldName: 'windowGuidelineName', label: 'Window Guideline Alias', type: 'string', updateable: false}); //LRCC-1493.
                    columns.push({fieldName: 'planGuidelineName', label: 'Plan Guideline Name', type: 'string', updateable: false}); //LRCC-1700
                    columns.push({fieldName: 'PlanName', label: 'Plan Name', type: 'string', updateable: false}); //LRCC-1700
          
                    for(let col of response.getReturnValue()) {
                        for(let colName of col) {
                            console.log('colName::',colName);
                            //LRCC-1236
                            if(colName.fieldName != 'Name' && colName.fieldName != 'Customers__c' && colName.fieldName != 'Start_Date_Rule__c' && colName.fieldName != 'End_Date_Rule__c') {
                                
                                //LRCC-1828
                                if(colName.fieldName == 'Is_Confidential_Window__c') {                                    
                                    colName.label = 'Confidential';
                                    columns.splice(5, 0, colName);
                                } else {                                
                                    columns.push(colName);
                                }
                            } 
                            if( colName.fieldName == 'Customers__c' ){
                            	colName.updateable = false;
                            	columns.push(colName);
                            }
                            if(colName.fieldName == 'Start_Date__c'){ // LRCC - 1154
                                //LRCC-1354
                            	var startDateText = [{label: "--None--", value: ""},{label: "TBD", value: "TBD"}];
                                columns.push({label: 'Start Date Text', fieldName: 'Start_Date_Text__c', type: 'picklist',updateable: true,picklistValues:startDateText});
                                //columns.push({label: 'Start Date Text', fieldName: 'Start_Date_Rule__c', type: '',updateable: false});
                            }
                            if(colName.fieldName == 'End_Date__c'){
                                //LRCC-1354
                            	var endDateText = [{label: "--None--", value: ""},
                            					   {label: "TBD", value: "TBD"},
                            					   {label: "Perpetuity", value: "Perpetuity"},
                            					   //{label: "Rights End Date", value: "Rights End Date"}
                            					   ];
                                columns.push({label: 'End Date Text', fieldName: 'End_Date_Text__c', type: 'picklist',updateable: true,picklistValues:endDateText});
                            	
                                //columns.push({label: 'End Date Text', fieldName: 'End_Date_Rule__c', type: '',updateable: false});
                            }
                            if(colName.fieldName == 'Right_Status__c' || colName.fieldName == 'License_Info_Codes__c'){ // LRCC - 1120
                                colName.updateable = false;
                            }
                        }
                    }
                    // LRCC-1236
                    /*columns.push({label: 'Customer', fieldName: 'Customers__c', type: '',updateable: false});*/
                    // LRCC:1096
                    // 
                    columns.push({label: 'Window Tag(s)', fieldName: 'windowTags', type: 'multiselect', updateable: true});
                    component.set('v.resultColumns', columns);
                   // helper.setBodyWidth(component); // LRCC - 1282                               
                     // LRCC-1518                               
                    if(component.get('v.searchCriteriaString') && component.get('v.searchCriteriaString').includes('Search')){
                        component.set('v.searchBased',true);
                    } else {
                        component.set('v.searchBased',false);
                    }
                     //LRCC-1459  
                                           
                    if(true){
                                                   
                        console.log('generateAlphabets::::');                          
            			helper.generateAlphabets(component, event, helper);
       				}                               
					
                } else if(state === "ERROR") {
                    var errors = response.getError();
                    
                    if(errors) {
                        if(errors[0] && errors[0].message) {
                            console.log("Error message: " +
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                } else {
                    console.log("Something went wrong, Please check with your admin");
                }
                        component.set('v.showSpinner',false);
            });
            $A.enqueueAction(action);
        } catch (e) {
            console.log('Exception: ' + e);
        }
    },
    convertArrayToCSV : function(component,data,helper){
        var csvStringResult, counter, header, keys, columnDivider, lineDivider;
        
        // check if 'objectRecords' parameter is null, then return from function
        if(data == null || !data.length) {
            return null;
        }
        
        // store ,[comma] in columnDivider variabel for sparate CSV values and
        // for start next line use '\n' [new line] in lineDivider varaible
        columnDivider = ',';
        lineDivider =  '\n';
        
        // in the keys variable store fields API Names as a key
        // this labels use in CSV file header
        var columns = JSON.parse(JSON.stringify(component.get('v.resultColumns')));
        header = ['Title Name','Plan Name'];
        keys =[];
        
        csvStringResult = component.get('v.searchCriteriaString') || '';
        
        for(var i=0;i<columns.length;i++){
            header.push(columns[i].label); //this array is the 'pretty' column names for the csv
            keys.push(columns[i].fieldName); //this array is the actual column name
        }
        
        
        csvStringResult += header.join(columnDivider);
        for(var i=0;i<data.length;i++){
            if(data[i].length){
                for(let j=0; j < data[i].length; j++) {
                    csvStringResult += lineDivider;
                    csvStringResult += (data[i].TitlePlan.Name || '').replace(',','');
                    csvStringResult += columnDivider + (data[i].TitlePlan.Acquired_Indicator__c || '').replace(',','') + columnDivider;
                    counter = 0;
                    for(var sTempkey in keys) {
                        var skey = keys[sTempkey];
                        
                        // add , [comma] after every String value (except first)
                        if(counter > 0){
                            csvStringResult += columnDivider;
                        }
                        if(columns[sTempkey].type == 'date' && !$A.util.isEmpty((data[i])[j][skey])){
                            var resultData = helper.getFormattedDate((data[i])[j][skey]);
                        } else{
                            //var resultData =  (data[i].Windows)[j][skey] || '';
                            //LRCC-1014 - 'Fin'l Title Id' column data is displayed incorrectly
                            var resultData = (skey=='FIN_PROD_ID__c')?((data[i])[j][skey])?'=""'+(data[i])[j][skey]+'""':'': (data[i])[j][skey] || '';
                        }
                        csvStringResult += '"'+ resultData +'"';
                        counter++;
                    }
                    // csvStringResult += columnDivider + (data[i].TitlePlan.Name || '').replace(',','');
                    // csvStringResult += columnDivider + (data[i].TitlePlan.Acquired_Indicator__c || '').replace(',','');
                    
                }
            }
            
        }
        
        
        
        /* for(var i=0;i<data.length;i++){
            csvStringResult += ((data[i].TitlePlan.Name || '').replace(',','') + '-' +  (data[i].TitlePlan.Acquired_Indicator__c || '').replace(',','') + ' (plan)') + lineDivider;
			if(data[i].Windows.length){
				csvStringResult += lineDivider + columnDivider;
				csvStringResult += header.join(columnDivider);
				
				for(let j=0; j < data[i].Windows.length; j++) {
					csvStringResult += lineDivider + columnDivider;
					counter = 0;
					
					for(var sTempkey in keys) {
					    var skey = keys[sTempkey];
					
					    // add , [comma] after every String value (except first)
						if(counter > 0){
						    csvStringResult += columnDivider;
						}
						if(columns[sTempkey].type == 'date' && !$A.util.isEmpty((data[i].Windows)[j][skey])){
		            		var resultData = helper.getFormattedDate((data[i].Windows)[j][skey]);
		            	} else{
		            		 var resultData =  (data[i].Windows)[j][skey] || '';
		            	}
						csvStringResult += '"'+ resultData +'"';
				        counter++;
				    }
					//csvStringResult += lineDivider;
				}
			} else{
				csvStringResult +=  columnDivider +  'No Window Records' + lineDivider;
			}
			csvStringResult += lineDivider;      	
        }*/
        // return the CSV format String
        return csvStringResult;
    },
    getFormattedDate : function(dateToFormat){
        /*
        var mydate = new Date(dateToFormat);
        var month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                     "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"][mydate.getMonth()];
        return ' '+mydate.getDate() + '-' + month + '-' +  mydate.getFullYear()+' ';
        */
        //LRCC-1675
        var mydate = dateToFormat.split('-');
        var month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                     "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"][mydate[1]-1];
        return ' '+mydate[2] + '-' + month + '-' +  mydate[0]+' ';
    },
    
    /****LRCC-1199-Edit toggle Action Logic****/
    handleCheckBoxValues: function(component) {
        //LRCC-1327
        window.setTimeout(
            $A.getCallback(function() {
                
                component.set('v.selectedWindows',[]);  
                component.set('v.selectedTitlePlans',[]);        
                component.set('v.selectedRows',[]); 
                component.set('v.selectedTitles',[]); 
                component.set('v.selectedPlan',[]);
               
                var checkboxes;
                if(component.find('select-all-plans-checkbox')) {
                    
                    //component.find('select-all-plans-checkbox').getElement().checked = false;
                    //LRCC-1327
                    console.log('testing :::plan check:::',component.find('select-all-plans-checkbox'));
                    var allPlanCheckBox = component.find('select-all-plans-checkbox');
                    if(!Array.isArray(allPlanCheckBox)){
                        allPlanCheckBox = [allPlanCheckBox];
                    }
                    for(var planCheckBox of allPlanCheckBox){
                        planCheckBox.getElement().checked = false;
                    }       
                }
                if(component.find('select-all-windows-checkbox')) {
                    //component.find('select-all-windows-checkbox').getElement().checked = false;
                    //LRCCC-1327
                    console.log('testing :::window check:::',component.find('select-all-windows-checkbox'));
                    
                    var allWindowCheckBox = component.find('select-all-windows-checkbox');
                    if(!Array.isArray(allWindowCheckBox)){
                        allWindowCheckBox = [allWindowCheckBox];
                    }
                    for(var windowCheckBox of allWindowCheckBox){
                        windowCheckBox.getElement().checked = false;
                    }      
                }
                if(component.find('title-plan-checkbox')) {
                    checkboxes = component.find('title-plan-checkbox');
                }
                
                if(Array.isArray(checkboxes)) {
                    for(let check of checkboxes) {
                        if(check && check.getElement())
                            check.getElement().checked = false;
                    }
                } else if(checkboxes && checkboxes.getElement()) {
                    checkboxes.getElement().checked = false;
                } 
                //LRCC-1380 check box uncheck issue.
                var windowTable = component.find('windowTable');
                console.log('windowTable:clear::',JSON.stringify(windowTable));
                if(windowTable) {
                    if(!Array.isArray(windowTable)){
                        windowTable = [windowTable];
                    }
                }
                console.log('windowTable:after::',windowTable);
                for(let windowInst of  windowTable){
                    
                    if(windowInst.find('header-checkbox')) {
                        
                        var headerCheckboxes = windowInst.find('header-checkbox'); 
                        if(Array.isArray(headerCheckboxes)) {
                            for(let check of headerCheckboxes) {
                                check.set('v.checked',false);
                            }
                        } else  {
                            headerCheckboxes.set('v.checked',false);
                        }
                    }
                    if(windowInst.find('row-checkbox')) {
                        
                        var rowCheckboxes = windowInst.find('row-checkbox'); 
                        if(Array.isArray(rowCheckboxes)) {
                            for(let check of rowCheckboxes) {
                                check.set('v.checked',false);
                            }
                        } else  {
                            rowCheckboxes.set('v.checked',false);
                        }
                    } 
                }
               
                var data = component.get('v.rowsToDisplay');
                if(data && data != null) {
                    for(let window1 of data) {	
                        window1.isChecked = false;
                    }
                }
                // convert window.
                /*if(data && data != null) {
                    for(let i=0; i < data.length; i++) {            
                        if( data[i] ){
                            for(let window1 of data[i]) {	
                                window1.isChecked = false;
                            }
                        }
                    }
                }*/
                console.log('before :::',component.get('v.rowsToDisplay'))
                component.set('v.rowsToDisplay',data);
                component.set('v.resultData',data);
                console.log('after :::',component.get('v.rowsToDisplay'))
            }), 300
        );
    },
    
    //LRCC-1120
    editGrid: function(component, event, helper) {
        
        console.log('::::::::::::::::',component.get('v.rowsToDisplay'));
        //LRCC-1445 && LRCC-1327 - Issue5
        //var resultData = component.get('v.resultData');
        var resultData = component.get('v.rowsToDisplay');        
        
        if(component.get('v.isEditable')) {
            
            component.set('v.myDataOriginal', JSON.parse(JSON.stringify(resultData)));
        }
        
        let isEditable = component.get('v.isEditable');
        let editTableEvent = $A.get('e.c:EAW_EditTableEvent');
        editTableEvent.setParams({
            'editMode': isEditable
        });
        editTableEvent.fire();
        if(component.find('windowTable'))component.find('windowTable').editWidthAdjustment(); // LRCC - 1701
        component.set('v.showSpinner', false);
        //helper.handleCheckBoxValues(component); //LRCC-1199
    },
    
    findOutChangedRecordInTable: function(component, event, helper) {
        
        var resultData = component.get('v.rowsToDisplay')//component.get('v.resultData');
        var myDataOriginal = component.get('v.myDataOriginal');
        var myColumns = component.get("v.resultColumns");
        var updatedData = new Array();
        
        /*for (var i = 0; i < resultData.length; i++) {
            
            var changedWindowList = resultData[i].Windows;
            var originalWindowList =  myDataOriginal[i].Windows;
            for(var j = 0;j < changedWindowList.length;j++){
                
                for (let column of myColumns) {
                    
                    var data = changedWindowList[j];
                    var dataOriginal = originalWindowList[j];
                    if (data[column.fieldName] != dataOriginal[column.fieldName]) {
                        updatedData.push(data);
                        break;
                    }
                }
            }
        }*/
        console.log('resultData :::find changes ::',resultData);
        
         console.log('myDataOriginal :::find changes ::',myDataOriginal);
        console.log('myColumns :::find changes ::',myColumns);
        for (var i = 0; i < resultData.length; i++) {
            for (let column of myColumns) {
                var data = JSON.parse(JSON.stringify(resultData[i]));
                var dataOriginal = JSON.parse(JSON.stringify(myDataOriginal[i]));
                
                if (data[column.fieldName] && (typeof (data[column.fieldName]) == 'object') && data[column.fieldName].length != undefined) {
                    console.log('ccccc:::',column.fieldName);
                     console.log(' data[column.fieldName]:::', data[column.fieldName]);
                    console.log(' dataOriginal[column.fieldName]:::', dataOriginal[column.fieldName]);
                    var temp = true;
                    for (let dataObj of data[column.fieldName]) {
                        var originalObjIds = [];
                        for (let originalObj of dataOriginal[column.fieldName]) {
                            var originalObjId;
                            if (originalObj.Id) {originalObjId=originalObj.Id;} else {originalObjId=originalObj.id;}
                            originalObjIds.push(originalObjId);
                        }
                        var dataObjId;
                        if (dataObj.id) {dataObjId=dataObj.id} else {dataObjId=dataObj.Id}
                        if ((! originalObjIds.includes(dataObjId))) {
                            console.log('hi');
                            updatedData.push(data);
                            temp = false;
                            break;
                        }
                    }
                    if (temp) {
                        for (let originalObj of dataOriginal[column.fieldName]) {
                            var dataObjIds = [];
                            for (let dataObj of data[column.fieldName]) {
                                var dataObjId;
                                if (dataObj.Id) {dataObjId=dataObj.Id;} else {dataObjId=dataObj.id;}
                                dataObjIds.push(dataObjId);
                            }
                            var originalObjId;
                            if (originalObj.id) {originalObjId=originalObj.id} else {originalObjId=originalObj.Id}
                            if ((! dataObjIds.includes(originalObjId))) {
                                console.log('hello');
                                updatedData.push(data);
                                break;
                            }
                        }
                    }
                } else { //LRCC-1787 if(data[column.fieldName] && (typeof (data[column.fieldName]) == 'string')) {
                    if (data[column.fieldName] != dataOriginal[column.fieldName]) {
                        console.log('welcome');
                        updatedData.push(data);
                        break;
                    }
                }
            }
        }  
        console.log('updatedData::::',updatedData);
        if (updatedData.length) {
            
            return updatedData;
        }
        return [];
        
    },
    updateChangedRecord : function(component, event, helper) {
        //LRCC-1636
        component.set('v.disableSave',true);
        var updatedWindowList = helper.findOutChangedRecordInTable(component, event, helper);
        //console.log('updatedWindowList::str:::',JSON.stringify(updatedWindowList));
        //LRCC-1096
        var recordMap = component.get("v.recordMap");
        //LRCC-1380 APPEND,REPLACE & CLEAR radio button for notes 
        var notes = component.get('v.massUpdateNotesParent');
        console.log('notes cwindow:::',component.get("v.noteschangedWindows")); 
        console.log('notes:::::',JSON.stringify(notes));
        console.log('recordMap:::::',JSON.stringify(recordMap));
        console.log('updatedWindowList:::::',JSON.parse(JSON.stringify(updatedWindowList)));
        
        //LRCC-1447
        
        var myDataOriginal = component.get('v.myDataOriginal');
        
        myDataOriginal.forEach(function(originalitem){
            updatedWindowList.forEach(function(item){
                if(item.Id == originalitem.Id && originalitem.Status__c == 'Firm' &&
                   (originalitem.Start_Date__c != item.Start_Date__c || 
                    originalitem.End_Date__c != item.End_Date__c ||
                    originalitem.Outside_Date__c != item.Outside_Date__c ||
                    originalitem.Tracking_Date__c != item.Tracking_Date__c)) {
                    //&& ! (! item.Start_Date__c && ! item.End_Date__c && originalitem.Status__c != item.Status__c)
                    component.set('v.firmModalOpen', true);
                    component.find("modalCmpforfirm").open();      
                }
            })                                      
        });
        
        if(component.get('v.firmModalOpen')) {
            component.set('v.firmModalData', updatedWindowList);
        }     
        
        //LRCC-1447
        if(!component.get('v.firmModalOpen')) {
            //sLRCC-1732
            if(updatedWindowList.length || notes){
                //LRCC-1018
                for(var i = 0;i<updatedWindowList.length;i++){
                   delete updatedWindowList[i].indication;
                }
                 console.log(':::::::updatedWindowList:;;;;;',updatedWindowList);
                var action = component.get("c.updateWindowRecords");
                action.setParams({ windowList : updatedWindowList, 
                                  recordsMap : JSON.stringify(recordMap),
                                  notes:JSON.stringify(notes),
                                  noteWindows:component.get("v.noteschangedWindows")});
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    console.log('::::::::;;;;;',state);
                    if (state === "SUCCESS") {
                        
                        //component.set('v.isEditable',false); //LRCC-1668
                        helper.successToast('The plan(s) has been updated successfully.');
                        //LRCC-1732
                        component.set('v.massUpdateNotesParent',{});
                        component.set('v.noteschangedWindows',[]);
                        //LRCC-1327 - Issue 
                        helper.handleCheckBoxValues(component, event, helper);
                        //LRCC-1327 - Issue1
                        
                        component.set('v.rowsToDisplay', JSON.parse(JSON.stringify( component.get('v.rowsToDisplay'))));                    
                        helper.editGrid(component, event, helper); //LRCC-1668
                        component.set('v.checkSaveRecords',true);
                        helper.clearUnsavedChanges(component, event, helper);
                        component.set('v.massUpdateNotes',{});
                        //LRCC-1636
                        component.set('v.disableSave',false);
                        component.set('v.massUpdateNotesParent',{});
                        component.find('notesComponent').set('v.notesAttachement', {});
                    }else if (state === "ERROR") {
                        //LRCC-1376
                        let mess = '';
                        for(let result of response.getError()) {
                            if(result.pageErrors && result.pageErrors.length) {
                                for(let iresult of result.pageErrors) {
                                    mess +=iresult.statusCode+' '+iresult.message;
                                }
                            }
                            if(result.fieldErrors && result.fieldErrors.length) {
                                for(let iresult of result.fieldErrors){
                                    if(iresult.CurrencyIsoCode){
                                        var error =  iresult.CurrencyIsoCode;
                                        if(!Array.isArray(error)){
                                            error = [error]
                                        }
                                        for(var ierror of error){
                                            mess +=ierror.statusCode+' '+ierror.message;
                                        }
                                    }
                                }
                            }
                        }
                        helper.errorToast(mess);
                        console.log('error::: '+JSON.stringify(response.getError()));
                        
                        console.log('error::: '+mess);
                        //LRCC-1636
                        component.set('v.disableSave',false);
                    }
                });
                $A.enqueueAction(action);
            }else {
                //LRCC-1636
                component.set('v.disableSave',false);
                helper.infoToast($A.get("$Label.c.EAW_RecordsNotEdited"));
            }
        }
    },
    infoToast : function(message) {
        
        var toastEvent = $A.get('e.force:showToast');
        toastEvent.setParams({
            'title': 'info!',
            'type': 'info',
            'message': message
        });
        toastEvent.fire();
    },
    successToast : function(message) {
        var toastEvent = $A.get('e.force:showToast');
        
        toastEvent.setParams({
            'title': 'Success!',
            'type': 'success',
            'message': message
        });
        
        toastEvent.fire();
    },
    makeUnsavedChanges: function(cmp, evt, helper) {
        var unSaved = cmp.getEvent("unsavedEvent");
        unSaved.setParams({
            "isChange": true,
            'cmpName' : 'Title Plan date Results'
        });
        unSaved.fire();
    },
    clearUnsavedChanges: function(cmp, evt, helper) {
        var unSaved = cmp.getEvent("unsavedEvent");
        unSaved.setParams({
            "isChange": false,
            'cmpName' : 'Title Plan date Results'
        });
        unSaved.fire();
    },
    setFixedHeader : function(component) {
        if(component.get('v.resultData') && component.get('v.resultData').length){  // LRCC - 1282
            console.log(':::::::::::::::;',component.getGlobalId());
            const globalId = component.getGlobalId(),
                parentnode = document.getElementById(globalId),
                thElements = parentnode.getElementsByTagName('th'),
                tdElements = parentnode.getElementsByTagName('td');
            var bodyWidth = 0;
            
            for (let i = 0; i < thElements.length; i++) {
                console.log(thElements[i].offsetWidth+':::::::::::::::'+tdElements[i]);
                tdElements[i].style.width = thElements[i].offsetWidth + 'px';
                bodyWidth += thElements[i].offsetWidth;
            }
            console.log(':::::::bodyWidth:::::::::::'+bodyWidth);
            var noOfOpenWindows = component.get('v.noOfOpenWindows') || 1;
            console.log(':::::::::::::::::'+parseInt(bodyWidth/noOfOpenWindows));
            component.set('v.bodyWidth',parseInt(bodyWidth/noOfOpenWindows));
            //cmp.set('v.bodyWidth',(cmp.get('v.myColumns').length  ) * 209 );*/
        }
    },//LRCC-1459
    generateAlphabets : function(component, event, helper) {
          console.log('generateAlphabets:::1::');
        var alphabets = [];
        for(var i= 65 ; i < 91 ; i++){
            
            alphabets.push(String.fromCharCode(i));
        }
        alphabets.push('All');
        component.set('v.alphabets',alphabets);
          //LRCC-1459
		 helper.renderPage(component, event, helper)
        
    },
    //LRCC-1459
    formPageIdMap : function(component, event, helper) {
      console.log('formPageIdMap:::::');
        const pageNumber = component.get("v.pageNumber"),
            rowsToDisplay = component.get("v.rowsToDisplay");
         console.log('formPageIdMap:::rowsToDisplay::',rowsToDisplay);
        let pageIdMap = component.get("v.pageIdMap") || {};
        //rec.TitlePlan.Id
        const recordIds = rowsToDisplay.map((rec) => rec.Id);
        
        pageIdMap[pageNumber] = recordIds;
        
        component.set("v.pageIdMap",pageIdMap);
        console.log('pageIdMap:::::test:::::::',JSON.stringify(pageIdMap));
        if(component.find('pagePerRecord')){
            let pagesPerRecord = parseInt(component.find('pagePerRecord').get("v.value"));
            component.set("v.maxPage", Math.floor((component.get('v.totalRecordCount') + pagesPerRecord - 1 )/pagesPerRecord)); 
            console.log('maxPage count ::::',component.get('v.maxPage'));
        }
        //LRCC-1646
        if(component.get('v.isDelete')) {
            
            let isEditable = component.get('v.isEditable');
            console.log('pageIdMap::isEditable:',component.get('v.isEditable'));
            let editTableEvent = $A.get('e.c:EAW_EditTableEvent');
            editTableEvent.setParams({
                'editMode': true
            });
            editTableEvent.fire();
            component.set('v.isEditable',true);
        }
        //LRCC-1690
        var childCmp = component.find('windowTable');
        if(childCmp){
             childCmp.toSort(); 
        }
        
    },
    //LRCC-1459
    reinitializePageMap : function(component,pageNumber) {
        
        let pageIdMap = component.get("v.pageIdMap");
        
        for(let i = pageNumber ; i <= Object.keys(pageIdMap).length ; i++ ){
            pageIdMap[i] = [];
        } 
        component.set("v.pageIdMap",pageIdMap);
    },
    //LRCC-1459
    renderPage: function(component, event, helper) {
        
        console.log('call render page:::1::',component.get("v.resultData"));
        let records = component.get("v.resultData") || [];
        
        //var pageNumber = component.get("v.pageNumber");
        //var pagesPerRecord = parseInt(component.find('pagePerRecord').get("v.value"));
        //var  rowsToDisplay = records;
        
        component.set("v.rowsToDisplay", records);
        console.log('rowsToDisplay::test:::',records);
        helper.formPageIdMap(component);
    },
    
    //LRCC-1459
    showErrorToast : function(component,msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: "Info!",
            message:msg ,
            type: "info"
        });
        toastEvent.fire();
    },
})