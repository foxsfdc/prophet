({
    setFieldColumns : function(cmp,event,helper,columns,fieldWrapper) {
        var fieldWrapperList = [];
        for(var i of columns){
            fieldWrapper.find(function(element){
                if(element.fieldName == i.value){
                    fieldWrapperList.push(element);
                    if(i.value == 'License_Type__c'){
                        fieldWrapperList.push({fieldName : 'startDateJSON'},{fieldName : 'endDateJSON'});
                    }
                }
            });
        }
       // console.log('::list:::',fieldWrapperList);
        cmp.set('v.fieldWrapper',fieldWrapperList);
    },
    openModal : function(cmp,event,helper,message){
        cmp.find('promt').set('v.content',message);
        cmp.find('promt').openModal();
    },
    checkForOverridden : function(cmp,event,helper){
        var originalSet = cmp.get('v.originalSet');
        var windowStrandList = cmp.get('v.windowStrandList');
        for(var i of originalSet){
            windowStrandList.find(function(element){
                if(element.Id && element.Id == i.Id){
                    helper.checkAndSetOverriden(element,Object.keys(element),i);
                } else if(!element.Id){
                    helper.setWindowStrandName(cmp,element);
                }
            });
        }
    },
    checkAndSetOverriden : function(changedWindowStrand,fieldList,originalRecord){
        for(var field of fieldList){
            if(field != 'Is_Overridden__c'){
                if(originalRecord[field] ){
                    if(originalRecord[field] != changedWindowStrand[field] ){
                        //console.log('here:::',originalRecord[field],changedWindowStrand[field]);
                        changedWindowStrand.Is_Overridden__c = true;
                    } else if($A.util.isEmpty(originalRecord.Is_Overridden__c)){
                        changedWindowStrand.Is_Overridden__c = false;
                    }
                } else if(!$A.util.isEmpty(changedWindowStrand[field])){ 
                    changedWindowStrand.Is_Overridden__c = true;
                } else{
                     changedWindowStrand.Is_Overridden__c = false;
                }
            }
        }
    },
    setWindowStrandName : function(cmp,windowStrand){
        if(windowStrand['License_Type__c'] && cmp.get('v.windowRecord')){
            windowStrand.Name = cmp.get('v.windowRecord').Name + ' - ' + windowStrand['License_Type__c'];
        }
    }
})