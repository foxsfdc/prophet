({
	init : function(cmp, event, helper) {
        console.log(':::::',cmp.get('v.windowStrandList'));
        cmp.set('v.originalSet',JSON.parse(JSON.stringify(cmp.get('v.windowStrandList'))));
       // console.log(':::',cmp.get('v.fieldWrapper'));
       /* for(var i of cmp.get('v.windowStrandList')){
            if(i.Is_Overridden__c){
                cmp.set('v.isOverridden',true);
                break;
            }
        }*/
        var columns = [{label : 'Licence Type',
                              value : 'License_Type__c'},
                             {label : 'Start Date',
                              value : 'startDate'},
                             {label : 'End Date',
                              value : 'endDate'},
                             {label : 'Media',
                              value : 'Media__c'},
                             {label : 'Territory',
                              value : 'Territory__c'},
                             {label : 'Language',
                              value : 'Language__c'}
                            ];
        helper.setFieldColumns(cmp,event,helper,columns,cmp.get('v.fieldWrapper'));
        cmp.set('v.columns',columns);
	},
	addWindowStrand : function(cmp,event,helper){
		cmp.set('v.spinner',true);
		var windowStrandList = cmp.get('v.windowStrandList');
		var windowStrand = JSON.parse(JSON.stringify(cmp.get('v.windowStrand')));
        windowStrand.Is_Overridden__c = true;
        windowStrand.Window__c = cmp.get('v.windowId');
		windowStrandList.push(windowStrand);
		cmp.set('v.windowStrandList',windowStrandList);
		cmp.set('v.spinner',false);
	},
	removeWindowStrand : function(cmp,event,helper){
        var windowStrandIndex = event.getSource().get('v.name');
        var windowStrandList = JSON.parse(JSON.stringify(cmp.get('v.windowStrandList')));
        var windowStrand = windowStrandList[windowStrandIndex];
        if(!$A.util.isEmpty(windowStrand.Id)){
            cmp.set('v.deleteIndex',windowStrandIndex);
            helper.openModal(cmp,event,helper,'Sure want to delete this Window Strand ?');
        } else{
            windowStrandList.splice(windowStrandIndex,1);
            cmp.set('v.windowStrandList',windowStrandList);
        }
    },
    checkForNullValues : function(cmp,event,helper){
        var isNull = false;
        var inputForm = cmp.find('inputForm');
        if(!Array.isArray(inputForm)){
            inputForm = [inputForm];
        }
        for(var i of inputForm){
            if(i.checkForValues()){
                isNull = true;
            }
        }
        if(!isNull){
            helper.checkForOverridden(cmp,event,helper);
        }
        return isNull;
    },
    deleteRuleDetail : function(cmp,event,helper){
       // console.log(':::',cmp.get('v.deleteIndex'));
        var windowStrandsToDelete = cmp.get('v.windowStrandsToDelete');
        var windowStrandList = JSON.parse(JSON.stringify(cmp.get('v.windowStrandList')));
        var windowStrand = windowStrandList[cmp.get('v.deleteIndex')];
        windowStrandsToDelete.push(windowStrand.Id);
        windowStrandList.splice(cmp.get('v.deleteIndex'),1);
		cmp.set('v.windowStrandsToDelete',windowStrandsToDelete);        
        cmp.set('v.windowStrandList',windowStrandList);
    },
    restoreStrand : function(cmp,event,helper){
        var restoreEvent = cmp.getEvent("restoreEvent");
        restoreEvent.fire();
    }
 })