({	
    init : function(cmp,event,helper){   
        console.log('init:::',cmp.get('v.recordId'))
        if(cmp.get('v.rule') && cmp.get('v.parentRuleDetailList') && cmp.get('v.parentRuleDetailList').length){
            cmp.set('v.originalParentRuleDetailList',JSON.parse(JSON.stringify(cmp.get('v.parentRuleDetailList'))));
           	cmp.set('v.originalRuleDetailList',JSON.parse(JSON.stringify(cmp.get('v.ruleDetailList'))));
            cmp.set('v.originalRule',JSON.parse(JSON.stringify(cmp.get('v.rule'))));
            helper.setRuleDetailByRules(cmp,event,helper,cmp.get('v.parentRuleDetailList')); 
            cmp.set('v.isNew',false);
        } else if(cmp.get('v.rule') && cmp.get('v.rule')['Condition_Type__c']){
            cmp.set('v.selectedContext',cmp.get('v.rule')['Condition_Type__c']); 
        }
        else{
            cmp.set('v.selectedContext',''); 
            cmp.set('v.isNew',true);
        }
    },
    handleSelect : function(cmp, event, helper) {
        if(event.getParam("value") == 'earliestLatest'){
            helper.setEarliestLatestBasedNode(cmp,event,helper);
        } else if(event.getParam("value") == 'condition'){
            helper.setConditionBasedNode(cmp,event,helper);
        } else if(event.getParam("value") == 'date'){
             helper.setDateBasedNode(cmp,event,helper);
        } else if(event.getParam("value") == 'multidate'){
            helper.setCaseBasedNode(cmp,event,helper);          
        } else{
            var rule = cmp.get('v.rule');
            rule.Condition_Type__c = event.getParam("value");
            cmp.set('v.rule',rule); 
        }
        cmp.set('v.selectedContext',event.getParam("value"));
    },
    removeWholeNode : function(cmp,event,helper){
        //console.log(cmp.get('v.rule'))
        if(cmp.get('v.rule').Id){
            helper.openModal(cmp,event,helper,'Delete this Rule and its Rule details?');
        } else{
            cmp.set('v.rule',{'sObjectType' : 'EAW_Rule__c'});
            cmp.set('v.selectedContext','');
        }        
        
    },
    saveRuleDetails : function(cmp,event,helper){
        var conditionType = cmp.get('v.selectedContext');
        //console.log(conditionType);
        var rule = cmp.get('v.rule');
       
        /**LRCC:769**/
        //rule.Operator__c = 'IN';
        var overRidden = false;
        rule.Nested__c = true;
        if(rule.Rule_Overridden__c){
            //rule.Rule_Overridden__c = false;
        }
        if(conditionType == 'condition') {
        
            rule.Condition_Type__c = 'if'; 
            cmp.set('v.rule',rule);
            
            var conditionBasedMap = cmp.get('v.conditionBasedMap');
            var conditionList = [];
            conditionBasedMap.TrueBased.ruleDetailJSON.ruleDetail.Condition_Type__c = 'If';
            conditionList.push(conditionBasedMap.TrueBased.ruleDetailJSON);
            conditionBasedMap.FalseBased.ruleDetailJSON.ruleDetail.Condition_Type__c = 'Else/If';
            conditionList.push(conditionBasedMap.FalseBased.ruleDetailJSON);
            
            var notValid = false;
            overRidden = false;
            //console.log('conditionlist:::',JSON.stringify(conditionList));
            for(var i=0;i<conditionList.length;i++){
                conditionList[i].ruleDetail.Rule_Order__c = i+1;
                var temp = helper.checkForNullNodes(cmp,event,helper,conditionList[i]);
                if(helper.earliestLatestOverridden(cmp,event,helper,conditionList[i],false)){
                    overRidden = true;
                }
                if(temp){
                    notValid = true;
                    break;
                }
            }
            //console.log(cmp.get('v.rule'));
           // console.log(cmp.get('v.originalRule'));
            if(helper.checkForOverridden(cmp.get('v.rule'),[cmp.get('v.originalRule')])){
                overRidden = true;
            }
            cmp.set('v.isOverridden',overRidden);
            //LRCC-1454 conditionList.length > 1 &&
            if( conditionList.length && (conditionList[0].ruleDetail.hasOwnProperty('Conditional_Operand_Id__c') ||conditionList[0].ruleDetail.hasOwnProperty('Condition_Date_Text__c') || conditionList[0].ruleDetail.hasOwnProperty('Condition_Timeframe__c') ) && !notValid && !helper.checkForRule(cmp,event,helper,cmp.get('v.rule'))){
                //console.log('its working ::::::::::::::::::::::::::ddd::::::::::::::::::::::::::::::::',JSON.stringify(conditionList));
                if((!conditionList[1].ruleDetail.hasOwnProperty('Conditional_Operand_Id__c') && !conditionList[1].ruleDetail.hasOwnProperty('Condition_Date_Text__c') && !conditionList[1].ruleDetail.hasOwnProperty('Condition_Timeframe__c'))){
                	
                    var tempConditionList = conditionList;
                    conditionList.splice(1,1);
                    conditionList = tempConditionList;
                }
                //console.log('its working ::::after:::::::::::::::',JSON.stringify(conditionList));
                return conditionList;
            } else{
                return null;
            }
            
        } else if(conditionType == 'earliestLatest'){
            overRidden = false;
          //  rule.Condition_Type__c = 'If'; 
            cmp.set('v.rule',rule);
            //console.log('earliestLatest::',cmp.get('v.newRuleDetailJSON'));
            if(helper.earliestLatestOverridden(cmp,event,helper,cmp.get('v.newRuleDetailJSON'),false) || helper.checkForOverridden(cmp.get('v.rule'),[cmp.get('v.originalRule')])){
                overRidden = true;
            }
            cmp.set('v.isOverridden',overRidden);
            if(!helper.checkForNullNodes(cmp,event,helper,cmp.get('v.newRuleDetailJSON')) && ((cmp.get('v.newRuleDetailJSON').dateCalcs.length) || (cmp.get('v.newRuleDetailJSON').nodeCalcs.length))){
                return [cmp.get('v.newRuleDetailJSON')];      
            } else{
                return null;
            }
        } else if(conditionType == 'multidate'){
            overRidden = false;
            var caseList = JSON.parse(JSON.stringify(cmp.get('v.caseRuleDetailMap').whenBased));
            for(var i=0;i<caseList.length;i++){
                caseList[i].ruleDetail.Condition_Type__c ='Case then';
            }
            cmp.get('v.caseRuleDetailMap').otherBased.ruleDetail.Rule_Order__c = cmp.get('v.caseRuleDetailMap').whenBased.length + 1;
            cmp.get('v.caseRuleDetailMap').otherBased.ruleDetail.Condition_Type__c = 'Otherwise';
            if(cmp.get('v.caseRuleDetailMap').otherBased.nodeCalcs.length && ((cmp.get('v.caseRuleDetailMap').otherBased.nodeCalcs[0].ruleDetail.hasOwnProperty('Condition_Type__c') && (cmp.get('v.caseRuleDetailMap').otherBased.nodeCalcs[0].ruleDetail.Condition_Type__c != 'Case') && cmp.get('v.caseRuleDetailMap').otherBased.nodeCalcs[0].ruleDetail.Condition_Type__c != 'OtherWise')|| cmp.get('v.caseRuleDetailMap').otherBased.nodeCalcs[0].ruleDetail.hasOwnProperty('Conditional_Operand_Id__c') || cmp.get('v.caseRuleDetailMap').otherBased.nodeCalcs[0].ruleDetail.hasOwnProperty('Condition_Timeframe__c'))){ // LRCC - 1555
            	caseList.push(cmp.get('v.caseRuleDetailMap').otherBased);
            } else{
                if(cmp.get('v.caseRuleDetailMap').otherBased.ruleDetail.Id){
                    var ruleDetailsToDelete = cmp.get('v.ruleDetailstoDelete')
                    ruleDetailsToDelete.push(cmp.get('v.caseRuleDetailMap').otherBased.ruleDetail.Id);
                    cmp.set('v.ruleDetailstoDelete',ruleDetailsToDelete);
                }
            }
            cmp.get('v.rule').Condition_Type__c = 'case';
            //console.log(caseList);
            var notValid = false;
            for(var i=0;i<caseList.length;i++){
                caseList[i].ruleDetail.Rule_Order__c = i+1;
                var temp = helper.validForCase(cmp,event,helper,caseList[i],notValid);
                if(helper.earliestLatestOverridden(cmp,event,helper,caseList[i],false) || helper.checkForOverridden(cmp.get('v.rule'),[cmp.get('v.originalRule')])){
                    overRidden = true;
                }
                if(temp){
                    notValid = true;
                    break;
                }
            }
            //console.log(notValid)
            cmp.set('v.isOverridden',overRidden);
            if(caseList.length >= 1 && !notValid){
               return caseList;
            } else{
                return null;
            }
        } else if(conditionType == 'date'){
            overRidden = false;
            var rule = cmp.get('v.rule');
            rule.Nested__c = false;
           // console.log('date::',cmp.get('v.dateCalcBasedMap'));
            var newRuleDetailJSON = {
                'ruleDetail' : cmp.get('v.dateCalcBasedMap'),
                'dateCalcs' : [],
                'nodeCalcs' : []                
            };  
            //console.log('parentRuleDetail:::',cmp.get('v.originalParentRuleDetailList'));
            //console.log('::::',helper.checkForOverridden(cmp.get('v.dateCalcBasedMap'),cmp.get('v.originalParentRuleDetailList')));
            //console.log('::::::',cmp.get('v.rule'),cmp.get('v.originalRule'));
            if(helper.checkForOverridden(cmp.get('v.dateCalcBasedMap'),cmp.get('v.originalParentRuleDetailList')) || helper.checkForOverridden(cmp.get('v.rule'),[cmp.get('v.originalRule')])){
                overRidden = true;
            }
            cmp.set('v.isOverridden',overRidden);
            if(!helper.validNodes(cmp,event,helper,false,[cmp.get('v.dateCalcBasedMap')],[])){
                return [newRuleDetailJSON];
            } else{
                return null;
            }
        }else if(conditionType == 'TBD' || conditionType == 'Perpetuity' || conditionType == 'Rights End Date'){
            var rule= cmp.get('v.rule');
            if(helper.checkForOverridden(cmp.get('v.rule'),[cmp.get('v.originalRule')],false)){
                cmp.set('v.isOverridden',true);
            }
            rule['Condition_Type__c'] = conditionType;
            var newRuleDetailJSON = {
                'ruleDetail' : null,
                'dateCalcs' : [],
                'nodeCalcs' : []                
            };
            return [newRuleDetailJSON];
        } else{
            return true;
        }
        
    },
    deleteRuleDetailById : function(cmp,event,helper){
        var ruleDetailId = event.getParam("ruleDetailId");
        var isDateCalc = event.getParam("isDateCalc");
        var toDeleteList = cmp.get('v.ruleDetailstoDelete') || [];
        toDeleteList.push(ruleDetailId);
        cmp.set('v.ruleDetailstoDelete',toDeleteList);
    },
    deleteRuleDetail : function(cmp,event,helper){
        event.stopPropagation();
        helper.deleteRuleDetailById(cmp,event,helper,cmp.get('v.rule').Id,false,true);     
        cmp.set('v.rule',{'sObjectType' : 'EAW_Rule__c'});
        cmp.set('v.selectedContext','');
        cmp.set('v.ruleDetailstoDelete',[]);
    }
})