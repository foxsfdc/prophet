({
	rowAction : function(component, event, helper) {
        var recId = event.getParam('row').Id;
        var actionName = event.getParam('action').name;
        
        if ( actionName == 'notes' ) {
            
            var notesEvent = $A.get("e.c:EAW_NotesEvent");
            notesEvent.setParams({"parentId":recId});
            notesEvent.fire();
        }        	
    },
    
    getSelectedRow : function(component, event, helper) {
        var selectedRows = event.getParam('selectedRows');
        component.set("v.selectedRows", selectedRows);
    },
    
    //LRCC-1430
    handleSort : function(component,event,helper){
        
        var sortBy = event.getParam("fieldName");        
        var sortDirection = event.getParam("sortDirection"); 
        
        component.set("v.sortBy",sortBy);
        component.set("v.sortDirection",sortDirection);
        
        helper.sortData(component,sortBy,sortDirection);
    }
})