({
    //LRCC-1430
	sortData : function(component,fieldName,sortDirection){
        
        if(fieldName == 'Name') {
            fieldName = 'Title_Name';
        }
        
        var data = component.get("v.myData");  
        
        var key = function(a) { return a[fieldName]; }
        var reverse = sortDirection == 'asc' ? 1: -1;
        
        /*
        if(fieldName == ''){ 
            data.sort(function(a,b){
                var a = key(a) ? key(a) : '';
                var b = key(b) ? key(b) : '';
                return reverse * ((a>b) - (b>a));
            }); 
        }
        else{
        */
            data.sort(function(a,b){ 
               
                var a = key(a) ? key(a).toLowerCase() : '';
                var b = key(b) ? key(b).toLowerCase() : '';
                
                return reverse * ((a>b) - (b>a));
            });    
        //}
        
        component.set("v.myData",data);
    }
})