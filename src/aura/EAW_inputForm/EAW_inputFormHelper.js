({
	checkForNull : function(cmp,event,helper) {
        
        //console.log(cmp.get("v.field.fieldName"),cmp.get('v.record')[cmp.get("v.field.fieldName")] );
 		//console.log(if(cmp.get('v.record')[cmp.get("v.field.fieldName")] == null);
         if($A.util.isEmpty(cmp.find('inputField').get('v.value')) || cmp.find('inputField').get('v.value') == null || cmp.find('inputField').get('v.value') == '') {           
           	//console.log(cmp.get('v.record')[cmp.get("v.field.fieldName")]);
            $A.util.addClass(cmp.find('inputField'),'slds-has-error');
            return true;
         } else{
             if(cmp.get("v.field.fieldName") == 'Condition_Year__c' &&( cmp.find('inputField').get('v.value').length != 4 || !(!isNaN(parseFloat(cmp.find('inputField').get('v.value'))) && isFinite(cmp.find('inputField').get('v.value'))))){
                 
                 $A.util.addClass(cmp.find('inputField'),'slds-has-error');
                 return true;
             }
            cmp.get('v.record')[cmp.get("v.field.fieldName")] = cmp.find('inputField').get('v.value');
            helper.clearErrors(cmp,event,helper);  
            return false;
        }	
	},
    clearErrors : function(cmp,event,helper){
        
        var inputField = cmp.find('inputField');
        if($A.util.hasClass(inputField,'slds-has-error')){
            $A.util.removeClass(inputField,'slds-has-error');
        }

    }
})