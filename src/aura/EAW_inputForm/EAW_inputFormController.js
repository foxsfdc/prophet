({
    init : function(cmp,event,helper){
        
        //console.log(':::::value::::'+cmp.get('v.record'));
        //console.log(cmp.get("v.field.fieldName"),cmp.get('v.record')[cmp.get("v.field.fieldName")] );

        if(cmp.get('v.record')){
            if(cmp.get('v.record')[cmp.get("v.field.fieldName")] && cmp.get('v.record')[cmp.get("v.field.fieldName")] != null /*&& cmp.find('inputField')*/) {
                if(cmp.find('inputField'))cmp.find('inputField').set('v.value',cmp.get('v.record')[cmp.get("v.field.fieldName")]);
                cmp.set('v.inputCmpValue',cmp.get('v.record')[cmp.get("v.field.fieldName")]);
                //console.log(cmp.get('v.inputCmpValue'));
            }
        }
     
 		//console.log('finished');
    },
	changeHandler : function(cmp, event, helper) {
        
        cmp.get('v.record')[cmp.get("v.field.fieldName")] = cmp.find('inputField').get('v.value');
        cmp.set('v.inputCmpValue',cmp.get('v.record')[cmp.get("v.field.fieldName")]);
        //LRCC-1443
		var tempRecord = cmp.get('v.record');
	    cmp.set('v.record',tempRecord);
       
        helper.clearErrors(cmp,event,helper);
        if(cmp.get('v.event') == true){
            var fieldEvent = cmp.getEvent('fieldEvent');
            fieldEvent.setParams({
                "fieldName":cmp.get("v.field.fieldName")
            })
            fieldEvent.fire();
        }
	},
    checkForValue: function(cmp,event,helper){
        
        return helper.checkForNull(cmp,event,helper);            
        
    },
    setChangedData : function(cmp,event,helper){
        if(cmp.get('v.instantReflect') && cmp.find('inputField') && cmp.find('inputField').get('v.value')){
            cmp.get('v.record')[cmp.get("v.field.fieldName")] = cmp.find('inputField').get('v.value');
        }
    },
    hideOtherList : function(cmp,event){
        var showListEvent = $A.get("e.c:showListEvent");
        showListEvent.setParams({
            "selectedList" :  event.getSource().get('v.name')
        });
        showListEvent.fire();
    }
    
})