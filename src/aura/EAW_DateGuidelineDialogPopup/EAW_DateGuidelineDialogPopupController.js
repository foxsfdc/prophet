({
	getDateCalc : function(component, event, helper) {
        console.log('::::::: rdgRecId ::::::::'+JSON.stringify(event.getParams().arguments));
		var rdgRecId = event.getParams().arguments.rdgId;
        var titleRecId = event.getParams().arguments.titleId;
		console.log('::::::: rdgRecId ::::::::'+rdgRecId);
        console.log('::::::: titleRecId ::::::::'+titleRecId);
        if(rdgRecId){
            component.set("v.spinnerFlag", true);
            var action = component.get("c.getReleaseDatesMapFromOperandRDG");
            action.setParam("rdgId", rdgRecId);
            action.setParam("titleId", titleRecId);
            action.setCallback(this, function(response) {
                var actionState = response.getState();
                console.log('::: actionState :::', actionState);
                if (actionState == 'SUCCESS') {
                    var results = response.getReturnValue();
                    console.log('Results::::::',results);
                    if(results){
                        component.set('v.releaseDateGuidelineId',rdgRecId);
                        component.set('v.ruleDetailDateMap',results.ruleDetailReleaseDateMap);
                        component.set('v.ruleDetailStatusMap',results.ruleDetailReleaseDateStatusMap);
                        component.set('v.showModalPopup', true);
                        $A.util.addClass(component.find('modal'),'slds-fade-in-open');
                        $A.util.addClass(component.find('backDrop'),'slds-backdrop_open');
                    }
                }
                component.set("v.spinnerFlag", false);
            });
            $A.enqueueAction(action);
        }
	},
	closeModal : function(component){
        component.set('v.showModalPopup', false);
        $A.util.removeClass(component.find('modal'),'slds-fade-in-open');
        $A.util.removeClass(component.find('backDrop'),'slds-backdrop_open');
    }
})