({
    fieldSet: function (component, event, helper) {
        try {
            component.set('v.spinner',true);
            var sObjectName = component.get('v.sObjectName');
            var param = {};
            if(sObjectName) {
                param[sObjectName] = "Detail_Page_Fields";
            }
            console.log('sObjectName:::',sObjectName);
            console.log('recordId:::',component.get('v.recordId'));
            var action = component.get("c.getFields");
            action.setParams({
                "objFieldSetMap": param
            });
            
            action.setCallback(this, function(response) {
                var state = response.getState();
                
                if(state === "SUCCESS") {
                    
                    console.log(';;;;',response.getReturnValue());
                    
                    var fieldNames = response.getReturnValue()[0];  
                    
                    if(fieldNames.length) {
                        for(var field of fieldNames) {
                            if(field.fieldName == 'Status__c' && component.get('v.sObjectName') == 'EAW_Window__c') {
                                field.picklistValues.splice(0, 0, {'label' : '--None--', 'value' : ''});
                            }
                        }
                    }
                    
                    component.set('v.fields', JSON.parse(JSON.stringify(fieldNames)));
                    
                } else if(state === "ERROR") {
                    var errors = response.getError();
                    
                    if(errors) {
                        if(errors[0] && errors[0].message) {
                            console.log("Error message: " +
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                } else {
                    console.log("Something went wrong, Please check with your admin");
                }
                component.set('v.spinner',false);
            });
            $A.enqueueAction(action);
        } catch (e) {
            console.log('Exception: ' + e);
        }
    },
    
    toast : function(type,message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            'type': type,
            'message': message
        });
        toastEvent.fire();
    }
})