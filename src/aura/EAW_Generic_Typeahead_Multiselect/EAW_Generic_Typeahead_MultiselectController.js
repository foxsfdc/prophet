({
    // LRCC:1232 start
    init : function(component, event, helper) {
        
        //Tags strings to Object changes 

        let def = component.get('v.defaultValue');
        let selectedRecordValues = component.get('v.selectedRecordValues');
        //console.log('def::;',def);
       // console.log('selectedRecordValues::;',selectedRecordValues);
        if(def) {
            
            var defElements = def.split(',');
            var temp = [];
            for (let element of defElements) {
                temp.push(element.trim());
            }
            defElements = temp;
            
            let selectedTitles = JSON.parse(JSON.stringify(component.get('v.selectedObjects')));
            selectedTitles = [];
            
            for(let defelement of defElements) {
                if (component.get('v.isObjArrayField')) {
                    for(let row of selectedRecordValues) {
                        var title;
                        if (row.Name) { title = row.Name; }
                        else { title = row.title; }
                        if(defelement == title) {    
                            var rowId;
                            if (row.Id) { rowId = row.Id; }
                            else { rowId = row.id; }
                            selectedTitles.push({'id' : rowId, 'title' : title, 'icon' : 'standard:account'});
                        }
                    }
                } else {
                    selectedTitles.push({'title' : defelement, 'icon' : 'standard:account'});
                }
            }
            component.set('v.selectedObjects',selectedTitles);
        }
    },
    
    searchTermFind : function(component, event, helper) {
        // LRCC:1232 end
        console.log('searchTermFind');
        const action = component.get('c.search');
        const type = component.get('v.genericType');
        const name = component.get('v.genericName');
        const searchTerm = event.getParam("searchTerm");
        //LRCC-1592
        const isWGstrandNewOverride = component.get('v.isWGstrandNewOverride');
        
        action.setParams({
            genericType: type,
            genericName: name,
            searchTerm: searchTerm,
            isWGstrandNewOverride: isWGstrandNewOverride //LRCC-1592
        });
        
        action.setCallback(this, (response) => {
            const state = response.getState();
            console.log('state:::',state);
            if(state === 'SUCCESS') {
            	console.log('success');
                const returnValue = response.getReturnValue();
            
                let def = component.get('v.defaultValue');
                let searchResults = component.get('v.searchResults');
            	
                if((type == 'EAW_Customer__c' || type == 'EDM_GLOBAL_TITLE__c' || type == 'EAW_Title__c') && !searchResults) {
                	for(let value of returnValue) {
            			value.title = value.titleId + ': ' + value.title;
                	}
                }
            	
				console.log('<<returnValue>>'+JSON.stringify(returnValue));
                component.set('v.totalRecords', returnValue);
        
				//Tags String to object

                if(def != null) {
                    
                    var defElements = def.split(',');
                    var temp = [];
                    for (let element of defElements) {
                        temp.push(element.trim());
                    }
                    defElements = temp;
                    
	                let selectedTitles = JSON.parse(JSON.stringify(component.get('v.selectedObjects')));
                    selectedTitles = [];
                    
                    for(let defelement of defElements) {
                        for(let row of returnValue) {                        
                            
                            if(defelement == row.title) {
                                selectedTitles.push(row);
                            }
                        }
                    }
                    
	                component.set('v.selectedObjects',selectedTitles);
                }
        		component.set('v.inputSpinner', false);
        	}
    	});
        
        action.setStorable();
		//action.setBackground();
        $A.enqueueAction(action);
    },
    
    handleTotalRecordsLoaded : function(component, event, helper) {
        helper.hideSpinner(component);
    },
    
    lookupSearch : function(component, event, helper) {
        const serverSearchAction = component.get('c.search');
        console.log('<<const>>'+serverSearchAction);
        component.find('lookup').search(serverSearchAction);
    }
})