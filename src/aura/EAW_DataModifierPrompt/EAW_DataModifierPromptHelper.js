({
    fireEvent : function(cmp,value,field,confirm) {
        var cmpEvent = cmp.getEvent("dataModifierEvent");
        cmpEvent.setParams({
            'valueToChange' : value,
            'field' : field,
            'confirmation' : confirm
        });
        cmpEvent.fire();
    }
})