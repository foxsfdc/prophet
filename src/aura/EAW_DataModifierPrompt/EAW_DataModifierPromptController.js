({
    openPrompt : function(cmp, event, helper) {
        var params = event.getParam('arguments');
        
        if(params.requestField && params.oldValue && params.newValue){
            console.log('::::::'+JSON.stringify(params));
            cmp.set('v.oldValue',params.oldValue);
            cmp.set('v.newValue',params.newValue);
            cmp.set('v.requestField',params.requestField); 
            if(params.message){
                cmp.set('v.message',params.message); 
            }
            $A.util.addClass(cmp.find('modal'),'slds-fade-in-open');
            $A.util.addClass(cmp.find('backDrop'),'slds-backdrop_open');
        }
    },
    confirm : function(cmp,event,helper){
        helper.fireEvent(cmp,cmp.get('v.newValue'),cmp.get('v.requestField'),true);
        $A.util.removeClass(cmp.find('modal'),'slds-fade-in-open');
        $A.util.removeClass(cmp.find('backDrop'),'slds-backdrop_open');
    },
    cancel : function(cmp,event,helper){
        helper.fireEvent(cmp,cmp.get('v.oldValue'),cmp.get('v.requestField'),false);
        $A.util.removeClass(cmp.find('modal'),'slds-fade-in-open');
        $A.util.removeClass(cmp.find('backDrop'),'slds-backdrop_open');
    }
    
})