({
    errorToast : function(message) {
        let toastEvent = $A.get('e.force:showToast');

        toastEvent.setParams({
            'title': 'Error!',
            'type': 'error',
            'message': message
        });

        toastEvent.fire();
    },

    successToast : function(message) {
        let toastEvent = $A.get('e.force:showToast');

        toastEvent.setParams({
            'title': 'Success!',
            'type': 'success',
            'message': message
        });

        toastEvent.fire();
    },
    
    collectIds : function(selectedResults) {
        let collectedIds = [];

        if(selectedResults && selectedResults.length > 0) {
            for(let result of selectedResults) {
                collectedIds.push(result.id);
            }
        }

        return collectedIds;
    },
    
    clearInputsAndHide : function (component) {
    
        document.getElementById("EAW_TitlePlanWindowMassUpdatePopup").style.display = "none";
        //component.set('v.windowList',null);
        //component.set('v.windowList',[]);
        component.set('v.selectedTags',[]);
        component.set('v.statusValue','');
        component.set('v.startDate', '');
        component.set('v.endDate', '');
        component.find('body').set('v.value','');
        //component.find('clearStartDateRadio').getElement().checked = false;
        //component.find('clearEndDateRadio').getElement().checked = false;
        component.find('clearTagsCheckbox').getElement().checked = false;
        component.set('v.endDateText','');
        component.set('v.startDateText','');
        //LRCC-1327
        var editTableEvent = $A.get('e.c:EAW_EditTableEvent');
        editTableEvent.setParams({
            'editMode': true
        });
        editTableEvent.fire();
        //var clearCheckedValuesEvent = component.getEvent("ClearCheckedValues");
        //clearCheckedValuesEvent.fire();
       // component.find('retiredCheckbox').getElement().checked = false;
        component.set('v.disableStartDateFlag',null);
        component.set('v.disableEndDateFlag', null);
        component.set('v.disableWindowFlag', null);
        // LRCC-1380
        component.set('v.isEndDateModified',false);
        component.set('v.isStartDateModified',false);
        component.find('title').set('v.value','');
        component.find("endDateTBD").getElement().checked=false;
	   	component.find("endDatePerpetuity").getElement().checked=false;
	   	component.find("clearEndDate").getElement().checked=false;
	   	component.find("clearStartDate").getElement().checked=false;
        component.find("startDateTBD").getElement().checked=false;
        // LRCC-1010 && 1327
        console.log('recordMap::::before::',JSON.stringify(component.get('v.recordMap')));
        var recordMap = component.get('v.recordMap');
    	var windowkeys =  Object.keys(recordMap);
    	var clearWindowTagMap = component.get('v.clearWindowTagMap');
    	   console.log('clearWindowTagMap::::::',JSON.stringify(component.get('v.clearWindowTagMap')));
    	for(var i = 0 ; i < windowkeys.length ; i++){
    	
    		if(clearWindowTagMap && clearWindowTagMap['clearWindowTagWindowIds'] && clearWindowTagMap['clearWindowTagWindowIds'].indexOf(windowkeys[i]) > -1){
    			recordMap[windowkeys[i]] = [] ;
    		}
    	}
    	component.set('v.recordMap',recordMap);
    	console.log('recordMap::::after::',JSON.stringify(component.get('v.recordMap')));
       
    }
})