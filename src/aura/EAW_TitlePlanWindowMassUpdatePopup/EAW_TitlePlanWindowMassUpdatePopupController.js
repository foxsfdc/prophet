({
	doInit : function(component,event,helper){
		console.log('init');
    	var action = component.get('c.getPicklistValues');
    	
    	var objectPickListMap =  {
					"EAW_Window__c" : ['Status__c']  // In future if we need to retrieve values for multiple picklist then send the values like this ['Status__c','Window_Type__c','Right_Status__c']
				};
    	action.setParams(
			{
				'objectPickListString' : JSON.stringify(objectPickListMap)
			    	 
			}
	    );
	    action.setCallback(this, function (response) {
            var state = response.getState();

            if (state === 'SUCCESS'){ 
            	console.log('response::::',response.getReturnValue());
            	var pickListMap = response.getReturnValue();
            	component.set('v.statusPickList',pickListMap['Status__c']);
            } else if(state == 'ERROR'){
            	console.log(response.getError());
            }      
	    });
	    $A.enqueueAction(action);
	},
	//LRCC-1380 - Modified validation logic
    updateWindows : function(component, event, helper) { 

        // Validate Inputs
        // Blank Start Date/End Date == clear them
        // Blank Tags == Clear them
        // Status MUST be there

        //let windowList = component.get('v.windowList') ? component.get('v.windowList') : [];
        
        console.log('::: WindowList :::::'+JSON.stringify(component.get('v.windowList')));
        //LRCC-1732
        var tempNoteschangedWindows = component.get('v.windowList');
        component.set('v.noteschangedWindows',JSON.parse(JSON.stringify(tempNoteschangedWindows)));
        console.log('::: noteschangedWindows :::::'+JSON.stringify(component.get('v.noteschangedWindows')));
        
        console.dir(component.get('v.windowList'));
        
        let isSelectedAnyOneItem = false;
        let windowList = [];
        for(let window of component.get('v.windowList')) {
            windowList.push(window);
        }
        
        console.log('::: WindowList - New :::::'+JSON.stringify(windowList));
        
        let windowStatus =  component.get('v.statusValue');
        let startDate = component.find('startDateInput').get('v.value') ? component.find('startDateInput').get('v.value') : '';
        if(startDate){
        	component.set('v.isStartDateModified',true);
        	isSelectedAnyOneItem=true;
        }
        let endDate = component.find('endDateInput').get('v.value') ? component.find('endDateInput').get('v.value') : '';
        if(endDate){
        	component.set('v.isEndDateModified',true);
        	isSelectedAnyOneItem=true;
        }
        let notes = component.find('body') ? component.find('body').get('v.value') : '';
        let selectedTags = component.get('v.selectedTags');//helper.collectIds(component.get('v.selectedTags'));
        if(selectedTags && selectedTags.length>0){
        	isSelectedAnyOneItem=true;
        }
       // let clearStartDate = component.find('clearStartDateRadio').getElement().checked;
      //  let clearEndDate = component.find('clearEndDateRadio').getElement().checked;
        let clearTags = component.find('clearTagsCheckbox').getElement().checked;
        if(clearTags){
        	isSelectedAnyOneItem=true;
        }
        let clearStartDate = false;
        let clearEndDate = false;
        //LRCC-1327
        let noteTitle = component.find('title') ? component.find('title').get('v.value') : '';
        //let retired = component.find('retiredCheckbox').getElement().checked;
        let retired = component.find('retiredSelection').get('v.value') ;
         
        if(retired){
        	isSelectedAnyOneItem=true;
        }
        let isRetired = false;
        if("Set to checked"==retired){
        	isRetired = true;
        }
        console.log('::::: noteTitle :::::', noteTitle);
         console.log('::::: noteTitle :::::', noteTitle && noteTitle == '');
        //LRCC-1327
        //LRCC-1732
        //if(noteTitle == ''){
            
            //let label = $A.get("$Label.c.EAW_NotesSubjectValidation");
           // helper.errorToast(label);
            //return;
       // }
        console.log('::::: component.find(startDateInput).get(v.value) :::::',component.find('startDateInput').get('v.value'));
        console.log(':::::component.find(endDateInput).get(v.value)  :::::',component.find('endDateInput').get('v.value') );
        console.log('::::: selectedTags :::::',selectedTags);
        console.log('::::: clearTags :::::', clearTags);
        
        //LRCC-1154
      /*  let tbdStartDate = component.find('tbdStartDateRadio').getElement().checked;
        let tbdEndDate = component.find('tbdEndDateRadio').getElement().checked;
        let perpetuityEndDate = component.find('perpetuityEndDateRadio').getElement().checked;
        let rightsEndDate = component.find('rightsEndDateRadio').getElement().checked;*/
        
        let startDateText = component.get('v.startDateText');
      /*  if(startDateText == 'clear'){
            clearStartDate = true;
            startDateText = '';
        }*/
        
        let endDateText = component.get('v.endDateText');
       /* if(endDateText == 'clear'){
            clearEndDate = true;
            endDateText = '';
        }*/
        
        console.log('::::: startDateText :::::',startDateText);
        console.log('::::: endDateText :::::', endDateText);
        
        if( windowList.length === 0 ) {
            helper.errorToast('No Windows Selected');
            return;
        }
        
        //<!-- LRCC-1380 APPEND,REPLACE & CLEAR radio button for notes  -->
        let applyNoteValue = component.get('v.applyNoteValue') ;
        let massUpdateNotes = {title:noteTitle,Body:notes,applyNoteValue:applyNoteValue};
        console.log('::::: isSelectedAnyOneItem :::::', isSelectedAnyOneItem);
        console.log('::::: isStartDateModified :::::', component.get('v.isStartDateModified'));
        console.log('::::: isEndDateModified :::::', component.get('v.isEndDateModified') );
        console.log('::::: noteTitle :::::', noteTitle);
        console.log('::::: notes :::::', notes);

        //LRCC-1732- removed condition || noteTitle ||notes

        if(!(isSelectedAnyOneItem || component.get('v.isStartDateModified') || component.get('v.isEndDateModified')|| noteTitle ||notes||'clear'==applyNoteValue)){
        	helper.errorToast('Please select at least one field before save');
                              
        	return;
        }
        //LRCC-1636
        var notesAttachmentEvent = component.getEvent("notesAttachmentEvent");
        notesAttachmentEvent.setParams({
            "title" : noteTitle,
            "body" : notes,
            "action" : applyNoteValue
        });
        notesAttachmentEvent.fire();
       
        //LRCC-1010
        let displayData = component.get('v.displayData') == null ? [] : component.get('v.displayData');
        let results = windowList;
        let clearWindowTagsWindowIds = [];
        console.log('::::: results :::::', results);
        console.log('::::: displayData :::::', displayData);
        for(let i = 0; i < results.length; i++) {
        
	        //for(let j = 0; j < displayData.length; j++) {
	        	
	            let windowIndex = displayData.map(function (window) { return window.Id; }).indexOf(results[i]);
	            console.log('::::: windowIndex :::::', windowIndex);
                 console.log('::::: startDate :::::', startDate);
                console.log('::::: endDate :::::', endDate);
                console.log('::::: con :::::', (startDate == '')?null:startDate);
                console.log('::::: con :::endDate::', (endDate == '')?null:endDate);
	            if(windowIndex !== -1) {
	            	
	            	if(component.get('v.isStartDateModified')){
	            		if(startDate){
                            displayData[windowIndex].Start_Date__c = startDate;
	            		}
	            		else{
	            			displayData[windowIndex].Start_Date_Rule__c = startDateText;
		                	displayData[windowIndex].Start_Date_Text__c= startDateText;
		                	displayData[windowIndex].Start_Date__c = startDate;
		                }
	            	}
        			if(component.get('v.isEndDateModified')){
	            		if(endDate){
	            			displayData[windowIndex].End_Date__c = endDate;
	            		}
	            		else {
		                	displayData[windowIndex].End_Date_Rule__c = endDateText;
		                	displayData[windowIndex].End_Date_Text__c= endDateText;
		                	displayData[windowIndex].End_Date__c = endDate;
		                }
	            	}
	            	if(windowStatus)
	            		displayData[windowIndex].Status__c = windowStatus;
	            	if(retired)
	            		displayData[windowIndex].Retired__c=isRetired;
	                console.log('isplayData[j].Windows[windowIndex].windowTags:::::',displayData[windowIndex]); 
	                //LRCC-1636
	                if(displayData && displayData[windowIndex] && selectedTags && selectedTags.length>0){
	                
	                	displayData[windowIndex].windowTags =[];
	                    for(var k = 0;k < selectedTags.length ; k++){
	                    
	                    	console.log('selectedTags:::::',selectedTags[k]);
	                    	if(Array.isArray(displayData[windowIndex].windowTags)){
	                    	
	                    		displayData[windowIndex].windowTags.push(selectedTags[k]);
	                    	}
	                    }  
                    }
                    if(clearWindowTagsWindowIds.indexOf(displayData[windowIndex].Id) == -1 && clearTags == true){
                        
                    	clearWindowTagsWindowIds.push(displayData[windowIndex].Id);
                    }
	            }
	        //}
        }
        //component.set('v.windowList',null);
        //component.set('v.windowList',[]);
        //<!-- LRCC-1380 APPEND,REPLACE & CLEAR radio button for notes  -->
        component.set('v.massUpdateNotes',massUpdateNotes);
        var clearWindowTagMap = {};
        if(!clearWindowTagMap['clearWindowTagWindowIds']){
        
        	clearWindowTagMap['clearWindowTagWindowIds'] = [];
        }
        clearWindowTagMap['clearWindowTagWindowIds'] = clearWindowTagsWindowIds;
        component.set('v.clearWindowTagMap',clearWindowTagMap);
        component.set('v.displayData', []);
        component.set('v.displayData', displayData);
        console.log('::::: displayData :::after::',displayData);
      
        
        // Send the APEX call
        try {
            /*let action = component.get("c.massUpdateWindows");
            action.setParams({
                'windowIds' : windowList,
                'tagIds' : selectedTags,
                'windowStatus' : windowStatus,
                'startDate' : startDate,
                'endDate' : endDate,
                'noteText' : notes,
                'clearStartDate' : clearStartDate,
                'clearEndDate' : clearEndDate,
                'clearTags' : clearTags,
                'startDateText' : startDateText,
                'endDateText' : endDateText
            });
            action.setCallback(this, function(response) {
                let state = response.getState();
                let results = response.getReturnValue();

                if (state === 'SUCCESS' && results != null) {
                    let displayData = component.get('v.displayData') == null ? [] : component.get('v.displayData');

                    // Find the Windows and Update their fields
                    for(let i = 0; i < results.length; i++) {
                        for(let j = 0; j < displayData.length; j++) {
                            let windowIndex = displayData[j].Windows.map(function (window) { return window.Id; }).indexOf(results[i].Id);
                            if(windowIndex !== -1) {
                                displayData[j].Windows[windowIndex].Start_Date__c = results[i].Start_Date__c;
                                displayData[j].Windows[windowIndex].End_Date__c = results[i].End_Date__c;
                                displayData[j].Windows[windowIndex].Status__c = results[i].Status__c;
                                displayData[j].Windows[windowIndex].Status__c = results[i].Status__c;
                                displayData[j].Windows[windowIndex].CurrencyIsoCode = results[i].CurrencyIsoCode; // To display the window tags
                            }
                        }
                    }

                    // Set the data so the UI updates
                    component.set('v.displayData', displayData);
                    helper.successToast(results.length + ' Window(s) Updated');

                } else if (state === 'ERROR') {
                    console.log(response.getError());
                    helper.errorToast('Error Mass Updating Windows');
                }
            });
            $A.enqueueAction(action);*/
        } catch (e) {
            console.log(e);
            helper.errorToast('Error calling Mass Update function');
        }

        helper.clearInputsAndHide(component);
        //LRCC-1327-- check box issue fix
		var clearCheckedValuesEvent = component.getEvent("ClearCheckedValues");
        clearCheckedValuesEvent.fire();
    },

    /** Methods below hide/show the popup*/
    showPopup : function(component, event, helper) {
        document.getElementById("EAW_TitlePlanWindowMassUpdatePopup").style.display = "block";
    },
    hidePopup : function(component, event, helper) {
        helper.clearInputsAndHide(component);
    },
    
    //LRCC-1327
    /*disableStartDate: function(component, event, helper) {
        
        console.log('v.startDateText:vss::',event.getSource().get("v.value"));
        let selectedValue = event.getSource().get("v.value");
        console.log('v.selected:vss::',selectedValue.split(',').indexOf('clear'));
        component.set('v.startDate',null);
    	component.set('v.disableStartDateFlag',selectedValue.split(',').indexOf('clear'));
       
    },*/
    clearStartDateRadioButtons : function(component, event, helper) {
    
       	console.log('startDate:vss::',component.get('v.startDate'));
       	 if(component.get('v.startDate') != null){
       	 
       		 component.set('v.startDateText',null);
       	 }
    },
    /*
    disableEndDate: function(component, event, helper) {
        
        console.log('v.end:vss::',event.getSource().get("v.value"));
        let selectedValue = event.getSource().get("v.value");
        console.log('v.end:vss::',selectedValue.split(',').indexOf('clear'));
        component.set('v.endDate',null);
    	component.set('v.disableEndDateFlag',selectedValue.split(',').indexOf('clear'));
    },*/
    clearEndDateRadioButtons : function(component, event, helper) {
    
       	 console.log('enddate:vss::',component.get('v.endDate'));
       	 if(component.get('v.endDate') != null){
       	 
       		 component.set('v.endDateText',null);
       	 }
    },
    clearWindowTags : function(component, event, helper) {
    
        console.log('clear:::',component.find('clearTagsCheckbox').getElement().checked);
        //component.set('v.disableWindowFlag',component.find('clearTagsCheckbox').getElement().checked);
        
        if(component.find('clearTagsCheckbox').getElement().checked){
        	component.set('v.disableWindowFlag',true);
        	component.set('v.selectedTags',[]);
        	
        }else{
        	component.set('v.disableWindowFlag',false);
        }
         console.log('disableWindowFlag:::',component.get('v.disableWindowFlag'));
    },
     //LRCC-1327
     //LRCC-1380
    onStartCheckBoxGroupClick: function(component, evt) {
         var checkCmp1 =component.find("startDateTBD").getElement().checked;
         var checkCmp2 =component.find("clearStartDate").getElement().checked;
          component.set('v.isStartDateModified',false);
          component.set('v.disableStartDateFlag',-1);
         if(checkCmp1 && evt.target.id=='startDateTBD'){
           component.find("clearStartDate").getElement().checked=false;
           component.set('v.startDateText','TBD');
           component.set('v.isStartDateModified',true);
           component.set('v.disableStartDateFlag',1);
         }else if(checkCmp2 && evt.target.id=='clearStartDate'){
           component.find("startDateTBD").getElement().checked=false;
           component.set('v.startDateText','');
           component.set('v.isStartDateModified',true);
           component.set('v.disableStartDateFlag',1);
         }
         /*if(component.find("startDateTBD").getElement().checked){
           component.set('v.disableStartDateFlag',1);
         }else{
           component.set('v.disableStartDateFlag',-1);
         }*/
         component.set('v.startDate',null);
	},
	//LRCC-1327
	//LRCC-1380
	onEndCheckBoxGroupClick: function(component, evt) {
         var checkcomponent1 =component.find("endDateTBD").getElement().checked;
         var checkcomponent2 =component.find("endDatePerpetuity").getElement().checked;
         var checkcomponent3 =component.find("clearEndDate").getElement().checked;
          component.set('v.isEndDateModified',false);
          component.set('v.disableEndDateFlag',-1);
         if(checkcomponent1 && evt.target.id=='endDateTBD'){
           component.find("endDatePerpetuity").getElement().checked=false;
           component.find("clearEndDate").getElement().checked=false;
           component.set('v.isEndDateModified',true);
           component.set('v.endDateText','TBD');
           component.set('v.disableEndDateFlag',1);
         }else if(checkcomponent2 && evt.target.id=='endDatePerpetuity'){
           component.find("endDateTBD").getElement().checked=false;
           component.find("clearEndDate").getElement().checked=false;
           component.set('v.isEndDateModified',true);
           component.set('v.endDateText','Perpetuity');
           component.set('v.disableEndDateFlag',1);
         }else if(checkcomponent3 && evt.target.id=='clearEndDate'){
           component.find("endDateTBD").getElement().checked=false;
           component.find("endDatePerpetuity").getElement().checked=false
           component.set('v.isEndDateModified',true);
           component.set('v.endDateText','');
           component.set('v.disableEndDateFlag',1);
         }
         /*if(component.find("endDateTBD").getElement().checked || component.find("endDatePerpetuity").getElement().checked){
           component.set('v.disableEndDateFlag',1);
         }else{
           component.set('v.disableEndDateFlag',-1);
         }*/
         component.set('v.endDate',null);
	},
    

})