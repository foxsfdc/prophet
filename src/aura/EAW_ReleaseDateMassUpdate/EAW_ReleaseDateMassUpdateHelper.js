({
    handleErrors: function(errors, component, event, helper) {
    	
    	console.log(':::::: errors :::::::'+errors);
	    let errorMessage = '';
        
        if (errors && Array.isArray(errors) && errors.length > 0) {
            
	        errors.forEach(error => {
                
	            if (error.message && errorMessage.includes(error.message) == false) {
	                errorMessage += error.message + "\n";
	            }
	            if (error.pageErrors && error.pageErrors.length > 0) {
	                error.pageErrors.forEach(pageError => {
	                    if (errorMessage.includes(error.message) == false)
	                        errorMessage += pageError.message + "\n";
	                });
	            } else if (error.fieldErrors) {
	                let fields = Object.keys(error.fieldErrors);
	                fields.forEach(fieldError => {
	                    if (Array.isArray(fieldError)) {
	                        fieldError.forEach(err => {
	                            if (errorMessage.includes(error.message) == false)
	                                errorMessage += err.message;
	                        });
	                    }
	                });
	            }
	        });
	    } else {
	        errorMessage = errors;
	    }
	    if (errorMessage.indexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION') !== -1) {
	        
	        let errorMessageList = errorMessage.split('FIELD_CUSTOM_VALIDATION_EXCEPTION,');
	        if (errorMessageList.length > 1) {
	            let finalErroList = errorMessageList[1].split(': []');
	            if (finalErroList.length) {
	                errorMessage = finalErroList[0];
	            }
	        }
	    }
	    if (errorMessage.indexOf('REQUIRED_FIELD_MISSING') !== -1) {
	        
	        let errorMessageList = errorMessage.split('REQUIRED_FIELD_MISSING,');
	        if (errorMessageList.length > 1) {
	            errorMessage = errorMessageList[1];
	        }
	    }
	    console.log('***** errorMessage-->', errorMessage);
	    return errorMessage;
	},
        
    hideMassUpdate : function(component) {
    
        component.set('v.displayComponent', false);
        component.set('v.selectedReleaseDateTags', []);
        component.set('v.title', '');
        component.set('v.body', '');
        component.set('v.disalbleReleaseDateTags', false);
        component.set('v.disalbleManualDateAndTemp', false);
        component.set('v.releaseDate', null);
        component.set('v.tempOrPerm', null);
       
        console.log('recordMap:v::',JSON.stringify(component.get('v.recordMap')));
        // LRCC-1010 && 1327
        var recordMap = component.get('v.recordMap');
    	var releaseDatekeys =  Object.keys(recordMap);
    	var clearReleaseDateMap = component.get('v.clearReleaseDateMap');
    	
    	for(var i = 0 ; i < releaseDatekeys.length ; i++){
    	
    		if(clearReleaseDateMap && clearReleaseDateMap['clearReleaseDateTagReleaseDateIds'] && clearReleaseDateMap['clearReleaseDateTagReleaseDateIds'].indexOf(releaseDatekeys[i]) > -1){
    			recordMap[releaseDatekeys[i]] = [] ;
    		}
    	}
    	component.set('v.recordMap',recordMap);
    	console.log('recordMap::::vvvv::',JSON.stringify(component.get('v.recordMap')));
    },
	
    errorToast : function(message) {
        var toastEvent = $A.get('e.force:showToast');

        toastEvent.setParams({
            'title': 'Error!',
            'type': 'error',
            'message': message
        });

        toastEvent.fire();
    },

    successToast : function(message) {
        var toastEvent = $A.get('e.force:showToast');

        toastEvent.setParams({
            'title': 'Success!',
            'type': 'success',
            'message': message
        });

        toastEvent.fire();
    },
    checkForManualDateUpdate : function(cmp,event,helper){
        var data = cmp.get('v.data');
        var isFirm = false;
        for(var i of data){
            if(i.Status__c == 'Firm'){
                isFirm = true;
                break;
            }
        }
        if(isFirm){
            var message = 'The Release Date Status is Firm. Updating the Manual date will update the Release Date.Do you want to continue ?';
            component.find('modifierPrompt').openModal(event.getParam("oldValue"),event.getParam("value"),message,'Manual_Date__c');
        } else{
            // mass update
        }
    }
})