({
	windowStrandColumns : function() {
		return [
			{
				label: "License Type",
				fieldName: "licenseType",
				type: "select"
			},
			{
				label: "Start Date",
				fieldName: "startDateFrom",
				type: ""
			},
			{
				label: "End Date",
				fieldName: "endDateFrom",
				type: ""
			},
			{
				label: "Media",
				fieldName: "medias",
				type: ""
			},
			{
				label: "Territory",
				fieldName: "territories",
				type: ""
			},
			{
				label: "Language",
				fieldName: "languages",
				type: ""
			}
		];
	}
})