({
	onChildAttrChange : function(component, event, helper) {
		console.log("onChildAttrChange")
        
	},
	init : function(component, event, helper) {
		var collectWindowStrandPicklists = component.get("c.collectWindowStrandPicklists");

        collectWindowStrandPicklists.setCallback(this, function(response) {
			var picklistsValues = response.getReturnValue();
            var licenseTypes = picklistsValues[0];
			var medias = picklistsValues[1];
			var territories = picklistsValues[2];
			var languages = picklistsValues[3];
            
            component.set("v.licenseTypes", licenseTypes);
            component.set("v.medias", medias);
            component.set("v.territories", territories);
            component.set("v.languages", languages);
        });
		$A.enqueueAction(collectWindowStrandPicklists);
	}
})