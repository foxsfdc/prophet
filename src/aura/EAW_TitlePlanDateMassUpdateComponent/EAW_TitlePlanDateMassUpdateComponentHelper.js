({
    errorToast : function(message) {
        var toastEvent = $A.get('e.force:showToast');

        toastEvent.setParams({
            'title': 'Error!',
            'type': 'error',
            'message': message
        });

        toastEvent.fire();
    },

    successToast : function(message) {
        var toastEvent = $A.get('e.force:showToast');

        toastEvent.setParams({
            'title': 'Success!',
            'type': 'success',
            'message': message
        });

        toastEvent.fire();
    },
    collectIds : function(selectedResults) {
        var collectedIds = [];

        if(selectedResults && selectedResults.length > 0) {
            for(var result of selectedResults) {
                collectedIds.push(result.id);
            }
        }

        return collectedIds;
    }
})