({
	getErrorLog : function(component, event, helper) {
	
	    var action = component.get("c.getErrorLogs");
	    action.setParams({ windowId : component.get("v.recordId")});
	    action.setCallback(this, function(response) {
	        var state = response.getState();
	
	        if (state === "SUCCESS") {
	            
	            component.set('v.userinfoMsg');
	
				var results = response.getReturnValue();
	            if(results && results.length){
	
	                console.log('response::helper::',results);
					var userInfo = component.get('v.userinfoMsg');
					userInfo = results[0];
					
					if(userInfo && userInfo.Error_Message__c == 'Read timed out'){
					
						component.set('v.userinfoMsg',userInfo);
					}else{
					
					
						 window.setTimeout($A.getCallback(function() {
	
		                    $A.get("e.force:closeQuickAction").fire();
		                    location.reload();
		                }),1000);
					}
	            }else {
	
	                window.setTimeout($A.getCallback(function() {
	
	                    $A.get("e.force:closeQuickAction").fire();
	                    location.reload();
	                }),1000);
	            }
	            
	        }else if (state === "ERROR") {
	            
	            console.log('error:::');
	        }
	    });
	    $A.enqueueAction(action);
		
	}
})