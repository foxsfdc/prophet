({
    setData : function(cmp,usedGuidelines) {
        var columnsRDG = 
            [
                {label: 'Territory', fieldName: 'Territory__c', type: 'Picklist'},
                {label: 'Product Type', fieldName: 'Product_Type__c', type: 'Picklist'},
                {label: 'Date Type', fieldName: 'EAW_Release_Date_Type__c', type: 'Picklist'},
                {label: 'Language', fieldName: 'Language__c', type: 'Picklist'}
            ];
        var columnsWGL = 
            [
                {label: 'Window Name', fieldName: 'Name', type: 'text'},
                {label: 'Product Type', fieldName: 'Product_Type__c', type: 'Picklist'},
                {label: 'Window Date', fieldName: 'usedDate', type: 'text'}
            ];
        var headerColumnsWGL = 
            [
                {label: 'Window Name', fieldName: 'Name', type: 'text'},
                {label: 'Product Type', fieldName: 'Product_Type__c', type: 'Picklist'},
                {label: 'Language', fieldName: 'EAW_Language__c', type: 'Picklist'},
                {label: 'Window Type', fieldName: 'Window_Type__c', type: 'picklist'}
            ];
        
        cmp.set("v.columnsRDG",columnsRDG);
        cmp.set("v.columnsWGL",columnsWGL);
        cmp.set("v.headerColumnsWGL",headerColumnsWGL);
        if(usedGuidelines.usedByWGL && usedGuidelines.usedByWGL.length){
            var usedWg = usedGuidelines.usedByWGL;
            var originalUsedWg = [];
            var usedBylength = usedWg.length;
            for(var i=0;i<usedWg.length;i++){
                if(cmp.get('v.windowGuidelineDateMap')[usedWg[i].Id] != null){            
                    usedWg[i].usedDate = cmp.get('v.windowGuidelineDateMap')[usedWg[i].Id];
                }
            }
            var temp = 0;
            for(var i=0;i<usedBylength;i++){
                var tempWG = JSON.parse(JSON.stringify(usedWg[i]));
                if(cmp.get('v.windowGuidelineDateMap')[usedWg[i].Id] != null && cmp.get('v.windowGuidelineDateMap')[usedWg[i].Id] != ''){            
                    var dateList = (usedWg[i].usedDate).split(';');
                    console.log(usedWg[i].Name +'::::::::::::::::'+dateList);
                    if(dateList.length){
                        for(var j=0;j<dateList.length;j++){
                            if(dateList[j]){
                                if(j == 0){
                                    tempWG.usedDate = dateList[j];
                                    originalUsedWg.push(JSON.parse(JSON.stringify(tempWG)));
                                } else{
                                    tempWG.usedDate = dateList[j];
                                    originalUsedWg.push(JSON.parse(JSON.stringify(tempWG)));
                                }
                            }
                        }
                    }
                }
            }
            
            console.log(':::::::originalUsedWg:::::::::::'+JSON.stringify(originalUsedWg));
            usedGuidelines.usedByWGL = originalUsedWg;
        }
        cmp.set('v.usedGuidelines',usedGuidelines);
	}
})