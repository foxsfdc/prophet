({
	convertArrayToCSV : function(component, records) {
        
        // declare variables
        var csvStringResult, counter, header, keys, columnDivider, lineDivider;
       
        // check if "objectRecords" parameter is null, then return from function
        if (records == null || !records.length) {
            return null;
         }
        
        // store ,[comma] in columnDivider variabel for sparate CSV values and 
        // for start next line use '\n' [new line] in lineDivider varaible  
        columnDivider = ',';
        lineDivider =  '\n';
 
        // in the keys variable store fields API Names as a key
        // this labels use in CSV file header
        header = ['Tag Name', 'Tag Type', 'Active Flag']; // this array is the 'pretty' column names for the csv
        keys = ['Name','Tag_Type__c','isActive']; // this array is the actual column name
        
        csvStringResult = '';
        csvStringResult += header.join(columnDivider);
        csvStringResult += lineDivider;
 
        for(var i=0; i < records.length; i++){   
            counter = 0;
           
             for(var sTempkey in keys) {
                var skey = keys[sTempkey] ;  
 
              // add , [comma] after every String value,. [except first]
                  if(counter > 0){ 
                      csvStringResult += columnDivider; 
                   }   
               
               csvStringResult += '"'+ records[i][skey]+'"'; 
               
               counter++;
 
            } // inner for loop close 
             csvStringResult += lineDivider;
          }// outer main for loop close 
       
       // return the CSV formate String 
        return csvStringResult;        
    }
})