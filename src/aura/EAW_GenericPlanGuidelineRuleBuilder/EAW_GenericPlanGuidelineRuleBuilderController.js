({
	init : function(cmp, event, helper) {
        cmp.set('v.spinner',true);
        helper.setConditionFields(cmp,event,helper);
        var action = cmp.get('c.getDependentFieldSet');
        action.setParams({
            'fieldName' : 'Condition_Criteria__c',
            'planGuidelineId' : cmp.get('v.recordId')
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state == 'SUCCESS'){
                console.log(response.getReturnValue());
                var controllingPickListValues = response.getReturnValue().dependentWrapper.controllingPicklistValues;
                cmp.set('v.isActivePlanGuideline',response.getReturnValue().isActivePlanGuideline);
                var validControllingPicklistvalues = [];
                for(var i=0;i<controllingPickListValues.length;i++){
                    if(controllingPickListValues[i].label != 'Title' && controllingPickListValues[i].label != 'Window Schedule Tag' && controllingPickListValues[i].label != 'Release Date Tag' && controllingPickListValues[i].label != 'Released By' && controllingPickListValues[i].label != 'Distributed By'
                       && controllingPickListValues[i].label != 'Line of Business' && controllingPickListValues[i].label != 'Release Date Status' && controllingPickListValues[i].label != 'Window Schedule Status' && controllingPickListValues[i].label != 'Window Tag'){//LRCC-1384-Remove window tag.
                        validControllingPicklistvalues.push(controllingPickListValues[i]);
                    }
                }
                cmp.set('v.conditionBasedField',validControllingPicklistvalues);
                if(response.getReturnValue().dependentWrapper.dependentPicklistValues['Release Date Status'].length){
                    cmp.set('v.releaseDateStatusList',response.getReturnValue().dependentWrapper.dependentPicklistValues['Release Date Status']);
                }
                cmp.set('v.fieldSet',response.getReturnValue().fieldWrapper);
                if(response.getReturnValue().ruleAndRuleDetailList.length){
                    cmp.set('v.ruleWrapper',response.getReturnValue().ruleAndRuleDetailList);
                    cmp.set('v.isNew',false);
                    cmp.set('v.edit',false);
                } else{
                    var ruleWrapper = [{
                        'rule' : JSON.parse(JSON.stringify(cmp.get('v.rule'))),
                        'ruleDetailList' : JSON.parse(JSON.stringify(cmp.get('v.ruleDetailList')))
                    }];
                    cmp.set('v.ruleWrapper',ruleWrapper);
                }
            } else{
                helper.showToast(cmp,event,helper,'error',helper.buildErrorMsg(cmp,event,helper,response.getError()));
            }
            cmp.set('v.spinner',false);
        });
        $A.enqueueAction(action);
    },
    addRule : function(cmp,event,helper){
        var ruleWrapperList = cmp.get('v.ruleWrapper');
        var ruleWrapper = {
            'rule' : JSON.parse(JSON.stringify(cmp.get('v.rule'))),
            'ruleDetailList' : JSON.parse(JSON.stringify(cmp.get('v.ruleDetailList')))
        };
        ruleWrapperList.push(ruleWrapper);
        cmp.set('v.ruleWrapper',ruleWrapperList);
    },
    removeRule : function(cmp,event,helper){
        var indexValue = event.getSource().get('v.name');
        var ruleWrapperList = cmp.get('v.ruleWrapper');
        if(ruleWrapperList[indexValue].rule && ruleWrapperList[indexValue].rule.Id){
            var rulesToDelete = cmp.get('v.rulesToDelete') || [];
            rulesToDelete.push(ruleWrapperList[indexValue].rule.Id);
            cmp.set('v.rulesToDelete',rulesToDelete);
        }
        if(ruleWrapperList.length != 1){
            ruleWrapperList.splice(indexValue,1);
        } else{
            var ruleWrapperList = [{
                'rule' : JSON.parse(JSON.stringify(cmp.get('v.rule'))),
                'ruleDetailList' : JSON.parse(JSON.stringify(cmp.get('v.ruleDetailList')))
            }];
        }
        cmp.set('v.ruleWrapper',ruleWrapperList);
    },
    saveCondition : function(cmp,event,helper){
        if(helper.validConditionSets(cmp,event,helper)){
            console.log('valid:::',cmp.get('v.ruleWrapper'));
            helper.saveRuleConditions(cmp,event,helper,cmp.get('v.ruleWrapper'));
        } else{
             helper.showToast(cmp,event,helper,'error','Please provide valid values');
        }
    }
})