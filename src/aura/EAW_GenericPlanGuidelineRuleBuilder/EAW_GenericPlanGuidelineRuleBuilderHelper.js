({
	setConditionFields : function(cmp,event,helper) {
        
        var conditionMap = {
            'US Box Office' : {
                'noOfRuleDetails' : 1,
                'fieldList' : ['Condition_Operator__c','Condition_Criteria_Amount__c'],
                'annotations' : [
                    {'fieldName' : 'Condition_Operator__c','annotation' : 'IS'},
                    {'fieldName' : 'Condition_Criteria_Amount__c', 'annotation'  : 'than'}
                ],
                'ruleDetail' : []
            },
            'Local Box Office' : {
                'noOfRuleDetails' : 1,
                'fieldList' : ['Territory__c','Condition_Operator__c','Condition_Criteria_Amount__c'],
                'annotations' : [
                    {'fieldName' : 'Territory__c','annotation' : 'IN'},
                    {'fieldName' : 'Condition_Operator__c','annotation' : 'IS'},
                    {'fieldName' : 'Condition_Criteria_Amount__c', 'annotation'  : 'than'}
                ],
                'ruleDetail' : []
            },
            'Local Admissions' : {
                'noOfRuleDetails' : 1,
                'fieldList' : ['Territory__c','Condition_Operator__c','Condition_Criteria_Amount__c'],
                'annotations' : [
                    {'fieldName' : 'Territory__c','annotation' : 'IN'},
                    {'fieldName' : 'Condition_Operator__c','annotation' : 'IS'},
                    {'fieldName' : 'Condition_Criteria_Amount__c', 'annotation'  : 'than'}
                ],
                'ruleDetail' : []
            },
            'US Admissions' : {
                'noOfRuleDetails' : 1,
                'fieldList' : ['Condition_Operator__c','Condition_Criteria_Amount__c'],
                'annotations' : [
                    {'fieldName' : 'Condition_Operator__c','annotation' : 'IS'},
                    {'fieldName' : 'Condition_Criteria_Amount__c', 'annotation'  : 'than'}
                ],
                'ruleDetail' : []
            },
            'US Number of Screens' : {
                'noOfRuleDetails' : 1,
                'fieldList' : ['Condition_Operator__c','Condition_Criteria_Amount__c'],
                'annotations' : [
                    {'fieldName' : 'Condition_Operator__c','annotation' : 'IS'},
                    {'fieldName' : 'Condition_Criteria_Amount__c', 'annotation'  : 'than'}
                ],
                'ruleDetail' : []
            },
            'Local Number of Screens' : {
                'noOfRuleDetails' : 1,
                'fieldList' : ['Territory__c','Condition_Operator__c','Condition_Criteria_Amount__c'],
                'annotations' : [
                    {'fieldName' : 'Territory__c','annotation' : 'IN'},
                    {'fieldName' : 'Condition_Operator__c','annotation' : 'IS'},
                    {'fieldName' : 'Condition_Criteria_Amount__c', 'annotation'  : 'than'}
                ],
                'ruleDetail' : []
            },
            'Release Date Status' : {
                'noOfRuleDetails' : 1,
                'fieldList' : ['Condition_Operator__c','Condition_Status__c'], //need to add
                'annotations' : [
                    {'fieldName' : 'Condition_Operator__c','annotation' : 'IS'},
                    {'fieldName' : 'Condition_Status__c', 'annotation'  : ''}
                ],
                'ruleDetail' : []
            },
            'Window Schedule Status' : {
                'noOfRuleDetails' : 1,
                'fieldList' : ['Condition_Operator__c','Condition_Status__c'], //need to add
                'annotations' : [
                    {'fieldName' : 'Condition_Operator__c','annotation' : 'IS'},
                    {'fieldName' : 'Condition_Status__c', 'annotation'  : ''}
                ],
                'ruleDetail' : []
            },
            'Released By' : {
                'noOfRuleDetails' : 1,
                'fieldList' : ['Condition_Company__c'],
                'annotations' : [
                    {'fieldName' : 'Condition_Company__c','annotation' : 'By'}
                ],
                'ruleDetail' : []
            },
            'Distributed By' : {
                'noOfRuleDetails' : 1,
                'fieldList' : ['Condition_Company__c'],
                'annotations' : [
                    {'fieldName' : 'Condition_Company__c','annotation' : 'By'}
                ],
                'ruleDetail' : []
            },
            'Local Theatrical Release' : {
                'noOfRuleDetails' : 1,
                'fieldList' : ['Territory__c','Condition_Operator__c','Condition_Date__c'],
                'annotations' : [
                    {'fieldName' : 'Territory__c','annotation' : 'IN'},
                    {'fieldName' : 'Condition_Operator__c','annotation' : 'IS'},
                    {'fieldName' : 'Condition_Date__c', 'annotation'  : 'than'}
                ],
                'ruleDetail' : []
            },
            'Release Year' : {
                'noOfRuleDetails' : 1,
                'fieldList' : ['Condition_Operator__c','Condition_Year__c'],
                'annotations' : [
                    {'fieldName' : 'Condition_Operator__c','annotation' : 'IS'},
                    {'fieldName' : 'Condition_Year__c', 'annotation'  : 'than'}
                ],
                'ruleDetail' : []
            },
            'Title' : {
                'noOfRuleDetails' : 1,
                'fieldList' : ['Condition_Operator__c','Territory__c','Alt_Condition_Media__c'],
                'annotations' : [
                    {'fieldName' : 'Condition_Operator__c','annotation' : 'Is'},
                    {'fieldName' : 'Territory__c','annotation' : 'For'}
                ],
                'ruleDetail' : []
            },
            'Line of Business' : {
                'noOfRuleDetails' : 1,
                'fieldList' : ['Condition_Status__c'],
                'annotations' : [
                    {'fieldName' : 'Condition_Status__c','annotation' : 'In'}
                ],
                'ruleDetail' : []
            },
            'Division' : {
                'noOfRuleDetails' : 1,
                'fieldList' : ['Condition_Operand_Details__c'],
                'annotations' : [
                    {'fieldName' : 'Condition_Operand_Details__c','annotation' : 'In'}
                ],
                'ruleDetail' : []
            },
            'Title Tag' : {
                'noOfRuleDetails' : 1,
                'fieldList' : ['Condition_Tag__c'],
                'annotations' : [
                    {'fieldName' : 'Condition_Tag__c','annotation' : 'In'}
                ],
                'ruleDetail' : []
            },
            'Release Date Tag' : {
                'noOfRuleDetails' : 1,
                'fieldList' : ['Condition_Tag__c'],
                'annotations' : [
                    {'fieldName' : 'Condition_Tag__c','annotation' : 'In'}
                ],
                'ruleDetail' : []
            }
        };
        cmp.set('v.conditionNodeMap',conditionMap);
    },
    validConditionSets : function(cmp,event,helper){
        var planGuidelineCmp = cmp.find('planGuidelineCmp');
        var ruleDetailsToDelete = [];
        if(!Array.isArray(planGuidelineCmp)){
            planGuidelineCmp = [planGuidelineCmp];
        }
        var valid = true;
        for(var i=0;i<planGuidelineCmp.length;i++){
            if(planGuidelineCmp[i]){
                console.log('Delete::',planGuidelineCmp[i].get('v.ruleDetailsToDelete'));
                if(planGuidelineCmp[i].get('v.ruleDetailsToDelete') && planGuidelineCmp[i].get('v.ruleDetailsToDelete').length){
                    if(ruleDetailsToDelete.length){
                        ruleDetailsToDelete.concat(planGuidelineCmp[i].get('v.ruleDetailsToDelete'));
                    } else{
                        ruleDetailsToDelete = planGuidelineCmp[i].get('v.ruleDetailsToDelete');
                    }
                }
                if(!(planGuidelineCmp[i].validation())){
                    valid = false;
                }
            }
        }
        cmp.set('v.ruleDetailsToDelete',ruleDetailsToDelete);
        return valid;
    },
    saveRuleConditions : function(cmp,event,helper,ruleWrapper){
        cmp.set('v.spinner',true);
        var action = cmp.get('c.saveConditionSetFields');
        action.setParams({
            'ruleWrapper' : JSON.stringify(ruleWrapper),
            'planGuidelineId' : cmp.get('v.recordId'),
            'rulesToDelete' : cmp.get('v.rulesToDelete') || null,
            'ruleDetailsToDelete' : cmp.get('v.ruleDetailsToDelete') || null,
            'ruleJSON' : helper.setRuleJSON(cmp,event,helper,cmp.get('v.ruleWrapper')) || null
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            console.log(state);
            if(state == 'SUCCESS'){
                console.log('success',response.getReturnValue());
                if(!cmp.get('v.isNew')){
                    var message = 'Rule Details was successfully updated';
                } else{
                    var message = 'Rule Details was successfully get Inserted';
                }
                helper.showToast(cmp,event,helper,'success',message);
                window.setTimeout(function(){
                    location.reload();
                },500);
            } else{
                var errorMessage = helper.buildErrorMsg(cmp,event,helper,response.getError());
                helper.showToast(cmp,event,helper,'error',errorMessage);
            }
            cmp.set('v.spinner',false);
        });
        $A.enqueueAction(action);
    },
    buildErrorMsg : function(cmp, event, helper, errors){
        var errorMsg = "";
        
        if(errors[0] && errors[0].message){  // To show other type of exceptions
            errorMsg = errors[0].message;
        }else if(errors[0] && errors[0].pageErrors.length) {  // To show DML exceptions
            errorMsg = errors[0].pageErrors[0].message; 
        }else if(errors[0] && errors[0].fieldErrors){  // To show field exceptions
            var fields = Object.keys(errors[0].fieldErrors);
            var field = fields[0];
            errorMsg = errors[0].fieldErrors[field];
            errorMsg = errorMsg[0].message;
        }else if(errors[0] && errors[0].duplicateResults.length){ // To show duplicateResults exceptions
            errorMsg = errors[0].duplicateResults[0].message;
        }
        return errorMsg;
        
    },
    showToast : function(cmp, event, helper, type, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "type": type,
            "message": message
        });
        toastEvent.fire();
    },
    setRuleJSON : function(cmp,event,helper,ruleJSON){
        var JSONString = '';
        if(ruleJSON.length){
            for(var node of ruleJSON){
                if(node.ruleDetailList.length){
                    for(var i of node.ruleDetailList){
                        if(i.Condition_Field__c && cmp.get('v.conditionNodeMap')[i.Condition_Field__c] && (cmp.get('v.conditionNodeMap')[i.Condition_Field__c]).fieldList.length){
                            JSONString += i.Condition_Field__c + ' ';
                            var fields = JSON.parse(JSON.stringify((cmp.get('v.conditionNodeMap')[i.Condition_Field__c]).fieldList));
                            //fields = fields.split(',')[0].replace
                            var annotations = (cmp.get('v.conditionNodeMap')[i.Condition_Field__c]).annotations;
                            console.log('::'+JSON.stringify(annotations));
                            console.log(fields);
                            for(var j in fields){
                                console.log(i[fields[j]]);
                                if(i[fields[j]]){
                                    if(annotations[j].annotation)JSONString += annotations[j].annotation + ' '; 
                                    JSONString += i[fields[j]] + ' ';
                                }
                            }
                            JSONString += ';  ';
                        }
                    }
                }
            }
            console.log(':::JSONString::'+JSONString);
        }
        return JSONString ;
    }
})