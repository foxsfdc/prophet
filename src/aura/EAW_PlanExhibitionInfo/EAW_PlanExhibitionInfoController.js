({
	doInit : function(component, event, helper) {
        
        // Call Controller function to grab all Junction Objs and string together the exhibition data
		var action = component.get("c.getExhibitionData");
        action.setParams({"recordId" : component.get("v.recordId")});
        action.setCallback(this, function(response) {
            let results = response.getReturnValue();
            if(results != null) {
            	component.set("v.terrData", results.length > 0 ? response.getReturnValue()[0] : '');
            	component.set("v.langData", results.length > 1 ? response.getReturnValue()[1] : '');    
            } else {
                component.set("v.terrData", '');
            	component.set("v.langData", '');
            }
            
        });
        
        $A.enqueueAction(action);
	}
})