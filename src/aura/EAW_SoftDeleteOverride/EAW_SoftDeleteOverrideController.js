({
	init : function(component, event, helper) {
       
        component.set('v.showSpinner', true);
        var action = component.get('c.getError');
        console.log('L>>>>>ss>>>',component.get('v.sObjectListString'));
        console.log('sObjectName>>>>>>>>',component.get('v.sObjectName'));
        action.setParams({'sObjectListString':component.get('v.sObjectListString') ,
                          'sObjectName' :component.get('v.sObjectName')});
        action.setCallback(this, function (response) {
           console.log('response.getReturnValue::::::',response.getReturnValue());
            var state = response.getState();
            console.log('state::::::',state);
            if (state === 'SUCCESS') {
                
               var results = response.getReturnValue();
                console.log('results::::',results);
                
                if(results == 'Success'){
                    
                    component.set('v.goListViewFlag',results);
                    let handleErrors = helper.handleErrors($A.get("$Label.c.EAW_SuccessfullySoftDeleted"), component, event, helper);
                    if(handleErrors) {
                        
                        component.set('v.showModalPopup',true);
                        component.set('v.modalPopupObject',{'msg':handleErrors,'status':'success','header':'Success'});
                  
                    }
                    //component.find('toast').toastShow('Successfully soft deleted','success',false,200);
                }else {
                    //component.find('toast').toastShow(results,'error',false,200);
                    let handleErrors = helper.handleErrors(results, component, event, helper);
                    if(handleErrors) {
                        component.set('v.showModalPopup',true);
                        component.set('v.modalPopupObject',{'msg':handleErrors,'status':'error','header':'Error'});
                    }
                }
                
                component.set('v.showSpinner', false);
            }
        });
        $A.enqueueAction(action);
	},
    navigateToBack : function(component, event, helper) {
        
        console.log('component.get("v.toastStatus")::::',component.get('v.sobjectRecId'),component.get('v.goListViewFlag'));
        component.set('v.showSpinner', true);
        component.set('v.showModalPopup',false);
        //if(component.get("v.toastStatus") == 'close'){
            
            var base_url = window.location.origin;
            if(component.get('v.sobjectRecId') &&
              component.get('v.goListViewFlag') != 'Success') {
                
                location.replace(base_url+"/lightning/r/"+component.get('v.sObjectName')+"/"+component.get('v.sobjectRecId')+"/view");
            }else if(component.get('v.goListViewFlag') == 'Success'){
                
                location.replace(base_url+"/lightning/o/"+component.get('v.sObjectName')+"/list?filterName=Recent");
            } 
       // }
       
    }
})