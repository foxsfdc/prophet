({
    deleteSelected : function(component, event, helper) {
        let selectedRows = component.get('v.selectedRows');
        let selectedIds = [];

        for(let row of selectedRows) {
            selectedIds.push(row.Id);
        }

        let action = component.get('c.deleteReleaseDates');
        action.setParams({
            'selectedIds': selectedIds
        });

        action.setCallback(this, function(response) {
            let state = response.getState();
            let results = response.getReturnValue();

            if(state === 'SUCCESS' && results != null) {
                let myData = component.get('v.myData');

                for(let i = 0; i < myData.length; i++) {
                    if(results.find(result => result.Id === myData[i].Id)) {
                        myData.splice(i, 1);
                        i--;
                    }
                }

                component.set('v.myData', myData);

                helper.hideDeletePopup(component);
                helper.successToast('Successfully deleted the selected release dates(s).');
            } else {
                helper.errorToast('There was an error while trying to delete the release date(s).');
            }
        });
        $A.enqueueAction(action);

    },

    hideDeletePopup : function(component, event, helper) {
        helper.hideDeletePopup(component);
    }
})