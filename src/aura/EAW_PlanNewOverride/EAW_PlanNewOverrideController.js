({
	init : function(cmp, event, helper) {
        cmp.set('v.spinner', true);
        cmp.set('v.showModal', true);
        helper.setFieldValues(cmp, event, helper);
        cmp.set('v.spinner', false);
	},
    handleLoad : function(cmp){
        
    },
    handleSubmit : function(component ,event,helper) {
        component.set('v.spinner',true);
        event.preventDefault(); // stop form submission
        var eventFields = event.getParam("fields");        
        component.find('recordForm').submit(eventFields);
        
        component.set('v.spinner',false);
    },
    handleSuccess : function(cmp,event,helper) { 
        
			var plan = event.getParams().response.id;
        	console.log('::::::::',plan);
            var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId": plan
            });
            navEvt.fire();
            cmp.set('v.spinner',false);        
    },
    closeModal : function(cmp){
        cmp.set('v.showModal',false);
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        var base_url = window.location.origin;
        location.replace(base_url+"/lightning/o/EAW_Plan__c/list?filterName=Recent");
        dismissActionPanel.fire();
    }
})