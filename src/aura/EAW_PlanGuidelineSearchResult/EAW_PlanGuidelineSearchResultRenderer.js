({
    rerender : function (component, helper) { // LRCC - 1282
        this.superRerender();
        
        window.setTimeout($A.getCallback(function(){
            helper.setFixedHeader(component);
        }),500);
  
    },
    afterRender: function (cmp,helper) {
        
        this.superAfterRender();
        
        helper.windowClick = $A.getCallback(function(event){
            if(cmp.isValid()){
                helper.setFixedHeader(cmp);
            }
        });
        
        window.addEventListener('resize',helper.windowClick);
    }
})