({
    getFieldSet: function(component, event, helper) {
        helper.fieldSet(component,event,helper);
        //LRCC-1459
        console.log('initialTableShow  ::::',component.get('v.initialTableShow'));
        if(component.get('v.initialTableShow')){
             //LRCC-1795
            component.set("v.pagesPerRecod",component.find('pagePerRecord').get("v.value"));
            
            helper.generateAlphabets(component, event, helper);
        }
    },
	
    updateSelectedRows : function(component, event, helper) {
        let selectedRows  = component.get('v.selectedRows');
        let self = event.target;

        if(self.checked) {
            selectedRows.push(self.value);
        } else {
            selectedRows.splice(selectedRows.indexOf(self.value), 1);
        }
        
        component.set('v.selectedRows', selectedRows);
        
        //LRCC-1633
        let checkboxes = component.find('header-checkbox');
        let check = Array.isArray(checkboxes) ? checkboxes[0] : checkboxes;
        
        if(component.get('v.windowData').length == component.get('v.selectedRows').length) {            
            check.getElement().checked = true;
        } else {
            check.getElement().checked = false;
        }
    },

    checkAll : function(component, event, helper) {
        let checkboxes = component.find('row-checkbox');
        let self = event.target;
        let selectedRows  = component.get('v.selectedRows');
        let windows = component.get('v.windowData') == null ? [] : component.get('v.windowData');

        // add/delete each window from selectedRows
        if(Array.isArray(windows)) {
            for(let window of windows) {
                window.isChecked = self.checked;
                if(self.checked && !selectedRows.includes(window.Id)) {
                    selectedRows.push(window.Id);
                } else if(!self.checked && selectedRows.includes(window.Id)) {
                    selectedRows.splice(selectedRows.indexOf(window.Id), 1);
                }
            }
        } else {
            windows.isChecked = self.checked;
            if(self.checked && !selectedRows.includes(windows.Id)) {
                selectedRows.push(windows.Id);
            } else if(!self.checked && selectedRows.includes(windows.Id)) {
                selectedRows.splice(selectedRows.indexOf(windows.Id), 1);
            }
        }

        // check/uncheck each window
        if(Array.isArray(checkboxes)) {
            for(let check of checkboxes) {
                check.getElement().checked = self.checked;
            }
        } else {
            checkboxes.getElement().checked = self.checked;
        }

        component.set('v.selectedRows', selectedRows);
    },

    cloneRecords : function(component, event, helper) {
        try {
            var selectedRows = component.get('v.selectedRows');

            if(selectedRows.length === 1) {
			    document.getElementById("clone-window-guideline-modal").style.display = "block";
			} else {
                helper.errorToast('You can only clone one Window Guideline at a time.');
            }
        } catch(e) {
            console.log(e);
            helper.errorToast('There was an error while trying to clone this title.');
        }
    },

    showDeactivateModal : function(component, event, helper) {
        var selectedRows = component.get('v.selectedRows');

        if(selectedRows.length > 0) {
            document.getElementById("deactivate-window-guideline-modal").style.display = "block";
        } else {
            helper.errorToast('You have to select one or more records to be deactivated.');
        }
    },

    editGrid : function(component, event, helper) {
        helper.handleCheckBoxValues(component); //LRCC:1199
        
        var resultData = component.get('v.windowData');
    	component.set('v.myDataOriginal',JSON.parse(JSON.stringify(resultData)));
        let isEditable = component.get('v.isEditable');
		
        let editTableEvent = $A.get('e.c:EAW_EditTableEvent');
        editTableEvent.setParams({'editMode': isEditable});
        editTableEvent.fire();
        //helper.makeUnsavedChanges(component, event, helper);
    },

    resetColumn: function(component, event, helper) {
        let currentEle = component.get('v.currentEle');

        if(component.get("v.currentEle") !== null) {
            let newWidth = component.get("v.newWidth");
            let currentEleParent = currentEle.parentNode.parentNode;
            let parObj = currentEleParent.parentNode;

            parObj.style.width = newWidth + 'px';
            currentEleParent.style.width = newWidth + 'px';

            component.get("v.currentEle").style.right = 0;
            component.set("v.currentEle", null);
        }
    },

    setNewWidth: function(component, event, helper) {

       /* let currentEle = component.get("v.currentEle");
        if(currentEle != null && currentEle.tagName) {
            let parObj = currentEle;
			
            while(parObj.parentNode.tagName != 'TH') {
                if(parObj.className == 'slds-resizable__handle') {
                    currentEle = parObj;
                }

                parObj = parObj.parentNode;
            }

            let mouseStart = component.get("v.mouseStart");
            let oldWidth = parObj.offsetWidth;
            let newWidth = oldWidth + (event.clientX - parseFloat(mouseStart));

            component.set("v.newWidth", newWidth);
            currentEle.style.right = ( oldWidth - newWidth ) + 'px';
            component.set("v.currentEle", currentEle);
            if(currentEle.id){
                var myColumns = component.get('v.myColumns');
                for(var col of myColumns){
                    if(col.fieldName == currentEle.id){
                        console.log(currentEle.id,':::current::',oldWidth - newWidth);
                        col.width = newWidth - 20;
                    }
                }
                component.set('v.myColumns',myColumns);
                component.set('v.bodyWidth',((component.get('v.myColumns').length + 1) * 140 ) + newWidth );
            }
        }*/
        var currentEle = component.get("v.currentEle");
        if( currentEle != null && currentEle.tagName ) {
            var parObj = currentEle;
            while(parObj.parentNode.tagName != 'TH') {
                if( parObj.className == 'slds-resizable__handle')
                    currentEle = parObj;    
                parObj = parObj.parentNode;
                count++;
            }
            var count = 1;
            var mouseStart = component.get("v.mouseStart");
            var oldWidth = parObj.offsetWidth;  // Get the width of DIV
            var newWidth = oldWidth + (event.clientX - parseFloat(mouseStart));
            component.set("v.newWidth", newWidth);
            currentEle.style.right = ( oldWidth - newWidth ) +'px';
            component.set("v.currentEle", currentEle);
            if(currentEle.id){ // LRCC - 1282
                var myColumns = component.get('v.myColumns');
                for(var col of myColumns){
                    if(col.fieldName == currentEle.id){
                        console.log(currentEle.id,':::current::',oldWidth - newWidth);
                        col.width = newWidth;
                    }
                }
                component.set('v.myColumns',myColumns);
                component.set('v.bodyWidth',((component.get('v.myColumns').length + 1) * 140 ) + newWidth );
            }
        }
    },
	saveWindowGuidelines : function( component, event, helper ){
    	
        var resultData = component.get('v.windowData');
    	var myDataOriginal = component.get('v.myDataOriginal');
    	var myColumns = component.get("v.myColumns");
        
        var updatedData = new Array();
        
        for(var i=0 ; i<resultData.length; i++){
            for(let column of myColumns){
                var data = resultData[i];
                var dataOriginal = myDataOriginal[i];
                if(data[column.fieldName] != dataOriginal[column.fieldName]){
                    updatedData.push(data);
                    break;
                }
            }
        }
        console.log(updatedData.length);
        console.log(updatedData);
        if( updatedData && updatedData.length > 0 ) {
	    	var action = component.get('c.saveRecords');
	    	action.setParams({
	    		"releaseDateRecords":JSON.stringify(updatedData)
	    	});
	    	action.setCallback(this,function(response){
	    		var state = response.getState();
	    		if(state == 'SUCCESS') {
	    		    var results = response.getReturnValue();
	    			helper.successToast('The Window Guideline(s) updated successfully.');
	    			//component.set('v.isEditable',false); //LRCC-1668
                    helper.handleCheckBoxValues(component); //LRCC:1199
	                /*
                     * LRCC-1668
	                var editTableEvent = $A.get("e.c:EAW_EditTableEvent");
	                editTableEvent.setParams({"editMode":false});
	                editTableEvent.fire();
                    */
	                component.set('v.myDataOriginal',JSON.parse(JSON.stringify(results)));
	                //component.set("v.checkSaveRecords", true);
	                //helper.clearUnsavedChanges(component, event, helper);  
	    		}else{
	    			helper.errorToast('Error has occurred. Please contact administrator');
	    		}
	    	});
	    	$A.enqueueAction(action);
    	} else {
    		//LRCC-892
        	var toastEvent = $A.get("e.force:showToast");
				toastEvent.setParams({
				    title: "Info!",
				    message: $A.get("$Label.c.EAW_RecordsNotEdited"),
				    type: "info"
				});
				toastEvent.fire();
    	}
    },
    cancel : function( component, event, helper ){
    	var myDataOriginal = component.get('v.myDataOriginal');
    	component.set('v.windowData',myDataOriginal);
    	component.set('v.isEditable',false);
        helper.handleCheckBoxValues(component); //LRCC:1199
        console.log('length:::',component.get('v.selectedRows').length);
        var editTableEvent = $A.get("e.c:EAW_EditTableEvent");
        editTableEvent.setParams({"editMode":false});
        editTableEvent.fire();
    },
    calculateWidth: function(component, event, helper) {
        let childObj = event.target;
        let mouseStart = event.clientX;

        component.set("v.currentEle", childObj);
        component.set("v.mouseStart", mouseStart);
		
        if(event.stopPropagation) {
            event.stopPropagation();
        }

        if(event.preventDefault){
            event.preventDefault();
        }

        event.cancelBubble = true;
        event.returnValue = false;
    },

    sort: function(component, event, helper) {
        //LRCC-1690
        let sortVal = event.currentTarget.dataset.value;
        
        component.set('v.selectedSortingParam',{'sortVal':sortVal});
        console.log('sortparam::',JSON.stringify(component.get('v.selectedSortingParam')));
        console.log('old value:'+sortVal);
        helper.sortBy(component,sortVal,false);
        //LRCC-1459
        component.set('v.colWithAlpha',{'selectedCol':null,'selectedAlpha':null});	
        var target = event.currentTarget;
        var rowIndex = target.getAttribute("data-colIndex");
        console.log("Row No : " + rowIndex);
        console.log('::: selected :::', JSON.stringify(component.get("v.myColumns")[rowIndex]));
        
        if((component.get("v.myColumns")[rowIndex].type == "string" || 
           component.get("v.myColumns")[rowIndex].type == "picklist") && 
           ((component.get("v.myColumns")[rowIndex].fieldName.indexOf("__c") > -1)||
           component.get("v.myColumns")[rowIndex].fieldName == "Name")){
            
            var colAplpha = component.get('v.colWithAlpha');
            colAplpha.selectedCol = component.get("v.myColumns")[rowIndex].fieldName;
            component.set('v.colWithAlpha',colAplpha)
        }else{
            helper.showErrorToast(component,'Selecting column is not valid for alphabet sort');
            component.set('v.colWithAlpha',{'selectedCol':null,'selectedAlpha':null});	
            return;
        }
        console.log('::: selected :::',JSON.stringify(component.get('v.colWithAlpha')));
        console.log('::: selected :myColumns::',JSON.stringify( component.get("v.myColumns")));
    },
    rowAction : function(component, event, helper) {
    	//var recId = event.currentTarget.dataset.value;
        var recId = event.getSource().get('v.name');
        var notesEvent = $A.get("e.c:EAW_NotesEvent");
        notesEvent.setParams({"parentId":recId});
        notesEvent.fire();
    },
    //LRCC-1459
    renderPage : function(component, event, helper) {
        
        console.log('click::::',component.get("v.pagesPerRecods"),':::>::',component.get("v.pageNumber"),'<<>>',component.get('v.colWithAlpha'))
        let isAlphaFiltered = component.get("v.isAlphaFiltered");
        
        let pageConfigEvt = component.getEvent("pageConfigEvt");
  
        pageConfigEvt.setParams({"pagesPerRecord" : component.get("v.pagesPerRecod"),
                                 "currentPageNumber":component.get("v.pageNumber"),
                                 "selectedFilterMap" : isAlphaFiltered ? component.get('v.colWithAlpha') : null
                                 });
        pageConfigEvt.fire();
		/*let pageRecordsIdMap = component.get("v.pageRecordsIdMap");
        pageRecordsIdMap[component.get("v.pageNumber")] = component.get("v.rowsToDisplay");
        component.set("v.pageRecordsIdMap",pageRecordsIdMap);
        console.log('pageRecordsIdMap',pageRecordsIdMap);
		helper.renderPage(component);*/
	},
	handleChange: function(component, event, helper) {
        
         if(component.get("v.isEditable")){
            helper.showErrorToast(component);
            component.find('pagePerRecord').set("v.value",component.get("v.oldPagesPerRecord"));
            return;
        } 
         
        component.set('v.pageNumber', 1);
        let pagesPerRecord = parseInt(component.find('pagePerRecord').get("v.value"));
         
        component.set("v.pagesPerRecod",component.find('pagePerRecord').get("v.value"));
        component.set("v.oldPagesPerRecord",component.find('pagePerRecord').get("v.value"));
        component.set("v.maxPage", Math.floor((component.get('v.totalRecordCount')+pagesPerRecord - 1 )/pagesPerRecord)); 
         
         var pageConfigEvt = component.getEvent("pageConfigEvt");
            pageConfigEvt.setParams({"pagesPerRecord" : component.get("v.pagesPerRecod"),
                                     "currentPageNumber":component.get("v.pageNumber")});
            pageConfigEvt.fire();
    },    
    chooseAlphabet : function(component, event, helper) {
        
       if(component.get("v.isEditable")){
            helper.showErrorToast(component);
            return;
        } 
        
        var target = event.currentTarget;
        var rowIndex = target.getAttribute("data-rowIndex");
       
        if(component.get("v.alphabets")[rowIndex]){
            
            var colAplpha = component.get('v.colWithAlpha');
            colAplpha.selectedAlpha = component.get("v.alphabets")[rowIndex];
            component.set('v.colWithAlpha',colAplpha);
        }
        let colWithAlpha = component.get('v.colWithAlpha');
        if( colWithAlpha.selectedCol != null ||   colWithAlpha.selectedAlpha == 'All' ){
            
            if(colWithAlpha.selectedAlpha == 'All'){
                
                colWithAlpha.selectedCol == null;
                component.set('v.colWithAlpha',colWithAlpha);
                component.set("v.isAlphaFiltered",false);
            }
            component.set("v.isAlphaFiltered",true);
            component.set('v.pageNumber', 1);
            component.set("v.pageIdMap",{});
            var pageConfigEvt = component.getEvent("pageConfigEvt");
            
            pageConfigEvt.setParams({"pagesPerRecord" : component.get("v.pagesPerRecod"),
                                     "currentPageNumber":1,
                                     "selectedFilterMap" : component.get('v.colWithAlpha') });
            pageConfigEvt.fire();

        }
        if(colWithAlpha.selectedAlpha == 'All'){
            
            component.set('v.colWithAlpha',{'selectedCol':null,'selectedAlpha':null});	
        }
        
         console.log('::: selected :::', component.get('v.colWithAlpha'));
    },
    displayUsedBy : function(cmp,event,helper){
        if(event.getSource().get('v.name')){
            //console.log(event.getSource().get('v.name')+'::::::::::'+cmp.find('usedByModal'));
            //cmp.find('modalCmp').set('v.recordId',event.getSource().get('v.name'));
            cmp.find('usedByModal').openModal(event.getSource().get('v.name'));
        }
    }
})