({
    sortBy : function (component, field,nextFlag) {
        let sortAsc = component.get("v.sortAsc");
        let sortField = component.get("v.sortField");
        let records = component.get("v.rowsToDisplay");//component.get("v.windowData");
		if(!nextFlag){
            
             sortAsc = field == sortField ? !sortAsc : true;
        }
         records.sort(function(a, b) {
            if(sortAsc) {
                if(a[field] == undefined) {
                    return -1;
                } else if(b[field] == undefined) {
                    return 1;
                }
                
                return (new String(a[field]).toUpperCase() > new String(b[field]).toUpperCase()) ? 1 : ((new String(b[field]).toUpperCase() > new String(a[field]).toUpperCase()) ? -1 : 0);
            } else {
                if(a[field] == undefined) {
                    
                    return 1;
                } else if(b[field] == undefined) {
                    return -1;
                }
                
                return (new String(a[field]).toUpperCase() < new String(b[field]).toUpperCase()) ? 1 : ((new String(b[field]).toUpperCase() < new String(a[field]).toUpperCase()) ? -1 : 0);
            }
        });
		
        component.set("v.sortAsc", sortAsc);
        component.set("v.sortField", field);
        component.set("v.windowData", records);
        component.set("v.rowsToDisplay", records);
    	
        /****LRCC:887-Sorting indicator applied logic****/
        component.set("v.selectedTabsoft", field);
        if (sortAsc == true) {
            component.set("v.arrowDirection", 'arrowup');
        } else {
            component.set("v.arrowDirection", 'arrowdown');
        }
    },
	
    fieldSet: function (component,event,helper) {
        try {
            var strObjectName = component.get("v.strObjectName");
            var strFieldSetName = component.get("v.strFieldSetName");
            var strObjFieldSetMap = new Map();

            // Map EAW_Window_Guideline__c to Search_Result_Fields
            strObjFieldSetMap.set(strObjectName, strFieldSetName);

            var action = component.get("c.getFields");

            action.setParams(
                {
                    "objFieldSetMap": {
                        "EAW_Window_Guideline__c": "Search_Result_Fields",
                        "EAW_Plan_Guideline__c": "Plan_Guideline_Name",
                        "EAW_Customer__c": "Customer_Name"
                    }
                }
            );

            action.setCallback(this, function(response) {
                var state = response.getState();

                if(state === "SUCCESS") {
                    
                    var columns = new Array();
                    for(let col of response.getReturnValue()) {
                        
                        for(let colName of col) {
                            colName.width = '119';
                            /****LRCC:800****/ 
                            if(colName && colName.label == 'Window Guideline Alias'){
                                //LRCC-1694
                                colName.width = '199';// '165';
                            }
                            if(colName && colName.label == 'Window Guideline Status'){
                              
                                colName.width = '170';
                            }
                            if(colName && colName.label == 'Plan Guideline Name') {
                            	colName.fieldName = 'planName';
                                //LRCC-1694
                                colName.width = '199';// '140';
                                columns.push(colName);	
                            } 
                            else if(colName &&  colName.label == 'Plan Guideline Status') {
                            	colName.fieldName = 'planStatus';
                                colName.width = '150';
                                columns.push(colName);	
                            } 
                            else if(colName &&  colName.label == 'PG Version') {
                            	colName.fieldName = 'planVersion';
                                columns.push(colName);	
                            } 
                            else if(colName &&  colName.label == 'Sales Region') {
                            	colName.fieldName = 'planSalesRegion';
                                columns.push(colName);	
                            } 
                            else if(colName &&  colName.label == 'Release Type') {
                            	colName.fieldName = 'planReleaseType';
                                columns.push(colName);	
                            } 
                            else if(colName && colName.label == 'Customer Name') {
                                colName.fieldName = 'customerName';
                                columns.push(colName);
                            } 
                            else if(colName && colName.label != 'PG Product Type'  && colName.label != 'All Rules') { //&& colName.fieldName != 'Product_Type__c') { //LRCC-1493
                                columns.push(colName);
                            } 
                        }
                    }
                    if(columns.length){
                        columns = helper.setEditable(columns);
                    }
                    component.set('v.myColumns', columns);
                    //helper.setBodyWidth(component); // LRCC - 1282
                } else if(state === "ERROR") {
                    var errors = response.getError();

                    if(errors) {
                        if(errors[0] && errors[0].message) {
                            console.log("Error message: " +
                            errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                } else {
                    console.log("Something went wrong, Please check with your admin");
                }
            });
            $A.enqueueAction(action);
        } catch (e) {
            console.log('Exception: ' + e);
        }
    },

    errorToast : function(message) {
        var toastEvent = $A.get('e.force:showToast');

        toastEvent.setParams({
            'title': 'Error!',
            'type': 'error',
            'message': message
        });

        toastEvent.fire();
    },

    successToast : function(message) {
        var toastEvent = $A.get('e.force:showToast');
		
        toastEvent.setParams({
            'title': 'Success!',
            'type': 'success',
            'message': message
        });

        toastEvent.fire();
    },
    
    /****LRCC:1199-Edit toggle Action Logic****/
    handleCheckBoxValues: function(component) {
        //LRCC-1633
        let windows = component.get('v.rowsToDisplay');
        
        if(component.get('v.isEditable') == false) {
            component.find('header-checkbox').getElement().checked = false; 
            //LRCC-1633
            var checkboxes = component.find("row-checkbox");
            console.log('checkboxes:::',checkboxes);
            
            if(!Array.isArray(checkboxes)){
                checkboxes = [checkboxes];
            }
            if(checkboxes) {
                for(let check of checkboxes) {
                    if(check.getElement() != null)
                    check.getElement().checked = false;
                }
            }
            
            for(let window of windows) {
                window.isChecked = false;
            }
            component.set('v.selectedRows',[]);
            component.set('v.windowData',windows);
            
            console.log('windows value::',windows);
        }
    },
    setEditable : function(columns) {
        
        var updatableColumns = columns;
        updatableColumns.find(function(element) {
            element.updateable = false;
            /*LRCC:1214
            if(element.fieldName == 'Status__c' || element.fieldName == 'Customer__c' || element.fieldName == 'Media__c' || element.fieldName == 'EAW_Life_cycle__c' || element.fieldName == 'Release_Type__c'){
                element.updateable = true;
            } else{
                element.updateable = false;
            }*/
        });
        return updatableColumns;
    },
    makeUnsavedChanges: function(cmp, evt, helper) {
        var unsaved = cmp.find("unsaved");
       // unsaved.setUnsavedChanges(true, { label: 'Window Guideline Results' });
    },
    clearUnsavedChanges: function(cmp, evt, helper) {
        var unsaved = cmp.find("unsaved");
        //unsaved.setUnsavedChanges(false);
    },
    setBodyWidth : function(component) {
        //console.log(':::::::::::::::::',component.get('v.myColumns').length * 140 );
        component.set('v.bodyWidth',(component.get('v.myColumns').length + 1 ) * 140 );
    },//LRCC-1459
    generateAlphabets : function(component, event, helper) {
          console.log('generateAlphabets:::1::');
        var alphabets = [];
        for(var i= 65 ; i < 91 ; i++){
            
            alphabets.push(String.fromCharCode(i));
        }
        alphabets.push('All');
        component.set('v.alphabets',alphabets);
          //LRCC-1459
		 helper.renderPage(component, event, helper)
        
    },
    //LRCC-1459
    formPageIdMap : function(component, event, helper) {
      console.log('formPageIdMap:::::');
        const pageNumber = component.get("v.pageNumber"),
            rowsToDisplay = component.get("v.rowsToDisplay");
        
        let pageIdMap = component.get("v.pageIdMap") || {};
        
        const recordIds = rowsToDisplay.map((rec) =>  rec.Id);
        
        pageIdMap[pageNumber] = recordIds;
        
        component.set("v.pageIdMap",pageIdMap);
        console.log('pageIdMap:::::test:::::::',JSON.stringify(pageIdMap));
        
        let pagesPerRecord = parseInt(component.find('pagePerRecord').get("v.value"));
        console.log('pagesPerRecord count ::::',pagesPerRecord);
        console.log('total count ::::',component.get('v.totalRecordCount'));
        console.log('total count ::::',component.get('v.totalRecordCount'));
        console.log('calc ::::',(component.get('v.totalRecordCount') + pagesPerRecord - 1 )/pagesPerRecord);
        component.set("v.maxPage", Math.floor((component.get('v.totalRecordCount') + pagesPerRecord - 1 )/pagesPerRecord)); 
        console.log('maxPage count ::::',component.get('v.maxPage'));
        
        var selctsort = component.get('v.selectedSortingParam');
        console.log('tosort::',selctsort);
        if(selctsort){
            this.sortBy(component, selctsort.sortVal,true);  
        }
       
    },
    //LRCC-1459
    reinitializePageMap : function(component,pageNumber) {
        
        let pageIdMap = component.get("v.pageIdMap");
        
        for(let i = pageNumber ; i <= Object.keys(pageIdMap).length ; i++ ){
            pageIdMap[i] = [];
        } 
        component.set("v.pageIdMap",pageIdMap);
    },
    //LRCC-1459
    renderPage: function(component, event, helper) {
        
        console.log('call render page:::1::',component.get("v.windowData"));
        console.log('call render page:::2::',component.get("v.pageNumber"));
        console.log('call render page:::3::',component.find('pagePerRecord'));
        console.log('call render page:::::',component.find('pagePerRecord').get("v.value"));
        
        let records = component.get("v.windowData") || [];
        
         //var pageNumber = component.get("v.pageNumber");
        //var pagesPerRecord = parseInt(component.find('pagePerRecord').get("v.value"));
       //var  rowsToDisplay = records;
        
        component.set("v.rowsToDisplay", records);
        console.log('rowsToDisplay::test:::',records);
        helper.formPageIdMap(component);
        
    },
    //LRCC-1459
    showErrorToast : function(component,msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: "Info!",
            message:msg ,
            type: "info"
        });
        toastEvent.fire();
    },
    setFixedHeader : function(component){
        if(component.get('v.rowsToDisplay') && component.get('v.rowsToDisplay').length){  // LRCC - 1282
            const globalId = component.getGlobalId(),
                parentnode = document.getElementById(globalId),
                thElements = parentnode.getElementsByTagName('th'),
                tdElements = parentnode.getElementsByTagName('td');
                
            var bodyWidth = 0;
             
            for (let i = 0; i < thElements.length; i++) {
                //console.log(thElements[i].offsetWidth+':::::::::::::::'+tdElements[i]);
                if(tdElements[i] && thElements[i]){
                    tdElements[i].style.width = thElements[i].offsetWidth + 1 + 'px';
                }
                bodyWidth += thElements[i].offsetWidth;
            }
            //  console.log(':::::::bodyWidth:::::::::::'+bodyWidth); 
            component.set('v.bodyWidth',bodyWidth);
        }
    },
})