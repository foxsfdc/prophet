({
    resetNewWindowGuidelineFields : function(component) {
        var newWindowGuideline = component.get("v.newWindowGuideline");
        newWindowGuideline = this.newWindowGuidelineFormat();
        component.set("v.newWindowGuideline", newWindowGuideline);
        
        component.find("new-window-guideline-input-name").set("v.value", newWindowGuideline.Name);
        component.find("new-window-guideline-select-media").set("v.value", newWindowGuideline.Media__c);
        //LRCC-1560 - Replace Customer lookup field with Customer text field
        //component.find("new-window-guideline-lookup-customer").set("v.value", newWindowGuideline.Customer__c);
        component.find("new-window-guideline-lookup-customer").set("v.value", newWindowGuideline.Customers__c);
        component.find("new-window-guideline-textarea-description").set("v.value", newWindowGuideline.Description__c);
        component.find("new-window-guideline-select-window-type").set("v.value", newWindowGuideline.Window_Type__c);
        component.find("new-window-guideline-input-version").set("v.value", newWindowGuideline.Version__c);
        component.find("new-window-guideline-select-version-status").set("v.value", newWindowGuideline.Version_Status__c);
        component.find("new-window-guideline-textarea-version-notes").set("v.value", newWindowGuideline.Version_Notes__c);
    },
    newWindowGuidelineFormat : function(newWindowGuideline) {
        return {
            Name: "",
            Media__c: "Basic TV",
            //LRCC-1560 - Replace Customer lookup field with Customer text field
            //Customer__c: "",
            Customers__c: "",
            Description__c: "",
            Window_Type__c: "1st Run",
            Version__c: 1,
            Version_Status__c: "Draft",
            Version_Notes__c: ""
        };
    },
    formatPlanWindowGuidelineJunction : function(response) {
        var formatted = {};
        
        formatted.Id = response.Plan_Window_Guideline_Junction__r.Id;
        formatted.Plan__c = response.Plan_Window_Guideline_Junction__r.Plan__c;
        formatted.Window_Guideline__c = response.Plan_Window_Guideline_Junction__r.Window_Guideline__c;
        formatted.Window_Guideline__r = response.Window_Guideline_r;
        
        return formatted;
    },
	hideNewWindowGuidelineModal : function(component) {
        document.getElementById("eaw-new-window-guideline-modal").style.display = "none";
        this.resetNewWindowGuidelineFields(component);
    }
})