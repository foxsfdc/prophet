({	
    getRulesAndDetails : function(cmp,event,helper){
        cmp.set('v.spinner',true);
        cmp.set('v.setBody',false);
        var action = cmp.get('c.getRulesAndDetails');
        console.log(':::::recordId:::::::::'+cmp.get('v.recordId'))
        action.setParams({
            releasedateGuidelineId : cmp.get('v.recordId')
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state == 'SUCCESS') {
                
                cmp.set('v.releaseWindowDateWrapper',response.getReturnValue().releaseWindowDateWrapper);
                //LRCC-1540
                var isAllowManual = response.getReturnValue().isAllowManual;
                if(isAllowManual) {
                    cmp.set('v.isAllowManual', isAllowManual);
                }
                if(cmp.get('v.isAllowManual') && ! isAllowManual) {
                    helper.showToast(cmp,event,helper,'info','There may be manual dates entered for this release date. These will not be overwritten or removed.');          
					cmp.set('v.isAllowManual', isAllowManual);
                }
                
                if(response.getReturnValue().rule) {
                    cmp.set('v.rule',response.getReturnValue().rule);
                    helper.setRuleDetailByRules(cmp,event,helper,response.getReturnValue()); 
                    cmp.set('v.isNew',false);
                    cmp.set('v.edit',false);
                } else {
                    cmp.set('v.selectedContext',''); 
                    cmp.set('v.isNew',true);
                }
                cmp.set('v.setBody',true);
                //LRCC-1631
                cmp.set('v.isActive', response.getReturnValue().isActive);
            } else if(state == 'ERROR') {
                console.log(response.getError()[0].message);
            }
             cmp.set('v.spinner',false);
        });
        $A.enqueueAction(action);
    },
    setConditionBasedNode : function(cmp,event,helper,trueBased,falseBased) {
        
        var trueBasedRuleDetailObject = JSON.parse(JSON.stringify(cmp.get('v.ruleDetailObject')));
        trueBasedRuleDetailObject.Rule_Order__c = 1;
        trueBasedRuleDetailObject.Nest_Order__c = 1;
        
        var trueNewRuleDetailJSON = {
            'ruleDetail' : trueBased || JSON.parse(JSON.stringify(trueBasedRuleDetailObject)),
            'dateCalcs' : [],
            'nodeCalcs' : []                
        };   
        var falseBasedRuleDetailObject = JSON.parse(JSON.stringify(cmp.get('v.ruleDetailObject')));
        falseBasedRuleDetailObject.Rule_Order__c = 2;
        falseBasedRuleDetailObject.Nest_Order__c = 1;
        
        var falseNewRuleDetailJSON  = {
            'ruleDetail' : falseBased || JSON.parse(JSON.stringify(falseBasedRuleDetailObject)),
            'dateCalcs' : [],
            'nodeCalcs' : []                
        }; 
     	
        var conditionBased = {
            'TrueBased' : {'ruleOrder' : 1,
                           'ruleDetailJSON' :  JSON.parse(JSON.stringify(trueNewRuleDetailJSON))},
            'FalseBased' : {'ruleOrder' : 2,
                            'ruleDetailJSON' : JSON.parse(JSON.stringify(falseNewRuleDetailJSON))}
        };
        cmp.set('v.conditionBasedMap',conditionBased);
    },
    setCaseBasedNode : function(cmp,event,helper,whenBasedList,otherBased) {
        
        var newRuleDetail = JSON.parse(JSON.stringify(cmp.get('v.ruleDetailObject')));
        newRuleDetail.Rule_Order__c = 1;
        newRuleDetail.Nest_Order__c = 1;
        newRuleDetail.Condition_Type__c = 'Case';
       
        var newRuleDetailJSON = {
            'ruleDetail' : newRuleDetail,
            'dateCalcs' : [],
            'nodeCalcs' :  []               
        };   
        newRuleDetail = JSON.parse(JSON.stringify(newRuleDetail));
        newRuleDetail.Nest_Order__c = 2;
        var nodeRuleDetail = {
            'ruleDetail' : newRuleDetail,
            'dateCalcs' : [],
            'nodeCalcs' :  [] 
        }
        newRuleDetailJSON.nodeCalcs = [nodeRuleDetail];
        var caseRuleDetailMap = {
            'whenBased' : whenBasedList ||  [JSON.parse(JSON.stringify(newRuleDetailJSON))],
            'otherBased' : otherBased || JSON.parse(JSON.stringify(newRuleDetailJSON))
        }
        
        cmp.set('v.caseRuleDetailMap',caseRuleDetailMap);
        console.log('caseRuleDetailMap::',caseRuleDetailMap);
    },
    setEarliestLatestBasedNode : function(cmp,event,helper,ruleDetail) {
        var newRuleDetailJSON = {
            'ruleDetail' : ruleDetail || JSON.parse(JSON.stringify(cmp.get('v.ruleDetailObject'))),
            'dateCalcs' : [],
            'nodeCalcs' : []                
        };   
        cmp.set('v.newRuleDetailJSON',newRuleDetailJSON);
    },
    setDateBasedNode : function(cmp,event,helper,dateNode) {
        var newRuleDetail = dateNode || JSON.parse(JSON.stringify(cmp.get('v.ruleDetailObject')));
        newRuleDetail.Rule_Order__c = 1;
        newRuleDetail.Nest_Order__c = 1;
        cmp.set('v.dateCalcBasedMap',newRuleDetail);
    },
    setRuleDetailByRules : function(cmp,event,helper,response){
        cmp.set('v.ruleDetailList',response.ruleDetailList);
        //LRCC-1454
        console.log(':::::::::::::'+JSON.stringify(response.parentRuleDetailList));
        if(response.parentRuleDetailList.length == 1 && response.parentRuleDetailList[0].Condition_Type__c != 'If' && response.parentRuleDetailList[0].Condition_Type__c != 'Case then'){
            if(!response.ruleDetailList.length){
                helper.setDateBasedNode(cmp,event,helper,response.parentRuleDetailList[0]);
                cmp.set('v.selectedContext','date');
            } else{
                helper.setEarliestLatestBasedNode(cmp,event,helper,response.parentRuleDetailList[0]);   
                cmp.set('v.selectedContext','earliestLatest');
            }
            //LRCC-1454
        } else if(response.parentRuleDetailList.length > 0 && (response.parentRuleDetailList[0].Condition_Type__c == 'If' || response.parentRuleDetailList[0].Condition_Type__c == 'Else/If')){
            var trueBased =  response.parentRuleDetailList.find(function(element) {
                return element.Condition_Type__c == 'If';
            });
            var falseBased = response.parentRuleDetailList.find(function(element) {
                return element.Condition_Type__c == 'Else/If';
            });
            console.log(trueBased,falseBased)
            helper.setConditionBasedNode(cmp,event,helper,trueBased,falseBased);
            cmp.set('v.selectedContext','condition');
        } else{
            var whenBasedList = response.parentRuleDetailList;
            var otherBased = whenBasedList.find(function(element) {
                return element.Condition_Type__c == 'Otherwise';
            });
            console.log(otherBased);
            if(otherBased){
            	whenBasedList.splice(whenBasedList.indexOf(otherBased),1);
            }
            var caseNodeList = helper.setNodeRuleDetailForCase(cmp,event,helper,whenBasedList,response.ruleDetailList);
            if(otherBased){
            	otherBased = helper.setNodeRuleDetailForCase(cmp,event,helper,[otherBased],response.ruleDetailList);
            } else{
                otherBased = [otherBased];
            }
            console.log('otherBased:::::',otherBased)
            helper.setCaseBasedNode(cmp,event,helper,caseNodeList,otherBased[0]);
            cmp.set('v.selectedContext','multidate');
        }
    },
    
    saveRuleAndRuleDetailsNodes : function(cmp,event,helper,ruleDetailContent){
        cmp.set('v.spinner',true);
        var action = cmp.get('c.saveRuleAndRuleDetails');
        cmp.get('v.rule').Release_Date_Guideline__c = cmp.get('v.recordId');
        //console.log(ruleDetailContent);
        action.setParams({
            'rule' : JSON.stringify(cmp.get('v.rule')),
            'ruleAndRuleDetailNodes' : JSON.stringify(ruleDetailContent),
            'ruleDetilsToDelete' : cmp.get('v.ruleDetailstoDelete') || null,
            'releaseDateGuidelineId' : cmp.get('v.recordId') || null,
            'ruleText' : helper.setDetailJSONString(JSON.parse(JSON.stringify(ruleDetailContent)),JSON.parse(JSON.stringify(cmp.get('v.rule'))),helper) || ''
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state == 'SUCCESS'){
                console.log('sucess');
                var message = 'Record was updated successfully';
                if(cmp.get('v.isNew')){
                    var message = 'Reocrd was inserted succesfully';
                }
                helper.showToast(cmp,event,helper,state,message);
                cmp.set('v.edit',false);
                cmp.set('v.isNew',false);
                cmp.set('v.spinner',false);
                location.reload();
            } else if(state == 'ERROR'){
                var errorMessage = helper.buildErrorMsg(cmp,event,helper,response.getError());
                helper.showToast(cmp,event,helper,state,errorMessage);
                cmp.set('v.spinner',false);
            }
        });
        $A.enqueueAction(action);
    },
    showToast : function(cmp, event, helper, type, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "type": type,
            "message": message
        });
        toastEvent.fire();
    },
    buildErrorMsg : function(cmp, event, helper, errors){
        var errorMsg = "";
        
        if(errors[0] && errors[0].message){  // To show other type of exceptions
            errorMsg = errors[0].message;
        }else if(errors[0] && errors[0].pageErrors.length) {  // To show DML exceptions
            errorMsg = errors[0].pageErrors[0].message; 
        }else if(errors[0] && errors[0].fieldErrors){  // To show field exceptions
            var fields = Object.keys(errors[0].fieldErrors);
            var field = fields[0];
            errorMsg = errors[0].fieldErrors[field];
            errorMsg = errorMsg[0].message;
        }else if(errors[0] && errors[0].duplicateResults.length){ // To show duplicateResults exceptions
            errorMsg = errors[0].duplicateResults[0].message;
        }
        
        return errorMsg;
        
    },
    deleteRuleDetailById : function(cmp,event,helper,ruleDetailId,isDateCalc,isRule){
        
        var action = cmp.get('c.deleteRuleAndDetails');
       // console.log('action');
        action.setParams({
            'isRule' : isRule,
            'isDateCalc' : isDateCalc,
            'ruleOrRuleDetailId' : ruleDetailId,
            'releaseDateGuidelineId' : cmp.get('v.recordId') || null,
        });
        cmp.set('v.spinner',true);
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state == 'SUCCESS'){
                console.log(response.getReturnValue());                
            } else if(state == 'ERROR'){
                var errorMessage = helper.buildErrorMsg(cmp,event,helper,response.getError());
                helper.showToast(cmp,event,helper,state,errorMessage);              
            }
            cmp.set('v.spinner',false);
        });
        $A.enqueueAction(action);            
    },
    openModal : function(cmp,event,helper,message){
        cmp.find('promt').set('v.content',message);
        cmp.find('promt').openModal();
    },
    checkForNullNodes : function(cmp,event,helper,ruleDetailJSON){
        var notValid = false;
        if(ruleDetailJSON.dateCalcs.length || ruleDetailJSON.nodeCalcs.length){
            if(ruleDetailJSON.ruleDetail && !ruleDetailJSON.ruleDetail.Condition_Timeframe__c){
                notValid = true;
                return notValid;
            }
            return helper.validNodes(cmp,event,helper,notValid,ruleDetailJSON.dateCalcs,ruleDetailJSON.nodeCalcs);
        } else{
            //return helper.validNodes(cmp,event,helper,notValid,[ruleDetailJSON.ruleDetail],[]);
            //LRCC-1454
            if(ruleDetailJSON.ruleDetail && (ruleDetailJSON.ruleDetail.hasOwnProperty('Conditional_Operand_Id__c') ||ruleDetailJSON.ruleDetail.hasOwnProperty('Condition_Date_Text__c') || ruleDetailJSON.ruleDetail.hasOwnProperty('Condition_Timeframe__c'))){
                
            	return helper.validNodes(cmp,event,helper,notValid,[ruleDetailJSON.ruleDetail],[]);    
            }else {
                return false;
            }
        }
    },
    validNodes : function(cmp,event,helper,notValid,dateCalcs,nodeCalcs){

        if(dateCalcs.length){
            for(var i=0;i<dateCalcs.length;i++){
                if($A.util.isEmpty(dateCalcs[i].Conditional_Operand_Id__c) || $A.util.isEmpty(dateCalcs[i].Condition_Met_Months__c) ||  $A.util.isEmpty(dateCalcs[i].Condition_Met_Days__c) || $A.util.isEmpty(dateCalcs[i].Condition_Operand_Details__c) || (!$A.util.isEmpty(dateCalcs[i].Condition_Date_Duration__c) && ($A.util.isEmpty(dateCalcs[i].Condition_Criteria_Amount__c) || (dateCalcs[i].Condition_Date_Duration__c == 'Day' && $A.util.isEmpty(dateCalcs[i].Day_of_the_Week__c))))){  // LRCC - 1477
                    notValid = true;
                    break;
                }
            } 
        } else if(!nodeCalcs.length){
            return true;
        }
        if(!notValid &&  nodeCalcs.length){
            for(var i=0;i<nodeCalcs.length;i++){
                if((!nodeCalcs[i].ruleDetail.Condition_Timeframe__c)){
                    if(nodeCalcs[i].ruleDetail.Parent_Condition_Type__c){
                        console.log('::Parent_Condition_Type__c:'+nodeCalcs[i].ruleDetail.Parent_Condition_Type__c);
                        if(helper.checkForNestedNodes(cmp,event,helper,nodeCalcs[i])){
                            notValid = true;
                            return notValid;
                        }
                    } else if(nodeCalcs[i].ruleDetail.Condition_Type__c){
                        console.log(':::here::'+JSON.stringify(nodeCalcs[i].ruleDetail));
                        if(helper.validNodes(cmp,event,helper,notValid,[nodeCalcs[i].ruleDetail],[])){
                            notValid = true;
                            return notValid;
                        }
                    }
                    else{
                        notValid = true;
                        return notValid;
                    }
                } else{
                    if(helper.validNodes(cmp,event,helper,notValid,nodeCalcs[i].dateCalcs,nodeCalcs[i].nodeCalcs)){
                        return true;
                    }
                }
            }
        } else{
            return notValid;
        }       
        
    },
    checkForRule : function(cmp,event,helper,rule){
        if(!rule.Operator__c  || !rule.Condition_Field__c || (!rule.Condition_Operand_Details__c && rule.Condition_Field__c != 'Title Tag') || !rule.Criteria__c){
            return true;
        } else{
            return false;
        }
    },
    setNodeRuleDetailForCase : function(cmp,event,helper,basedNode,ruleDetailList){
        var basedList = [];
        for(var i=0;i<basedNode.length;i++){
            var trueNewRuleDetailJSON = {
                'ruleDetail' : basedNode[i],
                'dateCalcs' : [],
                'nodeCalcs' : []                
            };  
            basedList.push(trueNewRuleDetailJSON);
        }
        console.log(basedList);
        return basedList;
    },
    validForCase : function(cmp,event,helper,caseNode,isValid){
        if(caseNode.ruleDetail){
          //  console.log(caseNode.nodeCalcs[0])
            if(caseNode.nodeCalcs[0].nodeCalcs.length){
                isValid = helper.validNodes(cmp,event,helper,false,caseNode.dateCalcs,caseNode.nodeCalcs) || helper.validParentRuleDetail(caseNode.ruleDetail);
            } else{
                if(caseNode.nodeCalcs[0].dateCalcs && caseNode.nodeCalcs[0].dateCalcs.length){
                    isValid = helper.validParentRuleDetail(caseNode.ruleDetail) || helper.validNodes(cmp,event,helper,false,[],caseNode.nodeCalcs);
                } else{
                    isValid = helper.validDateCalcs(caseNode.nodeCalcs[0].ruleDetail) || helper.validParentRuleDetail(caseNode.ruleDetail);
                }
            }
            return isValid;
        } else{
            return true;
        }
    },
    validParentRuleDetail : function(ruleDetail){
        if(ruleDetail.Condition_Type__c == 'Case then'){
            //console.log(ruleDetail)
            if($A.util.isEmpty(ruleDetail.Condition_Set_Operator__c) || $A.util.isEmpty(ruleDetail.Condition_Field__c) ||  ($A.util.isEmpty(ruleDetail.Condition_Operand_Details__c) && ruleDetail.Condition_Field__c != 'Title Tag') || $A.util.isEmpty(ruleDetail.Condition_Criteria__c)){
                return true;
            } else{
                return false;
            }
        } else{
            return false;
        }
        
    },
    validDateCalcs : function(dateCalcs){
        if($A.util.isEmpty(dateCalcs.Conditional_Operand_Id__c) || $A.util.isEmpty(dateCalcs.Condition_Met_Months__c) ||  $A.util.isEmpty(dateCalcs.Condition_Met_Days__c) || $A.util.isEmpty(dateCalcs.Condition_Operand_Details__c) || (!$A.util.isEmpty(dateCalcs.Condition_Date_Duration__c) && ($A.util.isEmpty(dateCalcs.Condition_Criteria_Amount__c) || (dateCalcs.Condition_Date_Duration__c == 'Day' && $A.util.isEmpty(dateCalcs.Day_of_the_Week__c))))){  // LRCC - 1477
            return true;
        } else{
            return false;
        }
        
    },
    setDetailJSONString : function(ruleDetailJSON,rule,helper){
        var JSONString = '';
        if(rule.Condition_Type__c ){
            if(rule.Condition_Type__c == 'If'){
                JSONString = 'Condition : '+ helper.setConditionNode(rule.Condition_Field__c,rule.Condition_Operand_Details__c,rule.Operator__c,rule.Criteria__c);
                for(var ruleDetailNode of ruleDetailJSON){
                    if(ruleDetailNode.ruleDetail.Condition_Type__c == 'If'){
                        JSONString += 'If TRUE then, ';
                    } else{
                        JSONString += 'If FALSE then, ';
                    }
                    if(ruleDetailNode.ruleDetail.Condition_Timeframe__c){
                        JSONString += helper.setEarliestLatestNode(ruleDetailNode,helper);
                    } else{
                        JSONString += helper.setDateNode(ruleDetailNode.ruleDetail);
                    }
                }
            } else if(rule.Condition_Type__c == 'case'){
                JSONString + 'Case Condition : ';
                for(var ruleDetailNode of ruleDetailJSON){
                    if(ruleDetailNode.ruleDetail.Condition_Type__c == 'Case then'){
                        JSONString += 'When - '+ helper.setCaseNode(ruleDetailNode,helper,true) + ' ';
                    } else {
                        JSONString += 'Otherwise - '+ helper.setCaseNode(ruleDetailNode,helper,false) + ' ';
                    }
                }
            } 
        }else{
            for(var ruleDetailNode of ruleDetailJSON){
                if(ruleDetailNode.ruleDetail.Condition_Timeframe__c){
                    JSONString += helper.setEarliestLatestNode(ruleDetailNode,helper);
                } else{
                    JSONString += 'Date Calculation : ' + helper.setDateNode(ruleDetailNode.ruleDetail);
                }
            }
        }
        console.log(':::::'+JSONString);
        return JSONString;
    },
    setConditionNode : function(conditionField,operandDetails,operator,criteria){
        return conditionField +' on '+operandDetails + ' '+operator +' '+ criteria ;
    },
    setCaseNode : function(node,helper,thenCase){
        var thisString = '';
        if(thenCase){
        	thisString += helper.setConditionNode(node.ruleDetail.Condition_Field__c,node.ruleDetail.Condition_Operand_Details__c,node.ruleDetail.Condition_Set_Operator__c,node.ruleDetail.Condition_Criteria__c) + ' Then :';
        }
        for(var detailNode of node.nodeCalcs){
            if(detailNode.ruleDetail.Condition_Timeframe__c){
                thisString += helper.setEarliestLatestNode(detailNode,helper);
            } else{
                thisString += helper.setDateNode(detailNode.ruleDetail);
            }
        }   
        return thisString;
    },
    setDateNode : function(node){
        var thisString =  node.Condition_Met_Months__c + ' Months '+ node.Condition_Met_Days__c + ' Days of '+ node.Condition_Operand_Details__c;
        if(node.Condition_Date_Duration__c){
             thisString = thisString +' in ' + node.Condition_Date_Duration__c +' '+ node.Condition_Criteria_Amount__c + ' ';
            if(node.Day_of_the_Week__c){
                thisString = thisString + ' of ' +node.Day_of_the_Week__c + ' ';
            }
        }
        
        return thisString + '; ';
    },
    setEarliestLatestNode : function(node,helper){
        var thisString = '';
        if(node.ruleDetail.Condition_Timeframe__c){
            if(node.ruleDetail.Condition_Timeframe__c == 'True_Earliest' || node.ruleDetail.Condition_Timeframe__c == 'False Earliest'){
                thisString = 'Earliest Of {';
            } else{
                thisString = 'Latest Of {';
            }
            if(node.dateCalcs.length){
                for(var dateCalc of node.dateCalcs){
                    thisString += helper.setDateNode(dateCalc);
                }
            }
            if(node.nodeCalcs.length){
                for(var detailNode of node.nodeCalcs){
                    thisString += helper.setEarliestLatestNode(detailNode,helper);
                }
            }
        } else if(node.ruleDetail.Parent_Condition_Type__c){
            if(node.ruleDetail.Parent_Condition_Type__c == 'Condition'){
                thisString = 'Condition : '+ helper.setConditionNode(node.ruleDetail.Condition_Field__c,node.ruleDetail.Condition_Operand_Details__c,node.ruleDetail.Condition_Set_Operator__c,node.ruleDetail.Condition_Criteria__c);
                thisString += ' ;';
                for(var ruleDetailNode of node.nodeCalcs){
                    if(ruleDetailNode.ruleDetail.Condition_Type__c == 'If'){
                        thisString += 'If TRUE then, ';
                    } else{
                        thisString += 'If FALSE then, ';
                    }
                    if(ruleDetailNode.ruleDetail.Condition_Timeframe__c){
                        thisString += helper.setEarliestLatestNode(ruleDetailNode,helper);
                    } else{
                        thisString += helper.setDateNode(ruleDetailNode.ruleDetail);
                    }
                }
            } else if(node.ruleDetail.Parent_Condition_Type__c == 'Case'){
                thisString + 'Case Condition : ';
                for(var ruleDetailNode of node.nodeCalcs){
                    if(ruleDetailNode.ruleDetail.Condition_Type__c == 'Case then'){
                        thisString += 'When - '+ helper.setCaseNode(ruleDetailNode,helper,true) + ' ';
                    } else {
                        thisString += 'Otherwise - '+ helper.setCaseNode(ruleDetailNode,helper,false) + ' ';
                    }
                     thisString += ' ;';
                }
            }
        } 
        return thisString + '}';
    },
    checkForNestedNodes : function(cmp,event,helper,node){
        var notValid = false;
        if(node.ruleDetail && node.ruleDetail.Parent_Condition_Type__c){
            if(node.ruleDetail.Parent_Condition_Type__c == 'Condition'){
                if($A.util.isEmpty(node.ruleDetail.Condition_Field__c) || ($A.util.isEmpty(node.ruleDetail.Conditional_Operand_Id__c) && node.ruleDetail.Condition_Field__c != 'Title Tag') || $A.util.isEmpty(node.ruleDetail.Condition_Criteria__c) || $A.util.isEmpty(node.ruleDetail.Condition_Set_Operator__c)){
                    console.log('errorOnparentRuleDetail:::'+node.ruleDetail);
                    notValid = true;
                } else{
                    if(helper.validNodes(cmp,event,helper,false,node.dateCalcs,node.nodeCalcs)){
                        console.log('errorOnthisNode:::'+JSON.stringify(node));
                        notValid = true;
                    }
                }
            } else if(node.ruleDetail.Parent_Condition_Type__c == 'Case'){
                if(node.nodeCalcs.length){
                    for(var i of node.nodeCalcs){
                        if(helper.validForCase(cmp,event,helper,i,false)){
                            notValid = true;
                            break;
                        }
                    }
                }
            }
        } else{
            notValid = true;
        }
        return notValid;
    }
})