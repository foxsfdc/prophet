({
	helperMethod : function(cmp, event) {
        event.stopPropagation(); 
        var optionsList = cmp.find("optionList");
        $A.util.addClass(optionsList, 'slds-hide');
    },
    checkForNullValues: function(cmp,event,helper){
        
        if(cmp.get('v.selectedvalue').length == 0){
            $A.util.addClass(cmp.find('multiSelectInput'),'slds-has-error');
            return true;
        } else{
            helper.clearError(cmp,event,helper);
            return false;
        }
    },
    clearError : function(cmp,event,helper){
        if($A.util.hasClass(cmp.find('multiSelectInput'),'slds-has-error')){
            $A.util.removeClass(cmp.find('multiSelectInput'),'slds-has-error');
        }
    },
    setSelectedValueString : function(component,event,helper,selectedValues){
        
        var selectedValueString = '';
        
        if(selectedValues.length){
            for(var i=0;i<selectedValues.length;i++){
                if(selectedValues[i].value){
                     selectedValueString += selectedValues[i].value + ';';
                }
            }
        }
       // console.log('::',selectedValueString);
        selectedValueString = selectedValueString.slice(0,-1);
        component.set('v.value',selectedValueString);
    },
    setSelectedValue : function(component,event,helper,selectedValueString) {
        
        var selectedList = selectedValueString.split(";");
        var selectedValueList = [];
        //console.log(selectedList);
        for(var i=0;i<selectedList.length;i++){
            if(selectedList[i]) {
                var temp = {
                    'label' : selectedList[i],
                    'value' : selectedList[i]
                };
                selectedValueList.push(temp);
            }
        }
        component.set('v.selectedvalue',selectedValueList);
    },
    sortByInputValue : function(cmp,event,helper){
        var sortValue = cmp.find('multiSelectInput').getElement().value;
        var options = JSON.parse(JSON.stringify(cmp.get('v.options')));
        var sortOptions = [];
        if(cmp.find('multiSelectInput').getElement().value){
            for(var i of options){ 
                if(i.label && i.label.label && ((i.label.label).toLowerCase()).startsWith(sortValue.toLowerCase())){
                    sortOptions.push(i);
                }
            }
            cmp.set('v.sortOptions',sortOptions);
        } else{ 
            cmp.set('v.sortOptions',options);
        }
        var optionsList = cmp.find("optionList");
        $A.util.removeClass(optionsList, 'slds-hide');
    }
})