({	
    doInit : function(component, event, helper) {
        //console.log('init',component.get('v.value'));
        if(component.get('v.value')){
           // console.log('here',component.get('v.value'));
            helper.setSelectedValue(component,event,helper,component.get('v.value'));
        } else{            
        	if(!component.get('v.selectedvalue')){
                
        		component.set('v.selectedvalue',[]);
        	}
        }
        var optionsList = component.get("v.optionsList") || [],
            selectedList = JSON.parse(JSON.stringify(component.get("v.selectedvalue"))) || [],
        	options = [];
        var selectedValues = [];
        
        if(selectedList.length){
            for(var i=0;i<selectedList.length;i++){
                selectedValues.push(selectedList[i].label.toLowerCase());
            }
        }
        
        var hasValue = false;
       // console.log(':::',selectedValues);
        if( Array.isArray(optionsList)){
            for(var i=0;i<optionsList.length;i++){
                var opt = {};
                
                opt.label = optionsList[i];
                
                if(optionsList[i].label != '' && selectedValues.indexOf(optionsList[i].label.toLowerCase()) != -1){
                    opt.selected = 'true';
                    hasValue = true;
                }
                else{
                     opt.selected = 'false';
                }
                options.push(opt);
            }
        }
        
        component.set("v.options", options);
        component.set("v.sortOptions", JSON.parse(JSON.stringify(options)));
        if(!hasValue){
            component.set('v.selectedvalue',[]);
        }
        if(selectedList.length){
            helper.setSelectedValueString(component,event,helper,selectedList);
        }
    },
    
	showList : function(component, event, helper) {
        //LRCC:1283 start
        var showListEvent = $A.get("e.c:showListEvent");
        var currentId = component.getLocalId();
        if(component.get('v.hideOtherFieldList')){
           currentId = component.get('v.fieldName'); 
        }
        showListEvent.setParams({
            "selectedList" :  currentId
        });
        showListEvent.fire();
        //LRCC:1283 end
        event.stopPropagation(); 
        var optionsList = component.find("optionList");
        $A.util.toggleClass(optionsList, 'slds-hide');
        helper.clearError(component,event,helper);
    },
    //LRCC:1283 start
    handleShowListEvent : function(component, event, helper) {
        
        var selectedList = event.getParam("selectedList");
       // console.log('handle',selectedList,component.getLocalId());
        if(component.get('v.hideOtherFieldList')){
            if (selectedList != component.get('v.fieldName')) {
                //var optionsList = component.find("optionList");
                // $A.util.addClass(optionsList, 'slds-hide');
                helper.helperMethod(component,event);
            }
        } else {
            if (selectedList != component.getLocalId()) {
                //var optionsList = component.find("optionList");
                // $A.util.addClass(optionsList, 'slds-hide');
                helper.helperMethod(component,event);
            }
        }
    },
    //LRCC:1283 end
    selectOption : function(component, event, helper) {
        
        event.stopPropagation(); 
        var index = event.currentTarget.getAttribute("data-index"),
        	options = component.get("v.sortOptions"),
            originalOptions = component.get('v.options'),
       		selectedvalue = component.get("v.selectedvalue");
        
        if(options[index].selected == 'true'){
            options[index].selected = 'false';
            for(var i in selectedvalue){
               // console.log(selectedvalue[i].label,options[index].label.label);
                if((selectedvalue[i].label).toLowerCase() == (options[index].label.label).toLowerCase()){
                    selectedvalue.splice(i, 1);
                }
            }
        }
        else{
            options[index].selected = 'true';
            if(options[index].label){
                selectedvalue.push(options[index].label);
            }
            component.set('v.noneOption',false);
        }
        for(var i of originalOptions){
           
            if(i.label.value == options[index].label.value){
                console.log(':::',i.label.value+':::::',options[index].label.value);
                i.selected = options[index].selected;
            }
        }
        component.set('v.options',originalOptions);
        component.set("v.sortOptions", options);
        component.set("v.selectedvalue", selectedvalue);
        helper.setSelectedValueString(component,event,helper,selectedvalue);
        if(component.find('multiSelectInput').getElement().value){
            component.find('multiSelectInput').getElement().value = '';
            helper.sortByInputValue(component,event,helper);
            helper.helperMethod(component,event);
        }       
    },
    checkForValue: function(cmp,event,helper){
        return helper.checkForNullValues(cmp,event,helper);
    },
    hideListBox : function(cmp,event,helper){
         helper.helperMethod(cmp,event);
    },
    sortByValue : function(cmp,event,helper){
        console.log('::::::::::',cmp.find('multiSelectInput').getElement().value);
        helper.sortByInputValue(cmp,event,helper);
        
        //console.log('::',sortOptions);
    },
    noneOption : function(cmp,event,helper){
    	cmp.set('v.noneOption',true);
		var options = cmp.get("v.sortOptions"),
	        originalOptions = cmp.get('v.options');
	    for(var i of originalOptions){
	    	i.selected = false;
	    }
	    for(var i of options){
	    	i.selected = false;
	    }
	    cmp.set('v.options',originalOptions);
        cmp.set("v.sortOptions", options);
        cmp.set('v.value','');
        cmp.set('v.selectedvalue',[]);
        cmp.find('multiSelectInput').getElement().value = '';
        helper.sortByInputValue(cmp,event,helper);
    }
})