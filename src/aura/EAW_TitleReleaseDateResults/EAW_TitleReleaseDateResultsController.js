({
    fireComponentEvent : function(component, event, helper) {
        var cmpEvent = component.getEvent("releaseDatesEvent");
        cmpEvent.fire();
	},
    
    //LRCC-1517
    handleFilter : function(component, event, helper) {
        
        if(component.get("v.colWithAlpha").selectedCol){
            helper.showErrorToast(component,'Alphabetical Search Option is Enabled');
            return;
        } 
        
        var buttonstate = component.get('v.buttonstate');
        component.set('v.buttonstate', !buttonstate);
        
        if(! component.get('v.buttonstate')) {
            var pageConfigEvt = component.getEvent("pageConfigEvt");
            pageConfigEvt.setParams({"pagesPerRecord" : component.get("v.pagesPerRecod"),
                                     "currentPageNumber": 1,
                                     "selectedFilterMap" : null});
            pageConfigEvt.fire();
        }
    },
    
    setBodyWidth : function(cmp,event,helper){ // LRCC - 1701
        console.log(':::::::edit Width::')
        window.setTimeout(
            $A.getCallback(function() {
                helper.setBodyWidth(cmp);
            }),2000);
    },
    
    //LRCC-1633 method added
    checkHeaderCheckbox: function(component, event, helper) {
        console.log('checkHeaderCheckbox!!!');
        var checkboxes = component.find("row-checkbox");
        var allselected = true;
        
        if(checkboxes){
            if(Array.isArray(checkboxes)) {
                for(let check of checkboxes){
                    var ele = check.getElement();
                    if(! ele.checked){
                        allselected = false;
                        break;
                    }
                }
            } else {
                var ele = checkboxes.getElement();
                if(! ele.checked){
                    allselected = false;
                }
            }
        }
        console.log('allselected:::',allselected);
        let headerCheckbox = component.find('header-checkbox');
        let headercheck = Array.isArray(headerCheckbox) ? headerCheckbox[0] : headerCheckbox;
        
        if(allselected) {
            headercheck.getElement().checked = true;
        } else {
            headercheck.getElement().checked = false;
        }
    },
    
    init: function(component, event, helper) {
        
        if (component.get('v.isEditable')) {
            component.set('v.isEditable', false);
        }
        setTimeout(function(){component.set('v.showToggle', true)}, 100);
        
        helper.fieldSet(component, event, helper);
        console.log('::: Length :::', component.get("v.resultColumns"));
        console.log(component.get('v.searchCriteriaString'));
        //LRCC-1795
        if(component.find('pagePerRecord')){
            component.set("v.pagesPerRecod",component.find('pagePerRecord').get("v.value"));
        }
        //LRCC-1459
        helper.generateAlphabets(component, event, helper);
        
    },
    //LRCC:1010
    handleNotesAttachmentEvent : function(component, event, helper) {
        component.set('v.title', event.getParam("title"));
        component.set('v.body', event.getParam("body"));
        component.find('notesComponent').notesAttachmentAction(component.get('v.selectedRows'), event.getParam("title"), event.getParam("body"), event.getParam("action"));
   },
    
    addReleaseDate: function(component, event, helper) {
        helper.clearUnsavedChanges(component, event, helper);
        let createRecordEvent = $A.get("e.force:createRecord");
        
        createRecordEvent.setParams({
            "entityApiName": "EAW_Release_Date__c"
        });
        
        createRecordEvent.fire();
    },
    
    showMassUpdate: function(component, event, helper) {
        
        //Tags String to Object
        //upd
        //LRCC:1195
        var resultData = JSON.parse(JSON.stringify(component.get('v.resultData')));      
        var myDataOriginal = JSON.parse(JSON.stringify(component.get('v.myDataOriginal')));
        var myColumns = component.get("v.resultColumns");
        
        console.log('resss:::',resultData);
        console.log('myDataOriginal::',myDataOriginal);
        console.log('myColumns::',myColumns);
        
        var updatedData = new Array();
        // upd
        
        for (var i = 0; i < resultData.length; i++) {
            for (let column of myColumns) {
                var data = JSON.parse(JSON.stringify(resultData[i]));
                var dataOriginal = JSON.parse(JSON.stringify(myDataOriginal[i]));
                
                if (data[column.fieldName] && (typeof (data[column.fieldName]) == 'object') && data[column.fieldName].length != undefined) {
                    console.log('ccccc:::',column.fieldName);
                    var temp = true;
                    for (let dataObj of data[column.fieldName]) {
                        var originalObjIds = [];
                        for (let originalObj of dataOriginal[column.fieldName]) {
                            var originalObjId;
                            if (originalObj.Id) {originalObjId=originalObj.Id;} else {originalObjId=originalObj.id;}
                            originalObjIds.push(originalObjId);
                        }
                        var dataObjId;
                        if (dataObj.id) {dataObjId=dataObj.id} else {dataObjId=dataObj.Id}
                        if ((! originalObjIds.includes(dataObjId))) {
                            console.log('hi');
                            updatedData.push(data);
                            temp = false;
                            break;
                        }
                    }
                    if (temp) {
                        for (let originalObj of dataOriginal[column.fieldName]) {
                            var dataObjIds = [];
                            for (let dataObj of data[column.fieldName]) {
                                var dataObjId;
                                if (dataObj.Id) {dataObjId=dataObj.Id;} else {dataObjId=dataObj.id;}
                                dataObjIds.push(dataObjId);
                            }
                            var originalObjId;
                            if (originalObj.id) {originalObjId=originalObj.id} else {originalObjId=originalObj.Id}
                            if ((! dataObjIds.includes(originalObjId))) {
                                console.log('hello');
                                updatedData.push(data);
                                break;
                            }
                        }
                    }
                } else if((data[column.fieldName] && (typeof (data[column.fieldName]) == 'string')) || (! data[column.fieldName])) {
                    if (data[column.fieldName] != dataOriginal[column.fieldName]) {
                        console.log('welcome');
                        updatedData.push(data);
                        break;
                    }
                }
            }
        } 
        helper.getSelectedRecords(component, event, helper);
        if (component.get('v.selectedRows').length > 0) {
            component.set('v.displayMassUpdate', true);
        } else {
            helper.errorToast($A.get("$Label.c.NoRecordsSelected"));
        }
    },
    
    exportToCsv: function(component, event, helper) {
         //LRCC-1675
        let exportSearchResultEvt = component.getEvent("EAW_ExportSearchResult");
        exportSearchResultEvt.setParams({"resultColumns" : component.get("v.resultColumns"),
                                         "eventFlag":'releasedate'});
        exportSearchResultEvt.fire();
        
        /*let data = component.get('v.myDataOriginal');
        if (data == null) {
            return;
        }
        
        let csv = helper.convertArrayToCSV(component, data, helper);
        
        var hiddenElement = document.createElement('a');
        hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
        hiddenElement.target = '_self'; //
        hiddenElement.download = 'ReleaseDateResults.csv'; // CSV file Name* you can change it.[only name not .csv]
        document.body.appendChild(hiddenElement); // Required for FireFox browser
        hiddenElement.click(); // using click() js function to download csv file*/
    },
    
    // Show confirm box before delete
    deleteReleaseDatePopupShow: function(component, event, helper) {
        if (component.get('v.selectedRows').length > 0) {
            component.set('v.displayDeletePopup', true);
        } else {
            helper.errorToast('Please select at least one Release Date to be deleted.');
        }
    },
    
    editGrid: function(component, event, helper) {
        
        /*component.set('v.showSpinner', true);
        var resultData = component.get('v.resultData');
        if (component.get('v.isEditable')) {
            component.set('v.myDataOriginal', JSON.parse(JSON.stringify(resultData)));
        }
        let isEditable = component.get('v.isEditable');
        let editTableEvent = $A.get('e.c:EAW_EditTableEvent');
        editTableEvent.setParams({
            'editMode': isEditable
        });
        editTableEvent.fire();
        component.set('v.showSpinner', false);
        //component.find('editToggle').set('v.disabled',true);*/
    },
    
    setNewWidth: function(component, event, helper) {
        let currentEle = component.get("v.currentEle");
        
        if (currentEle != null && currentEle.tagName) {
            let parObj = currentEle;
            
            while (parObj.parentNode.tagName != 'TH') {
                if (parObj.className == 'slds-resizable__handle') {
                    currentEle = parObj;
                }
                
                parObj = parObj.parentNode;
            }
            
            let mouseStart = component.get("v.mouseStart");
            let oldWidth = parObj.offsetWidth;
            let newWidth = oldWidth + (event.clientX - parseFloat(mouseStart));
            
            component.set("v.newWidth", newWidth);
            currentEle.style.right = (oldWidth - newWidth) + 'px';
            component.set("v.currentEle", currentEle);
            if(currentEle.id){ // LRCC - 1282
                var myColumns = component.get('v.resultColumns');
                for(var col of myColumns){
                    if(col.fieldName == currentEle.id){
                        console.log(currentEle.id,':::current::',oldWidth - newWidth);
                        col.width = newWidth;
                    }
                }
                component.set('v.resultColumns',myColumns);
                component.set('v.bodyWidth',((component.get('v.resultColumns').length + 1) * 140 ) + newWidth );
            }
        }
    },
    
    calculateWidth: function(component, event, helper) {
        let childObj = event.target;
        let mouseStart = event.clientX;
        
        component.set("v.currentEle", childObj);
        component.set("v.mouseStart", mouseStart);
        
        if (event.stopPropagation) {
            event.stopPropagation();
        }
        
        if (event.preventDefault) {
            event.preventDefault();
        }
        
        event.cancelBubble = true;
        event.returnValue = false;
    },
    
    resetColumn: function(component, event, helper) {
        let currentEle = component.get('v.currentEle');
        
        if (component.get("v.currentEle") !== null) {
            let newWidth = component.get("v.newWidth");
            let currentEleParent = currentEle.parentNode.parentNode;
            let parObj = currentEleParent.parentNode;
            
            parObj.style.width = newWidth + 'px';
            currentEleParent.style.width = newWidth + 'px';
            
            component.get("v.currentEle").style.right = 0;
            component.set("v.currentEle", null);
        }
    },
    
    sort: function(component, event, helper) {
        
        //LRCC-1517
        if(component.get("v.buttonstate")){
            helper.showErrorToast(component,'Filter Search Option is Enabled');
            return;
        } 
        
         let sortVal = event.currentTarget.dataset.value
        //LRCC-1690
        component.set('v.selectedSortingParam',{'sortVal':sortVal});
        console.log('sortparam::',JSON.stringify(component.get('v.selectedSortingParam')));
        helper.sortBy(component, sortVal,false);

    	         //LRCC-1459
		component.set('v.colWithAlpha',{'selectedCol':null,'selectedAlpha':null});	
        var target = event.currentTarget;
        var rowIndex = target.getAttribute("data-colIndex");
        console.log("Row No : " + rowIndex);
        console.log('::: selected :::', JSON.stringify(component.get("v.resultColumns")[rowIndex]));
        
        //if(component.get("v.resultColumns")[rowIndex].type == "string" || 
          // component.get("v.resultColumns")[rowIndex].type == "picklist"){
       if((component.get("v.resultColumns")[rowIndex].type == "string" || 
       component.get("v.resultColumns")[rowIndex].type == "picklist") && 
       ((component.get("v.resultColumns")[rowIndex].fieldName.indexOf("__c") > -1)||
       component.get("v.resultColumns")[rowIndex].fieldName == "Name")){
            
            var colAplpha = component.get('v.colWithAlpha');
            colAplpha.selectedCol = component.get("v.resultColumns")[rowIndex].fieldName;
            component.set('v.colWithAlpha',colAplpha)
        }else{
             helper.showErrorToast(component,'Selecting column is not valid for alphabet sort');
             component.set('v.colWithAlpha',{'selectedCol':null,'selectedAlpha':null});	
             return;
        }
         console.log('::: selected :::',JSON.stringify(component.get('v.colWithAlpha')));
        console.log('::: selected :::',JSON.stringify( component.get("v.resultColumns")));
        
         //end-LRCC-1459

         if(component.get("v.isEditable")){
            helper.showErrorToast(component,'Save Or cancel Unsaved changes before navigating across the page');
            return;
        }
    },
    
    checkAll: function(component, event, helper) {
        console.log('checkAll!!!');
        var self = event.target;
        var checkboxes = component.find("row-checkbox");
        
        var selectedIds = [];
        
        if (checkboxes) {
            if (Array.isArray(checkboxes)) {
                for (var check of checkboxes) {
                    check.getElement().checked = self.checked;
                    if (self.checked) selectedIds.push(check.getElement().getAttribute('id'));
                }
            } else {
                checkboxes.getElement().checked = self.checked;
                if (self.checked) selectedIds.push(checkboxes.getElement().getAttribute('id'));
            }
            if (self.checked) {
                component.set("v.selectedRows", selectedIds);
            } else {
                component.set("v.selectedRows", []);
            }
        }
    },
    
    rowAction: function(component, event, helper) {
        var recId = event.getSource().get('v.name');
        var notesEvent = $A.get("e.c:EAW_NotesEvent");
        notesEvent.setParams({
            "parentId": recId
        });
        notesEvent.fire();
    },
    
    deleteReleaseDateRecords: function(component, event, helper) {
        //LRCC-1573
        component.set('v.showSpinner', true);
        component.find("deletemodalCmp").close();
        helper.getSelectedRecords(component, event, helper);
        
        var selectedRecordIds = component.get("v.selectedRows");
        
        //console.log('::: Selected Records :::',selectedRecordIds);
        //console.log('::: Selected Records Length :::',selectedRecordIds.length);
        
        if (selectedRecordIds.length > 0) {
            var deleteAction = component.get("c.deleteReleaseDates");
            deleteAction.setParam("selectedIds", selectedRecordIds);
            deleteAction.setCallback(this, function(response) {
                var actionState = response.getState();
                console.log('::: actionState :::', actionState);
                if (actionState == 'SUCCESS') {
                    var resultData = component.get("v.resultData");
                    var tempResultData = new Array();
                    for (var data of resultData) {
                        if (selectedRecordIds.indexOf(data.Id) == -1) {
                            tempResultData.push(data);
                        }
                    }
                    
                    //LRCC-1459
                    component.set("v.resultData", tempResultData);
                    //helper.renderPage(component);
                    let pagesPerRecord = parseInt(component.find('pagePerRecord').get("v.value"));
                    component.set("v.maxPage", Math.floor((component.get('v.totalRecordCount')+pagesPerRecord - 1 )/pagesPerRecord)); 
                  
                    helper.editGrid(component, event, helper); 
                    helper.makeUnsavedChanges(component, event, helper);
                    helper.successToast($A.get("$Label.c.RecordsDeletedSuccessfully"));
                    helper.reinitializePageMap(component,component.get("v.pageNumber"));
  					component.set('v.pageNumber', 1);
                    var pageConfigEvt = component.getEvent("pageConfigEvt");
                    pageConfigEvt.setParams({"pagesPerRecord" : component.get("v.pagesPerRecod"),
                                             "currentPageNumber":component.get("v.pageNumber"),
                                             "isDeleted":true});
                    pageConfigEvt.fire();
                    component.set('v.isDelete', true);
                    //console.log('::: Release Date Results  - After Delete :::');
                    //console.log('::: Records :::'+component.get("v.resultData"));
                    //console.log('::: Length :::'+component.get("v.resultData").length);
                    
                } else if (response.getState() === 'ERROR') {
                    console.log(JSON.stringify(response.getError()));
                }
            });
            $A.enqueueAction(deleteAction);
            
        } else {
            helper.errorToast($A.get("$Label.c.NoRecordsSelected"));
        }
    },
    
    saveReleaseDates: function(component, event, helper) {
    	//LRCC-1459
        component.set('v.resultData',component.get('v.rowsToDisplay'));
        var resultData = JSON.parse(JSON.stringify(component.get('v.resultData')));
        var myDataOriginal = JSON.parse(JSON.stringify(component.get('v.myDataOriginal')));
        var myColumns = component.get("v.resultColumns");
        console.log('myColumns:::',myColumns);
        console.log('dataOriginal::::::',myDataOriginal);
        console.log('resultData::::::',resultData);
        var updatedData = new Array();
        //upd		
        for (var i = 0; i < resultData.length; i++) {
            for (let column of myColumns) {
                var data = JSON.parse(JSON.stringify(resultData[i]));
                var dataOriginal = JSON.parse(JSON.stringify(myDataOriginal[i]));
                //console.log('dataOriginal::::::',dataOriginal);
                //console.log('data::::::',data);
                // console.log('data[column.fieldName]::::::',data[column.fieldName]);
                 // console.log('typeof (data[column.fieldName]) == ::::::',typeof (data[column.fieldName]) == 'object');
                 
                    console.log('all ::::::',data[column.fieldName] && (typeof (data[column.fieldName]) == 'object') && data[column.fieldName].length != undefined);
                if (data[column.fieldName] && (typeof (data[column.fieldName]) == 'object') && data[column.fieldName].length != undefined) {
                    console.log('ccccc:::',column.fieldName);
                    var temp = true;
                    for (let dataObj of data[column.fieldName]) {
                        var originalObjIds = [];
                        for (let originalObj of dataOriginal[column.fieldName]) {
                            var originalObjId;
                            if (originalObj.Id) {originalObjId=originalObj.Id;} else {originalObjId=originalObj.id;}
                            originalObjIds.push(originalObjId);
                        }
                        var dataObjId;
                        if (dataObj.id) {dataObjId=dataObj.id} else {dataObjId=dataObj.Id}
                        if ((! originalObjIds.includes(dataObjId))) {
                            console.log('hi');
                            updatedData.push(data);
                            temp = false;
                            break;
                        }
                    }
                    if (temp) {
                        for (let originalObj of dataOriginal[column.fieldName]) {
                            var dataObjIds = [];
                            for (let dataObj of data[column.fieldName]) {
                                var dataObjId;
                                if (dataObj.Id) {dataObjId=dataObj.Id;} else {dataObjId=dataObj.id;}
                                dataObjIds.push(dataObjId);
                            }
                            var originalObjId;
                            if (originalObj.id) {originalObjId=originalObj.id} else {originalObjId=originalObj.Id}
                            if ((! dataObjIds.includes(originalObjId))) {
                                console.log('hello');
                                updatedData.push(data);
                                break;
                            }
                        }
                    }
                } else if((data[column.fieldName] && (typeof (data[column.fieldName]) == 'string')) || (! data[column.fieldName])) {
                console.log('column:::else:::',column.fieldName);
                	console.log('testing:::else:::',data[column.fieldName]);
                	console.log('dataOriginal[column.fieldName]:::else:::',dataOriginal[column.fieldName]);
                	console.log('data[column.fieldName] != dataOriginal[column.fieldName]:::else:::',data[column.fieldName] != dataOriginal[column.fieldName]);
                    if (data[column.fieldName] != dataOriginal[column.fieldName]) {
                        console.log('welcome');
                        updatedData.push(data);
                        break;
                    }
                }
            }
        }
        
        //LRCC-1528 && LRCC-1447
        
        myDataOriginal.forEach(function(originalitem){
            updatedData.forEach(function(item){
                if(item.Id == originalitem.Id && originalitem.Status__c == 'Firm' &&
                   originalitem.Manual_Date__c != item.Manual_Date__c) {
                    
                    component.set('v.firmModalOpen', true);
                                                      
                    if(item.Manual_Date__c) {
                        item.Release_Date__c = item.Manual_Date__c;
                    } else if(! item.Feed_Date__c && item.Status__c != 'Not Released') {  
                        item.Status__c = 'Estimated';
                        item.Temp_Perm__c = '';
                        item.Release_Date__c = item.Projected_Date__c	;
                    } else {
                        item.Release_Date__c = item.Feed_Date__c;
                    }     
                }
            })                                      
        });
        
        if(component.get('v.firmModalOpen')) {
        	component.set('v.firmModalData', updatedData);
            component.find("modalCmpforfirm").open(); 
        }     
        
        console.log('updatedData:::::::testing',updatedData);
        console.log('recordsMap:::::::testing',component.get("v.recordMap"));
        //LRCC-1010 APPEND,REPLACE & CLEAR radio button for notes 
        var notes = component.get('v.massUpdateNotesParent');
        //LRCC-1528
        if(!component.get('v.firmModalOpen')) {
            console.log('::enter::cc:');
            if (updatedData && updatedData.length > 0) {
                 //LRCC-1018
                for(var i = 0;i<updatedData.length;i++){
                   delete updatedData[i].indication;
                }
                console.log(':::::::updatedData:;;;;;',updatedData);
                component.set('v.showSpinner', true);
                var action = component.get('c.saveReleaseDateRecords');
                action.setParams({
                    "releaseDateRecords": updatedData,
                    "recordsMap" : JSON.stringify(component.get("v.recordMap"))
                });
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state == 'SUCCESS') {
                        var results = response.getReturnValue();
                        console.log('::results::cc:',results);
                        //LRCC:1010
                        if (component.get('v.title')) {
                            console.log('success....');
                            //LRCC-1732
                            helper.insertNotes(component,false,helper);
                        }
                        helper.successToast('The Release Date records are updated successfully.');
                        //component.set('v.isEditable', false); //LRCC-1668
                        helper.handleCheckBoxValues(component); //LRCC:1199
                        
                        //LRCC-1447
                        resultData.forEach(function(item){
                            if(item.Status__c == 'Not Released') {
                                item.Release_Date__c = null;
                            }
                        });
                        
                        //Tag string to object
                        //upd
                        //LRCC-1459
                        component.set('v.myDataOriginal', JSON.parse(JSON.stringify(resultData)));
                        //upd
                        component.set('v.resultData', resultData);
                        component.set('v.rowsToDisplay', resultData);
                        //LRCC-1668
                        var editTableEvent = $A.get("e.c:EAW_EditTableEvent");
                        editTableEvent.setParams({
                            "editMode": true
                        });
                        editTableEvent.fire();
                        
                        component.set('v.checkSaveRecords', true);
                        helper.clearUnsavedChanges(component, event, helper);
                        //component.find('editToggle').set('v.disabled',false);
                    } else if(state === 'ERROR') {
                       
                            //LRCC-1539
                            let errors = response.getError();
                            let handleErrors = helper.handleErrors(errors, component, event, helper);
                            if(handleErrors) {
                                helper.errorToastModalPopup(component,handleErrors);
                            }
                        
                    }
                    component.set('v.showSpinner', false);
                });
                $A.enqueueAction(action);
            } else {
                //LRCC:1010//LRCC-1732
                if (component.get('v.title')||component.get('v.body')) {
                    
                    console.log('success....');
                    helper.insertNotes(component,true,helper);
                }else{
                    
                    //LRCC-892
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title: "Info!",
                        message: $A.get("$Label.c.EAW_RecordsNotEdited"),
                        type: "info"
                    });
                    toastEvent.fire();
                }
            }
        }
    },
    
    cancel: function(component, event, helper) {
        var myDataOriginal = component.get('v.myDataOriginal');
        //Tags string to object changes
        component.set('v.resultData', JSON.parse(JSON.stringify(myDataOriginal)));
        //LRCC-1010 & LRCC-1459
        component.set('v.rowsToDisplay', JSON.parse(JSON.stringify(myDataOriginal)));  
        component.set('v.isEditable', false);
        helper.handleCheckBoxValues(component); //LRCC:1199
        var editTableEvent = $A.get("e.c:EAW_EditTableEvent");
        editTableEvent.setParams({
            "editMode": false
        });
        editTableEvent.fire();
        helper.clearUnsavedChanges(component, event, helper);
        //component.find('editToggle').set('v.disabled',false);
    },
    setLookupFields: function(component, event, helper) {
        console.log('setLookupFields!!!');
        //LRCC-1633
        component.set('v.showSpinner', true);
		if(component.find('header-checkbox') && component.find('header-checkbox').getElement() && component.find('header-checkbox').getElement().checked == true) {
			component.find('header-checkbox').getElement().checked = false;
		}
        var params = event.getParam('arguments');
        var param1 = params.childrefresh;
        console.log('param1:::tresult: result:',param1);
        //component.set('v.paginationRefresh', param1);
        component.set("v.pageNumber", param1);
        
        var resultData = helper.getLookupFields(component.get('v.resultColumns'), component.get('v.resultData'), 'Name');
        component.set('v.resultData', resultData);
        component.set('v.myDataOriginal', resultData);
         //LRCC-1459
        helper.renderPage(component);
       //LRCC-1690
        let pagesPerRecord = component.find('pagePerRecord') ? parseInt(component.find('pagePerRecord').get("v.value")) : 10;
        component.set("v.maxPage", Math.floor((component.get('v.totalRecordCount')+pagesPerRecord - 1 )/pagesPerRecord));        
        component.set('v.myDataOriginal', JSON.parse(JSON.stringify(component.get('v.resultData'))));
        
      
        //LRCC-1646
        if (component.get('v.isEditable') && component.get('v.isDelete')) {
            
            component.set('v.isEditable', true);
            console.log('isEditable:::if::',component.get('v.isEditable'));
        }else{
             
            component.set('v.isEditable', false);
            console.log('isEditable:::else::',component.get('v.isEditable'));
        }
        if(component.find('notesComponent')){
            
        	component.find('notesComponent').set('v.notesAttachement', {});   
        }
        component.set('v.showSpinner', false);
    },
    //LRCC:1019
    checkChangesInDataTable: function(component, event, helper) {
        
        return helper.checkChangesInDataTable(component, event, helper);
    },    
    toggle: function(component, event, helper) {
        
        var checked = component.get("v.isEditable");  
        if(checked == false && component.get("v.checkSaveRecords") == false) {
            
            component.find("modalCmp").open();
        }
        helper.handleCheckBoxValues(component); //LRCC:1199
        helper.editGrid(component, event, helper);
        helper.makeUnsavedChanges(component, event, helper);
        /*
       if (component.get('v.isEditable')) {
           
           component.find('showconfirmmodal').close();
           helper.editGrid(component, event, helper);
       }else {
           
           if(helper.checkChangesInDataTable(component, event, helper)){
               
               component.find('showconfirmmodal').open();
           }else{               
              
           }           
       }*/
   },
    //LRCC-1295
    closeModal: function(component, event, helper) {
        component.find("modalCmp").close();
        component.set('v.isEditable', true);
        var editTableEvent = $A.get("e.c:EAW_EditTableEvent");
        editTableEvent.setParams({"editMode":true});
        editTableEvent.fire();
        
    },
    saveModal : function(component, event, helper) {
        var myDataOriginal = component.get('v.myDataOriginal');
        component.find("modalCmp").close();
        component.set('v.isEditable', false);        
        component.set('v.resultData', JSON.parse(JSON.stringify(myDataOriginal))); 
        //LRCC-1010 & LRCC-1459
        component.set('v.rowsToDisplay', JSON.parse(JSON.stringify(myDataOriginal)));
        
        helper.clearUnsavedChanges(component, event, helper);
    },
     //LRCC-1459
    renderPage : function(component, event, helper) {
        let isAlphaFiltered = component.get("v.isAlphaFiltered");
        let isColFiltered = component.get("v.buttonstate"); //LRCC-1517
        
        let pageConfigEvt = component.getEvent("pageConfigEvt");
  		//LRCC-1795
        if(component.find('pagePerRecord')){
            component.set("v.pagesPerRecod",component.find('pagePerRecord').get("v.value"));
        }
        pageConfigEvt.setParams({"pagesPerRecord" : component.get("v.pagesPerRecod"),
                                 "currentPageNumber":component.get("v.pageNumber"),
                                 "selectedFilterMap" : isAlphaFiltered || isColFiltered ? component.get('v.colWithAlpha') : null //LRCC-1517
                                 });
        pageConfigEvt.fire();
		/*let pageRecordsIdMap = component.get("v.pageRecordsIdMap");
        pageRecordsIdMap[component.get("v.pageNumber")] = component.get("v.rowsToDisplay");
        component.set("v.pageRecordsIdMap",pageRecordsIdMap);
        console.log('pageRecordsIdMap',pageRecordsIdMap);
		helper.renderPage(component);*/
	},
    chooseAlphabet : function(component, event, helper) {
       console.log('init::::',JSON.stringify(component.get('v.colWithAlpha'))); 
       if(component.get("v.isEditable")){
            helper.showErrorToast(component);
            return;
        } 
        
        var target = event.currentTarget;
        var rowIndex = target.getAttribute("data-rowIndex");
       
        if(component.get("v.alphabets")[rowIndex]){
            
            var colAplpha = component.get('v.colWithAlpha');
            colAplpha.selectedAlpha = component.get("v.alphabets")[rowIndex];
            component.set('v.colWithAlpha',colAplpha);
        }
       
        let colWithAlpha = component.get('v.colWithAlpha');
        if( colWithAlpha.selectedCol != null ||   colWithAlpha.selectedAlpha == 'All' ){
            
            if(colWithAlpha.selectedAlpha == 'All'){
                
                colWithAlpha.selectedCol == null;
                component.set('v.colWithAlpha',colWithAlpha);
                component.set("v.isAlphaFiltered",false);
            }
            component.set("v.isAlphaFiltered",true);
            component.set('v.pageNumber', 1);
            component.set("v.pageIdMap",{});
            var pageConfigEvt = component.getEvent("pageConfigEvt");
             console.log('before event call::::',JSON.stringify(component.get('v.colWithAlpha')));  
            pageConfigEvt.setParams({"pagesPerRecord" : component.get("v.pagesPerRecod"),
                                     "currentPageNumber":1,
                                     "selectedFilterMap" : component.get('v.colWithAlpha') });
            pageConfigEvt.fire();

        }
         console.log('after event call::::',JSON.stringify(component.get('v.colWithAlpha')));  
        if(colWithAlpha.selectedAlpha == 'All'){
            
            component.set('v.colWithAlpha',{'selectedCol':null,'selectedAlpha':null});	
        }
        
         console.log('::: selected :xxx::', component.get('v.colWithAlpha'));
    },
     handleChange: function(component, event, helper) {
        
         if(component.get("v.isEditable")){
            helper.showErrorToast(component);
            component.find('pagePerRecord').set("v.value",component.get("v.oldPagesPerRecord"));
            return;
        } 
         
        component.set('v.pageNumber', 1);
        let pagesPerRecord = parseInt(component.find('pagePerRecord').get("v.value"));
         
        component.set("v.pagesPerRecod",component.find('pagePerRecord').get("v.value"));
        component.set("v.oldPagesPerRecord",component.find('pagePerRecord').get("v.value"));
        component.set("v.maxPage", Math.floor((component.get('v.totalRecordCount')+pagesPerRecord - 1 )/pagesPerRecord)); 
         
         var pageConfigEvt = component.getEvent("pageConfigEvt");
            pageConfigEvt.setParams({"pagesPerRecord" : component.get("v.pagesPerRecod"),
                                     "currentPageNumber":component.get("v.pageNumber"),
                                     "selectedFilterMap" : component.get('v.colWithAlpha')}); //LRCC-1517
            pageConfigEvt.fire();
    },   
    //LRCC-1573
    openDeleteReleaseDatePopup:function(component, event, helper) {
       
        helper.getSelectedRecords(component, event, helper);
        var selectedRecordIds = component.get("v.selectedRows");
        if (selectedRecordIds.length > 0) {
            component.find("deletemodalCmp").open();
        } else {
            helper.errorToast($A.get("$Label.c.NoRecordsSelected"));
        }
        
    },
    removeDeleteConfirmation:function(component, event, helper) {
        
       component.find("deletemodalCmp").close();
    },
   
    //LRCC-1528
    saveModalforfirm : function(component, event, helper) {
        
        component.find("modalCmpforfirm").close();
        
        var resultData = JSON.parse(JSON.stringify(component.get('v.resultData')));
        component.set('v.showSpinner', true);
        
        var firmModalData = component.get("v.firmModalData");
        //LRCC-1018
        for(var i = 0;i<firmModalData.length;i++){
            delete firmModalData[i].indication;
        }
        
        var action = component.get('c.saveReleaseDateRecords');
        action.setParams({
            "releaseDateRecords": firmModalData,
            "recordsMap" : JSON.stringify(component.get("v.recordMap"))
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state == 'SUCCESS') {
                var results = response.getReturnValue();
                console.log('results::::',results);
                //LRCC:1010
                if (component.get('v.title')) {
                    console.log('success....');
                    helper.insertNotes(component);
                }
                helper.successToast('The Release Date records are updated successfully.');
                //component.set('v.isEditable', false); //LRCC-1668                
                helper.handleCheckBoxValues(component); //LRCC:1199
                
                //LRCC-1447
                var myDataOriginal = JSON.parse(JSON.stringify(component.get('v.myDataOriginal')));
                
                myDataOriginal.forEach(function(originalitem){
                    resultData.forEach(function(item){
                        if(item.Id == originalitem.Id && originalitem.Status__c == 'Firm' &&
                           originalitem.Manual_Date__c != item.Manual_Date__c) {
                            if(item.Manual_Date__c) {
                                item.Release_Date__c = item.Manual_Date__c;
                            } else if(! item.Feed_Date__c && item.Status__c != 'Not Released') {  
                                item.Status__c = 'Estimated';
                                item.Temp_Perm__c = '';
                                item.Release_Date__c = item.Projected_Date__c	;
                            } else {
                                item.Release_Date__c = item.Feed_Date__c;
                            }
                        }
                    })                                      
                });
                
                resultData.forEach(function(item){
                    if(item.Status__c == 'Not Released') {
                        item.Release_Date__c = null;
                    }
                });
                
                //upd
                //LRCC-1459
                component.set('v.myDataOriginal', JSON.parse(JSON.stringify(resultData)));
                //upd
                component.set('v.resultData', resultData);
                component.set("v.firmModalData", []);
                component.set('v.firmModalOpen', false);
                
                helper.renderPage(component);
                
                console.log('rs:::',resultData);
                component.set('v.checkSaveRecords', true);
                //LRCC-1668
                var editTableEvent = $A.get("e.c:EAW_EditTableEvent");
                editTableEvent.setParams({
                    "editMode": true
                });
                editTableEvent.fire();
                helper.clearUnsavedChanges(component, event, helper);
                //component.find('editToggle').set('v.disabled',false);
            } else if(state === 'ERROR') {
                let errors = response.getError();
                let handleErrors = helper.handleErrors(errors, component, event, helper);
                if(handleErrors) {
                    helper.errorToast(handleErrors);
                }
            }
            component.set('v.showSpinner', false);
        });
        $A.enqueueAction(action);       
    },
    //LRCC-1528
    closeModalforfirm : function(component, event, helper) {
        
        component.find("modalCmpforfirm").close();
        component.set("v.firmModalData", []);
        component.set('v.firmModalOpen', false);
    },
    //LRCC:1260
    openDateGuidelineDialog: function(component, event, helper) {
		 
        console.log('::::: Event :::::'+event.getSource().get("v.name"));
        
         if( component.find("dateGuidelineDialog") ){
        	 
             component.find("dateGuidelineDialog").getDateCalculations(event.getSource().get("v.name"), event.getSource().get("v.value"));
         }
    },
    openAffectedWindowPopup: function(component, event, helper) {
		 
        console.log('::::: Event :::::'+event.getSource().get("v.name"));
        
         if( component.find("affectedWindowsPopup") ){
        	 
             component.find("affectedWindowsPopup").getAffectedWindows(event.getSource().get("v.name"), event.getSource().get("v.value"));
         }
    },
    openHistory : function(cmp,event){
        var recId = event.getSource().get('v.name');
        if(recId && cmp.find('historyModal')){
            cmp.find('historyModal').openModal(recId);
        }
    }
})