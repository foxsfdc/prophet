({
    //LRCC:1010//LRCC-1732
    insertNotes : function(component,flag,helper) {
        var action = component.get('c.insertNoteRecords');
        console.log('selectedIds::::',component.get('v.selectedRows'));
        console.log('notesTitle::::',component.get('v.title'));
        console.log('notesBody::::',component.get('v.body'));
        console.log('notesAction::::',component.get('v.massUpdateNotesParent').applyNoteValue);
        action.setParams({
            "selectedIds" : component.get('v.selectedRows'),
            "notesTitle" : component.get('v.title'),
            "notesBody" : component.get('v.body'),
            "notesAction":component.get('v.massUpdateNotesParent').applyNoteValue,
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state == 'SUCCESS') {
                console.log('notesComponent:::',component.find('notesComponent'));
                component.find('notesComponent').set('v.notesAttachement', {});
                //LRCC-1732
                component.set('v.title','');
                component.set('v.body','');
                if(flag){
                    
                     helper.successToast('The Release Date records are updated successfully.');
                }
                              
            }
        });
        $A.enqueueAction(action);
    },
   //LRCC-1539 
  handleErrors: function(errors, component, event, helper) {
		
        let errorMessage = '';
        if (errors && Array.isArray(errors) && errors.length > 0) {
    		
            errors.forEach(error => {
        	    if (error.message && errorMessage.includes(error.message) == false) {
	                errorMessage += error.message + "\n";
	            }
	            if (error.pageErrors && error.pageErrors.length > 0) {
	                error.pageErrors.forEach(pageError => {
	                    if (errorMessage.includes(error.message) == false)
	                        errorMessage += pageError.message + "\n";
	                });
	            } else if (error.fieldErrors) {
	                let fields = Object.keys(error.fieldErrors);
	                fields.forEach(fieldError => {
	                    if (Array.isArray(fieldError)) {
	                        fieldError.forEach(err => {
	                            if (errorMessage.includes(error.message) == false)
	                                errorMessage += err.message;
	                        });
	                    }
	                });
	            }
	        });
	    } else {
            errorMessage = errors;
        }
	    if (errorMessage.indexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION') !== -1) {
	        
            let errorMessageList = errorMessage.split('FIELD_CUSTOM_VALIDATION_EXCEPTION,');
	        if (errorMessageList.length > 1) {
	            let finalErroList = errorMessageList[1].split(': []');
	            if (finalErroList.length) {
	                errorMessage = finalErroList[0];
	            }
	        }
	    }
	    if (errorMessage.indexOf('REQUIRED_FIELD_MISSING') !== -1) {
	        
            let errorMessageList = errorMessage.split('REQUIRED_FIELD_MISSING,');
            if (errorMessageList.length > 1) {
                let finalErrorList = errorMessageList[1].split(': []');
	            if (finalErrorList.length) {
                	errorMessage = finalErrorList[0];
                }
	        }
	    } else {
            let errMsg = errorMessage.split(",").join("\n");
            errorMessage = errMsg;
        }
		console.log('***** errorMessage-->', errorMessage);
        return errorMessage;
	},
        //LRCC-1539
    errorToastModalPopup : function(component, message) {
        component.find('notifLib').showNotice({
            "variant": "error",
            "header": "Error!",
            "message": message
        });
    },
    errorToast : function(message) {
        var toastEvent = $A.get('e.force:showToast');
	
        toastEvent.setParams({
            'title': 'Error!',
            'type': 'error',
            'message': message
        });
	
        toastEvent.fire();
    },

    successToast : function(message) {
        var toastEvent = $A.get('e.force:showToast');

        toastEvent.setParams({
            'title': 'Success!',
            'type': 'success',
            'message': message
        });

        toastEvent.fire();
    },

    sortBy : function (component, field,nextFlag) {
        //LRCC-1690
        let sortAsc = component.get("v.sortAsc");
        let sortField = component.get("v.sortField");
        let records = component.get("v.resultData");
        
        if(field == 'releaseDateTags'){
            
           //sortField = 'releaseDateTagsSortColumn';
            field ='releaseDateTagsSortColumn';
        }
        if(!nextFlag){
            
             sortAsc = field == sortField ? !sortAsc : true;
        }
        console.log('sortField:::',sortField);
        console.log('records:::',records);
          console.log('field:::',field);
       records.sort(function(a, b) {
            if(sortAsc) {
                if(a[field] == undefined) {
                    return -1;
                } else if(b[field] == undefined) {
                    return 1;
                }
                
                return (new String(a[field]).toUpperCase() > new String(b[field]).toUpperCase()) ? 1 : ((new String(b[field]).toUpperCase() > new String(a[field]).toUpperCase()) ? -1 : 0);
            } else {
                if(a[field] == undefined) {
                    
                    return 1;
                } else if(b[field] == undefined) {
                    return -1;
                }
                
                return (new String(a[field]).toUpperCase() < new String(b[field]).toUpperCase()) ? 1 : ((new String(b[field]).toUpperCase() < new String(a[field]).toUpperCase()) ? -1 : 0);
            }
        });
		
        component.set("v.sortAsc", sortAsc);
        component.set("v.sortField", field);
        component.set("v.resultData", records);
        //LRCC-1690
        component.set("v.rowsToDisplay",  component.get("v.resultData"));
         //LRCC-1459
		//this.renderPage(component);
        /****LRCC:887-Sorting indicator applied logic****/
        if(field == 'releaseDateTagsSortColumn'){
            
            field ='releaseDateTags';
        }
        component.set("v.selectedTabsoft", field);
        if (sortAsc == true) {
            component.set("v.arrowDirection", 'arrowup');
        } else {
            component.set("v.arrowDirection", 'arrowdown');
        }
    },

    convertArrayToCSV : function (component, data,helper) {

        // declare variables
        var csvStringResult, counter, header, keys, columnDivider, lineDivider;

        // check if 'objectRecords' parameter is null, then return from function
        if(data == null || !data.length) {
            return null;
        }

        // store ,[comma] in columnDivider variabel for sparate CSV values and
        // for start next line use '\n' [new line] in lineDivider varaible
        columnDivider = ',';
        lineDivider =  '\n';

        // in the keys variable store fields API Names as a key
        // this labels use in CSV file header
        var columns = component.get('v.resultColumns');
        header = [];
        keys =[];
        for(var i=0;i<columns.length;i++){
        	header.push(columns[i].label);// this array is the 'pretty' column names for the csv
        	keys.push(columns[i].fieldName);// this array is the actual column name
        }
        /*header = ['Fin\'l Title Id', 'Title', 'Int\'l Product Type', 'Title Tag(s)', 'Release Date Type', 'Territory',
                    'Language', 'Customer', 'Release Date', 'Release Date Status', 'Feed Date', 'Feed Date Status',
                    'Manual Date', 'Temp/Perm', 'Projected Date', 'Release Date Tag(s)']; // this array is the 'pretty' column names for the csv
        keys = ['edmTitleFinProdId', 'Name', 'edmTitleIntlProdType', 'titleTags', 'releaseDateTypeName', 'Territory__c',
                    'Language__c', 'customerName', 'Release_Date__c', 'Status__c', 'Feed_Date__c', 'Feed_Date_Status__c',
                   'Manual_Date__c', 'Temp_Perm__c', 'Projected_Date__c', 'releaseDateTags']; // this array is the actual column name
		*/
        csvStringResult = component.get('v.searchCriteriaString') || '';
        csvStringResult += header.join(columnDivider);
        csvStringResult += lineDivider;

        for(let i=0; i < data.length; i++) {
            counter = 0;

            for(var sTempkey in keys) {
            
                var skey = keys[sTempkey];

                // add , [comma] after every String value (except first)
                if(counter > 0){
                    csvStringResult += columnDivider;
                }
                if(columns[sTempkey].type == 'date' && !$A.util.isEmpty(data[i][skey])){
                	var resultData = helper.getFormattedDate(data[i][skey]);
            	} else {
            		 //var resultData = data[i][skey] || '';
            		 //LRCC-1014 - 'Fin'l Title Id' column data is displayed incorrectly
            		 var resultData = (skey=='edmTitleFinProdId')?(data[i][skey])?'=""'+data[i][skey]+'""':'': data[i][skey] || '';
				}
               	csvStringResult += '"'+ resultData +'"';
				counter++;
            }
            csvStringResult += lineDivider;
        }
		//return the CSV format String
        return csvStringResult;
    },
    
    getSelectedRecords : function(component,event,helper){
    	console.log('getSelectedRecords!!!');
    	var checkboxes = component.find("row-checkbox"); 
        
        var selectedIds = new Array();
        
        if( checkboxes ){
        	if( Array.isArray(checkboxes)) {
        		for( var check of checkboxes){ 
	            	if( check.getElement().checked  ) {
	                	selectedIds.push(check.getElement().getAttribute('id'));
                	}
	            }
            } else {
            	if( checkboxes.getElement().checked  ) selectedIds.push(checkboxes.getElement().getAttribute('id'));
            }
        }
        component.set("v.selectedRows",selectedIds);
    },
    
    fieldSet: function (component, event, helper) {
        try {
            component.set('v.showSpinner',true);
            var action = component.get("c.getFields");
	            action.setParams(
	            {
	                "objFieldSetMap": {
	                    "EAW_Release_Date__c" : "Search_Result_Fields"
	                }
	            }
	        );

            action.setCallback(this, function (response) {
                var state = response.getState();

                if (state === 'SUCCESS') 
                {
                    var columns = new Array(); 
                    columns.push({label: 'Fin\'l Title Id', fieldName: 'edmTitleFinProdId', type: ''});
                    //LRCC-1539- remove fieldName: 'TitleName'
                    columns.push({width:'199',label: 'Title', fieldName: 'Name', type: 'SubTitle', typeAttributes : {label: {fieldName: 'titleLink'}, target: '_self', sortable: true}});
                    columns.push({label: 'Int\'l Product Type', fieldName: 'edmTitleIntlProdType', type: ''});
                    columns.push({label: 'Title Tag(s)', fieldName: 'titleTags', type: ''}); //LRCC-1551
                    
                    for(let col of response.getReturnValue())
                    {
                        for(let colName of col){
                            //colName.width = '118'; // LRCC - 1282
                            //LRCC-1236 & LRCC-1474
                             if(colName.fieldName != 'Customers__c'&& colName.fieldName != 'Release_Date__c' 
                               && colName.fieldName != 'Feed_Date__c' && colName.fieldName != 'Feed_Date_Status__c' 
                               && colName.fieldName != 'Projected_Date__c'){
                                 
                                 //LRCC-1828
                                 if(colName.fieldName == 'Is_Confidential_Release_Date__c') {
                                     colName.label = 'Confidential';
                                 }
                                 
                                columns.push(colName); 
                            } else {
                            	colName.updateable = false;
                            	columns.push(colName);
                            	if( colName.fieldName == 'Customers__c' ){
                            		
                            		columns.push({label: 'Release Date Tag(s)', fieldName: 'releaseDateTags', type: 'multiselect', updateable: true});
                            	}
                            }
                        }
                           
                    }
                    
                    //LRCC-1474
                    /*columns.push({label: 'Customer', fieldName: 'Customers__c', type: '',updateable: false});
                    columns.push({label: 'Release Date', fieldName: 'Release_Date__c', type: '',updateable: false});
                    columns.push({label: 'Feed Date', fieldName: 'Feed_Date__c', type: '',updateable: false});
                    columns.push({label: 'Feed Date Status', fieldName: 'Feed_Date_Status__c', type: '',updateable: false});
                    columns.push({label: 'Projected Date', fieldName: 'Projected_Date__c', type: '',updateable: false});*/
                    
                    console.log('Columns:');
                    console.dir(columns);
                    
                    component.set('v.resultColumns', columns);
                    // helper.setBodyWidth(component); // LRCC - 1282
                    var resultData = component.get('v.resultData');
                    component.set("v.resultData", this.getLookupFields(columns, resultData, 'Name'));
                   
                    //Tags string to object changes
                    component.set("v.myDataOriginal", JSON.parse(JSON.stringify(this.getLookupFields(columns, resultData, 'Name'))));
                    helper.clearUnsavedChanges(component, event, helper);
                    //LRCC-1459
                    this.renderPage(component);
                    let pagesPerRecord = component.find('pagePerRecord') ? parseInt(component.find('pagePerRecord').get("v.value")) : 10; // LRCC - 1690
                    component.set("v.maxPage", Math.floor((component.get('v.totalRecordCount')+pagesPerRecord - 1 )/pagesPerRecord));
                    this.formPageIdMap(component, event, helper);
                    setTimeout(function(){component.set('v.showSpinner',false)},1000);
                } 
                else if (state === 'ERROR') {
                    var errors = response.getError();

                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " +
                                errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                    component.set('v.showSpinner',false);
                } else {
                    console.log('Something went wrong, Please check with your admin');
                   component.set('v.showSpinner',false);
                }
                
            });
            $A.enqueueAction(action);
        } catch (e) {
            component.set('v.showSpinner',false);   
            console.log('Exception:' + e);
        }
    },
    getLookupFields: function (col, data, fName) {
        for (var i = 0, clen = col.length; i < clen; i++) {
            if (col[i]['type'] == 'reference') {
                for (var j = 0, rlen = data.length; j < rlen; j++) {
                    var str = (col[i]['fieldName']).replace('__c', '__r');
                    var fValue = data[j][str];
                    if (fValue != undefined) {
                        // console.dir(fValue[fName]);
                        data[j][col[i]['fieldName']] = fValue[fName];
                    }
                }
            }
        }
        console.log('data::',JSON.stringify(data));
       
        return data;
    },
    getFormattedDate : function(dateToFormat){
    	var mydate = new Date(dateToFormat);
		var month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
		"Jul", "Aug", "Sept", "Oct", "Nov", "Dec"][mydate.getMonth()];
		return ' '+mydate.getDate() + '-' + month + '-' +  mydate.getFullYear()+' ';
    },
    //LRCC:1019
    revertChanges :function(component, event, helper) {
    	//Tags string to objects
        var myDataOriginal = JSON.parse(JSON.stringify(component.get('v.myDataOriginal')));
        component.set('v.resultData', myDataOriginal);
        component.set('v.isEditable', false);
        //LRCC-1459
        this.renderPage(component);
        let pagesPerRecord = parseInt(component.find('pagePerRecord').get("v.value"));
        
        component.set("v.maxPage", Math.floor((component.get('v.totalRecordCount')+pagesPerRecord - 1 )/pagesPerRecord));
        
        var editTableEvent = $A.get("e.c:EAW_EditTableEvent");
        editTableEvent.setParams({
            "editMode": false
        });
        editTableEvent.fire();
        
    },
    editGrid: function(component, event, helper) {

        component.set('v.showSpinner', true);
        var resultData = component.get('v.resultData');
        if (component.get('v.isEditable')) {
            //Tags string to object
            component.set('v.myDataOriginal', JSON.parse(JSON.stringify(resultData)));
        }
        let isEditable = component.get('v.isEditable');
        
        console.log('isEditable::editGrid:::',isEditable);
        let editTableEvent = $A.get('e.c:EAW_EditTableEvent');
        editTableEvent.setParams({
            'editMode': isEditable
        });
        editTableEvent.fire();
        component.set('v.showSpinner', false);
       
    }, 
    checkChangesInDataTable: function(component, event, helper) {
        
		//Tags string to object changes
        //upd
        var resultData = JSON.parse(JSON.stringify(component.get('v.resultData')));
        var myDataOriginal = JSON.parse(JSON.stringify(component.get('v.myDataOriginal')));
        var myColumns = JSON.parse(JSON.stringify(component.get("v.resultColumns")));
        console.log(';;;;',component.get('v.resultData'));
                    console.log(';;;;',component.get('v.myDataOriginal'));
        console.log('resss:::',resultData);
        console.log('originall::',myDataOriginal);

        var updatedData = new Array();
		//upd
        for (var i = 0; i < resultData.length; i++) {
            for (let column of myColumns) {
                var data = JSON.parse(JSON.stringify(resultData[i]));
                var dataOriginal = JSON.parse(JSON.stringify(myDataOriginal[i]));
                
                if (data[column.fieldName] && (typeof (data[column.fieldName]) == 'object') && data[column.fieldName].length != undefined) {
                    console.log('ccccc:::',column.fieldName);
                    var temp = true;
                    for (let dataObj of data[column.fieldName]) {
                        var originalObjIds = [];
                        for (let originalObj of dataOriginal[column.fieldName]) {
                            var originalObjId;
                            if (originalObj.Id) {originalObjId=originalObj.Id;} else {originalObjId=originalObj.id;}
                            originalObjIds.push(originalObjId);
                        }
                        var dataObjId;
                        if (dataObj.id) {dataObjId=dataObj.id} else {dataObjId=dataObj.Id}
                        if ((! originalObjIds.includes(dataObjId))) {
                            console.log('hi');
                            updatedData.push(data);
                            temp = false;
                            break;
                        }
                    }
                    if (temp) {
                        for (let originalObj of dataOriginal[column.fieldName]) {
                            var dataObjIds = [];
                            for (let dataObj of data[column.fieldName]) {
                                var dataObjId;
                                if (dataObj.Id) {dataObjId=dataObj.Id;} else {dataObjId=dataObj.id;}
                                dataObjIds.push(dataObjId);
                            }
                            var originalObjId;
                            if (originalObj.id) {originalObjId=originalObj.id} else {originalObjId=originalObj.Id}
                            if ((! dataObjIds.includes(originalObjId))) {
                                console.log('hello');
                                updatedData.push(data);
                                break;
                            }
                        }
                    }
                } else if(data[column.fieldName] && (typeof (data[column.fieldName]) == 'string')) {
                    if (data[column.fieldName] != dataOriginal[column.fieldName]) {
                        console.log('welcome');
                        updatedData.push(data);
                        break;
                    }
                }
            }
        }  
        console.log('updatedData::::',updatedData);
        if (updatedData.length) {
        
           // component.find('showconfirmmodal').open();
            return true;
        }

       // component.find('showconfirmmodal').close();
        return false;
        console.log(":::new:", updatedData.length);
    },
        
    /****LRCC:1199-Edit toggle Action Logic****/
    handleCheckBoxValues: function(component) {
        console.log('handleCheckBoxValues!!!');
        if(component.get('v.isEditable') == false) {
            console.log(component.find('header-checkbox'));
            var headerCheckBox = component.find('header-checkbox');
            if(component.find('header-checkbox') && !Array.isArray(headerCheckBox)){
                headerCheckBox = [headerCheckBox];
            } 
            //LRCC-1633
            if(headerCheckBox) headerCheckBox[0].getElement().checked = false; 
            var checkboxes = component.find("row-checkbox");
            if(component.find('row-checkbox') && !Array.isArray(checkboxes)){
                checkboxes = [checkboxes];
            }
            if(checkboxes) {
                for(let check of checkboxes) {
                    if(check.getElement() != null)
                        check.getElement().checked = false;
                }
            }
            component.set('v.selectedRows',[]);
        }
    },
    makeUnsavedChanges: function(cmp, evt, helper) {
        var unSaved = cmp.getEvent("unsavedEvent");
        unSaved.setParams({
            "isChange": true,
            'cmpName' : 'Release Date results'
        });
        unSaved.fire();
    },
    clearUnsavedChanges: function(cmp, evt, helper) {
        var unSaved = cmp.getEvent("unsavedEvent");
        unSaved.setParams({
            "isChange": false,
            'cmpName' : 'Release Date results'
        });
        unSaved.fire();
    },
    setBodyWidth : function(component){
        console.log('setBodyWidth!!!');
       
       if(component.get('v.resultData') && component.get('v.resultData').length){ // LRCC - 1282
            console.log(':::::::::::::::;',component.getGlobalId());
            const globalId = component.getGlobalId(),
                parentnode = document.getElementById(globalId),
                thElements = parentnode.getElementsByTagName('th'),
                tdElements = parentnode.getElementsByTagName('td');
            var bodyWidth = 0;
            
            for (let i = 0; i < thElements.length; i++) {
               // console.log(thElements[i].offsetWidth+':::::::::::::::'+tdElements[i]);
                if(tdElements[i] && thElements[i])tdElements[i].style.width = thElements[i].offsetWidth + 'px';
                bodyWidth += thElements[i].offsetWidth;
            }
            console.log(':::::::bodyWidth:::::::::::'+bodyWidth);
           //LRCC-1517
           if(component.get('v.buttonstate')) {
               component.set('v.bodyWidth', (bodyWidth / 2) + 15);
           } else {
               component.set('v.bodyWidth',bodyWidth + 15);
           }
            //cmp.set('v.bodyWidth',(cmp.get('v.myColumns').length  ) * 209 );*/
        }  
    },
    //LRCC-1459
    generateAlphabets : function(component, event, helper) {
                
        var alphabets = [];
        for(var i= 65 ; i < 91 ; i++){
            
            alphabets.push(String.fromCharCode(i));
        }
         alphabets.push('All');
        component.set('v.alphabets',alphabets);
    },
        //LRCC-1459
    formPageIdMap : function(component, event, helper) {
        const pageNumber = component.get("v.pageNumber"),
              rowsToDisplay = component.get("v.rowsToDisplay");

        let pageIdMap = component.get("v.pageIdMap") || {};
        
        const recordIds = rowsToDisplay.map((rec) =>  rec.Id);
        
        pageIdMap[pageNumber] = recordIds;
        
        component.set("v.pageIdMap",pageIdMap);
        component.set('v.paginationRefresh', true);
        console.log('pageIdMap:::',pageIdMap);
        //LRCC-1646
        if(component.get('v.isDelete')) {
            
            let isEditable = component.get('v.isEditable');
            console.log('pageIdMap::isEditable:',component.get('v.isEditable'));
            let editTableEvent = $A.get('e.c:EAW_EditTableEvent');
            editTableEvent.setParams({
                'editMode': isEditable
            });
            editTableEvent.fire();
        }
        
        var selctsort = component.get('v.selectedSortingParam');
        console.log('tosort::',selctsort);
        if(selctsort){
            this.sortBy(component, selctsort.sortVal,true);  
        }
        
        
    },
    //LRCC-1459
    reinitializePageMap : function(component,pageNumber) {
        let pageIdMap = component.get("v.pageIdMap");
        
        for(let i = pageNumber ; i <= Object.keys(pageIdMap).length ; i++ ){
            pageIdMap[i] = [];
        } 
	   component.set("v.pageIdMap",pageIdMap);
        
    },
       //LRCC-1459
     renderPage: function(component) {
         console.log('pagesPerRecod::::',component.get("v.pagesPerRecod"));
        
		let records = component.get("v.resultData"),
            pageNumber = component.get("v.pageNumber"),
            pagesPerRecord =  component.get("v.pagesPerRecod"),//parseInt(component.find('pagePerRecord').get("v.value")),
            rowsToDisplay = records;
       
        component.set("v.rowsToDisplay", rowsToDisplay);
         console.log('rowsToDisplay::::',rowsToDisplay);
        this.formPageIdMap(component);
    },
        //LRCC-1459
        showErrorToast : function(component,msg) {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title: "Info!",
                message:msg ,
                type: "info"
            });
            toastEvent.fire();
        },
    
})