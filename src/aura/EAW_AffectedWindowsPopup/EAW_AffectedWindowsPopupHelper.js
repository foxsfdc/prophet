({
	setColumns : function(cmp,event,helper,columns) {
        console.log(':::'+columns);
        var columnList = [];
        columns = ['alias','Start_Date__c','Start_Date_Text__c','End_Date__c','End_Date_Text__c','Outside_Date__c','Tracking_Date__c','windowTag','Right_Status__c','License_Info_Codes__c'];
        /*if(columns.indexOf('alias') == -1){
            columns.splice(1, 0, 'alias'); 
        } else{
            columns.pop();
            columns.splice(1, 0, 'alias'); 
        }
        if(columns.indexOf('Start_Date__c') == -1){
            columns.splice(2, 0, 'Start_Date__c');
        }
        if(columns.indexOf('Start_Date_Text__c') == -1){
            columns.splice(3, 0, 'Start_Date_Text__c');
        }
        if(columns.indexOf('End_Date__c') == -1){
            columns.splice(4, 0, 'End_Date__c');
        }
        if(columns.indexOf('End_Date_Text__c') == -1){
            columns.splice(5, 0, 'End_Date_Text__c');
        }
        if(columns.indexOf('Outside_Date__c') == -1){
            columns.splice(6, 0, 'Outside_Date__c');
        }
        if(columns.indexOf('Tracking_Date__c') == -1){
            columns.splice(7, 0, 'Tracking_Date__c');
        }
        
        columns.splice(8, 0, 'windowTag');
        columns.splice(9, 0, 'Right_Status__c');
        columns.splice(10, 0, 'License_Info_Codes__c');*/
        for(var i of columns){
            var fieldName = i;
            if( i != 'Id' && i != 'EAW_Window_Guideline__c' && i != 'EAW_Window_Guideline__r'){
                //console.log('::i:'+fieldName);
                var fieldLabel = 'Window Name';
                if(i == 'EAW_Window_Guideline__r')fieldName = 'EAW_Window_Guideline__r.Window_Guideline_Alias__c';
                var type = 'text';
                if(i == 'Start_Date__c'){
                    fieldLabel = 'Start Date';
                } else if( i == 'Status__c'){
                    fieldLabel = 'Window Status';
                } else if(i == 'End_Date__c'){
                    fieldLabel = 'End Date';
                }
                else if(i == 'Start_Date_Text__c'){
                    fieldLabel = 'Start Date Text';
                }
                else if(i == 'End_Date_Text__c'){
                    fieldLabel = 'End Date Text';
                }
                else if(i == 'Tracking_Date__c'){
                    fieldLabel = 'Tracking Date';
                }
                else if(i == 'windowTag'){
                    fieldLabel = 'Win Sched Tag(s)';
                } else if(i == 'Right_Status__c'){
                    fieldLabel = 'Right Status';
                } else if(i == 'Outside_Date__c'){
                    fieldLabel = 'Outside Date';
                } else if(i == 'License_Info_Codes__c'){
                    fieldLabel = 'Licence Info Codes';
                } 
                columnList.push({label: fieldLabel, fieldName: fieldName, type: type});
            }
        }
       
        //console.log(':;',columnList)
        cmp.set('v.columns',columnList);
	},
    setResults : function(cmp,event,helper,results,windowTagMap){
        for( var i of results){
            if(i.Start_Date__c){
                i.Start_Date__c = helper.setDate(cmp,event,helper,i.Start_Date__c);
            } else{
                i.Start_Date__c =  '';
            }
            if(i.End_Date__c){
                i.End_Date__c = helper.setDate(cmp,event,helper,i.End_Date__c);
            } else{
                i.End_Date__c = '';
            }
            if(i.Tracking_Date__c){
                i.Tracking_Date__c = helper.setDate(cmp,event,helper,i.Tracking_Date__c);
            } else{
                i.Tracking_Date__c = '';
            }
            if(i.Outside_Date__c){
                i.Outside_Date__c = helper.setDate(cmp,event,helper,i.Outside_Date__c);
            } else{
                i.Outside_Date__c = '';
            }
            if(windowTagMap && windowTagMap[i.Id]){
                i.windowTag = windowTagMap[i.Id];
            } else{
                i.windowTag = '';
            }
        }
        cmp.set('v.windowList',results);
    },
    setDate : function(cmp,event,helper,dateValue) {
		var dateList = dateValue.split('-');
        var date = dateList[2] + ' - ' +  helper.getMonth(dateList[1]) + ' - ' + dateList[0];
        return date;
	},
    getMonth : function(monthValue){
        var month = {
            '01' : 'Jan',
            '02' : 'Feb',
            '03' : 'Mar',
            '04' : 'Apr',
            '05' : 'May',
            '06' : 'Jun',
            '07' : 'Jul',
            '08' : 'Aug',
            '09' : 'Sep',
            '10' : 'Oct',
            '11' : 'Nov',
            '12' : 'Dec'
        };
		return month[monthValue];
    },
    setAlias : function(windows){
        for(var window of windows){
            if(window.EAW_Window_Guideline__r){
                var guideline = window.EAW_Window_Guideline__r;
                if(guideline.Window_Guideline_Alias__c)window.alias = guideline.Window_Guideline_Alias__c;
            }
        }
        return windows;
    }
})