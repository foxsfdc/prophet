({
	init: function (cmp, event, helper) {
		cmp.set('v.spinner', true);
		var action = cmp.get('c.getWindowGuideline');
		action.setParams({
			'recordId': cmp.get('v.recordId')
		});
		action.setCallback(this, function (response) {
			var state = response.getState();
			if (state == 'SUCCESS') {
				var wG = response.getReturnValue();
                console.log('wg::::',wG);
				if (wG.Id) delete wG.Id;
				if (wG.Status__c) wG.Status__c = 'Draft';
                if (wG.Version__c) wG.Version__c = 1;
                cmp.set('v.windowGuideline', wG);
				helper.setFieldValues(cmp, event, helper);
				cmp.set('v.showModal', true);
			} else if (state == 'ERROR') {
				console.log('error::', response.getError().message);
			}
		});
		cmp.set('v.spinner', false);
		$A.enqueueAction(action);
	},
	
    handleSubmit: function (component, event, helper) {
        
		component.set('v.spinner', true);
		event.preventDefault(); //stop form submission
		var eventFields = event.getParam("fields");
		if (!$A.util.isEmpty(eventFields['Name'])) {
            console.log('::dskfa;ds::',component.get('v.windowGuideline'));
            
            eventFields['Name'] = component.get('v.windowGuideline').Name + ' clone';
            
            if(component.get('v.windowGuideline').Clone_WG_Name__c != null) {
            	eventFields['Clone_WG_Name__c'] = component.get('v.windowGuideline').Clone_WG_Name__c + ' clone';
            } else {
                eventFields['Clone_WG_Name__c'] = 'clone';
            }
            
            if(component.get('v.windowGuideline').Window_Guideline_Alias__c != null) {
                eventFields['Window_Guideline_Alias__c'] = component.get('v.windowGuideline').Window_Guideline_Alias__c;
            	eventFields['Name'] = eventFields['Window_Guideline_Alias__c'] + "-" + eventFields['Product_Type__c'] + " " + eventFields['Clone_WG_Name__c'];
            }  
            console.log('::eventFields::',JSON.stringify(eventFields));
            component.find('recordForm').submit(eventFields);
        } 
        else {
        	helper.toast('error','Release Date Guideline Name was required');
        }
		component.set('v.spinner', false);
	},
    
    handleErrors: function (cmp, event, helper) {
        
        var errors = event.getParams();
        console.log("response", JSON.stringify(errors));
    },
    
	handleSuccess: function (cmp, event, helper) {
		
		var windowGuideline = event.getParams().response.id;
		console.log('::::::::', windowGuideline);
		cmp.set('v.spinner', true);
		var action = cmp.get('c.cloneRuleAndRuleDetails');
		action.setParams({
			'oldWG': cmp.get('v.recordId'),
			'newWG': windowGuideline
		});
		action.setCallback(this, function (response) {
			var state = response.getState();
			if (state == 'SUCCESS') {
				console.log('success');
				var message = cmp.get('v.windowGuideline').Name + ' was cloned sucessfully';
				helper.toast(state, message);
			} else {
				helper.toast(state, response.getError()[0].message);
			}
			var navEvt = $A.get("e.force:navigateToSObject");
			navEvt.setParams({
				"recordId": windowGuideline
			});
			navEvt.fire();
			cmp.set('v.spinner', false);
		});
		$A.enqueueAction(action);
	},
	closeModal: function (cmp) {
		cmp.set('v.showModal', false);
		var dismissActionPanel = $A.get("e.force:closeQuickAction");
		dismissActionPanel.fire();
	}
})