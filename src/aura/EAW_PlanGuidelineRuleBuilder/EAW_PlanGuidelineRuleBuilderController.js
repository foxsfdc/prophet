({
    init : function(cmp, event, helper) {
        console.log(cmp.get('v.ruleDetailList')[0]);
        if(cmp.get('v.ruleDetailList')[0] && cmp.get('v.ruleDetailList')[0].Condition_Field__c != ''){
            helper.setConditionField(cmp,event,helper,cmp.get('v.ruleDetailList')[0].Condition_Field__c,cmp.get('v.ruleDetailList'));
            cmp.set('v.oldConditionField',cmp.get('v.ruleDetailList')[0].Condition_Field__c);
        }
    },
    setConditionFieldSet : function(cmp,event,helper){
    
        var selectedValue = event.getSource().get('v.value');
        if(cmp.get('v.conditionNodeMap')[selectedValue]){
            var ruleDetailList = cmp.get('v.ruleDetailList') || [];
            console.log(cmp.get('v.oldConditionField'));
            if(cmp.get('v.oldConditionField')){
                var fieldList = cmp.get('v.conditionNodeMap')[(cmp.get('v.oldConditionField'))].fieldList;
            }
            if(ruleDetailList.length && fieldList && fieldList.length){
                for(var i=0;i<ruleDetailList.length;i++){
                    for(var j=0;j<fieldList.length;j++){
                        if(!$A.util.isEmpty(ruleDetailList[i][fieldList[j]])){
                            if(fieldList[j] == 'Condition_Date__c'){
                                ruleDetailList[i][fieldList[j]] = null;
                            } else{
                                ruleDetailList[i][fieldList[j]] = '';
                            }
                        }
                    }
                }
            }
            cmp.set('v.oldConditionField',selectedValue);
            helper.setConditionField(cmp,event,helper,selectedValue,ruleDetailList || []);
        } else{
            cmp.set('v.conditionSet',[]);
        }
        
    },
    saveConditionSet : function(cmp,event,helper){
        var conditionSet = cmp.find('conditionSetCmp');
        if(!Array.isArray(conditionSet)){
            conditionSet = [conditionSet];
        }
        if(helper.validConditionSet(cmp,event,helper,conditionSet)){
            var ruleDetailList = [];
            for(var i=0;i<conditionSet.length;i++){
                if(conditionSet[i] != undefined){
                    conditionSet[i].get('v.ruleDetail').Condition_Field__c = cmp.get('v.conditionField');
                    if(conditionSet[i].get('v.ruleDetail').field){
                        delete conditionSet[i].get('v.ruleDetail').field;
                    }
                    if(!conditionSet[i].get('v.ruleDetail').sObjectType){
                        conditionSet[i].get('v.ruleDetail').sObjectType = 'EAW_Rule_Detail__c'
                    }
                    if(cmp.get('v.conditionField') == 'US Box Office'){
                        conditionSet[i].get('v.ruleDetail').Currency_Type__c = 'USD';
                    } else{
                        // conditionSet[i].get('v.ruleDetail').Currency_Type__c = '';
                    }
                    ruleDetailList.push(conditionSet[i].get('v.ruleDetail')); 
                    cmp.set('v.ruleDetailList',ruleDetailList);
                }
            }
            cmp.get('v.rule').sObjectType = 'EAW_Rule__c';
            if(cmp.get('v.rule').Rule_Orders__r){
                delete cmp.get('v.rule').Rule_Orders__r;
            }
            if(ruleDetailList.length > 1){
                cmp.get('v.rule').Operator__c = 'AND';
            }
            console.log(ruleDetailList,cmp.get('v.rule'));
            return true;
            //helper.saveConditon(cmp,event,helper,ruleDetailList);
        } else{
            return false;
            //helper.showToast(cmp,event,helper,'warning','Please provide valid values');
        }
    }
})