({
    
    setRuleAndRuleDetail : function(cmp,event,helper,rule,ruleDetailList){
        cmp.set('v.rule',rule);
        var conditionSet = cmp.find('conditionSetCmp');
        if(!Array.isArray(conditionSet)){
            // conditionSet = [conditionSet];
        }
        for(var i=0;i<cmp.find('conditionSetCmp').length;i++){
            if(conditionSet[i].get('v.valid')){
                cmp.find('conditionSetCmp')[i].set('v.ruleDetail', ruleDetailList[i]);
            }
        }
        cmp.set('v.ruleDetailList',ruleDetailList);
    },
    validConditionSet : function(cmp,event,helper,conditionSet){
        var valid = true;
        for(var i=0;i<conditionSet.length;i++){
            if(!cmp.get('v.conditionField') || (conditionSet[i] && !conditionSet[i].validation())){
                valid = false;
            }
        }
        return valid;
    },
    setConditionField : function(cmp,event,helper,selectedValue,ruleDetailList){
        console.log(selectedValue);
        if(cmp.get('v.conditionNodeMap')[selectedValue]){
            var number = (cmp.get('v.conditionNodeMap')[selectedValue]).noOfRuleDetails;
            var fields = cmp.get('v.conditionSet');
            fields.length = 0;
            var ruleDetailsToDelete = cmp.get('v.ruleDetailsToDelete') || [];
            if(number){
                var conditionList = [];
                for(var i=0;i<number;i++){
                    console.log(':::::',ruleDetailList[i])
                    //     if(i < number){
                    var conditionSet = {
                        'fields' : (cmp.get('v.conditionNodeMap')[selectedValue]).fieldList,
                        'ruleDetail' : ruleDetailList[i] || JSON.parse(JSON.stringify(cmp.get('v.ruleDetail'))) 
                    }
                    conditionList.push(conditionSet);
                }
                if(ruleDetailList.length != conditionList.length && ruleDetailList[1] && ruleDetailList[1].Id && ruleDetailsToDelete.indexOf(ruleDetailList[1].Id) == -1){
                    console.log(':::todelere::',ruleDetailList[1])
                    ruleDetailsToDelete.push(ruleDetailList[1].Id);
                }
                cmp.set('v.ruleDetailsToDelete',ruleDetailsToDelete);
                cmp.set('v.conditionField',selectedValue);
                cmp.set('v.annotations',(cmp.get('v.conditionNodeMap')[selectedValue]).annotations);
                console.log('set::',conditionList);
                cmp.set('v.conditionSet',conditionList);
                //cmp.set('v.ruleDetailList',ruleDetailList);
            }
        } else{
            cmp.set('v.conditionSet',[]);
        }
    },
    saveConditon : function(cmp,event,helper,ruleDetailList){
        cmp.set('v.spinner',true);
        var action = cmp.get('c.saveConditionSetFields');
        action.setParams({
            'ruleDetailList' : JSON.stringify(ruleDetailList),
            'rule' : cmp.get('v.rule')
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            console.log(state);
            if(state == 'SUCCESS'){
                console.log('success',response.getReturnValue());
                if(cmp.get('v.rule').Id){
                    var message = 'Rule Details was successfully updated';
                    location.reload();
                } else{
                    var message = 'Rule Details was successfully get Inserted';
                    location.reload();
                }
                helper.showToast(cmp,event,helper,'success',message);
            } else{
                var errorMessage = helper.buildErrorMsg(cmp,event,helper,response.getError());
                console.log(errorMessage);
                helper.showToast(cmp,event,helper,'error',errorMessage);
            }
            cmp.set('v.spinner',false);
        });
        $A.enqueueAction(action);
},
    buildErrorMsg : function(cmp, event, helper, errors){
        var errorMsg = "";
        
        if(errors[0] && errors[0].message){  // To show other type of exceptions
            errorMsg = errors[0].message;
        }else if(errors[0] && errors[0].pageErrors.length) {  // To show DML exceptions
            errorMsg = errors[0].pageErrors[0].message; 
        }else if(errors[0] && errors[0].fieldErrors){  // To show field exceptions
            var fields = Object.keys(errors[0].fieldErrors);
            var field = fields[0];
            errorMsg = errors[0].fieldErrors[field];
            errorMsg = errorMsg[0].message;
        }else if(errors[0] && errors[0].duplicateResults.length){ // To show duplicateResults exceptions
            errorMsg = errors[0].duplicateResults[0].message;
        }
        return errorMsg;
        
    },
        showToast : function(cmp, event, helper, type, message) {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "type": type,
                "message": message
        });
        toastEvent.fire();
    },
})