({
	showUsedByModal : function(component, event, helper) {
		document.getElementById("eaw-used-by-modal").style.display = "block";
	},
    showNotesModal : function(component, event, helper) {
		document.getElementById("eaw-notes-modal").style.display = "block";
	}
})