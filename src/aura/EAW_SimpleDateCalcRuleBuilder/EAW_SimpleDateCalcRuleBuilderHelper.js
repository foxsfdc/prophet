({
    isActiveConditionId : function(cmp,id){
        if(cmp.get('v.releaseWindowDateWrapper')){
            return cmp.get('v.releaseWindowDateWrapper').find(function(element){
                if(element.value == id){
                    return true;
                } else{
                    return false;
                }
            }); 
        }
    },
    setWindowDatesByRulesDate : function(cmp,event,helper){
        console.log(':::::',cmp.get('v.ruleDate') );
        if(cmp.get('v.ruleDate') == 'Start Date'){
            cmp.set('v.windowDates',[{'label': 'Start Date', 'value': 'Start Date'},
                                     {'label': 'End Date', 'value': 'End Date'}]);
        }
        else if(cmp.get('v.ruleDate') == 'End Date'){
            if(!cmp.get('v.previousVersion')){
                cmp.set('v.windowDates',[{'label': 'Start Date', 'value': 'Start Date'}]);
            	if(cmp.get('v.validForAllDates'))cmp.set('v.dateCalc.Window_Guideline_Date__c','Start Date');
            } else if(cmp.get('v.previousVersion')){
                if(cmp.get('v.mediaAndTerritoryValue') === cmp.get('v.previousVersion')){
                    cmp.set('v.windowDates',[{'label': 'Start Date', 'value': 'Start Date'},
                                             {'label': 'End Date', 'value': 'End Date'}]);
                }else{
                    if(cmp.get('v.validForAllDates'))cmp.set('v.dateCalc.Window_Guideline_Date__c','Start Date');
                    cmp.set('v.windowDates',[{'label': 'Start Date', 'value': 'Start Date'}]);
                }
                
            } 
        } else if(cmp.get('v.ruleDate') == 'Outside Date' && (cmp.get('v.validForAllDates') || cmp.get('v.previousVersion'))){
            cmp.set('v.windowDates',[{'label': 'Start Date', 'value': 'Start Date'},
                                     {'label': 'End Date', 'value': 'End Date'}]);
        } else if(cmp.get('v.ruleDate') == 'Tracking Date' && (cmp.get('v.validForAllDates') || cmp.get('v.previousVersion'))){
            cmp.set('v.windowDates',[{'label': 'Start Date', 'value': 'Start Date'},
                                     {'label': 'End Date', 'value': 'End Date'}]);
        }
    },
    setModifiedOperandDateAndStatus : function(cmp,event,helper,operandDateAndStatus){
        if(cmp.get('v.dateCalc').Id && cmp.get('v.dateCalc').Conditional_Operand_Id__c && operandDateAndStatus[cmp.get('v.dateCalc').Conditional_Operand_Id__c]){
            var dateValue = '';
            if(cmp.get('v.windowGuidelineList').find(function(element){
                return element.value == cmp.get('v.dateCalc').Conditional_Operand_Id__c})){
                var window = operandDateAndStatus[cmp.get('v.dateCalc').Conditional_Operand_Id__c].window;
                if(cmp.get('v.dateCalc').Window_Guideline_Date__c == 'Start Date')dateValue = window.Start_Date__c;
                if(cmp.get('v.dateCalc').Window_Guideline_Date__c == 'End Date')dateValue = window.End_Date__c;
                if(cmp.get('v.dateCalc').Window_Guideline_Date__c == 'Outside Date')dateValue = window.Outside_Date__c;
                if(cmp.get('v.dateCalc').Window_Guideline_Date__c == 'Tracking Date')dateValue = window.Tracking_Date__c;
                helper.setDateAndStatus(cmp,dateValue,operandDateAndStatus[cmp.get('v.dateCalc').Conditional_Operand_Id__c].statusValue);
            }
            if(cmp.get('v.releaseDateGuidelineList').find(function(element){
                return element.value == cmp.get('v.dateCalc').Conditional_Operand_Id__c})){
                dateValue = new Date(operandDateAndStatus[cmp.get('v.dateCalc').Conditional_Operand_Id__c].dateValue);
                helper.setDateAndStatus(cmp,dateValue,operandDateAndStatus[cmp.get('v.dateCalc').Conditional_Operand_Id__c].statusValue);
            }
        }
    },
    setDateAndStatus : function(cmp,dateValue,status){
        if(dateValue && cmp.get('v.dateCalc').Condition_Met_Months__c){
            dateValue = new Date(dateValue);
            dateValue = dateValue.setMonth(dateValue.getMonth() + cmp.get('v.dateCalc').Condition_Met_Months__c);
        }
        if(dateValue && cmp.get('v.dateCalc').Condition_Met_Days__c){
            dateValue = new Date(dateValue);
            if(dateValue.getDate()){
                dateValue = dateValue.setDate(dateValue.getDate() + cmp.get('v.dateCalc').Condition_Met_Days__c);
            }
        }
        if(dateValue){
            dateValue = new Date(dateValue);
            dateValue = dateValue.getFullYear() + "-" + (dateValue.getMonth() + 1) + "-" + dateValue.getDate();
        }
        if(status){
            status = '( ' + status + ' )';
        } else {
            status = '';
        }
        var dateStatusString = {
            'dateValue' : dateValue,
            'statusValue' : status
        };
        console.log(':::::::::::::::::',dateStatusString);
        cmp.set('v.modifiedOperandDateAndStatus',dateStatusString);
    }
})