({
	init: function (cmp, event, helper) {
		cmp.set('v.spinner', true);
        //cmp.set('v.showModal', true);
        cmp.find("modalCmp").open();
        helper.setFieldValues(cmp, event, helper);
		cmp.set('v.spinner', false);
	},
	
    handleSubmit: function (component, event, helper) {
        component.set('v.spinner', true);
		event.preventDefault(); //stop form submission
		var eventFields = event.getParam("fields");
        component.find('recordForm').submit(eventFields);
		component.set('v.spinner', false);
	},
    
    handleErrors: function (cmp, event, helper) {
        var errors = event.getParams();
    },
    
	handleSuccess: function (cmp, event, helper) {
		
		var windowGuideline = event.getParams().response.id;
		cmp.set('v.spinner', true);
	    var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": windowGuideline
        });
        navEvt.fire();
        cmp.set('v.spinner', false);
	},
    closeMethod : function(component, event, helper){
       // component.set('v.showModal', false);
         component.find("modalCmp").close();
        component.set('v.showhidecmp', false);
    },
})