({
	fetchNotes : function(component, event, helper) {
        
        let recId = component.get("v.recordId");
        var action = component.get("c.getNotes");
        action.setParams({
            "recId" : recId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            var result = response.getReturnValue();
            if (state === 'SUCCESS') { 
                component.set("v.notes",result.attachmentList);
                helper.fetchfiles(component, event, helper);
            } else if (state === 'ERROR') {
                
                console.log(JSON.stringify(response.getError()));
            }
        });
        $A.enqueueAction(action);
        
    },
     //LRCC-1456
    fetchfiles : function(component, event, helper) {
        	
        let recId = component.get("v.recordId");
        var prenotes = [];
        var action = component.get("c.getContentDocumentLink");
        action.setParams({
            "recId" : recId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            var result = response.getReturnValue().attachmentList;
            if (state === 'SUCCESS') { 
               
                prenotes = component.get("v.notes");
                result.forEach(function(res){
                    prenotes.push(res);
                });
                
                component.set("v.notes",prenotes);
            } else if (state === 'ERROR') {
                
                console.log(JSON.stringify(response.getError()));
            }
        });
        $A.enqueueAction(action);
    }
})