({
    handleUploadFinished: function(component, event, helper) {
        
        // This will contain the List of File uploaded data and status
        var uploadedFiles = event.getParam("files");  
        component.set('v.notes',[]);
        helper.fetchNotes(component, event, helper);
    },
    rowAction: function(component, event, helper) {
        
        var recId = event.currentTarget.dataset.value;
        var notesEvent = $A.get("e.c:EAW_NotesEvent");
        component.find("notesComponent").set("v.body", []);
        component.find("notesComponent").set("v.title", []);
        notesEvent.setParams({
            
            "parentId": recId
        });
        notesEvent.fire();
    },
    displayNotes : function(component, event, helper) {
        
        helper.fetchNotes(component, event, helper);       
        
    },
    gotoRecord : function (component, event, helper) {
        
        var noteindex = event.target.getAttribute("data-Index");
        console.log('Test::::',component.get("v.notes")[noteindex].Id);
        if(noteindex > -1){
            
            var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId": component.get("v.notes")[noteindex].Id,
                "slideDevName": "related"
            });
            navEvt.fire();
        }
    },
    reloadList : function (component, event, helper) {
         console.log("reloadList");
         helper.fetchNotes(component, event, helper);
       
    },
})