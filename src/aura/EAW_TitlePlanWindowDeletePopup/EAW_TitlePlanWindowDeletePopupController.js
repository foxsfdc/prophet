({
    updateWindows : function(component, event, helper) {
        let windowList = component.get('v.windowList') == null ? [] : component.get('v.windowList');
        let planList = component.get('v.planList') == null ? [] : component.get('v.planList');

        let action = component.get("c.deleteTitlePlanWindows");
        action.setParams({
            'windowIds' : windowList,
            'planIds' : planList
        });

        action.setCallback(this, function(response) {
            let state = response.getState();
            let results = response.getReturnValue();

            if (state === 'SUCCESS' && results != null) {
                let displayData = component.get('v.displayData') == null ? [] : component.get('v.displayData');
                let planIds = results.planIds == null ? [] : results.planIds;
                let windowIds = results.windowIds == null ? [] : results.windowIds;

                if(displayData.length > 0) {
                    for(let i = 0; i < displayData.length; i++) {
                        if(planIds.length > 0 && displayData[i] && displayData[i].TitlePlan && planIds.indexOf(displayData[i].TitlePlan.Id) !== -1) {
                            displayData.splice(i, 1);
                            i--;
                        }
                    }
                }

                component.set('v.displayData', displayData);

                if(planIds.length > 0 || windowIds.length > 0) {
                    component.set('v.windowList', []);
                    component.set('v.planList', []);
                }

                let successMsg = (planIds.length === 0 ? '' : planIds.length + ' Plan(s) ')
                    + (windowIds.length === 0 ? '' : windowIds.length + ' Windows(s)')
                    + ' deleted.';

                helper.successToast(successMsg);
                
                //LRCC-1573
                //component.set('v.isEditable', false);
                
            } else {
                helper.errorToast('There was an issue when attempting to delete your Plan(s)');
            }
        });
        $A.enqueueAction(action);
		
        document.getElementById("EAW_TitlePlanWindowDeletePopup").style.display = "none";
    },

    showPopup : function(component, event, helper) {
        document.getElementById("EAW_TitlePlanWindowDeletePopup").style.display = "block";
    },

    hidePopup : function(component, event, helper){
        document.getElementById("EAW_TitlePlanWindowDeletePopup").style.display = "none";
    }
})