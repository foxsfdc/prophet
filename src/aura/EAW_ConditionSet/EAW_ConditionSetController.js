({
    doInit : function(cmp, event, helper) { 
        cmp.set('v.releaseWindowDateWrapper',JSON.parse(JSON.stringify(cmp.get('v.releaseWindowDateWrapperSet'))));
        if(cmp.get('v.fromCmp') == 'Condition'){
            if(!cmp.get('v.insideNested')){
                if($A.util.isEmpty(cmp.get('v.rule').Operator__c)){
                    cmp.get('v.rule').Operator__c = 'IN'; // change for nested
                }
                if(!$A.util.isEmpty(cmp.get('v.rule').Criteria__c)){
                   cmp.set('v.criteria',cmp.get('v.rule').Criteria__c);
                }
                cmp.set('v.operator',cmp.get('v.rule').Operator__c);
            } else{
                if($A.util.isEmpty(cmp.get('v.rule').Condition_Set_Operator__c)){
                    cmp.get('v.rule').Condition_Set_Operator__c = 'IN'; // change for nested
                }
                if(!$A.util.isEmpty(cmp.get('v.rule').Condition_Criteria__c)){
                   cmp.set('v.criteria',cmp.get('v.rule').Condition_Criteria__c);
                }
                cmp.set('v.operator',cmp.get('v.rule').Condition_Set_Operator__c);
            }
            if(!$A.util.isEmpty(cmp.get('v.rule').Condition_Field__c)){
                var selectedValue  = cmp.get('v.rule').Condition_Field__c;
                
                if(selectedValue == 'Title Tag'){
                    cmp.set('v.isTitletag',true);
                } else{
                    cmp.set('v.isTitletag',false);
                }
                cmp.set('v.dependentList',cmp.get('v.conditionBasedDependentMap')[selectedValue]);
                var validList = [];
                if(cmp.get('v.rule').Conditional_Operand_Id__c){
                     validList = helper.isActiveConditionalId(cmp,(cmp.get('v.rule').Conditional_Operand_Id__c).split(';'));
                }
                if(!validList.length){
                    cmp.get('v.rule').Conditional_Operand_Id__c = '';
                    cmp.get('v.rule').Condition_Operand_Details__c = '';
                } else{
                	/*var operandString = '';
                	for(var i of validList){
                		operandString = operandString + i + ';'
                	}*/
                	console.log(validList);
                	cmp.set('v.selectedOperands',validList);
                }
            }  
        } else{
            if($A.util.isEmpty(cmp.get('v.ruleDetail').Condition_Set_Operator__c)){
                cmp.get('v.ruleDetail').Condition_Set_Operator__c = 'IN';    
            }
            
            if(cmp.get('v.ruleDetail') && !$A.util.isEmpty(cmp.get('v.ruleDetail').Condition_Field__c)){
                var selectedValue  = cmp.get('v.ruleDetail').Condition_Field__c;
                
                if(selectedValue == 'Title Tag'){
                    cmp.set('v.isTitletag',true);
                } else{
                    cmp.set('v.isTitletag',false);
                }
                cmp.set('v.dependentList',cmp.get('v.conditionBasedDependentMap')[selectedValue]);
                var validList = [];
                if(cmp.get('v.ruleDetail').Conditional_Operand_Id__c){
                    validList = helper.isActiveConditionalId(cmp,(cmp.get('v.ruleDetail').Conditional_Operand_Id__c).split(';'));
                }
                if(!validList.length){
                    cmp.get('v.ruleDetail').Conditional_Operand_Id__c = '';
                    cmp.get('v.ruleDetail').Condition_Operand_Details__c = '';
                } else{
                	var operandString = '';
                	for(var i of validList){
                		operandString = operandString + i + ';'
                	}
                	cmp.set('v.selectedOperands',validList);
                }              
            } 
        } 
    },
    setDependentList : function(cmp,event,helper){
        var selectedValue = event.getSource().get('v.value');
        console.log(':::selectedValue:::;;',selectedValue);
        console.log(':::::dependentList:::;',cmp.get('v.conditionBasedDependentMap')[selectedValue]);
        cmp.set('v.dependentList',cmp.get('v.conditionBasedDependentMap')[selectedValue]);
        if(selectedValue == 'Title Tag'){
            cmp.set('v.isTitletag',true);
        } else{
            cmp.set('v.isTitletag',false);
        }
        if(cmp.get('v.fromCmp') == 'Condition'){
           
            var rule = cmp.get('v.rule');
            rule.Criteria__c = '';
            if(cmp.get('v.isTitletag')){
                rule.Condition_Operand_Details__c = '';
                rule.Conditional_Operand_Id__c = '';
            }
            cmp.set('v.rule',rule);
        } else{
   
            var ruleDetail = cmp.get('v.ruleDetail');
            ruleDetail.Condition_Criteria__c = '';
            if(cmp.get('v.isTitletag')){
                ruleDetail.Condition_Operand_Details__c = '';
                ruleDetail.Conditional_Operand_Id__c = '';
            }
            cmp.set('v.ruleDetail',ruleDetail);
        }
        console.log('::::::'+cmp.get('v.selectedOperands'));
        if(cmp.get('v.selectedOperands').length){
            cmp.set('v.selectedOperands',[]);
        }
    },
    setConditionalOperand : function(cmp,event){
        var selecetedIds = cmp.get('v.selectedOperands');
        console.log(selecetedIds);
        var detailsString =  '';
    	var selecetedId = '';
        if(!$A.util.isEmpty(selecetedIds)){
        	/*var selectedIdList = [];
        	for(var i of selecetedIds){
        		selectedIdList.push(i.value);
        	}
        	console.log(selectedIdList);
	        var selectedLabel = [];
	        cmp.get('v.releaseWindowDateWrapper').find(function(element){
	            if(selectedIdList.includes(element.value)){
	                selectedLabel.push(element);
	            }
	        }); 
            console.log(':::::::::',selectedLabel);*/
	        if(selecetedIds.length){
	        	
	        	for(var i of selecetedIds){
	        		detailsString = detailsString + i.label + ';';
	        		selecetedId = selecetedId + i.value + ';'
	        	}
	            console.log('detailsString',detailsString);
	            console.log('selecetedId',selecetedId);
	        } 
	        
	    }
        if(cmp.get('v.fromCmp') == 'Condition'){
            cmp.get('v.rule').Condition_Operand_Details__c = detailsString.slice(0, -1);
            cmp.get('v.rule').Conditional_Operand_Id__c = selecetedId.slice(0, -1);
            if(selecetedIds.length && selecetedIds.length > 1){
                cmp.get('v.rule').Multi_conditional_operands__c = true;
            } else{
                cmp.get('v.rule').Multi_conditional_operands__c = false;
            }
        } else{
            cmp.get('v.ruleDetail').Condition_Operand_Details__c = detailsString.slice(0, -1);
            cmp.get('v.ruleDetail').Conditional_Operand_Id__c = selecetedId.slice(0, -1);
            if(selecetedIds.length && selecetedIds.length > 1){
                cmp.get('v.ruleDetail').Multi_conditional_operands__c = true;
            } else{
                cmp.get('v.ruleDetail').Multi_conditional_operands__c = false;
            }
        }
    },
    hideOtherList : function(cmp){
        if(cmp.find('operandDetails')){
            cmp.find('operandDetails').hideList();
        }
        if(cmp.find('ruleCriteria')){
            cmp.find('ruleCriteria').hideList();
        }
        if(cmp.find('ruleDetailCriteria')){
            cmp.find('ruleDetailCriteria').hideList();
        }
    },
    setCriteria : function(cmp){
        var criteria = cmp.get('v.criteria');
        if(cmp.get('v.insideNested')){
            if(criteria){
                cmp.get('v.rule').Condition_Criteria__c = criteria;
            } else{
                cmp.get('v.rule').Condition_Criteria__c = '';
            }
        } else{
            if(criteria){
                cmp.get('v.rule').Criteria__c = criteria;
            } else{
                cmp.get('v.rule').Criteria__c = '';
            }
        }
    },
    setOperator : function(cmp){
        console.log(':::'+cmp.get('v.operator'))
        if(cmp.get('v.insideNested')){
            if(cmp.get('v.operator')){
                cmp.get('v.rule').Condition_Set_Operator__c = cmp.get('v.operator');
            } else{
                cmp.get('v.rule').Condition_Set_Operator__c = '';
            }
        } else{
            if(cmp.get('v.operator')){
                cmp.get('v.rule').Operator__c = cmp.get('v.operator');
            } else{
                cmp.get('v.rule').Operator__c = '';
            }
        }
    }
})