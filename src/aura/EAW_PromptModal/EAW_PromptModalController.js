({
	proceed : function(cmp, event, helper) {
        var cmpEvent = cmp.getEvent("promptEvent");
        cmpEvent.fire();
        $A.util.removeClass(cmp.find('modal'),'slds-fade-in-open');
        $A.util.removeClass(cmp.find('backDrop'),'slds-backdrop_open');
    },
    openPrompt : function(cmp,event,helper){
        $A.util.addClass(cmp.find('modal'),'slds-fade-in-open');
        $A.util.addClass(cmp.find('backDrop'),'slds-backdrop_open');
    },
    closePromot : function(cmp,event,helper){
        $A.util.removeClass(cmp.find('modal'),'slds-fade-in-open');
        $A.util.removeClass(cmp.find('backDrop'),'slds-backdrop_open');
    }
    
})