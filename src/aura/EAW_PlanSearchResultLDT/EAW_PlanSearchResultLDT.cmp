<aura:component controller="EAW_PlanController">
	<aura:attribute name="planData" type="EAW_Plan_Guideline__c[]" />
    <aura:attribute name="myColumns" type="List"/>
    <aura:attribute name="selectedRows" type="List" default="[]" />
    
    <aura:attribute name="cloneOpts" type="List" default="[
    	{'label': 'Clone Window Guidelines', 'value': 'cloneWindowGuidelines'},
    	{'label': 'Use Same Window Guidelines', 'value': 'useSameWindowGuidelines'},
        {'label': 'Clone without Window Guidelines', 'value': 'cloneWithoutWindowGuidelines'}
    	]"/>
    <aura:attribute name="cloneValue" type="String" default="cloneWindowGuidelines"/>
    
    <aura:handler name="init" value="{!this}" action="{!c.init}"/>
    
    <div class="container slds-p-around_medium">
        <br/>
        <div class="slds-grid">
            <div class="slds-col slds-size_3-of-5">
                <div class="slds-text-heading_medium">Search Results</div>
            </div>
            <div class="slds-col slds-size_2-of-5">
                <div class="slds-float_right">
                    <lightning:button variant="neutral" label="Add New Plan Guideline" onclick="{! c.createRecord }"/>
                    <lightning:button variant="neutral" label="Deactivate" onclick="{!c.showDeactivateModal}" />
                    <lightning:button variant="neutral" label="Delete Draft" onclick="{!c.showDeleteModal}"/>
                    <lightning:button variant="neutral" label="Clone" onclick="{!c.showCloneModal}" />
                </div>
            </div>
            
            <!-- Refactor modals to components here -->
        </div>
        
        <br/>
        <c:EAW_FieldSetComponent myData="{!v.planData}" myColumns="{!v.myColumns}" selectedRows="{!v.selectedRows}" />
        
        
        
        <!-- Delete Confirmation popup -->
        <div class="popup" id="eaw-delete-popup" style="height: 640px; display:none">
            <section role="dialog" tabindex="-1" aria-labelledby="modal-heading-01" aria-modal="true" aria-describedby="modal-content-id-1" class="slds-modal slds-fade-in-open">
                <div class="slds-modal__container">
                    
                    <!-- Title for Modal + Close button -->
                    <header class="slds-modal__header">
                        <button class="slds-button slds-button_icon slds-modal__close slds-button_icon-inverse" title="Close" onclick="{!c.hideDeleteModal}" aria-hidden="true">
                            <lightning:icon iconName="utility:close" size="small" />
                            <span class="slds-assistive-text">Close</span>
                        </button>
                        <h2 id="modal-heading-01" class="slds-text-heading_medium slds-hyphenate">Delete Drafts</h2>
                    </header>
                    <div class="slds-modal__content slds-p-around_medium" id="modal-content-id-1">
                        <!-- Modal main content goes here -->
                        <p>Delete all selected plan guideline(s)<br/>(Only plan guidelines with statuses of draft will be deleted)</p>
                    </div>
                    
                    <!-- Footer Buttons -->
                    <footer class="slds-modal__footer">
                        <lightning:button variant="neutral" label="OK" onclick="{!c.deleteDraft}" />
                        <lightning:button variant="neutral" label="Cancel" onclick="{!c.hideDeleteModal}" />
                    </footer>
                </div>
            </section>
            <div class="slds-backdrop slds-backdrop_open"></div>
        </div>
        
        <!-- Clone Popup -->
        <div class="popup" id="eaw-clone-popup" style="height: 640px; display:none">
            <section role="dialog" tabindex="-1" aria-labelledby="modal-heading-01" aria-modal="true" aria-describedby="modal-content-id-1" class="slds-modal slds-fade-in-open">
                <div class="slds-modal__container">
                    <header class="slds-modal__header">
                        <button class="slds-button slds-button_icon slds-modal__close slds-button_icon-inverse" title="Close" onclick="{!c.hideCloneModal}" aria-hidden="true">
                            <lightning:icon iconName="utility:close" size="small" />
                            <span class="slds-assistive-text">Close Plan Guideline(s)</span>
                        </button>
                        <h2 id="modal-heading-01" class="slds-text-heading_medium slds-hyphenate">Clone</h2>
                    </header>
                    
                    <div class="slds-modal__content slds-p-around_medium" id="modal-content-id-1">
                        <fieldset class="slds-form-element">
                            <lightning:radioGroup aura:id="mygroup"
                                                  name="radioButtonGroup"
                                                  label="Decision on if underlying plan guidelines are cloned:"
                                                  options="{! v.cloneOpts }"
                                                  value="{! v.cloneValue }"
                                                  onchange="{! c.handleChange }"
                                                  required="true" />
                        </fieldset>
                    </div>
                    
                    <footer class="slds-modal__footer">
                        <lightning:button variant="neutral" label="OK" onclick="{!c.clone}" />
                        <lightning:button variant="neutral" label="Cancel" onclick="{!c.hideCloneModal}" />
                    </footer>
                </div>
            </section>
            <div class="slds-backdrop slds-backdrop_open"></div>
        </div>
        
        <!-- Deactivate Popup -->
        <div class="popup" id="eaw-deactivate-popup" style="height: 640px; display:none">
            <section role="dialog" tabindex="-1" aria-labelledby="modal-heading-01" aria-modal="true" aria-describedby="modal-content-id-1" class="slds-modal slds-fade-in-open">
                <div class="slds-modal__container">
                    
                    <!-- Title for Modal + Close button -->
                    <header class="slds-modal__header">
                        <button class="slds-button slds-button_icon slds-modal__close slds-button_icon-inverse" title="Close" onclick="{!c.hideDeactivateModal}" aria-hidden="true">
                            <lightning:icon iconName="utility:close" size="small" />
                            <span class="slds-assistive-text">Deactivate Plan Guideline(s)</span>
                        </button>
                        <h2 id="modal-heading-01" class="slds-text-heading_medium slds-hyphenate">Deactivate</h2>
                    </header>
                    <div class="slds-modal__content slds-p-around_medium" id="modal-content-id-1">
                        <!-- Modal main content goes here -->
                        <p>Deactivate all selected Plan Guideline(s)</p>
                    </div>
                    
                    <!-- Footer Buttons -->
                    <footer class="slds-modal__footer">
                        <lightning:button variant="neutral" label="OK" onclick="{!c.deactivate}" />
                        <lightning:button variant="neutral" label="Cancel" onclick="{!c.hideDeactivateModal}" />
                    </footer>
                </div>
            </section>
            <div class="slds-backdrop slds-backdrop_open"></div>
        </div>
    </div>
</aura:component>