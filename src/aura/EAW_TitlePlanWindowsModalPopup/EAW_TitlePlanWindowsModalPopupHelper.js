({
	sortBy : function (component, field, index) {
        
        let sortAsc = component.get("v.sortAsc");
        let sortField = component.get("v.sortField");
        let records = component.get("v.resultData");
      	
        sortAsc = field == sortField ? !sortAsc : true;
        records.sort(function(a, b) {
            if(sortAsc) {
                if(a[field] == undefined) {
                    return -1;
                } else if(b[field] == undefined) {
                    return 1;
                }
				
                return (a[field] > b[field]) ? 1 : ((b[field] > a[field]) ? -1 : 0);
            } else {
                if(a[field] == undefined) {
                    return 1;
                } else if(b[field] == undefined) {
                    return -1;
                }

                return (a[field] < b[field]) ? 1 : ((b[field] < a[field]) ? -1 : 0);
            }
        });
		
        component.set("v.sortAsc", sortAsc);
        component.set("v.sortField", field);
        component.set("v.resultData", records);
        
        /****LRCC:887-Sorting indicator applied logic****/
        component.set("v.selectedTabsoft", field);
        if (sortAsc == true) {
            component.set("v.arrowDirection", 'arrowup');
        } else {
            component.set("v.arrowDirection", 'arrowdown');
        }
    },
    setWindowList : function(cmp,response) {
        
        var plan = response.plan;
        var tag = response.tagList;
        var windowTag = response.windowTagList;
        console.log('::response:',response);
        var FIN_PROD_ID__c,PROD_TYP_CD_INTL__c,FIN_DIV_CD__c,TitleTags,windowGuidelineName,windowTags = '';
        //LRCC-1250 changes for EDM title to EAW title.
        if(plan.EAW_Title__r && plan.EAW_Title__c){
            var titleEDM = plan.EAW_Title__r;
            if(titleEDM.FIN_PROD_ID__c ){
                FIN_PROD_ID__c =titleEDM.FIN_PROD_ID__c;
            }
            if(titleEDM.PROD_TYP_CD_INTL__c){
                PROD_TYP_CD_INTL__c = titleEDM.PROD_TYP_CD_INTL__r.Name 
            } else if(plan.EAW_Title__r.Title_EDM__r && plan.EAW_Title__r.Title_EDM__r.CurrencyIsoCode){
                // PROD_TYP_CD_INTL__c = titleEDM.CurrencyIsoCode;
            }
            if(titleEDM.FIN_DIV_CD__c){
                FIN_DIV_CD__c = titleEDM.FIN_DIV_CD__r.Name;
            } else if(titleEDM.DIR_NM__c){
               // FIN_DIV_CD__c = titleEDM.DIR_NM__c || '';
            }
            if(tag.length){
                TitleTags = '';
                for(var i in tag){
                    TitleTags = TitleTags+tag[i].Tag__r.Name+';' || '';
                }
                //TitleTags = titleEDM.Approx_Run_Time__c;
            }
            if(windowTag.length){
                windowTags = '';
                for(var i of windowTags){
                    windowTags = windowTags + i.Tag__r.Name + ';' || '';
                }
            }
        }
        var windowList = response.windowList;
        var dataList = [];
        for(var i=0;i<windowList.length;i++){
            var window = windowList[i];
            window.FIN_PROD_ID__c = FIN_PROD_ID__c || '';
            window.PROD_TYP_CD_INTL__c = PROD_TYP_CD_INTL__c || '';
            window.FIN_DIV_CD__c = FIN_DIV_CD__c || '';
            window.TitleTags = TitleTags || '';
            window.windowGuidelineName = windowList[i].EAW_Window_Guideline__r.Window_Guideline_Alias__c || '';
            if(windowTag.length){
                var windowTagString = '';
                for(var j of windowTag){
                    if(window.Id == j.Window__c){
                        windowTagString = windowTagString + j.Tag__r.Name + ';';
                    }
                }
                window.windowTags = windowTagString;
            } else {
                 window.windowTags =  '';
            }
           
            dataList.push(window);
        }
        cmp.set('v.resultData',dataList);
    }
})