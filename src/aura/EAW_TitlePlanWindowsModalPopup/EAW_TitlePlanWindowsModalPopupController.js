({
	init : function(cmp, event, helper) {},
    
    openModalWindow : function(cmp,event,helper) {
        
        $A.util.addClass(cmp.find('modal'),'slds-fade-in-open');
        $A.util.addClass(cmp.find('backDrop'),'slds-backdrop_open');
        var fieldString = '';
        for(var i=0;i<cmp.get('v.columns').length;i++){
            console.log(cmp.get('v.columns')[i].fieldName);
            var fieldName = cmp.get('v.columns')[i].fieldName;
            if(fieldName == 'windowGuidelineName'){
                fieldName = 'EAW_Window_Guideline__r.Window_Guideline_Alias__c'; //LRCC-1493.
            }
            if(i != cmp.get('v.columns').length-1){
                if(i == 0){
                    fieldString += 'SELECT ' + fieldName + ', ';
                } else{
                    fieldString += fieldName + ', ';
                }
            } else{
                fieldString += fieldName + ' FROM EAW_Window__c WHERE ';
            }
        }
        
        fieldString  = fieldString.replace('FIN_PROD_ID__c,','');
        fieldString  = fieldString.replace('PROD_TYP_CD_INTL__c,','');
        fieldString  = fieldString.replace('FIN_DIV_CD__c,','');
        fieldString  = fieldString.replace('TitleTags,','');
        fieldString  = fieldString.replace('windowTags','');
        fieldString = fieldString.replace('Customers__c,','Customers__c,');
        fieldString = fieldString.replace('Retired__c,','Retired__c');
        fieldString = fieldString.replace('titleName,','');
        
        console.log(fieldString);
        cmp.set('v.spinner',true);
        if(cmp.get('v.planId')){
            var action = cmp.get('c.getAllWindows');
            action.setParams({
                'planId' : cmp.get('v.planId'),
                'fieldString' : fieldString
            });
            action.setCallback(this,function(response){
                var state = response.getState();
                if(state == 'SUCCESS'){
                    console.log(response.getReturnValue());
                    helper.setWindowList(cmp,response.getReturnValue());
                } else if(state == 'ERROR'){
                    console.log(response.getError());
                }
                cmp.set('v.spinner',false);
            });
            $A.enqueueAction(action);
        }
        
    },
    closeModalWindow : function(cmp,event,helper){
        $A.util.removeClass(cmp.find('modal'),'slds-fade-in-open');
        $A.util.removeClass(cmp.find('backDrop'),'slds-backdrop_open');
    },
    
    sort: function(component, event, helper) {
        let sortVal = event.currentTarget.dataset.value;
        let sortIdx = event.currentTarget.dataset.idx;
        helper.sortBy(component, sortVal, sortIdx);
    },
})