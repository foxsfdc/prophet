public with sharing class EAW_PlanReQualifierController {
    
    public static void checkForPlanReQualifications(List<EAW_Rule_Detail__c> ruleDetailList, String planGuidelineId){
            
        Boolean isTVDBoxOffice = false;
        Boolean isTitleAttribute = false;
        Boolean isTitleTag = false;
        Boolean isReleaseDate = false;
        
        Set<Id> qualifiedEAWTitleIdSet = new Set<Id>();
        
        System.debug('::::::ruleDetailList::::'+ruleDetailList);
        System.debug('::::::planGuidelineId::::'+planGuidelineId);
        
        EAW_PlanQualifierCalculationController qualifierController = new EAW_PlanQualifierCalculationController();  
        
        for(EAW_Rule_Detail__c ruleDetail : ruleDetailList){
            if( ruleDetail.Condition_Field__c == 'US Box Office' || ruleDetail.Condition_Field__c == 'US Admissions' || ruleDetail.Condition_Field__c == 'US Number of Screens' || ruleDetail.Condition_Field__c == 'Local Box Office' || ruleDetail.Condition_Field__c == 'Local Admissions' || ruleDetail.Condition_Field__c == 'Local Number of Screens' ){
                isTVDBoxOffice = true;
            } else if(ruleDetail.Condition_Field__c == 'Division' || ruleDetail.Condition_Field__c == 'Release Year'){
                isTitleAttribute = true;
            } else if(ruleDetail.Condition_Field__c == 'Title Tag'){
                isTitleTag = true;
            } else if(ruleDetail.Condition_Field__c == 'Local Theatrical Release'){
                isReleaseDate = true;
            }
        }
        
        EAW_Plan_Guideline__c planGuideline =[SELECT Id, Product_Type__c FROM EAW_Plan_Guideline__c WHERE Id =: planGuidelineId];
        Set<String> productTypeSet = new Set<String>();
        if( planGuideline.Product_Type__c != NULL ){
            productTypeSet.addAll(planGuideline.Product_Type__c.split(';')); 
        }
        //LRCC-1265 
        List<EAW_Title__c> titleAttributeList = [SELECT Id,Title_EDM__c,PROD_TYP_CD_INTL__c,PROD_TYP_CD_INTL__r.Name FROM EAW_Title__c WHERE PROD_TYP_CD_INTL__r.Name IN :productTypeSet ]; //LRCC - 1239 LIMIT ISSUE
        
        
        Map<Id, Id> edmTitleToEawTitleIdMap = new Map<Id, Id>();
        
        Set<Id> titleSet = new Set<Id>();
        //Set<Id> edmTitleSet = new Set<Id>();
        if( titleAttributeList.size() > 0 ){
            for(EAW_Title__c title : titleAttributeList){
                titleSet.add(title.Id);
                //edmTitleSet.add(title.Title_EDM__c);
                edmTitleToEawTitleIdMap.put(title.Title_EDM__c, title.Id);
            }
        }
        System.debug('::::::isTVDBoxOffice::::'+isTVDBoxOffice+':::::isTitleAttribute::::'+isTitleAttribute);
        System.debug('::::::isTitleTag::::'+isTitleTag+':::::isReleaseDate::::'+isReleaseDate);
        
        System.debug(':::::titleSet:::::'+titleSet);
        //System.debug(':::::edmTitleSet:::::'+edmTitleSet);
        System.debug(':::::edmTitleToEawTitleIdMap:::::'+edmTitleToEawTitleIdMap);
        
        //Condition check related to TVD Box Office Object records
        if( isTVDBoxOffice && titleSet.size() > 0 ){
            //LRCC-1250
            EAW_BoxOfficeHandler boxOfficeHanlder = new EAW_BoxOfficeHandler();
            
            List<EAW_Box_Office__c> boxOfficeList = [ SELECT Id, Name, TVD_US_Box_Office__c,Title_Attribute__c,Title_Attribute__r.PROD_TYP_CD_INTL__c,Title_Attribute__r.PROD_TYP_CD_INTL__r.Name,TVD_Local_Currency_Value__c, TVD_Local_Admissions_Count__c,TVD_Number_of_Screens__c FROM EAW_Box_Office__c WHERE Title_Attribute__c IN : titleSet ];
            
            System.debug(':::::: boxOfficeList ::::::'+boxOfficeList);
            
            qualifiedEAWTitleIdSet = boxOfficeHanlder.processTVDBoxOfficeToQualifyTitleForInsert(boxOfficeList, new Set<Id>{planGuidelineId});
            
            /*List<EDM_GLOBAL_TITLE__c> edmTitleTvdBoxOffices = [ Select Id, Name, ( SELECT Id, Name, TVD_US_Box_Office__c,TVD_Admissions_Count__c,TVD_Local_Currency_Value__c, TVD_Local_Admissions_Count__c,TVD_Number_of_Screens__c, TVD_Title__c FROM Box_Office__r ) From EDM_GLOBAL_TITLE__c Where Id IN : edmTitleSet ];
            
            if( edmTitleTvdBoxOffices.size() > 0 ){
                
                //boxOfficeHanlder.processTVDBoxOfficeToQualifyTitleForInsert(boxOfficeList);
                
                set<Id> tvdTitleIdSet = new set<Id>();
                
                for( EDM_GLOBAL_TITLE__c tvdTitle : edmTitleTvdBoxOffices ){
                    
                    Boolean titleQualified = false;
                    
                    for( TVD_Box_Office__c tvd : tvdTitle.Box_Office__r ){
                        
                        Boolean qualified = false;
                        
                        for( EAW_Rule_Detail__c ruleDetail: ruleDetailList ){
                            
                            System.debug('::::: ruleDetail :::::'+ruleDetail);
                            
                            if( tvd.Name == 'USA' ){
                                if( ! String.isBlank(ruleDetail.Condition_Operator__c) ) {
                                    if( ruleDetail.Condition_Field__c == 'US Box Office' && tvd.TVD_US_Box_Office__c != NULL ) {
                                        qualified = qualifierController.arithmeticDecimalConditionalChecking( tvd.TVD_US_Box_Office__c, ruleDetail.Condition_Operator__c, ruleDetail.Condition_Criteria_Amount__c );
                                    } else if( ruleDetail.Condition_Field__c == 'US Admissions' && tvd.TVD_Admissions_Count__c != NULL ) {
                                        qualified = qualifierController.arithmeticDecimalConditionalChecking( tvd.TVD_Admissions_Count__c, ruleDetail.Condition_Operator__c, ruleDetail.Condition_Criteria_Amount__c );
                                    } else if( ruleDetail.Condition_Field__c == 'US Number of Screens' && tvd.TVD_Number_of_Screens__c != NULL ) {
                                        qualified = qualifierController.arithmeticDecimalConditionalChecking( tvd.TVD_Number_of_Screens__c, ruleDetail.Condition_Operator__c, ruleDetail.Condition_Criteria_Amount__c );
                                    }
                                }
                                
                            } else {
                                
                                if( tvd.Name == ruleDetail.Territory__c ){
                                    if( ! String.isBlank(ruleDetail.Condition_Operator__c) ) {
                                        if(ruleDetail.Condition_Field__c == 'Local Box Office' && tvd.TVD_Local_Currency_Value__c != NULL ){
                                            qualified = qualifierController.arithmeticDecimalConditionalChecking( tvd.TVD_Local_Currency_Value__c, ruleDetail.Condition_Operator__c, ruleDetail.Condition_Criteria_Amount__c );
                                        } else if(ruleDetail.Condition_Field__c == 'Local Admissions' && tvd.TVD_Local_Admissions_Count__c != NULL ){
                                            qualified = qualifierController.arithmeticDecimalConditionalChecking( tvd.TVD_Local_Admissions_Count__c, ruleDetail.Condition_Operator__c, ruleDetail.Condition_Criteria_Amount__c );
                                        } else if(ruleDetail.Condition_Field__c == 'Local Number of Screens' && tvd.TVD_Number_of_Screens__c != NULL ){
                                            qualified = qualifierController.arithmeticDecimalConditionalChecking( tvd.TVD_Number_of_Screens__c, ruleDetail.Condition_Operator__c, ruleDetail.Condition_Criteria_Amount__c );
                                        }
                                    }
                                }
                            }
                            
                            if( qualified ) break;
                        }
                        
                        if( qualified ) {
                             titleQualified = TRUE;
                             break;
                        }
                    }
                    
                    if( titleQualified ){
                        qualifiedEAWTitleIdSet.add(edmTitleToEawTitleIdMap.get(tvdTitle.Id));
                    }
                }
                
            }*/
        }
        
        System.debug(':::::: TVD Box Office Qualified Title Ids Set ::::::'+qualifiedEAWTitleIdSet);
        if( qualifiedEAWTitleIdSet != NULL && qualifiedEAWTitleIdSet.size() > 0 ){
            titleSet.removeAll(qualifiedEAWTitleIdSet);
        }
        
        //Condition check related to Release date Object records
        if(isReleaseDate && titleSet.size() > 0){
            EAW_ReleaseDate_Handler releaseDateHandler = new EAW_ReleaseDate_Handler();
            //List<EAW_Release_Date__c> releaseDateList = [SELECT Id, Release_Date__c, Title__c FROM EAW_Release_Date__c  WHERE Title__c IN : titleSet ];
            //if(releaseDateList.size() > 0){
             qualifiedEAWTitleIdSet = releaseDateHandler.checkPlanQualificationRules(null,new Set<Id>{planGuidelineId},titleSet);
            //}
        }
        if( qualifiedEAWTitleIdSet != NULL && qualifiedEAWTitleIdSet.size() > 0 ){
            titleSet.removeAll(qualifiedEAWTitleIdSet);
        }
        
        //Condition check related to Title attribute Object records
        if( isTitleAttribute && titleSet.size() > 0 ){
        	//LRCC-1265
        	EAW_Title_Handler titleHandler = new EAW_Title_Handler();
            List<EAW_Title__c> titleList = [SELECT Id, RLSE_CAL_YR__c, FIN_DIV_CD__c,FIN_DIV_CD__r.Name, PROD_TYP_CD_INTL__c,PROD_TYP_CD_INTL__r.Name FROM EAW_Title__c WHERE Id IN : titleSet ];
            if(titleList.size() > 0){
                qualifiedEAWTitleIdSet = titleHandler.processTitleToCheckForPlanQualification(titleList,new Set<Id>{planGuidelineId});
            }
            System.debug(':::::qualifiedEAWTitleIdSet:::::'+qualifiedEAWTitleIdSet);
        }
        if( qualifiedEAWTitleIdSet != NULL && qualifiedEAWTitleIdSet.size() > 0 ){
            titleSet.removeAll(qualifiedEAWTitleIdSet); 
        }
        //Condition check related to Title Tag Object records
        if( isTitleTag && titleSet.size() > 0 ){
            EAW_TitleTagJunctionTriggerHandler tagHandler = new EAW_TitleTagJunctionTriggerHandler();
           // List<EAW_Title_Tag_Junction__c> tagList = [ SELECT Id,Title_Attribute__c FROM EAW_Title_Tag_Junction__c WHERE Title_Attribute__c IN : titleSet ];
            //if(tagList.size() > 0){
            tagHandler.processTitleTagToCheckForPlanQualification(null,new Set<Id>{planGuidelineId},titleSet);
           // }
        }
    }
    
}