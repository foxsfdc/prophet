public with sharing class EAW_NotesTrigger_Handler {
    
    //LRCC-1260
    public void updateHasNotesInReleaseDateObjectInsert(List<Note> triggerNew){
        
        Set<Id> parentIdSet = new Set<Id>();
        
        
        String releaseDateSobjectPrefix = EAW_Release_Date__c.sobjecttype.getDescribe().getKeyPrefix();
        
        String windowSobjectPrefix = EAW_Window__c.sobjecttype.getDescribe().getKeyPrefix();
        
        
        if( triggerNew != NULL && triggerNew.size() > 0 ){
            
            List<EAW_Release_Date__c> rdListToUpdate = new List<EAW_Release_Date__c>();
            List<EAW_Window__c> windowListToUpdate = new List<EAW_Window__c>();
            
            for( Note noteRec : triggerNew ){
                
                if( noteRec.ParentId != NULL && ! parentIdSet.contains(noteRec.ParentId) ){
                    
                    if( String.valueOf(noteRec.ParentId).startsWith(releaseDateSobjectPrefix) ) {
                        parentIdSet.add(noteRec.ParentId);
                        EAW_Release_Date__c rdRec = new EAW_Release_Date__c();
                        rdRec.Id = noteRec.ParentId;
                        rdRec.Has_Notes__c  = TRUE;
                        rdListToUpdate.add(rdRec);
                    }
                    
                    if( String.valueOf(noteRec.ParentId).startsWith(windowSobjectPrefix) ) {
                        parentIdSet.add(noteRec.ParentId);
                        EAW_Window__c windowRec = new EAW_Window__c();
                        windowRec.Id = noteRec.ParentId;
                        windowRec.Has_Notes__c  = TRUE;
                        windowListToUpdate.add(windowRec);
                    }
                }
            }
            
            if( rdListToUpdate != NULL && rdListToUpdate.size() > 0 ) update rdListToUpdate;
            
            if( windowListToUpdate != NULL && windowListToUpdate.size() > 0 ) update windowListToUpdate;
        }
    }
    
    //LRCC-1787
    public void updateOldNotes( List<Note> Notes ){
    
        Set<Id> notesParentIds = new Set<Id>();
        Set<String> notesApiNames = new Set<String>();
        Map<Id, List<String>> parentIdNotesMap = new Map<Id, List<String>>();
        
        for(Note noteRec : Notes) {
        
            notesParentIds.add(noteRec.ParentId);
            notesApiNames.add(noteRec.ParentId.getSObjectType().getDescribe().getName());
        }
        system.debug('notesParentIdss:::'+notesParentIds);
        
        for(Note noteRec : [Select ParentId, Title, Body From Note where ParentId IN : notesParentIds]) {
            
            if(! parentIdNotesMap.containsKey(noteRec.ParentId)) {
                parentIdNotesMap.put(noteRec.ParentId, new List<String>());
            }
            String newNote = noteRec.Title;
            if(String.isNotBlank(noteRec.Body)) {
                newNote = newNote + '-' + noteRec.Body;
            }
            parentIdNotesMap.get(noteRec.ParentId).add(newNote);
        }
        system.debug('parentIdNotesMap:::'+parentIdNotesMap);
        
        String updateObjectName = new List<String>(notesApiNames)[0];
        
        if(notesApiNames.size() == 1 && updateObjectName == 'EAW_Release_Date__c') {
            
            List<EAW_Release_Date__c> updateReleaseDates = [SELECT OldNotes__c, Notes_Changed_Dates__c, Notes_Changed_Users__c FROM EAW_Release_Date__c WHERE Id IN : notesParentIds];
            
            for(EAW_Release_Date__c record : updateReleaseDates) {
            
                if(record.OldNotes__c != null) {
                    
                    List<String> oldNotesValues = record.OldNotes__c.split('&&&');
                    
                    if(oldNotesValues.size() == 5) {
                        oldNotesValues.remove(0);
                    }
                    
                    if(parentIdNotesMap.containsKey(record.Id)) {
                        oldNotesValues.add(String.join(parentIdNotesMap.get(record.Id), ';'));
                    } else {
                        oldNotesValues.add('null');
                    }
                    
                    record.OldNotes__c = String.join(oldNotesValues, '&&&');
                } else {
                    
                    if(parentIdNotesMap.containsKey(record.Id)) {
                        record.OldNotes__c = String.join(parentIdNotesMap.get(record.Id), ';');
                    } else {
                        record.OldNotes__c = 'null';
                    }
                }
                
                if(record.Notes_Changed_Dates__c != null) {
                
                    List<String> oldValues = record.Notes_Changed_Dates__c.split('&&&');
                    if(oldValues.size() == 5) {
                        oldValues.remove(0);
                    }
                    oldValues.add(String.valueOfGmt(system.now()));
                    record.Notes_Changed_Dates__c = String.join(oldValues,'&&&');
                } else {
                    record.Notes_Changed_Dates__c = String.valueOfGmt(system.now());
                }
                
                if(record.Notes_Changed_Users__c != null) {
                
                    List<String> oldValues = record.Notes_Changed_Users__c.split('&&&');
                    if(oldValues.size() == 5) {
                        oldValues.remove(0);
                    }
                    oldValues.add(UserInfo.getName());
                    record.Notes_Changed_Users__c = String.join(oldValues,'&&&');
                } else {
                    record.Notes_Changed_Users__c = UserInfo.getName();
                }
            
            }
            
            system.debug('updateReleaseDates:::'+updateReleaseDates);
            
            if(updateReleaseDates.isEmpty() == FALSE) {
                update updateReleaseDates;
            }
            
        } else if(notesApiNames.size() == 1 && updateObjectName == 'EAW_Window__c') {
            
            List<EAW_Window__c> updateWindows = [SELECT OldNotes__c, Notes_Changed_Dates__c, Notes_Changed_Users__c FROM EAW_Window__c WHERE Id IN : notesParentIds];
            
            for(EAW_Window__c record : updateWindows) {
            
                if(record.OldNotes__c != null) {
                    
                    List<String> oldNotesValues = record.OldNotes__c.split('&&&');
                    
                    if(oldNotesValues.size() == 5) {
                        oldNotesValues.remove(0);
                    }
                    
                    if(parentIdNotesMap.containsKey(record.Id)) {
                        oldNotesValues.add(String.join(parentIdNotesMap.get(record.Id), ';'));
                    } else {
                        oldNotesValues.add('null');
                    }
                    
                    record.OldNotes__c = String.join(oldNotesValues, '&&&');
                } else {
                    
                    if(parentIdNotesMap.containsKey(record.Id)) {
                        record.OldNotes__c = String.join(parentIdNotesMap.get(record.Id), ';');
                    } else {
                        record.OldNotes__c = 'null';
                    }
                }
                
                if(record.Notes_Changed_Dates__c != null) {
                
                    List<String> oldValues = record.Notes_Changed_Dates__c.split('&&&');
                    if(oldValues.size() == 5) {
                        oldValues.remove(0);
                    }
                    oldValues.add(String.valueOfGmt(system.now()));
                    record.Notes_Changed_Dates__c = String.join(oldValues,'&&&');
                } else {
                    record.Notes_Changed_Dates__c = String.valueOfGmt(system.now());
                }
                
                if(record.Notes_Changed_Users__c != null) {
                
                    List<String> oldValues = record.Notes_Changed_Users__c.split('&&&');
                    if(oldValues.size() == 5) {
                        oldValues.remove(0);
                    }
                    oldValues.add(UserInfo.getName());
                    record.Notes_Changed_Users__c = String.join(oldValues,'&&&');
                } else {
                    record.Notes_Changed_Users__c = UserInfo.getName();
                }
            
            }
            
            system.debug('updateWindows:::'+updateWindows);
            
            if(updateWindows.isEmpty() == FALSE) {
                update updateWindows;
            }
        }        
    }
    
    //LRCC-1260
    public void updateHasNotesInReleaseDateObjectDelete(List<Note> triggerOld){
        
        Set<Id> releaseDateIdSet = new Set<Id>();
        
        Set<Id> parentIdSet = new Set<Id>();
        
        String releaseDateSobjectPrefix = EAW_Release_Date__c.sobjecttype.getDescribe().getKeyPrefix();
        
        String windowSobjectPrefix = EAW_Window__c.sobjecttype.getDescribe().getKeyPrefix();
        
        
        if( triggerOld != NULL && triggerOld.size() > 0 ){
            
            for( Note noteRec : triggerOld ){
                if( noteRec.ParentId != NULL && ! parentIdSet.contains(noteRec.ParentId) && ( String.valueOf(noteRec.ParentId).startsWith(releaseDateSobjectPrefix) || String.valueOf(noteRec.ParentId).startsWith(windowSobjectPrefix) ) ){
                    parentIdSet.add(noteRec.ParentId);
                }
            }
            
            if( parentIdSet.size() > 0 ){
                List<Note> noteList = [ Select Id, Title, ParentId From Note Where ParentId IN : parentIdSet ];
                if( noteList != NULL && noteList.size() > 0 ){
                    for( Note noteRec : noteList ){
                        parentIdSet.remove(noteRec.ParentId);
                    }
                }
            }
            
            if( parentIdSet != NULL && parentIdSet.size() > 0 ){
                
                List<EAW_Release_Date__c> rdListToUpdate = new List<EAW_Release_Date__c>();
                List<EAW_Window__c> windowListToUpdate = new List<EAW_Window__c>();
                
                for( Id rdId : parentIdSet ){
                    if( String.valueOf(rdId).startsWith(releaseDateSobjectPrefix) ) {
                        EAW_Release_Date__c rdRec = new EAW_Release_Date__c();
                        rdRec.Id = rdId;
                        rdRec.Has_Notes__c  = FALSE;
                        rdListToUpdate.add(rdRec);
                    } else if(String.valueOf(rdId).startsWith(windowSobjectPrefix)){
                        EAW_Window__c windowRec = new EAW_Window__c();
                        windowRec.Id = rdId;
                        windowRec.Has_Notes__c  = FALSE;
                        windowListToUpdate.add(windowRec);
                    }
                }
                if( rdListToUpdate != NULL && rdListToUpdate.size() > 0 ) update rdListToUpdate;
                if( windowListToUpdate != NULL && windowListToUpdate.size() > 0 ) update windowListToUpdate;
            }
        }
        
    }
}