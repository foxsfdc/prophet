public class EAW_RDGSoftDeleteController {
   
    public EAW_Release_Date_Guideline__c rdg {get;set;}
    public List <EAW_Release_Date_Guideline__c> rdgList {get;set;}
    public String rdgListString {get;set;}
    public String rdgId {get;set;}
   
    public EAW_RDGSoftDeleteController (ApexPages.StandardController controller) {
    
        rdgList = new List <EAW_Release_Date_Guideline__c> ();     
        rdg = (EAW_Release_Date_Guideline__c)controller.getRecord();
       
        if(rdg != NULL){
            rdgId =rdg.Id;
            rdgList = [SELECT Name,Id,Soft_Deleted__c FROM EAW_Release_Date_Guideline__c WHERE Id =: rdg.Id];
            rdgListString  = JSON.serialize(rdgList);
        }
       
    }
}