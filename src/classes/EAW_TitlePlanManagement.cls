public with sharing class EAW_TitlePlanManagement {
    
    @AuraEnabled
    public static Object getFields(String strObjectName, String strFieldSetName)
    {
        return EAW_FieldSetController.getFields(strObjectName, strFieldSetName); 
    }
    
    @AuraEnabled
    public static List<EAW_Title__c> searchTitleByName(String titleList){
        
        List<EAW_Title__c> titleResultList = new List<EAW_Title__c>();
        String searchQuery = 'SELECT id, name FROM EAW_Title__c Where ';
        
        if(String.isNotBlank(titleList)){
        
            searchQuery+=' Name ='+ '\''+ titleList + '\'';
        }
        
        System.debug('query:'+searchQuery);
        titleResultList = Database.query(searchQuery);
        
        return titleResultList;
    }
}