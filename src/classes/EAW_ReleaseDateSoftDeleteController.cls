public class EAW_ReleaseDateSoftDeleteController{
   
    public EAW_Release_Date__c rd {get;set;}
    public List <EAW_Release_Date__c > rdList {get;set;}
    public String rdListString {get;set;}
    public String rdId {get;set;}
   
    public EAW_ReleaseDateSoftDeleteController(ApexPages.StandardController controller) {
    
        rdList = new List <EAW_Release_Date__c > ();     
        rd = (EAW_Release_Date__c )controller.getRecord();
       
        if(rd != NULL){
        
            rdId =rd.Id;
            rdList = [SELECT Soft_Deleted__c,Active__c,Alert_Count__c,Allow_Manual__c,Customers__c,Customer__c,EAW_Release_Date_Type__c,
                             Feed_Date_Status__c,Feed_Date__c,Galileo_DML_Type__c,Has_Notes__c,HEP_Promotion_Dating_Id__c,
                             Id,Is_Confidential__c,Language__c,Locked_Status__c,Manual_Date__c,Media__c,Name,Product_Type__c,Projected_Date__c,Release_Date_Guideline__c,
                             Release_Date__c,Repo_DML_Type__c,Re_qualify_Check_Prevent_Flag__c,Send_To_Third_Party__c,Sent_To_Galileo__c,
                             Sent_To_Repo__c,SFDC_Base_Instance__c,Status__c,Temp_Perm__c,Territory__c,Title_Id__c,Title__c
                     FROM EAW_Release_Date__c  
                     WHERE Id =: rd.Id]; 
                     
            System.debug('rdList ::::::::'+rdList );
            //rdList.add(rd);
            rdListString  = JSON.serialize(rdList);
        }
       
    }
}