public with sharing class EAW_ReleaseDateGuidelineController {
    
    //LRCC:1010 start
    @AuraEnabled 
    public static UserInformation fetchUser(){
     // query current user information  
      User oUser = [select id,Name  
                 FROM User Where id =: userInfo.getUserId()];
        UserInformation userInformationWrapper = new UserInformation();
        userInformationWrapper.userDetail = oUser;
        userInformationWrapper.timeZone = system.now();
        return userInformationWrapper;
    }    
    
    public class UserInformation {        
        @AuraEnabled public User userDetail;
        @AuraEnabled public DateTime timeZone;
    }
    //LRCC:1010 end
    
    @AuraEnabled
    public static List<Object> getFields(Map<String,String> objFieldSetMap) {

        List<Object>fieldNames = new List<Object>();
        for(String strObjectName: objFieldSetMap.keySet())
        {
            fieldNames.add(EAW_FieldSetController.getFields(strObjectName, objFieldSetMap.get(strObjectName)));
        }
        return fieldNames;
    }
    
    @AuraEnabled
    public static Map<String, List<Object>>  getRecords(Map<String,String> objFieldSetMap, String territory, String language, String productType, 
                                          String customer, String releaseDateType,Integer pagesPerRecord,
                                          Integer currentPageNumber,String pageIdMapUI,String selectedFilterMap) {
                                          
        String queryCondition = ' WHERE Id != null AND Soft_Deleted__c = \'FALSE\'';
        
        Set<Object>fieldValues = new Set<Object>();        
        List<Object>records = new List<Object>();
            
        List<String> products = new List<String>();
        List<String> territories = new List<String>();
        List<String> languages = new List<String>();
        List<String> customers = new List<String>();
        List<String> releaseDateTypes = new List<String>();
        String languageName;
        String territoryName;
        List<String> customersName = new List<String>();
         
        /**LRCC -1459 Pagination Limit **/   
        
        System.debug('pagesPerRecord::::::::::::::::::::::::::'+pagesPerRecord);
        System.debug('currentPageNumber:::::'+currentPageNumber);  
        System.debug('pageIdMap:::::'+pageIdMapUI);  
        System.debug('selectedFilterMap:::::'+selectedFilterMap);   
        Map<String, List<Object>> finalMap =  new  Map<String, List<Object>> ();   
        Map<Integer,Set<Id>> pageIdMapConfig = (Map<Integer,Set<Id>>)JSON.deserialize(pageIdMapUI, Map<Integer,Set<Id>>.class);
        Map<Integer,Set<Id>> pageIdMap = pageIdMapConfig ;
        Map<String,String> selectedFilterMapConfig ;
        if(selectedFilterMap != null){
        
            selectedFilterMapConfig = (Map<String,String>)JSON.deserialize(
            selectedFilterMap, Map<String,String>.class);
        }
        
        System.debug('pageIdMapConfig :::::'+pageIdMapConfig );
        System.debug('selectedFilterMapConfig :::::'+selectedFilterMapConfig );
        
        /**LRCC -1459 End Pagination Limit **/
        
        try {
            for (EAW_LookupSearchResult e : (List<EAW_LookupSearchResult>) JSON.deserialize(productType, List<EAW_LookupSearchResult>.class)) {
                products.add('\'' + e.title + '\'');
            }
            queryCondition += products.size() > 0 ? ' AND Product_Type__c IN' + products : '';
            
            for (EAW_LookupSearchResult e : (List<EAW_LookupSearchResult>) JSON.deserialize(territory, List<EAW_LookupSearchResult>.class)) {
                territories.add('\'' + e.title + '\'');
                territoryName = e.title;
            }
            if(territoryName == 'All') {
                queryCondition += ' AND Territory__c != NULL';
                
            } else if(territoryName != 'All') {
                queryCondition += territories.size() > 0 ? ' AND Territory__c IN' + territories : '';
            }
            for (EAW_LookupSearchResult e : (List<EAW_LookupSearchResult>) JSON.deserialize(language, List<EAW_LookupSearchResult>.class)) {
                languages.add('\'' + e.title + '\'');
                languageName =  e.title ;
            }
            //LRCC-1340
            if(languageName == 'No Specific Language') {
                queryCondition += ' AND (Language__c != NULL OR Language__c = NULL)';              
            } else if(languageName != 'No Specific Language') {
                queryCondition += languages.size() > 0 ? ' AND Language__c IN' + languages : '';
            }
            
            if(String.isNotBlank(releaseDateType)) {
                for (EAW_LookupSearchResult e : (List<EAW_LookupSearchResult>) JSON.deserialize(releaseDateType, List<EAW_LookupSearchResult>.class)) {
                    releaseDateTypes.add('\'' + e.title + '\'');
                }
                queryCondition += releaseDateTypes.size() > 0 ? ' AND EAW_Release_Date_Type__c IN ' + releaseDateTypes : '';
            }
            
            System.debug(':::::::: queryCondition ::::::::::'+queryCondition);
            //LRCC-1236 
            for (EAW_LookupSearchResult e : (List<EAW_LookupSearchResult>) JSON.deserialize(customer, List<EAW_LookupSearchResult>.class)) {
                customers.add((e.title.split(':')[1]).trim().toLowerCase()); //LRCC-1667
            }
            
            /**LRCC -1459 Pagination Limit **/
            system.debug('pageIdMap.containsKey(currentPageNumber)::'+pageIdMap.containsKey(currentPageNumber));
            system.debug('pageIdMap.get(currentPageNumber).size()::'+pageIdMap.get(currentPageNumber));
            string aggQuery = queryCondition ;
            Set<Id> paginationIdSet  ;
            if(pageIdMap.containsKey(currentPageNumber) && pageIdMap.get(currentPageNumber).size() == pagesPerRecord){
                paginationIdSet  = new Set<Id>();
                paginationIdSet  = pageIdMap.get(currentPageNumber);
            
                //if (hasChanged) {
                
                    queryCondition += ' AND ';
                //}
                
                queryCondition += 'Id IN :paginationIdSet ';
                system.debug('queryCondition ::1if::::'+queryCondition );
                //hasChanged = true;
                
            } else if(pageIdMap.containsKey(currentPageNumber) && pageIdMap.get(currentPageNumber).size() != pagesPerRecord){
                paginationIdSet  = new Set<Id>();
                
                for(Integer i=1;i<currentPageNumber;i++){
                    paginationIdSet.addAll(pageIdMap.get(i));
                }
            
                //if (hasChanged) {
                    queryCondition  += ' AND ';
                //}
                
                queryCondition  += 'Id NOT IN :paginationIdSet';
                //hasChanged = true;
                 system.debug('queryCondition ::1ifelse::::'+queryCondition );
                
            } else if(!pageIdMap.containsKey(currentPageNumber) && currentPageNumber != 1){
                 paginationIdSet  = new Set<Id>();
                
                for(Integer i=1;i<currentPageNumber;i++){
                    paginationIdSet.addAll(pageIdMap.get(i));
                }
                
                //if (hasChanged) {
                    queryCondition  += ' AND ';
                //}
                
                queryCondition  += 'Id NOT IN :paginationIdSet ';
               // hasChanged = true;
                system.debug('queryCondition ::2ifelse::::'+queryCondition );
            }
            if(selectedFilterMapConfig != null &&  selectedFilterMapConfig.get('selectedAlpha') != null && selectedFilterMapConfig.get('selectedAlpha') != 'All' && selectedFilterMapConfig.get('selectedCol') != null ){
                //if (hasChanged) {
                    queryCondition  += ' AND ';
                    aggQuery += ' AND ';
                //}
                
                queryCondition  += selectedFilterMapConfig.get('selectedCol') + ' LIKE  \'' + selectedFilterMapConfig.get('selectedAlpha') + '%\'';  
                aggQuery += selectedFilterMapConfig.get('selectedCol') + ' LIKE  \'' + selectedFilterMapConfig.get('selectedAlpha') + '%\'';  
                 
                system.debug('queryCondition ::like::::'+queryCondition );          
            }   
            List<String> listStrings = new List<String>();
            if(paginationIdSet != NULL){
                for(Id pid : paginationIdSet){
                    listStrings.add(pid); 
                }
            }  
            
            //LRCC-1547
            if(String.isNotBlank(queryCondition)) {
                
                queryCondition += ' ORDER BY Territory__c, Product_Type__c, EAW_Release_Date_Type__c ASC NULLS LAST ';
            }
                 
            /**LRCC -1459 Pagination Limit END **/       
            
            if(customers.size()>0)
            {    
                //LRCC-1459
                finalMap.put('totalCount',EAW_FieldSetController.GetRecordsCount('EAW_Release_Date_Guideline__c', objFieldSetMap.get('EAW_Release_Date_Guideline__c'), aggQuery, null, null, listStrings)); //LRCC-1667
                /*
                *LRCC-1667
                if(pagesPerRecord != null){
        
                   queryCondition +=  ' Limit ' + pagesPerRecord;
                }   
                */
                Integer result_size = 0; //LRCC-1667
                Integer totalCount = 0; //LRCC-1667
                
                //LRCC-1675 - added last param NULL 
                
                for(Object obj:(EAW_FieldSetController.genericGetRecords('EAW_Release_Date_Guideline__c', objFieldSetMap.get('EAW_Release_Date_Guideline__c'), queryCondition, null, null, listStrings,null)))
                {

                    EAW_Release_Date_Guideline__c rdg = (EAW_Release_Date_Guideline__c)obj;
                    List<String>rdgCustomers = new List<String>();
                    
                    if(rdg.Customers__c!=null && result_size < 100) //LRCC-1667
                    {
                        rdgCustomers = rdg.Customers__c.split(';');
                        for(String cs: rdgCustomers)
                        {
                            if(customers.contains(cs.toLowerCase())) //LRCC-1667
                            {
                                fieldValues.add(obj); 
                                result_size += 1;  //LRCC-1667
                                totalCount += 1; 
                            } 
                        }
                    } else if(rdg.Customers__c!=null) { //LRCC-1667
                        
                        rdgCustomers = rdg.Customers__c.split(';');
                        for(String cs: rdgCustomers)
                        {
                            if(customers.contains(cs.toLowerCase())) //LRCC-1667
                            {
                                totalCount += 1; 
                            } 
                        }
                    }
                }
                records.addAll(fieldValues);
            }else{
            
                finalMap.put('totalCount',EAW_FieldSetController.GetRecordsCount('EAW_Release_Date_Guideline__c', objFieldSetMap.get('EAW_Release_Date_Guideline__c'), aggQuery, null, null,listStrings));
                
                if(pagesPerRecord != null){
        
                   queryCondition +=  ' Limit ' + pagesPerRecord;
                } 
                //LRCC-1675 - added last param NULL   
               records.addAll(EAW_FieldSetController.genericGetRecords('EAW_Release_Date_Guideline__c', objFieldSetMap.get('EAW_Release_Date_Guideline__c'), queryCondition, null, null,listStrings,null));
           }
        } 
        catch (Exception e) {
            system.debug('Exception:'+e.getMessage()+e.getStackTraceString());
            throw new AuraHandledException('Error : ' + e.getMessage()+e.getStackTraceString());
        }
        
        //LRCC-1459
        finalMap.put('ReleaseDate',records); 
        System.debug('finalMap::::::'+finalMap); 
               
        return finalMap;//records;
    }

    @AuraEnabled
    public static void saveReleaseDateGuidelinesChanges(List<EAW_Release_Date_Guideline__c> updatedDataList) {
        
        String errorMsg = '';
            
        try {
            List<EAW_Release_Date_Guideline__c> dataToUpdate = new List<EAW_Release_Date_Guideline__c>(updatedDataList);
            List<String> customers = new List<String>();
            Map<String, Id> customerMap = new Map<String, Id>();
            List<String> releaseDtTypes = new List<String>();
            Map<String, Id> releaseDtTypeMap = new Map<String, Id>();
            
            Set<Id> rdgIdSet = new Set<Id>();
            //LRCC-1821
            Set<Id> inActiveRDGIdSet = new Set<Id>();
            Set<Id> activeRDGIdSet = new Set<Id>();
            
            Map<Id, EAW_Release_Date_Guideline__c>  rdgMap = new  Map<Id, EAW_Release_Date_Guideline__c> ();
            
            Map<Id, EAW_Release_Date_Guideline__c> oldRDGMap = new Map<Id, EAW_Release_Date_Guideline__c>([ Select Id, Active__c From EAW_Release_Date_Guideline__c Where Id IN : updatedDataList ]);
            
            System.debug('::::::: oldRDGMap ::::::::::'+oldRDGMap);
            
            for (EAW_Release_Date_Guideline__c rdGuideLine : dataToUpdate) {
                //LRCC-1560 - Replace Customer lookup field with Customer text field
                customers.add(rdGuideLine.Customers__c);
                releaseDtTypes.add(rdGuideLine.EAW_Release_Date_Type__c);
                
                EAW_Release_Date_Guideline__c oldRDG = oldRDGMap.get(rdGuideline.Id);
                
                if( rdGuideLine.Active__c != oldRDG.Active__c ) {
                    
                    rdgIdSet.add(rdGuideLine.Id);
                    //LRCC-1821
                    if(rdGuideLine.Active__c == TRUE){
                    
                        activeRDGIdSet.add(rdGuideLine.Id);
                    }
                    if(rdGuideLine.Active__c == FALSE){
                    
                        inActiveRDGIdSet.add(rdGuideLine.Id);
                    }
                }
                if(!rdgMap.containsKey(rdGuideLine.Id)){
                
                    rdgMap.put(rdGuideLine.Id,rdGuideLine);
                }
                
            }
            for (EAW_Customer__c c : [Select Id,Name from EAW_Customer__c where Name IN:customers]) {
                customerMap.put(c.Name, c.Id);
            }
            for (EAW_Release_Date_Type__c r : [Select Id,Name from EAW_Release_Date_Type__c where Name IN:releaseDtTypes]) {
                releaseDtTypeMap.put(r.Name, r.Id);
            }
            for (EAW_Release_Date_Guideline__c rdGuideLine : dataToUpdate) {
                //LRCC-1560 - Replace Customer lookup field with Customer text field
                //rdGuideLine.Customer__c = customerMap.get(rdGuideLine.Customer__c);
                if(customerMap.get(rdGuideLine.Customers__c)!=null)rdGuideLine.Customers__c = customerMap.get(rdGuideLine.Customers__c);
                //LRCC - 1015 and LRCC-1285
                rdGuideLine.Allow_Manual_Check_Flag__c = true;
                //rdGuideLine.EAW_Release_Date_Type__c = releaseDtTypeMap.get(rdGuideLine.EAW_Release_Date_Type__c);
            }
            
            //update dataToUpdate; LRCC-1548.
           /* Savepoint sp = Database.setSavepoint();
            List<Database.SaveResult> updateResults = database.update(dataToUpdate, false);
            
            Integer i = 0;
             
            for(Database.SaveResult sr: updateResults) {
            
                EAW_Release_Date_Guideline__c guideLine = dataToUpdate[i];
                if(!sr.isSuccess()) {
                    
                    Database.Error err = sr.getErrors()[0];
                    if(errorMsg == '') {
                        errorMsg = err.getMessage()+': '+guideLine.Name; 
                    } else {
                        errorMsg += +', '+err.getMessage()+': '+guideLine.Name; 
                    } 
                }
                i++;
            }
            
            if(String.isNotBlank(errorMsg)) {
                Database.rollback(sp);
                throw new AuraHandledException(errorMsg);
            } else {
                if( rdgIdSet != NULL && rdgIdSet.size() > 0 ) {
                    checkRecursiveData.bypassDateCalculation = TRUE;
                    System.debug('::::::: rdgIdSet ::::::::::'+rdgIdSet);
                    EAW_CreateRDForNewRDGBatch releaseDateBatch = new EAW_CreateRDForNewRDGBatch(rdgIdSet,null);
                    releaseDateBatch.releaseDateGuidelineIdSet = rdgIdSet;
                    ID batchprocessid = Database.executeBatch(releaseDateBatch, 200);
                }
            }*/
            // LRCC-1548 - The above logic was commented out to avoid the multiple retries while use the database.update. So, We commented out that logic and used nomal Update
           
            if( datatoUpdate != NULL && datatoUpdate.size() > 0 ) {
            
                //LRCC-1821
                //checkRecursiveData.bypassDateCalculation = TRUE;
                //update datatoUpdate;
                
                checkRecursiveData.bypassDateCalculation = TRUE;
                Savepoint sp = Database.setSavepoint();
                List<Database.SaveResult> updateResults = database.update(dataToUpdate, false);
                
                //if( rdgIdSet != NULL && rdgIdSet.size() > 0 ) { 
                
                    EAW_DownstreamRuleGuidelinesFindUtil util = new EAW_DownstreamRuleGuidelinesFindUtil();
                    Map<Id, Set<Id>> referrencedDownstreamRDGMap = new Map<Id, Set<Id>>();
                    Map<Id, Set<Id>> referrencedDownstreamWGMap = new Map<Id, Set<Id>>();
                    Set<Id> rdgAndWgRefferedRDGIDs = new Set<Id> ();
                    
                   // if( inActiveRDGIdSet != NULL && inActiveRDGIdSet.size() > 0 ) { 
                   if( rdgIdSet != NULL && rdgIdSet.size() > 0 ) {
                    
                        referrencedDownstreamRDGMap = util.findReferencedReleaseDateGuidelineRelatedToReleaseDateGuideline(rdgIdSet );
                        
                        System.debug('::::::: referrencedDownstreamRDGMap ::::::::::'+referrencedDownstreamRDGMap);
                        referrencedDownstreamWGMap = util.findReferencedWindowGuidelineRelatedToReleaseDateGuideline(rdgIdSet);
                    }
        
                    if( referrencedDownstreamRDGMap != NULL && referrencedDownstreamRDGMap.size() > 0) {
                    
                         rdgAndWgRefferedRDGIDs.addAll(referrencedDownstreamRDGMap.keySet());
                    }
                    if(referrencedDownstreamWGMap != NULL && referrencedDownstreamWGMap.size() > 0 ) {
                        
                         rdgAndWgRefferedRDGIDs.addAll(referrencedDownstreamWGMap.keySet());
                    } 
                    for( Id rdgId : inActiveRDGIdSet){
                        
                        if(rdgAndWgRefferedRDGIDs.contains(rdgId)){
                            
                            if(errorMsg ==  NULL){
                                errorMsg = '';
                            }
                            EAW_Release_Date_Guideline__c rdgInst;
                            if(rdgMap.get(rdgId) != NULL){
                            
                                rdgInst = rdgMap.get(rdgId);
                            }
                           
                            errorMsg = errorMsg+Label.EAW_RDGCantBeInactive+' :['+rdgInst.Name+']\n';
                        }
                    }
                    System.debug('errorMsg :::::::'+errorMsg );
                    if(errorMsg != NULL && String.isNotBlank(errorMsg)){
                    
                         Database.rollback(sp);
                        throw new AuraHandledException(errorMsg);
                    }else{
                    
                        //LRCC-1821
                        //checkRecursiveData.bypassDateCalculation = TRUE;
                        //update datatoUpdate;
                        //Savepoint sp = Database.setSavepoint();
                        //List<Database.SaveResult> updateResults = database.update(dataToUpdate, false);
                        
                        Integer i = 0;
                         
                        for(Database.SaveResult sr: updateResults) {
                        
                            EAW_Release_Date_Guideline__c guideLine = dataToUpdate[i];
                            if(!sr.isSuccess()) {
                                
                                Database.Error err = sr.getErrors()[0];
                                if(errorMsg == '') {
                                    errorMsg = err.getMessage()+': '+guideLine.Name; 
                                } else {
                                    errorMsg += +', '+err.getMessage()+': '+guideLine.Name; 
                                } 
                            }
                            i++;
                        }
                        
                        if(String.isNotBlank(errorMsg)) {
                            Database.rollback(sp);
                            throw new AuraHandledException(errorMsg);
                        }else  { //if( rdgIdSet != NULL && rdgIdSet.size() > 0 ) {
                        
                           if( inActiveRDGIdSet != NULL && inActiveRDGIdSet.size() > 0 ) {
                            
                                EAW_BatchReleaseDateSoftDelete softDeleteRDBatch = new EAW_BatchReleaseDateSoftDelete();
                                softDeleteRDBatch.inActiveGuideLineIds = inActiveRDGIdSet ;
                                ID batchprocessid = Database.executeBatch(softDeleteRDBatch, 200);
                            }
                        
                            //for( Id rdgId : referrencedDownstreamRDGMap.keySet()){
                               // rdgIdSet.removeAll(referrencedDownstreamRDGMap.get(rdgId));
                            //}
                            
                            if( rdgAndWgRefferedRDGIDs.size() > 0){
                             
                                activeRDGIdSet.removeAll(rdgAndWgRefferedRDGIDs);
                            }
                            System.debug('::::::: activeRDGIdSet::::::::::'+activeRDGIdSet);
                            
                            /*EAW_CreateRDForNewRDGBatch releaseDateBatch = new EAW_CreateRDForNewRDGBatch(rdgIdSet,null);
                            releaseDateBatch.releaseDateGuidelineIdSet = rdgIdSet;
                            ID batchprocessid = Database.executeBatch(releaseDateBatch, 200);*/
                            if(activeRDGIdSet.size() > 0){
                                EAW_RDGDateCalculationBatchClass releaseDateBatch = new EAW_RDGDateCalculationBatchClass(activeRDGIdSet);
                                releaseDateBatch.titleIdSet = NULL;
                                releaseDateBatch.windowGuidelineIdSet = NULL;
                                ID batchprocessid = Database.executeBatch(releaseDateBatch, 200);
                            }
                        }
                    }
                //}
            }
        } catch (Exception e) {
            throw new AuraHandledException(errorMsg);
        }
    }
    
    @AuraEnabled
    public static void deleteReleaseDateGuidelines(List<String> recIds) {

            
            List<EAW_Release_Date_Guideline__c> items = new List<EAW_Release_Date_Guideline__c>();
             List<EAW_Release_Date_Guideline__c> updateItems = new List<EAW_Release_Date_Guideline__c>();
            String errorMsg;
            
            items = [SELECT Id,Name,Soft_Deleted__c FROM EAW_Release_Date_Guideline__c WHERE Id IN: recIds];
            System.debug('items ::::'+items );
            if(items.size() > 0){
            
                errorMsg = EAW_GenericSoftDeleteOverride.preventDeletionofRDG(items);
                System.debug('errorMsg ::RDG::'+errorMsg );
                if(errorMsg  != NULL){
                
                    throw new AuraHandledException('Error : ' + errorMsg );
                    
                }else{
                
                    for (EAW_Release_Date_Guideline__c  rdgRec: items ) {
                    
                        EAW_Release_Date_Guideline__c rlDtGuideline = new EAW_Release_Date_Guideline__c();
                        rdgRec.Soft_Deleted__c = 'TRUE';
                        updateItems.add(rdgRec);
                    }
                     System.debug('updateItems ::::'+updateItems );
                    update updateItems ;  
                }
            }
            
        //Commented for soft delete
      
       /* try {
            for (String recId : recIds) {
                EAW_Release_Date_Guideline__c rlDtGuideline = new EAW_Release_Date_Guideline__c();
                rlDtGuideline.Id = recId;
                items.add(rlDtGuideline);
            }
            delete items;
        } catch (Exception e) {
            throw new AuraHandledException('Error : ' + e.getMessage());
        }*/
    }
    @AuraEnabled
    public static void massUpdateReleaseDateGuidelines(List<String> recIds, String newTerritoryName, String newLanguageName, String newCustomerName, String newProductName, String newReleaseDateName, String note, String title) {

        try {

            List<EAW_Release_Date_Guideline__c> items = new List<EAW_Release_Date_Guideline__c>();
          //  List<EAW_Release_Date_Type__c> releaseDateTypeList = [Select Id,Name from EAW_Release_Date_Type__c where Name = :newReleaseDateName];
            List<EAW_Customer__c> customerList = [Select Id from EAW_Customer__c where Name = :newCustomerName];

            for (String recId : recIds) {
                EAW_Release_Date_Guideline__c rlDtGuideline = new EAW_Release_Date_Guideline__c();
                rlDtGuideline.Id = recId;

                if (newTerritoryName != null)
                    rlDtGuideline.Territory__c = newTerritoryName;
                if (newReleaseDateName != null)
                    rlDtGuideline.EAW_Release_Date_Type__c = newReleaseDateName;
                if (customerList.size() > 0){
                    //LRCC-1560 - Replace Customer lookup field with Customer text field
                    //rlDtGuideline.Customer__c = customerList[0].Id;
                    rlDtGuideline.Customers__c = customerList[0].Name;
                }
                if (newProductName != null)
                    rlDtGuideline.Product_Type__c = newProductName;
                if (newLanguageName != null)
                    rlDtGuideline.Language__c = newLanguageName;

                items.add(rlDtGuideline);
            }
            if (items.size() > 0) {
                update items;
            }
            insertNotes(note, recIds, title);

        } catch (Exception e) {
            throw new AuraHandledException('Error : ' + e.getMessage());
        }
    }
    @AuraEnabled
    public static AttachmentWrapper getNotes(String recId) {
        
        AttachmentWrapper attachWrapper = new AttachmentWrapper();
        
        List<Note> notesList = [Select Title, CreatedBy.Name, Body, LastModifiedDate From Note where ParentId =: recId ORDER BY LastModifiedDate];
        
        attachWrapper.attachmentList = notesList;
        return attachWrapper;
        
    }
    //LRCC-1456
    @AuraEnabled
    public static AttachmentWrapper getContentDocumentLink(String recId) {
        
        Set<Id> contDocId = new Set<Id>();        
        AttachmentWrapper attachWrapper = new AttachmentWrapper();
        List<ContentDocumentLink> contDoctList = [SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =:recId];
        if(contDoctList.isEmpty() == False) {
        
            for(ContentDocumentLink cdl : contDoctList ) {    
                contDocId.add(cdl.ContentDocumentId);
            }   
        }     
        List<ContentDocument> contentDocumentList = [Select Id, Title from ContentDocument Where Id IN :contDocId ORDER BY LastModifiedDate];    
        
        attachWrapper.attachmentList = contentDocumentList;            
      
        return attachWrapper;
    }
    
    public class AttachmentWrapper {
        
        @AuraEnabled
        public List<SObject> attachmentList;
    }
    
    @AuraEnabled
    public static List<Note> insertNotes(String note, List<String> parentIds, String title) {
        List<Note> notes = new List<Note>();
        List<Id>noteIds = new List<Id>();
        try {
            if (title != null && title != '') {
                for (String pId : parentIds) {
                    Note n = new Note(Title = title, Body = note, ParentId = pId);
                    notes.add(n);
                }
                if (notes.size() > 0)
                    insert notes;
            } else if (note != null && note != '') {
                throw new AuraHandledException('Title cannot be blank');
            }
        } catch (Exception e) {
            throw new AuraHandledException('Error : ' + e.getMessage());
        }
        for (Note n : notes)
            noteIds.add(n.Id);
        return [Select Title, CreatedBy.Name, Body, LastModifiedDate From Note where Id = :noteIds];
    }
}