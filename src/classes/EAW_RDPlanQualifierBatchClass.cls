public class EAW_RDPlanQualifierBatchClass implements Database.Stateful,Database.Batchable<sObject>{

    public set<Id> releaseDateIdSet = new Set<Id>();
    public set<Id> windowGuidelineIdSet = new Set<Id>();
    public Set<Id> releaseDateGuidelineIdSet = new Set<Id>();
    public Set<Id> titleIdSet = new Set<Id>();
    public boolean disQualifyFlag = false;
    
    Set<Id> windowGuidelinesIdToProcess = new Set<Id>();
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
    
        system.debug('::releaseDateIdSet::'+releaseDateIdSet);
        System.debug('::::::: titleIdSet ::::::::::'+titleIdSet);
        
        String query = '';
        
        if( titleIdSet != NULL && titleIdSet.size() > 0 ) {
            Boolean statusFlag = TRUE;
            String releaseDateType = 'Theatrical';
            //query = 'SELECT Id,PROD_TYP_CD_INTL__c,PROD_TYP_CD_INTL__r.Name, (SELECT Id, Release_Date__c,Territory__c  FROM Release_Dates__r WHERE Active__c =: statusFlag AND Release_Date__c != NULL ) FROM EAW_Title__c WHERE Id IN :titleIdSet AND PROD_TYP_CD_INTL__c != NULL AND PROD_TYP_CD_INTL__r.Name != NULL';
            if(releaseDateIdSet != NULL && releaseDateIdSet.size() > 0 ){
                query = 'SELECT Id,PROD_TYP_CD_INTL__c,PROD_TYP_CD_INTL__r.Name, (SELECT Id, Release_Date__c,Territory__c  FROM Release_Dates__r WHERE Id IN :releaseDateIdSet AND Active__c =: statusFlag AND EAW_Release_Date_Type__c =: releaseDateType AND Release_Date__c != NULL ) FROM EAW_Title__c WHERE Id IN : titleIdSet AND PROD_TYP_CD_INTL__c != NULL AND PROD_TYP_CD_INTL__r.Name != NULL';
            } else {
                query = 'SELECT Id,PROD_TYP_CD_INTL__c,PROD_TYP_CD_INTL__r.Name, (SELECT Id, Release_Date__c,Territory__c  FROM Release_Dates__r WHERE EAW_Release_Date_Type__c =: releaseDateType AND Active__c =: statusFlag AND Release_Date__c != NULL ) FROM EAW_Title__c WHERE Id IN : titleIdSet AND PROD_TYP_CD_INTL__c != NULL AND PROD_TYP_CD_INTL__r.Name != NULL';
            }
        } else {
            query = 'SELECT Id,PROD_TYP_CD_INTL__c,PROD_TYP_CD_INTL__r.Name FROM EAW_Title__c Limit 0';
        }
        System.debug(':::: query :::::'+query);
        return Database.getQueryLocator(query);
    }

    
    public void execute(Database.BatchableContext BC, list<sObject> rdRecList){
        
        //System.debug(':::::::::: rdRecList :::::::::::;'+rdRecList);
        //Plan-Qualifier variables
        Map<Id, Set<Id>> titleAndPlanGuidelineIdMap = new Map<Id, Set<Id>>();
        Set<Id> qualifiedPlanGuidelineSet = new Set<Id>();
        Set<String> productTypeNameSet = new Set<String>();
        
        //LRCC-1394 - Notification.
        List<FeedItem> feedItems = new List<FeedItem> ();
        Map<Id, String> titleIdName = new Map<Id, String> ();
        String bodyContent;
        //Get custom settings - ON/OFF switches for notification.
        EAW_Notification_Status__c ns = EAW_Notification_Status__c.getOrgDefaults();
        //Get chatter group Id.
        List<CollaborationGroup> lstChatterGroup = [SELECT Id, Name FROM CollaborationGroup WHERE Name = 'Title Attribute Review'];
        Set<Id> titleIds = new Set<Id> ();
        Set<Id> disQualifiedPG = new Set<Id> ();
        
        List<EAW_Title__c> eawTitleList = (List<EAW_Title__c>) rdRecList;
        //System.debug(':::::::::: eawTitleList :::::::::::;'+eawTitleList);
        
        RecordType recType = [Select Id from RecordType WHERE Name = 'Qualifier' AND SObjectType = 'EAW_Rule__c'];
            
        //List<EAW_Rule__c> qualificationRules = [Select Id, Plan_Guideline__c,Plan_Guideline__r.Product_Type__c, (Select Id, Territory__c, Condition_Field__c,Condition_Date__c, Condition_Operator__c, Condition_Criteria_Amount__c from Rule_Orders__r WHERE Condition_Field__c = 'Local Theatrical Release' AND Territory__c IN : territorySet ) from EAW_Rule__c WHERE RecordTypeId=: recType.Id AND Plan_Guideline__c != NULL AND Plan_Guideline__r.Product_Type__c != NULL AND Plan_Guideline__r.Status__c = 'Active' ];
        
        List<EAW_Rule__c> qualificationRules = [Select Id, Plan_Guideline__c,Plan_Guideline__r.Product_Type__c, (Select Id, Territory__c, Condition_Field__c,Condition_Date__c, Condition_Operator__c, Condition_Criteria_Amount__c from Rule_Orders__r WHERE Condition_Field__c = 'Local Theatrical Release' ) from EAW_Rule__c WHERE RecordTypeId=: recType.Id AND Plan_Guideline__c != NULL AND Plan_Guideline__r.Product_Type__c != NULL AND Plan_Guideline__r.Status__c = 'Active' ];
        
        //System.debug(':::::::::: qualificationRules :::::::::::;'+qualificationRules);
        
        EAW_PlanQualifierCalculationController qualifierController = new EAW_PlanQualifierCalculationController();
        
        for( EAW_Rule__c rule: qualificationRules ){    
                
            Boolean qualified = false;
            
            List<EAW_Rule_Detail__c> ruleDetailList = (List<EAW_Rule_Detail__c>) rule.getSobjects('Rule_Orders__r');
                        
            System.debug(':::::::::: ruleDetailList :::::::::::;'+ruleDetailList);
            
            if( ruleDetailList != NULL && ruleDetailList.size() > 0 ) { 
            
                for(EAW_Title__c title : eawTitleList){
                    
                    List<String> productTypeSet = rule.Plan_Guideline__r.Product_Type__c.toUpperCase().split(';');
                    
                    if(productTypeSet.contains(title.PROD_TYP_CD_INTL__r.Name.toUpperCase())){
                        
                        //System.debug(':::::::::: title.Release_Dates__r :::::::::::;'+title.Release_Dates__r);
                        
                        List<EAW_Release_Date__c> releaseDateList = (List<EAW_Release_Date__c>) title.getSobjects('Release_Dates__r');
                        
                        System.debug(':::::::::: releaseDateList :::::::::::;'+releaseDateList);
                        
                        if( releaseDateList != NULL && releaseDateList.size() > 0 ) {
                            
                            for(EAW_Release_Date__c releaseDate: releaseDateList ){
                                
                                //System.debug(':::::::::: releaseDate :::::::::::;'+releaseDate);
                            
                                for(EAW_Rule_Detail__c ruleDetail: ruleDetailList )
                                {
                                    if( ruleDetail.Condition_Field__c == 'Local Theatrical Release' && releaseDate.Release_Date__c != NULL && releaseDate.Territory__c != NULL && releaseDate.Territory__c == ruleDetail.Territory__c /*LRCC - 1201*/){
                                        qualified = qualifierController.arithmeticDateConditionalChecking( releaseDate.Release_Date__c, ruleDetail.Condition_Operator__c, ruleDetail.Condition_Date__c);
                                    }
                                    if(qualified) break;
                                }
                                if(qualified) break;
                            }
                        }
                    }
                    //System.debug(':::::::::: qualified :::::::::::;'+qualified);
                    if(qualified)
                    {
                        String titleId = title.Id;
                        //create a map of plan guideline and title and use the method of title plan controller to create plans and windows
                        Set<Id> planGuidelineSet = new Set<Id>();
                        if( titleAndPlanGuidelineIdMap.containsKey(titleId) ) {
                            planGuidelineSet = titleAndPlanGuidelineIdMap.get(titleId);
                        }
                        planGuidelineSet.add(rule.Plan_Guideline__c);
                        qualifiedPlanGuidelineSet.add(rule.Plan_Guideline__c);
                        titleAndPlanGuidelineIdMap.put(titleId, planGuidelineSet);
                        productTypeNameSet.add(title.PROD_TYP_CD_INTL__r.Name);
                    }
                    //LRCC-1394: Notification.
                    else if(disQualifyFlag != null && disQualifyFlag == true) {
                    
                        disQualifiedPG.add(rule.Plan_Guideline__c);
                        titleIds.add(title.Id);
                    }
                    //System.debug(':::::::::: titleAndPlanGuidelineIdMap  :::::::::::;'+titleAndPlanGuidelineIdMap);
                    //System.debug(':::::::::: qualifiedPlanGuidelineSet :::::::::::;'+qualifiedPlanGuidelineSet);
                }
            
            }
        }
        
        //System.debug(':::::::::: titleAndPlanGuidelineIdMap  :::::::::::;'+titleAndPlanGuidelineIdMap);
        //System.debug(':::::::::: qualifiedPlanGuidelineSet :::::::::::;'+qualifiedPlanGuidelineSet);
        
        if( titleAndPlanGuidelineIdMap != NULL && titleAndPlanGuidelineIdMap.size() > 0 ){
            //LRCC - 1239
            checkRecursiveData.bypassDateCalculation = TRUE; // To avoid the recursive call for date calculation during Plan/Window insert
            EAW_PlanGuidelineQualifierController.createPlanAndWindowsForQualifiedTitles(titleAndPlanGuidelineIdMap,qualifiedPlanGuidelineSet);
            
            if( productTypeNameSet != NULL && qualifiedPlanGuidelineSet != NULL ) {
                for( EAW_Plan_Window_Guideline_Junction__c pwJun : [ Select Id, Window_Guideline__c From EAW_Plan_Window_Guideline_Junction__c Where Plan__c IN : qualifiedPlanGuidelineSet AND Window_Guideline__r.Product_Type__c IN : productTypeNameSet AND Window_Guideline__r.Status__c = 'Active' AND Plan__r.Status__c = 'Active' ] ){
                    windowGuidelinesIdToProcess.add(pwJun.Window_Guideline__c);
                }
            }
        }
        
        //LRCC-1394: Notification.
        if(!lstChatterGroup.isEmpty()) {
        
            if(disQualifiedPG.size() > 0 && titleIds.size() > 0 
                && ns.TACategoryNotification__c == true && ns.TATitlePlanDisQualifierNotification__c == true) {
                
                for(EAW_Plan__c plan: [SELECT Id, Plan_Guideline__c, Plan_Guideline__r.Name, EAW_Title__c, EAW_Title__r.Name__c FROM EAW_Plan__c WHERE Plan_Guideline__c IN: disQualifiedPG AND EAW_Title__c IN: titleIds]) {
                    
                    if(checkRecursiveDataforNotification.isRecursiveData(plan.Id)) {
                        
                        if(String.isBlank(bodyContent)) {
                            bodyContent = plan.EAW_Title__r.Name__c +' - disqualifies for '+plan.Plan_Guideline__r.Name+'.'+'\n'+System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+plan.Id;
                        }
                        else {
                            bodyContent += '\n\n'+plan.EAW_Title__r.Name__c +' - disqualifies for '+plan.Plan_Guideline__r.Name+'.'+'\n'+System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+plan.Id;
                        }
                    }
                }               
            }
                
            //LRCC-1394 - Notification.
            if(String.isNotBlank(bodyContent)) {
                
                String temp = bodyContent;
                if(bodyContent.length() < 10000) {
                    bodyContent = temp.substring(0, temp.length());
                } 
                else {
                    bodyContent = temp.substring(0,9999);    
                }
                                    
                feedItems.add(
                    new FeedItem(
                        Body = bodyContent,
                        ParentId = lstChatterGroup[0].Id
                    )
                );  
                if(feedItems != null && feedItems.size() > 0) {
                    insert feedItems;
                }
            }
        }
    }
        
    public void finish(Database.BatchableContext BC){
    
        EAW_DownstreamRuleGuidelinesFindUtil dependentGuidelineFindUtil = new EAW_DownstreamRuleGuidelinesFindUtil();
        
        Set<Id> windowGuidelinesIdToProcess = new Set<Id>();
            
        if( windowGuidelineIdSet != NULL && windowGuidelineIdSet.size() > 0 ) windowGuidelinesIdToProcess.addAll(windowGuidelineIdSet);
        
        if( releaseDateGuidelineIdSet != NULL && releaseDateGuidelineIdSet.size() > 0 ) {
            windowGuidelinesIdToProcess.addAll(dependentGuidelineFindUtil.findDependentWindowGuidelineToProcess(releaseDateGuidelineIdSet));
        }
        
        System.debug('::::::: windowGuidelinesIdToProcess ::::::::::'+windowGuidelinesIdToProcess);
        System.debug('::::::: titleIdSet ::::::::::'+titleIdSet);
        
        Set<Id> releaseDateGuidelineIdToProcess;
        
        if( windowGuidelinesIdToProcess == NULL || windowGuidelinesIdToProcess.size() == 0 ) releaseDateGuidelineIdToProcess = dependentGuidelineFindUtil.findDependentReleaseDateGuidelineToProcess(releaseDateGuidelineIdSet );
        
        if( windowGuidelinesIdToProcess != NULL && windowGuidelinesIdToProcess.size() > 0 ) {
            EAW_WGDateCalculationBatchClass windowDateBatch = new EAW_WGDateCalculationBatchClass(windowGuidelinesIdToProcess);
            windowDateBatch.releaseDateGuidelineIdSet = releaseDateGuidelineIdSet;
            windowDateBatch.titleIdSet = titleIdSet;
            
            //windowDateBatch.windowGuidelineIdSet = windowGuidelinesIdToProcess;
            ID batchprocessid = Database.executeBatch(windowDateBatch, 200);
        } else if( releaseDateGuidelineIdToProcess != NULL && releaseDateGuidelineIdToProcess.size() > 0 ) {
            /*EAW_CreateRDForNewRDGBatch releaseDateBatch = new EAW_CreateRDForNewRDGBatch(releaseDateGuidelineIdToProcess, titleIdSet);
            releaseDateBatch.titleIdSet = titleIdSet;
            releaseDateBatch.releaseDateGuidelineIdSet = releaseDateGuidelineIdToProcess;
            releaseDateBatch.windowGuidelineIdSet = NULL;
            ID batchprocessid = Database.executeBatch(releaseDateBatch, 200);*/
            
            EAW_RDGDateCalculationBatchClass releaseDateBatch = new EAW_RDGDateCalculationBatchClass(releaseDateGuidelineIdToProcess);
            releaseDateBatch.titleIdSet = titleIdSet;
            releaseDateBatch.windowGuidelineIdSet = NULL;
            ID batchprocessid = Database.executeBatch(releaseDateBatch, 200);
        } else {
          
            EAW_ReleaseDate_API_BatchProcess rdBatch = new EAW_ReleaseDate_API_BatchProcess();
            ID batchprocessid = Database.executeBatch(rdBatch, 100);
        }
    }
}