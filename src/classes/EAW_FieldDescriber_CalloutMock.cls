@isTest
global class EAW_FieldDescriber_CalloutMock implements HttpCalloutMock  {

    EAW_Plan_Guideline__c planguideLine = new EAW_Plan_Guideline__c();
    
    String testString = '{"aA43D0000004Cqu" : "EAW_Plan_Guideline__c ", "controllerValues" : {"Lifecycle__c" : "1st Run"}, "values" : ["EAW_Plan_Guideline__c"]}';
    Blob bodyAsBlob = Blob.valueof(testString );

        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse resp = new HttpResponse();
            resp.setStatusCode(200);
            resp.setStatus('ok');
            resp.setBodyAsBlob(bodyAsBlob);
           
            return resp;
    }
}