public with sharing class EAW_HEP_PromotionDatingHandler {
	
    private static Map<String,List<String>> channelFormatMap;
    private static Map<String,String> statusMap;
    public List<HEP_Promotion_Dating__c>prmDtList = new List<HEP_Promotion_Dating__c>();
    public List<String>channelList = new List<String>();
    public List<String>catalogList = new List<String>();
    public List<String>languageList = new List<String>();
    public List<String>territoryList = new List<String>();
    //We need to add product id in all filter conditions
    private Set<String> finProductIdSet = new Set<String>();
    
    private Set<String> prophetTerritorySet = new Set<String>();
    private Set<String> prophetLanguageSet = new Set<String>();
    private Set<String> productTypeSet = new Set<String>(); 

    private Set<String> mediaSet = new Set<String>();
    //Review: To check existence of title in Title attribute and also get product type
    public Map<String,EAW_Title__C> finProdIdEtcMap=new Map<String,EAW_Title__C> ();
    
    public Map<HEP_Promotion_Dating__c,List<HEP_Promotion_Dating__c>>matchingPrmDts = new Map<HEP_Promotion_Dating__c,List<HEP_Promotion_Dating__c>>();
    public Map<HEP_Promotion_Date_API.PromotionDate,Set<HEP_Promotion_Date_API.PromotionDate>>groupingPrmDts = new Map<HEP_Promotion_Date_API.PromotionDate,Set<HEP_Promotion_Date_API.PromotionDate>>();
    public Map<HEP_Promotion_Dating__c,List<EAW_Release_Date_Guideline__c>>matchingPrmDtRdgs = new Map<HEP_Promotion_Dating__c,List<EAW_Release_Date_Guideline__c>>();
    public Map<HEP_Promotion_Date_API.PromotionDate,Set<EAW_Release_Date_Guideline__c>>groupingPrmDtRdgs = new Map<HEP_Promotion_Date_API.PromotionDate,Set<EAW_Release_Date_Guideline__c>>();
    
    public String[] ProductTypeFromFoxSphere = new String[]{'feature','direct to video','movie of the week','special'};
    
    private static String[] middleEastTerritoryCodes = new String[]{'EG','SA','AE','BH','JO','KW','LB','OM','QA','SY','IQ'};
    private static Set<String> middleEastTerritories = new Set<String>();
    static {
    	//Based on the channel received, apply the release dates to respective date types in prophet
		//For Example: If we receive ‘EST’ as channel, we need to apply the date to EST-HD and EST-SD release date types.
    	channelFormatMap = new Map<String,List<String>>();
    	channelFormatMap.put('EST',new String[]{'EST-HD','EST-SD'});
    	channelFormatMap.put('DHD',new String[]{'EST-HD','EST-SD'});
    	
    	channelFormatMap.put('PDHD',new String[]{'PEST'});
    	channelFormatMap.put('PEST',new String[]{'PEST'});
    	
    	channelFormatMap.put('SPDHD',new String[]{'SPEST'});
    	channelFormatMap.put('SPEST',new String[]{'SPEST'});
    	
    	channelFormatMap.put('RENTAL',new String[]{'HV-Rental'});
    	channelFormatMap.put('RETAIL',new String[]{'HV-Retail'});
    	channelFormatMap.put('KIOSK',new String[]{'HV-Retail'});
    	channelFormatMap.put('PVOD',new String[]{'PVOD'});
    	channelFormatMap.put('SPVOD',new String[]{'SPVOD'});
    	channelFormatMap.put('VOD',new String[]{'VOD-HD','VOD-SD'});
    	
    	statusMap = new Map<String,String>();
    	
    	statusMap.put('Tentative','Tentative');
    	statusMap.put('Confirmed','Firm');
    	statusMap.put('Projected','Estimated');
    	statusMap.put('Not Released','Not Released');
    	statusMap.put('Reconfirm','Tentative');
		
    }
    /*
    *Depricated
    */
    /*
    //handling bulk scenario
    public void handlePromotionDate(HEP_Promotion_Dating__c[] prmDts)
    {
    	//get custom setting ids for language, media and territory?
    	List<EAW_Territories__c> territorySettings = EAW_Territories__c.getall().values();
    	List<EAW_Languages__c> languageSettings = EAW_Languages__c.getall().values();
    	List<EAW_Media__c> mediaSettings = EAW_Media__c.getall().values();
    	
    	boolean isAllLanguage = false;
    	
    	Map<String,EAW_Territories__c> territoryMap = new Map<String,EAW_Territories__c>();
    	for(EAW_Territories__c eawtc:territorySettings){
    		territoryMap.put(eawtc.Territory_Code__c,eawtc);
    		if(middleEastTerritoryCodes.contains(eawtc.Territory_Code__c)){
    			middleEastTerritories.add(eawtc.name);
    		}
    	}
    	
    	Map<String,EAW_Languages__c> languageMap = new Map<String,EAW_Languages__c>();
    	for(EAW_Languages__c eawlc:languageSettings)languageMap.put(String.valueof(eawlc.Language_Id__c),eawlc);

    	for(HEP_Promotion_Dating__c prmDt: prmDts)
    	{
    		if(prmDt.HEP_Catalog__c != NULL && prmDt.HEP_Catalog__r.Title_EDM__c !=null && prmDt.Territory__c!=null && prmDt.Territory__r.Send_to_Prophet__c 
    			&& prmDt.Customer__c == NULL && prmDt.Date__c != NULL && prmDt.DateType__c!=null && prmDt.DateType__c.contains('Release Date'))
    		//Dates matching these conditions are only considered in Prophet
			{
				prmDtList.add(prmDt);
				channelList.add(prmDt.Channel__c);
				catalogList.add(prmDt.HEP_Catalog__c);
				if(prmDt.Language__c==null)isAllLanguage = true;//If Language of promotion date is null then it will applied to All language i.e, 'No Specific Language' on prophet side
				languageList.add(prmDt.Language__c);
				territoryList.add(prmDt.Territory__c);
				
				if(prmDt.Territory__r.Territory_Code_Integration__c=='AE'){
					prophetTerritorySet.addall(middleEastTerritories);
				}else{
					prophetTerritorySet.add(territoryMap.get(prmDt.Territory__r.Territory_Code_Integration__c).Name);
				}
				prophetLanguageSet.add((prmDt.Language__c!=null)?languageMap.get(prmDt.Language__r.Prophet_Language_Id__c).name:'No Specific Language');
				finProductIdSet.add(prmDt.HEP_Catalog__r.Title_EDM__r.FIN_PROD_ID__c);
				mediaSet.addall(channelFormatMap.get(prmDt.Channel__c));
			}
    	}
    	List<HEP_Promotion_Dating__c> filteredPrmDtList = null;
    	if(isAllLanguage){
	    	filteredPrmDtList = [SELECT Channel__c,Date__c,HEP_Catalog__c,HEP_Catalog__r.Title_EDM__c,Language__c,Locked_Status__c,Record_Status__c,Territory__c  FROM HEP_Promotion_Dating__c WHERE 
															Channel__c IN:channelList AND HEP_Catalog__c IN:catalogList 
															AND (Language__c IN:languageList OR Language__c = null)//if Language is null, then add that condition in filteredPrmDtList soql
															AND Territory__c IN:territoryList AND HEP_Catalog__r.Title_EDM__r.FIN_PROD_ID__c in:finProductIdSet
															ORDER BY Date__c ASC ];
    	}else{
    		filteredPrmDtList = [SELECT Channel__c,Date__c,HEP_Catalog__c,HEP_Catalog__r.Title_EDM__c,Language__c,Locked_Status__c,Record_Status__c,Territory__c  FROM HEP_Promotion_Dating__c WHERE 
															Channel__c IN:channelList AND HEP_Catalog__c IN:catalogList 
															AND (Language__c IN:languageList)
															AND Territory__c IN:territoryList AND HEP_Catalog__r.Title_EDM__r.FIN_PROD_ID__c in:finProductIdSet
															ORDER BY Date__c ASC ];
    	}
															
    	//for each promotion we get corresponding list of promotions that match with channel/language/territory/catalog in ascending order
    	for(HEP_Promotion_Dating__c prmDt: prmDtList)
    	{
    		List<HEP_Promotion_Dating__c>fPrmDtList = new List<HEP_Promotion_Dating__c>();
    		for(HEP_Promotion_Dating__c fPrmDt: filteredPrmDtList)
    		{
    			if(fPrmDt.Channel__c == prmDt.Channel__c && fPrmDt.HEP_Catalog__c == prmDt.HEP_Catalog__c && fPrmDt.Language__c == prmDt.Language__c && fPrmDt.Territory__c == prmDt.Territory__c)
    			{
    				fPrmDtList.add(fPrmDt);
    			}
    		}
    		matchingPrmDts.put(prmDt,fPrmDtList);
    	}
    	
    	//Review: To get product types of each title and we get titles which are exist in Title Attribute object
    	for(EAW_Title__c etc:[select id,FIN_PROD_ID__c,PROD_TYP_CD_INTL__c,PROD_TYP_CD_INTL__r.Name,FOX_ID__c from EAW_Title__c where FIN_PROD_ID__c in :finProductIdSet]){
    		productTypeSet.add(etc.PROD_TYP_CD_INTL__r.Name);
    		finProdIdEtcMap.put(etc.FIN_PROD_ID__c,etc);
    	}
    	
    	List<EAW_Release_Date_Guideline__c> rdgList = [Select Id, Name,Language__c,Territory__c,Product_Type__c, EAW_Release_Date_Type__c,
            											(select Id,Feed_Date__c,Feed_Date_Status__c,Title__c,Release_Date_Guideline__c from Release_Dates__r where Title__r.FIN_PROD_ID__c IN:finProductIdSet)
             										From EAW_Release_Date_Guideline__c Where Territory__c IN:prophetTerritorySet and Product_Type__c IN:productTypeSet and EAW_Release_Date_Type__c IN:mediaSet
             										and Language__c IN :prophetLanguageSet and Active__c=true];
             																		//Review: Check where conditions
             										
        for(HEP_Promotion_Dating__c prmDt: prmDtList)
    	{
    		List<EAW_Release_Date_Guideline__c>matchingRDGList = new List<EAW_Release_Date_Guideline__c>();
    		for(EAW_Release_Date_Guideline__c rdg: rdgList)
    		{
    			String pDtLanguage = (prmDt.Language__c!=null)?languageMap.get(prmDt.Language__r.Prophet_Language_Id__c).name:'No Specific Language';
    			Set<String> tempTerritoryList = null;
    			if(prmDt.Territory__r.Territory_Code_Integration__c=='AE'){
    				tempTerritoryList = middleEastTerritories;
    			}else{
    				tempTerritoryList = new Set<String>();
    				tempTerritoryList.add(territoryMap.get(prmDt.Territory__r.Territory_Code_Integration__c).name);
    			}
    			if(prmDt.Channel__c == rdg.EAW_Release_Date_Type__c 
    			&& finProdIdEtcMap.get(prmDt.HEP_Catalog__r.Title_EDM__r.FIN_PROD_ID__c)!=null //Review: Checking Title exists in Title Attribute object
    			&& finProdIdEtcMap.get(prmDt.HEP_Catalog__r.Title_EDM__r.FIN_PROD_ID__c).PROD_TYP_CD_INTL__r.Name == rdg.Product_Type__c
    				
    			 && pDtLanguage==rdg.Language__c
    			 //Review: Compare language using custom setting
    			 
    			 && tempTerritoryList.contains(rdg.Territory__c))
    			{
    																													
    				matchingRDGList.add(rdg);
    			}
    		}
    		matchingPrmDtRdgs.put(prmDt,matchingRDGList);
    	}
    	List<EAW_Release_Date__c> rdList = new List<EAW_Release_Date__c>();
    	for(HEP_Promotion_Dating__c tempPrmDt: prmDtList)
	   		{
	   			HEP_Promotion_Dating__c prmDt = matchingPrmDts.get(tempPrmDt)[0];//To DO: Check coditions to find Promotion date is active or inactive or deleted.
	   			//this is where updates happen. use both maps.
	   			for(EAW_Release_Date_Guideline__c rdgc:matchingPrmDtRdgs.get(prmDt)){
	   				EAW_Release_Date__c erdc;
	   				if(rdgc.Release_Dates__r!=null && !rdgc.Release_Dates__r.isempty()){
		   				for(EAW_Release_Date__c tempErdc:rdgc.Release_Dates__r){
		   					if(tempErdc.Title__r.FIN_PROD_ID__c==prmDt.HEP_Catalog__r.Title_EDM__r.FIN_PROD_ID__c){
		   						erdc=tempErdc;break;
		   					}
		   				}
	   				}
					if(erdc==null){//If new release date, create new one
			            erdc = new EAW_Release_Date__c();
			            erdc.Release_Date_Guideline__c=rdgc.id;
			             erdc.Active__C=true;
			        }
			        erdc.Feed_Date__c = prmDt.date__c;
		        	erdc.Feed_Date_Status__c= '';//To DO: Don't know how to handle
		        	//erdc.Locked_Status__c=prmDt.Locked_Status__c;//Current phase, there is no plan to implement Locked status
			        erdc.HEP_Promotion_Dating_Id__c=prmDt.id;
			        
			        if(erdc.Status__c!='Firm' && erdc.Status__c!='Not Released'){//Upsert release date if release date status is other than Firm or Not Released
			        	 if(erdc.Temp_Perm__c=='Permanent' && erdc.Manual_Date__c!=null){
			        		//erdc.Release_Date__c=erdc.Manual_Date__c; - Process builder will take care
			        	}else if(erdc.Feed_Date__c!=null){
			        		//erdc.Release_Date__c=erdc.Feed_Date__c; - Process builder will take care
			        		erdc.Status__c='Tentative';
			        	}else if(erdc.Temp_Perm__c=='Temporary' && erdc.Manual_Date__c!=null){
			        		//erdc.Release_Date__c=erdc.Manual_Date__c; - Process builder will take care
			        	}else if(erdc.Projected_Date__c!=null){
			        		//erdc.Release_Date__c=erdc.Projected_Date__c; - Process builder will take care
			        		erdc.Status__c='Estimated';
			        	}
			        }else{
			        	//No need to update release date
			        }
			        
			        rdList.add(erdc);
	   			}
	   		}
	   		if(!rdList.isempty())upsert rdList;
    }
    */
    public void handlePromotionDates(List<HEP_Promotion_Date_API.PromotionDate> prmDts,Map<String,Date> titleFadMap,
    					Map<String,Set<id>> productTypeRdgIds,Map<String,Set<id>> productTypeTitleIds,List<String> mainProductTypeList)
    {
    	
    	//system.debug('prmDts: '+system.JSON.serialize(prmDts));
    	//get custom setting ids for language, media and territory?
    	List<EAW_Territories__c> territorySettings = EAW_Territories__c.getall().values();
    	//system.debug('territorySettings: '+system.JSON.serialize(territorySettings));
    	List<EAW_Languages__c> languageSettings = EAW_Languages__c.getall().values();
    	//system.debug('languageSettings: '+system.JSON.serialize(languageSettings));
    	List<EAW_Media__c> mediaSettings = EAW_Media__c.getall().values();
    	//system.debug('mediaSettings: '+system.JSON.serialize(mediaSettings));
    	
    	Map<String,EAW_Territories__c> territoryMap = new Map<String,EAW_Territories__c>();
    	for(EAW_Territories__c eawtc:territorySettings){
    		if(eawtc.Territory_Code__c!=null){
	    		territoryMap.put(eawtc.Territory_Code__c,eawtc);
	    		if(middleEastTerritoryCodes.contains(eawtc.Territory_Code__c)){
	    			middleEastTerritories.add(eawtc.name);
	    		}
    		}
    	}
    	//system.debug('territoryMap: '+system.JSON.serialize(territoryMap));
    	//system.debug('middleEastTerritories: '+system.JSON.serialize(middleEastTerritories));
    	Map<String,EAW_Languages__c> languageMap = new Map<String,EAW_Languages__c>();
    	for(EAW_Languages__c eawlc:languageSettings)languageMap.put(String.valueof(eawlc.Language_Id__c),eawlc);
		//system.debug('languageMap: '+system.JSON.serialize(languageMap));
		//system.debug('prophetTerritorySet: '+system.JSON.serialize(prophetTerritorySet));
		//system.debug('prophetLanguageSet: '+system.JSON.serialize(prophetLanguageSet));
		//system.debug('finProductIdSet: '+system.JSON.serialize(finProductIdSet));
		//system.debug('mediaSet: '+system.JSON.serialize(mediaSet));
		
		//Collect few details individually to get required Title Attributes and RDG with RD
    	for(HEP_Promotion_Date_API.PromotionDate prmDt: prmDts)
    	{
			if(prmDt.territoryCode=='AE'){
				prophetTerritorySet.addall(middleEastTerritories);
			}else{
				prophetTerritorySet.add(territoryMap.get(prmDt.territoryCode).Name);
			}
			prophetLanguageSet.add((prmDt.languageId!=null && languageMap.get(prmDt.languageId)!=null)?languageMap.get(prmDt.languageId).name:'No Specific Language');
			finProductIdSet.add(prmDt.finTitleId);
			if(channelFormatMap.get(prmDt.channel)!=null)mediaSet.addall(channelFormatMap.get(prmDt.channel));
    	}
    	if(titleFadMap!=null && !titleFadMap.isempty()){
    		finProductIdSet.addAll(titleFadMap.keyset());
    	}
		//system.debug('prophetTerritorySet: '+system.JSON.serialize(prophetTerritorySet));
		//system.debug('prophetLanguageSet: '+system.JSON.serialize(prophetLanguageSet));
		//system.debug('finProductIdSet: '+system.JSON.serialize(finProductIdSet));
		//system.debug('mediaSet: '+system.JSON.serialize(mediaSet));
		
    	//for each promotion we get corresponding list of promotions that match with channel/language/territory/catalog in ascending order
    	Set<String> processedCombination = new Set<string>();//We don't need to process completed pmlt combination
    	for(HEP_Promotion_Date_API.PromotionDate prmDt: prmDts)
    	{
    		Set<HEP_Promotion_Date_API.PromotionDate>fPrmDtList = new Set<HEP_Promotion_Date_API.PromotionDate>();
    		if(!processedCombination.contains(uniquePromotionDate(prmDt))){//We don't need to process completed pmlt combination
	    		processedCombination.add(uniquePromotionDate(prmDt));
	    		for(HEP_Promotion_Date_API.PromotionDate fPrmDt: prmDts)
	    		{
	    			if(arePdsEqual(prmDt,fPrmDt))
	    			{
	    				if(groupingPrmDts.get(prmDt)!=null)fPrmDtList=groupingPrmDts.get(prmDt);
	    				fPrmDtList.add(fPrmDt);
	    			}
	    		}
	    		groupingPrmDts.put(prmDt,fPrmDtList);
    		}
    	}
    	//system.debug('processedCombination: '+system.JSON.serialize(processedCombination));
    	//system.debug('groupingPrmDts: '+system.JSON.serialize(groupingPrmDts));
    	//Review: To get product types of each title and we get titles which are exist in Title Attribute object
    	for(EAW_Title__c etc:[select id,FIN_PROD_ID__c,PROD_TYP_CD_INTL__c,PROD_TYP_CD_INTL__r.Name,FOX_ID__c from EAW_Title__c where FIN_PROD_ID__c in :finProductIdSet]){
    		productTypeSet.add(etc.PROD_TYP_CD_INTL__r.Name);
    		finProdIdEtcMap.put(etc.FIN_PROD_ID__c,etc);
    	}
    	//system.debug('productTypeSet: '+system.JSON.serialize(productTypeSet));
    	//system.debug('finProdIdEtcMap: '+system.JSON.serialize(finProdIdEtcMap));
    	List<EAW_Release_Date_Guideline__c> rdgList = [Select Id, Name,Language__c,Territory__c,Product_Type__c, EAW_Release_Date_Type__c,
            											(select Id,Feed_Date__c,Feed_Date_Status__c,Title__c,Release_Date_Guideline__c,Title__r.FIN_PROD_ID__c,Temp_Perm__c,Manual_Date__c,
            											Galileo_DML_Type__c,Locked_Status__c,HEP_Promotion_Dating_Id__c,Release_Date__c,Status__c,
            											Repo_DML_Type__c,Sent_To_Galileo__c,Sent_To_Repo__c,Send_To_Third_Party__c from Release_Dates__r where Title__r.FIN_PROD_ID__c IN:finProductIdSet)
             										From EAW_Release_Date_Guideline__c Where Territory__c IN:prophetTerritorySet and Product_Type__c IN:productTypeSet and EAW_Release_Date_Type__c IN:mediaSet
             										and Language__c IN :prophetLanguageSet and Active__c=true];
             																		//Review: Check where conditions
        system.debug('rdgList: '+system.JSON.serialize(rdgList));     										
        for(HEP_Promotion_Date_API.PromotionDate prmDt: groupingPrmDts.keyset())
    	{
    		Set<EAW_Release_Date_Guideline__c>matchingRDGList = new Set<EAW_Release_Date_Guideline__c>();
    		for(EAW_Release_Date_Guideline__c rdg: rdgList)
    		{
    			String pDtLanguage = (prmDt.languageId!=null && languageMap.get(prmDt.languageId)!=null)?languageMap.get(prmDt.languageId).name:'No Specific Language';
    			Set<String> tempTerritoryList = null;
    			if(prmDt.territoryCode=='AE'){
    				tempTerritoryList = middleEastTerritories;
    			}else{
    				tempTerritoryList = new Set<String>();
    				tempTerritoryList.add(territoryMap.get(prmDt.territoryCode).name);
    			}
    			if(channelFormatMap.get(prmDt.channel)!=null && channelFormatMap.get(prmDt.channel).contains(rdg.EAW_Release_Date_Type__c) 
    			&& finProdIdEtcMap.get(prmDt.finTitleId)!=null //Review: Checking Title exists in Title Attribute object
    			&& finProdIdEtcMap.get(prmDt.finTitleId).PROD_TYP_CD_INTL__r.Name == rdg.Product_Type__c
    			&& ProductTypeFromFoxSphere.contains(finProdIdEtcMap.get(prmDt.finTitleId).PROD_TYP_CD_INTL__r.Name.tolowercase())
    			&& pDtLanguage==rdg.Language__c
    			&& tempTerritoryList.contains(rdg.Territory__c))
    			{
    				matchingRDGList.add(rdg);
    			}
    		}
    		groupingPrmDtRdgs.put(prmDt,matchingRDGList);
    	}
    	//system.debug('groupingPrmDtRdgs: '+system.JSON.serialize(groupingPrmDtRdgs));
    	List<EAW_Release_Date__c> rdList = new List<EAW_Release_Date__c>();
    	for(HEP_Promotion_Date_API.PromotionDate prmDt: groupingPrmDts.keyset())
	   		{
	   			
	   			HEP_Promotion_Date_API.PromotionDate activeMinDateRecord;
				HEP_Promotion_Date_API.PromotionDate deletedDateRecord;
				Map<String,HEP_Promotion_Date_API.PromotionDate> activePds = new Map<String,HEP_Promotion_Date_API.PromotionDate>();
				Map<String,HEP_Promotion_Date_API.PromotionDate> deletedPds = new Map<String,HEP_Promotion_Date_API.PromotionDate>();
				//Aggregate/Explode records to get minimun active date 
				for(HEP_Promotion_Date_API.PromotionDate pd:groupingPrmDts.get(prmDt)){
					if(pd.recordStatus =='Active'){//condition to check promotion date is active
					   if(activeMinDateRecord!=null && activeMinDateRecord.releaseDate>pd.releaseDate  ){
					   	 activeMinDateRecord=pd;
					   	 activePds = new Map<String,HEP_Promotion_Date_API.PromotionDate>();
					   }else if(activeMinDateRecord!=null && activeMinDateRecord.releaseDate==pd.releaseDate  ){
					   	 //activePds.put(pd.recordId,pd);
					   }else if(activeMinDateRecord==null){
					   	 activeMinDateRecord=pd;
					   }
					   activePds.put(pd.recordId,pd);
		    		}else {//condition to check promotion date is (Inactive + Deleted)
		    		   deletedPds.put(pd.recordId,pd);
		    		}
				}
				//system.debug('activePds: '+system.JSON.serialize(activePds));
				//system.debug('activeMinDateRecord: '+system.JSON.serialize(activeMinDateRecord));
				//system.debug('deletedPds: '+system.JSON.serialize(deletedPds));
				//store recordId of earliest grouping 
				if(activePds!=null && !activePds.isempty()){
					List<String> rId = new List<String>(activePds.keyset());
					rId.sort();//values are arranged in Ascending order
					activeMinDateRecord=activePds.get(rId[0]);
				}
				if(deletedPds!=null && !deletedPds.isempty()){
					List<String> rId = new List<String>(deletedPds.keyset());
					rId.sort();//values are arranged in Ascending order
					deletedDateRecord=deletedPds.get(rId[0]);
				}
				//system.debug('activePds: '+system.JSON.serialize(activePds));
				//system.debug('activeMinDateRecord: '+system.JSON.serialize(activeMinDateRecord));
				//system.debug('deletedDateRecord: '+system.JSON.serialize(deletedDateRecord));
				boolean isActive = activeMinDateRecord!=null;//{"versionId":"6171104","territoryCode":"AU","status":"Confirmed","releaseDate":"2018-07-05","recordStatus":"Active","recordId":"aAu0v000000CgQmCAK","lockStatus":"Locked","languageId":"","languageCode":"","foxId":"6732541","finTitleId":"191065","channel":"RETAIL"}
				boolean isActiveNotReleasedRecord = (isActive)?activeMinDateRecord.status=='Not Released':false;//{"versionId":"6171104","territoryCode":"GB","status":"Not Released","releaseDate":null,"recordStatus":"Active","recordId":"aAu0v000000CgQeCAK","lockStatus":"Locked","languageId":"","languageCode":"","foxId":"6732541","finTitleId":"191065","channel":"RETAIL"}
				boolean isDelete = deletedDateRecord!=null;//{"versionId":"1920207","territoryCode":"GB","status":"Not Released","releaseDate":null,"recordStatus":"Inactive","recordId":"aAu0H0000004M4PSAU","lockStatus":"Locked","languageId":"","languageCode":"","foxId":"2432169","finTitleId":"031747","channel":"EST"}
				boolean isNew = false;
	   			
	   			//this is where updates happen. use both maps.
	   			for(EAW_Release_Date_Guideline__c rdgc:groupingPrmDtRdgs.get(prmDt)){
	   				EAW_Release_Date__c erdc;
	   				EAW_Release_Date__c tempErdc=new EAW_Release_Date__c();
	   				if(rdgc.Release_Dates__r!=null && !rdgc.Release_Dates__r.isempty()){
		   				for(EAW_Release_Date__c rd:rdgc.Release_Dates__r){
		   					if(rd.Title__r.FIN_PROD_ID__c==prmDt.finTitleId){
		   						erdc=rd;break;
		   					}
		   				}
	   				}
	   				//system.debug('erdc: '+system.JSON.serialize(erdc));
					if(erdc==null && isActive && !isActiveNotReleasedRecord){//If new release date, create new one
			            erdc = new EAW_Release_Date__c();
			            erdc.Release_Date_Guideline__c=rdgc.id;
			            erdc.Active__C=true;
			            erdc.Title__c=finProdIdEtcMap.get(prmDt.finTitleId).id;
			            erdc.Territory__c=rdgc.Territory__c;
			            erdc.Language__c=rdgc.Language__c;
			            erdc.EAW_Release_Date_Type__c=rdgc.EAW_Release_Date_Type__c;
			            erdc.Product_Type__c=rdgc.Product_Type__c;
			            erdc.Galileo_DML_Type__c = 'Insert';
			            erdc.Repo_DML_Type__c= 'Insert';
			            isNew = true;
			        }else if(erdc==null && (!isActive || isActiveNotReleasedRecord)){//No need to insert an inactive date
			        	continue;
			        }
			        
			        
			        tempErdc.Feed_Date__c = (isActive)?activeMinDateRecord.releaseDate:null;
		        	tempErdc.Feed_Date_Status__c= (isActive)?activeMinDateRecord.status:(isDelete)?deletedDateRecord.status:null;
		        	tempErdc.Locked_Status__c=(isActive)?activeMinDateRecord.lockStatus:(isDelete)?deletedDateRecord.lockStatus:null;//Current phase, there is no plan to implement Locked status
			        tempErdc.HEP_Promotion_Dating_Id__c=(isActive)?activeMinDateRecord.recordId:null;
			        
			        //Date is Firmed (For LIO - Tentative to Confirmed,For Sub - Projected or Tentative to Confirmed)
			        if('Confirmed'.equals(tempErdc.Feed_Date_Status__c)){
			        	tempErdc.Feed_Date_Status__c='Firm';
			        }
			        if(isNew || erdc.Feed_Date__c!=tempErdc.Feed_Date__c || erdc.Feed_Date_Status__c!=tempErdc.Feed_Date_Status__c 
        						||erdc.Locked_Status__c!=tempErdc.Locked_Status__c ||erdc.HEP_Promotion_Dating_Id__c!=tempErdc.HEP_Promotion_Dating_Id__c){
			        	erdc.Feed_Date__c = tempErdc.Feed_Date__c;
			        	erdc.Feed_Date_Status__c= tempErdc.Feed_Date_Status__c;
			        	erdc.Locked_Status__c=tempErdc.Locked_Status__c ;//Current phase, there is no plan to implement Locked status
				        erdc.HEP_Promotion_Dating_Id__c=tempErdc.HEP_Promotion_Dating_Id__c;
			        }else{
			        	continue;
			        }
			        
			        if(erdc.Sent_To_Galileo__c){
			        	erdc.Galileo_DML_Type__c = 'Update';
			        }
			        if(erdc.Sent_To_Repo__c){
			        	erdc.Repo_DML_Type__c= 'Update';
			        }
			        erdc.Send_To_Third_Party__c=true;
			        
			        if(erdc.Status__c!='Firm' && erdc.Status__c!='Not Released'){//Upsert release date if release date status is other than Firm or Not Released
			        	if('Not Released'.equalsignorecase(erdc.Feed_Date_Status__c)){
			        		erdc.Status__c='Estimated';
			        	}else if(erdc.Temp_Perm__c=='Permanent' && erdc.Manual_Date__c!=null){
			        	}else if(erdc.Feed_Date__c!=null){
			        		//if we receive a Feed date for Release date having temporary manual date then remove the manual date and update the Feed date. 
			        		erdc.Status__c='Tentative';
			        		if(erdc.Temp_Perm__c=='Temporary' && erdc.Manual_Date__c!=null){
			        			erdc.Temp_Perm__c='None';
			        			erdc.Manual_Date__c=null;
			        		}
			        	}else if(erdc.Temp_Perm__c=='Temporary' && erdc.Manual_Date__c!=null){
			        	}else if(erdc.Projected_Date__c!=null && erdc.Status__c==null){
			        		erdc.Status__c='Estimated';
			        	}
			        }else{
			        	//No need to update release date
			        	if(erdc.Feed_Date__c!=null){
			        		//if we receive a Feed date for Release date having temporary manual date then remove the manual date and update the Feed date. 
			        		if(erdc.Temp_Perm__c=='Temporary' && erdc.Manual_Date__c!=null){
			        			erdc.Temp_Perm__c='None';
			        			erdc.Manual_Date__c=null;
			        		}
			        	}
			       }
        			//system.debug('erdc: '+system.JSON.serialize(erdc));
        			
        			//Collect product types of modified Titles
        			if(!mainProductTypeList.contains(finProdIdEtcMap.get(prmDt.finTitleId).PROD_TYP_CD_INTL__r.Name))mainProductTypeList.add(finProdIdEtcMap.get(prmDt.finTitleId).PROD_TYP_CD_INTL__r.Name);
        			
        			//Collect Title IDs which are modified and group by product types
        			Set<id> tempPId = new Set<id>();
					if(productTypeTitleIds.get(finProdIdEtcMap.get(prmDt.finTitleId).PROD_TYP_CD_INTL__r.Name)!=null)tempPId=productTypeTitleIds.get(finProdIdEtcMap.get(prmDt.finTitleId).PROD_TYP_CD_INTL__r.Name);
					tempPId.add(finProdIdEtcMap.get(prmDt.finTitleId).id);
					productTypeTitleIds.put(finProdIdEtcMap.get(prmDt.finTitleId).PROD_TYP_CD_INTL__r.Name,tempPId);
			        //system.debug('productTypeTitleIds: '+system.JSON.serialize(productTypeTitleIds));
			        
			        //Collect RDG IDs which are modified and group by product types
			        Set<id> tempRdgId = new Set<id>();
			        if(productTypeRdgIds.get(finProdIdEtcMap.get(prmDt.finTitleId).PROD_TYP_CD_INTL__r.Name)!=null 
			        /*&& !productTypeRdgIds.get(finProdIdEtcMap.get(prmDt.finTitleId).PROD_TYP_CD_INTL__r.Name).contains(rdgc.id)*/){
   						tempRdgId = productTypeRdgIds.get(finProdIdEtcMap.get(prmDt.finTitleId).PROD_TYP_CD_INTL__r.Name);
   					}
   					tempRdgId.add(rdgc.id);
	   				productTypeRdgIds.put(finProdIdEtcMap.get(prmDt.finTitleId).PROD_TYP_CD_INTL__r.Name,tempRdgId);
	   				
			        rdList.add(erdc);
	   			}
	   		}
	   		//system.debug('rdList: '+system.JSON.serialize(rdList));
	   		if(titleFadMap!=null && !titleFadMap.isempty()){
	   			List<EAW_Release_Date_Guideline__c> fadRdgList = [Select Id, Name,Language__c,Territory__c,Product_Type__c, EAW_Release_Date_Type__c,
            											(select Id,Feed_Date__c,Feed_Date_Status__c,Title__c,Release_Date_Guideline__c,Title__r.FIN_PROD_ID__c,Galileo_DML_Type__c,Locked_Status__c,HEP_Promotion_Dating_Id__c,Release_Date__c,Status__c,
            											Repo_DML_Type__c,Sent_To_Galileo__c,Sent_To_Repo__c,Send_To_Third_Party__c from Release_Dates__r where Title__r.FIN_PROD_ID__c IN:finProductIdSet)
             										From EAW_Release_Date_Guideline__c Where EAW_Release_Date_Type__c='First Availability Date' and Territory__c='Worldwide' and Product_Type__c IN:productTypeSet and Active__c=true];
             	//system.debug('fadRdgList: '+system.JSON.serialize(fadRdgList));
             	if(fadRdgList!=null && !fadRdgList.isempty()){
             		for(String finProdId: titleFadMap.keyset()){
	         			for(EAW_Release_Date_Guideline__c fadRdg:fadRdgList){
	         				if(finProdIdEtcMap.get(finProdId)==null || fadRdg.Product_Type__c!=finProdIdEtcMap.get(finProdId).PROD_TYP_CD_INTL__r.Name){
	         					continue;
	         				}
		         			EAW_Release_Date__c erdc;
			   				EAW_Release_Date__c tempErdc=new EAW_Release_Date__c();
			   				boolean isNew = false;
			   				if(fadRdg.Release_Dates__r!=null && !fadRdg.Release_Dates__r.isempty()){
				   				for(EAW_Release_Date__c rd:fadRdg.Release_Dates__r){
				   					if(rd.Title__r.FIN_PROD_ID__c==finProdId){
				   						erdc=rd;break;
				   					}
				   				}
			   				}
			   				boolean isActive = titleFadMap.get(finProdId)!=null;
			   				//system.debug('erdc: '+system.JSON.serialize(erdc));
			   				if(erdc==null && isActive){//If new release date, create new one
					            erdc = new EAW_Release_Date__c();
					            erdc.Release_Date_Guideline__c=fadRdg.id;
					            erdc.Active__C=true;
					            erdc.Title__c=finProdIdEtcMap.get(finProdId).id;
					            erdc.Territory__c=fadRdg.Territory__c;
					            erdc.Language__c=fadRdg.Language__c;
					            erdc.EAW_Release_Date_Type__c=fadRdg.EAW_Release_Date_Type__c;
					            erdc.Product_Type__c=fadRdg.Product_Type__c;
					            erdc.Galileo_DML_Type__c = 'Insert';
					            erdc.Repo_DML_Type__c= 'Insert';
					            erdc.Status__c='Tentative';
					            isNew = true;
				        	}else if(erdc==null && !isActive){
				        		continue;//No need to insert an null date
				        	}
				        	if(isNew || erdc.Release_Date__c!=titleFadMap.get(finProdId)){
				        		erdc.Release_Date__c=titleFadMap.get(finProdId);
				        		erdc.Status__c='Tentative';
				        		if(erdc.Sent_To_Galileo__c){
						        	erdc.Galileo_DML_Type__c = 'Update';
						        }
						        if(erdc.Sent_To_Repo__c){
						        	erdc.Repo_DML_Type__c= 'Update';
						        }
						        erdc.Send_To_Third_Party__c=true;
						        
						        //Collect product types of modified Titles
						        if(!mainProductTypeList.contains(finProdIdEtcMap.get(finProdId).PROD_TYP_CD_INTL__r.Name))mainProductTypeList.add(finProdIdEtcMap.get(finProdId).PROD_TYP_CD_INTL__r.Name);
						        
						        //Collect Title IDs which are modified and group by product types
			        			Set<id> tempPId = new Set<id>();
								if(productTypeTitleIds.get(finProdIdEtcMap.get(finProdId).PROD_TYP_CD_INTL__r.Name)!=null)tempPId=productTypeTitleIds.get(finProdIdEtcMap.get(finProdId).PROD_TYP_CD_INTL__r.Name);
								tempPId.add(finProdIdEtcMap.get(finProdId).id);
								productTypeTitleIds.put(finProdIdEtcMap.get(finProdId).PROD_TYP_CD_INTL__r.Name,tempPId);
						        
						        //Collect RDG IDs which are modified and group by product types
						        Set<id> tempRdgId = new Set<id>();
						        if(productTypeRdgIds.get(finProdIdEtcMap.get(finProdId).PROD_TYP_CD_INTL__r.Name)!=null 
						        /*&& !productTypeRdgIds.get(finProdIdEtcMap.get(finProdId).PROD_TYP_CD_INTL__r.Name).contains(fadRdg.id)*/){
			   						tempRdgId = productTypeRdgIds.get(finProdIdEtcMap.get(finProdId).PROD_TYP_CD_INTL__r.Name);
			   					}
		   						tempRdgId.add(fadRdg.id);
			   					productTypeRdgIds.put(finProdIdEtcMap.get(finProdId).PROD_TYP_CD_INTL__r.Name,tempRdgId);
				        		rdList.add(erdc);
				        		
				        		//system.debug('erdc: '+system.JSON.serialize(erdc));
				        		//system.debug('productTypeTitleIds: '+system.JSON.serialize(productTypeTitleIds));
				        		//system.debug('productTypeTitleIds: '+system.JSON.serialize(productTypeTitleIds));
				        	}
		         		}
             		}
             	}									
	   		}
	   		//system.debug('rdList: '+system.JSON.serialize(rdList));
	   		if(rdList!=null && !rdList.isempty()){
	   			/*
	   			List<EAW_Rule__c> rules = [SELECT Release_Date_Guideline__c FROM EAW_Rule__c WHERE Release_Date_Guideline__c in :releaseDateGuidelineIds];
	   			system.debug('rules: '+system.JSON.serialize(rules));
	   			if(rules!=null && !rules.isempty()){
	   				Set<id> tempReleaseDateGuidelineIds = new Set<id>();
	   				for(EAW_Rule__c r:rules){
	   					//Check RDG which have rules
	   					tempReleaseDateGuidelineIds.add(r.Release_Date_Guideline__c);
	   				}
	   				//Remove set of RDG having rules from main RDG set, to get independent RDG set
	   				system.debug('tempReleaseDateGuidelineIds: '+system.JSON.serialize(tempReleaseDateGuidelineIds)); 
	   				releaseDateGuidelineIds.removeAll(tempReleaseDateGuidelineIds);
	   			}
	   			*/
	   			/*
	   			for(String productType:productTypeSet){
	   				Set<Id> tempRdgId = new Set<id>();
	   				for(Id rdgId:releaseDateGuidelineIds){
	   					if(productTypeRdgIds.get(productType)!=null && !productTypeRdgIds.get(productType).contains(rdgId)){
	   						tempRdgId = productTypeRdgIds.get(productType);
	   					}
   						tempRdgId.add(rdgId);
	   					productTypeRdgIds.put(productType,tempRdgId);
	   				}
	   				if(!mainProductTypeList.contains(productType))mainProductTypeList.add(productType);
	   				
	   			}
	   			*/
	   			//system.debug('productTypeRdgIds: '+system.JSON.serialize(productTypeRdgIds));
	   			//system.debug('mainProductTypeList: '+system.JSON.serialize(mainProductTypeList));
   				checkRecursiveData.bypassDateCalculation=true;//To bypass recursive calculation of downstream release dates
   				system.debug('rdList: '+system.JSON.serialize(rdList));
   				upsert rdList;//TO do
   			}
   		}
    
    private static boolean arePdsEqual(HEP_Promotion_Date_API.PromotionDate pd1,HEP_Promotion_Date_API.PromotionDate pd2){
		return pd1.finTitleId==pd2.finTitleId && pd1.channel==pd2.channel && pd1.languageCode==pd2.languageCode && pd1.territoryCode==pd2.territoryCode;
	}
	
	private static String uniquePromotionDate(HEP_Promotion_Date_API.PromotionDate pd){
		String languageCode=(String.isnotblank(pd.languageCode))?pd.languageCode:'';
		return pd.finTitleId+'-'+pd.channel+'-'+pd.territoryCode+'-'+languageCode;
	}
}