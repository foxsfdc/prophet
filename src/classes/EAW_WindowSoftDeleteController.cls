public class EAW_WindowSoftDeleteController{
   
    public EAW_Window__c window {get;set;}
    public List <EAW_Window__c > windowList {get;set;}
    public String windowListString {get;set;}
    public String windowId {get;set;}
   
    public EAW_WindowSoftDeleteController(ApexPages.StandardController controller) {
    
        windowList = new List <EAW_Window__c > ();     
        window = (EAW_Window__c )controller.getRecord();
       
        if(window != NULL){
        
            windowId = window.Id;
            windowList = [ SELECT Customers__c,Customer__c,EAW_Plan__c,EAW_Window_Guideline__c,EAW_Title_Attribute__c
                          FROM EAW_Window__c WHERE Id =: window.Id]; 
                     
            System.debug('windowList ::::::::'+windowList );
            //windowList.add(window);
            windowListString  = JSON.serialize(windowList);
        }
       
    }
}