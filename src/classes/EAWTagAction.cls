public with sharing class EAWTagAction {

    public List<EAW_Tag__c> tag{get; set;}
    public void export(){
        
        // Make the title
        String title = 'Selected_Tags_Export';
        
        // Add CSV Headers
        List<String> headers = new List<String>();
		headers.add('Tag Name');
        headers.add('Tag Type');
        headers.add('Is Active');
        
        
        // We export tags by making a new listview
    	List<EAW_Tag__c> selectedPlans = new List<EAW_Tag__c>();

    }
}