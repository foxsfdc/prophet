public class EAW_WindowRuleBuilderController {
    
    @AuraEnabled
    public static EAW_Window__c getWindowDetails(String recordId){
        EAW_Window__c window = [SELECT Id, Name,EAW_Window_Guideline__c,EAW_Window_Guideline__r.Name,EAW_Window_Guideline__r.Window_Guideline_Alias__c,Start_Date__c,End_Date__c,Outside_Date__c,Tracking_Date__c,
                                Start_Date_Rule__c,End_Date_Rule__c,Title_Name__c,Projected_Start_Date__c,Projected_End_Date__c,Projected_Outside_Date__c,Projected_Tracking_Date__c,
                                Manual_Start_Date_Overridden__c,Manual_End_Date_Overridden__c,Manual_Outside_Date_Overridden__c,Manual_Tracking_Date_Overridden__c FROM EAW_Window__c WHERE Id = :recordId];
        return window;
    }
    @AuraEnabled
    public static windowClass getWindowStrands(String windowId,List<String> fieldList){
        windowClass newwindowClass = new windowClass();
        EAW_Window__c currentWindow = getWindowDetails(windowId);
        newwindowClass.window = currentWindow;
        newWindowClass.windowStrandList = [SELECT Id,Name,Start_Date_Source__c,End_Date_Source__c,
                                           Start_Date_Days_Offset__c,Start_Date_Months_Offset__c,End_Date_Days_Offset__c,
                                          End_Date_Months_Offset__c,License_Type__c,Language__c,Territory__c,Media__c,EAW_Window_Guideline__c FROM EAW_Window_Guideline_Strand__c WHERE EAW_Window_Guideline__c = :currentWindow.EAW_Window_Guideline__c];
        newWindowClass.fieldWrapperSet = getFieldSet(fieldList);
        return newwindowClass;
    }
    @AuraEnabled
    public static void saveWindowStrands(String windowStrandList,List<String> windowStrandListToDelete){
        /*try{
            List<EAW_Window_Strand__c> windowStrands = (List<EAW_Window_Strand__c>)JSON.deserialize(windowStrandList,List<EAW_Window_Strand__c>.class);
            if(windowStrands != NULL){
                upsert windowStrands;
            }
            if(windowStrandListToDelete != NULL){
                List<EAW_Window_Strand__c> windowStrandToDelete = [SELECT Id,Name FROM EAW_Window_Strand__c WHERE Id IN :windowStrandListToDelete];
                if(windowStrandToDelete != null){
                    delete windowStrandToDelete;
                }
            }
        } catch(Exception e){
            throw New AuraHandledException(e.getMessage());
        }*/
    }
    @AuraEnabled
    public static void restoreOverriddenWindowStrands(String windowId){
        
        /*List<EAW_Window_Strand__c> windowStrandList = [SELECT Id,Is_Overridden__c,Window_Guideline_Strand__c,Window__c FROM EAW_Window_Strand__c WHERE Window__c = :windowId AND Is_Overridden__c = true ];
        try{
            system.debug(':::::'+windowStrandList);
            if(windowStrandList != null){
                Set<String> windowGuidelineStrandIdSet= new Set<String>();
                for(EAW_Window_Strand__c windowStrand : windowStrandList){
                    if(windowStrand.Window_Guideline_Strand__c != null){
                        windowGuidelineStrandIdSet.add(windowStrand.Window_Guideline_Strand__c);
                    }
                }
                delete windowStrandList;
                if(windowGuidelineStrandIdSet.size() > 0){
                    List<EAW_Window_Guideline_Strand__c> windowGuidelineStrands = [SELECT End_Date_Days_Offset__c, End_Date_From__c, End_Date_Months_Offset__c, Start_Date_Days_Offset__c, Start_Date_From__c, Start_Date_Months_Offset__c,
                        Territory__c, Language__c, Media__c, License_Type__c, EAW_Window_Guideline__c, Name FROM EAW_Window_Guideline_Strand__c WHERE Id IN: windowGuidelineStrandIdSet];
                    if(windowGuidelineStrands.size() > 0){
                        List<EAW_Window_Strand__c> windowStrandToInsert = new List<EAW_Window_Strand__c>();
                        for(EAW_Window_Guideline_Strand__c windowGuidelineStrand : windowGuidelineStrands){
                            windowStrandToInsert.add(createWindowStrand(windowId,windowGuidelineStrand));
                        }
                        insert windowStrandToInsert;
                    }
                }
            }
        }  catch(Exception e){
            throw New AuraHandledException(e.getMessage());
        }*/
    }
    /*private static EAW_Window_Strand__c createWindowStrand(Id windowId,EAW_Window_Guideline_Strand__c windowGuidelineStrand){
        EAW_Window_Strand__c windowStrand = new EAW_Window_Strand__c();
        windowStrand.Name = windowGuidelineStrand.Name;
        windowStrand.End_Date_Days_After__c = windowGuidelineStrand.End_Date_Days_Offset__c;
        windowStrand.End_Date_From__c = windowGuidelineStrand.End_Date_From__c;
        windowStrand.End_Date_Months_After__c = windowGuidelineStrand.End_Date_Months_Offset__c;
        windowStrand.Start_Date_Days_After__c = windowGuidelineStrand.Start_Date_Days_Offset__c;
        windowStrand.Start_Date_From__c = windowGuidelineStrand.Start_Date_From__c;
        windowStrand.Start_Date_Months_After__c = windowGuidelineStrand.Start_Date_Months_Offset__c;
        windowStrand.Territory__c = windowGuidelineStrand.Territory__c;
        windowStrand.Language__c = windowGuidelineStrand.Language__c;
        windowStrand.Media__c = windowGuidelineStrand.Media__c;
        windowStrand.License_Type__c = windowGuidelineStrand.License_Type__c;
        windowStrand.Window_Guideline_Strand__c = windowGuidelineStrand.Id;
        windowStrand.Window__c = windowId;
        return windowStrand;
    }*/
    public class windowClass {
        @AuraEnabled
        public EAW_Window__c window;
        
        @AuraEnabled
        public List<EAW_Window_Guideline_Strand__c> windowStrandList;
        
        @AuraEnabled
        public List<fieldSetWrapper> fieldWrapperSet;
    }
    public static List<fieldSetWrapper> getFieldSet(List<String> fieldList){
        
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get('EAW_Window_Guideline_Strand__c');//From the Object Api name retrieving the SObject
        Sobject Object_name = targetType.newSObject();
        Schema.sObjectType sobject_type = Object_name.getSObjectType(); //grab the sobject that was passed
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap();
        
        List<fieldSetWrapper> newfieldSetWrapper = new List<fieldSetWrapper>();
        for(Schema.SObjectField fieldName : field_map.values()){
            if(fieldList.contains(String.valueOf(fieldName))){
                String fieldAPI = String.valueOf(fieldName);
                
                String dataType = String.valueOf(field_map.get(fieldAPI).getDescribe().getType());
                String fieldLabel = field_map.get(fieldAPI).getDescribe().getLabel();
                fieldSetWrapper newWrapper = new fieldSetWrapper();
                
                if(dataType == 'PICKLIST' || dataType == 'MULTIPICKLIST'){
                    newWrapper.dataType = dataType;
                    newWrapper.fieldLabel = fieldLabel;
                    newWrapper.fieldName = fieldAPI;
                    newWrapper.isRequired = false;
                    List<Schema.PicklistEntry> ple = field_map.get(fieldAPI).getDescribe().getPicklistValues();
                    List<pickListWrapper> pickListValuesList = new List<pickListWrapper>();
                    for( Schema.PicklistEntry pickListVal : ple){
                        pickListWrapper newpickListWrapper = new pickListWrapper();
                        newpickListWrapper.label = String.valueOf(pickListVal.getLabel());
                        newpickListWrapper.value = String.valueOf(pickListVal.getValue());
                        pickListValuesList.add(newpickListWrapper);
                    }  
                    newWrapper.pickListValues = pickListValuesList;
                } else{
                    newWrapper.dataType = dataType;
                    newWrapper.fieldLabel = fieldLabel;
                    newWrapper.fieldName = fieldAPI;
                    newWrapper.isRequired = false;
                }
                newfieldSetWrapper.add(newWrapper);
            }
        }
        return newfieldSetWrapper;
    }
    public class fieldSetWrapper{
        @AuraEnabled
        public String fieldLabel;
        @AuraEnabled
        public String fieldName;
        @AuraEnabled
        public List<pickListWrapper> pickListValues;
        @AuraEnabled
        public Boolean isRequired;
        @AuraEnabled
        public String dataType;
    }
    public class pickListWrapper{
        @AuraEnabled
        public String label;
        @AuraEnabled
        public String value;
    }
}