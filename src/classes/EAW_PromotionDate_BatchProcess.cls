global class EAW_PromotionDate_BatchProcess implements Database.Batchable<sObject>,Database.Stateful,Schedulable {
	
	//static global boolean skipApiCallout = false;
	global Map<String,Set<id>> productTypeRdgIds = new Map<String,Set<id>>();
	global Map<String,Set<id>> productTypeTitleIds = new Map<String,Set<id>>();
	global List<String> mainProductTypeList = new List<String>(); 
	
	
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(
            'select FIN_PROD_ID__c, Source_System__c, Territory_Codes__c,Status__c from EAW_Inbound_Notifications__c where Source_System__c=\'foxchange\' and Status__c not in (\'Success\',\'Processing\')'
        );
    }
    global void execute(Database.BatchableContext bc, List<EAW_Inbound_Notifications__c> records){
        try{
        	//system.debug('records: '+system.json.serialize(records));
            if(records!=null && !records.isempty()){
		        Map<String, Set<String>> releaseKeys = new Map<String, Set<String>>();
		        List<HEP_Promotion_Date_API.PromotionDate> PromotionDates = new List<HEP_Promotion_Date_API.PromotionDate>();
		        Map<String,Date> titleFadMap = new Map<String,Date>();
		        
		        for(EAW_Inbound_Notifications__c eawIN:records){
		        	if(String.isnotblank(eawIN.Territory_Codes__c) && releaseKeys.get(eawIN.FIN_PROD_ID__c)!=null){
		        		Set<String> tCodes = releaseKeys.get(eawIN.FIN_PROD_ID__c);
		        		tCodes.addall(eawIN.Territory_Codes__c.split(';'));
		    			releaseKeys.put(eawIN.FIN_PROD_ID__c,tCodes);
		        	}else if(String.isnotblank(eawIN.Territory_Codes__c)){
		        		releaseKeys.put(eawIN.FIN_PROD_ID__c,new Set<String>(eawIN.Territory_Codes__c.split(';')));
		        	}
	                //eawIN.Status__c = 'Checking';
		        }
		        //update records;
		        //system.debug('releaseKeys: '+system.json.serialize(releaseKeys));
        		if(releaseKeys!=null && !releaseKeys.isempty()){
					for(String finProdId:releaseKeys.keyset()){
						
						List<HEP_Promotion_Date_API.PromotionDate> tempPromotionDates = EAW_PromotionDateController.getPromotionDates( finProdId, releaseKeys.get(finProdId) );
						//system.debug('tempPromotionDates: '+system.json.serialize(tempPromotionDates));
						Date faddate = EAW_PromotionDateController.getTitleFAD(finProdId);
						//system.debug('faddate: '+system.json.serialize(faddate));
						if(tempPromotionDates!=null && !tempPromotionDates.isEmpty())PromotionDates.addAll(tempPromotionDates);
						titleFadMap.put(finProdId,faddate);
					}
			    	
			    	//system.debug('PromotionDates: '+system.json.serialize(PromotionDates));
			    	//system.debug('titleFadMap: '+system.json.serialize(titleFadMap));
					
					if((PromotionDates!=null && !PromotionDates.isempty()) || (titleFadMap!=null && !titleFadMap.isempty())){
						for(EAW_Inbound_Notifications__c eawIN:records){
				    		eawIN.Status__c = 'Processing';
				    	}
				    	update records;
				    	EAW_HEP_PromotionDatingHandler ehPromotionDate = new EAW_HEP_PromotionDatingHandler();
				    	ehPromotionDate.handlePromotionDates(PromotionDates,titleFadMap,productTypeRdgIds,productTypeTitleIds,mainProductTypeList);
				    	for(EAW_Inbound_Notifications__c eawIN:records){
				    		eawIN.Status__c = 'Success';
				    	}
			    		update records;
        			}
        		}
    		}
        }catch(Exception e){
            String message = e.getMessage();
    		System.debug('e.getMessage(): '+e.getMessage());
    		if(message!=null && message.length()>10000)message=message.substring(0, 10000);
    		if(records!=null && !records.isempty()){
	            for(EAW_Inbound_Notifications__c eawIN:records){
	            	eawIN.Status__c = 'Failed';
	               // eawIN.Message__c=message;
	            }
	            update records;
        	}
        }
    	
    }    
    global void finish(Database.BatchableContext bc){
    	//system.debug('mainProductTypeList: '+system.json.serialize(mainProductTypeList));
    	//system.debug('productTypeRdgIds: '+system.json.serialize(productTypeRdgIds));
    	//system.debug('productTypeTitleIds: '+system.json.serialize(productTypeTitleIds));
    	
    	if((mainProductTypeList!=null && !mainProductTypeList.isempty()) && (productTypeRdgIds!=null && !productTypeRdgIds.isempty()) && (productTypeTitleIds!=null && !productTypeTitleIds.isempty())){
    		String productType = mainProductTypeList[0];
    		Set<id> rgdIds = productTypeRdgIds.get(productType);
    		Set<id> tIds = productTypeTitleIds.get(productType);
    		//system.debug('rgdIds: '+system.json.serialize(rgdIds));
    		//system.debug('tIds: '+system.json.serialize(tIds));
    		//system.debug('productType: '+system.json.serialize(productType));
    		
    		//To do Call the downstream batch class
    		if(rgdIds!=null && !rgdIds.isempty() && tIds!=null && !tIds.isempty()){
	    		EAW_RDGDateCalculationBatchClass releaseDateBatch = new EAW_RDGDateCalculationBatchClass(rgdIds);
	            releaseDateBatch.titleIdSet = tIds;
	            releaseDateBatch.windowGuidelineIdSet = NULL;
	            ID batchprocessid = Database.executeBatch(releaseDateBatch, 200);
	    		system.debug('batchprocessid: '+batchprocessid);
    		}
    		mainProductTypeList.remove(0);
    		productTypeRdgIds.remove(productType);
    		productTypeTitleIds.remove(productType);
    		
    		if((mainProductTypeList!=null && !mainProductTypeList.isempty()) && (productTypeRdgIds!=null && !productTypeRdgIds.isempty()) && (productTypeTitleIds!=null && !productTypeTitleIds.isempty())){
	    		EAW_PromotionDate_BatchProcess epb = new EAW_PromotionDate_BatchProcess();
	    		epb.mainProductTypeList=mainProductTypeList;
	    		epb.productTypeRdgIds=productTypeRdgIds;
	    		epb.productTypeTitleIds=productTypeTitleIds;
	    		Id childBatchJobId = Database.executeBatch(epb, 10);
	    		system.debug('childBatchJobId: '+childBatchJobId);
    		}
    	}
    	//To do: skip api callout of Release date
        //system.debug('finish skipApiCallout : '+skipApiCallout);*/
    }
    
    
    public void execute(SchedulableContext sc) {
        
        EAW_PromotionDate_BatchProcess pdBatch = new EAW_PromotionDate_BatchProcess();
        Database.executeBatch(pdBatch,10);//size should be ten, since we may hit too many dml rows exception,apex heap size
    }     
}