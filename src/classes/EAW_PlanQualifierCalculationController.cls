public class EAW_PlanQualifierCalculationController {
    
    public  Boolean arithmeticIntegerConditionalChecking(Integer operandValue, String conditionOperator, Integer conditionValue){
        
        Boolean conditionCheckStatus = false;
        
        if( conditionOperator.equals('<') ){
            return operandValue < conditionValue;
        } else if( conditionOperator.equals('<=') ) {
            return operandValue <= conditionValue;
        } else if( conditionOperator.equals('>') ){
            return operandValue > conditionValue;
        } else if( conditionOperator.equals('>=') ) {
            return operandValue >= conditionValue;
        } else if( conditionOperator.equals('=') ){
            return operandValue == conditionValue;
        }
        
        return conditionCheckStatus;
    }
    
    public  Boolean arithmeticDecimalConditionalChecking(Decimal operandValue, String conditionOperator, Decimal conditionValue){
        
        system.debug(':::operandValue:::'+operandValue);
        system.debug(':::conditionOperator:::'+conditionOperator);
        system.debug(':::conditionValue:::'+conditionValue);
        
        Boolean conditionCheckStatus = false;
        
        if( conditionOperator.equals('<') ){
            return operandValue < conditionValue;
        } else if( conditionOperator.equals('<=') ) {
            return operandValue <= conditionValue;
        } else if( conditionOperator.equals('>') ){
            return operandValue > conditionValue;
        } else if( conditionOperator.equals('>=') ) {
            return operandValue >= conditionValue;
        } else if( conditionOperator.equals('=') ){
            return operandValue == conditionValue;
        }
        system.debug(':::conditionCheckStatus:::'+conditionCheckStatus);
        return conditionCheckStatus;
    }
    
    public  Boolean arithmeticDateConditionalChecking(Date operandValue, String conditionOperator, Date conditionValue){
        
        system.debug(':::operandValue:::'+operandValue);
        system.debug(':::conditionOperator:::'+conditionOperator);
        system.debug(':::conditionValue:::'+conditionValue);
        
        Boolean conditionCheckStatus = false;
        
        if( conditionOperator.equals('<') ){
            return operandValue < conditionValue;
        } else if( conditionOperator.equals('<=') ) {
            return operandValue <= conditionValue;
        } else if( conditionOperator.equals('>') ){
            return operandValue > conditionValue;
        } else if( conditionOperator.equals('>=') ) {
            return operandValue >= conditionValue;
        } else if( conditionOperator.equals('=') ){
            return operandValue == conditionValue;
        }
        system.debug(':::conditionCheckStatus:::'+conditionCheckStatus);
        return conditionCheckStatus;
    }
}