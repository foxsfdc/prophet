/*
Utility class for outbound to have common methonds
*/
public with sharing class EAW_Outbound_Utility {
	
	
	
	//{Window Guideline,Window Guideline Strand,Release Date Guideline,Release Date,Window,Release date audit information,Window audit information}
	public enum OutboundDataType {WindowGuideline,WindowGuidelineStrands,ReleaseDateGuideline,ReleaseDate,Window,ReleaseDateAuditInformation,WindowAuditInformation}
	public OutboundDataType odataType;
	public enum OutboundSystem {Repo,Galileo,GalileoRightsCheck}
	public OutboundSystem oSystem;
	public List<id> currentApiIds;
	
	public static Integer MAX_API_RECORDS_LIMIT=200;
	
	public Rest_Endpoint_Settings__c outboundAuthenticationDetails;
	
	private EAW_Outbound_Utility(){
		//To use this class, OutboundDataType,OutboundSystem and currentApiIds are mandatory.
	}
	
	public EAW_Outbound_Utility(OutboundDataType odataType,OutboundSystem oSystem,List<id> currentApiIds){
		this.odataType=odataType;
		this.oSystem=oSystem;
		this.currentApiIds=currentApiIds;
		outboundAuthenticationDetails = Rest_Endpoint_Settings__c.getInstance('Outbound OAuth Details');
	}
	
	public String SetupAuthendicateAndGetAccessToken(){
		system.debug('outboundAuthenticationDetails: '+json.serialize(outboundAuthenticationDetails));
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setEndpoint(outboundAuthenticationDetails.End_Point__c);
       // req.setHeader('Host','ms-devapi.foxinc.com');
        req.setHeader('Content-type', 'application/x-www-form-urlencoded');
        req.setTimeout(60*1000);//60 secs
        String requestBody = 'grant_type=' + outboundAuthenticationDetails.Grant_Type__c +'&client_id=' + outboundAuthenticationDetails.Client_Id__c + '&client_secret=' + outboundAuthenticationDetails.Client_Secret__c;  
        req.setBody(requestBody);
        //System.debug('req::::'+req);
        Http http = new Http();
        HTTPResponse res = http.send(req);
        //System.debug('::::::::get:::::::'+res.getBody());
        //System.debug('>::::>'+res);
        
        if(res.getStatus() == 'Ok' && res.getStatusCode() == 200) {
        
            AccessTokenResponseWrapper atRes = (AccessTokenResponseWrapper) System.JSON.deserialize( res.getBody(), AccessTokenResponseWrapper.class);
            String accessToken =atRes.access_token;
            System.debug('::EAW_Outbound_Utility accessToken:: '+accessToken);
            return accessToken;
        }
        return null;
        
    }
    
    public class AccessTokenResponseWrapper {
        public String access_token;
        public String token_type;
        public String expires_in;
    }
	
    public static String getFormattedDate(Date d){
    	//m/d/yyyy
    	if(d!=null)return d.month()+'/'+d.day()+'/'+d.year();
    	else return null;
    }
    
    public static String getFormattedDateTime(DateTime dt){
    	if(dt!=null)return dt.format('dd/MM/yyyy hh:mm:ss');
		else return null;
    }
    
    public static void sendDataThroughWebAPI(String requestBody,String url){
    	Http http = new Http();
		HttpRequest request = new HttpRequest();
		request.setEndpoint(url);
		request.setMethod('POST');
		request.setBody(requestBody);
		request.setHeader('Content-type', 'application/x-www-form-urlencoded');
        request.setTimeout(60*1000);//60 secs
		HttpResponse response = http.send(request);
		// If the request is successful, parse the JSON response.
		if (response.getStatus() == 'Ok' && response.getStatusCode() == 200) {
		    System.debug('Success');
		}
    }
    
    public void sendDataThroughWebAPI(String requestBody,String url,List<EAW_Error_Log__c> errorLogs,set<id> successIds){
    	try{
    		String accessToken = SetupAuthendicateAndGetAccessToken(); 
            
	    	Http http = new Http();
			HttpRequest request = new HttpRequest();
			request.setEndpoint(url);
			request.setMethod('POST');
			request.setBody(requestBody);
			request.setHeader('Content-type', 'application/x-www-form-urlencoded');
	        request.setTimeout(60*1000);//60 secs
	        
	        request.setHeader('Authorization', 'Bearer '+accessToken);
	        
	        system.debug('requestBody: '+requestBody);
	        system.debug('request: '+request);
	        system.debug('request.getHeader(Authorization): '+request.getHeader('Authorization'));
			HttpResponse response = http.send(request);
			// If the request is successful, parse the JSON response.
			if (response.getStatus() == 'Ok' && response.getStatusCode() == 200) {
			    System.debug('Success');
			    successIds.addall(currentApiIds);
			}else{
				postApiCall(errorLogs,response.getStatusCode()+'',response.getBody());
			}
    	}catch(exception e){
    		postApiCall(errorLogs,null,e.getMessage());
    	}
    }
    
    private void postApiCall(List<EAW_Error_Log__c> errorLogs,String errorCode,String errorMessage){
        System.debug('errorMessage: '+errorMessage);
        if(errorMessage!=null && errorMessage.length()>250)errorMessage=errorMessage.substring(0, 250);
		if(errorLogs != null){
			errorLogs.add(new EAW_Error_Log__c(Error_Code__c=errorCode,Error_Message__c=errorMessage,Record_Type__c=odataType.name()
						,System__c=oSystem.name(),Failed_Record_Ids__c=String.join(currentApiIds,';')));
		}
    }
    
    public static String getTerritoryIds(String territoryNames){
    	List<Integer> TerritoryIds;
    	if(String.isnotblank(territoryNames)){
    		Map<String,EAW_Territories__c> territoryMap = EAW_Territories__c.getAll();
    		TerritoryIds = new List<integer>();
    		for(String tName:territoryNames.split(';')){
    			if(territoryMap.get(tName)!=null){
    				TerritoryIds.add(territoryMap.get(tName).Territory_Id__c.intValue());
    			}
    		}
    	}else return null;
    	if(TerritoryIds!=null && !TerritoryIds.isempty()){
    		return String.join(TerritoryIds,';');
    	}
    	return null;
    }
    
    public static String getTerritoryIds(Map<String,EAW_Territories__c> territoryMap,String territoryNames){
    	List<Integer> TerritoryIds;
    	if(String.isnotblank(territoryNames)){
    		TerritoryIds = new List<integer>();
    		for(String tName:territoryNames.split(';')){
    			if(territoryMap.get(tName)!=null){
    				TerritoryIds.add(territoryMap.get(tName).Territory_Id__c.intValue());
    			}
    		}
    	}else return null;
    	if(TerritoryIds!=null && !TerritoryIds.isempty()){
    		if(TerritoryIds.size()==1){
    			return String.valueOf(TerritoryIds[0]);
    		}
    		return String.join(TerritoryIds,';');
    	}
    	return null;
    }
    
    public static String getLanguageIds(String languageNames){
    	List<Integer> LanguageIds;
    	if(String.isnotblank(LanguageNames)){
    		Map<String,EAW_Languages__c> languageMap = EAW_Languages__c.getAll();
    		LanguageIds = new List<integer>();
    		for(String tName:LanguageNames.split(';')){
    			if(LanguageMap.get(tName)!=null){
    				LanguageIds.add(LanguageMap.get(tName).Language_Id__c.intValue());
    			}
    		}
    	}else return null;
    	if(LanguageIds!=null && !LanguageIds.isempty()){
    		return String.join(LanguageIds,';');
    	}
    	return null;
    }
    
    public static String getLanguageIds(Map<String,EAW_Languages__c> languageMap,String languageNames){
    	List<Integer> LanguageIds;
    	if(String.isnotblank(LanguageNames)){
    		LanguageIds = new List<integer>();
    		for(String tName:LanguageNames.split(';')){
    			if(LanguageMap.get(tName)!=null){
    				LanguageIds.add(LanguageMap.get(tName).Language_Id__c.intValue());
    			}
    		}
    	}else return null;
    	if(LanguageIds!=null && !LanguageIds.isempty()){
    		if(LanguageIds.size()==1){
    			return String.valueOf(LanguageIds[0]);
    		}
    		return String.join(LanguageIds,';');
    	}
    	return null;
    }
    
    public static String getMediaIds(Map<String,EAW_Media__c> mediaMap,String mediaNames){
    	List<Integer> mediaIds;
    	if(String.isnotblank(mediaNames)){
    		mediaIds = new List<integer>();
    		for(String mName:mediaNames.split(';')){
    			if(mediaMap.get(mName)!=null){
    				mediaIds.add(mediaMap.get(mName).media_Id__c.intValue());
    			}
    		}
    	}else return null;
    	if(mediaIds!=null && !mediaIds.isempty()){
    		if(mediaIds.size()==1){
    			return String.valueOf(mediaIds[0]);
    		}
    		return String.join(mediaIds,';');
    	}
    	return null;
    }
    
    
    public class AggregationModel{
    	public Id recordId;
    	public boolean sentToRepo=false;
    	public boolean sentToGalileo=false;
    	public boolean sendToThirdParty=false;
    	public boolean canBeDeleted=false;
    	public boolean setRepoDmlTypeToBlank=false;
    	public boolean setGalileoDmlTypeToBlank=false;
    }
    
    public Map<Id,AggregationModel> aggregateAll(set<id> allIds,set<id> repoSuccessIds,Set<id> repoIgnoredIds,set<id> galileoSuccessIds,Set<id> galileoIgnoredIds){
    	Map<Id,AggregationModel> aggregationModelMap = new Map<Id,AggregationModel>();
    	if(repoSuccessIds==null)repoSuccessIds=new set<id>();
    	if(repoIgnoredIds==null)repoIgnoredIds=new set<id>();
    	if(galileoSuccessIds==null)galileoSuccessIds=new set<id>();
    	if(galileoIgnoredIds==null)galileoIgnoredIds=new set<id>();
    	if(allIds!=null && !allIds.isempty()){
    		//if response 200 then Send_To_Third_Party - False, Dml_Type as blank
			//Sent_To_Repo-true
			//Sent_To_Galileo-true
			for(Id recordId:allIds){
				AggregationModel am = new AggregationModel();
				am.recordId=recordId;	
				if((repoSuccessIds.contains(recordId) && galileoSuccessIds.contains(recordId)) //Both are success
				|| (repoSuccessIds.contains(recordId) && galileoIgnoredIds.contains(recordId)) // repo is success and galileo is failed even one time 
					|| (galileoSuccessIds.contains(recordId) && repoIgnoredIds.contains(recordId)) // galileo is success and repo is failed even one time 
					|| (repoIgnoredIds.contains(recordId) && galileoIgnoredIds.contains(recordId))//If record is inactive and didn't send to both api then we can delete without sending them
					){
					am.sendToThirdParty=false;
					am.canBeDeleted = true; // Can be deleted
				}
				if(repoSuccessIds.contains(recordId)){//Repo call is success then repo DML type is blank and sent to repo should be true
					am.sentToRepo=true;
					am.setRepoDmlTypeToBlank=true;
				}
				if(galileoSuccessIds.contains(recordId)){//Galileo call is success then Galileo DML type is blank and sent to Galileo should be true
					am.sentToGalileo=true;
					am.setGalileoDmlTypeToBlank=true;
				}
				/*if(repoIgnoredIds.contains(recordId)){//If record is inactive and didn't send to repo even one time we can delete without sending them
					rd.sentToRepo=true;
					rd.Repo_DML_Type__c='';
				}
				if(galileoIgnoredIds.contains(recordId)){//If record is inactive and didn't send to Galileo even one time we can delete without sending them
					rd.sentToGalileo=true;
					rd.Repo_DML_Type__c='';
				}*/
				aggregationModelMap.put(recordId,am);
			}
    	}
    	
		return aggregationModelMap;
    }
}