public with sharing class EAW_RulesHandler 
{
    public List<EAW_Rule__c> releaseDateRules = new List<EAW_Rule__c>();
    public void EAW_RulesHandler()
    {
        
    }
    
    public void handleRuleRecordDelete( List<EAW_Rule__c> oldRules ) {
        
        Map<Id,Schema.RecordTypeInfo> rtMap = EAW_Rule__c.sobjectType.getDescribe().getRecordTypeInfosById();
        System.debug('::: rtMap :::'+rtMap);
        
        Set<Id> parentRecordsIdSet = new Set<Id>();
        
        if( oldRules != NULL && oldRules.size() > 0 ){
            
            for( EAW_Rule__c rule : oldRules ){
                
                String recTypeName = rtMap.get(rule.RecordTypeId).getName();
                System.debug('::: recTypeName :::'+recTypeName);
                
                if( rtMap != NULL &&  recTypeName == 'Release Date Calculation' ){
                    if( rule.Release_Date_Guideline__c != NULL ) parentRecordsIdSet.add(rule.Release_Date_Guideline__c);
                }
            }
            System.debug('::: parentRecordsIdSet :::'+parentRecordsIdSet);
            
            /*if( parentRecordsIdSet != NULL && parentRecordsIdSet.size() == 1 ){
                List<EAW_Release_Date__c> releaseDateToDeleteList = new List<EAW_Release_Date__c>();
                Set<Id> rdgIdSet = parentRecordsIdSet;
                Set<Id> titleIdSet = new Set<Id>();
                for( EAW_Release_Date__c releaseDate : [Select Id, Release_Date__c, Title__c,Projected_Date__c,Status__c,Feed_Date__c ,Manual_Date__c From EAW_Release_Date__c where (Status__c != 'FIRM' OR (Projected_Date__c != null AND Feed_Date__c = null AND Manual_Date__c = null))]){
                    titleIdSet.add(releaseDate.Title__c);
                    releaseDateToDeleteList.add(releaseDate);
                }
                System.debug('::: releaseDateToDeleteList :::'+releaseDateToDeleteList);
                if( releaseDateToDeleteList != NULL && releaseDateToDeleteList.size() > 0 ){
                    checkRecursiveData.bypassDateCalculation = TRUE;
                    delete releaseDateToDeleteList;
                }
                
                EAW_DownstreamRuleGuidelinesFindUtil dependentGuidelineFindUtil = new EAW_DownstreamRuleGuidelinesFindUtil();
                Set<Id> releaseDateGuidelineIdSet = dependentGuidelineFindUtil.findDependentReleaseDateGuidelineToProcess(rdgIdSet);
                
                if( releaseDateGuidelineIdSet != NULL && releaseDateGuidelineIdSet.size() > 0 ) {
                    EAW_RDGDateCalculationBatchClass releaseDateBatch = new EAW_RDGDateCalculationBatchClass(releaseDateGuidelineIdSet);
                    releaseDateBatch.titleIdSet = titleIdSet;
                    releaseDateBatch.windowGuidelineIdSet = NULL;
                    ID batchprocessid = Database.executeBatch(releaseDateBatch, 200);
                }
            }*/
            if( parentRecordsIdSet != NULL && parentRecordsIdSet.size() > 0){
                EAW_BatchReleaseDateSoftDelete softDeleteRDBatch = new EAW_BatchReleaseDateSoftDelete();
                softDeleteRDBatch.inActiveGuideLineIds = parentRecordsIdSet;
                softDeleteRDBatch.callDownstreamCalcBatchLogic = true;
                softDeleteRDBatch.query = 'Select Id, Release_Date__c, Title__c,Projected_Date__c,Status__c,Feed_Date__c ,Manual_Date__c,Release_Date_Guideline__r.Active__c From EAW_Release_Date__c WHERE Release_Date_Guideline__c IN: inActiveGuideLineIds AND Active__c=True AND ((Status__c != \'FIRM\' AND Status__c != \'Tentative\' AND Status__c != \'Estimated\') OR (Projected_Date__c != null AND Feed_Date__c = null AND Manual_Date__c = null))';
                ID batchprocessid = Database.executeBatch(softDeleteRDBatch, 200);
            }
        }
        
    }
   
}