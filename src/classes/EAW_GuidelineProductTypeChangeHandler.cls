public class EAW_GuidelineProductTypeChangeHandler {
    
    public static Map<String,String> updateRDGRulesBasedOnProductType(Map<Id,List<String>> oldConditionalNameMap,Map<Id,String> oldProductTypeMap,Map<Id,String> newProductTypeMap,Boolean isRDG,Map<Id,string> clonedGuidelineMap){
        List<String> newGuidelineProductTypeList = new List<String>();
        Map<String,String> rdgToUpdate = new Map<String,String>(); //  To Update All rules field
        Map<String,String> wgToUpdate = new Map<String,String>(); // To update rule text field
        Map<Id,EAW_Rule__c> ruleMap = new Map<Id,EAW_Rule__c>();
        Map<Id,EAW_Rule_Detail__c> allRuleDetailMap = new Map<Id,EAW_Rule_Detail__c>();
        
        if(oldConditionalNameMap != null && oldConditionalNameMap.keySet().size() > 0){
            for(Id guidelineId : oldConditionalNameMap.keySet()){
                if(newProductTypeMap.containsKey(guidelineId) && oldProductTypeMap.containsKey(guidelineId)){
                    for(String oldName : oldConditionalNameMap.get(guidelineId)){
                        String newName = oldName.replace(oldProductTypeMap.get(guidelineId),newProductTypeMap.get(guidelineId));
                        newGuidelineProductTypeList.add(newName);
                    }
                }
            }
            if(newGuidelineProductTypeList != null && newGuidelineProductTypeList.size() > 0){
                system.debug(':::::newGuidelineProductTypeList::::'+newGuidelineProductTypeList);
                List<EAW_Rule__c> ruleList = getGuidelineRules(oldConditionalNameMap.keySet(),isRDG);
                Map<Id,Set<Id>> ruleDetailMap = new Map<Id,Set<Id>>();
                if(ruleList != null && ruleList.size() > 0){
                    for(EAW_Rule__c rule : ruleList){
                        ruleMap.put(rule.Id,rule);
                        if(rule.Rule_Orders__r != null && rule.Rule_Orders__r.size() > 0){
                            Set<Id> ruleDetailSet = new Set<Id>();
                            if(ruleDetailMap.containsKey(rule.Id)){
                                ruleDetailSet = ruleDetailMap.get(rule.Id);
                            }
                            for(EAW_Rule_Detail__c ruleDetail : rule.Rule_Orders__r){
                                allRuleDetailMap.put(ruleDetail.Id,ruleDetail);
                                ruleDetailSet.add(ruleDetail.Id);
                            }
                            ruleDetailMap.put(rule.Id,ruleDetailSet);
                        }
                    }
                }
                // Need to built map for Rule delete when there is no ruledetail under it.
                Map<String,EAW_Release_Date_Guideline__c> rdgNameMap = new Map<String,EAW_Release_Date_Guideline__c>();
                for(EAW_Release_Date_Guideline__c rdg :  [SELECT Id,Name,Active__c FROM EAW_Release_Date_Guideline__c WHERE Name IN :newGuidelineProductTypeList AND Active__c = true]){
                    if(rdg.Name != null){
                        rdgNameMap.put(String.valueOf(rdg.Name),rdg);
                    }
                }
                Map<String,EAW_Window_Guideline__c> wdgMap = new Map<String,EAW_Window_Guideline__c>();
                for(EAW_Window_Guideline__c wg :  [SELECT Id,Name,Status__c FROM EAW_Window_Guideline__c WHERE Name IN :newGuidelineProductTypeList AND Status__c = 'Active']){
                    if(wg.Name != null){
                        wdgMap.put(String.valueOf(wg.Name),wg);
                    }
                }
                List<EAW_Rule__c> ruleListToUpdate = new List<EAW_Rule__c>();
                List<EAW_Rule_Detail__c> ruleDetailListToUpdate = new List<EAW_Rule_Detail__c>();
                List<EAW_Rule_Detail__c> ruleDetailToDelete = new List<EAW_Rule_Detail__c>();
                
                if(ruleList != null && ruleList.size() > 0){
                    for(EAW_Rule__c rule : ruleList){
                        String guidelineId = '';
                        if(isRDG){
                            guidelineId = rule.Release_Date_Guideline__c;
                        } else{
                            guidelineId = rule.Window_Guideline__c;
                        }
                        if(rule.Condition_Operand_Details__c != null && rule.Conditional_Operand_Id__c != null){
                            String oldOperandValue = rule.Condition_Operand_Details__c;
                            rule = setRuleConditionalOperands(rule,oldProductTypeMap,newProductTypeMap,rdgNameMap,wdgMap,guidelineId,isRDG);
                            ruleListToUpdate.add(rule);
                            String newOperandValue = rule.Condition_Operand_Details__c;
                            if(isRDG){
                                if(rule.Release_Date_Guideline__r.All_Rules__c != null){
                                    RDGRuleUpdate(rule.Release_Date_Guideline__r.All_Rules__c,guidelineId,oldOperandValue,newOperandValue,rdgToUpdate,null);
                                }
                            } else{
                                if(rule.Date_To_Modify__c != null){
                                    if(rule.Date_To_Modify__c == 'Start Date' && rule.Window_Guideline__r.Start_Date_Rule__c != null){
                                        wgRuleUpdate(rule.Window_Guideline__r.Start_Date_Rule__c,rule.Date_To_Modify__c,guidelineId,oldOperandValue,newOperandValue,wgToUpdate,null);
                                    } else if(rule.Date_To_Modify__c == 'End Date' && rule.Window_Guideline__r.End_Date_Rule__c != null){
                                        wgRuleUpdate(rule.Window_Guideline__r.End_Date_Rule__c,rule.Date_To_Modify__c,guidelineId,oldOperandValue,newOperandValue,wgToUpdate,null);
                                    } else if(rule.Date_To_Modify__c == 'Outside Date' && rule.Window_Guideline__r.Outside_Date_Rule__c != null){
                                        wgRuleUpdate(rule.Window_Guideline__r.Outside_Date_Rule__c,rule.Date_To_Modify__c,guidelineId,oldOperandValue,newOperandValue,wgToUpdate,null);
                                    } else if(rule.Date_To_Modify__c == 'Tracking Date' && rule.Window_Guideline__r.Tracking_Date_Rule__c != null){
                                        wgRuleUpdate(rule.Window_Guideline__r.Tracking_Date_Rule__c,rule.Date_To_Modify__c,guidelineId,oldOperandValue,newOperandValue,wgToUpdate,null);
                                    }
                                }
                            }
                        }
                        if(rule.Rule_Orders__r != null && rule.Rule_Orders__r.size() > 0){
                            for(EAW_Rule_Detail__c ruleDetail : rule.Rule_Orders__r){
                                if(ruleDetail.Condition_Operand_Details__c != null && ruleDetail.Conditional_Operand_Id__c != null){
                                    system.debug(':::::First ruleDetail::::::::'+ruleDetail);
                                    String oldOperandValue = ruleDetail.Condition_Operand_Details__c;
                                    system.debug('::::::::before::;'+oldOperandValue);
                                    ruleDetail = setRuleDetailConditionalOperands(ruleDetail,oldProductTypeMap,newProductTypeMap,rdgNameMap,wdgMap,guidelineId,isRDG,clonedGuidelineMap);
                                    ruleDetailListToUpdate.add(ruleDetail);
                                    String newOperandValue = ruleDetail.Condition_Operand_Details__c;
                                    system.debug('::::::::after::;'+newOperandValue);
                                    if(isRDG){
                                        if(rule.Release_Date_Guideline__r.All_Rules__c != null){
                                            RDGRuleUpdate(rule.Release_Date_Guideline__r.All_Rules__c,guidelineId,oldOperandValue,newOperandValue,rdgToUpdate,ruleDetail);
                                        }
                                    } else{
                                        if(rule.Date_To_Modify__c != null){
                                            if(rule.Date_To_Modify__c == 'Start Date' && rule.Window_Guideline__r.Start_Date_Rule__c != null){
                                                wgRuleUpdate(rule.Window_Guideline__r.Start_Date_Rule__c,rule.Date_To_Modify__c,guidelineId,oldOperandValue,newOperandValue,wgToUpdate,ruleDetail);
                                            } else if(rule.Date_To_Modify__c == 'End Date' && rule.Window_Guideline__r.End_Date_Rule__c != null){
                                                wgRuleUpdate(rule.Window_Guideline__r.End_Date_Rule__c,rule.Date_To_Modify__c,guidelineId,oldOperandValue,newOperandValue,wgToUpdate,ruleDetail);
                                            } else if(rule.Date_To_Modify__c == 'Outside Date' && rule.Window_Guideline__r.Outside_Date_Rule__c != null){
                                                wgRuleUpdate(rule.Window_Guideline__r.Outside_Date_Rule__c,rule.Date_To_Modify__c,guidelineId,oldOperandValue,newOperandValue,wgToUpdate,ruleDetail);
                                            } else if(rule.Date_To_Modify__c == 'Tracking Date' && rule.Window_Guideline__r.Tracking_Date_Rule__c != null){
                                                wgRuleUpdate(rule.Window_Guideline__r.Tracking_Date_Rule__c,rule.Date_To_Modify__c,guidelineId,oldOperandValue,newOperandValue,wgToUpdate,ruleDetail);
                                            }
                                        }                                        
                                    }
                                    if(newOperandValue == null && ruleDetail.Condition_Met_Months__c != null && ruleDetail.Condition_Met_Days__c != null){
                                        ruleDetailToDelete.add(ruleDetail);
                                    }
                                }
                            }
                        }
                    }
                }
                if(ruleListToUpdate.size() > 0){
                    update ruleListToUpdate;
                }
                if(ruleDetailListToUpdate.size() > 0){
                    update ruleDetailListToUpdate;
                }
                if(ruleDetailToDelete.size() > 0){
                    if(ruleDetailMap != null && ruleDetailMap.keySet().size() > 0){
                        Set<Id> ruleDetailIdSet = new Set<Id>();
                        for(EAW_Rule_Detail__c ruleDetail : ruleDetailToDelete){
                            ruleDetailIdSet.add(ruleDetail.Id);
                        }
                        for(Id ruleId : ruleDetailMap.keySet()){
                            Set<Id> originalSet = ruleDetailMap.get(ruleId);
                            originalSet.removeAll(ruleDetailIdSet);
                            ruleDetailMap.put(ruleId,originalSet);
                        }
                    }
                    delete ruleDetailToDelete;
                    if(ruleDetailMap.keySet().size() > 0){
                        List<EAW_Rule__c> ruleListToDelete = new List<EAW_Rule__c>();
                        for(Id ruleId : ruleDetailMap.keySet()){
                            if(ruleDetailMap.get(ruleId).size() == 0 || (ruleDetailMap.get(ruleId).size() == 1 && allRuleDetailMap.containsKey(new List<Id>(ruleDetailMap.get(ruleId))[0]) && allRuleDetailMap.get(new List<Id>(ruleDetailMap.get(ruleId))[0]).Condition_Timeframe__c != null)){
                                EAW_Rule__c rule = new EAW_Rule__c();
                                rule.id = ruleId;
                                if(ruleMap.containsKey(ruleId) && ruleMap.get(ruleId) != null){
                                    if(isRDG && ruleMap.get(ruleId).Release_Date_Guideline__c != null && rdgToUpdate.containsKey(ruleMap.get(ruleId).Release_Date_Guideline__c)){
                                        rdgToUpdate.put(ruleMap.get(ruleId).Release_Date_Guideline__c,'');
                                    } else if(ruleMap.get(ruleId).Window_Guideline__c != null){
                                        EAW_Rule__c currentRule = ruleMap.get(ruleId);
                                        if(currentRule.Date_To_Modify__c == 'Start Date' && wgToUpdate.containsKey(String.valueof(ruleMap.get(ruleId).Window_Guideline__c)  + '-SD')){
                                            wgToUpdate.put(String.valueof(ruleMap.get(ruleId).Window_Guideline__c) + '-SD' , '');
                                        } else if(currentRule.Date_To_Modify__c == 'End Date' && wgToUpdate.containsKey(String.valueof(ruleMap.get(ruleId).Window_Guideline__c)  + '-ED')){
                                            wgToUpdate.put(String.valueof(ruleMap.get(ruleId).Window_Guideline__c) + '-ED' , '');
                                        } else if(currentRule.Date_To_Modify__c == 'Outside Date' && wgToUpdate.containsKey(String.valueof(ruleMap.get(ruleId).Window_Guideline__c) + '-OD')){
                                            wgToUpdate.put(String.valueof(ruleMap.get(ruleId).Window_Guideline__c)  + '-OD' , '');
                                        } else if(currentRule.Date_To_Modify__c == 'Tracking Date' && wgToUpdate.containsKey(String.valueof(ruleMap.get(ruleId).Window_Guideline__c)  + '-TD')){
                                            wgToUpdate.put(String.valueof(ruleMap.get(ruleId).Window_Guideline__c)  + '-TD' , '');
                                        }
                                    }
                                }
                                ruleListToDelete.add(rule);
                            } 
                        }
                        if(ruleListToDelete.size() > 0){
                            delete ruleListToDelete;
                        }
                    }
                }
            }
        }
        if(isRDG){
            system.debug('::::::rdgToUpdate:::::::'+rdgToUpdate); 
            return rdgToUpdate;
        } else{
            return wgToUpdate;
        }
        
    }
    public static List<EAW_Rule__c> getGuidelineRules(Set<Id> guidelineIdSet,Boolean isRDG){
        if(isRDG){
            return [SELECT Id,Condition_Operand_Details__c,Conditional_Operand_Id__c,Release_Date_Guideline__c,
                    Release_Date_Guideline__r.All_Rules__c, (SELECT Id,Rule_No__c,Condition_Timeframe__c,Condition_Operand_Details__c,Conditional_Operand_Id__c,Condition_Met_Months__c,Condition_Met_Days__c,Window_Guideline_Date__c,
                    Condition_Date_Duration__c,Condition_Criteria_Amount__c,Day_of_the_Week__c FROM Rule_Orders__r)
                    FROM EAW_Rule__c WHERE Release_Date_Guideline__c IN :guidelineIdSet];
        } else{
            return [SELECT Id,Condition_Operand_Details__c,Conditional_Operand_Id__c,Window_Guideline__c,Date_To_Modify__c,
                    Window_Guideline__r.Start_Date_Rule__c,Window_Guideline__r.End_Date_Rule__c,Window_Guideline__r.Outside_Date_Rule__c,
                    Window_Guideline__r.Tracking_Date_Rule__c,(SELECT Id,Rule_No__c,Condition_Timeframe__c,Condition_Operand_Details__c,Conditional_Operand_Id__c,Condition_Met_Months__c,Condition_Met_Days__c,Window_Guideline_Date__c,
                     Condition_Date_Duration__c,Condition_Criteria_Amount__c,Day_of_the_Week__c FROM Rule_Orders__r)
                    FROM EAW_Rule__c WHERE Window_Guideline__c IN :guidelineIdSet];
        }
    }
    public static EAW_Rule__c setRuleConditionalOperands(EAW_Rule__c rule,Map<Id,String> oldProductTypeMap,Map<Id,String> newProductTypeMap,Map<String,EAW_Release_Date_Guideline__c> rdgNameMap,Map<String,EAW_Window_Guideline__c> wdgMap,String guidelineId,Boolean isRDG){
        List<String> ruleConditionalOperandDetails = String.valueOf(rule.Condition_Operand_Details__c).split(';');
        List<String> ruleConditionalOperandId = String.valueOf(rule.Conditional_Operand_Id__c).split(';');
        for(Integer i=0;i<ruleConditionalOperandDetails.size();i++){
            if(rdgNameMap.containsKey(ruleConditionalOperandDetails[i].replace(oldProductTypeMap.get(guidelineId),newProductTypeMap.get(guidelineId)))){
                
                ruleConditionalOperandDetails[i] = ruleConditionalOperandDetails[i].replace(oldProductTypeMap.get(guidelineId),newProductTypeMap.get(guidelineId));
                ruleConditionalOperandId[i] = rdgNameMap.get(ruleConditionalOperandDetails[i].replace(oldProductTypeMap.get(guidelineId),newProductTypeMap.get(guidelineId))).Id;
                
            } else if(wdgMap.containsKey(ruleConditionalOperandDetails[i].replace(oldProductTypeMap.get(guidelineId),newProductTypeMap.get(guidelineId)))){
                ruleConditionalOperandDetails[i] = ruleConditionalOperandDetails[i].replace(oldProductTypeMap.get(guidelineId),newProductTypeMap.get(guidelineId));
                ruleConditionalOperandId[i] = wdgMap.get(ruleConditionalOperandDetails[i].replace(oldProductTypeMap.get(guidelineId),newProductTypeMap.get(guidelineId))).Id;
                
            }  else{
                ruleConditionalOperandDetails.remove(i);
                ruleConditionalOperandId.remove(i);
                
            }
        }
        if(ruleConditionalOperandDetails.size() > 0){
            String details = '';
            String operand = '';
            for(Integer i=0;i<ruleConditionalOperandDetails.size();i++){
                if(i != ruleConditionalOperandDetails.size()-1){
                    details += ruleConditionalOperandDetails[i] + ';';
                    operand += ruleConditionalOperandId[i] + ';';
                } else{
                    details +=  ruleConditionalOperandDetails[i];
                    operand += ruleConditionalOperandId[i];
                }
            }
            rule.Condition_Operand_Details__c = details;
            rule.Conditional_Operand_Id__c = operand;
        } else{
            rule.Condition_Operand_Details__c = null;
            rule.Conditional_Operand_Id__c = null;
        }
        return rule;
    }
    public static EAW_Rule_Detail__c setRuleDetailConditionalOperands(EAW_Rule_Detail__c ruleDetail,Map<Id,String> oldProductTypeMap,Map<Id,String> newProductTypeMap,Map<String,EAW_Release_Date_Guideline__c> rdgNameMap,Map<String,EAW_Window_Guideline__c> wdgMap,String guidelineId,Boolean isRDG,Map<Id,String> clonedGuidelineMap){
        
        List<String> ruleConditionalOperandDetails = String.valueOf(ruleDetail.Condition_Operand_Details__c).split(';');
        List<String> ruleConditionalOperandId = String.valueOf(ruleDetail.Conditional_Operand_Id__c).split(';');
        for(Integer i=0;i<ruleConditionalOperandDetails.size();i++){
            if(rdgNameMap.containsKey(ruleConditionalOperandDetails[i].replace(oldProductTypeMap.get(guidelineId),newProductTypeMap.get(guidelineId)))){
                ruleConditionalOperandDetails[i] = ruleConditionalOperandDetails[i].replace(oldProductTypeMap.get(guidelineId),newProductTypeMap.get(guidelineId));
                ruleConditionalOperandId[i] = rdgNameMap.get(ruleConditionalOperandDetails[i].replace(oldProductTypeMap.get(guidelineId),newProductTypeMap.get(guidelineId))).Id;
                
            } else if(wdgMap.containsKey(ruleConditionalOperandDetails[i].replace(oldProductTypeMap.get(guidelineId),newProductTypeMap.get(guidelineId)))){
                ruleConditionalOperandDetails[i] = ruleConditionalOperandDetails[i].replace(oldProductTypeMap.get(guidelineId),newProductTypeMap.get(guidelineId));
                ruleConditionalOperandId[i] = wdgMap.get(ruleConditionalOperandDetails[i].replace(oldProductTypeMap.get(guidelineId),newProductTypeMap.get(guidelineId))).Id;
                
            } else if(oldProductTypeMap.containsKey(ruleConditionalOperandId[i]) ){
                ruleConditionalOperandDetails[i] = ruleConditionalOperandDetails[i].replace(oldProductTypeMap.get(guidelineId),newProductTypeMap.get(guidelineId));
                if(clonedGuidelineMap != null && clonedGuidelineMap.containsKey(ruleConditionalOperandId[i])){
                    ruleConditionalOperandId[i] = clonedGuidelineMap.get(ruleConditionalOperandId[i]);
                }
            } else{
                ruleConditionalOperandDetails.remove(i);
                ruleConditionalOperandId.remove(i);
                
            }
        }
        if(ruleConditionalOperandDetails.size() > 0){
            String details = '';
            String operand = '';
            for(Integer i=0;i<ruleConditionalOperandDetails.size();i++){
                if(i != ruleConditionalOperandDetails.size()-1){
                    details += ruleConditionalOperandDetails[i] + ';';
                    operand += ruleConditionalOperandId[i] + ';';
                } else{
                    details +=  ruleConditionalOperandDetails[i];
                    operand += ruleConditionalOperandId[i];
                }
            }
            ruleDetail.Condition_Operand_Details__c = details;
            ruleDetail.Conditional_Operand_Id__c = operand;
        } else{
            ruleDetail.Condition_Operand_Details__c = null;
            ruleDetail.Conditional_Operand_Id__c = null;
        }
        return ruleDetail;
    }
    public static void RDGRuleUpdate(String allRules,String rdgId,String operand,String newOperandValue,Map<String,String> rdgToUpdate,EAW_Rule_Detail__c ruleDetail){
        
        if(rdgToUpdate.containsKey(rdgId)){
            allRules = rdgToUpdate.get(rdgId);
        }
        String daysString = '';
        String dateString = '';
        if(newOperandValue == null){
            newOperandValue = '';
            if(ruleDetail != null){
                
                if(ruleDetail.Condition_Met_Months__c != null){
                    daysString += ruleDetail.Condition_Met_Months__c + ' Months ' + ruleDetail.Condition_Met_Days__c + ' Days of ';
                    operand = daysString + operand;
                    if(ruleDetail.Window_Guideline_Date__c != null){
                        dateString += ' on ' + ruleDetail.Window_Guideline_Date__c;
                    }
                    if(ruleDetail.Condition_Date_Duration__c != null){
                        dateString += ' in ' + ruleDetail.Condition_Date_Duration__c + ' ';
                        if(ruleDetail.Condition_Criteria_Amount__c != null){
                            dateString +=  string.valueof(ruleDetail.Condition_Criteria_Amount__c) + ' ';
                            if(ruleDetail.Day_of_the_Week__c != null){
                                dateString += ' of ' + ruleDetail.Day_of_the_Week__c ;
                            }
                        }
                    }
                    operand = operand + dateString;
                }
                operand = String.valueof(operand);
            }
        }
        system.debug('::::allRules:::::'+allRules);
        system.debug(':::operand::::'+operand+':::::newOperandValue:::::'+newOperandValue); 
        system.debug(daysString+':::::::::::::::::::'+allRules.containsIgnorecase(operand));
        allRules = allRules.replace(operand,newOperandValue);
        if(!allRules.containsIgnorecase(operand)){
            allRules = allRules.replace(daysString,'');
            allRules = allRules.replace(dateString,'');
        }
        rdgToUpdate.put(rdgId,allRules);
        
    }
    public static void wgRuleUpdate(String fieldValue,String allRules,String wgId,String operand,String newOperand,Map<String,String> wgMap,EAW_Rule_Detail__c ruleDetail){
	
        String daysString = '';
        String dateString = '';
        if(newOperand == null){
            newOperand = ''; 
            system.debug(operand+':::::::wgRuleUpdate:::::'+newOperand);
            if(ruleDetail != null){
                
                if(ruleDetail.Condition_Met_Months__c != null){
                    daysString += ruleDetail.Condition_Met_Months__c + ' Months ' + ruleDetail.Condition_Met_Days__c + ' Days of ';
                    operand = daysString + operand;
                    if(ruleDetail.Window_Guideline_Date__c != null){
                        dateString += ' on ' + ruleDetail.Window_Guideline_Date__c;
                    }
                    if(ruleDetail.Condition_Date_Duration__c != null){
                        dateString += ' in ' + ruleDetail.Condition_Date_Duration__c + ' ';
                        if(ruleDetail.Condition_Criteria_Amount__c != null){
                            dateString +=  ruleDetail.Condition_Criteria_Amount__c + ' ';
                            if(ruleDetail.Day_of_the_Week__c != null){
                                dateString += ' of ' + ruleDetail.Day_of_the_Week__c + ' ';
                            }
                        }
                    }
                    operand = operand + dateString;
                }
            }
        } 
        if(allRules == 'Start Date'){
            wgId = wgId + '-SD';
            if(wgMap.containsKey(wgId)){
                fieldValue = wgMap.get(wgId);
            }
            fieldValue = fieldValue.replace(operand,newOperand);
        } else if(allRules == 'End Date'){
            wgId = wgId + '-ED';
            if(wgMap.containsKey(wgId)){
                fieldValue = wgMap.get(wgId);
            }
            fieldValue = fieldValue.replace(operand,newOperand);
        } else if(allRules == 'Outside Date'){
            wgId = wgId + '-OD';
            if(wgMap.containsKey(wgId)){
                fieldValue = wgMap.get(wgId);
            }
            fieldValue = fieldValue.replace(operand,newOperand);
        } else{
            wgId = wgId + '-TD';
            if(wgMap.containsKey(wgId)){
                fieldValue = wgMap.get(wgId);
            }
            fieldValue = fieldValue.replace(operand,newOperand);
        }
        if(!fieldValue.containsIgnorecase(operand)){
            fieldValue = fieldValue.replace(daysString,'');
            fieldValue = fieldValue.replace(dateString,'');
        }
        system.debug(':::::::::'+ruleDetail);
        system.debug(fieldValue+'::::allRulesValue:::::::::'+allRules+':::::::operand:::::'+operand+'::::::'+newOperand);
        wgMap.put(wgId,fieldValue);
    }
    
}