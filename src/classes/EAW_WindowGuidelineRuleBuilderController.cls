public class EAW_WindowGuidelineRuleBuilderController {
    
    @AuraEnabled
    public static ruleWrapper getRulesAndDetails(String windowGuidelineId,String windowId){
        ruleWrapper newRuleWrapper  = new ruleWrapper();
        
        newruleWrapper.releaseWindowDateWrapper = retrieveRelatedReleaseAndWindowGuidelines(windowGuidelineId);
        try {
            EAW_Window_Guideline__c currentWindowGuideline = [SELECT Id,Product_Type__c,Previous_Version__c,Status__c FROM EAW_Window_Guideline__c WHERE Id = :windowGuidelineId ];
            List<EAW_Rule__c> ruleList = new List<EAW_Rule__c>();
            system.debug(windowGuidelineId+'::::::::::::;'+windowId);
            //if(windowId == null){
                ruleList = [SELECT Id, Window_Guideline__c,First_of_the_Month__c,Criteria__c,Condition_Field__c, Condition_Type__c,Nested__c,Nested_Times__c,Date_Type_Description__c,Product_Type__c,Operator__c,Date_To_Modify__c,Conditional_Operand_Id__c,Territory__c,Multi_conditional_operands__c, Condition_Operand_Details__c FROM EAW_Rule__c WHERE Window_Guideline__c = :windowGuidelineId]; 
            /*} else{
                ruleList = [SELECT Id, Window__c,First_of_the_Month__c,Rule_Overridden__c,Criteria__c,Condition_Field__c, Condition_Type__c,Nested__c,Nested_Times__c,Date_Type_Description__c,Product_Type__c,Operator__c,Date_To_Modify__c,Conditional_Operand_Id__c,Territory__c,Multi_conditional_operands__c,Condition_Operand_Details__c FROM EAW_Rule__c WHERE Window__c = :windowId];
            }*/
            List<EAW_Rule_Detail__c> parentRuleDetailList = [SELECT Id,Rule_No__c,Rule_Order__c,Nest_Order__c,Condition_Date_Text__c,Condition_Date_Duration__c,Condition_Criteria_Amount__c,Day_of_the_Week__c,Condition_Timeframe__c,Condition_Met_Months__c, Condition_Met_Days__c, Condition_Met_Media__c, Condition_Met_Territory__c,Condition_Type__c,Condition_Field__c,Conditional_Operand_Id__c,Condition_Criteria__c,Window_Guideline_Date__c, Condition_Set_Operator__c,Parent_Condition_Type__c, Condition_Operand_Details__c FROM EAW_Rule_Detail__c WHERE Rule_No__c IN :ruleList AND Parent_Rule_Detail__c = null];
            List<EAW_Rule_Detail__c> ruleDetailList = [SELECT id, Rule_No__c, Condition_Set_Field__c,Parent_Rule_Detail__c ,Condition_Date_Text__c, Condition_Date_Duration__c,Day_of_the_Week__c,Condition_Criteria_Amount__c,Condition_Met_Months__c, Condition_Met_Days__c, Condition_Met_Media__c, Condition_Met_Territory__c,Conditional_Operand_Id__c,Parent_Condition_Type__c,Condition_Type__c,Condition_Field__c,Condition_Criteria__c,
                                                       Start_Date__c, End_Date__c, WindowName__c, WindowName__r.Name, Condition_Not_Met_Days__c, Condition_Not_Met_Months__c, Condition_Not_Met_Media__c,Condition_Not_Met_Territory__c,Condition_Set_Operator__c,
                                                       Alt_Conditon_Days__c, Alt_Condition_Months__c, Alt_Condition_Media__c,Alt_Condition_Territory__c,Rule_Order__c,Nest_Order__c,Condition_Timeframe__c,Window_Guideline_Date__c,Multi_conditional_operands__c,Condition_Operand_Details__c
                                                       FROM EAW_Rule_Detail__c WHERE Rule_No__c IN :ruleList AND Parent_Rule_Detail__c != null]; 
            newruleWrapper.ruleList = ruleList;
            newruleWrapper.parentRuleDetailList = parentRuleDetailList;
            newruleWrapper.ruleDetailList = ruleDetailList;
            newRuleWrapper.isActive = false;
            newRuleWrapper.previousVersion = currentWindowGuideline.Previous_Version__c; // LRCC - 1276
            newRuleWrapper.operandDateAndStatus = conditionalOperandDateAndStatus(parentRuleDetailList,ruleDetailList,windowId);
            if(currentWindowGuideline.Status__c == 'Active'){
                newRuleWrapper.isActive = true;
            }
        } catch(Exception e){
            newruleWrapper.ruleList = null;
        }
        
        return newRuleWrapper;
    }
    private static releaseAndWindowGuidelinesWrapper retrieveRelatedReleaseAndWindowGuidelines(String recordId) {
        
        releaseAndWindowGuidelinesWrapper newreleaseAndWindowGuidelinesWrapper = new releaseAndWindowGuidelinesWrapper();
        
        List<ReleaseWindowDateWrapper> picklistWrappers = new List<ReleaseWindowDateWrapper> ();
        
        EAW_Window_Guideline__c currentWindowGuideline = [SELECT Id,Product_Type__c FROM EAW_Window_Guideline__c WHERE Id = :recordId ];
        
       // for(EAW_Release_Date_Guideline__c releaseDate : [SELECT Id,Name FROM EAW_Release_Date_Guideline__c WHERE Active__c = true LIMIT 10]){
            //picklistWrappers.add(
                //new ReleaseWindowDateWrapper(releaseDate.Name, releaseDate.Name)
            //); 
       // }
        if(currentWindowGuideline.Product_Type__c != NULL){
            for ( EAW_Release_Date_Guideline__c  rdg : [ SELECT Id,Name, EAW_Release_Date_Type__c, Territory__c, Language__c, Product_Type__c, Active__c FROM EAW_Release_Date_Guideline__c WHERE Product_Type__c = :currentWindowGuideline.Product_Type__c AND  Active__c = TRUE ORDER BY Name ASC] ) {
                
                if (rdg.EAW_Release_Date_Type__c != null && rdg.Territory__c != null) {
                   // String pickVal = rdg.EAW_Release_Date_Type__c + ' / ' +rdg.Territory__c;
                    picklistWrappers.add(
                        new ReleaseWindowDateWrapper(rdg.Name, rdg.Id)
                    ); 
                }
            }
            newreleaseAndWindowGuidelinesWrapper.releaseDateGuidelines = picklistWrappers;
            picklistWrappers = new List<ReleaseWindowDateWrapper> ();
            for(EAW_Window_Guideline__c windowGuideline : [SELECT Id,Name,Media__c,Status__c,Start_Date_Rule__c,End_Date_Rule__c,EAW_Territory__c FROM EAW_Window_Guideline__c WHERE (Status__c = 'Active' OR Id = :recordId) AND Product_Type__c = :currentWindowGuideline.Product_Type__c  AND Name != NULL ORDER BY Name ASC]){
                String pickVal = windowGuideline.Name;
                if(windowGuideline.Id == recordId && windowGuideline.Status__c == 'Draft'){
                    pickVal = pickVal + ' (+)';
                }
                picklistWrappers.add(
                    new ReleaseWindowDateWrapper(pickVal, windowGuideline.Id)
                ); 
            }
        }
        newreleaseAndWindowGuidelinesWrapper.windowGuidelines = picklistWrappers;
        return newreleaseAndWindowGuidelinesWrapper;
    }
    @AuraEnabled
    public static List<EAW_Rule__c> saveWindowRuleAndRuleDetails(String ruleList,String ruleAndRuleDetailsNodes,List<String> ruleDetilsToDelete,String DetailJSONmap,String currentWindowGuidelineId,EAW_Window__c windowRecord){
        // try{
        Map<String,String> ruleTextMap = (Map<String,String>)JSON.deserialize(DetailJSONmap,Map<String,String>.class);
        List<EAW_Rule__c> newruleList = (List<EAW_Rule__c>)JSON.deserialize(ruleList,List<EAW_Rule__c>.class);
        List<List<ruleDetailNodeWrapper>> detailNodeList = (List<List<ruleDetailNodeWrapper>>)JSON.deserialize(ruleAndRuleDetailsNodes,List<List<ruleDetailNodeWrapper>>.class);
        system.debug(newruleList);
        system.debug('detailNodeList::::'+detailNodeList);
        if(ruleDetilsToDelete != null) {
            String windowId;
            if(windowRecord != null) {
                windowId = windowRecord.Id;
            }
            deleteRuleAndDetails(false,false,null,ruleDetilsToDelete,currentWindowGuidelineId,windowId);
        }
        Set<Id> ruleSet = new Set<Id>();
        String startDateRule,endDateRule,trackingDateRule,outsideDateRule = '';
        
        if(newruleList != null && newruleList.size() > 0) {
            for(Integer i=0;i<newruleList.size();i++){
                
                saveRuleAndRuleDetails(newruleList[i],detailNodeList[i],windowRecord);
                if(ruleTextMap.containsKey(newruleList[i].Date_To_Modify__c)){
                    if(newruleList[i].Date_To_Modify__c == 'Start Date' ){
                        startDateRule = ruleTextMap.get(newruleList[i].Date_To_Modify__c);
                    }
                    if(newruleList[i].Date_To_Modify__c == 'End Date'){
                        endDateRule = ruleTextMap.get(newruleList[i].Date_To_Modify__c);
                    }
                    if(newruleList[i].Date_To_Modify__c == 'Tracking Date' ){
                        trackingDateRule = ruleTextMap.get(newruleList[i].Date_To_Modify__c);
                    }
                    if(newruleList[i].Date_To_Modify__c == 'Outside Date' ){
                        outsideDateRule = ruleTextMap.get(newruleList[i].Date_To_Modify__c);
                    }
                }
                ruleSet.add(newruleList[i].Id);
            }
        }
        List<EAW_Window__c> windowsToUpdate = new List<EAW_Window__c>();
        if(currentWindowGuidelineId != null){
            EAW_Window_Guideline__c windowGuideline = [SELECT Id,Status__c,Start_Date_Rule__c,End_Date_Rule__c,Outside_Date_Rule__c,Tracking_Date_Rule__c,(SELECT Id,Start_Date__c,End_Date__c,Outside_Date__c,Tracking_Date__c FROM Windows__r) FROM EAW_Window_Guideline__c WHERE Id = :currentWindowGuidelineId];
            windowGuideline.Start_Date_Rule__c = startDateRule;
            windowGuideline.End_Date_Rule__c = endDateRule;
            windowGuideline.Outside_Date_Rule__c = outsideDateRule;
            windowGuideline.Tracking_Date_Rule__c = trackingDateRule;
            update windowGuideline;
            /*if(windowGuideline.Status__c == 'Active'){
                if(String.isNotBlank(windowGuideline.Id)) {
                    windowGuidelineQueuableCall(new Set<Id>{windowGuideline.Id},null); 
                }
            }*/
        } 
       /* if(windowRecord != null){
            update windowRecord;
        }
        if(newruleList != null && newruleList.size() > 0) {
            
            /*if(newruleList[0].Window__c != null ){
                EAW_Window__c window = [SELECT Id,EAW_Plan__r.EAW_Title__c,EAW_Window_Guideline__c FROM EAW_Window__c WHERE Id = :newruleList[0].Window__c];
                if(window.EAW_Window_Guideline__c != null){
                    windowGuidelineQueuableCall(new Set<Id>{window.EAW_Window_Guideline__c},new Set<Id>{window.EAW_Plan__r.EAW_Title__c}); 
                }
            }
        }*/
        return newruleList;
        //} catch(Exception e){
        // throw New AuraHandledException(e.getMessage());
        //}
    }
    /*private static void windowGuidelineQueuableCall(Set<Id> windowGuidleineIdSet,Set<Id> titleIdSet){
        //LRCC-1478
        EAW_QueueableDateCalculationClass caclQueueJob = new EAW_QueueableDateCalculationClass();
        caclQueueJob.releaseDateGuidelineIdSet = NULL;
        caclQueueJob.filteredTitleIdSet = titleIdSet;
        caclQueueJob.windowGuidelineIdSet = windowGuidleineIdSet;
        String jobId = System.enqueueJob(caclQueueJob);
        System.debug(':::: Job Id ::::'+ jobId);
        
        /*EAW_QWGRuleBuilderCalculationController qwg =  new EAW_QWGRuleBuilderCalculationController();    //Queueable
        qwg.windowGuielineRecordIdSet = windowGuidleineIdSet;
        qwg.filteredTitleIdSet = titleIdSet;
        String jobId = System.enqueueJob(qwg);
    }*/
    @AuraEnabled
    public static List<EAW_Rule_Detail__c> saveRuleAndRuleDetails(EAW_Rule__c newrule,List<ruleDetailNodeWrapper> newruleDetailNodeWrapper,EAW_Window__c windowRecord){
        
        Id ruleRecordTypeId = Schema.SObjectType.EAW_Rule__c.getRecordTypeInfosByName().get('Window Date Calculation').getRecordTypeId();
        Id ruleDetailRecordTypeId = Schema.SObjectType.EAW_Rule_Detail__c.getRecordTypeInfosByName().get('Window Date Calculation').getRecordTypeId();
        if(windowRecord != null){
            ruleRecordTypeId = Schema.SObjectType.EAW_Rule__c.getRecordTypeInfosByName().get('Window Calculation').getRecordTypeId();
            ruleDetailRecordTypeId = Schema.SObjectType.EAW_Rule_Detail__c.getRecordTypeInfosByName().get('Window Calculation').getRecordTypeId();
        }
        newRule.RecordTypeId = ruleRecordTypeId;
        upsert newRule;
        //List<ruleDetailNodeWrapper> newruleDetailNodeWrapper = new List<ruleDetailNodeWrapper>();
        //newruleDetailNodeWrapper = (List<ruleDetailNodeWrapper> )JSON.deserialize(ruleAndRuleDetailNodes,List<ruleDetailNodeWrapper> .class);
        List<EAW_Rule_Detail__c> parentRuleDetailList = new List<EAW_Rule_Detail__c>();
        for(ruleDetailNodeWrapper newruleDetail : newruleDetailNodeWrapper){
            System.debug('newruleDetail::::::'+newruleDetail);
            if(newruleDetail.ruleDetail != null){
                EAW_Rule_Detail__c parentRuleDetail = newruleDetail.ruleDetail;
                parentRuleDetail.Rule_No__c = newRule.Id;
                parentRuleDetail.RecordTypeId = ruleDetailRecordTypeId;
                parentRuleDetailList.add(parentRuleDetail);
            }
        }       
        upsert parentRuleDetailList;
        List<EAW_Rule_Detail__c> recursiveRuleDetailList = new List<EAW_Rule_Detail__c>();
        List<EAW_Rule_Detail__c> ruleDetailList = new List<EAW_Rule_Detail__c>();
        for(ruleDetailNodeWrapper newruleDetailNode : newruleDetailNodeWrapper){
            if(newruleDetailNode.dateCalcs != null && (newruleDetailNode.dateCalcs.size() > 0 || newruleDetailNode.nodeCalcs.size() > 0) && newruleDetailNode.nodeCalcs != null){
                recursiveRuleDetailList = new List<EAW_Rule_Detail__c>();
                ruleDetailList.addAll(recursiveNodeSave(ruleDetailRecordTypeId,recursiveRuleDetailList,newRule.Id,newruleDetailNode.ruleDetail.Id,newruleDetailNode.dateCalcs,newruleDetailNode.nodeCalcs));            
            }
        }       
        upsert ruleDetailList;
        if(ruleDetailList != null && ruleDetailList.size() >= 1){
            newRule.Nested_Times__c = ruleDetailList[ruleDetailList.size()-1].Nest_Order__c;
        }
        else{
            newRule.Nested_Times__c = 1;
        }
        update newRule;
        //To dynmaic creation of Release Date for each Title
       // EAW_RuleBuilderCalculationController.processTitlesWithNewRule(newRule.Id);
        return ruleDetailList;
    }
    private static List<EAW_Rule_Detail__c> recursiveNodeSave(Id ruleDetailRecordTypeId,List<EAW_Rule_Detail__c> ruleDetailList,Id ruleId,Id parentRuleDetailId,List<EAW_Rule_Detail__c> dateCalcs,List<ruleDetailNodeWrapper> nodeCalcs){
        
        for(EAW_Rule_Detail__c dateCalcRuleDetail : dateCalcs){
            dateCalcRuleDetail.Rule_No__c = ruleId;
            dateCalcRuleDetail.RecordTypeId = ruleDetailRecordTypeId;
            dateCalcRuleDetail.Parent_Rule_Detail__c = parentRuleDetailId;
            ruleDetailList.add(dateCalcRuleDetail);
        }       

        List<EAW_Rule_Detail__c> childComeParentList = new List<EAW_Rule_Detail__c>();
        for(ruleDetailNodeWrapper ruleDetailNode : nodeCalcs){          
            EAW_Rule_Detail__c childComeParent = ruleDetailNode.ruleDetail;
            childComeParent.Rule_No__c = ruleId;
            childComeParent.RecordTypeId = ruleDetailRecordTypeId;
            childComeParent.Parent_Rule_Detail__c = parentRuleDetailId;
            childComeParentList.add(childComeParent);           
        }
        upsert childComeParentList;
        for(ruleDetailNodeWrapper ruleDetailNode : nodeCalcs){
            recursiveNodeSave(ruleDetailRecordTypeId,ruleDetailList,ruleId,ruleDetailNode.ruleDetail.Id,ruleDetailNode.dateCalcs,ruleDetailNode.nodeCalcs);
        }
        return ruleDetailList;
    }
    @AuraEnabled
    public static String deleteRuleAndDetails(Boolean isRule,Boolean isDateCalc,String ruleOrRuleDetailId,List<String> ruleDetilsToDelete,String currentWindowGuidelineId,String windowId){
        try {
            if(isRule == true) {
                EAW_Rule__c rule = [SELECT Id FROM EAW_Rule__c WHERE Id = :ruleOrRuleDetailId];
                delete rule;
                return 'success';
            } else {
                
                //if(isDateCalc == true){
               // EAW_Rule_Detail__c dateCalcRuleDetail = [SELECT Id,Rule_No__c FROM EAW_Rule_Detail__c WHERE Id = :ruleOrRuleDetailId ];
               // EAW_Rule__c newRule = [SELECT Id FROM EAW_Rule__c WHERE Id = :dateCalcRuleDetail.Rule_No__c];
              //  delete dateCalcRuleDetail;
                //  EAW_RuleBuilderCalculationController.processTitlesWithNewRule(newRule.Id);
               // return 'success';
                //} else{
                
                Map<Id,Set<Id>> ruleDetailMap = new Map<Id,Set<Id>>();
                List<EAW_Rule_Detail__c> ruleDetailList = new List<EAW_Rule_Detail__c> ();
                
                if(String.isNotBlank(currentWindowGuidelineId)) {
                    ruleDetailList = [SELECT Id,Rule_No__r.Window_Guideline__c,Parent_Rule_Detail__r.Id FROM EAW_Rule_Detail__c WHERE Rule_No__r.Window_Guideline__c =: currentWindowGuidelineId AND Parent_Rule_Detail__c != null];
                } 
                else if(String.isNotBlank(windowId)) {
                    ruleDetailList = [SELECT Id,Rule_No__r.Window__c, Parent_Rule_Detail__r.Id FROM EAW_Rule_Detail__c WHERE Rule_No__r.Window__c =:windowId AND Parent_Rule_Detail__c != null];
                }
                
                for(EAW_Rule_Detail__c ruleDetail : ruleDetailList){
                    Set<Id> temp = new Set<Id>();
                    if(ruleDetailMap.containsKey(ruleDetail.Parent_Rule_Detail__c)){
                        temp = ruleDetailMap.get(ruleDetail.Parent_Rule_Detail__c);
                        temp.add(ruleDetail.Id);
                    } else{
                        temp.add(ruleDetail.Id);
                    }
                    ruleDetailMap.put(ruleDetail.Parent_Rule_Detail__c,temp);
                }
                Set<Id> ruleDetailSetId = new Set<Id>();
                
                for(String ruleDetailId : ruleDetilsToDelete){
                    if(ruleDetailMap.containsKey(ruleDetailId)){
                        ruleDetailSetId.addAll(setChildRuleDetailById(ruleDetailMap,ruleDetailId));
                        ruleDetailSetId.add(ruleDetailId);
                    } else{
                        ruleDetailSetId.add(ruleDetailId);
                    }
                }
                system.debug('ruleDetailSetId:::::'+ruleDetailSetId.size()+':::::'+ruleDetailSetId);
                
                if(ruleDetailSetId.size()> 0){
                    List<EAW_Rule_Detail__c> parentRuleDetail = [SELECT Id,Rule_No__c FROM EAW_Rule_Detail__c WHERE Id IN :ruleDetailSetId];
                    system.debug(parentRuleDetail);
                    delete parentRuleDetail;
                }
                return 'success';
                
                //  EAW_Rule__c newRule = [SELECT Id FROM EAW_Rule__c WHERE Id = :parentRuleDetail.Rule_No__c];
                //  EAW_RuleBuilderCalculationController.processTitlesWithNewRule(newRule.Id);
                //}
            }
        } catch (Exception e){
            throw New AuraHandledException(e.getMessage());
        }        
    }
    public static Set<Id> setChildRuleDetailById(Map<Id,Set<Id>> ruleDetailMap,Id parentRuleDetailId){
        Set<Id> wholeIdSet = new Set<Id>();
        for(Id ruleDetailId : ruleDetailMap.get(parentRuleDetailId)){
            system.debug('Map:::'+ruleDetailId);
            Set<Id> ruleDetailSet = new Set<Id>();
            if(ruleDetailMap.containsKey(ruleDetailId)){
                ruleDetailSet.add(ruleDetailId);
                ruleDetailSet.addAll(setChildRuleDetailById(ruleDetailMap,ruleDetailId));
            } else{
                ruleDetailSet.add(ruleDetailId);                
            }
            wholeIdSet.addAll(ruleDetailSet);
            ruleDetailMap.put(parentRuleDetailId,wholeIdSet);        
        }
        return wholeIdSet;
    }
    @AuraEnabled
    public static void overriddenWindowRules(Id windowGuidelineId, Id windowId, String dateToModify) {
        
      /*  List<EAW_Rule__c> rulesToDelete = [
            SELECT Id, Window__c, Date_To_Modify__c 
            FROM EAW_Rule__c 
            WHERE Window__c = :windowId AND Date_To_Modify__c = :dateToModify
        ];
        System.debug('rulesToDelete ::: ' + rulesToDelete);
        
        if(rulesToDelete != null && rulesToDelete.size() > 0) {
            
            delete rulesToDelete;
            
            Map<String, Schema.SObjectField> ruleFieldMap = Schema.SObjectType.EAW_Rule__c.Fields.getMap(); 
            
            String ruleQueryString = 'SELECT';
            for(Schema.SObjectField fieldName : ruleFieldMap.values()) {
                String fieldAPI = String.valueOf(fieldName);
                ruleQueryString = ruleQueryString + ' ' + fieldAPI + ' ,';
            }
            ruleQueryString = ruleQueryString.removeEnd(',');
            ruleQueryString += ' FROM EAW_Rule__c WHERE Window_Guideline__c = :windowGuidelineId AND Date_To_Modify__c = :dateToModify';               
            System.debug('ruleQueryString:::::' + ruleQueryString );
            
            List<EAW_Rule__c> rules = Database.query(ruleQueryString);
            System.debug('Rules:::::' + rules );
            
            if(rules != null && rules.size() > 0) {
                
                List<EAW_Rule__c> clonedRules = new List<EAW_Rule__c>();
                Map<Id, List<EAW_Rule__c>> ruleIdClonedRules = new Map<Id, List<EAW_Rule__c>>();
                
                for(EAW_Rule__c r : rules) {
                    
                    EAW_Rule__c rule = r.clone(false, true, false, false);
                    rule.Window__c = windowId; 
                    rule.Window_Guideline__c = null;
                    
                    if(!ruleIdClonedRules.containsKey(r.Id)) {
                        ruleIdClonedRules.put(r.Id, new List<EAW_Rule__c>());
                    }
                    ruleIdClonedRules.get(r.Id).add(rule);
                    clonedRules.add(rule);
                } 
                System.debug('clonedRules:::::: ' + clonedRules); 
                
                if(clonedRules != null && clonedRules.size() > 0) {
                    
                    Database.insert(clonedRules, false);
                    System.debug('ruleIdClonedRules::::: ' + ruleIdClonedRules);
                    
                    Map<String, Schema.SObjectField> ruleDetailFieldMap = Schema.SObjectType.EAW_Rule_Detail__c.Fields.getMap();
                    Set<Id> ruleIds = ruleIdClonedRules.keySet();
                    
                    String ruleDetailQueryString = 'SELECT ';
                    for(Schema.SObjectField fieldName : ruleDetailFieldMap.values()){
                        String fieldAPI = String.valueOf(fieldName);
                        ruleDetailQueryString = ruleDetailQueryString + ' ' + fieldAPI + ' ,';
                    }
                    ruleDetailQueryString = ruleDetailQueryString.removeEnd(',');
                    ruleDetailQueryString += 'FROM EAW_Rule_Detail__c WHERE Rule_No__c IN :ruleIds ';            
                    System.debug('ruleDetailQueryString::::' + ruleDetailQueryString );
                    
                    List<EAW_Rule_Detail__c> ruleDetails = Database.query(ruleDetailQueryString);
                    System.debug('ruleDetails ::::' + ruleDetails );
                    
                    if(ruleDetails != null && ruleDetails.size() > 0){
                        
                        List<EAW_Rule_Detail__c> clonedRuleDetails = new List<EAW_Rule_Detail__c>();
                        Map<Id, List<EAW_Rule_Detail__c>> ruleDetailIdClonedRuleDetails = new Map<Id, List<EAW_Rule_Detail__c>>();
                        Map<Id, Id> ruleDetailIdParentRDId = new Map<Id, Id>();
                        
                        for(EAW_Rule_Detail__c rd : ruleDetails){
                            
                            if(rd.Rule_No__c != null && ruleIdClonedRules.get(rd.Rule_No__c) != null){
                                
                                for(EAW_Rule__c r : ruleIdClonedRules.get(rd.Rule_No__c)){
                                    
                                    EAW_Rule_Detail__c ruleDetail = rd.clone(false, true, false, false);                                
                                    ruleDetail.Rule_No__c = r.Id;
                                    ruleDetail.WindowName__c = r.Window__c;
                                    
                                    if(!ruleDetailIdClonedRuleDetails.containsKey(rd.Id)){
                                        ruleDetailIdClonedRuleDetails.put(rd.Id, new List<EAW_Rule_Detail__c >());
                                    }
                                    ruleDetailIdClonedRuleDetails.get(rd.Id).add(ruleDetail);
                                    clonedRuleDetails.add(ruleDetail);
                                }
                            }
                            
                            if(rd.Parent_Rule_Detail__c != null){
                                ruleDetailIdParentRDId.put(rd.Id, rd.Parent_Rule_Detail__c);
                            }
                        }
                        System.debug('clonedRuleDetails::::: ' + clonedRuleDetails); 
                        System.debug('ruleDetailIdParentRDId::::: ' + ruleDetailIdParentRDId); 
                        
                        if(clonedRuleDetails != null && clonedRuleDetails.size()>0){
                            
                            Database.insert(clonedRuleDetails, false);  
                            System.debug('ruleDetailIdClonedRuleDetails::::: ' + ruleDetailIdClonedRuleDetails); 
                            
                            List<EAW_Rule_Detail__c> updatedRuleDetails = new List<EAW_Rule_Detail__c>();
                            
                            for(Id rdId : ruleDetailIdClonedRuleDetails.keySet()){
                                
                                if(ruleDetailIdParentRDId.containsKey(rdId)){
                                    
                                    for(Integer i=0; i<ruleDetailIdClonedRuleDetails.get(rdId).size(); i++){
                                        
                                        if(ruleDetailIdClonedRuleDetails.get(rdId)[i].Parent_Rule_Detail__c != null){
                                            
                                            ruleDetailIdClonedRuleDetails.get(rdId)[i].Parent_Rule_Detail__c = 
                                                ruleDetailIdClonedRuleDetails.get(ruleDetailIdParentRDId.get(rdId))[i].Id;
                                        }
                                    }
                                    updatedRuleDetails.addAll(ruleDetailIdClonedRuleDetails.get(rdId));
                                }
                            }             
                            Database.update(updatedRuleDetails, false);
                            System.debug('updatedRuleDetails::::::' + updatedRuleDetails);
                        }            
                    }
                }
                EAW_Window__c window = [SELECT Id,EAW_Plan__r.EAW_Title__c FROM EAW_Window__c WHERE Id = :windowId];
                //windowGuidelineQueuableCall(new Set<Id>{windowGuidelineId},new Set<Id>{window.EAW_Plan__r.EAW_Title__c}); 
            }
        }*/
    }
    public static  Map<Id,dateAndStatus> conditionalOperandDateAndStatus(List<EAW_Rule_Detail__c> parentRuleDetailList,List<EAW_Rule_Detail__c> childRuleDetailList,Id windowId){
        
        if(windowId != null){
            List<EAW_Rule_Detail__c> ruleDetailList = new List<EAW_Rule_Detail__c>();
            ruleDetailList.addAll(parentRuleDetailList);
            ruleDetailList.addAll(childRuleDetailList);
            Map<Id,dateAndStatus> dateAndStatusMap = new Map<Id,dateAndStatus>();
            Set<Id> conditionalIdSet = new Set<Id>();
            EAW_Window__c window = [SELECT Id,EAW_Plan__r.EAW_Title__c,EAW_Title_Attribute__c FROM EAW_Window__c WHERE Id = :windowId];
            if(ruleDetailList.size() > 0){
                for(EAW_Rule_Detail__c ruleDetail : ruleDetailList){
                    if(ruleDetail.Conditional_Operand_Id__c != null && ruleDetail.Conditional_Operand_Id__c.contains(';') == false){
                        conditionalIdSet.add(ruleDetail.Conditional_Operand_Id__c);
                    }
                }
                system.debug('::::::conditionalIdSet::::::::'+conditionalIdSet);
                if(conditionalIdSet.size() > 0){
                    
                    try{
                        List<EAW_Release_Date__c> releaseDateList = [SELECT Id,Release_Date_Guideline__c,Release_Date__c,Status__c,Title__c FROM EAW_Release_Date__c  WHERE Release_Date_Guideline__c IN :conditionalIdSet AND Title__c = :window.EAW_Title_Attribute__c];
                        List<EAW_Window__c> windowList = [SELECT Id,EAW_Window_Guideline__c,EAW_Title_Attribute__c,Status__c,Start_Date__c,End_Date__c,Outside_Date__c,Tracking_Date__c FROM EAW_Window__c WHERE EAW_Window_Guideline__c IN :conditionalIdSet AND EAW_Title_Attribute__c = :window.EAW_Title_Attribute__c];
                        system.debug(':::::::::releaseDateList::::::::::'+releaseDateList);
                        system.debug('::::::::windowList:::::::::::'+windowList);
                        if(releaseDateList != null && releaseDateList.size() > 0){
                            for(EAW_Release_Date__c releaseDate : releaseDateList){
                                dateAndStatus newdateAndStatus = new dateAndStatus();
                                newdateAndStatus.dateValue = releaseDate.Release_Date__c;
                                newdateAndStatus.statusValue = releaseDate.Status__c;
                                dateAndStatusMap.put(releaseDate.Release_Date_Guideline__c,newdateAndStatus);
                            }
                        }
                        if(windowList != null && windowList.size() > 0){
                            for(EAW_Window__c conditionWindow : windowList){
                                dateAndStatus newdateAndStatus = new dateAndStatus();
                                newdateAndStatus.statusValue = conditionWindow.Status__c;
                                newdateAndStatus.window = conditionWindow;
                                dateAndStatusMap.put(conditionWindow.EAW_Window_Guideline__c,newdateAndStatus);
                            }
                        }
                    } catch(Exception e){
                        
                    }
                }
            } 
            system.debug('::::::::dateAndStatusMap:::::::::::'+dateAndStatusMap);
            return dateAndStatusMap;
        }else{
            return null;
        }
    }
    public class dateAndStatus {
        @AuraEnabled
        public Date dateValue;
        @AuraEnabled
        public String statusValue;
        @AuraEnabled
        public EAW_Window__c window;
    }
    public class ruleDetailNodeWrapper{
        @AuraEnabled
        public EAW_Rule_Detail__c ruleDetail;
        @AuraEnabled
        public List<EAW_Rule_Detail__c> dateCalcs;
        @AuraEnabled
        public List<ruleDetailNodeWrapper> nodeCalcs;
    }
    public class ruleWrapper{
        @AuraEnabled
        public List<EAW_Rule__c> ruleList;
        @AuraEnabled
        public List<EAW_Rule_Detail__c> parentRuleDetailList;
        @AuraEnabled
        public List<EAW_Rule_Detail__c> ruleDetailList;
        @AuraEnabled
        public releaseAndWindowGuidelinesWrapper releaseWindowDateWrapper;
        @AuraEnabled
        public Boolean isActive;
        @AuraEnabled
        public String previousVersion;
        @AuraEnabled
        public Map<Id,dateAndStatus> operandDateAndStatus;
    }
     public class ReleaseWindowDateWrapper {
        @AuraEnabled
        public String label;
        @AuraEnabled
        public String value;
        
        public ReleaseWindowDateWrapper(String label, String value) {
            this.label = label;
            this.value = value;
        }
    }
    public class releaseAndWindowGuidelinesWrapper{
        @AuraEnabled
        public List<ReleaseWindowDateWrapper> releaseDateGuidelines;
        @AuraEnabled
        public List<ReleaseWindowDateWrapper> windowGuidelines;
    }
}