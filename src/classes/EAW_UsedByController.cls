public class EAW_UsedByController {
    
    @AuraEnabled
    public static usedByWrapper getUsedByRecords(String recordId, String objectName){
        
        usedByWrapper wrap = new usedByWrapper();
        EAW_DownstreamRuleGuidelinesFindUtil utilClass = new EAW_DownstreamRuleGuidelinesFindUtil();
        
        // Current Record
        String queryStringCR = '';
        
        if(objectName == 'EAW_Release_Date_Guideline__c'){            
            queryStringCR = 
                'SELECT Id, Territory__c, Product_Type__c, EAW_Release_Date_Type__c, Language__c FROM ' + 
                String.escapeSingleQuotes(objectName) + ' WHERE Id = \'' + recordId + '\'';
        }
        
        if(objectName == 'EAW_Window_Guideline__c'){
            queryStringCR = 
                'SELECT Id, Name,EAW_Territory__c, Product_Type__c, Window_Type__c, EAW_Language__c FROM ' + 
                String.escapeSingleQuotes(objectName) + ' WHERE Id = \'' + recordId + '\'';
            
        }
        
        System.debug('queryStringCR : ' + queryStringCR);
        
        wrap.currentRecord = Database.query(queryStringCR);
        
        // RDG
        
        if(objectName == 'EAW_Release_Date_Guideline__c'){
            Set<Id> rdgIds = new Set<Id>();
            
            /*String ruleQueryString = 'SELECT Id, Conditional_Operand_Id__c, Release_Date_Guideline__c FROM EAW_Rule__c' + 
                ' WHERE Conditional_Operand_Id__c = \'' + recordId + '\'';
            System.debug('ruleQueryString : ' + ruleQueryString);
            
            for(EAW_Rule__c r : Database.query(ruleQueryString)){
                
                if(r.Release_Date_Guideline__c != null){                
                    rdgIds.add(r.Release_Date_Guideline__c); 
                }                        
            }
            
            String ruleDetailsQueryString = 
                'SELECT Id, Conditional_Operand_Id__c, Rule_No__r.Release_Date_Guideline__c FROM EAW_Rule_Detail__c ' +
                'WHERE Conditional_Operand_Id__c = \'' + recordId + '\'';
            System.debug('ruleDetailsQueryString : ' + ruleDetailsQueryString);
            
            for(EAW_Rule_Detail__c rd : Database.query(ruleDetailsQueryString)){
                
                if(rd.Rule_No__r.Release_Date_Guideline__c != null){                
                    rdgIds.add(rd.Rule_No__r.Release_Date_Guideline__c);
                }                        
            }*/
            Map<Id, Set<Id>> referenceMap = utilClass.findReferencedReleaseDateGuidelineRelatedToReleaseDateGuideline(new Set<Id>{recordId});
            if(referenceMap != null && referenceMap.keySet().size() > 0){   
                rdgIds = referenceMap.get(recordId);
            }
            System.debug('rdgIds : ' + rdgIds);
            
            if(rdgIds != null && rdgIds.size() > 0){
             
                
                String queryStringRDG = 
                    'SELECT Id, Territory__c, Product_Type__c, EAW_Release_Date_Type__c, Language__c, Active__c FROM ' + 
                    'EAW_Release_Date_Guideline__c WHERE Active__c = TRUE AND Id IN :rdgIds ORDER By Territory__c';
                
                System.debug('queryStringRDG : ' + queryStringRDG);
                
                wrap.usedByRDG = Database.query(queryStringRDG);
            } 
        }
        // WGL
        
        Set<Id> wglIds = new Set<Id>();
        
       /* String ruleQueryStringWGL = 
            'SELECT Id, Conditional_Operand_Id__c, Window_Guideline__c FROM EAW_Rule__c ' + 
            'WHERE Conditional_Operand_Id__c = \'' + recordId + '\'';
        System.debug('ruleQueryStringWGL : ' + ruleQueryStringWGL);
        
        for(EAW_Rule__c r : Database.query(ruleQueryStringWGL)){
            
            if(r.Window_Guideline__c != null){                
                wglIds.add(r.Window_Guideline__c); 
            }                        
        }
        
        String ruleDetailsQueryStringWGL = 
            'SELECT Id, Conditional_Operand_Id__c, Rule_No__r.Window_Guideline__c FROM EAW_Rule_Detail__c ' +
            'WHERE Conditional_Operand_Id__c = \'' + recordId + '\'';
        System.debug('ruleDetailsQueryStringWGL : ' + ruleDetailsQueryStringWGL);
        
        for(EAW_Rule_Detail__c rd : Database.query(ruleDetailsQueryStringWGL)){
            
            if(rd.Rule_No__r.Window_Guideline__c != null){                
                wglIds.add(rd.Rule_No__r.Window_Guideline__c);
            }                        
        }*/
        Map<Id,Set<String>> wgDateMap = new Map<Id,Set<String>>();
        Map<Id,String> wgDateStringMap = new Map<Id,String>();
        
        Map<Id, Map<Id,Set<String>>> referenceWgMap = findReferencedWindowGuidelines(new Set<Id>{recordId});
        if(referenceWgMap != null && referenceWgMap.keySet().size() > 0){   
            wgDateMap = referenceWgMap.get(recordId);
            if(wgDateMap != null && wgDateMap.keySet().size() > 0){
                for(String wgId : wgDateMap.keySet()){
                    if(wgDateMap.get(wgId) != null){
                        String dateString = '';
                        for(String dateValue : wgDateMap.get(wgId)){
                            dateString += 'Window ' + dateValue +';';
                        }
                        wgDateStringMap.put(wgId,dateString);
                    }
                }
                wrap.wgDateMap = wgDateStringMap;
                wglIds = wgDateMap.keySet();
            }
        }
        
        System.debug('wglIds : ' + wglIds);
        
        if(wglIds != null && wglIds.size() > 0){
            
            String queryStringWGL = 
                'SELECT Id, Name,EAW_Territory__c, Product_Type__c, Window_Type__c, EAW_Language__c, Status__c FROM ' + 
                'EAW_Window_Guideline__c WHERE Status__c = \'Active\' AND Id IN :wglIds ORDER BY Name';
            
            System.debug('queryStringWGL : ' + queryStringWGL);
            
            wrap.usedByWGL = Database.query(queryStringWGL);
        }
        
        return wrap;
    }
    
    public class usedByWrapper{
        
        @AuraEnabled
        public Object currentRecord;
        @AuraEnabled
        public List<sObject> usedByRDG;
        @AuraEnabled
        public List<sObject> usedByWGL;
        @AuraEnabled
        public Map<Id,String> wgDateMap;
    }
    public static Map<Id, Map<Id,Set<String>>> findReferencedWindowGuidelines( Set<Id> windowGuielineRecordIdSet ){
        
        Map<Id, Map<Id,Set<String>>> referencedWGMap = new Map<Id, Map<Id,Set<String>>>();
        
        Id widowDateRuleRecordTypeId = Schema.SObjectType.EAW_Rule__c.getRecordTypeInfosByName().get('Window Date Calculation').getRecordTypeId();
        System.debug('::: Rule WG Recordtype Id :::'+widowDateRuleRecordTypeId);
        
        List<EAW_Rule_Detail__c> ruleDetailList = [SELECT Id, Rule_No__c, Rule_No__r.Window_Guideline__c, Rule_No__r.Date_To_Modify__c,Rule_No__r.Conditional_Operand_Id__c, Conditional_Operand_Id__c FROM EAW_Rule_Detail__c
                                                   WHERE Rule_No__r.Window_Guideline__r.Status__c = 'Active' AND (Rule_No__r.Conditional_Operand_Id__c IN : windowGuielineRecordIdSet OR Conditional_Operand_Id__c IN : windowGuielineRecordIdSet) AND Rule_No__r.Window_Guideline__c != null AND Rule_No__r.RecordTypeId =: widowDateRuleRecordTypeId ];
        Set<Id> windowGuidelineSetToProcess = new Set<Id>();
        Map<Id,Set<String>> wgDateMap = new Map<Id,Set<String>>();
        Set<String> dateSet = new Set<String>();
        System.debug(':::ruleDetailList:::'+ruleDetailList);
        for( EAW_Rule_Detail__c rd : ruleDetailList ){
            if( rd.Rule_No__r.Window_Guideline__c != NULL ) {
                
                if( !String.isBlank(rd.Conditional_Operand_Id__c) && windowGuielineRecordIdSet.contains(rd.Conditional_Operand_Id__c) ) {
                   dateSet = new Set<String>();
                    if( referencedWGMap.containsKey(rd.Conditional_Operand_Id__c) ) wgDateMap = referencedWGMap.get(rd.Conditional_Operand_Id__c);
                    if(wgDateMap.containsKey(rd.Rule_No__r.Window_Guideline__c)) dateSet = wgDateMap.get(rd.Rule_No__r.Window_Guideline__c);
                    if(rd.Rule_No__r.Date_To_Modify__c != null)dateSet.add(rd.Rule_No__r.Date_To_Modify__c);
                    wgDateMap.put(rd.Rule_No__r.Window_Guideline__c,dateSet);
                    referencedWGMap.put(rd.Conditional_Operand_Id__c, wgDateMap);
                }
                
                if( !String.isBlank(rd.Rule_No__r.Conditional_Operand_Id__c) && windowGuielineRecordIdSet.contains(rd.Rule_No__r.Conditional_Operand_Id__c) ) {
                    dateSet = new Set<String>();
                    if( referencedWGMap.containsKey(rd.Rule_No__r.Conditional_Operand_Id__c) ) wgDateMap = referencedWGMap.get(rd.Rule_No__r.Conditional_Operand_Id__c);
                    if(wgDateMap.containsKey(rd.Rule_No__r.Window_Guideline__c)) dateSet = wgDateMap.get(rd.Rule_No__r.Window_Guideline__c);
                    if(rd.Rule_No__r.Date_To_Modify__c != null)dateSet.add(rd.Rule_No__r.Date_To_Modify__c);
                    wgDateMap.put(rd.Rule_No__r.Window_Guideline__c,dateSet);
                    referencedWGMap.put(rd.Rule_No__r.Conditional_Operand_Id__c, wgDateMap);
                }
                
            }
        }
        
        List<EAW_Rule__c> ruleList = [ Select Id, Window_Guideline__c,Date_To_Modify__c, Conditional_Operand_Id__c, Multi_conditional_operands__c From EAW_Rule__c Where Multi_conditional_operands__c = TRUE AND Window_Guideline__c NOT IN : windowGuidelineSetToProcess AND Window_Guideline__r.Status__c = 'Active' AND Window_Guideline__c != NULL AND RecordTypeId =: widowDateRuleRecordTypeId ];
        
        if( ruleList != NULL && ruleList.size() > 0 ){
          
          for( EAW_Rule__c ruleRec : ruleList ){
            
            if( ! windowGuidelineSetToProcess.contains(ruleRec.Window_Guideline__c) ) {
              
              if( ruleRec.Conditional_Operand_Id__c.contains(';') ){
                
                List<String> tempConditionalIdSet =  ruleRec.Conditional_Operand_Id__c.split(';');
                
                for( String operandId : tempConditionalIdSet ){
                
                  if( windowGuielineRecordIdSet.contains(operandId) ) {
                      dateSet = new Set<String>();
                      if( referencedWGMap.containsKey(operandId) ) wgDateMap = referencedWGMap.get(operandId);
                      if(wgDateMap.containsKey(ruleRec.Window_Guideline__c)) dateSet = wgDateMap.get(ruleRec.Window_Guideline__c);
                      if(ruleRec.Date_To_Modify__c != null)dateSet.add(ruleRec.Date_To_Modify__c);
                      wgDateMap.put(ruleRec.Window_Guideline__c,dateSet);
                      referencedWGMap.put(operandId, wgDateMap);
                  }
                }
              }
            }
          }
        }
        List<EAW_Rule_Detail__c> rdList = [SELECT Id, Rule_No__r.Window_Guideline__c,Rule_No__r.Date_To_Modify__c, Conditional_Operand_Id__c FROM EAW_Rule_Detail__c
                                                   WHERE Rule_No__r.Window_Guideline__r.Status__c = 'Active' AND Multi_conditional_operands__c = TRUE AND Rule_No__r.Window_Guideline__c != NULL AND Rule_No__r.Window_Guideline__c NOT IN : windowGuidelineSetToProcess AND Rule_No__r.RecordTypeId =: widowDateRuleRecordTypeId  ];
        
        if( rdList != NULL && rdList.size() > 0 ){
          
          for( EAW_Rule_Detail__c rd : rdList ){
            
            if( ! windowGuidelineSetToProcess.contains(rd.Rule_No__r.Window_Guideline__c) ) {
              
              if( rd.Conditional_Operand_Id__c.contains(';') ){
                
                List<String> tempConditionalIdSet =  rd.Conditional_Operand_Id__c.split(';');
                
                for( String operandId : tempConditionalIdSet ){
                  if( windowGuielineRecordIdSet.contains(operandId) ) {
                     dateSet = new Set<String>();
                      if( referencedWGMap.containsKey(operandId) ) wgDateMap = referencedWGMap.get(operandId);
                      if(wgDateMap.containsKey(rd.Rule_No__r.Window_Guideline__c)) dateSet = wgDateMap.get(rd.Rule_No__r.Window_Guideline__c);
                      if(rd.Rule_No__r.Date_To_Modify__c != null)dateSet.add(rd.Rule_No__r.Date_To_Modify__c);
                      wgDateMap.put(rd.Rule_No__r.Window_Guideline__c,dateSet);
                      referencedWGMap.put(operandId, wgDateMap);
                  }
                }
              }
            }
          }
        }
        
        System.debug(':::referencedWGMap:::'+referencedWGMap);
        return referencedWGMap;
    }
    
}