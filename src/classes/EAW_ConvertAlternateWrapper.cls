public class EAW_ConvertAlternateWrapper {


    public class GetTitleAlternateNameWrapper {
    
        public List<Data> data ; 
        public Links_Z links ; 
    }
    
    public class Relationships {
    
        public Title title ; 
    }
    
    public class Links_Z {
    
        public String self ; 
    }
    
    public class Attributes {
    
        public String type_Z ; 
        public String foxId ; 
        public String rowIdTitle ; 
        public String foxVersionId ; 
        public String rowIdTitleVersion ; 
        public String titleAlternateId ; 
        public String rowIdObject ; 
        public String titleAlternateTypeCode ; 
        public String titleAlternateTypeDescription ; 
        public String countryCode ; 
        public String countryDescription ; 
        public String languageCode ; 
        public String languageDescription ; 
        public String alternateDisplayTitle ; 
        public String alternateSortedTitle ; 
        public String alternateUnsortedTitle ; 
        public String titleVersionTypeCode ; 
        public String titleVersionTypeDescription ; 
        public String titleVersionDescription ; 
    
    }
    
    public class Links {
    
        public String related ; 
    }
    
    public class Title {
        public Links links ; 
    }
    
    public class Data {
    
        public String type_Z ; 
        public String id ; 
        public Relationships relationships ; 
        public Attributes attributes ; 
    }
}