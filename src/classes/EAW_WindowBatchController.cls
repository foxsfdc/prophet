global class EAW_WindowBatchController implements Database.Batchable < sObject > , Database.stateful {

    global Set < Id > windowGuidelineIds = new Set < Id > ();
    global List < EAW_Window_Guideline__c > windowGuideline = new List < EAW_Window_Guideline__c > ();
    global Set < Id > deleteWindowStarndWindowIds = new Set < Id > ();
    global List < EAW_Window__c > windowsToProcess = new List < EAW_Window__c > ();

    global EAW_WindowBatchController(Set < Id > wGIdSet, List < EAW_Window_Guideline__c > wgList) {

        System.debug('::::::wGIdSet::::' + wGIdSet);
        windowGuidelineIds = wGIdSet;
        windowGuideline = wgList;
        System.debug('::::::windowGuidelineIds ::::' + windowGuidelineIds);
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        
        String query = 'SELECT Id, EAW_Window_Guideline__c FROM EAW_Window__c WHERE Status__c != \'Firm\' AND EAW_Window_Guideline__c IN: windowGuidelineIds';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List < sObject > scope) {

        System.debug(':::scope:::' + scope);
        System.debug(':::scope::lenght:' + scope.size());

        List < EAW_Window__c > batcWindowList = new List < EAW_Window__c > ();
        Map < Id, List < EAW_Window__c >> wgIdAndWindowsMap = new Map < Id, List < EAW_Window__c >> ();
        List < EAW_Window__c > newWindows = new List < EAW_Window__c > ();
        //LRCC-1009
        Set < Id > newWindowsIds = new Set < Id > ();
        batcWindowList = scope;

        for (EAW_Window__c window: batcWindowList) {

            if (wgIdAndWindowsMap.containskey(window.EAW_Window_Guideline__c)) {

                List < EAW_Window__c > tempList = wgIdAndWindowsMap.get(window.EAW_Window_Guideline__c);
                tempList.add(window);
                wgIdAndWindowsMap.put(window.EAW_Window_Guideline__c, tempList);
            } else {

                wgIdAndWindowsMap.put(window.EAW_Window_Guideline__c, new List < EAW_Window__c > {
                    window
                });
            }
        }
        system.debug(':::wgIdAndWindowsMap:::' + wgIdAndWindowsMap);

        if (wgIdAndWindowsMap != null && wgIdAndWindowsMap.size() > 0) {

            for (Integer i = 0; i < windowGuideline.size(); i++) {

                EAW_Window_Guideline__c newWindowGuideline = windowGuideline.get(i);

                if (wgIdAndWindowsMap.get(newWindowGuideline.Previous_Version__c) != null) {

                    List < EAW_Window__c > preVersionWGWindows = wgIdAndWindowsMap.get(newWindowGuideline.Previous_Version__c);

                    for (EAW_Window__c oldWindow: preVersionWGWindows) {

                        oldWindow.EAW_Window_Guideline__c = newWindowGuideline.Id;
                        newWindows.add(oldWindow);
                        windowsToProcess.add(oldWindow);
                        newWindowsIds.add(oldWindow.Id);
                        deleteWindowStarndWindowIds.add(oldWindow.Id);
                    }
                }
            }
        }
        //deleteWindowStarndWindowIds  = newWindowsIds;
        System.debug('deleteWindowStarndWindowIds::::::::::::::::' + deleteWindowStarndWindowIds);
        system.debug(':::newWindows:::' + newWindows);
        system.debug(':::newWindowsIds:::' + newWindowsIds);

        if (newWindows != null && newWindows.size() > 0) {
            //Moved windows previous version to current version.
            Database.update(newWindows, false);
        }
    }

    global void finish(Database.BatchableContext BC) {
    	
        // Commented by LRCC - 1259 since we don't need any Rules at window level.
        /*EAW_CloneRuleAndRuleDetBatchController ewcrrbc = new EAW_CloneRuleAndRuleDetBatchController(windowsToProcess);
        Integer bcount = Integer.valueOf(Label.CloneRuleAndRuleDetailsBatchCount);
        System.debug('::::::bcount ::::' + bcount);
        database.executeBatch(ewcrrbc, bcount);*/
    }
}