public class EAW_BatchReleaseDateSoftDelete implements Database.Stateful,Database.Batchable<sObject>{
    
    public Set<Id> inActiveGuideLineIds;
    public String query = '';
    public boolean callDownstreamCalcBatchLogic = FALSE;
    public Set<Id> titleIdSet ;
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
        
        if(inActiveGuideLineIds.isEmpty() == False && String.isBlank(query)) {
            //query = 'Select Id, Release_Date__c, Title__c,Projected_Date__c,Status__c,Feed_Date__c ,Manual_Date__c From EAW_Release_Date__c WHERE Release_Date_Guideline__c IN: inActiveGuideLineIds AND Active__c=True AND ((Status__c != \'FIRM\' AND Status__c != \'Tentative\' AND Status__c != \'Estimated\') OR (Projected_Date__c != null AND Feed_Date__c = null AND Manual_Date__c = null))';   // LRCC - 1613
            query = 'SELECT Id, Active__c,Status__c, Release_Date_Guideline__c,Release_Date_Guideline__r.Active__c, Title__c FROM EAW_Release_Date__c WHERE Release_Date_Guideline__c IN: inActiveGuideLineIds ';
        }
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext bc, List<EAW_Release_Date__c> rdList) {
        
        titleIdSet = new Set<Id>();
        List<EAW_Release_Date__c> rdUpdateList = new List<EAW_Release_Date__c>();
    
        for (EAW_Release_Date__c rd: rdList) {
                    
            titleIdSet.add(rd.Title__c);
            EAW_Release_Date__c  tempRd = new EAW_Release_Date__c (id=rd.Id);
            
            if(callDownstreamCalcBatchLogic && rd.Release_Date_Guideline__r.Active__c && rd.Status__c == 'Not Released') { //LRCC-1830
                tempRd.Projected_Date__c = null;
            } else {
                tempRd.Active__c = FALSE;
                tempRd.Soft_Deleted__c = 'TRUE';
                tempRd.Send_To_Third_Party__c = TRUE;
                tempRd.Repo_DML_Type__c = 'Delete';
                tempRd.Galileo_DML_Type__c = 'Delete';
            }
            rdUpdateList.add(temprd);
        }
        
        if(rdUpdateList.size()>0) {
            update rdUpdateList;
        }
    }
    
    public void finish(Database.BatchableContext bc) {
        
        if(callDownstreamCalcBatchLogic && inActiveGuideLineIds != NULL) {
            
            EAW_DownstreamRuleGuidelinesFindUtil dependentGuidelineFindUtil = new EAW_DownstreamRuleGuidelinesFindUtil();
            Set<Id> releaseDateGuidelineIdSetToProcess = dependentGuidelineFindUtil.findDependentReleaseDateGuidelineToProcess(inActiveGuideLineIds);
            
            EAW_CreateRDForNewRDGBatch releaseDateBatch = new EAW_CreateRDForNewRDGBatch(releaseDateGuidelineIdSetToProcess,null);
            releaseDateBatch.titleIdSet = titleIdSet;
            releaseDateBatch.releaseDateGuidelineIdSet = releaseDateGuidelineIdSetToProcess;
            releaseDateBatch.windowGuidelineIdSet = NULL;
            ID batchprocessid = Database.executeBatch(releaseDateBatch, 200);
        }
        else {
            EAW_ReleaseDate_API_BatchProcess rdBatch = new EAW_ReleaseDate_API_BatchProcess();
            ID batchprocessid = Database.executeBatch(rdBatch, 100);            
        }
    }
}