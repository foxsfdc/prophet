public with sharing class EAW_TitleTagJunctionTriggerHandler {
    
    //LRCC-723 Content Owner Integration
    public Set<Id> wgIds = new Set<Id>(); 
    public Set<Id> rdgIds = new Set<Id>();
    
    //LRCC-1394 - Notification.
    //Get custom settings - ON/OFF switches for notification.
    EAW_Notification_Status__c ns = EAW_Notification_Status__c.getOrgDefaults();
    //Get chatter group Id.
    List<CollaborationGroup> lstChatterGroup = [SELECT Id, Name FROM CollaborationGroup WHERE Name = 'Title Attribute Review'];
    
    public void reQualificationCheckForAllRules(List<EAW_Title_Tag_Junction__c> newTitleTags, Boolean disQualifyFlag){
        try {
            Set<Id> titleTagIdSet = new Set<Id>();
            Set<Id> titleIdSet = new Set<Id>();
            
            for(Integer i=0;i<newTitleTags.size();i++){
                if( !checkRecursiveData.bypassDateCalculation && newTitleTags.size() == 1 && newTitleTags[i].Tag__c != NULL && newTitleTags[i].Title_Attribute__c != NULL){
                    if(checkRecursiveData.isRecursiveData(newTitleTags[i].Id)){
                        titleTagIdSet.add(newTitleTags[i].Tag__c);
                        titleIdSet.add(newTitleTags[i].Title_Attribute__c);
                    }
                }
            }
            if( titleTagIdSet.size() > 0 ){
                //if( ! deleteTriggerStatus ) { //LRCC-1394: After discussed with kumar the flag will be commented.
                    Set<Id> qualifiedTitleIdSet = processTitleTagToCheckForPlanQualification(titleTagIdSet,null,titleIdSet,disQualifyFlag);
                //}
                filterAndProcessRulesForReQualification(titleTagIdSet,titleIdSet,disQualifyFlag);
                
                system.debug('::disQualifyFlag::'+disQualifyFlag);
            }
        } catch (Exception ex){
            System.debug('<<--Exception-->>'+ex.getMessage());
        }
    }
    
    public void reQualificationCheckForAllRules(List<EAW_Title_Tag_Junction__c> newTTJList,List<EAW_Title_Tag_Junction__c> oldTTJList){
        try{
            Set<Id> titleTagIdSet = new Set<Id>();
            Set<Id> titleIdSet = new Set<Id>();
            
            if( newTTJList != NULL && newTTJList.size() > 0 ) {
                
                for( Integer i=0; i < newTTJList.size(); i++ ){
                    
                    EAW_Title_Tag_Junction__c newTTJ = newTTJList[i];
                    EAW_Title_Tag_Junction__c oldTTJ = oldTTJList[i];
                    
                    if( !checkRecursiveData.bypassDateCalculation && newTTJList.size() == 1 && newTTJ.Tag__c != oldTTJ.Tag__c || newTTJ.Title_Attribute__c != oldTTJ.Title_Attribute__c ){
                        if(checkRecursiveData.isRecursiveData(newTTJ.Id)){
                            if( newTTJ.Tag__c != NULL ) titleTagIdSet.add(newTTJ.Tag__c);
                            if( oldTTJ.Tag__c != NULL ) titleTagIdSet.add(oldTTJ.Tag__c);
                            if( newTTJ.Title_Attribute__c != NULL ) titleIdSet.add(newTTJ.Title_Attribute__c);
                            if( oldTTJ.Title_Attribute__c != NULL ) titleIdSet.add(oldTTJ.Title_Attribute__c);
                        }
                    }
                }
            }
            if( titleTagIdSet.size() > 0 ){
                Set<Id> qualifiedTitleIdSet = processTitleTagToCheckForPlanQualification(titleTagIdSet,null,titleIdSet,true);
                filterAndProcessRulesForReQualification(titleTagIdSet,titleIdSet,true);
            }
        } catch (Exception ex){
            System.debug('<<--Exception-->>'+ex.getMessage());
        }
    }
    
    //Plan Guideline Re-Qualification logic are processed in this method
    public Set<Id> processTitleTagToCheckForPlanQualification(Set<Id> titleTagIdSet, Set<Id> planGuidelineIdSet, Set<Id> titleIdSet, Boolean disQualifyFlag) {
        
        List<FeedItem> feedItems = new List<FeedItem>();
        Map<Id, String> titleIdName = new Map<Id, String> ();
        Set<Id> disQualifiedPG = new Set<Id> ();
        String bodyContent;
        
        try {
            RecordType recType = [Select Id from RecordType WHERE Name = 'Qualifier' AND SObjectType = 'EAW_Rule__c'];
            List<EAW_Rule__c> qualificationRules = new List<EAW_Rule__c>();
            
            if(planGuidelineIdSet != NULL) {
                qualificationRules = [Select Id, Plan_Guideline__c,Plan_Guideline__r.Product_Type__c, (Select Id, Territory__c, Condition_Tag__c , Condition_Field__c,Condition_Date__c, Condition_Operator__c, Condition_Criteria_Amount__c from Rule_Orders__r WHERE Condition_Field__c = 'Title Tag' ) from EAW_Rule__c WHERE RecordTypeId=: recType.Id AND Plan_Guideline__c IN :planGuidelineIdSet AND Plan_Guideline__r.Product_Type__c != NULL AND Plan_Guideline__r.Status__c = 'Active'];
            } else {
                qualificationRules = [Select Id, Plan_Guideline__c,Plan_Guideline__r.Product_Type__c, (Select Id, Territory__c, Condition_Tag__c , Condition_Field__c,Condition_Date__c, Condition_Operator__c, Condition_Criteria_Amount__c from Rule_Orders__r WHERE Condition_Field__c = 'Title Tag' ) from EAW_Rule__c WHERE RecordTypeId=: recType.Id AND Plan_Guideline__c != NULL AND Plan_Guideline__r.Product_Type__c != NULL AND Plan_Guideline__r.Status__c = 'Active'];
            }
            
            Map<Id, Set<Id>> titleAndPlanGuidelineIdMap = new Map<Id, Set<Id>>();
            Set<Id> qualifiedPlanGuidelineSet = new Set<Id>();
            
            List<EAW_Title__c> eawTitleList;
            
            if(titleTagIdSet != NULL && titleIdSet != NULL){
                eawTitleList = [SELECT Id,Name,PROD_TYP_CD_INTL__c,PROD_TYP_CD_INTL__r.Name, (SELECT Id,Name,Tag__r.Name, Title_Attribute__c FROM Title_Tag_Junctions__r WHERE Tag__c IN :titleTagIdSet) FROM EAW_Title__c WHERE Id IN :titleIdSet AND PROD_TYP_CD_INTL__c != NULL AND PROD_TYP_CD_INTL__r.Name != NULL];
            } else if(titleIdSet != NULL){
                eawTitleList = [SELECT Id,Name,PROD_TYP_CD_INTL__c,PROD_TYP_CD_INTL__r.Name, (SELECT Id,Name,Tag__r.Name, Title_Attribute__c FROM Title_Tag_Junctions__r) FROM EAW_Title__c WHERE Id IN :titleIdSet AND PROD_TYP_CD_INTL__c != NULL AND PROD_TYP_CD_INTL__r.Name != NULL];
            }
            
            for( EAW_Title__c title : eawTitleList ){
                
                for(EAW_Rule__c rule: qualificationRules){
                    
                    Boolean qualified = false;
                    
                    List<String> productTypeSet = rule.Plan_Guideline__r.Product_Type__c.toUpperCase().split(';');
                    
                    if(productTypeSet.contains(title.PROD_TYP_CD_INTL__r.Name.toUpperCase())){
                        
                        Map<Id, Set<String>> eawTitleTagMap = new Map<Id, Set<String>>();
                        for(EAW_Title_Tag_Junction__c tag : title.Title_Tag_Junctions__r)
                        {
                            Set<String> tempTagNameSet = new  Set<String>();
                            if( eawTitleTagMap.containsKey(title.Id)) tempTagNameSet = eawTitleTagMap.get(title.Id);
                            tempTagNameSet.add(tag.Tag__r.Name.toUpperCase());
                            eawTitleTagMap.put(title.Id, tempTagNameSet);
                        }
                        for(EAW_Rule_Detail__c ruleDetail: rule.Rule_Orders__r)
                        {    
                            if( ruleDetail.Condition_Field__c == 'Title Tag' &&  !String.isBlank(ruleDetail.Condition_Tag__c) && eawTitleTagMap.get(title.Id) != NULL){
                                Set<String> selectedTitleTags = new Set<String>();
                                selectedTitleTags.addAll(ruleDetail.Condition_Tag__c.toUppercase().split(';'));
                                qualified = checkTagInStatus(selectedTitleTags,eawTitleTagMap.get(title.Id));
                            }
                            
                            //LRCC-1394: Notification.
                            if(qualified == false && disQualifyFlag != null && disQualifyFlag == true) {
                                disQualifiedPG.add(rule.Plan_Guideline__c);
                                titleIdName.put(title.Id, title.Name);
                            }
                            if(qualified) break;
                        }
                    }
                    if(qualified)
                    {
                        //create a map of plan guideline and title and use the method of title plan controller to create plans and windows
                        Set<Id> planGuidelineSet = new Set<Id>();
                        if( titleAndPlanGuidelineIdMap.containsKey(title.Id) ) {
                            planGuidelineSet = titleAndPlanGuidelineIdMap.get(title.Id);
                        }
                        planGuidelineSet.add(rule.Plan_Guideline__c);
                        qualifiedPlanGuidelineSet.add(rule.Plan_Guideline__c);
                        titleAndPlanGuidelineIdMap.put(title.Id, planGuidelineSet);
                    }
                }
            }
            
            if(!lstChatterGroup.isEmpty()) {
                
                if(disQualifiedPG.size() > 0 && titleIdName.size() > 0 && ns.TACategoryNotification__c == true && ns.TATitlePlanDisQualifierNotification__c == true) {
                    
                    for(EAW_Plan__c plan: [SELECT Id, Plan_Guideline__c, Plan_Guideline__r.Name, EAW_Title__c, EAW_Title__r.Name__c FROM EAW_Plan__c WHERE Plan_Guideline__c IN: disQualifiedPG AND EAW_Title__c IN: titleIdName.keySet()]) {
                        
                        if(checkRecursiveDataforNotification.isRecursiveData(plan.Id)) {
                            
                            if(String.isBlank(bodyContent)) {
                                bodyContent = plan.EAW_Title__r.Name__c +' - disqualifies for '+plan.Plan_Guideline__r.Name+'.'+'\n'+System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+plan.Id;
                            }
                            else {
                                bodyContent += '\n\n'+ plan.EAW_Title__r.Name__c +' - disqualifies for '+plan.Plan_Guideline__r.Name+'.'+'\n'+System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+plan.Id;
                            }
                        }
                    }               
                }
                
                //LRCC-1394 - Notification.
                if(String.isNotBlank(bodyContent)) {
                
                    String temp = bodyContent;
                    
                    if(bodyContent.length() < 10000) {
                        bodyContent = temp.substring(0, temp.length());
                    } 
                    else {
                        bodyContent = temp.substring(0,9999);    
                    }
                                        
                    feedItems.add(
                        new FeedItem(
                            Body = bodyContent,
                            ParentId = lstChatterGroup[0].Id
                        )
                    );  
                            
                    if(feedItems != null && feedItems.size() > 0) {
                        insert feedItems;
                    }
                }
            }
            system.debug('::::::::::;titleAndPlanGuidelineIdMap:::::'+titleAndPlanGuidelineIdMap);
            if( titleAndPlanGuidelineIdMap != NULL && titleAndPlanGuidelineIdMap.size() > 0 ){
                //LRCC - 1239
                EAW_PlanGuidelineQualifierController.createPlanAndWindowsForQualifiedTitles(titleAndPlanGuidelineIdMap,qualifiedPlanGuidelineSet);
            }
            
            return titleAndPlanGuidelineIdMap.keyset();
        } catch(Exception ex) {
            System.debug('<<--Exception-->>'+ex.getMessage());
            return null;
        }
    }  
    
    public static Boolean checkTagInStatus( Set<String> selectedTagValues, Set<String> availableTagValues ){
        
        Boolean tagStatus = FALSE;
        
        if( availableTagValues != NULL && availableTagValues.size() > 0 && selectedTagValues != NULL && selectedTagValues.size() > 0 ){
            for( String tagVal : selectedTagValues ){
                if( availableTagValues.contains(tagVal) ){
                    tagStatus = TRUE;
                    break;
                }
            }
        }
        
        return tagStatus;
    }
    
    // Release Date Guideline and Window Guideline Re-Qualification logic are processed in this method
    public void filterAndProcessRulesForReQualification(Set<id> tagSet, Set<id> titleSet, Boolean disQualifyFlag){
        
        Set<Id> releaseDateGuidelineSet = new Set<Id>();
        Set<Id> windowGuidelineIdSet = new Set<Id>();
        
        if( titleSet != NULL && titleSet.size() > 0 ){
        
            List<EAW_Release_Date__c> releaseDateList = [SELECT Id,Release_Date_Guideline__c,Title__c FROM EAW_Release_Date__c WHERE Title__c IN : titleSet AND Release_Date_Guideline__c != NULL AND Active__c = TRUE];
            if( releaseDateList != NULL && releaseDateList.size() > 0){
                for(EAW_Release_Date__c releaseDate : releaseDateList){
                    releaseDateGuidelineSet.add(releaseDate.Release_Date_Guideline__c);
                }
            } 
            
            List<EAW_Plan__c> planList = [SELECT Id, (SELECT Id,EAW_Window_Guideline__c FROM Windows__r WHERE EAW_Window_Guideline__c != NULL),EAW_Title__c FROM EAW_Plan__c WHERE EAW_Title__c IN : titleSet ];
            if( planList != NULL && planList.size() > 0 ){
                for(EAW_Plan__c plan : planList){
                    for(EAW_Window__c window : plan.Windows__r){
                        windowGuidelineIdSet.add(window.EAW_Window_Guideline__c);
                    }
                }
            }
        }   
        
        List<EAW_Tag__c> tagList = [SELECT Id,Name FROM EAW_Tag__c WHERE Id IN :tagSet];
        
        List<String> tagNameList = new List<String>();
        
        for(EAW_Tag__c tag : tagList){
            String tagNameString = '\''+tag.Name+'\'';
            tagNameList.add(tagNameString);
        }
        
        if( tagNameList.size() > 0  && ( releaseDateGuidelineSet.size() > 0 || windowGuidelineIdSet.size() > 0 ) ){
            
            String tagType = 'Title Tag'; // It was used in dynamic soql query
            
            Set<Id> rdgIdSet = new Set<Id>();
            Set<Id> wgIdSet = new Set<Id>();
            
            //Multi-Operand selection related changes
            EAW_DownstreamRuleGuidelinesFindUtil dependentGuidelineFindUtil = new EAW_DownstreamRuleGuidelinesFindUtil();
            
            if( releaseDateGuidelineSet != NULL && releaseDateGuidelineSet.size() > 0 ) {
                rdgIdSet = dependentGuidelineFindUtil.findDependentReleaseDateGuidelineToProcessUsingTags( releaseDateGuidelineSet, tagNameList, tagType );
            }
            
            if( windowGuidelineIdSet != NULL && windowGuidelineIdSet.size() > 0 ) {
                wgIdSet = dependentGuidelineFindUtil.findDependentWindowGuidelineToProcessUsingTags( windowGuidelineIdSet, tagNameList, tagType );
            }
            //LRCC-723 Content Owner Integration
            if(wgIdSet!=null && !wgIdSet.isEmpty()){
                wgIds.addAll(wgIdSet);
            }
            //LRCC-723 Content Owner Integration
            if(rdgIdSet!=null && !rdgIdSet.isEmpty()){
                rdgIds.addAll(rdgIdSet);
            }
            
            //LRCC-1478
            /*EAW_CreateRDForNewRDGBatch releaseDateBatch = new EAW_CreateRDForNewRDGBatch(rdgIdSet, titleSet);
            releaseDateBatch.titleIdSet = titleSet;
            releaseDateBatch.releaseDateGuidelineIdSet = rdgIdSet;
            releaseDateBatch.windowGuidelineIdSet = wgIdSet;
            ID batchprocessid = Database.executeBatch(releaseDateBatch, 200);*/
            //LRCC-723 Content Owner Integration - Added if condition to skip batch class invocation.
            if( !checkRecursiveData.bypassDateCalculation){//Don't remove this if condition. As part of Content Owner integration we have added this condition.
                EAW_RDGDateCalculationBatchClass releaseDateBatch = new EAW_RDGDateCalculationBatchClass(rdgIdSet);
                releaseDateBatch.titleIdSet = titleSet;
                releaseDateBatch.windowGuidelineIdSet = wgIdSet;
                releaseDateBatch.disQualifyFlag = disQualifyFlag;
                ID batchprocessid = Database.executeBatch(releaseDateBatch, 200);
            }
        }
    }
    
    //Notifications related implementation: LRCC-1405.
    public void sentNotificationTAReviewGroup(List<EAW_Title_Tag_Junction__c> titleTagJuncs, Boolean flag) {
        
        String bodyContent = '';
        String msgBody = '';
        Set<Id> tagIds = new Set<Id> ();
        List<FeedItem> feedItems = new List<FeedItem> ();
        Map<Id, String> tagIdName = new Map<Id, String> ();
        Set<Id> titleIds = new Set<Id> ();
        Map<Id, String> titleIdName = new Map<Id, String> ();
        
        try {
            
            if(!lstChatterGroup.isEmpty() && ns.TACategoryNotification__c == true && ns.TAContentOwnerChangeNotifcation__c == true) {
                
                for(EAW_Title_Tag_Junction__c tag: titleTagJuncs) {
                    tagIds.add(tag.Tag__c);
                    titleIds.add(tag.Title_Attribute__c);
                }
                
                for(EAW_Tag__c tag: [SELECT Id, Name FROM EAW_Tag__c WHERE Id IN: tagIds]) {
                    tagIdName.put(tag.Id, tag.Name);
                }
                
                for(EAW_Title__c title: [SELECT Id, Name__c FROM EAW_Title__c WHERE Id IN: titleIds]) {
                    titleIdName.put(title.Id, title.Name__c);
                }
                
                for(EAW_Title_Tag_Junction__c tagJunction: titleTagJuncs) {
                
                    //NA-12
                    if(String.isNotBlank(tagJunction.Tag__c)) {
                        
                        if(flag == true) {
                            msgBody = 'Content Owner is added.'+'\n'+tagJunction.SFDC_Base_Instance__c+'/'+tagJunction.Id;
                        }
                        else if(flag == false && String.isBlank(msgBody)) {
                            msgBody = 'Content Owner is deleted.';
                        }
                        
                        if(String.isBlank(bodyContent)) {
                            bodyContent += titleIdName.get(tagJunction.Title_Attribute__c) +' - '+tagIdName.get(tagJunction.Tag__c)+' - '+msgBody;
                        }
                        else {
                            bodyContent += '\n\n'+ titleIdName.get(tagJunction.Title_Attribute__c) +' - '+tagIdName.get(tagJunction.Tag__c)+' - '+msgBody;
                        }
                    }
                }
                
                if(String.isNotBlank(bodyContent)) {
                    
                    String temp = bodyContent;
                    
                    if(bodyContent.length() < 10000) {
                        bodyContent = temp.substring(0, temp.length());
                    } 
                    else {
                        bodyContent = temp.substring(0,9999);    
                    }
                                        
                    feedItems.add(
                        new FeedItem(
                            Body = bodyContent,
                            ParentId = lstChatterGroup[0].Id
                        )
                    );  
                            
                    if(feedItems != null && feedItems.size() > 0) {
                        insert feedItems;
                    }
                }
            }
        }
        catch(Exception e) {}  
    }
}