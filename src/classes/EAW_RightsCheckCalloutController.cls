global with sharing class EAW_RightsCheckCalloutController {
    
    public static List<EAW_Window__c> upsertWindows;
    public static List<EAW_Error_Log__c> errorLogs;
   // public Static String performRightsCheck='Yes';
    
    /**LRCC:631 - Generic Rights check callout class**/
    public static void sentWindowstoGalilio(Set<Id> windowIds,String action) {
        
        genericRightsCheckRequestBuilder(windowIds,true,action);
    }
    public static String initiateBtnAction(Set<Id> windowIds) {
       
       return genericRightsCheckRequestBuilder(windowIds,false,'');
       
    }
    
     /*
        if Flag is false then make callout in future
    */
    Public static String genericRightsCheckRequestBuilder(Set<Id> windowIds,Boolean flag,String action){
        if(windowIds.size() > 0) {
            List<EAW_Window__c> windowsList = new List<EAW_Window__c> (
                    [    
                       SELECT
                        Customers__c, 
                        EAW_Plan__c,
                        Soft_Deleted__c, 
                        EAW_Plan__r.EAW_Title__c, 
                        EAW_Plan__r.Product_Type__c, 
                        EAW_Window_Guideline__c, 
                        EAW_Window_Guideline__r.Next_Version__c, 
                        EAW_Window_Guideline__r.Previous_Version__c, 
                        EAW_Window_Guideline__r.Status__c, 
                        EAW_Window_Guideline__r.Version__c, 
                        LastModifiedBy.name, 
                        Media__c, 
                        Retired__c, 
                        Start_Date__c, 
                        Status__c, 
                        Window_Type__c, 
                        EAW_Plan__r.id, 
                        id, 
                        EAW_Window_Guideline__r.id, 
                        name, 
                        EAW_Plan__r.name, 
                        EAW_Window_Guideline__r.name, 
                        EAW_Title_Attribute__r.id,
                        EAW_Title_Attribute__r.FIN_PROD_ID__c,
                        EAW_Title_Attribute__r.Product_Version_Id__c,
                        EAW_Title_Attribute__r.name,
                        EAW_Window_Guideline__r.Start_Date_Rule__c, 
                        End_Date__c, 
                        EAW_Window_Guideline__r.End_Date_Rule__c, 
                        Outside_Date__c, 
                        EAW_Window_Guideline__r.Outside_Date_Rule__c, 
                        Tracking_Date__c, 
                        EAW_Window_Guideline__r.Tracking_Date_Rule__c, 
                        Right_Status__c, 
                        License_Info_Codes__c, 
                        LastModifiedDate, 
                        Repo_DML_Type__c, 
                        Sent_To_Repo__c, 
                        Galileo_DML_Type__c, 
                        Sent_To_Galileo__c, 
                        Send_To_Third_Party__c, 
                        Sent_To_Galileo_RC__c, 
                        Galileo_RC_DML_Type__c,
                        Start_DATE_TEXT__C,
                        END_DATE_TEXT__C,
                        performRightsCheck__c
                    FROM
                        EAW_Window__c
                    WHERE
                        Id  in: windowIds  
                        //AND  (Start_Date__c != null OR Start_DATE_TEXT__C!=null)
                        //AND ( End_Date__c != null OR END_DATE_TEXT__C!=null or EAW_Window_Guideline__r.End_Date_Rule__c!=null) 
                        AND  Name != null //TO do - not sure about where conditions and will be handled in code level     
                    ]
                );
           Set<Id> titleIds = new Set<Id> ();
           Set<Id> wGIds = new Set<id>();
           Map<Id, EAW_Title__c> titleIdWithTitles = new Map<Id, EAW_Title__c> ();
            for(EAW_Window__c window: windowsList) {
                    titleIds.add(window.EAW_Plan__r.EAW_Title__c);
                    wGIds.add(window.EAW_Window_Guideline__c);
                }
                 
            Map<Id,EAW_Window_Guideline__c> wGMap = new Map<Id,EAW_Window_Guideline__c>(
            [select Id,
            (select Id,Name,Language__c,License_Type__c,Media__c,Territory__c,EAW_Window_Guideline__c from Window_Guideline_Strands__r WHERE  License_Type__c != NULL AND Territory__c != NULL AND Language__c != NULL AND Media__c  != NULL /*and License_Type__c='Exhibition License'*/) from EAW_Window_Guideline__c
            where id IN: wGIds
            ]);
            for(EAW_Title__c title: [SELECT Id,Name,FOX_ID__c FROM EAW_Title__c WHERE Id IN:titleIds AND FOX_ID__c != NULL]) {
                titleIdWithTitles.put(title.Id, title);
            }
            System.debug('windowsList::::'+windowsList);
            boolean skipDbCalls = false;
           genericRightsCheckRequestBuilder(windowsList,windowIds,flag,action,titleIdWithTitles,wGMap,skipDbCalls);
        }
        return null;
    }
    /*
        if Flag is false then make callout in future
    */
    Public static String genericRightsCheckRequestBuilder(List<EAW_Window__c> windowsList,Set<Id> windowIds,Boolean flag,String action,
                                                        Map<Id, EAW_Title__c> titleIdWithTitles,Map<Id,EAW_Window_Guideline__c> wGMap,boolean skipDbCalls){
        //TO do - filter the windows where Start_Date__c != NULL AND End_Date__c != NULL AND Name != NULL and build windowsList
        windowsWrapper windowsWrapper = new windowsWrapper();
        List<windowsWrapper> windowsWrapperList = new List<windowsWrapper> ();
       
        List<EAW_Window_Tag_Junction__c> wTJList = [select Id,Name,Window__c,Tag__c,Tag__r.Name from EAW_Window_Tag_Junction__c where Window__c in : windowIds];
        Map<Id,List<EAW_Window_Tag_Junction__c>> wtgMap = new Map<Id,List<EAW_Window_Tag_Junction__c>>();
        for(EAW_Window_Tag_Junction__c wtg: wtJList){
            List<EAW_Window_Tag_Junction__c> tempList = new List<EAW_Window_Tag_Junction__c>();
            if(wtgMap.get(wtg.Window__c)!=null){
                tempList = wtgMap.get(wtg.Window__c);
            }
            tempList.add(wtg);
            wtgMap.put(wtg.Window__c,tempList);
        }
        Set<String> cusNames = new Set<String>();
        for(EAW_Window__c w : windowsList){
            if(String.isnotblank(w.Customers__c))cusNames.addall(w.Customers__c.split(';'));
        }
        system.debug('cusNames: '+cusNames);
        List<EAW_Customer__c> cList = [select Id,Name,Customer_Id__c from EAW_Customer__c where Name in : cusNames];
        Map<String,EAW_Customer__c> cMap = new Map<String,EAW_Customer__c>();
        for(EAW_Customer__c c: cList){
            cMap.put(c.Name,c);
        }
          //LRCC-1653 - -In this ticket once passed QA it with remove the commented code.
        //Map<Id,List<EAW_Window_Strand__c>> windowWithWindowStrands = new Map<Id,List<EAW_Window_Strand__c>> ();
            Map<String,EAW_Error_Log__c> errorLogWithWindowMap = new  Map<String,EAW_Error_Log__c> ();
            
            /* //To do Need to work on old Error logs later
            List<EAW_Error_Log__c > existingErrorLogList = [SELECT Id,Window__c FROM EAW_Error_Log__c WHERE Window__c IN : windowIds];
            for(EAW_Error_Log__c el: existingErrorLogList ) {
            
                if(!errorLogWithWindowMap.containsKey(el.Window__c)){
                   errorLogWithWindowMap.put(el.Window__c,el);
                }
            }
            System.debug('errorLogWithWindowMap::::::'+errorLogWithWindowMap);
            */
            for(EAW_Window__c win: windowsList) {
                if(win.performRightsCheck__c 
                && (win.Start_Date__c != NULL || win.Start_DATE_TEXT__C != NULL) 
                && (win.End_Date__c!= NULL || win.End_DATE_TEXT__C != NULL || win.EAW_Window_Guideline__r.End_Date_Rule__c!=null)){
                    List<String> ExibitionLicenseMedia = new List<String> ();
                    List<String> ExibitionLicenseMediaIds = new List<String> ();
                    List<windowStrands> windowStrandsWrapperList = new List<windowStrands> ();
                    
                    String wTag = '';
                    if(wtgMap.get(win.id)!=null && !wtgMap.get(win.id).isEmpty()){
                        for(EAW_Window_Tag_Junction__c wtg: wtgMap.get(win.id)){
                            wTag+=wtg.Tag__r.Name+';';
                        }
                        wTag.removeEnd(';');
                    }else{
                        wTag = null;
                    }
                    
                    windowsWrapper.performRightsCheck=(win.performRightsCheck__c)?'Yes':'No';
                    windowsWrapper.tags=wTag;                               
                    
                    windowsWrapper.windowId = win.Id;
                    windowsWrapper.windowName = win.Name;
                    windowsWrapper.previousWindowGuidelineId = win.EAW_Window_Guideline__r.Previous_Version__c;
                    windowsWrapper.nextWindowGuidelineId = win.EAW_Window_Guideline__r.Next_Version__c;
                    if(win.EAW_Window_Guideline__r.Version__c!=null){
                        windowsWrapper.versionNo = String.valueof(win.EAW_Window_Guideline__r.Version__c);
                    }
                    windowsWrapper.updateName = win.LastModifiedBy.name;
                    windowsWrapper.rightStatus = win.Right_Status__c;
                    windowsWrapper.startDate = (win.Start_Date__c != NULL)?getRightsCheckDateFormat(win.Start_Date__c):(win.Start_DATE_TEXT__C != NULL)?getRightsCheckDateFormat(date.today()):null;//getRightsCheckDateFormat(win.Start_Date__c);//win.Start_Date__c.month()+'/'+win.Start_Date__c.day()+'/'+win.Start_Date__c.year();
                    System.debug('<<--windowsWrapper.startDate-->>'+windowsWrapper.startDate);
                    windowsWrapper.endDate = (win.End_Date__c!= NULL)?getRightsCheckDateFormat(win.End_Date__c):(win.End_DATE_TEXT__C != NULL || win.EAW_Window_Guideline__r.End_Date_Rule__c!=null)?'12/31/9999':NULL;//win.End_Date__c.month()+'/'+win.End_Date__c.day()+'/'+win.End_Date__c.year();
                    windowsWrapper.status = win.Status__c;
                   // windowsWrapper.media = (mediaMap!=null && mediaMap.get(win.Media__c)!=null)?mediaMap.get(win.Media__c).MEDIA_CD__c:formatMedia(win.Media__c,mediaMap);//win.Media__c;
                    //windowsWrapper.media = 'FTV';
                    if(win.Outside_Date__c!=null)
                        windowsWrapper.outsideDate =getRightsCheckDateFormat(win.Outside_Date__c);// win.Outside_Date__c.month()+'/'+win.Outside_Date__c.day()+'/'+win.Outside_Date__c.year();                
                    if(win.Tracking_Date__c!=null)
                        windowsWrapper.trackingDate = getRightsCheckDateFormat(win.Tracking_Date__c);//win.Tracking_Date__c.month()+'/'+win.Tracking_Date__c.day()+'/'+win.Tracking_Date__c.year();
                    windowsWrapper.windowGuidelineId = win.EAW_Window_Guideline__c;
                        
                    if(titleIdWithTitles != null && titleIdWithTitles.get(win.EAW_Plan__r.EAW_Title__c) != null) {
                        windowsWrapper.title = titleIdWithTitles.get(win.EAW_Plan__r.EAW_Title__c).Name;
                        //windowsWrapper.financialProdID = titleIdWithTitles.get(win.EAW_Plan__r.EAW_Title__c).Title_EDM__r.FIN_PROD_ID__c;
                        //windowsWrapper.productVersionId = titleIdWithTitles.get(win.EAW_Plan__r.EAW_Title__c).Title_EDM__r.FOX_VERSION_ID__c;
                        //windowsWrapper.lastUpdateDate = titleIdWithTitles.get(win.EAW_Plan__r.EAW_Title__c).Title_EDM__r.LAST_UPDATE_DATE__c;
                        //windowsWrapper.productId
                        windowsWrapper.productId = titleIdWithTitles.get(win.EAW_Plan__r.EAW_Title__c).FOX_ID__c;//titleIdWithTitles.get(win.EAW_Plan__r.EAW_Title__c).Title_EDM__r.FOX_ID__c;
                    }
                    
                    windowsWrapper.windowType = win.Window_Type__c;
                    //LRCC-631
                    windowsWrapper.licenseInfoCode = win.License_Info_Codes__c;
                    windowsWrapper.customer = win.Customers__c;
                    //windowsWrapper.customerID = win.Customer__r.Customer_Id__c;
                    if(String.isnotblank(win.Customers__c)){
                        for(String c:win.Customers__c.split(';')){
                            EAW_Customer__c cust = cMap.get(c);
                            if(cust!=null){
                                windowsWrapper.customerID += cust.Customer_Id__c+';';
                            }
                        }
                        if(String.isnotblank(windowsWrapper.customerID))windowsWrapper.customerID.removeEnd(';');
                    }
                    
                    //windowsWrapper.originalCustomerId = win.Customer__r.Customer_Id__c;
                    //windowsWrapper.originalCustomerId = 3;
                    windowsWrapper.productType = win.EAW_Plan__r.Product_Type__c;
                    windowsWrapper.retired = win.Retired__c;
                    windowsWrapper.action = action;
                    windowsWrapper.windowGuidelineStatus = win.EAW_Window_Guideline__r.Status__c;
                    //for(EAW_Window_Strand__c winstrands: windowStrands)
                    //System.debug('winstrands::::'+win.Window_Strands__r);
                    for(EAW_Window_Guideline_Strand__c winstrands : wGMap.get(win.EAW_Window_Guideline__c).Window_Guideline_Strands__r) {
                        
                        windowStrands windowStrandsWrapper = new windowStrands();
                        System.debug('winstrands::::'+winstrands);
                        windowStrandsWrapper.Id = winstrands.Id;
                        windowStrandsWrapper.media = genericMTLGalileoNames(winstrands.Media__c,'media');
                        windowStrandsWrapper.name = winstrands.Name;
                        windowStrandsWrapper.territories = genericMTLGalileoNames(winstrands.Territory__c,'territory');
                        windowStrandsWrapper.languages = genericMTLGalileoNames(winstrands.Language__c,'language'); 
                        windowStrandsWrapper.licenseType = winstrands.License_Type__c;
                        
                        windowStrandsWrapper.mediaIDs = genericMTLIdscontruction(winstrands.Media__c,'media');
                        windowStrandsWrapper.territoryIDs = genericMTLIdscontruction(winstrands.Territory__c,'territory');
                        windowStrandsWrapper.languageIDs =  genericMTLIdscontruction(winstrands.Language__c,'language'); 
                        
                        windowStrandsWrapperList.add(windowStrandsWrapper);
                       
                       // if(winstrands.License_Type__c == 'Exhibition License'){
                            
                            ExibitionLicenseMedia.add(windowStrandsWrapper.media);
                            ExibitionLicenseMediaIds.add(winstrands.Media__c);
                       // }
                    }
                    System.debug('windowStrandsWrapperList::::'+windowStrandsWrapperList);
                    System.debug('ExibitionLicenseMedia:::'+ExibitionLicenseMedia);
                    System.debug('ExibitionLicenseMediaIds:::'+ExibitionLicenseMediaIds);
                    windowsWrapper.media = formatWindowMedia(ExibitionLicenseMedia);
                    windowsWrapper.mediaID = formatWindowMediaIds(ExibitionLicenseMediaIds);
                    windowsWrapper.windowStrands = windowStrandsWrapperList;
                    windowsWrapperList.add(windowsWrapper);
                }
            }
        
        if(windowsWrapperList != null && windowsWrapperList.size() > 0) {
        
            system.debug(':::JSON::: '+JSON.serialize(windowsWrapperList));
            system.debug(':::flag::: '+flag);
            RequestWrapper requestBodyData = new RequestWrapper();
            requestBodyData.windows = windowsWrapperList;
            
            if(flag){
            
                postWindowHttpRequest(JSON.serialize(requestBodyData),JSON.serialize(errorLogWithWindowMap),skipDbCalls);
                
            }else{
            
                return genericPostHttpRequest(JSON.serialize(requestBodyData),errorLogWithWindowMap,skipDbCalls);  
            }
        }
        return null;
    }
    
    private static String getRightsCheckDateFormat(final Date input){
        
        return (input.month()>9?''+input.month():'0'+input.month())+'/'+(input.day()>9?''+input.day():'0'+input.day())+'/'+input.year();//mm/dd/yyyy
    }
    
    @future(callout=true)
    public static void postWindowHttpRequest(String body,String errorLogWithWindowMap,boolean skipDbCalls) {
        
            genericPostHttpRequest(body,(Map<String,EAW_Error_Log__c>)JSON.deserialize(errorLogWithWindowMap,Map<String,EAW_Error_Log__c>.class),skipDbCalls);
    }
    
    public static String genericPostHttpRequest(String body,Map<String,EAW_Error_Log__c> errorLogWithWindowMap,boolean skipDbCalls) {
        system.debug(body);
        String endPointURL;
        if( Rest_Endpoint_Settings__c.getInstance('RightsCheck Galileo Instance') != NULL) {
                
            endPointURL =  Rest_Endpoint_Settings__c.getInstance('RightsCheck Galileo Instance').End_Point__c;
        }
        
        //if(endPoints.size() > 0 && endPoints[0].End_Point__c != null) {
        system.debug(':::endPointURL::: '+endPointURL);
        if(!String.isBlank(endPointURL)) {
        
           // endPointURL = endPoints[0].End_Point__c;
            String authToken = connect();
            system.debug('TOKEN:'+authToken);
           
            
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            HTTPResponse res = new HTTPResponse();
            request.setMethod('POST');
            request.setHeader('Content-Type', 'application/json');
            request.setHeader('Accept', 'application/json');
            request.setHeader('Authorization', 'Bearer ' + authToken);
            request.setTimeout(120000);
            System.debug('body:::::::req :'+system.JSON.serialize(body));
            request.setBody(body);
            request.setEndpoint(endPointURL);//'https://feg-devapi.foxinc.com/galileo-rights-check/api/prophet/rightsCheck');
             
            system.debug('REQUEST:'+request);
            system.debug('Request Body:'+request.getBody());            
            
            if(!Test.isRunningTest()) {
                    
                //Execute web service call here
                try{
                    res = http.send(request);
                    
                    system.debug('RESPONSE:'+res);
                    system.debug('RESPONSE BODY:'+res.getBody());
                    
                    if(res.getStatusCode() == 200){
                        
                        List<windowsWrapper> win = new List<windowsWrapper> ();
                        List<EAW_Window__c> windows = new List<EAW_Window__c> ();
                        List<EAW_Error_Log__c > elList =  new List<EAW_Error_Log__c >();
                        
                        RequestWrapper reqwrap = (RequestWrapper)JSON.deserialize(body,RequestWrapper.class);
                        win = reqwrap.windows;//(List<windowsWrapper>)JSON.deserialize(body, List<windowsWrapper>.class);
                       
                        for(windowsWrapper w: win) {
                            
                            EAW_Window__c tempwindows =  new EAW_Window__c ();
                            tempwindows.Id = w.windowId;
                            tempwindows.Right_Status__c = 'Processing';
                            windows.add(tempwindows);
                        }
                        system.debug('::windows::'+windows);
                        
                        if(win.size() > 0 && !skipDbCalls) {
                            checkRecursiveData.bypassDateCalculation=true;
                            update windows;
                        }else if(win.size() > 0){
                            upsertWindows.addall(windows);
                        }
                    }else{
                        
                        ResponseWrapper errorResponse  = (ResponseWrapper)JSON.deserialize(res.getBody(), ResponseWrapper.class);
                        upsertErrorlogs(body,errorLogWithWindowMap,errorResponse.message,String.ValueOf(res.getStatusCode()),skipDbCalls);
                        return JSON.serialize((ResponseWrapper)JSON.deserialize(res.getBody(), ResponseWrapper.class));
                    }
                }catch(System.CalloutException e) {
                
                    System.debug('e:::::::::::::::::'+e.getMessage());
                    upsertErrorlogs(body,errorLogWithWindowMap,e.getMessage(),NULL,skipDbCalls);
                }
                
            } else {}       
        }
        return  null;
       
    }
   
    public static String connect() {
        
        Rest_Endpoint_Settings__c RightsCheckAuthendicationDetails;
        RightsCheckAuthendicationDetails = Rest_Endpoint_Settings__c.getInstance('RightsCheckAuthendicationDetails'); 
        String key = RightsCheckAuthendicationDetails.Client_Id__c;//'SDMGL7LVr0iLS1wkpDxIQ8z3eQuLJfqA';
        String secret= RightsCheckAuthendicationDetails.Client_Secret__c;//'cjYt3pMHgnFhMoia';
        String token= RightsCheckAuthendicationDetails.End_Point__c;//'https://ms-devapi.foxinc.com/oauth/oauth20/token';
        
        String grant_type = RightsCheckAuthendicationDetails.Grant_Type__c;//'client_credentials';
        String requestBody = 'grant_type=' + grant_type +'&client_id=' + key + '&client_secret=' + secret;
        HttpRequest req = new HttpRequest();
        req.setEndpoint(token);
        req.setHeader('Content-Type','application/x-www-form-urlencoded');
        //req.setHeader('Host', host);
        req.setHeader('Connection', ' Keep-Alive');
        req.setHeader('scope', 'read');
        req.setMethod('POST');
        req.setBody(requestBody);
        Http http = new Http();
        HTTPResponse response;
        
        if(!Test.isRunningTest()) {
            response = http.send(req);
        } 
        else {}
        
        responseClass rc = (responseClass)JSON.deserialize(response.getBody(),responseClass.class);
        //system.debug('Response Body:'+responseBody);
        String tokenSetter = 'Not Set';
        tokenSetter = rc.access_token;
    
        return tokenSetter;
    }
    public static String genericMTLIdscontruction(String values,String flag) {
        
        System.debug('values::MTL:'+values);
        System.debug('flag::MTL:'+flag);
        EAW_Media__c eawMedia ;
        EAW_Territories__c eawTerritories;
        EAW_Languages__c eawLanguage;
        
        if (values == null) return null;         
        List<String> valueList = new List<String>();
        
        for(String mtl : values.split(';')){
            
            Decimal mtlId ;
            if(flag == 'media' && EAW_Media__c.getInstance(mtl) != NULL){
                
                mtlId = EAW_Media__c.getInstance(mtl).Media_Id__c;
                
            } else if(flag == 'territory' && EAW_Territories__c.getInstance(mtl) != NULL) {
                
                mtlId = EAW_Territories__c.getInstance(mtl).Territory_Id__c;
            } else if(flag == 'language' && EAW_Languages__c.getInstance(mtl) != NULL){
                
                mtlId = EAW_Languages__c.getInstance(mtl).Language_Id__c;
            }
            System.debug('mtlId ::MTL:'+mtlId );
            if(mtlId != NULL){ 
                
                valueList.add(String.valueOf(Integer.valueOf(mtlId.setScale(0))));
            }
        }
       
        return String.join(valueList, ';');
    }
    
    public static String genericMTLGalileoNames(String values,String flag) {
        
        System.debug('values::MTL:'+values);
        System.debug('flag::MTL:'+flag);
        EAW_Media__c eawMedia ;
        EAW_Territories__c eawTerritories;
        EAW_Languages__c eawLanguage;
        
        if (values == null) return null;         
        List<String> valueList = new List<String>();
        
        for(String mtl : values.split(';')){
            
            String mtlName ;
            if(flag == 'media' && EAW_Media__c.getInstance(mtl) != NULL){
                
                mtlName = EAW_Media__c.getInstance(mtl).Galileo_Media_Code__c;
                
            } else if(flag == 'territory' && EAW_Territories__c.getInstance(mtl) != NULL) {
                
                mtlName = EAW_Territories__c.getInstance(mtl).Galileo_Territory_Name__c;
            } else if(flag == 'language' && EAW_Languages__c.getInstance(mtl) != NULL){
                
                mtlName = EAW_Languages__c.getInstance(mtl).Galileo_Language_Name__c;
            }
            System.debug('mtlName ::MTL:'+mtlName );
            if(mtlName != NULL){ 
                
                valueList.add(mtlName);
            }
        }
       
        return String.join(valueList, ';');
    }
    
     public static String formatWindowMedia(List<String> values) {
     
        if (values == null) return null;
        return String.join(values, ';');
    }
    
    public static String formatWindowMediaIds(List<String> values) {
        
        System.debug('formatWindowMediaIds:::values::'+values);
        if (values == null) return null; 
        
        List<String> mediaList = new List<String>();
        for(String med : String.join(values, ';').split(';')){
            
            System.debug('EAW_Media__c.getInstance(med)::'+EAW_Media__c.getInstance(med));
            if(EAW_Media__c.getInstance(med) != NULL && EAW_Media__c.getInstance(med).Media_Id__c != NULL){ 
                
                mediaList.add(String.valueOf(Integer.valueOf(EAW_Media__c.getInstance(med).Media_Id__c.setScale(0))));
            }
        }
       
        return String.join(mediaList, ';');
    }
    /*
    public static String formatMedia(String values,Map<String,EDM_REF_MEDIA__c>mediaMap) {
        
        System.debug('values:::'+values);
        if (values == null) return null; 
        
        List<String> mediaList = new List<String>();
        for(String med : values.split(';')){
            
            if(mediaMap.containsKey(med) && mediaMap.get(med).MEDIA_CD__c != NULL){ 
                
                mediaList .add(mediaMap.get(med).MEDIA_CD__c);
            }
        }
       
        return String.join(mediaList, ';');
    }*/
    public static void upsertErrorlogs(String body,Map<String,EAW_Error_Log__c> errorLogWithWindowMap,String errorMessage,String errorCode,boolean skipDbCalls){
    
        List<EAW_Error_Log__c > elList =  new List<EAW_Error_Log__c >();
        List<windowsWrapper> win = new List<windowsWrapper> ();
        RequestWrapper reqwrap = (RequestWrapper)JSON.deserialize(body,RequestWrapper.class);
        win = reqwrap.windows;  
        for(windowsWrapper w: win) {
        
             EAW_Error_Log__c el = new EAW_Error_Log__c();
             el.Error_Code__c = errorCode;
             el.Error_Message__c = errorMessage;  
             el.Window__c = w.windowId;
             if(el.Window__c != NULL){
             
                 elList.add(el);
             }
        }
        
        system.debug('::elList::'+elList);
        
        if(elList.size() > 0 && !skipDbCalls) {
            //for prevant sandbox storage limit exception.
            //insert elList;
        }else if(elList.size() > 0){
                errorLogs.addAll(elList);
         }
    }
    public class responseClass {
    
        public responseClass() {}
        public String access_token;
        public String token_type;
        public String expires_in;
    }
    
    public class WindowStrands {
    
        public String Id;
        public String name;
        public String media;//06072019 KM: it should be medias
        public String territories;
        public String languages;
        public String licenseType;
        public String mediaIDs;
        public String territoryIDs;
        public String languageIDs;
    }
    public class WindowsWrapper {
    
        public String windowId;
        public String windowGuidelineId;
        public String previousWindowGuidelineId;
        public String nextWindowGuidelineId;
        public String versionNo ;
        public String windowName;
        public String startDate;
        public String endDate;
        public String outsideDate;
        public String trackingDate;
        public String status;
        public String media;
        public String mediaID;
        public String title;
        public String productId;
       // public String foxID;
        public String windowType;
        public String licenseInfoCode;
        public String productVersionId;
        public String Customer;//06072019 KM: it should be customers
        //public String originalCustomerId;
        public String customerID;//06072019 KM: it should be originalCustomerId
        public String productType;
        public Boolean retired;
        public String action;
        public List<WindowStrands> windowStrands;
        public String windowGuidelineStatus;
        //06072019
        public String performRightsCheck;
        public String rightStatus;
        public String tags;
        public String availableFlag;
        public String updateName;
        
        
        
        
    }
    
    public class RequestWrapper {

        public List<WindowsWrapper> windows;
    }
    
    public class ResponseWrapper {
        
        public String timestamp;
        public String message;
        public List<String> fieldErrors;
    }
   
   
}