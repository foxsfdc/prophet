//LRCC-1707, LRCC-631, LRCC-1783
public with sharing class EAW_Windows_Outbound {
    boolean Skip_GALILEO_RIGHTS_CHECK_API = false;
    List<EAW_Error_Log__c> repoErrorLogs  = new List<EAW_Error_Log__c>();
    set<id> repoSuccessIds= new set<id>();
    Set<id> repoIgnoredIds = new Set<id>();
    //No need to send data separately to Galileo: Galileo currently also gets Release Dates from Prophet using a Kafka pipeline that sources Release Dates from a Oracle View
    //This information will be extracted from Repo based Release Date view for Prophet Sales Force
   /*List<EAW_Error_Log__c> galileoErrorLogs  = new List<EAW_Error_Log__c>();
    set<id> galileoSuccessIds= new set<id>();
    Set<id> galileoIgnoredIds = new Set<id>();*/
    
    List<EAW_Error_Log__c> galileoRCErrorLogs  = new List<EAW_Error_Log__c>();
    set<id> galileoRCSuccessIds= new set<id>();
    Set<id> galileoRCIgnoredIds = new Set<id>();

    String repoUrl = '';
	/*String galileoUrl = '';*/
	
    public EAW_Windows_Outbound(){
		getUrls();
    }
    public EAW_Windows_Outbound(boolean Skip_GALILEO_RIGHTS_CHECK_API){
        this.Skip_GALILEO_RIGHTS_CHECK_API=Skip_GALILEO_RIGHTS_CHECK_API;
        getUrls();
    }
    
    private void getUrls(){
		Rest_Endpoint_Settings__c repoInstance = Rest_Endpoint_Settings__c.getInstance('Outbound-W-to-Repo');
		if(repoInstance!=null){
			repoUrl=repoInstance.End_Point__c;
		}
		/*Rest_Endpoint_Settings__c galileoInstance = Rest_Endpoint_Settings__c.getInstance('Outbound-Window-to-Galileo');
		if(galileoInstance!=null){
			galileoUrl=galileoInstance.End_Point__c;
		}*/
    	
    }

    public class WindowJson{
        public String planId;
        public String windowId;
        public String windowGuidelineId;
        public String windowName;
        public String planGuidelineName;
        public String windowGuidelineName;
        public String financialProdId;
        public String foxId;
        public String productVersionId;
        public String title;//converted from titleId to title on 06202019
        public String windowType;
        public String windowStatus;
        public String retired;
        public String deleted;
        public String startDate;
        public String startDateText;
        public String endDate;
        public String endDateText;
        public String outsideDate;
        public String outsideDateText;
        public String trackingDate;
        public String trackingDateText;
        public String rightStatus;
        public String licensingInfoCodes;
        public String windowTags;
        public String updateDate;
        public String operation;
        
        public WindowJson(String planId,String windowId,String windowGuidelineId,String windowName,String planGuidelineName,String windowGuidelineName,
                                    String financialProdId,String foxId,String productVersionId,String title,String windowType,String windowStatus,
                                    boolean retired,String deleted,String startDate,String startDateText,String endDate,String endDateText,String outsideDate,
                                    String outsideDateText,String trackingDate,String trackingDateText,String rightStatus,
                                    String licensingInfoCodes,String windowTags,String updateDate,String operation){
        this.planId=planId;
        this.windowId=windowId;
        this.windowGuidelineId=windowGuidelineId;
        this.windowName=windowName;
        this.planGuidelineName=planGuidelineName;
        this.windowGuidelineName=windowGuidelineName;
        
        this.financialProdId=financialProdId;
        this.foxId=foxId;
        this.productVersionId=productVersionId;
        this.title=title;
        this.windowType=windowType;
        this.windowStatus=windowStatus;
        
        if(retired){
        	this.retired='Y';
        }else{
        	this.retired='N';
        }
        
        if(deleted=='TRUE'){
        	this.deleted='Y';
        }else{
        	this.deleted='N';
        }

        this.startDate=startDate;
        this.startDateText=startDateText;
        this.endDate=endDate;
        this.endDateText=endDateText;
        this.outsideDate=outsideDate;
        this.outsideDateText=outsideDateText;
        this.trackingDate=trackingDate;
        this.trackingDateText=trackingDateText;
        
        this.rightStatus=rightStatus;
        this.licensingInfoCodes=licensingInfoCodes;
        this.windowTags=windowTags;
        this.updateDate=updateDate;
        this.operation=operation;
        }
    }
    private void getWindowJson(EAW_Outbound_Utility.OutboundSystem os, List<EAW_Window__c> windowList){
        try{
            Set<Id> tempWindowIds = new Set<Id>();

            /*List<List<WindowJson>> wJsonListOfList = new List<List<WindowJson>>();
            List<List<id>> wIdListOfList = new List<List<id>>();
            List<List<EAW_Window__c>> wListOfList = new List<List<EAW_Window__c>>();*/
            
            for(EAW_Window__c w:windowList){
                tempWindowIds.add(w.id);
            }
            
            List<EAW_Window_Tag_Junction__c> wtjList = [SELECT Id,Name,Window__c,Tag__c,Tag__r.Name FROM EAW_Window_Tag_Junction__c where Window__c in :tempWindowIds];
            Map<Id,List<EAW_Window_Tag_Junction__c>> wtMap = new Map<Id,List<EAW_Window_Tag_Junction__c>>();
    
            for(EAW_Window_Tag_Junction__c wtg: wtjList){
                List<EAW_Window_Tag_Junction__c> tempList = new List<EAW_Window_Tag_Junction__c>();
                if(wtMap.get(wtg.Window__c)!=null){
                    tempList = wtMap.get(wtg.Window__c);
                }
                tempList.add(wtg);
                wtMap.put(wtg.Window__c,tempList);
            }
            List<WindowJson> wJsonList = new List<WindowJson>();
            List<id> wIdList = new List<id>();
            
            for(EAW_Window__c w:windowList){
                String wTag = '';
                if(wtMap.get(w.id)!=null && !wtMap.get(w.id).isEmpty()){
                    for(EAW_Window_Tag_Junction__c wtg: wtjList){
                        wTag+=wtg.Tag__r.Name+';';
                    }
                    wTag.removeEnd(';');
                }else{
                    wTag = null;
                }
                
                wJsonList.add(new WindowJson(''+w.EAW_Plan__r.id,''+w.id,''+w.EAW_Window_Guideline__r.id,w.name,w.EAW_Plan__r.name,w.EAW_Window_Guideline__r.name,
                            w.EAW_Title_Attribute__r.FIN_PROD_ID__c,w.EAW_Title_Attribute__r.FOX_ID__c,w.EAW_Title_Attribute__r.Product_Version_Id__c,w.EAW_Title_Attribute__r.name,w.Window_Type__c,w.Status__c,
                            w.Retired__c,w.Soft_Deleted__c,EAW_Outbound_Utility.getFormattedDate(w.Start_Date__c),w.EAW_Window_Guideline__r.Start_Date_Rule__c,EAW_Outbound_Utility.getFormattedDate(w.End_Date__c),w.EAW_Window_Guideline__r.End_Date_Rule__c,
                            EAW_Outbound_Utility.getFormattedDate(w.Outside_Date__c),w.EAW_Window_Guideline__r.Outside_Date_Rule__c,EAW_Outbound_Utility.getFormattedDate(w.Tracking_Date__c),w.EAW_Window_Guideline__r.Tracking_Date_Rule__c,
                            w.Right_Status__c,w.License_Info_Codes__c,wTag,EAW_Outbound_Utility.getFormattedDateTime(w.LastModifiedDate),w.Repo_DML_Type__c));
                wIdList.add(w.id);
                /*if(wJsonList.size()==EAW_Outbound_Utility.MAX_API_RECORDS_LIMIT){
                    wJsonListOfList.add(wJsonList);
                    wIdListOfList.add(wIdList);
                    
                    wJsonList = new List<WindowJson>();
                    wIdList = new List<id>();
                }*/
            }
            /*if(wJsonList!=null && !wJsonList.isEmpty()){
                wJsonListOfList.add(wJsonList);
                wIdListOfList.add(wIdList);
            }*/
            if(wJsonList!=null && !wJsonList.isempty()){
                /*for(integer i=0;i<wJsonListOfList.size();i++){
                    List<WindowJson> tempWJsonList = wJsonListOfList[i];
                    List<id> tempIdList = wIdListOfList[i];*/
                    EAW_Outbound_Utility eouRepo = new EAW_Outbound_Utility(EAW_Outbound_Utility.OutboundDataType.Window,os,wIdList);
                    if(/*os==EAW_Outbound_Utility.OutboundSystem.Repo && */String.isNotBlank(repoUrl)){
                        eouRepo.sendDataThroughWebAPI( system.json.serialize(wJsonList),repoUrl,repoErrorLogs,repoSuccessIds);
                    /*}else if (os==EAW_Outbound_Utility.OutboundSystem.Galileo && String.isNotBlank(galileoUrl)){
                        eouRepo.sendDataThroughWebAPI( system.json.serialize(tempWJsonList),galileoUrl,galileoErrorLogs,galileoSuccessIds);*/
                    }
                /*}*/
            }
        }catch(exception e){
            system.debug('EAW_Windows_Outbound Exception: '+e.getmessage());
        }
    }
    
    private void doRightsCheck(List<EAW_Window__c> windowList){
        Set<Id> titleIds = new Set<Id>();
        Set<Id> wGIds = new Set<Id>();

        /*List<List<id>> wIdListOfList = new List<List<id>>();
        List<List<EAW_Window__c>> wListOfList = new List<List<EAW_Window__c>>();*/

        List<id> wIdList = new List<id>();
        List<EAW_Window__c> wList = new List<EAW_Window__c>();
        
        for(EAW_Window__c w:windowList){
            titleIds.add(w.EAW_Plan__r.EAW_Title__c);
            wGIds.add(w.EAW_Window_Guideline__c);
            wList.add(w);
            wIdList.add(w.Id);
                
            /*if(wIdList.size()==EAW_Outbound_Utility.MAX_API_RECORDS_LIMIT){
                wIdListOfList.add(wIdList);
                wListOfList.add(wList);
                    
                wIdList = new List<id>();
                wList = new List<EAW_Window__c>();
            }*/
                
        }
        /*if(wIdList!=null && !wIdList.isEmpty()){
            wIdListOfList.add(wIdList);
            wListOfList.add(wList);
        }*/
        
        Map<Id, EAW_Title__c> titleIdWithTitles = new Map<Id, EAW_Title__c>([SELECT Id,Name,FOX_ID__c FROM EAW_Title__c WHERE Id IN:titleIds AND FOX_ID__c != NULL]);
        Map<Id,EAW_Window_Guideline__c> wGMap = new Map<Id,EAW_Window_Guideline__c>(
        [select Id,
        (select Id,Name,Language__c,License_Type__c,Media__c,Territory__c,EAW_Window_Guideline__c from Window_Guideline_Strands__r WHERE  License_Type__c != NULL AND Territory__c != NULL AND Language__c != NULL AND Media__c  != NULL and License_Type__c='Exhibition License') from EAW_Window_Guideline__c
        where id IN: wGIds
        ]);
        
        if(wList!=null && !wList.isempty()){
            /*for(integer i=0;i<wListOfList.size();i++){
                List<id> tempIdList = wIdListOfList[i];
                List<EAW_Window__c> wGRCList = wListOfList[i];*/
                EAW_RightsCheckCalloutController.genericRightsCheckRequestBuilder(wList, new Set<Id>(wIdList),false,'',titleIdWithTitles,wGMap,true);
                if(EAW_RightsCheckCalloutController.errorLogs!=null && !EAW_RightsCheckCalloutController.errorLogs.isempty()){
                    galileoRCErrorLogs.addall(EAW_RightsCheckCalloutController.errorLogs);
                }else if(EAW_RightsCheckCalloutController.upsertWindows!=null && !EAW_RightsCheckCalloutController.upsertWindows.isempty()){
                    galileoRCSuccessIds.addAll(wIdList);
                }
            /*}*/
        }
    }
    
    public void aggregateAllWs(List<EAW_Window__c> records){
        List<EAW_Window__c> repoRecords = new List<EAW_Window__c>();
        /*List<EAW_Window__c> galileoRecords = new List<EAW_Window__c>();*/
        List<EAW_Window__c> galileoRCRecords = new List<EAW_Window__c>();
        
        List<EAW_Window__c> updateList = new List<EAW_Window__c>();
        List<EAW_Window__c> deleteList = new List<EAW_Window__c>();
        List<EAW_Error_Log__c> insertErrorList = new List<EAW_Error_Log__c>();
        
        if(records!=null && !records.isempty()){
            
            for(EAW_Window__c w:records){
                boolean isSoftDeletedRecord = w.Soft_Deleted__c== 'TRUE'; //w.Status__c=='Inactive';  
                
                if(String.isBlank(w.Repo_DML_Type__c)){
            		if(isSoftDeletedRecord){
                		w.Repo_DML_Type__c='Delete';
                	} else if(w.Sent_To_Repo__c){
    					w.Repo_DML_Type__c='Update';
    				}else{
    					w.Repo_DML_Type__c='Insert';
    				}
            	}  else if(w.Repo_DML_Type__c == 'Update' && !w.Sent_To_Repo__c){
            		w.Repo_DML_Type__c='Insert';
            	}
                
                if(w.Sent_To_Repo__c || !isSoftDeletedRecord){
                    repoRecords.add(w);
                }else if(!w.Sent_To_Repo__c || isSoftDeletedRecord){
                    repoIgnoredIds.add(w.id);
                }
                
                /*if(w.Sent_To_Galileo__c || !isSoftDeletedRecord){
                    galileoRecords.add(w);
                }else if(!w.Sent_To_Galileo__c || isSoftDeletedRecord){
                    galileoIgnoredIds.add(w.id);
                }*/
                
                if(w.Sent_To_Galileo_RC__c || !isSoftDeletedRecord){
                    galileoRCRecords.add(w);
                }else if(!w.Sent_To_Galileo_RC__c || isSoftDeletedRecord){
                    galileoRCIgnoredIds.add(w.id);
                }
            }
            if(!repoRecords.isEmpty() && String.isNotBlank(repoUrl)){
                getWindowJson(EAW_Outbound_Utility.OutboundSystem.Repo,repoRecords);
            }
            /*if(!galileoRecords.isEmpty() && String.isNotBlank(galileoUrl)){
                getWindowJson(EAW_Outbound_Utility.OutboundSystem.Galileo,galileoRecords);
            }*/
            /*
            if(!galileoRCRecords.isEmpty() && !this.Skip_GALILEO_RIGHTS_CHECK_API){
                doRightsCheck(galileoRCRecords);
            }*/
            
            for(EAW_Window__c w:records){
                    boolean canBeDeleted = false;
                    boolean isSoftDeletedRecord = w.Soft_Deleted__c== 'TRUE'; //w.Status__c=='Inactive';
                    /*if((repoSuccessIds.contains(w.id) && galileoSuccessIds.contains(w.id) && galileoRCSuccessIds.contains(w.id))//abc //All are success
                       || (repoIgnoredIds.contains(w.id) && galileoIgnoredIds.contains(w.id) && galileoRCIgnoredIds.contains(w.id))//ABC //If record is inactive and didn't send to all the apis then we can delete without sending again to failed API
                       || (repoSuccessIds.contains(w.id) && galileoIgnoredIds.contains(w.id) && galileoRCIgnoredIds.contains(w.id))//aBC  
                        || (repoSuccessIds.contains(w.id) && galileoIgnoredIds.contains(w.id) && galileoRCSuccessIds.contains(w.id))//aBc
                        || (repoSuccessIds.contains(w.id) && galileoSuccessIds.contains(w.id) && galileoRCIgnoredIds.contains(w.id))//abC
                        || (repoIgnoredIds.contains(w.id) && galileoSuccessIds.contains(w.id) && galileoRCSuccessIds.contains(w.id))//Abc 
                        || (repoIgnoredIds.contains(w.id) && galileoSuccessIds.contains(w.id) && galileoRCIgnoredIds.contains(w.id))//AbC  
                        || (repoIgnoredIds.contains(w.id) && galileoIgnoredIds.contains(w.id) && galileoRCSuccessIds.contains(w.id))//ABc
                        ){
                        w.Send_To_Third_Party__c=false;
                        canBeDeleted = true; // Can be deleted
                    }*/
                    if((repoSuccessIds.contains(w.id) && galileoRCSuccessIds.contains(w.id)) //Both are success
                    || (repoSuccessIds.contains(w.id) && galileoRCIgnoredIds.contains(w.id)) // repo is success and galileo is failed even one time 
                        || (repoIgnoredIds.contains(w.id) && galileoRCSuccessIds.contains(w.id)) // galileo is success and repo is failed even one time 
                        || (repoIgnoredIds.contains(w.id) && galileoRCIgnoredIds.contains(w.id))//If record is inactive and didn't send to both api then we can delete without sending them
                        ){
                        w.Send_To_Third_Party__c=false;
                        canBeDeleted = true; // Can be deleted
                    }
                    if(repoSuccessIds.contains(w.id)){//Repo call is success then repo DML type is blank and sent to repo should be true
                        w.Sent_To_Repo__c=true;
                        w.Repo_DML_Type__c='';
                        /*//To do: Once galileo RC is ready remove below 2 lines*/
                    	w.Send_To_Third_Party__c=false;
                    	canBeDeleted = true;
                    	/********************************/
                    }
                    /*if(galileoSuccessIds.contains(w.id)){//Galileo call is success then Galileo DML type is blank and sent to Galileo should be true
                        w.Sent_To_Galileo__c=true;
                        w.Galileo_DML_Type__c='';
                    }*/
                    if(galileoRCSuccessIds.contains(w.id)){//Galileo RC call is success then Galileo RC DML type is blank and sent to Galileo RC should be true
                    	w.Sent_To_Galileo_RC__c=true;
                        w.Galileo_RC_DML_Type__c='';
                        //w.Right_Status__c = 'Processing';
                    }
                    
                    if(isSoftDeletedRecord && canBeDeleted){
                        deleteList.add(w);
                    }else{
                        updateList.add(w);
                    }
            }
            if(repoErrorLogs!=null && !repoErrorLogs.isEmpty())insertErrorList.addAll(repoErrorLogs);
            /*if(galileoErrorLogs!=null && !galileoErrorLogs.isEmpty())insertErrorList.addAll(galileoErrorLogs);*/
            if(galileoRCErrorLogs!=null && !galileoRCErrorLogs.isEmpty())insertErrorList.addAll(galileoRCErrorLogs);
        }
        if(updateList!=null && !updateList.isEmpty()){
            update updateList;
        }
        if(deleteList!=null && !deleteList.isEmpty()){
            delete deleteList;
        }
        if(insertErrorList!=null && !insertErrorList.isEmpty()){
            insert insertErrorList;
        }
        //To do Skip downstream calculation of Windows
        //if response 200 then Send_To_Third_Party - False, Dml_Type as blank
        //Sent_To_Repo-true
        //Sent_To_Galileo-true
    }
    
    private void sendData(Set<Id> windowIds){
        List<EAW_Window__c> windows = [SELECT
											Customers__c, 
											EAW_Plan__c,
											Soft_Deleted__c, 
											EAW_Plan__r.EAW_Title__c, 
											EAW_Plan__r.Product_Type__c, 
											EAW_Window_Guideline__c, 
											EAW_Window_Guideline__r.Next_Version__c, 
											EAW_Window_Guideline__r.Previous_Version__c, 
											EAW_Window_Guideline__r.Status__c, 
											EAW_Window_Guideline__r.Version__c, 
											LastModifiedBy.name, 
											Media__c, 
											Retired__c, 
											Start_Date__c, 
											Status__c, 
											Window_Type__c, 
											EAW_Plan__r.id, 
											id, 
											EAW_Window_Guideline__r.id, 
											name, 
											EAW_Plan__r.name, 
											EAW_Window_Guideline__r.name, 
											EAW_Title_Attribute__r.id,
											EAW_Title_Attribute__r.FIN_PROD_ID__c,
											EAW_Title_Attribute__r.FOX_ID__c,
							                EAW_Title_Attribute__r.Product_Version_Id__c,
							                EAW_Title_Attribute__r.name,
											EAW_Window_Guideline__r.Start_Date_Rule__c, 
											End_Date__c, 
											EAW_Window_Guideline__r.End_Date_Rule__c, 
											Outside_Date__c, 
											EAW_Window_Guideline__r.Outside_Date_Rule__c, 
											Tracking_Date__c, 
											EAW_Window_Guideline__r.Tracking_Date_Rule__c, 
											Right_Status__c, 
											License_Info_Codes__c, 
											LastModifiedDate, 
											Repo_DML_Type__c, 
											Sent_To_Repo__c, 
											Galileo_DML_Type__c, 
											Sent_To_Galileo__c, 
											Send_To_Third_Party__c, 
											Sent_To_Galileo_RC__c, 
											Galileo_RC_DML_Type__c,
											Start_DATE_TEXT__C,
											END_DATE_TEXT__C,
											performRightsCheck__c
										FROM
											EAW_Window__c
										WHERE
											id in: windowIds];
        aggregateAllWs(windows);
    }
    
    @future(callout=true)
    public static void sendDataAsync(Set<id> windowIds){
        EAW_Windows_Outbound ewo = new EAW_Windows_Outbound();
        ewo.sendData(windowIds);
    }
    
    public static void sendDataSync(Set<id> windowIds){
        EAW_Windows_Outbound ewo = new EAW_Windows_Outbound();
        ewo.sendData(windowIds);
    }
    
}