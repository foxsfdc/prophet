@isTest
private class EAW_Test_PlanGuidelineTrigger {
    private static EAW_Plan_Guideline__c planGuidelineObj;
	//create test data
	static void setUpData(String status){
		planGuidelineObj = new EAW_Plan_Guideline__c(Name='Plan Guideline 1',Status__c = status);
		insert(planGuidelineObj);
	}
	
	//check for draft to active
	@isTest static void testDraftToActiveVersion(){
		setUpData('Draft');
		Test.startTest();
		EAW_Plan_Guideline__c planGuideline = [SELECT Id,Name,Status__c from EAW_Plan_Guideline__c where Id =: planGuidelineObj.Id];
		planGuideline.Status__c = 'Active';
		DataBase.SaveResult result = DataBase.update(planGuideline);
		Test.stopTest();
		System.assert(result.isSuccess());
        System.assert(result.getId()!=null);
	}
		
	//check for active to inactive
	@isTest static void testActiveToInactiveVersion(){
		setUpData('Active');
		Test.startTest();
		EAW_Plan_Guideline__c planGuideline = [SELECT Id,Name,Status__c from EAW_Plan_Guideline__c where Id =: planGuidelineObj.Id];
		planGuideline.status__c = 'Inactive';
		DataBase.SaveResult result = DataBase.update(planGuideline);
		Test.stopTest();
		System.assert(result.isSuccess());
        System.assert(result.getId()!=null);
	}
		
	//check for draft to inactive
	@isTest static void testDraftToInactiveVersion(){
		setUpData('Draft');
		Test.startTest();
		EAW_Plan_Guideline__c planGuideline = [SELECT Id,Name,Status__c from EAW_Plan_Guideline__c where Id =: planGuidelineObj.Id];
		planGuideline.status__c = 'Inactive';
		DataBase.SaveResult result = DataBase.update(planGuideline);
		Test.stopTest();
		System.assert(result.isSuccess());
        System.assert(result.getId()!=null);
	}
		
	//check for inactive to active
	@isTest static void testInactiveToActiveVersion(){
		setUpData('Inactive');
		Test.startTest();
		EAW_Plan_Guideline__c planGuideline = [SELECT Id,Name,Status__c from EAW_Plan_Guideline__c where Id =: planGuidelineObj.Id];
		planGuideline.status__c = 'Active';
		DataBase.SaveResult result = DataBase.update(planGuideline);
		
		System.assert(result.isSuccess());
		System.assert(planGuideline.Next_Version__c == null);
        System.assert(result.getId()!=null);
	}
}