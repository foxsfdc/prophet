public class EAW_Create_RelPerformAlt_Wrapper{

    public class Wrapper{
    
        //public FoxEDFEvent FoxEDFEvent;
    //}
   // public class FoxEDFEvent {
    
        public String EventName;
        public String EventType;
        public Data Data;
       // public String EventPriority;
    }

    public class Payload {
        public String MessageID;
        public String TimeStamp;
        public String SubTypeCode;
        public String ChangedEntity;
        public String BaseObject;
        public String Operation;
        //public String SourceSystem;
        //public String MergedRowId;
        public Change Change;
    }
    public class Data {
        public Payload Payload;
    }

   

    public class Change {
        public String rowidObject;
        public String foxId;
       // public String finDivCd;
        public String hubStateInd;
        public String titleSubTypCd;
        public String level1;
        public String changedEntity;
        public String rowidTitle;
        public String rowidTitleVer;
        public String foxVersionId;
        //public String cntryCd;
        public String level2;
        public String level3;
       // public String lngCd;
    }

    
}