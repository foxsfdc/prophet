public class EAW_WGStrandSoftDeleteController{
   
    public EAW_Window_Guideline_Strand__c wgs {get;set;}
    public List <EAW_Window_Guideline_Strand__c > wgsList {get;set;}
    public String wgsListString {get;set;}
    public String wgsId {get;set;}
   
    public EAW_WGStrandSoftDeleteController(ApexPages.StandardController controller) {
    
        wgsList = new List <EAW_Window_Guideline_Strand__c > ();     
        wgs = (EAW_Window_Guideline_Strand__c )controller.getRecord();
       
        if(wgs != NULL){
        
            wgsId =wgs.Id;
            wgsList = [SELECT EAW_Window_Guideline__c,End_Date_Days_Offset__c,End_Date_From__c,End_Date_Months_Offset__c,
                              End_Date_Source__c,Galileo_DML_Type__c,Id,Imported__c,Language__c,License_Type__c,Media__c,
                              Name,Repo_DML_Type__c,Send_To_Third_Party__c,Sent_To_Galileo__c,Sent_To_Repo__c,SFDC_Base_Instance__c,
                              Soft_Deleted__c,Start_Date_Days_Offset__c,Start_Date_From__c,Start_Date_Months_Offset__c,Start_Date_Source__c,
                              Territory__c,Window_Guideline_Status__c
                       FROM EAW_Window_Guideline_Strand__c  
                       WHERE Id =: wgs.Id]; 
                     
            System.debug('wgsList ::::::::'+wgsList );
            //wgsList.add(wgs);
            wgsListString  = JSON.serialize(wgsList);
        }
       
    }
}