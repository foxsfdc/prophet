public class EAW_ConvertTitlePerformanceWrapper  {

    public class GetTitlePerformanceWrapper {
        public List<Data> data ; 
        public Links_Z links ; 
    }
    public class Relationships {
        public Title title ; 
    }
    
    public class Links_Z {
        public String self ; 
    }
    public class Attributes {
        public String type ; // in json: type
        public String foxId ; 
        public String foxVersionId ; 
        public String rowIdTitleVersion ; 
        public String performanceId ; 
        public String rowIdObject ; 
        public String currencyCode ; 
        public String currencyDescription ; 
        public String mediaCode ; 
        public String mediaDescription ; 
        public Double localCurrencyValue ; 
        public String performanceTypeCode ; 
        public Integer performanceValue ; 
        public String publicationDate ; 
        public String countryCode ; 
        public String countryDescription ; 
        public String titleVersionTypeCode ; 
        public String titleVersionTypeDescription ; 
        public String titleVersionDescription ; 

        
    }
    
    public class Links {
        public String related ; 
    }
    
    public class Title {
        public Links links ; 
    }
    
    public class Data {
        public Attributes attributes ; 
        public String id ; 
        public String type ; // in json: type
        public Relationships relationships ; 
   }

}