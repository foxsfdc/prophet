public class EAW_ReleaseDateGuidelineCloneController {
    
    @AuraEnabled
    public static EAW_Release_Date_Guideline__c getReleaseDateGuideline(String recordId){
        try{
            //LRCC-1560 - Replace Customer lookup field with Customer text field
            EAW_Release_Date_Guideline__c releaseDateGuideline = [SELECT Id,Name,Territory__c,Language__c,Product_Type__c,Active__c,Allow_Manual__c,Customers__c,All_Rules__c,EAW_Release_Date_Type__c,Allow_Manual_Check_Flag__c FROM EAW_Release_Date_Guideline__c WHERE Id = :recordId];
            return releaseDateGuideline;
        } catch(Exception e){
            throw New AuraHandledException(e.getMessage());
        }
    }
    @AuraEnabled
    public static void cloneRuleAndRuleDetails(String oldRG, String newRG) {
        
        
        Map<String, Schema.SObjectField> ruleFieldMap = Schema.SObjectType.EAW_Rule__c.Fields.getMap(); 
        
        String ruleQueryString = 'SELECT';
        for(Schema.SObjectField fieldName : ruleFieldMap.values()) {
            String fieldAPI = String.valueOf(fieldName);
            ruleQueryString = ruleQueryString + ' ' + fieldAPI + ' ,';
        }
        ruleQueryString = ruleQueryString.removeEnd(',');
        ruleQueryString += ' FROM EAW_Rule__c WHERE Release_Date_Guideline__c = :oldRG';   //change            
        System.debug('ruleQueryString:::::' + ruleQueryString );
        
        List<EAW_Rule__c> rules = Database.query(ruleQueryString);
        System.debug('Rules:::::' + rules );
        
        if(rules != null && rules.size() > 0) {
            
            List<EAW_Rule__c> clonedRules = new List<EAW_Rule__c>();
            Map<Id, List<EAW_Rule__c>> ruleIdClonedRules = new Map<Id, List<EAW_Rule__c>>();
            
            for(EAW_Rule__c r : rules) {
                
                EAW_Rule__c rule = r.clone(false, true, false, false);
                rule.Release_Date_Guideline__c = newRG;
                
                if(!ruleIdClonedRules.containsKey(r.Id)) {
                    ruleIdClonedRules.put(r.Id, new List<EAW_Rule__c>());
                }
                ruleIdClonedRules.get(r.Id).add(rule);
                clonedRules.add(rule);
            } 
            System.debug('clonedRules:::::: ' + clonedRules); 
            
            if(clonedRules != null && clonedRules.size() > 0) {
                
                Database.insert(clonedRules, false);
                System.debug('ruleIdClonedRules::::: ' + ruleIdClonedRules);
                
                Map<String, Schema.SObjectField> ruleDetailFieldMap = Schema.SObjectType.EAW_Rule_Detail__c.Fields.getMap();
                Set<Id> ruleIds = ruleIdClonedRules.keySet();
                
                String ruleDetailQueryString = 'SELECT ';
                for(Schema.SObjectField fieldName : ruleDetailFieldMap.values()){
                    String fieldAPI = String.valueOf(fieldName);
                    ruleDetailQueryString = ruleDetailQueryString + ' ' + fieldAPI + ' ,';
                }
                ruleDetailQueryString = ruleDetailQueryString.removeEnd(',');
                ruleDetailQueryString += 'FROM EAW_Rule_Detail__c WHERE Rule_No__c IN :ruleIds ';            
                System.debug('ruleDetailQueryString::::' + ruleDetailQueryString );
                
                List<EAW_Rule_Detail__c> ruleDetails = Database.query(ruleDetailQueryString);
                System.debug('ruleDetails ::::' + ruleDetails );
                
                if(ruleDetails != null && ruleDetails.size() > 0){
                    
                    List<EAW_Rule_Detail__c> clonedRuleDetails = new List<EAW_Rule_Detail__c>();
                    Map<Id, List<EAW_Rule_Detail__c>> ruleDetailIdClonedRuleDetails = new Map<Id, List<EAW_Rule_Detail__c>>();
                    Map<Id, Id> ruleDetailIdParentRDId = new Map<Id, Id>();
                    
                    for(EAW_Rule_Detail__c rd : ruleDetails){
                        
                        if(rd.Rule_No__c != null && ruleIdClonedRules.get(rd.Rule_No__c) != null){
                            
                            for(EAW_Rule__c r : ruleIdClonedRules.get(rd.Rule_No__c)){
                                
                                EAW_Rule_Detail__c ruleDetail = rd.clone(false, true, false, false);                                
                                ruleDetail.Rule_No__c = r.Id;
                                
                                if(!ruleDetailIdClonedRuleDetails.containsKey(rd.Id)){
                                    ruleDetailIdClonedRuleDetails.put(rd.Id, new List<EAW_Rule_Detail__c >());
                                }
                                ruleDetailIdClonedRuleDetails.get(rd.Id).add(ruleDetail);
                                clonedRuleDetails.add(ruleDetail);
                            }
                        }
                        
                        if(rd.Parent_Rule_Detail__c != null){
                            ruleDetailIdParentRDId.put(rd.Id, rd.Parent_Rule_Detail__c);
                        }
                    }
                    System.debug('clonedRuleDetails::::: ' + clonedRuleDetails); 
                    System.debug('ruleDetailIdParentRDId::::: ' + ruleDetailIdParentRDId); 
                    
                    if(clonedRuleDetails != null && clonedRuleDetails.size()>0){
                        
                        Database.insert(clonedRuleDetails, false);  
                        System.debug('ruleDetailIdClonedRuleDetails::::: ' + ruleDetailIdClonedRuleDetails); 
                        
                        List<EAW_Rule_Detail__c> updatedRuleDetails = new List<EAW_Rule_Detail__c>();
                        
                        for(Id rdId : ruleDetailIdClonedRuleDetails.keySet()){
                            
                            if(ruleDetailIdParentRDId.containsKey(rdId)){
                                
                                for(Integer i=0; i<ruleDetailIdClonedRuleDetails.get(rdId).size(); i++){
                                    
                                    if(ruleDetailIdClonedRuleDetails.get(rdId)[i].Parent_Rule_Detail__c != null){
                                        
                                        ruleDetailIdClonedRuleDetails.get(rdId)[i].Parent_Rule_Detail__c = 
                                            ruleDetailIdClonedRuleDetails.get(ruleDetailIdParentRDId.get(rdId))[i].Id;
                                    }
                                }
                                updatedRuleDetails.addAll(ruleDetailIdClonedRuleDetails.get(rdId));
                            }
                        }             
                        Database.update(updatedRuleDetails, false);
                        System.debug('updatedRuleDetails::::::' + updatedRuleDetails);
                    }            
                }
            }
            checkForProductTypeChange(oldRG,newRG); // LRCC - 1624
            
            if( newRG != NULL && newRG != '' ) {
                Set<Id> rdgIdSet = new Set<Id>{newRG};
                /*EAW_CreateRDForNewRDGBatch releaseDateBatch = new EAW_CreateRDForNewRDGBatch(rdgIdSet, NULL);
                releaseDateBatch.titleIdSet = NULL;
                releaseDateBatch.releaseDateGuidelineIdSet = rdgIdSet;
                releaseDateBatch.windowGuidelineIdSet = NULL;
                ID batchprocessid = Database.executeBatch(releaseDateBatch, 200);*/
                EAW_RDGDateCalculationBatchClass releaseDateBatch = new EAW_RDGDateCalculationBatchClass(rdgIdSet);
                releaseDateBatch.titleIdSet = NULL;
                releaseDateBatch.windowGuidelineIdSet = NULL;
                ID batchprocessid = Database.executeBatch(releaseDateBatch, 200);
            }
        }
    }
    public static void checkForProductTypeChange(String oldRDG, String newRDG){
        EAW_Release_Date_Guideline__c oldGuideline = [SELECT Id,Product_Type__c,All_Rules__c FROM EAW_Release_Date_Guideline__c WHERE Id = :oldRDG];
        EAW_Release_Date_Guideline__c newGuideline = [SELECT Id,Product_Type__c,All_Rules__c FROM EAW_Release_Date_Guideline__c WHERE Id = :newRDG];
        
        if(oldGuideline != null && newGuideline != null && oldGuideline.Product_Type__c != null && newGuideline.Product_Type__c != null && oldGuideline.Product_Type__c != newGuideline.Product_Type__c){
            Set<Id> modifiedRdgIds = new Set<ID>();
            Map<Id,String> oldProductTypeMap = new Map<Id,String>();
            Map<Id,String> newProductTypeMap = new Map<Id,String>();
            Map<Id,List<String>> oldConditionalNameMap = new Map<Id,List<String>>();
            
            modifiedRdgIds.add(newGuideline.Id);
            oldProductTypeMap.put(newGuideline.Id,oldGuideline.Product_Type__c);
            newProductTypeMap.put(newGuideline.Id,newGuideline.Product_Type__c);
            
            if(modifiedRdgIds.size() > 0){
                for(EAW_Rule__c rule :  [SELECT Id,Condition_Operand_Details__c,Conditional_Operand_Id__c,
                                         Release_Date_Guideline__c,Release_Date_Guideline__r.All_Rules__c, 
                                         (SELECT Id,Condition_Operand_Details__c,Conditional_Operand_Id__c FROM Rule_Orders__r) FROM EAW_Rule__c WHERE Release_Date_Guideline__c IN :modifiedRdgIds]){
                    List<String> conditionalNameList = new List<String>();
                    if(oldConditionalNameMap.containsKey(rule.Release_Date_Guideline__c)){
                        conditionalNameList = oldConditionalNameMap.get(rule.Release_Date_Guideline__c);                  
                    }  
                    if(rule.Condition_Operand_Details__c != null){
                        conditionalNameList.addAll(String.valueOf(rule.Condition_Operand_Details__c).split(';'));
                    }
                    if(rule.Rule_Orders__r != null && rule.Rule_Orders__r.size() > 0){
                        for(EAW_Rule_Detail__c ruleDetail : rule.Rule_Orders__r){
                            if(ruleDetail.Condition_Operand_Details__c != null){
                                conditionalNameList.addAll(String.valueOf(ruleDetail.Condition_Operand_Details__c).split(';'));
                            }
                        }
                    }
                    oldConditionalNameMap.put(rule.Release_Date_Guideline__c,conditionalNameList);                       
                }
                if(oldConditionalNameMap != null && oldConditionalNameMap.keySet().size() > 0){
                    system.debug(':::::oldConditionalNameMap::::::'+oldConditionalNameMap);
                    Map<String,String> allRulesUpdateMap = EAW_GuidelineProductTypeChangeHandler.updateRDGRulesBasedOnProductType(oldConditionalNameMap,oldProductTypeMap,newProductTypeMap,true,null);
                    if(allRulesUpdateMap != null && allRulesUpdateMap.keySet().size() > 0){
                        if(allRulesUpdateMap.containsKey(String.valueof(newGuideline.Id))){
                            newGuideline.All_Rules__c = allRulesUpdateMap.get(String.valueof(newGuideline.Id));
                            update newGuideline;
                        }
                    }
                }
            }
        }
    }
}