//This file was referenced from https://github.com/pozil/sfdc-ui-lookup
public class EAW_LookupSearchResult {
    public String Id;
    public String sObjectType;
    public String icon;
    public String title;
    public String titleId;
    
    public EAW_LookupSearchResult() {
    }
    
    public EAW_LookupSearchResult(String Id, String sObjectType, String icon, String title, String titleId) {
        this.Id = Id;
        this.sObjectType = sObjectType;
        this.icon = icon;
        this.title = title;
        this.titleId = titleId;
    }
    
    @AuraEnabled
    public Id getId() {
        return Id;
    }
    
    @AuraEnabled
    public String getSObjectType() {
        return sObjectType;
    }
    
    @AuraEnabled
    public String getIcon() {
        return icon;
    }
    
    @AuraEnabled
    public String getTitle() {
        return title;
    }
    
    @AuraEnabled
    public String getTitleId() {
        return titleId;
    }
}