public with sharing class EAW_TVDBoxOfficeHandler {    
    
    public set<Id> processTVDBoxOfficeToQualifyTitleForInsert( List<TVD_Box_Office__c> newTVDList, Set<Id> planGuidelineIdsSet ){
        
        try {
        
            EAW_PlanQualifierCalculationController qualifierController = new EAW_PlanQualifierCalculationController();  
            
            Set<Id> qualifiedTitleIdSet = new Set<Id>();
            
            if( newTVDList != NULL ){
                
                RecordType recType = [Select Id from RecordType WHERE Name='Qualifier' AND SObjectType = 'EAW_Rule__c'];
                
                Set<String> conditionFieldValueSet = new Set<String>{ 'US Box Office', 'US Admissions', 'US Number of Screens', 'Local Box Office', 'Local Admissions', 'Local Number of Screens' };
                
                List<EAW_Rule__c> qualificationRules;
                
                if( planGuidelineIdsSet != NULL ) {
                    qualificationRules = [Select Id, Plan_Guideline__c,Plan_Guideline__r.Product_Type__c, (Select Id, Condition_Field__c, Territory__c, Condition_Operator__c, Condition_Criteria_Amount__c from Rule_Orders__r  WHERE Condition_Field__c IN : conditionFieldValueSet ) from EAW_Rule__c WHERE RecordTypeId=: recType.Id AND Plan_Guideline__c IN : planGuidelineIdsSet AND Plan_Guideline__r.Product_Type__c != NULL  AND Plan_Guideline__r.Status__c = 'Active'];
                } else {
                    qualificationRules = [Select Id, Plan_Guideline__c,Plan_Guideline__r.Product_Type__c, (Select Id, Condition_Field__c, Territory__c, Condition_Operator__c, Condition_Criteria_Amount__c from Rule_Orders__r  WHERE Condition_Field__c IN : conditionFieldValueSet ) from EAW_Rule__c WHERE RecordTypeId=: recType.Id AND Plan_Guideline__c != NULL AND Plan_Guideline__r.Product_Type__c != NULL AND Plan_Guideline__r.Status__c = 'Active'];
                }
                
                
                Map<Id, Set<Id>> titleAndPlanGuidelineIdMap = new Map<Id, Set<Id>>();
                Set<Id> qualifiedPlanGuidelineSet = new Set<Id>();
                for( TVD_Box_Office__c tvd : newTVDList ){
                    
                    for( EAW_Rule__c rule: qualificationRules ){    
                        
                        List<String> productTypeSet = rule.Plan_Guideline__r.Product_Type__c.toUpperCase().split(';');         
                        
                        if( (tvd.Product_Type_Code__c != NULL) && productTypeSet.contains(tvd.Product_Type_Code__c.toUpperCase()) &&  ( !titleAndPlanGuidelineIdMap.containsKey(tvd.TVD_Title__c) || ( titleAndPlanGuidelineIdMap.containsKey(tvd.TVD_Title__c) && ! titleAndPlanGuidelineIdMap.get(tvd.TVD_Title__c).contains(rule.Plan_Guideline__c)  ) )){
                            
                            Boolean qualified = false;            
                            
                            for(EAW_Rule_Detail__c ruleDetail: rule.Rule_Orders__r){                   
                                
                                if( tvd.Name == 'USA' ){
                                    if( ! String.isBlank(ruleDetail.Condition_Operator__c) ) {
                                        if( ruleDetail.Condition_Field__c == 'US Box Office' && tvd.TVD_US_Box_Office__c != NULL ) {
                                            qualified = qualifierController.arithmeticDecimalConditionalChecking( tvd.TVD_US_Box_Office__c, ruleDetail.Condition_Operator__c, ruleDetail.Condition_Criteria_Amount__c );
                                        } else if( ruleDetail.Condition_Field__c == 'US Admissions' && tvd.TVD_Admissions_Count__c != NULL ) {
                                            qualified = qualifierController.arithmeticDecimalConditionalChecking( tvd.TVD_Admissions_Count__c, ruleDetail.Condition_Operator__c, ruleDetail.Condition_Criteria_Amount__c );
                                        } else if( ruleDetail.Condition_Field__c == 'US Number of Screens' && tvd.TVD_Number_of_Screens__c != NULL ) {
                                            qualified = qualifierController.arithmeticDecimalConditionalChecking( tvd.TVD_Number_of_Screens__c, ruleDetail.Condition_Operator__c, ruleDetail.Condition_Criteria_Amount__c );
                                        }
                                    }
                                    
                                } else {
                                    
                                    if( tvd.Name == ruleDetail.Territory__c ){
                                        if( ! String.isBlank(ruleDetail.Condition_Operator__c) ) {
                                            if(ruleDetail.Condition_Field__c == 'Local Box Office' && tvd.TVD_Local_Currency_Value__c != NULL ){
                                                qualified = qualifierController.arithmeticDecimalConditionalChecking( tvd.TVD_Local_Currency_Value__c, ruleDetail.Condition_Operator__c, ruleDetail.Condition_Criteria_Amount__c );
                                            } else if(ruleDetail.Condition_Field__c == 'Local Admissions' && tvd.TVD_Local_Admissions_Count__c != NULL ){
                                                qualified = qualifierController.arithmeticDecimalConditionalChecking( tvd.TVD_Local_Admissions_Count__c, ruleDetail.Condition_Operator__c, ruleDetail.Condition_Criteria_Amount__c );
                                            } else if(ruleDetail.Condition_Field__c == 'Local Number of Screens' && tvd.TVD_Number_of_Screens__c != NULL ){
                                                qualified = qualifierController.arithmeticDecimalConditionalChecking( tvd.TVD_Number_of_Screens__c, ruleDetail.Condition_Operator__c, ruleDetail.Condition_Criteria_Amount__c );
                                            }
                                        }
                                    }
                                    
                                }
                                
                                if( qualified ) break;
                            }                       
                            
                            if(qualified)
                            {
                                //create a map of plan guideline and title and use the method of title plan controller to create plans and windows
                                Set<Id> planGuidelineIdSet = new Set<Id>();
                                if( titleAndPlanGuidelineIdMap.containsKey(tvd.TVD_Title__c) ) {
                                    planGuidelineIdSet = titleAndPlanGuidelineIdMap.get(tvd.TVD_Title__c);
                                }
                                planGuidelineIdSet.add(rule.Plan_Guideline__c);
                                qualifiedPlanGuidelineSet.add(rule.Plan_Guideline__c);
                                titleAndPlanGuidelineIdMap.put(tvd.TVD_Title__c, planGuidelineIdSet);
                            }
                        }
                    }
                }
                
                System.debug('::::: titleAndPlanGuidelineIdMap :::::'+titleAndPlanGuidelineIdMap);
                
                if( titleAndPlanGuidelineIdMap != NULL && titleAndPlanGuidelineIdMap.size() > 0 ){
                    
                    Map<Id, Set<Id>> qualifiedTitleAndPGMap = new Map<Id, Set<Id>>();
                    for( EAW_Title__c eawTitle : [ Select Id, Title_EDM__c From EAW_Title__c Where Title_EDM__c IN : titleAndPlanGuidelineIdMap.keySet() ]){
                        
                        qualifiedTitleAndPGMap.put(eawTitle.Id, titleAndPlanGuidelineIdMap.get(eawTitle.Title_EDM__c));
                        qualifiedTitleIdSet.add(eawTitle.Id);
                    } 
                     //LRCC - 1239
                    EAW_PlanGuidelineQualifierController.createPlanAndWindowsForQualifiedTitles(qualifiedTitleAndPGMap,qualifiedPlanGuidelineSet);
                    /*for( Id titleId : titleAndPlanGuidelineIdMap.keySet() ){
                        
                        for(Id planGuidelineId : titleAndPlanGuidelineIdMap.get(titleId) ){
                            if( edmAndEAWTitleMap.containsKey(titleId) ){
                                Map<String, List<SObject>> titlePlanAndWindowsResultMap = EAW_TitlePlanWindowController.createPlan(edmAndEAWTitleMap.get(titleId), planGuidelineId, null, null, false);
                            }
                        }                    
                    } */               
                }
            }
            return qualifiedTitleIdSet;
            
        } catch(Exception ex) {
            System.debug('<<--Exception-->>'+ex.getMessage());
            return null;
        }
    }
    
    public void processTVDBoxOfficeToQualifyTitleForUpdate( List<TVD_Box_Office__c> newTVDList, List<TVD_Box_Office__c> oldTVDList ){
        
        try {
        
            if( newTVDList != NULL && oldTVDList != NULL ){
                
                List<TVD_Box_Office__c> tvdListToProcess = new List<TVD_Box_Office__c>();
                    
                for( Integer i= 0; i < newTVDList.size(); i++ ){
                    
                    TVD_Box_Office__c newTvd = newTVDList[i];
                    TVD_Box_Office__c oldTvd = oldTVDList[i];
                    
                    if( newTvd.Name != oldTvd.Name || newTvd.TVD_US_Box_Office__c != oldTvd.TVD_US_Box_Office__c || newTvd.TVD_Admissions_Count__c != oldTvd.TVD_Admissions_Count__c || newTvd.TVD_Number_of_Screens__c != oldTvd.TVD_Number_of_Screens__c || newTvd.TVD_Local_Currency_Value__c != oldTvd.TVD_Local_Currency_Value__c || newTvd.TVD_Local_Admissions_Count__c != oldTvd.TVD_Local_Admissions_Count__c ){
                        tvdListToProcess.add(newTvd);
                    }
                    
                }
                
                if( tvdListToProcess.size() > 0 ){
                    Set<Id> qualifiedTitleIdSet = processTVDBoxOfficeToQualifyTitleForInsert(tvdListToProcess, null);
                }
            }            
        } catch(Exception ex) {
             System.debug('<<--Exception-->>'+ex.getMessage());
        }
    }
}