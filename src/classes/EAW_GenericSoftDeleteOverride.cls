public class EAW_GenericSoftDeleteOverride{

   
    
    @AuraEnabled
    public static String getError(String sObjectListString,String sObjectName){
    
        
        String errorMessage ;
        Boolean proceedToSoftDelete = false;
        List <SObject> sObjectUpdateList = new List <SObject> ();
        List <SObject> sObjectList= (List<SObject>)System.JSON.deserialize(sObjectListString, List<SObject>.class);
        
        if(sObjectName == 'EAW_Release_Date_Guideline__c'){
        
            List <EAW_Release_Date_Guideline__c> rdgList = (List<EAW_Release_Date_Guideline__c>)sObjectList;
            if(rdgList .size() == 1 && rdgList [0].Soft_Deleted__c == 'TRUE'){
            
                errorMessage = Label.EAW_AlreadySoftDeleted;
            }else{
                errorMessage =  preventDeletionofRDG(rdgList);
            }
        }else if(sObjectName == 'EAW_Window_Guideline__c'){
        
            List <EAW_Window_Guideline__c> wgList = (List<EAW_Window_Guideline__c>)sObjectList;
            if(wgList.size() == 1 && wgList[0].Soft_Deleted__c == 'TRUE'){
            
                errorMessage = Label.EAW_AlreadySoftDeleted;
            }else{
            
                errorMessage =  preventDeletionofWDG(wgList);
            }
        }else if(sObjectName == 'EAW_Window_Guideline_Strand__c'){
        
              List <EAW_Window_Guideline_Strand__c> wgsList = (List<EAW_Window_Guideline_Strand__c>)sObjectList;
            if(wgsList.size() == 1 && wgsList[0].Soft_Deleted__c == 'TRUE'){
            
                errorMessage = Label.EAW_AlreadySoftDeleted;
            }else{
            
                errorMessage =  preventDeletionofStrandsRelatedToActiveGuideline(wgsList);
            }
        
        }else if(sObjectName == 'EAW_Release_Date__c'){
             
               
            List <EAW_Release_Date__c> rdList = (List<EAW_Release_Date__c>)sObjectList;
            
            errorMessage = releaseDateSoftDelete(rdList);
            return errorMessage;
             
        }else if(sObjectName == 'EAW_Window__c'){
             
               
            List <EAW_Window__c> windowList = (List<EAW_Window__c>)sObjectList;
            List <EAW_Window__c> updateWinList = new  List <EAW_Window__c> ();
            
             if(windowList.size() == 1 && windowList[0].Soft_Deleted__c == 'TRUE'){
             
                 errorMessage = Label.EAW_AlreadySoftDeleted;
                 return errorMessage;
                 
             } else {
             
                 
                List<EAW_Window__c> windowsToBeSoftDelete = new List<EAW_Window__c>();
                Set<Id> softDeletedWindowGuidelineId = new Set<Id>();
                Set<Id> softDeletedWindowTitleId = new Set<Id>();
                
                for (EAW_Window__c window : windowList) {
                    
                    EAW_Window__c tempWindow = new EAW_Window__c(Id=window.Id);
                    tempWindow.Soft_Deleted__c = 'TRUE';
                    tempWindow.Galileo_DML_Type__c = 'Delete';
                    tempWindow.Repo_DML_Type__c = 'Delete';
                    tempWindow.Send_To_Third_Party__c = True;
                    //LRCC-1821
                    tempWindow.Galileo_RC_DML_Type__c='Delete';
                    windowsToBeSoftDelete.add(tempWindow);
                    
                    if(window.EAW_Window_Guideline__c != NULL){
                    
                        softDeletedWindowGuidelineId.add(window.EAW_Window_Guideline__c);
                    }
                    if(window.EAW_Title_Attribute__c != NULL){
                    
                        softDeletedWindowTitleId.add(window.EAW_Title_Attribute__c);
                    }
                }
                
                try{
                
                    if(windowsToBeSoftDelete != null && windowsToBeSoftDelete.size() > 0) {
                
                        checkRecursiveData.bypassDateCalculation = True;
                        update windowsToBeSoftDelete;
                        
                        Set<Id> windowGLIdSet  = new Set<Id>();
                        EAW_DownstreamRuleGuidelinesFindUtil dependentGuidelineFindUtil = new EAW_DownstreamRuleGuidelinesFindUtil();
                        windowGLIdSet = dependentGuidelineFindUtil.findDependentWindowGuidelineToProcess(softDeletedWindowGuidelineId);
                        
                        if(windowGLIdSet.size() > 0){
                        
                            EAW_WGDateCalculationBatchClass windowDateBatch = new EAW_WGDateCalculationBatchClass(windowGLIdSet);
                            windowDateBatch.releaseDateGuidelineIdSet = NULL;
                            windowDateBatch.titleIdSet = softDeletedWindowTitleId;
                            //LRCC-1863
                            windowDateBatch.allowWindowDateChange = true;
                            ID batchprocessid = Database.executeBatch(windowDateBatch, 200);
                        }
                        errorMessage = 'Success';
                     }
                     
                 } catch(Exception e){
                 
                     errorMessage = e.getMessage();
                     return errorMessage;
                 }
             }
             
        }
        
        
        if(errorMessage != NULL){
             
            return errorMessage;
            
        }else{
        
            proceedToSoftDelete = true;  
        }
        if(proceedToSoftDelete){
        
            Set<Id> sobjectIdSet = new Set<Id>(); 
            String listType = 'List<' +sObjectName+ '>';
            List<SObject> castRecords = (List<SObject>)Type.forName(listType).newInstance();
            castRecords.addAll(sObjectList);
            //upsert castRecords;
           
        
            for(SObject sobj : castRecords){
            
                //SObject sobj = (sObjectType)sobj;//.getSObjectType().newSObject(sobj.Id);
                sobj.put('Soft_Deleted__c', 'TRUE');
                sobj.put('Repo_DML_Type__c', 'Delete');
                //sobj.put('Galileo_DML_Type__c', 'Delete');
                

                sobjectIdSet.add(sobj.Id); 
                
                sObjectUpdateList.add(sobj); 
            }
            try{
            
                if(sObjectUpdateList.size()  > 0 ){
                
                    
                    update sObjectUpdateList;
                    
                    if(sobjectIdSet.size() > 0){
                    
                        if(sObjectName == 'EAW_Release_Date_Guideline__c'){
                         
                           EAW_ReleaseDateGuidelines_Outbound.sendDataAsync(sobjectIdSet,'Delete');
                            
                        }else if(sObjectName == 'EAW_Window_Guideline__c'){
                         
                            EAW_WindowGuidelines_Outbound.sendDataAsync(sobjectIdSet,'Delete');
                        
                        }else if(sObjectName == 'EAW_Window_Guideline_Strand__c'){
                          
                           EAW_WindowGuidelineStrands_Outbound.sendDataAsync(sobjectIdSet,'Delete');
                        }
                    }
                }
                 
             } catch(Exception e){
             
                 errorMessage = e.getMessage();
                 return errorMessage;
             }
        }
        
        return 'Success';
        
    }
      
    public static  String preventDeletionofRDG(List<EAW_Release_Date_Guideline__c> oldRDGs) {
    
       
        String errroMsg = NULL;
        Set<Id> releaseDateGuidelineSet = new Set<Id>(); 
        for(EAW_Release_Date_Guideline__c rgd : oldRDGs){
        
            if(rgd != null) releaseDateGuidelineSet.add(rgd.id);
        }
        
        if(releaseDateGuidelineSet.size() > 0){
            Map<Id, List<EAW_Release_Date__c>> rdgAndReleaseDateMap = new Map<Id, List<EAW_Release_Date__c>> ();
            List<EAW_Rule_Detail__c> ruleDetailList = new List<EAW_Rule_Detail__c>();
            Set<Id> referredConditionalSet = new Set<Id>();
            Id widowDateRuleRecordTypeId = Schema.SObjectType.EAW_Rule__c.getRecordTypeInfosByName().get('Window Date Calculation').getRecordTypeId();
            Id releaseDateRuleRecordTypeId = Schema.SObjectType.EAW_Rule__c.getRecordTypeInfosByName().get('Release Date Calculation').getRecordTypeId();
            for(EAW_Release_Date__c rd: [SELECT Id, Name, Release_Date_Guideline__c FROM EAW_Release_Date__c WHERE Release_Date_Guideline__c IN: oldRDGs]) {
                
             
             
                if(rdgAndReleaseDateMap.containsKey(rd.Release_Date_Guideline__c)) {
                    List<EAW_Release_Date__c> tempList = rdgAndReleaseDateMap.get(rd.Release_Date_Guideline__c);    
                    tempList.add(rd);
                    rdgAndReleaseDateMap.put(rd.Release_Date_Guideline__c, tempList);
                } 
                else {
                    rdgAndReleaseDateMap.put(rd.Release_Date_Guideline__c, new List<EAW_Release_Date__c> {rd});
                }    
            }
            if(!rdgAndReleaseDateMap.isEmpty()){
                releaseDateGuidelineSet.removeAll(rdgAndReleaseDateMap.keySet());
            }
            if(releaseDateGuidelineSet != null && releaseDateGuidelineSet.size() > 0){
                ruleDetailList = [SELECT Id,Rule_No__c,Conditional_Operand_Id__c,Rule_No__r.Conditional_Operand_Id__c, Rule_No__r.Window_Guideline__c FROM EAW_Rule_Detail__c WHERE (Conditional_Operand_Id__c IN : releaseDateGuidelineSet OR Rule_No__r.Conditional_Operand_Id__c IN : releaseDateGuidelineSet) AND (Rule_No__r.Window_Guideline__c != null OR Rule_No__r.Release_Date_Guideline__c != null) AND (Rule_No__r.RecordTypeId = :widowDateRuleRecordTypeId OR Rule_No__r.RecordTypeId = :releaseDateRuleRecordTypeId) AND (Rule_No__r.Window_Guideline__r.Status__c != 'Inactive' OR Rule_No__r.Release_Date_Guideline__r.Active__c = true)];
            }
            if(ruleDetailList.size() > 0){
                for(EAW_Rule_Detail__c ruleDetail : ruleDetailList){
                    if(ruleDetail.Conditional_Operand_Id__c != null){
                        referredConditionalSet.add(ruleDetail.Conditional_Operand_Id__c);
                    }
                    if(ruleDetail.Rule_No__r.Conditional_Operand_Id__c != null){
                        referredConditionalSet.add(ruleDetail.Rule_No__r.Conditional_Operand_Id__c);
                    }
                }
            }
            
            for(EAW_Release_Date_Guideline__c rdg: oldRDGs) {
                if(!rdgAndReleaseDateMap.isEmpty() && rdgAndReleaseDateMap.get(rdg.Id) != null) {
                    if(errroMsg == NULL){
                        errroMsg = '\n';
                    }
                   // rdg.addError('This Release Date Guideline cannot be deleted since it has associated release dates');
                   errroMsg = errroMsg +Label.EAW_RDGAssociatedRD +' : ['+rdg.Name+'] \n';
                } else if(referredConditionalSet.size() > 0 && referredConditionalSet.contains(rdg.Id)){
                  if(errroMsg == NULL){
                        errroMsg = '\n';
                    }
                   errroMsg = errroMsg +Label.EAW_RDGReferredToOtherRDG+' : ['+rdg.Name+' ] \n';
                   //rdg.addError('This Release Date Guideline cannot be deleted since it has referred to other guidelines');
                }
            }
        }
       
       return errroMsg;
    }
    public static String preventDeletionofWDG(List < EAW_Window_Guideline__c > oldWindowList) {
    
        String errroMsg = NULL;
        Set < Id > windowGuidelineIdSet = new Set < Id > ();

        for (EAW_Window_Guideline__c windowGuideline: oldWindowList) {
            if (windowGuideline != null) {
                windowGuidelineIdSet.add(windowGuideline.Id);
            }
        }
        if (windowGuidelineIdSet.size() > 0) {

            Map < Id, List < EAW_Window__c >> wdgAndwindowMap = new Map < Id, List < EAW_Window__c >> ();
            List < EAW_Rule_Detail__c > ruleDetailList = new List < EAW_Rule_Detail__c > ();
            Set < Id > referredConditionalSet = new Set < Id > ();
            for (EAW_Window__c windowIns: [SELECT Id, Name, EAW_Window_Guideline__c FROM EAW_Window__c WHERE EAW_Window_Guideline__c IN: windowGuidelineIdSet]) {
                if (wdgAndwindowMap.containsKey(windowIns.EAW_Window_Guideline__c) == True) {

                    wdgAndwindowMap.get(windowIns.EAW_Window_Guideline__c).add(windowIns);
                }
                wdgAndwindowMap.put(windowIns.EAW_Window_Guideline__c, new List < EAW_Window__c > {
                    windowIns
                });

            }
            if (!wdgAndwindowMap.isEmpty()) {
                windowGuidelineIdSet.removeAll(wdgAndwindowMap.keySet());
            }
            if (windowGuidelineIdSet.size() > 0) {
                Id widowDateRuleRecordTypeId = Schema.SObjectType.EAW_Rule__c.getRecordTypeInfosByName().get('Window Date Calculation').getRecordTypeId();
                for (EAW_Rule_Detail__c ruleDetail: [SELECT Id, Rule_No__c, Conditional_Operand_Id__c, Rule_No__r.Conditional_Operand_Id__c, Rule_No__r.Window_Guideline__c FROM EAW_Rule_Detail__c WHERE(Conditional_Operand_Id__c IN: windowGuidelineIdSet OR Rule_No__r.Conditional_Operand_Id__c IN: windowGuidelineIdSet) AND Rule_No__r.Window_Guideline__c != null AND Rule_No__r.RecordTypeId =: widowDateRuleRecordTypeId AND Rule_No__r.Window_Guideline__r.Status__c != 'Inactive']) {
                    if (ruleDetail.Conditional_Operand_Id__c != null) {
                        referredConditionalSet.add(ruleDetail.Conditional_Operand_Id__c);
                    }
                    if (ruleDetail.Rule_No__r.Conditional_Operand_Id__c != null) {
                        referredConditionalSet.add(ruleDetail.Rule_No__r.Conditional_Operand_Id__c);
                    }
                }

            }
            for (EAW_Window_Guideline__c wdIns: oldWindowList) {
                
                if (wdIns.Status__c == 'Active' && wdgAndwindowMap.isEmpty() == True) {
                       
                    //wdIns.addError('You cannot delete an Active window Guideline');
                    errroMsg  = Label.EAW_CantDeleteActiveWG+' : ['+wdIns.Name+'] \n';
                } 
                else if ((wdgAndwindowMap.isEmpty() == False && wdgAndwindowMap.containsKey(wdIns.Id) && wdgAndwindowMap.get(wdIns.Id) != null)) {
                    //wdIns.addError('This Window Guideline cannot be deleted since it has associated windows');
                      errroMsg  = Label.EAW_WGAssociatedWindows+' : ['+wdIns.Name+'] \n';
                } 
                else if (referredConditionalSet != null && referredConditionalSet.size() > 0 && referredConditionalSet.contains(wdIns.Id)) {
                   // wdIns.addError('This Window Guideline cannot be deleted since it has referred on another window guideline');
                     errroMsg  = Label.EAW_WGReferredOnAnotherWG+' : ['+wdIns.Name+'] \n';
                } 
                else if (wdIns.Status__c == 'Inactive' && wdIns.Next_Version__c != null) {
                   // wdIns.addError('You cannot delete an Inactive Window Guideline with newer versions');
                     errroMsg  = Label.EAW_InactiveWGWithNewerVersions+' : ['+wdIns.Name+'] \n';
                }
            }
        }
       
       return errroMsg;
    }
    
    public static String preventDeletionofStrandsRelatedToActiveGuideline(List<EAW_Window_Guideline_Strand__c> OldwindowGLStrands) {
        
        Set<Id> windowGuideLineActiveIdSet = new Set<Id>();
        Set<Id> windowGuideLineStrandIdSet = new Set<Id>();
        Map<Id, String> activeWindowGuidlineandStrandMap = new Map<Id, String>();
        String errroMsg = NULL;
        
        for(EAW_Window_Guideline_Strand__c wgsIns : OldwindowGLStrands) {
        
            if(wgsIns.EAW_Window_Guideline__c != null) {
                
                windowGuideLineActiveIdSet.add(wgsIns.EAW_Window_Guideline__c );
            }
            if(wgsIns != null) {
                
                windowGuideLineStrandIdSet.add(wgsIns.Id);
            }             
        }
        if(windowGuideLineActiveIdSet.isEmpty() == False) {
            for(EAW_Window_Guideline__c wgIns : [SELECT Id, Name, Status__c from EAW_Window_Guideline__c WHERE Id IN :windowGuideLineActiveIdSet]) {
                
                activeWindowGuidlineandStrandMap.put(wgIns.Id, wgIns.Status__c);
            }
        }
        
        if(activeWindowGuidlineandStrandMap.isEmpty() == False) {
            
            for(EAW_Window_Guideline_Strand__c wgins : OldwindowGLStrands) {
                
                if(activeWindowGuidlineandStrandMap.get(wgins.EAW_Window_Guideline__c ) == 'Active') {
                    
                    //wgins.addError('Window guideline strand cannot be deleted since it has associated active window guideline to it');
                    errroMsg = Label.EAW_WGStrandAssociatedActiveWG+' : ['+wgins.Name+'] \n';
                }
            }
        }
       
        return errroMsg ;
    }
    public static String releaseDateSoftDelete( List <EAW_Release_Date__c>  rdList){
    
        String errorMessage = NULL;
        
        List <EAW_Release_Date__c> updateRdList = new  List <EAW_Release_Date__c> ();
        
        Set<Id> deleteDateRDGs = new Set<Id>();
        Set<Id> titleIdsSet = new Set<Id>();
        
        if(rdList.size() == 1 && rdList[0].Soft_Deleted__c == 'TRUE'){
         errorMessage = Label.EAW_AlreadySoftDeleted;
         return errorMessage;
        } else {
        
            for(EAW_Release_Date__c rd : rdList){
            
                rd.Active__c = False;
                rd.Projected_Date__c = null;
                rd.Soft_Deleted__c = 'TRUE';
                if( rd.Release_Date_Guideline__c != NULL ) {
                    deleteDateRDGs.add(rd.Release_Date_Guideline__c);
                }
                if(rd.Title__c != null){
                    titleIdsSet.add(rd.Title__c);
                }
                updateRdList.add(rd);
            }
            
            try{
            
             if(updateRdList.size() > 0){
                
                checkRecursiveData.bypassDateCalculation = True;
                update updateRdList;
                
                if( deleteDateRDGs != NULL && deleteDateRDGs.size() > 0 ){
                
                    Set<Id> rdgIdSet = new  Set<Id>();
                
                    //Multi-Operand selection related changes
                    EAW_DownstreamRuleGuidelinesFindUtil dependentGuidelineFindUtil = new EAW_DownstreamRuleGuidelinesFindUtil();
                    
                    rdgIdSet = dependentGuidelineFindUtil.findDependentReleaseDateGuidelineToProcess(deleteDateRDGs);
                    
                    if( rdgIdSet != NULL && rdgIdSet.size() > 0 ) {
                        //Batch-Conversion logic
                        EAW_RDGDateCalculationBatchClass releaseDateBatch = new EAW_RDGDateCalculationBatchClass(rdgIdSet);
                        releaseDateBatch.titleIdSet = titleIdsSet;
                        releaseDateBatch.windowGuidelineIdSet = NULL;
                        checkRecursiveData.bypassDateCalculation = TRUE;
                        ID batchprocessid = Database.executeBatch(releaseDateBatch, 200);
                        
                    } else {
                        Set<Id> wgIds = dependentGuidelineFindUtil.findDependentWindowGuidelineToProcess(deleteDateRDGs);
                        if( wgIds != NULL && wgIds.size() > 0 ){
                            EAW_WGDateCalculationBatchClass windowDateBatch = new EAW_WGDateCalculationBatchClass(wgIds);
                            windowDateBatch.titleIdSet = titleIdsSet;
                            checkRecursiveData.bypassDateCalculation = TRUE;
                            ID batchprocessid = Database.executeBatch(windowDateBatch, 200);
                        }
                    }
                }
                
                errorMessage = 'Success';
             }
             
            } catch(Exception e){
            
                errorMessage = e.getMessage();
                return errorMessage;
            }
        }
        return errorMessage;
    }
    
}