public with sharing class EAW_ReleaseDateGuidelines_Outbound {
	List<EAW_Error_Log__c> repoErrorLogs  = new List<EAW_Error_Log__c>();
	set<id> repoSuccessIds= new set<id>();
	Set<id> repoIgnoredIds = new Set<id>();
	
	String repoUrl = '';
	
	public EAW_ReleaseDateGuidelines_Outbound(){
        checkOuboundRecursiveData.bypassTriggerFire=true;
		Rest_Endpoint_Settings__c repoInstance = Rest_Endpoint_Settings__c.getInstance('Outbound-RDG-to-Repo');
		if(repoInstance!=null){
			repoUrl=repoInstance.End_Point__c;
		}
	}
	
    public class ReleaseDateGuidelineJson{
       public String releaseDateGuidelineId;
       public String releaseDateGuidelineName;
       public boolean allowManual;
       public String customers;
       public String productType;
       public String productTypeId;
       public String territory;
       public String territoryId;
       public String language;
       public String languageId;
       public String releaseDateType;
       public String releaseDateTypeId;
       public String releaseDateRuleText;
       public boolean active;
       public String deleted;
       public String lastModifiedDate;
       public String lastModifiedBy;
       public String operation;
		
		public ReleaseDateGuidelineJson(String releaseDateGuidelineId,String releaseDateGuidelineName,boolean allowManual,String customers,String productType,
										String productTypeId,String territory,String territoryId,String language,String languageId,String releaseDateType,
										String releaseDateTypeId,String releaseDateRuleText,boolean active,String deleted,String lastModifiedDate,String lastModifiedBy,
										String operation){
		
		
			this.releaseDateGuidelineId=releaseDateGuidelineId;
			this.releaseDateGuidelineName=releaseDateGuidelineName;
			this.allowManual=allowManual;
			this.customers=customers;
			this.productType=productType;
			this.productTypeId=productTypeId;
			this.territory=territory;
			this.territoryId=territoryId;
			this.language=language;
			this.languageId=languageId;
			this.releaseDateType=releaseDateType;
			this.releaseDateTypeId=releaseDateTypeId;
			this.releaseDateRuleText=releaseDateRuleText;
			this.active=active;
			this.deleted=deleted;
			this.lastModifiedDate=lastModifiedDate;
			this.lastModifiedBy=lastModifiedBy;
			this.operation=operation;
		}
	}

    private void getJson(EAW_Outbound_Utility.OutboundSystem os, List<EAW_Release_Date_Guideline__c> recordList){
    	Map<String,EAW_Territories__c> territoryMap = EAW_Territories__c.getAll();
    	Map<String,EAW_Languages__c> languageMap = EAW_Languages__c.getAll();
		Set<String> pds = new Set<String>();
		Set<String> rdts = new Set<String>();
		Map<String,EDM_REF_PRODUCT_TYPE__c> pdMap = new Map<String,EDM_REF_PRODUCT_TYPE__c>();
		Map<String,EAW_Release_Date_Type__c> rdtMap = new Map<String,EAW_Release_Date_Type__c>();
		for(EAW_Release_Date_Guideline__c rdg:recordList){
			pds.add(rdg.Product_Type__c);
			rdts.add(rdg.EAW_Release_Date_Type__c);
		}
		List<EDM_REF_PRODUCT_TYPE__c> epts = [select Name,PROD_TYP_CD__c,PROD_TYP_DESC__c from EDM_REF_PRODUCT_TYPE__c where Name in :pds];
		List<EAW_Release_Date_Type__c> erdts = [select Name from EAW_Release_Date_Type__c where Name in :rdts];
		for(EDM_REF_PRODUCT_TYPE__c ept:epts){
			pdMap.put(ept.name,ept);
		}
		for(EAW_Release_Date_Type__c rdt:erdts){
			rdtMap.put(rdt.name,rdt);
		}
	 	for(EAW_Release_Date_Guideline__c rdg:recordList){
	 		List<id> idList = new List<id>();
            idList.add(rdg.id);
            EAW_Outbound_Utility eouRepo = new EAW_Outbound_Utility(EAW_Outbound_Utility.OutboundDataType.ReleaseDateGuideline,os,idList);
          	String operation = '';
            if(os==EAW_Outbound_Utility.OutboundSystem.Repo){
                ReleaseDateGuidelineJson rdgJson = new ReleaseDateGuidelineJson(''+rdg.id,rdg.Name,rdg.Allow_Manual__c,rdg.Customers__c,rdg.Product_Type__c,/*''+pdMap.get(rdg.Product_Type__c).id*/null,rdg.Territory__c,EAW_Outbound_Utility.getTerritoryIds(rdg.Territory__c),
														rdg.Language__c,EAW_Outbound_Utility.getLanguageIds(languageMap,rdg.Language__c),rdg.EAW_Release_Date_Type__c,/*''+rdtMap.get(rdg.EAW_Release_Date_Type__c).id*/null,rdg.All_Rules__c,
														rdg.Active__c,rdg.Soft_Deleted__c,EAW_Outbound_Utility.getFormattedDateTime(rdg.LastModifiedDate),rdg.LastModifiedBy.name,rdg.Repo_DML_Type__c);
                eouRepo.sendDataThroughWebAPI( system.json.serialize(rdgJson),repoUrl,repoErrorLogs,repoSuccessIds);
            }
		}											
       
    }
    
	public static List<EAW_Release_Date_Guideline__c> getRDGs(Set<Id> releaseDateGuidlineIds){
		List<EAW_Release_Date_Guideline__c> RDGlist = [select id,name,Allow_Manual__c,Customers__c,Product_Type__c,Territory__c,Language__c,
													EAW_Release_Date_Type__c,All_Rules__c,Active__c,IsDeleted ,LastModifiedDate,LastModifiedBy.name,
													Repo_DML_Type__c, Sent_To_Repo__c,Send_To_Third_Party__c, Soft_Deleted__c  
													from EAW_Release_Date_Guideline__c where id in :releaseDateGuidlineIds];
													
													
		return RDGlist;											
	
	}
    public void aggregateAllRds(List<EAW_Release_Date_Guideline__c> records,String operation){
    	List<EAW_Release_Date_Guideline__c> repoRecords = new List<EAW_Release_Date_Guideline__c>();
    	
    	List<EAW_Release_Date_Guideline__c> updateList = new List<EAW_Release_Date_Guideline__c>();
    	List<EAW_Release_Date_Guideline__c> deleteList = new List<EAW_Release_Date_Guideline__c>();
    	List<EAW_Error_Log__c> insertErrorList = new List<EAW_Error_Log__c>();
    	
    	if(records!=null && !records.isempty()){
    		for(EAW_Release_Date_Guideline__c record:records){
    			
    			if('update'.equalsignorecase(operation) || 'Insert'.equalsignorecase(operation)){
    				if(record.Sent_To_Repo__c){
    					record.Repo_DML_Type__c='Update';
    				}else{
    					record.Repo_DML_Type__c='Insert';
    				}
    			}else if('delete'.equalsignorecase(operation)){
    				record.Repo_DML_Type__c='Delete';
    			}
    			
                boolean isSoftDeleted = record.Soft_Deleted__c=='TRUE';//To do after handling soft delete please use the active flag
				if(record.Sent_To_Repo__c || !isSoftDeleted){
    				repoRecords.add(record);
				}else if(!record.Sent_To_Repo__c && isSoftDeleted){
					repoIgnoredIds.add(record.id);
				}				
			}
			if(!repoRecords.isEmpty()){
				getJson(EAW_Outbound_Utility.OutboundSystem.Repo,repoRecords);
			}
			
			for(EAW_Release_Date_Guideline__c record:records){
					boolean canBeDeleted = false;
					boolean isSoftDeleted = record.Soft_Deleted__c=='TRUE';//To do after handling soft delete please use the active flag	
					if(repoSuccessIds.contains(record.id) //Both are success
						|| repoIgnoredIds.contains(record.id)//If record is inactive and didn't send to both api then we can delete without sending them
						){
						record.Send_To_Third_Party__c=false;
						canBeDeleted = true; // Can be deleted
					}
					if(repoSuccessIds.contains(record.id)){//Repo call is success then repo DML type is blank and sent to repo should be true
						record.Sent_To_Repo__c=true;
						record.Repo_DML_Type__c='';
					}
					
					if(isSoftDeleted && canBeDeleted){
						deleteList.add(record);
					}else{
						updateList.add(record);
					}
			}
			insertErrorList.addAll(repoErrorLogs);			
    	}
    	if(updateList!=null && !updateList.isEmpty()){
    		update updateList;
    	}
    	if(deleteList!=null && !deleteList.isEmpty()){
    		delete deleteList;
    	}
    	if(insertErrorList!=null && !insertErrorList.isEmpty()){
    		insert insertErrorList;
    	}
    	//To do Skip downstream calculation of Release dates and Windows
    	//if response 200 then Send_To_Third_Party - False, Dml_Type as blank
		//Sent_To_Repo-true
		//Sent_To_Galileo-true
    }

    private static void sendData(Set<id> releaseDateGuidlineIds,String operation){
    	EAW_ReleaseDateGuidelines_Outbound ero = new EAW_ReleaseDateGuidelines_Outbound();
    	ero.aggregateAllRds(getRDGs(releaseDateGuidlineIds),operation);
    }
    
    @future(callout=true)
    public static void sendDataAsync(Set<id> RDGIds,String operation){
    	sendData(RDGIds, operation);
    }
    
    public static void sendDataSync(Set<id> RDGIds,String operation){
    	sendData(RDGIds, operation);
    }
	
	
    
}