public with sharing class EAW_BoxOfficeHandler {    
    
    //LRCC-1394 - Notification.
    //Get custom settings - ON/OFF switches for notification.
    EAW_Notification_Status__c ns = EAW_Notification_Status__c.getOrgDefaults();
    //Get chatter group Id.
    List<CollaborationGroup> lstChatterGroup = [SELECT Id, Name FROM CollaborationGroup WHERE Name = 'Title Attribute Review'];
        
    public set<Id> processTVDBoxOfficeToQualifyTitleForInsert( List<EAW_Box_Office__c> newTVDList, Set<Id> planGuidelineIdsSet ){
                
        Set<Id> boxOfficeTitleIds = new Set<Id> ();
        List<FeedItem> feedItems = new List<FeedItem>();
        Set<Id> disQualifiedPG = new Set<Id> ();
        String bodyContent;
        
        try {
        
            EAW_PlanQualifierCalculationController qualifierController = new EAW_PlanQualifierCalculationController();  
            
            Set<Id> qualifiedTitleIdSet = new Set<Id>();
            
            if( newTVDList != NULL ){
                
                RecordType recType = [Select Id from RecordType WHERE Name='Qualifier' AND SObjectType = 'EAW_Rule__c'];
                
                Set<String> usFieldValueSet = new Set<String>{ 'US Box Office', 'US Admissions', 'US Number of Screens'};
                Set<String> localFieldValueSet = new Set<String>{ 'Local Box Office', 'Local Admissions', 'Local Number of Screens' };
                Set<String> conditionFieldValueSet = new Set<String>();
                conditionFieldValueSet.addAll(usFieldValueSet);
                conditionFieldValueSet.addAll(localFieldValueSet);
                
                List<EAW_Rule__c> qualificationRules;
                
                if( planGuidelineIdsSet != NULL ) {
                    qualificationRules = [Select Id, Plan_Guideline__c,Plan_Guideline__r.Product_Type__c, (Select Id, Condition_Field__c, Territory__c, Condition_Operator__c, Condition_Criteria_Amount__c from Rule_Orders__r  WHERE Condition_Field__c IN : conditionFieldValueSet ) from EAW_Rule__c WHERE RecordTypeId=: recType.Id AND Plan_Guideline__c IN : planGuidelineIdsSet AND Plan_Guideline__r.Product_Type__c != NULL  AND Plan_Guideline__r.Status__c = 'Active'];
                } else {
                    qualificationRules = [Select Id, Plan_Guideline__c,Plan_Guideline__r.Product_Type__c, (Select Id, Condition_Field__c, Territory__c, Condition_Operator__c, Condition_Criteria_Amount__c from Rule_Orders__r  WHERE Condition_Field__c IN : conditionFieldValueSet ) from EAW_Rule__c WHERE RecordTypeId=: recType.Id AND Plan_Guideline__c != NULL AND Plan_Guideline__r.Product_Type__c != NULL AND Plan_Guideline__r.Status__c = 'Active'];
                }
                Set<Id> titleIds = new Set<ID>();
                for( EAW_Box_Office__c tvd : newTVDList ){
                    titleIds.add(tvd.Title_Attribute__c);
                }
                Map<Id,Eaw_title__c>titleAttributeIdMap = new Map<ID,Eaw_title__c>([SELECT Id, PROD_TYP_CD_INTL__c,PROD_TYP_CD_INTL__r.name FROM Eaw_title__c where Id in :titleIds]);
                
                Map<Id, Set<Id>> titleAndPlanGuidelineIdMap = new Map<Id, Set<Id>>();
                Set<Id> qualifiedPlanGuidelineSet = new Set<Id>();
                
                for( EAW_Box_Office__c tvd : newTVDList ){
                    
                    //System.debug('::::::::: TVD :::::::::::'+tvd);
                    
                    for( EAW_Rule__c rule: qualificationRules ){    
                        
                        //System.debug('::::::::: rule :::::::::::'+system.json.serialize(rule));
                        
                        List<String> productTypeSet = rule.Plan_Guideline__r.Product_Type__c.toUpperCase().split(';'); 
                       // System.debug('::::::::: productTypeSet :::::::::::'+productTypeSet); 
                       // System.debug('::::::::: titleAttributeIdMap.get(tvd.Title_Attribute__c) :::::::::::'+titleAttributeIdMap.get(tvd.Title_Attribute__c).PROD_TYP_CD_INTL__c);       
                        
                        if(titleAttributeIdMap.get(tvd.Title_Attribute__c).PROD_TYP_CD_INTL__c != NULL && productTypeSet.contains(titleAttributeIdMap.get(tvd.Title_Attribute__c).PROD_TYP_CD_INTL__r.name.toUpperCase()) 
                        &&  ( !titleAndPlanGuidelineIdMap.containsKey(tvd.Title_Attribute__c) || ( titleAndPlanGuidelineIdMap.containsKey(tvd.Title_Attribute__c) 
                        && ! titleAndPlanGuidelineIdMap.get(tvd.Title_Attribute__c).contains(rule.Plan_Guideline__c)  ) )){
                            
                            Boolean qualified = false;            
                            
                            for(EAW_Rule_Detail__c ruleDetail: rule.Rule_Orders__r){
                                System.debug('ruleDetail:::'+system.json.serialize(ruleDetail));
                                //System.debug('tvd.Name::::'+tvd.Name);
                                //System.debug('ruleDetail.Territory__c :::'+ruleDetail.Territory__c );
                                if( ! String.isBlank(ruleDetail.Condition_Operator__c) ) {    
                                    if(usFieldValueSet.contains(ruleDetail.Condition_Field__c) && tvd.Territory__c == 'United States +' ){
                                        if( ruleDetail.Condition_Field__c == 'US Box Office' && tvd.TVD_Local_Currency_Value__c != NULL ) {
                                            qualified = qualifierController.arithmeticDecimalConditionalChecking( tvd.TVD_Local_Currency_Value__c, ruleDetail.Condition_Operator__c, ruleDetail.Condition_Criteria_Amount__c );
                                            System.debug('US qualified :1::'+qualified );
                                        } else if( ruleDetail.Condition_Field__c == 'US Admissions' && tvd.TVD_Local_Admissions_Count__c != NULL ) {
                                            qualified = qualifierController.arithmeticDecimalConditionalChecking( tvd.TVD_Local_Admissions_Count__c, ruleDetail.Condition_Operator__c, ruleDetail.Condition_Criteria_Amount__c );
                                            System.debug('US qualified :2::'+qualified );
                                        } else if( ruleDetail.Condition_Field__c == 'US Number of Screens' && tvd.TVD_Number_of_Screens__c != NULL ) {
                                            qualified = qualifierController.arithmeticDecimalConditionalChecking( tvd.TVD_Number_of_Screens__c, ruleDetail.Condition_Operator__c, ruleDetail.Condition_Criteria_Amount__c );
                                            System.debug('US qualified :3::'+qualified );
                                        }
                                        
                                    } else {
                                    
                                       
                                        if(localFieldValueSet.contains(ruleDetail.Condition_Field__c) && tvd.Territory__c == ruleDetail.Territory__c ){
                                            
                                            if(ruleDetail.Condition_Field__c == 'Local Box Office' && tvd.TVD_Local_Currency_Value__c != NULL ){
                                                qualified = qualifierController.arithmeticDecimalConditionalChecking( tvd.TVD_Local_Currency_Value__c, ruleDetail.Condition_Operator__c, ruleDetail.Condition_Criteria_Amount__c );
                                                System.debug('qualified :1::'+qualified );
                                            } else if(ruleDetail.Condition_Field__c == 'Local Admissions' && tvd.TVD_Local_Admissions_Count__c != NULL ){
                                                qualified = qualifierController.arithmeticDecimalConditionalChecking( tvd.TVD_Local_Admissions_Count__c, ruleDetail.Condition_Operator__c, ruleDetail.Condition_Criteria_Amount__c );
                                                 System.debug('qualified :2::'+qualified );
                                            } else if(ruleDetail.Condition_Field__c == 'Local Number of Screens' && tvd.TVD_Number_of_Screens__c != NULL ){
                                                qualified = qualifierController.arithmeticDecimalConditionalChecking( tvd.TVD_Number_of_Screens__c, ruleDetail.Condition_Operator__c, ruleDetail.Condition_Criteria_Amount__c );
                                                System.debug('qualified :3::'+qualified );
                                            }
                                        }
                                        
                                    }                   
                                }
                              /*  if( tvd.Name == 'United States +' ){
                                    if( ! String.isBlank(ruleDetail.Condition_Operator__c) ) {
                                        if( ruleDetail.Condition_Field__c == 'US Box Office' && tvd.TVD_US_Box_Office__c != NULL ) {
                                            qualified = qualifierController.arithmeticDecimalConditionalChecking( tvd.TVD_US_Box_Office__c, ruleDetail.Condition_Operator__c, ruleDetail.Condition_Criteria_Amount__c );
                                        } else if( ruleDetail.Condition_Field__c == 'US Admissions' && tvd.TVD_Local_Admissions_Count__c != NULL ) {
                                            qualified = qualifierController.arithmeticDecimalConditionalChecking( tvd.TVD_Local_Admissions_Count__c, ruleDetail.Condition_Operator__c, ruleDetail.Condition_Criteria_Amount__c );
                                        } else if( ruleDetail.Condition_Field__c == 'US Number of Screens' && tvd.TVD_Number_of_Screens__c != NULL ) {
                                            qualified = qualifierController.arithmeticDecimalConditionalChecking( tvd.TVD_Number_of_Screens__c, ruleDetail.Condition_Operator__c, ruleDetail.Condition_Criteria_Amount__c );
                                        }
                                    }
                                    
                                } else {
                                
                                    System.debug('tvd:::'+tvd);
                                    System.debug('ruleDetail:::'+ruleDetail);
                                    System.debug('tvd.Name::::'+tvd.Name);
                                    System.debug('ruleDetail.Territory__c :::'+ruleDetail.Territory__c );
                                    
                                    if( tvd.Name == ruleDetail.Territory__c ){
                                        if( ! String.isBlank(ruleDetail.Condition_Operator__c) ) {
                                            if(ruleDetail.Condition_Field__c == 'Local Box Office' && tvd.TVD_Local_Currency_Value__c != NULL ){
                                                qualified = qualifierController.arithmeticDecimalConditionalChecking( tvd.TVD_Local_Currency_Value__c, ruleDetail.Condition_Operator__c, ruleDetail.Condition_Criteria_Amount__c );
                                                System.debug('qualified :1::'+qualified );
                                            } else if(ruleDetail.Condition_Field__c == 'Local Admissions' && tvd.TVD_Local_Admissions_Count__c != NULL ){
                                                qualified = qualifierController.arithmeticDecimalConditionalChecking( tvd.TVD_Local_Admissions_Count__c, ruleDetail.Condition_Operator__c, ruleDetail.Condition_Criteria_Amount__c );
                                                 System.debug('qualified :2::'+qualified );
                                            } else if(ruleDetail.Condition_Field__c == 'Local Number of Screens' && tvd.TVD_Number_of_Screens__c != NULL ){
                                                qualified = qualifierController.arithmeticDecimalConditionalChecking( tvd.TVD_Number_of_Screens__c, ruleDetail.Condition_Operator__c, ruleDetail.Condition_Criteria_Amount__c );
                                                System.debug('qualified :3::'+qualified );
                                            }
                                        }
                                    }
                                    
                                }*/
                                
                                if( qualified ) break;
                            }                       
                            
                            if(qualified)
                            {
                                //create a map of plan guideline and title and use the method of title plan controller to create plans and windows
                                Set<Id> planGuidelineIdSet = new Set<Id>();
                                if( titleAndPlanGuidelineIdMap.containsKey(tvd.Title_Attribute__c) ) {
                                    planGuidelineIdSet = titleAndPlanGuidelineIdMap.get(tvd.Title_Attribute__c);
                                }
                                planGuidelineIdSet.add(rule.Plan_Guideline__c);
                                qualifiedPlanGuidelineSet.add(rule.Plan_Guideline__c);
                                titleAndPlanGuidelineIdMap.put(tvd.Title_Attribute__c, planGuidelineIdSet);
                            }
                            //LRCC-1394: Notification.
                            else if(planGuidelineIdsSet == null) {
                                disQualifiedPG.add(rule.Plan_Guideline__c);
                                boxOfficeTitleIds.add(tvd.Title_Attribute__c);
                            }
                        }
                    }
                }
                
                if(!lstChatterGroup.isEmpty()) {
                
                    if(disQualifiedPG.size() > 0 && boxOfficeTitleIds.size() > 0 && ns.TACategoryNotification__c == true && ns.TATitlePlanDisQualifierNotification__c == true) {
                    
                        for(EAW_Plan__c plan: [SELECT Id, Plan_Guideline__c, Plan_Guideline__r.Name, EAW_Title__c, EAW_Title__r.Name__c FROM EAW_Plan__c WHERE Plan_Guideline__c IN: disQualifiedPG AND EAW_Title__c IN: boxOfficeTitleIds]) {
                            
                            if(checkRecursiveDataforNotification.isRecursiveData(plan.Id)) {
                                
                                if(String.isBlank(bodyContent)) {
                                    bodyContent = plan.EAW_Title__r.Name__c +' - disqualifies for '+plan.Plan_Guideline__r.Name+'.'+'\n'+System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+plan.Id;
                                }
                                else {
                                    bodyContent += '\n\n'+ plan.EAW_Title__r.Name__c +' - disqualifies for '+plan.Plan_Guideline__r.Name+'.'+'\n'+System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+plan.Id;
                                }
                            }
                        }               
                    }
                    
                    //LRCC-1394 - Notification.
                    if(String.isNotBlank(bodyContent)) {
                    
                        String temp = bodyContent;
                        
                        if(bodyContent.length() < 10000) {
                            bodyContent = temp.substring(0, temp.length());
                        } 
                        else {
                            bodyContent = temp.substring(0,9999);    
                        }
                                            
                        feedItems.add(
                            new FeedItem(
                                Body = bodyContent,
                                ParentId = lstChatterGroup[0].Id
                            )
                        );  
                                
                        if(feedItems != null && feedItems.size() > 0) {
                            insert feedItems;
                        }
                    }
                }
                
                System.debug('::::: titleAndPlanGuidelineIdMap :::::'+titleAndPlanGuidelineIdMap);
                
                if( titleAndPlanGuidelineIdMap != NULL && titleAndPlanGuidelineIdMap.size() > 0 ){
                    
                    Map<Id, Set<Id>> qualifiedTitleAndPGMap = new Map<Id, Set<Id>>();
                    for( EAW_Title__c eawTitle : [ Select Id From EAW_Title__c Where Id IN : titleAndPlanGuidelineIdMap.keySet() ]){
                        
                        qualifiedTitleAndPGMap.put(eawTitle.Id, titleAndPlanGuidelineIdMap.get(eawTitle.id));
                        qualifiedTitleIdSet.add(eawTitle.Id);
                    } 
                     //LRCC - 1239
                     System.debug('::::: qualifiedTitleAndPGMap :::::'+qualifiedTitleAndPGMap);
                     System.debug('::::: qualifiedPlanGuidelineSet :::::'+qualifiedPlanGuidelineSet);
                    EAW_PlanGuidelineQualifierController.createPlanAndWindowsForQualifiedTitles(qualifiedTitleAndPGMap,qualifiedPlanGuidelineSet);
                    /*for( Id titleId : titleAndPlanGuidelineIdMap.keySet() ){
                        
                        for(Id planGuidelineId : titleAndPlanGuidelineIdMap.get(titleId) ){
                            if( edmAndEAWTitleMap.containsKey(titleId) ){
                                Map<String, List<SObject>> titlePlanAndWindowsResultMap = EAW_TitlePlanWindowController.createPlan(edmAndEAWTitleMap.get(titleId), planGuidelineId, null, null, false);
                            }
                        }                    
                    } */               
                }
            }
            return qualifiedTitleIdSet;
            
        } catch(Exception ex) {
            System.debug('<<--Exception-->>'+ex.getMessage() + '<<--StackTrace-->>'+ex.getStackTraceString());
            return null;
        }
    }
    
    public void processTVDBoxOfficeToQualifyTitleForUpdate( List<EAW_Box_Office__c> newTVDList, List<EAW_Box_Office__c> oldTVDList ){
        
        try {
        
            if( newTVDList != NULL && oldTVDList != NULL ){
                
                List<EAW_Box_Office__c> tvdListToProcess = new List<EAW_Box_Office__c>();
                    
                for( Integer i= 0; i < newTVDList.size(); i++ ){
                    
                    EAW_Box_Office__c newTvd = newTVDList[i];
                    EAW_Box_Office__c oldTvd = oldTVDList[i];
                    //LRCC-1510 - Converting Name field From Text to Auto Number, before that remove the references.
                    if( /*newTvd.Name != oldTvd.Name ||*/ newTvd.TVD_US_Box_Office__c != oldTvd.TVD_US_Box_Office__c || newTvd.TVD_Local_Admissions_Count__c != oldTvd.TVD_Local_Admissions_Count__c || newTvd.TVD_Number_of_Screens__c != oldTvd.TVD_Number_of_Screens__c || newTvd.TVD_Local_Currency_Value__c != oldTvd.TVD_Local_Currency_Value__c || newTvd.TVD_Local_Admissions_Count__c != oldTvd.TVD_Local_Admissions_Count__c ){
                        tvdListToProcess.add(newTvd);
                    }
                    
                }
                
                if( tvdListToProcess.size() > 0 ){
                    Set<Id> qualifiedTitleIdSet = processTVDBoxOfficeToQualifyTitleForInsert(tvdListToProcess, null);
                }
            }            
        } catch(Exception ex) {
             System.debug('<<--Exception-->>'+ex.getMessage());
        }
    }
    
    //Notifications related implementation: LRCC-1048
    public void sentNotifcationsToTPVReviewGroup(List<EAW_Box_Office__c> triggerOld, List<EAW_Box_Office__c> triggerNew) {
        
        List<FeedItem> feedItems = new List<FeedItem>();
        Set<Id> titleIds = new Set<Id> ();
        Map<Id, String> titleIdName = new Map<Id, String> ();
        
        try {
            
            if(!lstChatterGroup.isEmpty()) {
                
                for(EAW_Box_Office__c box: triggerNew) {
                    titleIds.add(box.Title_Attribute__c);
                }
                
                if(titleIds != null && titleIds.size() > 0) {
                        
                    for(EAW_Title__c title: [SELECT Id, Name__c FROM EAW_Title__c WHERE Id IN: titleIds]) {
                        titleIdName.put(title.Id, title.Name__c);
                    }
                    
                    if(titleIdName != null && titleIdName.size() > 0) {
                        
                        for(Integer i = 0; i < triggerNew.size(); i++) {
                            
                            if(checkRecursiveDataforNotification.isRecursiveData(triggerNew[i].Id)) {
                            
                                String bodyContent = '';
                                
                                EAW_Box_Office__c oldOffice;
                                
                                if(triggerOld != null && triggerOld.size() > 0) {
                                    oldOffice = triggerOld.get(i);
                                }
                                
                                EAW_Box_Office__c newOffice = triggerNew.get(i);
                                
                                if(ns.TVDCategoryNotification__c == true) {
                                    
                                    //NA-13
                                    if(ns.TVDUSBoxOfficeNotifcation__c == true) {
                                        
                                        if((oldOffice == null || oldOffice.TVD_US_Box_Office__c == null) && newOffice.TVD_US_Box_Office__c != null) {
                                            bodyContent += titleIdName.get(newOffice.Title_Attribute__c) +' - U.S. Box Office has been added '+newOffice.TVD_US_Box_Office__c+'.'+'\n';     
                                        }
                                        else if(oldOffice != null 
                                                && newOffice.TVD_US_Box_Office__c != null
                                                && newOffice.TVD_US_Box_Office__c != oldOffice.TVD_US_Box_Office__c) {
                                        
                                            bodyContent += titleIdName.get(newOffice.Title_Attribute__c) +' - U.S. Box Office has been changed from '+oldOffice.TVD_US_Box_Office__c+' to '+newOffice.TVD_US_Box_Office__c+'.'+'\n';     
                                        }
                                        else if(oldOffice != null 
                                                && oldOffice.TVD_US_Box_Office__c != null
                                                && newOffice.TVD_US_Box_Office__c == null) {
                                                
                                            bodyContent += titleIdName.get(newOffice.Title_Attribute__c) +' - U.S. Box Office has been deleted.'+'\n';     
                                        }
                                    }
                                    
                                    //NA-14
                                    if(ns.TVDLocalBoxOfficeNotification__c == true) {
                                        
                                        if((oldOffice == null || oldOffice.TVD_Local_Currency_Value__c == null) && newOffice.TVD_Local_Currency_Value__c != null) {
                                        
                                            bodyContent += titleIdName.get(newOffice.Title_Attribute__c) +' - '+newOffice.Territory__c+' - Local Box Office has been added '+newOffice.TVD_Local_Currency_Value__c+'.'+'\n';     
                                        }
                                        else if(oldOffice != null 
                                                && newOffice.TVD_Local_Currency_Value__c != null
                                                && newOffice.TVD_Local_Currency_Value__c != oldOffice.TVD_Local_Currency_Value__c) {
                                            
                                            bodyContent += titleIdName.get(newOffice.Title_Attribute__c) +' - '+newOffice.Territory__c+' - Local Box Office has been changed from '+oldOffice.TVD_Local_Currency_Value__c+' to '+newOffice.TVD_Local_Currency_Value__c+'.'+'\n';     
                                        }
                                        else if(oldOffice != null 
                                                && oldOffice.TVD_Local_Currency_Value__c != null
                                                && newOffice.TVD_Local_Currency_Value__c == null) {
                                            
                                            bodyContent += titleIdName.get(newOffice.Title_Attribute__c) +' - '+newOffice.Territory__c+' - Local Box Office has been deleted.'+'\n';     
                                        }
                                    }
                                    
                                    //NA-16
                                    if(ns.TVDLocalAdmissionsNotification__c == true) {
                                    
                                        if((oldOffice == null || oldOffice.TVD_Local_Admissions_Count__c == null) && newOffice.TVD_Local_Admissions_Count__c != null && newOffice.Territory__c != 'United States +') {
                                        
                                            bodyContent += titleIdName.get(newOffice.Title_Attribute__c) +' - '+newOffice.Territory__c+' - Local Admissions has been added '+newOffice.TVD_Local_Admissions_Count__c+'.'+'\n';     
                                        }
                                        else if(oldOffice != null 
                                                && newOffice.TVD_Local_Admissions_Count__c != null
                                                && newOffice.Territory__c != 'United States +'
                                                && newOffice.TVD_Local_Admissions_Count__c != oldOffice.TVD_Local_Admissions_Count__c) {
                                            
                                            bodyContent += titleIdName.get(newOffice.Title_Attribute__c) +' - '+newOffice.Territory__c+' - Local Admissions has been changed from '+oldOffice.TVD_Local_Admissions_Count__c+' to '+newOffice.TVD_Local_Admissions_Count__c+'.'+'\n';     
                                        }
                                        else if(oldOffice != null 
                                                && oldOffice.TVD_Local_Admissions_Count__c != null
                                                && newOffice.Territory__c != 'United States +'
                                                && newOffice.TVD_Local_Admissions_Count__c == null) {
                                            
                                            bodyContent += titleIdName.get(newOffice.Title_Attribute__c) +' - '+newOffice.Territory__c+' - Local Admissions has been deleted.'+'\n';     
                                        }
                                    }
                                    
                                    //NA-17
                                    if(ns.TVDNumberOfScreensNotification__c == true) {
                                        
                                        if((oldOffice == null || oldOffice.TVD_Number_of_Screens__c == null) && newOffice.TVD_Number_of_Screens__c != null) {
                                        
                                            bodyContent += titleIdName.get(newOffice.Title_Attribute__c) +' - '+newOffice.Territory__c+' - Number of Screens has been added '+newOffice.TVD_Number_of_Screens__c+'.'+'\n';     
                                        }
                                        else if(oldOffice != null 
                                                && newOffice.TVD_Number_of_Screens__c != null
                                                && newOffice.TVD_Number_of_Screens__c != oldOffice.TVD_Number_of_Screens__c) {
                                            
                                            bodyContent += titleIdName.get(newOffice.Title_Attribute__c) +' - '+newOffice.Territory__c+' - Number of Screens has been changed from '+oldOffice.TVD_Number_of_Screens__c+' to '+newOffice.TVD_Number_of_Screens__c+'.'+'\n';     
                                        }
                                        else if(oldOffice != null 
                                                && oldOffice.TVD_Number_of_Screens__c != null
                                                && newOffice.TVD_Number_of_Screens__c == null) {
                                                
                                            bodyContent += titleIdName.get(newOffice.Title_Attribute__c) +' - '+newOffice.Territory__c+' - Number of Screens has been deleted.'+'\n';     
                                        }
                                    }
                                    
                                    if(String.isNotBlank(bodyContent)) {
                                        feedItems.add(
                                            new FeedItem(
                                                Body = bodyContent+'\n'+newOffice.SFDC_Base_Instance__c+'/'+newOffice.Id,
                                                ParentId = lstChatterGroup[0].Id
                                            )
                                        );
                                    } 
                                }
                            }
                        }
                        
                        if(feedItems != null && feedItems.size() > 0) {
                            insert feedItems;
                        }
                    }
                }
            }
        }
        catch(Exception e) {} 
    }
}