public with sharing class EAW_TPDController_v2 {

    /**
     *  APPLY LATEST VERSION
     *  - (Params) One Plan Id, and Window Ids (windows that are attached to that plan)
     *  - (Return Values) User will be returned Windows/Plans
     *
     *  1. Check if there is a version Update
     *  2. If (there exists a new version) Then:
     *      a. Plans will copy the value of their latest version
     *      b. Windows will copy the value of their latest version
     *      c. Send the Updated Plans/Windows to a <BLACK-BOX> that will recalculate
     *      d. Save the Updated Plans/Windows
     *  3. Return the new plan/window to the UI to update
     *
     */
    @AuraEnabled
    public static Map<String, List<SObject>> applyLatestVersion(String planId, List<String> windowIds) {

        // return objects with changed values
        Map<String, List<SObject>> results = new Map<String, List<SObject>>();

        // flags to see if we're sending a recalc msg
        Boolean planHasNewerVersion = false; // if this is set to true, then we will make a call to recalc Plans/Windows
        Boolean windowHasNewerVersion = false; // if this is set to true, then we will make a call to recalc Plans/Windows

        // Refs so we can update everything at once and stitch together the result
        EAW_Plan__c currPlan;
        List<EAW_Window__c> currWindowList = new List<EAW_Window__c>();

        // Fields that we query windows for
        String windowQueryFields =  'Id, ' +
                'Name, ' +
                //LRCC-1560 - Replace Customer lookup field with Customer text field
                //'Customer__c, ' +
                'Customers__c, ' +
                'Start_Date__c, ' +
                'End_date__c, ' +
                'Tracking_Date__c, ' +
                'Outside_Date__c, ' +
                'Media__c, ' +
                'Window_Type__c';

        // Fields we query plans for
        String planQueryFields = 'Name, ' +
                'Sales_Region__c, ' +
                'Territory__c, ' +
                'Language__c, ' +
                'Release_Type__c, ' +
                'Product_Type__c';


        // OUTER TRY CATCH BLOCK -> if anything is wrong throw an error
        // the inner try catches are expected exceptions, and we handle those accordingly
        try {
            // 1) Check for updates with plan
            if(planId != null) {
                // Grab the Ref to curr Plan Guideline && it's next_version field
                String planQuery = 'SELECT Id, Plan_Guideline__r.Id, Plan_Guideline__r.Next_Version__c FROM EAW_Plan__c WHERE Id=\'' + planId + '\'';
                try{
                    currPlan = Database.query(planQuery);
                } catch (Exception e) {
                    currPlan = null;
                }

                // Check to see if there's a new version, if so ... we're in for fun
                if(currPlan != null && currPlan.Plan_Guideline__r.Next_Version__c != null) {
                    planHasNewerVersion = true;

                    // Grab the current plan guideline's details
                    String planVersionQuery = 'SELECT Id, Next_Version__c FROM EAW_Plan_Guideline__c where Id=\'' + currPlan.Plan_Guideline__r.Next_Version__c + '\'';
                    EAW_Plan_Guideline__c latestPlanGuideline = Database.query(planVersionQuery);
                    System.debug('<<-latestPlanGuideline->>'+latestPlanGuideline);
                    // Traverse the linked list to grab a ref to the latest version
                    while(latestPlanGuideline.Next_Version__c != null) {
                        String traversePlanVersionQuery = 'SELECT Id, Next_Version__c FROM EAW_Plan_Guideline__c where Id=\'' + latestPlanGuideline.Next_Version__c + '\'';
                        latestPlanGuideline = Database.query(traversePlanVersionQuery);
                    }

                    // Got the latest version, let's grab all the necessary fields

                    String planGuidelineQuery = 'SELECT ' + planQueryFields + ' FROM EAW_Plan_Guideline__c where Id=\'' + latestPlanGuideline.Id + '\'';
                    latestPlanGuideline = Database.query(planGuidelineQuery);
                    System.debug('**latestPlanGuideline**'+latestPlanGuideline);
                    // Update the plan, by essentially copying the latest fields ... BUT don't save the changes yet
                    currPlan.Sales_Region__c = latestPlanGuideline.Sales_Region__c;
                    currPlan.Territory__c = latestPlanGuideline.Territory__c;
                    currPlan.Language__c = latestPlanGuideline.Language__c;
                    currPlan.Release_Type__c = latestPlanGuideline.Release_Type__c;
                    currPlan.Product_Type__c = latestPlanGuideline.Product_Type__c;
                }
            }

            // 2) Check for updates with windows
            if(windowIds != null) {
                for(String wId : windowIds) {

                    // Grab the Ref to Window Guideline && it's next_version field

                    String windowQuery = 'SELECT EAW_Window_Guideline__r.Id, EAW_Window_Guideline__r.Next_Version__c FROM EAW_Window__c WHERE Id=\'' + wId + '\'';
                    EAW_Window__c currWindow;
                    try {
                        currWindow = Database.query(windowQuery);
                    } catch(Exception e) {
                        currWindow = null;
                    }
					system.debug('<<-currWindow->>'+currWindow);
                    // Check to see if there's a new version, if so ... we're in for fun
                    if(currWindow != null && currWindow.EAW_Window_Guideline__r.Next_Version__c != null) {
                        windowHasNewerVersion = true;

                        // Query db to get curr pointer values
                        String windowVersionQuery = 'SELECT Id, Next_Version__c FROM EAW_Window_Guideline__c where Id=\'' + currWindow.EAW_Window_Guideline__r.Next_Version__c + '\'';
                        EAW_Window_Guideline__c latestWindowGuideline = Database.query(windowVersionQuery);

                        // Traverse the linked list to grab a ref to the latest version
                        while(latestWindowGuideline.Next_Version__c != null) {
                            String traverseWindowVersionQuery = 'SELECT Id, Next_Version__c FROM EAW_Window_Guideline__c where Id=\'' + latestWindowGuideline.Next_Version__c + '\'';
                            latestWindowGuideline = Database.query(traverseWindowVersionQuery);
                        }

                        // Got the latest version, let's grab all the necessary fields
                        String windowGuidelineQuery = 'SELECT ' + windowQueryFields + ' FROM EAW_Window_Guideline__c where Id=\'' + latestWindowGuideline.Id + '\'';
                        latestWindowGuideline = Database.query(windowGuidelineQuery);

                        // Update the Window, by essentially copying the latest fields ... BUT don't save the changes yet
                        
                        //LRCC-1560 - Replace Customer lookup field with Customer text field
                        //currWindow.Customer__c = latestWindowGuideline.Customer__c;
                        currWindow.Customers__c = latestWindowGuideline.Customers__c;
                        currWindow.Start_Date__c = latestWindowGuideline.Start_Date__c;
                        currWindow.End_Date__c = latestWindowGuideline.End_Date__c;
                        currWindow.Outside_Date__c = latestWindowGuideline.End_Date__c;
                        currWindow.Tracking_Date__c = latestWindowGuideline.Tracking_Date__c;
                        currWindow.Media__c = latestWindowGuideline.Media__c;
                        currWindow.Window_Type__c = latestWindowGuideline.Window_Type__c;
                        currWindow.EAW_Window_Guideline__c = latestWindowGuideline.Id;

                        // Add to list to be saved
                        currWindowList.add(currWindow);
                    }
                }
            }

            //TODO: send to rules engine to recalculate things
            // 3) Send update for recalculation
            if(planHasNewerVersion || windowHasNewerVersion) {
                // Send msg to recalculate
            }

            // Put updated plan in our result map if we have one
            if(planHasNewerVersion) {
                update currPlan;

                // Return our items for the UI to display
                List<SObject> plans = new List<SObject>();
                plans.add(currPlan);
                results.put('plan', plans);
            }

            // Put updated windows in our result map if we have any
            if(windowHasNewerVersion) {
                update currWindowList;

                // Return our items for the UI to display
                results.put('windows', currWindowList);
            }

        } catch (Exception e) {
            System.debug(e);
            throw e;
            // return null means the JS controller doesn't do anything
            //return null;
        }

        return results;
    }


    /**
     * MASS UPDATE – WINDOWS
     *  (Params) Window Ids, Start/End Date vals, Window Status, Tag vals, Notes
     *  (Return Value) List Windows w/ changes
     *
     */
    @AuraEnabled
    public static List<EAW_Window__c> massUpdateWindows(List<String> windowIds, List<String> tagIds,
                                            String startDate, String endDate, String noteText, String windowStatus,
                                            Boolean clearStartDate, Boolean clearEndDate, Boolean clearTags, String startDateText, String endDateText,Boolean  retired,String noteTitle) {
        
        System.debug('::::: tagIds :::::'+tagIds);
        System.debug('::::: clearTags :::::'+clearTags);
        System.debug('::::: retired :::::'+retired);                                        
        
        // results of this method
        List<EAW_Window__c> windowList = null; // <<-- WHAT WE RETURN

        if(windowIds != null && windowIds.size() > 0) {

            // Grab the Refs to the existing windows
            try {
                //windowList = [SELECT Id, Start_Date__c, End_Date__c FROM EAW_Window__c WHERE Id IN :windowIds];
                // //LRCC-1327 for add Retired__c
                String windowListQuery = 'SELECT Id, Start_Date__c, Start_Date_Rule__c, End_Date__c, End_Date_Rule__c,Retired__c FROM EAW_Window__c WHERE Id IN :windowIds';
                windowList = Database.query(windowListQuery);
				System.debug('::::::::::'+windowList);
            } catch(Exception e) {
                return null;  //if we error out return null so we know somethings wrong, don't update anything
            }

            // Stuff we insert later
            List<EAW_Window_Tag_Junction__c> tagJuncList = new List<EAW_Window_Tag_Junction__c>(); // List of new Junctions created for the tags
            List<Note> noteList = new List<Note>();

            // Loop thru all windows && lets mass update them
            for(EAW_Window__c window : windowList) {
				
				//window.Start_Date__c = clearStartDate ? null : ((startDate == null || startDate == '') ? window.Start_Date__c : Date.valueOf(startDate));
				
				//LRCC-1154
                // Set start/end dates, lambda statements below for clear, or update
                if( clearStartDate ){
                	window.Start_Date__c = NULL;
                	window.Start_Date_Rule__c = '';
                } else if( ! String.isBlank(startDateText) ) {
            		window.Start_Date__c = NULL;
            		window.Start_Date_Rule__c = startDateText;
            	} else {
            		window.Start_Date__c = (startDate == null || startDate == '') ? window.Start_Date__c : Date.valueOf(startDate);
                	window.Start_Date_Rule__c = '';
            	}
                
                //window.End_Date__c = clearEndDate ? null : ((endDate == null || endDate == '') ? window.End_Date__c : Date.valueOf(endDate));
				
				//LRCC-1154
				if( clearEndDate ){
                	window.End_Date__c = NULL;
                	window.End_Date_Rule__c = '';
                } else if( ! String.isBlank(endDateText) ) {
            		window.End_Date__c = NULL;
            		window.End_Date_Rule__c = endDateText;
            	} else {
            		window.End_Date__c = (endDate == null || endDate == '') ? window.End_Date__c : Date.valueOf(endDate);
                	window.End_Date_Rule__c = '';
            	}
            	
                // mass update status if we're provided one
                window.Status__c = (windowStatus == null || windowStatus == '') ? window.Status__c : windowStatus;

                // create the new relations to tags 

                if(tagIds != null && tagIds.size() > 0) {
                    for(String id : tagIds) {
                        EAW_Window_Tag_Junction__c tagJunc = new EAW_Window_Tag_Junction__c();
                        tagJunc.Window__c = window.Id;
                        tagJunc.Tag__c = id;
                        tagJuncList.add(tagJunc);
                    }
                }

                // simply append (add) notes to the object
                // LRCC-1327
                if(noteTitle != null && noteTitle != '') {
                    noteList.add(new Note(Title = noteTitle, Body = noteText, ParentId = window.Id));
                }
                //LRCC-1327
                window.Retired__c  = retired ;
            }
            
            System.debug('::::: tagJuncList :::::'+tagJuncList);
            
            try {
                // Insert/update our new records
                update windowList;

                if(clearTags) {
                    // Delete old tags, and then insert new ones
                    delete [SELECT Id, Window__r.Id FROM EAW_Window_Tag_Junction__c WHERE Window__r.Id IN :windowIds];
                }
                
                if(tagJuncList.size() > 0) {
                    
                    insert tagJuncList;
                    
                    string windowTags = '';
                    
                    List<EAW_Tag__c> wtList =  [ Select Id, Name From EAW_Tag__c Where Id IN : tagIds ];
                    
                    if( wtList != NULL && wtList.size() > 0 ) {
                        
                        for( Integer i=0; i < wtList.size(); i++ ){
                            if( i == wtList.size()-1 ){
                                windowTags += wtList[i].Name;
                            } else {
                                windowTags += wtList[i].Name + ', ';
                            }
                        }
                        
                        if( windowTags != '' ){
                            
                            for(EAW_Window__c window : windowList) {
                                window.CurrencyIsoCode = windowTags;
                            }
                            
                        }
                    }
                }
                
                if(noteList.size() > 0) {
                    insert noteList;
                }

            } catch(Exception e) {
                System.debug(e);
                throw e;
            }

        }

        return windowList;
    }

    @AuraEnabled
    public static Map<String, List<String>> deleteTitlePlanWindows(List<String> planIds, List<String> windowIds) {

        // Return the list of Ids so we can remove them from the UI later
        Map<String, List<String>> deleteResults = new Map<String, List<String>>();
        deleteResults.put('planIds', planIds);
        deleteResults.put('windowIds', windowIds);

        if(planIds != null && planIds.size() > 0) {
            try {
                delete [SELECT Id from EAW_Plan__c WHERE Id IN :planIds];
            } catch(Exception e) {
                System.debug('Error Deleting Plans: ' + e);
                return null;
            }
        }

        if(windowIds != null && windowIds.size() > 0) {
            try {
                // DON'T FORGET TO DELETE ALL TAGS JUNCS ASSOCIATED TO THESE WINDOWS
                // ELSE WE CAN'T DELETE THE TAGS LATER ON
                // Delete Juncs to Tags
                delete [SELECT Id FROM EAW_Window_Tag_Junction__c WHERE Window__r.Id IN :windowIds];

                // Now del the windows
                delete [SELECT Id from EAW_Window__c WHERE Id IN :windowIds];
            } catch(Exception e) {
                System.debug('Error Deleting Windows: ' + e);
                return null;
            }
        }

        return deleteResults;
    }
    
    @AuraEnabled
    public static Map<String,List<picklistEntry>> getPicklistValues(String objectPickListString) {
        Map<String,List<String>> objectPickListMap = (Map<String,List<String>>)System.JSON.deserialize(objectPickListString,Map<String,List<String>>.class);
        Map<String,List<picklistEntry>> pickListValuesMap = new Map<String,List<picklistEntry>>();
        for(String strObjectName: objectPickListMap.keySet()){
            Schema.SObjectType SObjectTypeObj = Schema.getGlobalDescribe().get(strObjectName);
            Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
            Map<String, Schema.SObjectField> fieldMap = DescribeSObjectResultObj.fields.getMap();   
            for(String fieldName :  objectPickListMap.get(strObjectName)){
                List<picklistEntry> picklistValues = new List<picklistEntry>();
                Schema.DescribeFieldResult fieldResult = fieldMap.get(fieldName).getDescribe();
                    List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                    system.debug('::::::: Ple ::::::::'+ple);
                    for( Schema.PicklistEntry pValues : ple){
                        if( pValues.isActive() ){
                            picklistValues.add(new picklistEntry(pValues.getLabel(),  pValues.getValue()));
                        }
                    }
                    pickListValuesMap.put(fieldName,picklistValues);
            }
            
        }
        return pickListValuesMap;
     }
     
     public class picklistEntry {
        @AuraEnabled
        public String label { get; set; }
        @AuraEnabled
        public String value { get; set; }
        public picklistEntry(String label, String value) {
            this.label = label;
            this.value = value;
        }
    }

}