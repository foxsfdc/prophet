public with sharing class EAW_PromotionDateController {
    
    //Wrapper class: Finally we need following details of Promotion Date
	public class PromotionDate {
		public String versionId;
		public String territoryCode;
		public String lockStatus;
		public Date releaseDate;
		public String recordId;
		public String languageId;
		public String languageCode;
        public String finTitleId;
        //public boolean deletedFlag;
		public String channel;
		//public boolean active;
		public String status;
		public String recordStatus;
		public Integer foxId;
		
		public PromotionDate(){}
		
		public PromotionDate(String versionId,String territoryCode,String lockStatus,Date releaseDate,String recordId,String languageId,String languageCode,String finTitleId,
								String channel,String status,String recordStatus,Integer foxId){
			this.versionId=versionId;
			this.territoryCode=territoryCode;
			this.lockStatus=lockStatus;
			this.releaseDate=releaseDate;
			this.recordId=recordId;
			this.languageId=languageId;
			this.languageCode=languageCode;
			this.finTitleId=finTitleId;
			this.channel=channel;
			this.status=status;
			this.recordStatus=recordStatus;
			this.foxId=foxId;
			
		}
		
		boolean equal(PromotionDate pd){
			return this.finTitleId==pd.finTitleId && this.channel==pd.channel && this.languageCode==pd.languageCode && this.territoryCode==pd.territoryCode;
		}
    }
    
    public class AuditHistory {
        public String createdBy;
		public Date createdDate;
		public String newValue;
		public String oldValue;
		public String fieldName;
		
		public AuditHistory(){}
		
		public AuditHistory(String createdBy,Date createdDate,String newValue,String oldValue,String fieldName){
			this.createdBy=createdBy;
			this.createdDate=createdDate;
			this.newValue=newValue;
			this.oldValue=oldValue;
			this.fieldName=fieldName;
		}
    }
    
    /*
	2)	Provide Apex Class / Function (synchronous) for when Xchange Batch processes update dating records.
	Notes: perhaps #1 simply invokes this same method to write to your temp stage object
	*/
	/*
	**************Don't change Method name or data type of Parameter***********************
	*/
	/*
	Deprecated
	*/
	/*
    public static void handlePromotionDatesOld(Map<String, Set<String>> releaseKeys){
    	if(releaseKeys!=null && !releaseKeys.isempty()){
	    	List<EAW_Inbound_Notification__c> eawINList = new List<EAW_Inbound_Notification__c>();
	    	String timestamp = ''+DateTime.now().getTime();
	    	integer countt = 0;
	    	for(String finProdId:releaseKeys.keyset()){
	    		if(releaseKeys.get(finProdId)!=null){
	    			eawINList.add(new EAW_Inbound_Notification__c(Name = timestamp+countt,Source_System__c='foxchange',FIN_PROD_ID__c=finProdId,Territory_Codes__c=String.join(new List<String>(releaseKeys.get(finProdId)), ';'),Created_Date__c=Datetime.now()));
	    			countt++;
	    		}
	    	}
            insert eawINList;
    	}
    }
    */
    public static void handlePromotionDates(Map<String, Set<String>> releaseKeys){
    	if(releaseKeys!=null && !releaseKeys.isempty()){
    		system.debug('handlePromotionDates releaseKeys: '+system.JSON.serialize(releaseKeys));
	    	List<EAW_Inbound_Notifications__c> eawINList = new List<EAW_Inbound_Notifications__c>();
	    	//String timestamp = ''+DateTime.now().getTime();
	    	//integer countt = 0;
	    	for(String finProdId:releaseKeys.keyset()){
	    		if(releaseKeys.get(finProdId)!=null){
	    			eawINList.add(new EAW_Inbound_Notifications__c(/*Name = timestamp+countt,*/Source_System__c='FoXchange',FIN_PROD_ID__c=finProdId,Territory_Codes__c=String.join(new List<String>(releaseKeys.get(finProdId)), ';')/*,Created_Date__c=Datetime.now()*/));
	    			//countt++;
	    		}
	    	}
            insert eawINList;
    	}
    }
	
	/*
	3)	Provide Wrapper Class definition for when Xchange outputs List of Release-Dates-Objects
		Include new properties: lockStatus, Status, recordStatus, and foxId
		Remove existing properties: active and deleted
		Add constructor so object can be created with all properties
	*/
	/*public static List<HEP_Promotion_Date_API.PromotionDate> getPromotionDates(Map<String, Set<String>> releaseKeys){
		system.debug('getPromotionDates: '+releaseKeys);
		List<HEP_Promotion_Date_API.PromotionDate> promotionDates = new List<HEP_Promotion_Date_API.PromotionDate>();
		if(releaseKeys!=null && !releaseKeys.isempty()){
			for(String finProdId:releaseKeys.keyset()){
				List < HEP_Promotion_Date_API.PromotionDate > lstPromotionDates = HEP_Promotion_Date_API.getPromotionDates_Prophet( finProdId, releaseKeys.get(finProdId) );
				system.debug('lstPromotionDates: '+lstPromotionDates);
				promotionDates.addall(lstPromotionDates);
			}
		}
		return promotionDates;
	}*/
	
	public static List<HEP_Promotion_Date_API.PromotionDate> getPromotionDates(String finProdId, Set<String> territories ){
		List < HEP_Promotion_Date_API.PromotionDate > lstPromotionDates = new List<HEP_Promotion_Date_API.PromotionDate>();
		if(finProdId!=null && territories!=null && !territories.isempty()){
			lstPromotionDates = HEP_Promotion_Date_API.getPromotionDates_Prophet( finProdId, territories );
			system.debug('lstPromotionDates: '+system.json.serialize(lstPromotionDates));
			//for (  HEP_Promotion_Date_API.PromotionDate oPromotionDate : lstPromotionDates ) {
					//system.debug( 'oPromotionDate=' + oPromotionDate );
			//}
		}
		return lstPromotionDates;
	}
	
	public static Map<String,Date> getTitleFAD(set<String> sFinTitleIds){
		system.debug('sFinTitleIds: '+sFinTitleIds);
		Map<String,Date> titleFadMap = new Map<String,Date>();
		if(sFinTitleIds!=null && !sFinTitleIds.isempty()){
			for(String finProdId:sFinTitleIds){
				if(String.isNotBlank( finProdId )){
					Date dtFAD = HEP_Promotion_Date_API.getTitleFAD_Prophet( finProdId);
					titleFadMap.put(finProdId,dtFAD);
				}
			}
		}
		return titleFadMap;
	}
	
	public static Date getTitleFAD(String finProdId){
		//system.debug('finProdId: '+finProdId);
		if(String.isNotBlank( finProdId )){
			return HEP_Promotion_Date_API.getTitleFAD_Prophet( finProdId);
		}
		return null;
	}
	
	/*
	4)	Provide Wrapper Class definition for when Xchange outputs List of Date-Audit-History-Objects
	Example properties: user, date, fieldname, oldValue, newValue
	Add constructor so object can be created with all properties
	*/
	public static list<HEP_Promotion_Date_API.AuditHistory> getAuditHistory(String HEP_Promotion_Dating_Id) {
    	system.debug('EAW_PromotionDateController HEP_Promotion_Dating_Id__c: '+HEP_Promotion_Dating_Id);
    	List<HEP_Promotion_Date_API.AuditHistory> auditHistories = HEP_Promotion_Date_API.getPromotionDatingAuditHistory( HEP_Promotion_Dating_Id, 10 );
    	system.debug('auditHistories: '+system.json.serialize(auditHistories));
		return auditHistories;
    }
    
    
}