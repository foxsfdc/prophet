public with sharing class EAW_RDGReleaseDatesController {
    
    @AuraEnabled
    public static List<Object> getFields(Map<String, String> objFieldSetMap) {
    
        List<Object>fieldNames = new List<Object>();
        for(String strObjectName: objFieldSetMap.keySet()) {
            fieldNames.add(EAW_FieldSetController.getFields(strObjectName, objFieldSetMap.get(strObjectName)));
        }
        return fieldNames;
    }
    
    @AuraEnabled
    public static List<EAW_Release_Date__c> getReleaseDates(String recordId) {
        
        List<EAW_Release_Date__c> releaseDates = new List<EAW_Release_Date__c> ();
        try {
            releaseDates = [
                SELECT Id, Name, Title__c, Title__r.Name, Release_Date__c, Status__c, Release_Date_Guideline__c, Active__c, 
                Allow_Manual__c, Customers__c, Feed_Date__c, Feed_Date_Status__c, Language__c, Manual_Date__c,
                Media__c, Product_Type__c, Projected_Date__c, EAW_Release_Date_Type__c, Temp_Perm__c, Territory__c 
                FROM EAW_Release_Date__c 
                WHERE Active__c = true AND Release_Date_Guideline__c =: recordId];
                
            return releaseDates;
        } catch (Exception e) {
            throw New AuraHandledException(e.getMessage());
        }
    }
}