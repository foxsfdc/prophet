public with sharing class EAW_ReleaseDateGuidelineHandler {

	/*
	Please note, if you are creating any new method in this handler, make sure add following flag at starting of the method
	to skip the trigger logic.
	if(!checkOuboundRecursiveData.bypassTriggerFire) 
	*/

    //LRCC-1462
    public void populateReleaseDateGuidelineName(List<EAW_Release_Date_Guideline__c> rdgs) {
    	
    	if(!checkOuboundRecursiveData.bypassTriggerFire){
        
	        for(EAW_Release_Date_Guideline__c rdg: rdgs) {
	            
	            String rdgName = rdg.Name__c;
	            String temp = 'clone';
	            
	            if(String.isNotBlank(rdg.EAW_Release_Date_Type__c) && String.isNotBlank(rdg.Territory__c) 
	                && String.isNotBlank(rdg.Product_Type__c)
	                && (rdg.Active__c == true || ( String.isNotBlank(rdgName) && !(rdgName.contains(temp)) ) )) {
	                
	                if(String.isNotBlank(rdg.Language__c)) {
	                    rdg.Name__c = rdg.EAW_Release_Date_Type__c + ' / ' + rdg.Territory__c + ' / ' + rdg.Language__c + ' / ' + rdg.Product_Type__c;
	                }
	                else {
	                    rdg.Name__c = rdg.EAW_Release_Date_Type__c + ' / ' + rdg.Territory__c + ' / ' + rdg.Product_Type__c;
	                }
	                
	                String trimmedName = rdg.Name__c;
	                
	                if(trimmedName.length() < 80) {
	                    rdg.Name = trimmedName.substring(0,trimmedName.length());
	                }
	                else {
	                    rdg.Name = trimmedName.substring(0,80);
	                }
	            }
	        }
    	}
    }
    
    //LRCC-1334
    public void setDefaultLanguage(List<EAW_Release_Date_Guideline__c> TriggerNew) {
        
        if(!checkOuboundRecursiveData.bypassTriggerFire){
	        for(EAW_Release_Date_Guideline__c rd: TriggerNew) {
	        
	             if(rd.Language__c == '' || rd.Language__c == NULL)
	                rd.Language__c = 'No Specific Language';
	        }
        }
    }
    
    //LRCC:1015, LRCC:1130, LRCC:1292
    public void preventDuplicate(List <EAW_Release_Date_Guideline__c> releaseDateGuidelines) {
        if(!checkOuboundRecursiveData.bypassTriggerFire){
	        Set<String> territorySet = new Set<String>();
	        Set<String> productTypeSet =  new Set<String>();
	        Set<String> releaseDateType = new Set<String>();
	        
	        if( releaseDateGuidelines != NULL && releaseDateGuidelines.size() > 0  ){
	            
	            for (EAW_Release_Date_Guideline__c rdg: releaseDateGuidelines) {
	                territorySet.add(rdg.Territory__c);
	                productTypeSet.add(rdg.Product_Type__c);
	                releaseDateType.add(rdg.EAW_Release_Date_Type__c);
	            }
	            
	        }
	        
	        if( territorySet.size() > 0 || productTypeSet.size() > 0 || releaseDateType.size() > 0 ){
	            
	            List < EAW_Release_Date_Guideline__c > existingReleaseDateGuidelines = [
	                Select Id, Name, Territory__c, Product_Type__c, EAW_Release_Date_Type__c, Language__c
	                From EAW_Release_Date_Guideline__c
	                Where Territory__c IN : territorySet AND Product_Type__c IN : productTypeSet  AND EAW_Release_Date_Type__c IN : releaseDateType AND Soft_Deleted__c = 'FALSE'
	            ];
	
	            if (existingReleaseDateGuidelines != null && !existingReleaseDateGuidelines.isEmpty()) {
	    
	                for (EAW_Release_Date_Guideline__c rdg: releaseDateGuidelines) {
	    
	                    for (EAW_Release_Date_Guideline__c existingRdg: existingReleaseDateGuidelines) {
	    
	                        Boolean isDuplicate = false;
	                        String errorMessage = 'A Guideline already exists for Languages with the same Territory, Release Date Type and Product Type';
	                        
	                        if (rdg.Territory__c != null && rdg.Product_Type__c != null && rdg.EAW_Release_Date_Type__c != null && rdg.Soft_Deleted__c == 'FALSE') {
	                        
	                            if (rdg.Territory__c.equals(existingRdg.Territory__c) &&
	                                rdg.Product_Type__c.equals(existingRdg.Product_Type__c) &&
	                                rdg.EAW_Release_Date_Type__c.equals(existingRdg.EAW_Release_Date_Type__c)) {
	                                
	                                /*if (String.isNotBlank(rdg.Language__c) && (rdg.Language__c.equals(existingRdg.Language__c) || rdg.Language__c.equals('No Specific Language') ||
	                                    (String.isNotBlank(existingRdg.Language__c) && existingRdg.Language__c.equals('No Specific Language')))) {
	                                                        
	                                    isDuplicate = true;
	                                }
	                                
	                                if ((String.isBlank(rdg.Language__c) && String.isBlank(existingRdg.Language__c)) || 
	                                    (String.isNotBlank(rdg.Language__c) && String.isBlank(existingRdg.Language__c))){
	                                    isDuplicate = true;
	                                }
	                                
	                                if (String.isBlank(rdg.Language__c) && 
	                                    (String.isNotBlank(existingRdg.Language__c) && existingRdg.Language__c.equals('No Specific Language'))) {
	                                    
	                                    isDuplicate = true;
	                                }*/
	                                
	                                if( ( rdg.Language__c == 'No Specific Language' || String.isBlank(rdg.Language__c) || existingRdg.Language__c == 'No Specific Language' || String.isBlank(existingRdg.Language__c) ) ){
	                                    isDuplicate = true;
	                                } else if( rdg.Language__c == existingRdg.Language__c ){
	                                    isDuplicate = true;
	                                }
	        
	                                if (isDuplicate == true) {
	                                    if (rdg.Id == null || (rdg.Id != null && rdg.Id != existingRdg.Id) ) {
	                                        rdg.addError(errorMessage);
	                                    }
	                                }
	                            }
	                        }
	                    }
	                }
	            }
	        }
	    }
    }
    
    //LRCC:1136
   public void preventDeletionofRDG(List<EAW_Release_Date_Guideline__c> oldRDGs) {
   		if(!checkOuboundRecursiveData.bypassTriggerFire){
            
	        /*Map<Id, List<EAW_Release_Date__c>> rdgAndReleaseDateMap = new Map<Id, List<EAW_Release_Date__c>> ();
	        List<String> relatedRuleBuilderIdList = new List<String> ();
	        
	        if(oldRDGs.isEmpty() == False) {
	        
	            for(EAW_Release_Date__c rd: [SELECT Id, Name, Release_Date_Guideline__c FROM EAW_Release_Date__c WHERE Release_Date_Guideline__c IN: oldRDGs]) {
	                
	                if(rdgAndReleaseDateMap.containsKey(rd.Release_Date_Guideline__c)) {
	                    List<EAW_Release_Date__c> tempList = rdgAndReleaseDateMap.get(rd.Release_Date_Guideline__c);    
	                    tempList.add(rd);
	                    rdgAndReleaseDateMap.put(rd.Release_Date_Guideline__c, tempList);
	                } 
	                else {
	                    rdgAndReleaseDateMap.put(rd.Release_Date_Guideline__c, new List<EAW_Release_Date__c> {rd});
	                }    
	            }
	            
	            //LRCC - 716 added validation when RDG is reffered in rule builder
	            for(EAW_Rule__c relatedRule: [SELECT Id, Conditional_Operand_Id__c, Release_Date_Guideline__c FROM EAW_Rule__c WHERE Conditional_Operand_Id__c != null]) {
	                relatedRuleBuilderIdList.add(relatedRule.Conditional_Operand_Id__c);               
	            }
	            for(EAW_Rule_Detail__c relatedRuleDetail : [SELECT Id, Conditional_Operand_Id__c, Rule_No__r.Release_Date_Guideline__c FROM EAW_Rule_Detail__c WHERE Conditional_Operand_Id__c != null]) {
	                relatedRuleBuilderIdList.add(relatedRuleDetail.Conditional_Operand_Id__c);
	            }
	            
	            if(rdgAndReleaseDateMap.isEmpty() == False || relatedRuleBuilderIdList.isEmpty() == False) {
	                for(EAW_Release_Date_Guideline__c rdg: oldRDGs) {
	                    if(rdgAndReleaseDateMap.get(rdg.Id) != null || relatedRuleBuilderIdList.contains(rdg.Id)) {
	                        rdg.addError('This Release Date Guideline cannot be deleted since it has associated release dates');
	                    }
	                }
	            }        
	        }*/
	        Set<Id> releaseDateGuidelineSet = new Set<Id>(); 
	        for(EAW_Release_Date_Guideline__c rgd : oldRDGs){
	            if(rgd != null) releaseDateGuidelineSet.add(rgd.id);
	        }
	        if(releaseDateGuidelineSet.size() > 0){
	            Map<Id, List<EAW_Release_Date__c>> rdgAndReleaseDateMap = new Map<Id, List<EAW_Release_Date__c>> ();
	            List<EAW_Rule_Detail__c> ruleDetailList = new List<EAW_Rule_Detail__c>();
	            Set<Id> referredConditionalSet = new Set<Id>();
	            Id widowDateRuleRecordTypeId = Schema.SObjectType.EAW_Rule__c.getRecordTypeInfosByName().get('Window Date Calculation').getRecordTypeId();
	            Id releaseDateRuleRecordTypeId = Schema.SObjectType.EAW_Rule__c.getRecordTypeInfosByName().get('Release Date Calculation').getRecordTypeId();
	            for(EAW_Release_Date__c rd: [SELECT Id, Name, Release_Date_Guideline__c FROM EAW_Release_Date__c WHERE Release_Date_Guideline__c IN: oldRDGs]) {
	                
	                if(rdgAndReleaseDateMap.containsKey(rd.Release_Date_Guideline__c)) {
	                    List<EAW_Release_Date__c> tempList = rdgAndReleaseDateMap.get(rd.Release_Date_Guideline__c);    
	                    tempList.add(rd);
	                    rdgAndReleaseDateMap.put(rd.Release_Date_Guideline__c, tempList);
	                } 
	                else {
	                    rdgAndReleaseDateMap.put(rd.Release_Date_Guideline__c, new List<EAW_Release_Date__c> {rd});
	                }    
	            }
	            if(!rdgAndReleaseDateMap.isEmpty()){
	                releaseDateGuidelineSet.removeAll(rdgAndReleaseDateMap.keySet());
	            }
	            if(releaseDateGuidelineSet != null && releaseDateGuidelineSet.size() > 0){
	                ruleDetailList = [SELECT Id,Rule_No__c,Conditional_Operand_Id__c,Rule_No__r.Conditional_Operand_Id__c, Rule_No__r.Window_Guideline__c FROM EAW_Rule_Detail__c WHERE (Conditional_Operand_Id__c IN : releaseDateGuidelineSet OR Rule_No__r.Conditional_Operand_Id__c IN : releaseDateGuidelineSet) AND (Rule_No__r.Window_Guideline__c != null OR Rule_No__r.Release_Date_Guideline__c != null) AND (Rule_No__r.RecordTypeId = :widowDateRuleRecordTypeId OR Rule_No__r.RecordTypeId = :releaseDateRuleRecordTypeId) AND (Rule_No__r.Window_Guideline__r.Status__c != 'Inactive' OR Rule_No__r.Release_Date_Guideline__r.Active__c = true)];
	            }
	            if(ruleDetailList.size() > 0){
	                for(EAW_Rule_Detail__c ruleDetail : ruleDetailList){
	                    if(ruleDetail.Conditional_Operand_Id__c != null){
	                        referredConditionalSet.add(ruleDetail.Conditional_Operand_Id__c);
	                    }
	                    if(ruleDetail.Rule_No__r.Conditional_Operand_Id__c != null){
	                        referredConditionalSet.add(ruleDetail.Rule_No__r.Conditional_Operand_Id__c);
	                    }
	                }
	            }
	            for(EAW_Release_Date_Guideline__c rdg: oldRDGs) {
	                if(!rdgAndReleaseDateMap.isEmpty() && rdgAndReleaseDateMap.get(rdg.Id) != null) {
	                    rdg.addError('This Release Date Guideline cannot be deleted since it has associated release dates');
	                } else if(referredConditionalSet.size() > 0 && referredConditionalSet.contains(rdg.Id)){
	                    rdg.addError('This Release Date Guideline cannot be deleted since it has referred to other guidelines');
	                }
	            }
	        }
	    }
    }
    
    
    //LRCC-1145: During batch processing this logic was changed.
    public void validateRDGAndReleaseDates(List<EAW_Release_Date_Guideline__c> newRDGs, List<EAW_Release_Date_Guideline__c> oldRDGs) {
    	if(!checkOuboundRecursiveData.bypassTriggerFire){
        
	        if( newRDGs != NULL && newRDGs.size() == 1 && ! checkRecursiveData.bypassDateCalculation ){
	            
	            Set<Id> rdgIdSet = new Set<Id>();
	            if( newRDGs[0].Active__c != oldRDGs[0].Active__c && newRDGs[0].Active__c == FALSE ) {
	                rdgIdSet.add(newRDGs[0].Id);
	            }
	            
	            if( rdgIdSet != NULL && rdgIdSet.size() > 0 ) {
	                EAW_DownstreamRuleGuidelinesFindUtil downStreamUtil = new EAW_DownstreamRuleGuidelinesFindUtil();
	                Set<Id> downStreamGuidelineIds = new Set<Id>();
	                
	                Set<Id> downstreamRDGIds = downStreamUtil.findDependentReleaseDateGuidelineToProcess(rdgIdSet);
	                if( downstreamRDGIds != NULL && downstreamRDGIds.size() > 0 ) downStreamGuidelineIds.addAll(downstreamRDGIds);
	                
	                Set<Id> downstreamWGIds = downStreamUtil.findDependentWindowGuidelineToProcess(rdgIdSet);
	                if( downstreamWGIds != NULL && downstreamWGIds.size() > 0 ) downStreamGuidelineIds.addAll(downstreamWGIds);
	                
	                
	                if( downStreamGuidelineIds != NULL && downStreamGuidelineIds.size() > 0 ) {
	                    newRDGs[0].addError('This Guideline cannot be inactivated, since at least one other Guideline refers to it. You must inactivate the referencing Guideline before inactivating this Guideline.');
	                } else if( rdgIdSet != NULL && rdgIdSet.size() > 0 ) {
	                    EAW_BatchReleaseDateSoftDelete softDeleteRDBatch = new EAW_BatchReleaseDateSoftDelete();
	                    softDeleteRDBatch.inActiveGuideLineIds = rdgIdSet;
	                    ID batchprocessid = Database.executeBatch(softDeleteRDBatch, 200);
	                }
	            }
	        }         
	    }
    }
    
    //This logic might not needed since the date calculation logic take care of the release date Active
    //LRCC-1145
    /*public void reActivateRDGAndReleaseDates(List<EAW_Release_Date_Guideline__c> newRDGs, List<EAW_Release_Date_Guideline__c> oldRDGs) {
        
        List<EAW_Release_Date_Guideline__c> newRDGRecords = new List<EAW_Release_Date_Guideline__c> ();
        
        try {
            List<EAW_Release_Date__c> releaseDates = new List<EAW_Release_Date__c> ();
                
            for(Integer i = 0; i < oldRDGs.size(); i++) {
            
                EAW_Release_Date_Guideline__c oldRDG = oldRDGs.get(i);
                EAW_Release_Date_Guideline__c newRDG = newRDGs.get(i);
            
                if(oldRDG != null && newRDG != null && oldRDG.Active__c == false && newRDG.Active__c == true) {
                    newRDGRecords.add(newRDG);
                }
            }
            
            if(newRDGRecords != null && newRDGRecords.size() > 0) {    
                for(EAW_Release_Date__c rd: [SELECT Id, Active__c, Release_Date_Guideline__c FROM EAW_Release_Date__c WHERE Release_Date_Guideline__c IN: newRDGRecords AND Status__c <> 'Firm']) {
                    rd.Active__c = true;
                    releaseDates.add(rd); 
                }
            }   
            
            if(releaseDates.size() > 0) {
                checkRecursiveData.bypassDateCalculation = TRUE;
                update releaseDates;
            }
        }
        catch(DmlException e) {
            for(EAW_Release_Date_Guideline__c rdg: newRDGRecords) {
                rdg.addError(e.getDmlMessage(0));   
            } 
        }             
    }*/
    
    /*
    //LRCC-1015 and 1285
    public void preventUncheckingallowManualCheckBox(Map < Id, EAW_Release_Date_Guideline__c > oldReleaseDateGuidelineMap, List < EAW_Release_Date_Guideline__c > newReleaseDateList) {

        for (EAW_Release_Date_Guideline__c releaseDateIns: newReleaseDateList) {
            
            if(checkRecursiveData.isRecursiveData(releaseDateIns.Id)) {
                EAW_Release_Date_Guideline__c oldReleaseDateIns = oldReleaseDateGuidelineMap.get(releaseDateIns.Id);                
                if (releaseDateIns.Allow_Manual_Check_Flag__c == false) {
                
                    if (oldReleaseDateIns.Allow_Manual__c == True && releaseDateIns.Allow_Manual__c == False) {
                        releaseDateIns.addError('Allow Manual can only be modified from Manage Release Date Guidelines.');
                    }
                } else if(releaseDateIns.Allow_Manual_Check_Flag__c == True) {
                    
                    releaseDateIns.Allow_Manual_Check_Flag__c = false;
                } 
            }      
        }
    }*///LRCC-1540

    //LRCC-1137
    public void reCalculateReleaseDates(List < EAW_Release_Date_Guideline__c > newRDGs, List < EAW_Release_Date_Guideline__c > oldRDGs) {
		if(!checkOuboundRecursiveData.bypassTriggerFire){
	        Set < Id > releaseDateGuidelineIds = new Set < Id > ();
	        List < EAW_Release_Date_Guideline__c > currentReleaseDateGuidelines = new List < EAW_Release_Date_Guideline__c > ();
	
	        try {
	
	            for (Integer i = 0; i < oldRDGs.size(); i++) {
	                EAW_Release_Date_Guideline__c oldRDG = oldRDGs.get(i);
	                EAW_Release_Date_Guideline__c newRDG = newRDGs.get(i);
	
	                if (newRDG.Active__c != oldRDG.Active__c && newRDG.Active__c == true) {
	                    //Need to include Release date re-calculation logics.
	                    currentReleaseDateGuidelines.add(newRDG);
	                    releaseDateGuidelineIds.add(newRDG.Id);
	                }
	            }
	
	            if (releaseDateGuidelineIds != NULL && releaseDateGuidelineIds.size() == 1 && !checkRecursiveData.bypassDateCalculation ) {
	                /*
	                EAW_RDGRuleBuilderCalculationController rdg = new EAW_RDGRuleBuilderCalculationController();
	                rdg.releaseDateGuidelineIdSet = releaseDateGuidelineIds;
	                rdg.filteredTitleIdSet = Null;
	                rdg.processedRDGWinningReleaseDateValuesBasedOnTitle = Null;
	                String jobId = System.enqueueJob(rdg);*/
	                //LRCC-1478
	                /*EAW_QueueableDateCalculationClass caclQueueJob = new EAW_QueueableDateCalculationClass();
	                caclQueueJob.releaseDateGuidelineIdSet = releaseDateGuidelineIds;
	                caclQueueJob.filteredTitleIdSet = Null;
	                caclQueueJob.windowGuidelineIdSet = Null;
	                String jobId = System.enqueueJob(caclQueueJob);
	                System.debug(':::: Job Id ::::'+ jobId);*/
	                
	                /*EAW_CreateRDForNewRDGBatch releaseDateBatch = new EAW_CreateRDForNewRDGBatch(releaseDateGuidelineIds, NULL);
	                releaseDateBatch.releaseDateGuidelineIdSet = releaseDateGuidelineIds;
	                releaseDateBatch.titleIdSet = NULL;
	                releaseDateBatch.windowGuidelineIdSet = NULL;
	                ID batchprocessid = Database.executeBatch(releaseDateBatch, 200);*/
	                
	                EAW_RDGDateCalculationBatchClass releaseDateBatch = new EAW_RDGDateCalculationBatchClass(releaseDateGuidelineIds);
	                releaseDateBatch.titleIdSet = NULL;
	                releaseDateBatch.windowGuidelineIdSet = NULL;
	                ID batchprocessid = Database.executeBatch(releaseDateBatch, 200);
	            }
	        } catch (DmlException e) {
	            for (EAW_Release_Date_Guideline__c rdg: currentReleaseDateGuidelines) {
	                rdg.addError(e.getDmlMessage(0));
	            }
	        }
    	}
    }
    public void ruleTextFieldUpdate(List<EAW_Release_Date_Guideline__c> oldReleaseDateGuideline,List<EAW_Release_Date_Guideline__c> newReleaseDateGuideline){ // LRCC - 1298
        if(!checkOuboundRecursiveData.bypassTriggerFire){
	        Set<String> conditionalOperandIdSet = new Set<String>();
	        Map<String,String> guidelineNameMap = new Map<String,String>();
	        
	        for(Integer i=0;i<oldReleaseDateGuideline.size();i++){
	            system.debug('::::::old:::::'+oldReleaseDateGuideline[i].Name);
	            system.debug('::::new::::::'+newReleaseDateGuideline[i].Name);
	            if(oldReleaseDateGuideline[i].Name != newReleaseDateGuideline[i].Name){
	                conditionalOperandIdSet.add(oldReleaseDateGuideline[i].Id);
	                guidelineNameMap.put(oldReleaseDateGuideline[i].Name,newReleaseDateGuideline[i].Name);
	            }
	        }
	        system.debug('::::guidelineNameMap::::::'+guidelineNameMap);
	        system.debug('::::'+conditionalOperandIdSet);
	        if(conditionalOperandIdSet.size() > 0){
	            EAW_RuleTextFieldUpdateHandler.guidelineRuleTextFieldUpdate(conditionalOperandIdSet,guidelineNameMap);
	        }
        }
    }
    //LRCC-1586
    public void preventUpdateOfRDG(List<EAW_Release_Date_Guideline__c> newReleaseDateGuideline, Map<Id, EAW_Release_Date_Guideline__c> oldReleaseDateGuideline) {
    	if(!checkOuboundRecursiveData.bypassTriggerFire){
	        Set<Id> newreleaseDateIdSet = new Set<Id>();
	        List<EAW_Rule_Detail__c> ruleDetailList = new List<EAW_Rule_Detail__c>();
	        Set<Id> referredConditionalSet = new Set<Id>();
	        Id widowDateRuleRecordTypeId = Schema.SObjectType.EAW_Rule__c.getRecordTypeInfosByName().get('Window Date Calculation').getRecordTypeId();
	        Id releaseDateRuleRecordTypeId = Schema.SObjectType.EAW_Rule__c.getRecordTypeInfosByName().get('Release Date Calculation').getRecordTypeId();
	        
	        if(newReleaseDateGuideline.isEmpty() == False && oldReleaseDateGuideline.isEmpty() == False) {
	        
	            for(EAW_Release_Date_Guideline__c rdgId: newReleaseDateGuideline) {
	                EAW_Release_Date_Guideline__c oldreleaseId = oldReleaseDateGuideline.get(rdgId.Id);
	                
	                if((rdgId.Territory__c != oldreleaseId.Territory__c) || (rdgId.Language__c != oldreleaseId.Language__c ) ||
	                (rdgId.EAW_Release_Date_Type__c != oldreleaseId.EAW_Release_Date_Type__c )) {
	                
	                    newreleaseDateIdSet.add(rdgId.Id);
	                }
	            }
	        }
	        EAW_DownstreamRuleGuidelinesFindUtil eawdownIns = new EAW_DownstreamRuleGuidelinesFindUtil();
	        
	        Map<Id, Set<Id>> ReleaseDateRelatedReleaseDateMap= eawdownIns.findReferencedReleaseDateGuidelineRelatedToReleaseDateGuideline(newreleaseDateIdSet);
	        
	        Map<Id, Set<Id>> ReleaseDateRelatedWindowMap = eawdownIns.findReferencedWindowGuidelineRelatedToReleaseDateGuideline(newreleaseDateIdSet);
	                
	        if(newReleaseDateGuideline.isEmpty() == False && oldReleaseDateGuideline.isEmpty() == False) {
	        
	            for(EAW_Release_Date_Guideline__c rdg: newReleaseDateGuideline) { 
	            
	                if((ReleaseDateRelatedReleaseDateMap.isEmpty() == False && ReleaseDateRelatedReleaseDateMap.get(rdg.Id) != null) 
	                || (ReleaseDateRelatedWindowMap.isEmpty() == false && ReleaseDateRelatedWindowMap.get(rdg.Id) !=  null)) {
	                
	                    rdg.addError('You cannot change Date Type, Language or Territory, since at least one other Guideline refers to it.');
	                }   
	            } 
	        }
    	} 
    }
    
    //LRCC-1559
    public void updateUnFirmDates(List<EAW_Release_Date_Guideline__c> oldRDGs,List<EAW_Release_Date_Guideline__c> newRDGs){
    	if(!checkOuboundRecursiveData.bypassTriggerFire){
	        Set<Id> modifiedRdgIds = new Set<ID>();
	        List<EAW_Release_Date__c> releaseDates = new List<EAW_Release_Date__c>();
	        for(Integer i = 0; i < oldRDGs.size(); i++) {
	            EAW_Release_Date_Guideline__c oldRDG = oldRDGs.get(i);
	            EAW_Release_Date_Guideline__c newRDG = newRDGs.get(i);
	        
	            if((oldRDG != null && newRDG != null) 
	            && (oldRDG.Territory__c != newRDG.Territory__c || oldRDG.EAW_Release_Date_Type__c != newRDG.EAW_Release_Date_Type__c 
	                    || oldRDG.Language__c != newRDG.Language__c || oldRDG.Product_Type__c != newRDG.Product_Type__c)) {
	                modifiedRdgIds.add(newRDG.id);
	            }
	            if(!modifiedRdgIds.isEmpty()){
	                List<EAW_Release_Date_Guideline__c> rdgList = [select id,Territory__c,EAW_Release_Date_Type__c,Language__c,Product_Type__c,(select id,Language__c,Status__c,EAW_Release_Date_Type__c,Territory__c,Product_Type__c from Release_Dates__r where Status__c!='Firm') from EAW_Release_Date_Guideline__c where id in:modifiedRdgIds];
	                for(EAW_Release_Date_Guideline__c rdg:rdgList){
	                    if(rdg.Release_Dates__r!=null && !rdg.Release_Dates__r.isempty()){
	                        for(EAW_Release_Date__c rd:rdg.Release_Dates__r){
	                            rd.Territory__c=rdg.Territory__c;
	                            rd.Language__c=rdg.Language__c;
	                            rd.EAW_Release_Date_Type__c=rdg.EAW_Release_Date_Type__c;
	                            rd.Product_Type__c=rdg.Product_Type__c;
	                            releaseDates.add(rd);
	                        }
	                    }
	                }
	                if(!releaseDates.isempty()){
	                    upsert releaseDates;
	                } 
	            }
	        }
    	}
    }
    public void updateRuleConditionalOperands(List<EAW_Release_Date_Guideline__c> oldRDGs, List<EAW_Release_Date_Guideline__c> newRDGs,Map<Id,EAW_Release_Date_Guideline__c> newRDGMap){
    	if(!checkOuboundRecursiveData.bypassTriggerFire){
	        Set<Id> modifiedRdgIds = new Set<ID>();
	        Map<Id,String> oldProductTypeMap = new Map<Id,String>();
	        Map<Id,String> newProductTypeMap = new Map<Id,String>();
	        Map<Id,List<String>> oldConditionalNameMap = new Map<Id,List<String>>();
	            
	        for(Integer i = 0; i < oldRDGs.size(); i++) {
	            EAW_Release_Date_Guideline__c oldRDG = oldRDGs.get(i);
	            EAW_Release_Date_Guideline__c newRDG = newRDGs.get(i);
	            
	            if(oldRDG != null && newRDG != null && oldRDG.Product_Type__c != null && newRDG.Product_Type__c != null && oldRDG.Product_Type__c != newRDG.Product_Type__c){
	                modifiedRdgIds.add(newRDG.Id);
	                oldProductTypeMap.put(oldRDG.Id,oldRDG.Product_Type__c);
	                newProductTypeMap.put(newRDG.Id,newRDG.Product_Type__c);
	            }
	        }
	        if(modifiedRdgIds.size() > 0){
	            for(EAW_Rule__c rule :  [SELECT Id,Condition_Operand_Details__c,Conditional_Operand_Id__c,Release_Date_Guideline__c,
	                                     Release_Date_Guideline__r.All_Rules__c, (SELECT Id,Condition_Operand_Details__c,Conditional_Operand_Id__c FROM Rule_Orders__r)
	                                     FROM EAW_Rule__c WHERE Release_Date_Guideline__c IN :modifiedRdgIds]){
	                List<String> conditionalNameList = new List<String>();
	                if(oldConditionalNameMap.containsKey(rule.Release_Date_Guideline__c)){
	                    conditionalNameList = oldConditionalNameMap.get(rule.Release_Date_Guideline__c);                  
	                }  
	                if(rule.Condition_Operand_Details__c != null){
	                    conditionalNameList.addAll(String.valueOf(rule.Condition_Operand_Details__c).split(';'));
	                }
	                if(rule.Rule_Orders__r != null && rule.Rule_Orders__r.size() > 0){
	                    for(EAW_Rule_Detail__c ruleDetail : rule.Rule_Orders__r){
	                        if(ruleDetail.Condition_Operand_Details__c != null){
	                            conditionalNameList.addAll(String.valueOf(ruleDetail.Condition_Operand_Details__c).split(';'));
	                        }
	                    }
	                }
	                oldConditionalNameMap.put(rule.Release_Date_Guideline__c,conditionalNameList);                       
	            }
	            if(oldConditionalNameMap != null && oldConditionalNameMap.keySet().size() > 0){
	                system.debug(':::::oldConditionalNameMap::::::'+oldConditionalNameMap);
	                Map<String,String> allRulesUpdateMap = EAW_GuidelineProductTypeChangeHandler.updateRDGRulesBasedOnProductType(oldConditionalNameMap,oldProductTypeMap,newProductTypeMap,true,null);
	                if(allRulesUpdateMap != null && allRulesUpdateMap.keySet().size() > 0){
	                    for(EAW_Release_Date_Guideline__c rdg : newRDGs){
	                        if(allRulesUpdateMap.containsKey(String.valueOf(rdg.Id))){
	                            rdg.All_Rules__c = allRulesUpdateMap.get(String.valueOf(rdg.Id));
	                        }
	                    }
	                }
	            }
	        }
    	}
    }
    
    //LRCC-1776
    public void sendData(List<EAW_Release_Date_Guideline__c> newRdgList,String operation){
    	if(!checkOuboundRecursiveData.bypassTriggerFire){
	    	Set<Id> modifiedRdgIds = new Set<ID>();
	    	for(Integer i = 0; i < newRdgList.size(); i++) {
	    		EAW_Release_Date_Guideline__c newRdg = newRdgList.get(i);
	    		if(!(operation == 'Update' && newRdg.Soft_Deleted__c=='TRUE'))
	            	modifiedRdgIds.add(newRdg.Id);            
	        }
	
	        if (!(System.isBatch() || System.isFuture() || System.isScheduled() || System.isQueueable())) {
	       		EAW_ReleaseDateGuidelines_Outbound.sendDataAsync(modifiedRdgIds,operation);
			} else {
			    EAW_ReleaseDateGuidelines_Outbound.sendDataSync(modifiedRdgIds,operation);
			}
    	}        
    }
}