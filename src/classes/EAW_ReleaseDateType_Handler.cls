public with sharing class EAW_ReleaseDateType_Handler {
        
    public void preventDelete(List<EAW_Release_Date_Type__c> triggerOld) {
        try {
            Set<String> releaseDateTypes = new Set<String> ();
            for(EAW_Release_Date_Type__c rd: triggerOld) {
                releaseDateTypes.add(rd.Name);
            }
            
            if(releaseDateTypes != null && releaseDateTypes.size() > 0) {
                List<EAW_Release_Date_Guideline__c> rdgs = new List<EAW_Release_Date_Guideline__c> (
                    [
                        SELECT Id, EAW_Release_Date_Type__c
                        FROM EAW_Release_Date_Guideline__c
                        WHERE EAW_Release_Date_Type__c != NULL AND EAW_Release_Date_Type__c IN: releaseDateTypes
                    ]
                );
                
                for(EAW_Release_Date_Type__c rd: triggerOld) {
                    if(rd.Source_Type__c == 'Data Feed' || rdgs.size() > 0)
                        rd.addError('Cannot delete. Date Type(s) are referenced in at least one release date guideline.'); // LRCC - 1626
                }
            }
        }
        catch(Exception e) {
            system.debug('EAW_ReleaseDateType_Handler.preventDelete:'+e.getMessage()+e.getStackTraceString());
        }
    }
   //LRCC-611
    public void calculateAutoFirm(List<EAW_Release_Date_Type__c> triggerNew) {
    
       Set<String> releaseDateTypesName = new Set<String> ();
        Map<String,EAW_Release_Date_Type__c> releaseDateTypeNameWithInst = new Map<String,EAW_Release_Date_Type__c>();
        List<EAW_Release_Date__c> releaseDateList = new  List<EAW_Release_Date__c>();
        Map<String,String> releaseDateIdWithName= new Map<String,String>();
        Date Lastday;
        
        String query = 'SELECT Id,Name,Release_Date__c,Status__c,EAW_Release_Date_Type__c,Title__c,Release_Date_Guideline__c FROM EAW_Release_Date__c WHERE Status__c != \'Firm\' AND (';
            
        try {
            
            for(EAW_Release_Date_Type__c rd: triggerNew) {
            
                if(rd.Auto_Firm__c == true){
                
                    Date currentDate = date.today();
                    
                    if(rd.Months_To_Auto_Firm_Period__c != NULL) {
                    
                        currentDate  = currentDate.addMonths(Integer.valueOf(rd.Months_To_Auto_Firm_Period__c));        
                    }
                    if(rd.Days_To_Auto_Firm_Period__c != NULL) {
                    
                        currentDate  = currentDate.addDays(Integer.valueOf(rd.Days_To_Auto_Firm_Period__c));
                    }
                    Lastday = currentDate.addMonths(1).toStartofMonth().addDays(-1);
                    System.debug('Lastday :::DateTime ::'+Lastday);
                     
                    if(!releaseDateTypesName.contains(rd.Name)) {
                    
                        if(releaseDateTypesName.size() == 0){
                        
                            query =query+'((EAW_Release_Date_Type__c =\''+ rd.Name +'\' AND Release_Date__c < :lastday) OR (EAW_Release_Date_Type__c =\''+ rd.Name +'\' AND Release_Date__c = :lastday))'; 
                        }else {
                        
                            query =query+' OR ((EAW_Release_Date_Type__c =\''+ rd.Name +'\' AND Release_Date__c < :lastday)OR (EAW_Release_Date_Type__c =\''+ rd.Name +'\' AND Release_Date__c = :lastday))';
                        }
                    }
                    releaseDateTypesName.add(rd.Name);
                    releaseDateTypeNameWithInst.put(rd.Name,rd);
                }
            }
            query =query+')';
            
            System.debug('query ::sche:::'+query );
           
            System.debug('releaseDateTypesName::RT:::'+releaseDateTypesName);
            if(releaseDateTypesName!= null && releaseDateTypesName.size() > 0) {
            System.debug('query ::be:::'+query );
             System.debug('Lastday ::be:::'+Lastday );
                EAW_AutoFirmBatchController eafbc= new EAW_AutoFirmBatchController (query,Lastday);
                Integer bcount = Integer.valueOf(Label.EAW_AutoFirmBatchCount);
                System.debug('::::::bcount ::auto::'+bcount);
                database.executeBatch(eafbc,bcount);
            
               /* for(EAW_Release_Date__c releaseDateInst : DataBase.query(query)){
                      
                    System.debug('releaseDateInst::RT:::'+releaseDateInst);
                    releaseDateInst.Status__c = 'Firm'; 
                    releaseDateList.add(releaseDateInst); 
                    
                    if(!releaseDateIdWithName.containsKey(releaseDateInst.Id)){
                    
                        releaseDateIdWithName.put(releaseDateInst.Id,releaseDateInst.Name);
                    }
                      ****if(releaseDateTypeNameWithInst.get(releaseDateInst.EAW_Release_Date_Type__c) != NULL){
                   
                       EAW_Release_Date_Type__c rdtInst = releaseDateTypeNameWithInst.get(releaseDateInst.EAW_Release_Date_Type__c);
                       Date currentDate = date.today();
                       
                       if(rdtInst.Months_To_Auto_Firm_Period__c != NULL){
                       
                           currentDate  = currentDate.addMonths(Integer.valueOf(rdtInst.Months_To_Auto_Firm_Period__c));        
                       }
                       if(rdtInst.Days_To_Auto_Firm_Period__c != NULL){
                       
                            currentDate  = currentDate.addDays(Integer.valueOf(rdtInst.Days_To_Auto_Firm_Period__c));
                       }
                       Date Lastday = currentDate.addMonths(1).toStartofMonth().addDays(-1); 
                       
                       System.debug('currentDate::RT:::'+currentDate);
                       System.debug('Lastday :::RT::'+Lastday);
                       System.debug('releaseDateInst.Release_Date__c::RT:::'+releaseDateInst.Release_Date__c);
                       
                      
                       //if((releaseDateInst.Release_Date__c == Lastday || releaseDateInst.Release_Date__c < Lastday)){
                       
                           System.debug('Release_Date__c:::rt::'+releaseDateInst);
                           releaseDateInst.Status__c = 'Firm'; 
                           releaseDateList.add(releaseDateInst); 
                           
                           if(!releaseDateIdWithName.containsKey(releaseDateInst.Id)){
                           
                               releaseDateIdWithName.put(releaseDateInst.Id,releaseDateInst.Name);
                           }
                       //}
                   }****
                }
                System.debug('releaseDateList:::::::'+releaseDateList);
               // releaseDateList =  new  List<EAW_Release_Date__c>();
                
                if(releaseDateList != NULL && releaseDateList.size() > 0) {
                
                    System.debug('releaseDateList::::sss:::'+releaseDateList);
                    //update releaseDateList;
                    String errorMsg = '';
                    Boolean flag = false;
                    List<Database.SaveResult> updateResults = database.update(releaseDateList,false);
                    
                    //For the failed record send to notification to user.
                   *** for (EAW_Release_Date_Type__c erdt : triggerNew) {
                    
                        for (Integer i = 0; i < releaseDateList.size(); i++) {
                        
                            EAW_Release_Date__c releaseDateInst = releaseDateList[i];
                            if(!updateResults[i].isSuccess()){
                            
                                System.debug('updateResults[i].errror()::::::'+updateResults[i]);
                                if(errorMsg == ''){
                                
                                    errorMsg  = 'Following ReleaseDates are error occurs :'+releaseDateInst.Name; 
                                }else{
                                
                                    errorMsg  = errorMsg +','+releaseDateInst.Name; 
                                } 
                                flag = true;         
                            }
                        }
                        if(flag == true) {
                        
                            erdt.addError(errorMsg+'. \n Due to some Data errors');
                        }
                    
                    }*** 
                }*/
                
            }
        }
        catch(Exception e) {
          
            system.debug('EAW_ReleaseDateType_Handler.preventDelete:'+e.getMessage()+e.getStackTraceString());
        }
    }
}