public with sharing class EAW_DateGuidelineAffectedWindowsPopup {
    
    public static Set<Id> referrencedRDGIdSet = new Set<Id>();
    
    public static Set<Id> referrencedWGIdSet = new Set<Id>();
    
    public static Set<Id> avoidDuplicateRDGIds = new Set<Id>();
    
    public static Set<Id> avoidDuplicateWGIds = new Set<Id>();
    
    @AuraEnabled
    public static outputWrapper getReleaseDatesMapFromOperandRDG( String rdgId, String titleId ) {
        
        outputWrapper ow = new outputWrapper();
        Set<Id> conditionalOperandRDGIdSet = new Set<Id>();
        Map<Id, EAW_Release_Date__c> rdgReleaseDateMap = new Map<Id, EAW_Release_Date__c>();
        
        System.debug('::::: rdgId :::::'+rdgId);
        
        System.debug('::::: titleId :::::'+titleId);
        
        Map<Id, Date> ruleDetailReleaseDateMap = new Map<Id, Date>();
        Map<Id, String> ruleDetailReleaseDateStatusMap = new Map<Id, String>();
        
        if( ! String.isBlank(rdgId) ) {
            
            List<EAW_Rule_Detail__c> ruleDetailsList = [ Select Id, Parent_Rule_Detail__c, Rule_No__c, Condition_Type__c, Condition_Timeframe__c, Condition_Operator__c, Condition_Field__c, Condition_Criteria__c, Condition_Met_Days__c, Condition_Met_Media__c, Condition_Met_Months__c, Condition_Met_Territory__c, Rule_Order__c, Nest_Order__c, Condition_Set_Operator__c, Conditional_Operand_Id__c, Condition_Date_Duration__c, Condition_Criteria_Amount__c, Day_of_the_Week__c From EAW_Rule_Detail__c Where Rule_No__r.Release_Date_Guideline__c =: rdgId AND Condition_Met_Months__c != NULL AND Condition_Met_Days__c != NULL ];
            
            System.debug('::::: ruleDetailsList :::::'+ruleDetailsList);
            
            if( ruleDetailsList != NULL && ruleDetailsList.size() > 0 ){
                
                for( EAW_Rule_Detail__c rd : ruleDetailsList ){
                    conditionalOperandRDGIdSet.add(rd.Conditional_Operand_Id__c);
                }
            }
            
            System.debug('::::: conditionalOperandRDGIdSet :::::'+conditionalOperandRDGIdSet);
            
            if( conditionalOperandRDGIdSet.size() > 0 ){
                
                List<EAW_Release_Date__c> releaseDateList = [ Select Id, Release_Date__c, Status__c, Release_Date_Guideline__c  From EAW_Release_Date__c Where Title__c =: titleId AND Release_Date_Guideline__c IN : conditionalOperandRDGIdSet AND Release_Date__c != NULL AND Active__c = TRUE ];
                
                if( releaseDateList != NULL && releaseDateList.size() > 0 ){
                    for( EAW_Release_Date__c rDate : releaseDateList ){
                        rdgReleaseDateMap.put(rDate.Release_Date_Guideline__c, rDate);
                    }
                }
            }
            
            if( ruleDetailsList != NULL && ruleDetailsList.size() > 0 ){
                
                for( EAW_Rule_Detail__c rd : ruleDetailsList ){
                    
                    if( rdgReleaseDateMap.containsKey(rd.Conditional_Operand_Id__c) ) {
                        
                        EAW_Release_Date__c rDate = rdgReleaseDateMap.get(rd.Conditional_Operand_Id__c);
                        
                        Date calcDate = rDate.Release_Date__c;
                        // Commneted below behalf of 1260 comment, which to show referrenced Release date without any calculation and its Status only.
                        /*if(rDate.Status__c != null && rDate.Status__c != 'Firm'){ // LRCC - 1861
                            if( rd.Condition_Met_Months__c != NULL ) calcDate = calcDate.addMonths(Integer.valueOf(rd.Condition_Met_Months__c));
                            if( rd.Condition_Met_Days__c != NULL ) calcDate = calcDate.addDays(Integer.valueOf(rd.Condition_Met_Days__c));
                            
                            //LRCC-1360
                            if( rd.Condition_Date_Duration__c == 'Day' || rd.Condition_Date_Duration__c == 'Week' || rd.Condition_Date_Duration__c == 'Month' ) {
                                EAW_DayMonthWeekDateCalculations calcController = new EAW_DayMonthWeekDateCalculations();
                                calcDate = calcController.getCalculatedDate( calcDate, rd );
                            }
                        }*/
                        if( calcDate != NULL ) {
                            ruleDetailReleaseDateMap.put(rd.Id, calcDate);
                            ruleDetailReleaseDateStatusMap.put(rd.Id, rDate.Status__c);
                        }
                    }
                }
            }
            
        }
        
        System.debug('::::: ruleDetailReleaseDateMap :::::'+ruleDetailReleaseDateMap);
        System.debug('::::: ruleDetailReleaseDateStatusMap :::::'+ruleDetailReleaseDateStatusMap);
        
        ow.ruleDetailReleaseDateMap = ruleDetailReleaseDateMap;
        ow.ruleDetailReleaseDateStatusMap = ruleDetailReleaseDateStatusMap;
        
        return ow;
    }
    
    
    @AuraEnabled
    public static affectedWindowsResultWrapper getAffectedWindowRecords( String rdgId, String titleId ) {
        
        System.debug('::::::: getAffectedWindows :::::::::'+rdgId+':::TitleId:::'+titleId);
        
        affectedWindowsResultWrapper affectedResulWrapper = new affectedWindowsResultWrapper();
         
        List<EAW_Window__c> windowList = new List<EAW_Window__c>();
        
        Map<String, Set<Id>> rdgIdReferencedMap = new Map<String, Set<Id>>();
        
        Map<String, Set<Id>> wgIdReferencedMap = new Map<String, Set<Id>>();
        
        if( rdgId != NULL ) {
            
            List<EAW_Rule__c> ruleList = [ Select Id, Conditional_Operand_Id__c, Release_Date_Guideline__c, ( Select Id, Conditional_Operand_Id__c From Rule_Orders__r Where Conditional_Operand_Id__c != NULL )  From EAW_Rule__c Where Release_Date_Guideline__c != : rdgId AND Release_Date_Guideline__r.Active__c = TRUE ];
            
            if( ruleList != NULL ){
                
                for( EAW_Rule__c rule : ruleList ){
                    
                    Set<String> tempIdSet = new Set<String>();
                        
                    if( ! String.isBlank(rule.Conditional_Operand_Id__c) ) {
                        tempIdSet.addAll(rule.Conditional_Operand_Id__c.split(';'));
                    }
                    
                    if( rule.Rule_Orders__r != NULL && rule.Rule_Orders__r.size() > 0 ){
                        
                        List<EAW_Rule_Detail__c> rdList = rule.Rule_Orders__r;
                        
                        for( EAW_Rule_Detail__c rd : rdList ){
                            
                            if( ! String.isBlank(rd.Conditional_Operand_Id__c) ) {
                                tempIdSet.addAll(rd.Conditional_Operand_Id__c.split(';'));
                            }
                        }
                        
                    }
                    
                    if( tempIdSet.size() > 0 ){
                        
                        for( String idStr : tempIdSet ){
                            Set<Id> tempRdgIdSet = new Set<Id>();
                            if( rdgIdReferencedMap.containsKey(idStr) ){
                                tempRdgIdSet = rdgIdReferencedMap.get(idStr);
                            }
                            tempRdgIdSet.add(rule.Release_Date_Guideline__c);
                            rdgIdReferencedMap.put(idStr, tempRdgIdSet);
                        }
                    }
                }
            }
            collectRDGIds( new Set<Id>{rdgId}, rdgIdReferencedMap );
            
            referrencedRDGIdSet.add(rdgId);
            
            System.debug(':::: referrencedRDGIdSet ::::'+referrencedRDGIdSet);
            
            List<EAW_Rule__c> wgRuleList = [ Select Id, Conditional_Operand_Id__c, Window_Guideline__c, ( Select Id, Conditional_Operand_Id__c From Rule_Orders__r Where Conditional_Operand_Id__c != NULL )  From EAW_Rule__c Where Window_Guideline__c != NULL AND Window_Guideline__r.Status__c = 'Active' AND Date_To_Modify__c = 'Start Date'];
            
            if( wgRuleList != NULL ){
                
                for( EAW_Rule__c rule : wgRuleList ){
                    
                    Set<String> tempIdSet = new Set<String>();
                        
                    if( ! String.isBlank(rule.Conditional_Operand_Id__c) ) {
                        tempIdSet.addAll(rule.Conditional_Operand_Id__c.split(';'));
                    }
                    
                    if( rule.Rule_Orders__r != NULL && rule.Rule_Orders__r.size() > 0 ){
                        
                        List<EAW_Rule_Detail__c> rdList = rule.Rule_Orders__r;
                        
                        for( EAW_Rule_Detail__c rd : rdList ){
                            
                            if( ! String.isBlank(rd.Conditional_Operand_Id__c) ) {
                                tempIdSet.addAll(rd.Conditional_Operand_Id__c.split(';'));
                            }
                        }
                        
                    }
                    
                    if( tempIdSet.size() > 0 ){
                        
                        for( String idStr : tempIdSet ){
                            Set<Id> tempWGIdSet = new Set<Id>();
                            if( wgIdReferencedMap.containsKey(idStr) ){
                                tempWGIdSet = wgIdReferencedMap.get(idStr);
                            }
                            tempWGIdSet.add(rule.Window_Guideline__c);
                            wgIdReferencedMap.put(idStr, tempWGIdSet);
                        }
                    }
                }
            }
                
            collectWGIds(referrencedRDGIdSet, wgIdReferencedMap);
            
            System.debug(':::: referrencedWGIdSet ::::'+referrencedWGIdSet);
            
            if( referrencedWGIdSet != NULL && referrencedWGIdSet.size() > 0 ){
                windowList = [ Select Id, EAW_Window_Guideline__r.Window_Guideline_Alias__c, Start_Date__c,Start_Date_Text__c, End_Date__c,End_Date_Text__c, Status__c, Outside_Date__c, Tracking_Date__c, Right_Status__c, License_Info_Codes__c From EAW_Window__c  Where EAW_Window_Guideline__c IN : referrencedWGIdSet AND EAW_Title_Attribute__c =: titleId ];
            }
            
            System.debug(':::: windowList ::::'+windowList);
        }
        
        if( titleId != NULL ){
            List<EAW_Title__c> titleList = [ Select Id, Name, FIN_PROD_ID__c From EAW_Title__c Where Id =: titleId ];
            if( titleList != NULL && titleList.size() > 0 ){
                affectedResulWrapper.title = titleList[0];
            }
        }
        
        affectedResulWrapper.affectedWindows = windowList;
        
        Map<Id, String> windowTagMap = new Map<Id, String>();
            
        if( windowList != NULL && windowList.size() > 0 ){
            
            List<EAW_Window_Tag_Junction__c> windowTagList = [ Select Id, Window__c, Tag__c, Tag__r.Name From EAW_Window_Tag_Junction__c Where Window__c IN : windowList];
            
            if( windowTagList != NULL && windowTagList.size() > 0 ){
                
                for( EAW_Window_Tag_Junction__c wTag : windowTagList ){
                    
                    String tagVal = '';
                    
                    if( windowTagMap.containsKey(wTag.Window__c) ){
                        tagVal = windowTagMap.get(wTag.Window__c);
                    }
                    
                    if( String.isBlank(tagVal) ){
                        tagVal += wTag.Tag__r.Name;
                    } else {
                        tagVal += ', '+wTag.Tag__r.Name;
                    }
                    windowTagMap.put(wTag.Window__c, tagVal);
                }
                
            }
        }
        affectedResulWrapper.windowTagMap = windowTagMap;
        
        return affectedResulWrapper;
    }
    
    
    public static void collectRDGIds ( Set<Id> parentRDGIds, Map<String, Set<Id>> rdgIdReferencedMap ){
        
        Set<Id> downstreamRdgIds = new Set<Id>();
        
        if( parentRDGIds != NULL && parentRDGIds.size() > 0 ){
            for( String rdgId : parentRDGIds ){
                if( rdgIdReferencedMap.containsKey(rdgId) && ! avoidDuplicateRDGIds.contains(rdgId) ) {
                    downstreamRdgIds.addAll(rdgIdReferencedMap.get(rdgId));
                    avoidDuplicateRDGIds.add(rdgId);
                }
            }
        }
        
        if( downstreamRdgIds != NULL && downstreamRdgIds.size() > 0 ){
            referrencedRDGIdSet.addAll(downstreamRdgIds);
            collectRDGIds(downstreamRdgIds, rdgIdReferencedMap);
        }
    }
    
    public static void collectWGIds ( Set<Id> parentIds, Map<String, Set<Id>> wgIdReferencedMap ){
        
        System.debug('::::parentIds:::::'+parentIds);
        
        Set<Id> downstreamWGIds = new Set<Id>();
        
        if( parentIds != NULL && parentIds.size() > 0 ){
            for( String gId : parentIds ){
                if( wgIdReferencedMap.containsKey(gId) && !avoidDuplicateWGIds.contains(gId)  ) {
                    downstreamWGIds.addAll(wgIdReferencedMap.get(gId));
                    avoidDuplicateWGIds.add(gId);
                }
            }
        }
        
        if( downstreamWGIds != NULL && downstreamWGIds.size() > 0 ){
            referrencedWGIdSet.addAll(downstreamWGIds);
            collectWGIds(downstreamWGIds, wgIdReferencedMap);
        }
    }
    
    
    public class outputWrapper {
        @AuraEnabled
        public Map<Id, Date> ruleDetailReleaseDateMap;
        @AuraEnabled
        public Map<Id, String> ruleDetailReleaseDateStatusMap;
    
    }
    
    public class affectedWindowsResultWrapper {
        @AuraEnabled
        public List<EAW_Window__c> affectedWindows; 
        @AuraEnabled
        public EAW_Title__c title;
        @AuraEnabled
        public Map<Id, String> windowTagMap;
    }
    
}