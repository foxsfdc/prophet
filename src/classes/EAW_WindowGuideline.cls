public with sharing class EAW_WindowGuideline {
    
    @AuraEnabled
    public static list<list<String>> collectPicklistsValues() {
        return EAW_WindowStrand.collectWindowStrandPicklists();
    }
    
    @AuraEnabled
    public static Map<String, SObject> insertWindowGuideline(EAW_Window_Guideline__c windowGuideline, String planRecordId) {
        insert windowGuideline;
        
        EAW_Plan_Window_Guideline_Junction__c planWindowGuideline = new EAW_Plan_Window_Guideline_Junction__c();
        planWindowGuideline.Plan__c = planRecordId;
        planWindowGuideline.Window_Guideline__c = windowGuideline.Id;
        insert planWindowGuideline;
        
        Map<String, SObject> planWindowGuidelineJunction = new Map<String, SObject>();
        planWindowGuidelineJunction.put('Plan_Window_Guideline_Junction__r', planWindowGuideline);
        planWindowGuidelineJunction.put('Window_Guideline_r', windowGuideline);
        
        return planWindowGuidelineJunction;
    }
}