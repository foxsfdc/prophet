public class EAW_QueueableDateCalculationClass implements Queueable {

    public Set<Id> releaseDateGuidelineIdSet = new Set<Id>(); // This set of Release date guideline records Id get passed from where this Queuable method get invoked
    public Set<Id> windowGuidelineIdSet = new Set<Id>(); // This set of window guideline records Id get passed from where this Queuable method get invoked
    public Set<Id> filteredTitleIdSet = new Set<Id>(); // This set of Title records Id get passed from where this Queuable method get invoked
    
    
    public void execute(QueueableContext context) {
    
        Set<Id> rdgIdSetToProcessInNextJob = new Set<Id>();
        Set<Id> wgIdSetToProcessInNextJob = new Set<Id>();
        
        if( releaseDateGuidelineIdSet != NULL && releaseDateGuidelineIdSet.size() > 0 ) {
    
            Map<String, Set<Id>> guidelineIdMapToProcess = new Map<String, Set<Id>>(); //This will have both Release date guideline and Window guideline Id's to Process
            
            EAW_NRDGRuleBuilderCalculationController rdgCalcHandler = new EAW_NRDGRuleBuilderCalculationController();
            
            guidelineIdMapToProcess = rdgCalcHandler.executeRDGRuleCalculations(releaseDateGuidelineIdSet, filteredTitleIdSet);
            
            if( guidelineIdMapToProcess != NULL && guidelineIdMapToProcess.size() > 0 ) {
                
                if( guidelineIdMapToProcess.containsKey('rdgidset') && guidelineIdMapToProcess.get('rdgidset') != NULL ) {
                    rdgIdSetToProcessInNextJob = guidelineIdMapToProcess.get('rdgidset');
                }
                
                if( guidelineIdMapToProcess.containsKey('wgidset') && guidelineIdMapToProcess.get('wgidset') != NULL ) {
                    if( windowGuidelineIdSet == NULL ) windowGuidelineIdSet = new Set<Id>();
                    windowGuidelineIdSet.addAll(guidelineIdMapToProcess.get('wgidset'));
                }
            }
        }
            
        system.debug('::windowGuidelineIdSet::'+windowGuidelineIdSet);
        system.debug('::filteredTitleIdSet::'+filteredTitleIdSet);
        
        if( windowGuidelineIdSet != NULL && windowGuidelineIdSet.size() > 0 ) {
            
            EAW_NWGRuleBuilderCalculationController wgCalcHanlder = new EAW_NWGRuleBuilderCalculationController();
            
            wgIdSetToProcessInNextJob = wgCalcHanlder.executeWGRuleCalculations(windowGuidelineIdSet, filteredTitleIdSet);
            
            System.debug('::::::::::: windowGuidelineIdSet ::::::::::'+windowGuidelineIdSet);
            System.debug('::::::::::: wgIdSetToProcessInNextJob ::::::::::'+wgIdSetToProcessInNextJob);
            
            wgIdSetToProcessInNextJob.removeAll(windowGuidelineIdSet);
            System.debug('::::::::::: wgIdSetToProcessInNextJob-2 ::::::::::'+wgIdSetToProcessInNextJob);
        }
        
        if( ( rdgIdSetToProcessInNextJob != NULL && rdgIdSetToProcessInNextJob.size() > 0 ) || (wgIdSetToProcessInNextJob != NULL && wgIdSetToProcessInNextJob.size() > 0 ) ){ 
        
            EAW_QueueableDateCalculationClass nextCalcQueueJob = new EAW_QueueableDateCalculationClass();
            nextCalcQueueJob.releaseDateGuidelineIdSet = rdgIdSetToProcessInNextJob;
            nextCalcQueueJob.windowGuidelineIdSet = wgIdSetToProcessInNextJob;
            if( filteredTitleIdSet != NULL && filteredTitleIdSet.size() > 0 ) {
                nextCalcQueueJob.filteredTitleIdSet = filteredTitleIdSet;
            }
            String jobId = System.enqueueJob(nextCalcQueueJob);
            System.debug(':::: JobID ::::'+jobId);
        }
    }
}