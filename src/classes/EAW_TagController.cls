public with sharing class EAW_TagController {
	
    @AuraEnabled
    public static List<EAW_Tag__c> getTagInfo(String tagIds) {
        
        String idArr = '(';
        for(String s : tagIds.split(', ')) {
            idArr += '\'' + s + '\',';
        }
        idArr = idArr.substring(0, idArr.length()-1) + ')';
        String query = 'select Name, Tag_Type__c, Active__c from EAW_Tag__c where Id in ' + idArr.replace('[','').replace(']','');
        System.debug(query);
        
        List<EAW_Tag__c> tags = Database.query(query);
        return tags;
    }
}