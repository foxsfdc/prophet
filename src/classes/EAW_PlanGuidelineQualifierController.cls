public class EAW_PlanGuidelineQualifierController {
    //LRCC-723 Content Owner Integration
    public static Set<Id> wgIds = new Set<Id>(); 
    
    public static void createPlanAndWindowsForQualifiedTitles(Map<Id, Set<Id>> titleAndPlanGuidelineIdMap, Set<Id> planGuidelineIdSet){
        
        //LRCC-1393.
        //Get custom settings - ON/OFF switches for notification.
        EAW_Notification_Status__c ns = EAW_Notification_Status__c.getOrgDefaults();
        //Get chatter group Id.
        List<CollaborationGroup> lstChatterGroup = [SELECT Id, Name FROM CollaborationGroup WHERE Name = 'Title Attribute Review']; 
        Map<Id, String> titleIdName = new Map<Id, String> ();
        String bodyContent;
        
        Set<Id> wgSetForQueqeable = new Set<Id>();
                       
        List<FeedItem> feedItems = new List<FeedItem>();
        
        
        if(planGuidelineIdSet != NULL && planGuidelineIdSet.size() > 0) {
        
            //system.debug(':::planGuidelineIdSet :::'+planGuidelineIdSet );
            
            Map<Id,EAW_Plan_Guideline__c> planGuidelineMap = new Map<Id,EAW_Plan_Guideline__c>([SELECT Name, Territory__c, Language__c, Product_Type__c, Release_Type__c, Sales_Region__c, Previous_Version__c
                                            FROM EAW_Plan_Guideline__c WHERE id IN :planGuidelineIdSet]);
            
            List<EAW_Plan__c> plans = [SELECT Id, Name, Plan_Guideline__c, Territory__c, Language__c, Product_Type__c, Release_Type__c, Sales_Region__c, EAW_Title__r.Title_EDM__c,EAW_Title__r.PROD_TYP_CD_INTL__r.Name
                                       FROM EAW_Plan__c WHERE EAW_Title__c IN :titleAndPlanGuidelineIdMap.keySet() AND Plan_Guideline__c IN : planGuidelineIdSet]; // Add Plan Guideline
            
            List<EAW_Title__c> titleList = [SELECT Id, Name, Name__c, PROD_TYP_CD_INTL__r.Name FROM EAW_Title__c WHERE Id IN :titleAndPlanGuidelineIdMap.keySet() ]; 
              
            Map<Id,String> titleProductTypeMap = new Map<Id,String>();
            Set<String> validProductTypeSet = new Set<String>();
            Map<Id,Map<Id, EAW_Plan__c>> planGuidelineTitlePlanMap = new Map<id,Map<Id, EAW_Plan__c>>();
            Map<Id,List<EAW_Plan__c>> insertedTitlePlanMap = new Map<id,List<EAW_Plan__c>>();
            Map<Id,List<EAW_Plan_Window_Guideline_Junction__c>> windowGuidelineJunctionMap = new Map<Id,List<EAW_Plan_Window_Guideline_Junction__c>>();
            List<EAW_Plan__c> planToInsert = new List<EAW_Plan__c>();
            List<EAW_Window__c> windowToInsert = new List<EAW_Window__c>();
            
            //LRCC-1335
            Map<Id, Map<Id, EAW_Plan__c>> previousVersionPGTitlePlanMap = new Map<Id, Map<Id, EAW_Plan__c>> ();
            Set<Id> previousVersionPGIds = new Set<Id> ();
            
            for(EAW_Plan_Guideline__c planGuideline: planGuidelineMap.values()) {
                if(planGuideline.Previous_Version__c != null) {
                    previousVersionPGIds.add(planGuideline.Previous_Version__c);
                }
            }
            
            List<EAW_Plan__c> previesVersionPGPlans = [
                SELECT Id, Name, Plan_Guideline__c, EAW_Title__c FROM EAW_Plan__c WHERE Plan_Guideline__c IN : previousVersionPGIds AND EAW_Title__c IN :titleAndPlanGuidelineIdMap.keySet() 
            ];
            
            if(previesVersionPGPlans.size() > 0) {
                
                Map<Id, EAW_Plan__c> titlePlanMap = new Map<Id, EAW_Plan__c> ();
                EAW_Plan__c newPlan = new EAW_Plan__c ();
                    
                for(EAW_Plan__c plan: previesVersionPGPlans) {
                    
                    if(previousVersionPGTitlePlanMap.containsKey(plan.Plan_Guideline__c)) {
                        titlePlanMap = previousVersionPGTitlePlanMap.get(plan.Plan_Guideline__c);
                    }
                    titlePlanMap.put(plan.EAW_Title__c, plan);
                    previousVersionPGTitlePlanMap.put(plan.Plan_Guideline__c, titlePlanMap);
                }
            }
            //finished LRCC-1335.
            
            if(titleList != null && titleList.size() > 0){
                for(EAW_Title__c title : titleList){
                    validProductTypeSet.add(title.PROD_TYP_CD_INTL__r.Name);
                    titleProductTypeMap.put(title.Id,title.PROD_TYP_CD_INTL__r.Name);
                    titleIdName.put(title.Id, title.Name__c);
                }
            }
            
            if(plans.size() > 0) {
                
                Map<Id, EAW_Plan__c> titlePlanMap = new Map<id, EAW_Plan__c>();
                    
                for(EAW_Plan__c plan : plans) {
                    
                    if( planGuidelineTitlePlanMap.containsKey(plan.Plan_Guideline__c) ){
                        titlePlanMap = planGuidelineTitlePlanMap.get(plan.Plan_Guideline__c);
                    } else {
                        titlePlanMap = new Map<id, EAW_Plan__c>();
                    }
                    titlePlanMap.put(plan.EAW_Title__c, plan);
                    planGuidelineTitlePlanMap.put(plan.Plan_Guideline__c, titlePlanMap);
                }
            }
            
            //LRCC-1560 - Replace Customer lookup field with Customer text field
            //1515-Auto create at Window guideline level
            List<EAW_Plan_Window_Guideline_Junction__c> windowGuidelineJunctionList = [SELECT Window_Guideline__c, Window_Guideline__r.Name, Window_Guideline__r.Window_Type__c, Window_Guideline__r.Product_Type__c, Window_Guideline__r.Status__c, Window_Guideline__r.Start_Date__c, Window_Guideline__r.End_Date__c,
                            Window_Guideline__r.AutoCreate__c, Window_Guideline__r.Tracking_Date__c, Window_Guideline__r.Outside_Date__c, Window_Guideline__r.Media__c, Window_Guideline__r.Description__c, Window_Guideline__r.Customers__c, Plan__c FROM EAW_Plan_Window_Guideline_Junction__c WHERE Plan__c IN :planGuidelineIdSet AND Window_Guideline__r.Product_Type__c IN :validProductTypeSet AND Window_Guideline__r.AutoCreate__c = TRUE ];
            if(windowGuidelineJunctionList.size() > 0){
                for(EAW_Plan_Window_Guideline_Junction__c junction : windowGuidelineJunctionList){
                    List<EAW_Plan_Window_Guideline_Junction__c> junctionList = new List<EAW_Plan_Window_Guideline_Junction__c>();
                    if(windowGuidelineJunctionMap.get(junction.Plan__c) != NULL && windowGuidelineJunctionMap.get(junction.Plan__c).size() > 0){
                        junctionList = windowGuidelineJunctionMap.get(junction.Plan__c);
                    }
                    junctionList.add(junction);
                    windowGuidelineJunctionMap.put(junction.Plan__c,junctionList);
                }
            }
            
            for(Id titleId : titleAndPlanGuidelineIdMap.keySet()) {
                
                Boolean isDuplicate = false;
                
                for(Id planGuidelineId : titleAndPlanGuidelineIdMap.get(titleId)) {
                    
                    isDuplicate = false; // LRCC - 723
                    
                   // system.debug('::planGuidelineId ::'+planGuidelineId);
                    
                    String previousVersionPGId = planGuidelineMap.get(planGuidelineId).Previous_Version__c;
                    if(String.isNotBlank(previousVersionPGId) && previousVersionPGTitlePlanMap.containsKey(previousVersionPGId) && previousVersionPGTitlePlanMap.get(previousVersionPGId) != null && previousVersionPGTitlePlanMap.get(previousVersionPGId).get(titleId) != null) {
                        //system.debug('::existing Plan::'+planGuidelineId);
                        EAW_Plan__c newPlan = previousVersionPGTitlePlanMap.get(previousVersionPGId).get(titleId);
                        newPlan.Plan_Guideline__c = planGuidelineId;
                        newPlan.isCustom__c = true; //LRCC-1390
                        planToInsert.add(newPlan);
                        Map<Id, EAW_Plan__c> tempMap = previousVersionPGTitlePlanMap.get(previousVersionPGId);
                        tempMap.remove(titleId);
                        previousVersionPGTitlePlanMap.put(previousVersionPGId, tempMap);
                    } 
                    else {
                        system.debug('::new Plan::'+planGuidelineId);
                        List<EAW_Plan__c> planList;
                        if(planGuidelineTitlePlanMap.containsKey(planGuidelineId)) {
                            if(planGuidelineTitlePlanMap.get(planGuidelineId).containsKey(titleId)) {
                                //planList = planGuidelineTitlePlanMap.get(planGuidelineId).get(titleId);
                                isDuplicate = TRUE;
                                //system.debug('::duplicate Plan::'+planGuidelineTitlePlanMap.get(planGuidelineId).get(titleId));
                                planToInsert.add(planGuidelineTitlePlanMap.get(planGuidelineId).get(titleId));
                            }
                        }
                        //isDuplicate = checkPlanForDuplicates(planList, planGuidelineMap.get(planGuidelineId));
                        system.debug(':isDuplicate:::'+isDuplicate);
                        if(isDuplicate == false) {
                            
                            //system.debug('::new Plan:::::::::'+planGuidelineMap.get(planGuidelineId));
                            
                            EAW_Plan_Guideline__c qualifiedPlanGuideline = planGuidelineMap.get(planGuidelineId);
                            EAW_Plan__c newPlan = new EAW_Plan__c();
                            newPlan.Name = qualifiedPlanGuideline.Name;
                            newPlan.Territory__c = qualifiedPlanGuideline.Territory__c;
                            newPlan.Language__c = qualifiedPlanGuideline.Language__c;
                            newPlan.Product_Type__c = qualifiedPlanGuideline.Product_Type__c;
                            newPlan.Release_Type__c = qualifiedPlanGuideline.Release_Type__c;
                            newPlan.Sales_Region__c = qualifiedPlanGuideline.Sales_Region__c;
                            newPlan.Plan_Guideline__c = qualifiedPlanGuideline.Id;
                            newPlan.EAW_Title__c = titleId;
                            newPlan.isCustom__c = true; //LRCC-1390
                            
                            planToInsert.add(newPlan);
                        }
                    }
                }
            }    
                
            if(planGuidelineIdSet != null && planGuidelineIdSet.size() > 0) {
            
                for(Id planGuidelineId: planGuidelineIdSet) {
                    
                    String previousVersionPGId = planGuidelineMap.get(planGuidelineId).Previous_Version__c;
                    
                    //system.debug('::previousVersionPGId::'+previousVersionPGId );
                    if(String.isNotBlank(previousVersionPGId) && previousVersionPGTitlePlanMap.containsKey(previousVersionPGId)) {
                        
                        Map<Id, EAW_Plan__c> titlePlanMap = previousVersionPGTitlePlanMap.get(previousVersionPGId);
                        
                        if(titlePlanMap != null && titlePlanMap.values() != null) {
                            
                            for(EAW_Plan__c plan: titlePlanMap.values()) {
                                plan.Plan_Guideline__c = planGuidelineId;
                                planToInsert.add(plan);
                            }
                        }
                    }
                }
            }
            if(planToInsert.size() > 0) {
                
                List<Database.UpsertResult> savePlanResult = Database.upsert(planToInsert, false); 
                //upsert planToInsert;
                
                for(EAW_Plan__c plan : planToInsert) {
                    
                    if(plan.Id != NULL) {
                    
                        List<EAW_Plan__c> planList = new List<EAW_Plan__c>();
                        if(insertedTitlePlanMap.get(plan.EAW_Title__c) != NULL && insertedTitlePlanMap.get(plan.EAW_Title__c).size() > 0){
                            planList = insertedTitlePlanMap.get(plan.EAW_Title__c);
                        }
                        planList.add(plan);
                        insertedTitlePlanMap.put(plan.EAW_Title__c,planList);
                        
                        //LRCC-1393 - Notification.
                        if(!lstChatterGroup.isEmpty() && ns.TACategoryNotification__c == true && ns.TATitlePlanQualifierNotification__c == true
                            && titleIdName.containsKey(plan.EAW_Title__c) && planGuidelineMap.containsKey(plan.Plan_Guideline__c)) {
                            
                            if(String.isBlank(bodyContent)) {
                                bodyContent = titleIdName.get(plan.EAW_Title__c) +' - qualifies for '+planGuidelineMap.get(plan.Plan_Guideline__c).Name+'.'+'\n'+System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+plan.Id;
                            }
                            else {
                                bodyContent += '\n\n'+ titleIdName.get(plan.EAW_Title__c) +' - qualifies for '+planGuidelineMap.get(plan.Plan_Guideline__c).Name+'.'+'\n'+System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+plan.Id;
                            }
                        }
                    }
                }
                
                //LRCC-1393 - Notification.
                if(!lstChatterGroup.isEmpty() && String.isNotBlank(bodyContent)) {
                
                    String temp = bodyContent;
                    
                    if(bodyContent.length() < 10000) {
                        bodyContent = temp.substring(0, temp.length());
                    } 
                    else {
                        bodyContent = temp.substring(0,9988);    
                    }
                                        
                    feedItems.add(
                        new FeedItem(
                            Body = bodyContent,
                            ParentId = lstChatterGroup[0].Id
                        )
                    );  
                            
                    if(feedItems != null && feedItems.size() > 0) {
                        insert feedItems;
                    }
                }
                
                Map<Id,Map<Id,EAW_Window__c>> junctionWindows = getDuplicateWindowMap(windowGuidelineJunctionMap,insertedTitlePlanMap);
                Set<Id> titleIdSet = new Set<Id>();
                Set<Id> conditionalOperandSet = new Set<Id>();
                Set<Id> recursiveWgSet = new Set<Id>();
                
                for(Id titleId : insertedTitlePlanMap.keySet()) {
                    for(EAW_Plan__c plan : insertedTitlePlanMap.get(titleId)) {
                        recursiveWgSet = new Set<Id>(); // To Avoid creation of Duplicate window
                        if(windowGuidelineJunctionMap.containsKey(plan.Plan_Guideline__c) && windowGuidelineJunctionMap.get(plan.Plan_Guideline__c) != NULL) {
                            for(EAW_Plan_Window_Guideline_Junction__c junction : windowGuidelineJunctionMap.get(plan.Plan_Guideline__c)){
                                if(titleProductTypeMap.containsKey(titleId) && (titleProductTypeMap.get(titleId)).toUpperCase() == (junction.Window_Guideline__r.Product_Type__c).toUpperCase() && !recursiveWgSet.contains(junction.Window_Guideline__r.Id)) { // LRCC - 1286
                                    windowToInsert.add(createWindow(plan.Id , junction , plan.EAW_Title__c, junctionWindows,wgSetForQueqeable,conditionalOperandSet));
                                    recursiveWgSet.add(junction.Window_Guideline__r.Id);
                                    titleIdSet.add(titleId);
                                }
                            }
                        }
                    }
                }
               
                Set<Id> newWindowIds = new Set<Id>();
                
                if(windowToInsert.size() > 0){
                    
                    List<Database.UpsertResult> saveResult = Database.upsert(windowToInsert,false); // To allow partial success of insert when the duplicate error occur
                    
                    //LRCC-723 Content Owner Integration
                    if(wgSetForQueqeable!=null && !wgSetForQueqeable.isEmpty()){
                        wgIds.addAll(wgSetForQueqeable);
                    }
                    
                    if( !checkRecursiveData.bypassDateCalculation && wgSetForQueqeable.size() > 0){ 
                        //LRCC-1478
                        EAW_WGDateCalculationBatchClass windowDateBatch = new EAW_WGDateCalculationBatchClass(wgSetForQueqeable);
                        windowDateBatch.releaseDateGuidelineIdSet = NULL;
                        windowDateBatch.titleIdSet = titleIdSet;
                        //windowDateBatch.windowGuidelineIdSet = wgSetForQueqeable;
                        ID batchprocessid = Database.executeBatch(windowDateBatch, 200);
                    }
                    //insert windowToInsert;
                    //LRCC-1009 - for commented line
                    
                    /**LRCC:631**/
                    /*
                    As part of Window API batch class Rights check is handled. commenting below code.
                    */
                    /*
                    if(newWindowIds.size() > 0) {
                        EAW_RightsCheckCalloutController.sentWindowstoGalilio(newWindowIds,'insert');
                    }
                    */
                }
            }
        }
    }
    
    private static EAW_Window__c createWindow(Id planId, EAW_Plan_Window_Guideline_Junction__c windowJunction,Id titleId,  Map<Id,Map<Id,EAW_Window__c>> junctionWindows,Set<Id> wgSetForQueqeable,Set<Id> conditionalOperandSet) {
        
        EAW_Window__c newWindow = new EAW_Window__c();
        if(junctionWindows.containsKey(windowJunction.Window_Guideline__r.Id) 
            && junctionWindows.get(windowJunction.Window_Guideline__r.Id).containsKey(titleId) 
            && junctionWindows.get(windowJunction.Window_Guideline__r.Id).get(titleId) != null
            ) {
            newWindow = junctionWindows.get(windowJunction.Window_Guideline__r.Id).get(titleId);
            if( newWindow.Id != NULL && newWindow.EAW_Plan__c == planId ) {
                return newWindow;
            }
        }
        newWindow.Name = windowJunction.Window_Guideline__r.Name;
        newWindow.Window_Type__c = windowJunction.Window_Guideline__r.Window_Type__c;
        newWindow.Status__c = 'Estimated';
        newWindow.Media__c = windowJunction.Window_Guideline__r.Media__c;
        newWindow.Customers__c = windowJunction.Window_Guideline__r.Customers__c;
        newWindow.EAW_Window_Guideline__c = windowJunction.Window_Guideline__r.Id;
        newWindow.EAW_Plan__c = planId;
        newWindow.EAW_Title_Attribute__c  = titleId;
        if( windowJunction.Window_Guideline__r.Id != null && windowJunction.Window_Guideline__r.AutoCreate__c == TRUE && checkRecursiveData.isRecursiveData(windowJunction.Window_Guideline__r.Id) ) wgSetForQueqeable.add(windowJunction.Window_Guideline__r.Id); // LRCC - 1240
        
        return newWindow;
    }
    
    private static Boolean checkPlanForDuplicates(List<EAW_Plan__c> titleplans, EAW_Plan_Guideline__c planGuideline) {
        
        Boolean isDuplicate = false;
        if( titleplans != NULL && titleplans.size() > 0 ){
            for(EAW_Plan__c plan : titlePlans){
                if (plan.Name == planGuideline.Name &&
                    plan.Territory__c == planGuideline.Territory__c &&
                    plan.Language__c == planGuideline.Language__c &&
                    plan.Product_Type__c == planGuideline.Product_Type__c &&
                    plan.Release_Type__c == planGuideline.Release_Type__c &&
                    plan.Sales_Region__c == planGuideline.Sales_Region__c) 
                {
                    isDuplicate = true;
                    break;
                }
            }
        }
        return isDuplicate;
    }
    private static Map<Id,Map<Id,EAW_Window__c>> getDuplicateWindowMap(Map<Id,List<EAW_Plan_Window_Guideline_Junction__c>> windowGuidelineJunctionMap, Map<Id,List<EAW_Plan__c>> insertedTitlePlanMap){
        Set<Id> WGSet = new Set<Id>();
        Map<Id,Map<Id,EAW_Window__c>> junctionWindows = new Map<Id,Map<Id,EAW_Window__c>>();
        for(Id titleId : insertedTitlePlanMap.keySet()) {
            for(EAW_Plan__c plan : insertedTitlePlanMap.get(titleId)) {
                if(windowGuidelineJunctionMap.containsKey(plan.Plan_Guideline__c) && windowGuidelineJunctionMap.get(plan.Plan_Guideline__c) != NULL) {
                    for(EAW_Plan_Window_Guideline_Junction__c junction : windowGuidelineJunctionMap.get(plan.Plan_Guideline__c)){
                        WGSet.add(junction.Window_Guideline__r.Id);
                        junctionWindows.put(junction.Window_Guideline__r.Id,new Map<Id,EAW_Window__c>());
                    }
                }
            }
        }
        if(WGSet.size() > 0 &&  insertedTitlePlanMap.keySet() != null &&  insertedTitlePlanMap.keySet().size() > 0){
            for(EAW_Window__c window : [SELECT Id,EAW_Title_Attribute__c,EAW_Plan__c,EAW_Window_Guideline__c FROM EAW_Window__c WHERE EAW_Window_Guideline__c IN :WGSet AND EAW_Title_Attribute__c IN :insertedTitlePlanMap.keySet()  /*AND EAW_Plan__c = null*/]){
                Map<Id,EAW_Window__c> tempTitleMap = junctionWindows.get(window.EAW_Window_Guideline__c);
                tempTitleMap.put(window.EAW_Title_Attribute__c,window);
                junctionWindows.put(window.EAW_Window_Guideline__c,tempTitleMap);
            }
        }
        return junctionWindows;
    }
}