public with sharing class EAW_Title_Handler {
    
    //Get custom settings - ON/OFF switches for notification.
    EAW_Notification_Status__c ns = EAW_Notification_Status__c.getOrgDefaults();
    //Get chatter group Id.
    List<CollaborationGroup> lstChatterGroup = [SELECT Id, Name FROM CollaborationGroup WHERE Name = 'Title Attribute Review'];
    
    public void deleteRelatedReleaseDates(List<EAW_Title__c> titles) {
        
        try {        
            if(titles != null && titles.size() > 0) {
                
                List<EAW_Release_Date__c> deleteDates = new List<EAW_Release_Date__c> ([
                    SELECT Id, Title__c
                    FROM EAW_Release_Date__c
                    WHERE Title__c IN: titles
                ]);
                
                if(deleteDates != null && deleteDates.size() > 0) {
                    delete deleteDates;
                }
                
                for( Integer i=0; i < titles.size(); i++ ){
                    EAW_Title__c newTitle = titles[i];
                    system.debug(':::::: deleteRelatedReleaseDates: NewTitle ::::::'+newTitle);
                    
                }
            }
        } catch(Exception ex) {
            
            System.debug('<<--exception-->>'+ex.getMessage());
        }
    }
    
    public Set<Id> processTitleToCheckForPlanQualification ( List<EAW_Title__c> titles,Set<Id> planGuidelineSet, Boolean disQualifyFlag)
    {
        //LRCC-1394 - Notification.
        Map<Id, String> titleIdName = new Map<Id, String> ();
        List<FeedItem> feedItems = new List<FeedItem>();
        Set<Id> disQualifiedPG = new Set<Id> ();
        String bodyContent;
        
        Map<Id, Set<Id>> titleAndPlanGuidelineIdMap = new Map<Id, Set<Id>>();
        Set<Id> qualifiedPlanGuidelineSet = new Set<Id>();
        Set<Id> productTypeId = new Set<Id> ();
        Map<Id, String> productTypeIdName = new Map<Id, String> ();
        Set<Id> finDivId = new Set<Id> ();
        Map<Id, String> finDivIdName = new Map<Id, String> ();
        
        try
        {
            RecordType recType = [Select Id from RecordType WHERE Name = 'Qualifier' AND SObjectType = 'EAW_Rule__c'];
            
            Set<String> conditionFieldValueSet = new Set<String>{ 'Release Year', 'Division' };
                
            List<EAW_Rule__c> qualificationRules = new List<EAW_Rule__c>();
            if(planGuidelineSet != NULL){
                 
                qualificationRules = [Select Id,Condition_Operand_Details__c, Plan_Guideline__c,Plan_Guideline__r.Product_Type__c, (Select Id, Territory__c, Condition_Field__c,Condition_Date__c, Condition_Year__c,Condition_Operator__c, Condition_Criteria_Amount__c,Condition_Operand_Details__c from Rule_Orders__r WHERE Condition_Field__c IN : conditionFieldValueSet ) from EAW_Rule__c WHERE RecordTypeId=: recType.Id AND Plan_Guideline__c IN :planGuidelineSet AND Plan_Guideline__r.Product_Type__c != NULL  AND Plan_Guideline__r.Status__c = 'Active'];
            } else{
            
                qualificationRules = [Select Id,Condition_Operand_Details__c, Plan_Guideline__c,Plan_Guideline__r.Product_Type__c, (Select Id, Territory__c, Condition_Field__c,Condition_Date__c, Condition_Year__c,Condition_Operator__c, Condition_Criteria_Amount__c,Condition_Operand_Details__c from Rule_Orders__r WHERE Condition_Field__c IN : conditionFieldValueSet ) from EAW_Rule__c WHERE RecordTypeId=: recType.Id AND Plan_Guideline__c != NULL AND Plan_Guideline__r.Product_Type__c != NULL AND Plan_Guideline__r.Status__c = 'Active'];
            }           
            
            System.debug('::::: qualificationRules :::::'+qualificationRules);
            
            for(EAW_Title__c title: titles) {
                if(String.isNotBlank(title.PROD_TYP_CD_INTL__c)) {
                    productTypeId.add(title.PROD_TYP_CD_INTL__c);
                }
                if(String.isNotBlank(title.FIN_DIV_CD__c)) {
                    finDivId.add(title.FIN_DIV_CD__c);    
                }
            }
            
            if(productTypeId.size() > 0) {
                
                for(EDM_REF_PRODUCT_TYPE__c pd: [SELECT Id, Name FROM EDM_REF_PRODUCT_TYPE__c WHERE Id IN: productTypeId]) {
                    productTypeIdName.put(pd.Id, pd.Name);
                }
            }
            
            if(finDivId.size() > 0) {
                
                for(EDM_REF_DIVISION__c rd: [SELECT Id, Name FROM EDM_REF_DIVISION__c WHERE Id IN: finDivId]) {
                    finDivIdName.put(rd.Id, rd.Name);
                }
            }
            
            EAW_PlanQualifierCalculationController qualifierController = new EAW_PlanQualifierCalculationController();
            
            for(EAW_Title__c title: titles)
            {
                for(EAW_Rule__c rule: qualificationRules)
                {
                    List<String> productTypeSet = rule.Plan_Guideline__r.Product_Type__c.toUpperCase().split(';');
                   // System.debug('<<-productTypeSet->>'+productTypeSet);
                    if(title.PROD_TYP_CD_INTL__c != null && productTypeIdName.containsKey(title.PROD_TYP_CD_INTL__c) && productTypeIdName.get(title.PROD_TYP_CD_INTL__c) != null && productTypeSet.contains(productTypeIdName.get(title.PROD_TYP_CD_INTL__c).toUpperCase())){
                        Boolean qualified = false;
                        
                        System.debug('::::: rule :::::'+rule);
                        
                        for(EAW_Rule_Detail__c ruleDetail: rule.Rule_Orders__r)
                        {
                            
                            System.debug('::::: ruleDetail :::::'+ruleDetail);
                            
                            if(ruleDetail.Condition_Field__c == 'Release Year' && title.RLSE_CAL_YR__c != NULL){
                                qualified = qualifierController.arithmeticDecimalConditionalChecking( Decimal.valueOf((title.RLSE_CAL_YR__c)), ruleDetail.Condition_Operator__c, Decimal.valueOf(ruleDetail.Condition_Year__c));
                            
                            } else if(ruleDetail.Condition_Field__c == 'Division' && title.FIN_DIV_CD__c != null && finDivIdName.containsKey(title.FIN_DIV_CD__c) && finDivIdName.get(title.FIN_DIV_CD__c) != null  && finDivIdName.get(title.FIN_DIV_CD__c) == ruleDetail.Condition_Operand_Details__c ){ 
                                qualified = TRUE;
                            } 
                            if( !qualified ) break;
                        } 
                        
                        System.debug('::::: qualified :::::'+qualified);
                        
                        
                        if(qualified)
                        {
                            //create a map of plan guideline and title and use the method of title plan controller to create plans and windows
                            Set<Id> planGuidelineIdSet = new Set<Id>();
                            if( titleAndPlanGuidelineIdMap.containsKey(title.Id) ) {
                                planGuidelineIdSet = titleAndPlanGuidelineIdMap.get(title.Id);
                            }
                            planGuidelineIdSet.add(rule.Plan_Guideline__c);
                            qualifiedPlanGuidelineSet.add(rule.Plan_Guideline__c);
                            titleAndPlanGuidelineIdMap.put(title.Id, planGuidelineIdSet);
                        }
                        //LRCC-1394: Notification.
                        else if(disQualifyFlag != null && disQualifyFlag == true)  {
                            disQualifiedPG.add(rule.Plan_Guideline__c);
                            titleIdName.put(title.Id, title.Name);
                        }
                    }
                }
            }
            
            if(!lstChatterGroup.isEmpty()) {
                 
                if(disQualifiedPG.size() > 0 && titleIdName.size() > 0 
                    && ns.TACategoryNotification__c == true && ns.TATitlePlanDisQualifierNotification__c == true) {
                    
                    for(EAW_Plan__c plan: [SELECT Id, Plan_Guideline__c, Plan_Guideline__r.Name, EAW_Title__c, EAW_Title__r.Name__c FROM EAW_Plan__c WHERE Plan_Guideline__c IN: disQualifiedPG AND EAW_Title__c IN: titleIdName.keySet()]) {
                
                        if(checkRecursiveDataforNotification.isRecursiveData(plan.Id)) {
                        
                            if(String.isBlank(bodyContent)) {
                                bodyContent = plan.EAW_Title__r.Name__c +' - disqualifies for '+plan.Plan_Guideline__r.Name+'.'+'\n'+System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+plan.Id;
                            }
                            else {
                                bodyContent += '\n\n'+ plan.EAW_Title__r.Name__c +' - disqualifies for '+plan.Plan_Guideline__r.Name+'.'+'\n'+System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+plan.Id;
                            }
                        }
                    }               
                }
                
                if(String.isNotBlank(bodyContent)) {
                
                    String temp = bodyContent;
                    
                    if(bodyContent.length() < 10000) {
                        bodyContent = temp.substring(0, temp.length());
                    } 
                    else {
                        bodyContent = temp.substring(0,9999);    
                    }
                                        
                    feedItems.add(
                        new FeedItem(
                            Body = bodyContent,
                            ParentId = lstChatterGroup[0].Id
                        )
                    );  
                            
                    if(feedItems != null && feedItems.size() > 0) {
                        insert feedItems;
                    }
                }
            }
            
            if( titleAndPlanGuidelineIdMap != NULL && titleAndPlanGuidelineIdMap.size() > 0 ){
                //LRCC - 1239
                EAW_PlanGuidelineQualifierController.createPlanAndWindowsForQualifiedTitles(titleAndPlanGuidelineIdMap,qualifiedPlanGuidelineSet);
                /*for( Id titleId : titleAndPlanGuidelineIdMap.keySet() ){
                    
                    for(Id planGuidelineId : titleAndPlanGuidelineIdMap.get(titleId) ){
                        Map<String, List<SObject>> titlePlanAndWindowsResultMap = EAW_TitlePlanWindowController.createPlan(titleId, planGuidelineId, null, null, false);
                    }
                    
                }*/
            }
        } catch(Exception ex) {
            System.debug('<<--Exception-->>'+ex.getMessage());
        }
        for( Integer i=0; i < titles.size(); i++ ){
            EAW_Title__c newTitle = titles[i];
        }
        return titleAndPlanGuidelineIdMap.keyset();
    }
    
    public void filterTitleToCheckForPlanQualification( List<EAW_Title__c> newTitles, List<EAW_Title__c> oldTitles ){
        
        List<EAW_Title__c> filteredTitleList = new List<EAW_Title__c>();
        
        if( newTitles != NULL && newTitles.size() > 0 ) {
            
            for( Integer i=0; i < newTitles.size(); i++ ){
                
                EAW_Title__c newTitle = newTitles[i];
                EAW_Title__c oldTitle = oldTitles[i];
                
                if( newTitle.RLSE_CAL_YR__c != oldTitle.RLSE_CAL_YR__c /* || newTitle.FIN_DIV_CD__c != oldTitle.FIN_DIV_CD__c*/ ){ // Commented for LRCC - 1827
                    filteredTitleList.add(newTitle);
                }
            }
        }
        if( filteredTitleList.size() > 0 ) processTitleToCheckForPlanQualification(filteredTitleList, null, true);
    }
    
    //LRCC-1567
    public void findTitleName( List<EAW_Title__c> newTitles) {
        if( newTitles != NULL && newTitles.size() > 0 ) {
            for( EAW_Title__c eawTc: newTitles){
                if(String.isNotBlank(eawTc.TVD_Alias__c)){
                    eawTc.Name__c = eawTc.TVD_Alias__c;
                } 
                else if(String.isNotBlank(eawTc.WPR_title__c)) {
                    eawTc.Name__c = eawTc.WPR_title__c;
                }
                if(String.isNotBlank(eawTc.Name__c)){
                    eawTC.Name = eawTc.Name__c;
                    if((eawTc.Name__c).length() >= 80){
                        eawTc.Name = (eawTc.Name__c).substring(0, 80); // LRCC - 1870
                    }
                } else if(String.isNotBlank(eawTc.Name)){ 
                    eawTc.Name__c = eawTc.Name;                    
                    if((eawTc.Name).length() >= 80){                                              
                        eawTc.Name = (eawTc.Name__c).substring(0, 80); // LRCC - 1870
                    } 
                }
            }
        }
    }
    
    //Notifications related implementation: LRCC-1048
    public void sentNotificationTAReviewGroup(List<EAW_Title__c> triggerOld, List<EAW_Title__c> triggerNew) {
            
        Set<Id> divisionIds = new Set<Id> ();
        Set<Id> intlIds = new Set<Id> ();
        Map<Id, String> divisionIdName = new Map<Id, String> ();
        Map<Id, String> intlIdName = new Map<Id, String> ();
        
        List<FeedItem> feedItems = new List<FeedItem> ();
        
        try {
            
            if(!lstChatterGroup.isEmpty()) {
                
                for(EAW_Title__c title: triggerNew) {
                    if(String.isNotBlank(title.FIN_DIV_CD__c)) {
                        divisionIds.add(title.FIN_DIV_CD__c);
                    }
                    if(String.isNotBlank(title.PROD_TYP_CD_INTL__c)) {
                        intlIds.add(title.PROD_TYP_CD_INTL__c);
                    }
                }
                
                if(divisionIds != null && divisionIds.size() > 0) {
                    
                    for(EDM_REF_DIVISION__c div: [SELECT Id, Name FROM EDM_REF_DIVISION__c WHERE Id IN: divisionIds]) {
                        divisionIdName.put(div.Id, div.Name);
                    }
                }
                
                if(intlIds != null && intlIds.size() > 0) {
                    
                    for(EDM_REF_PRODUCT_TYPE__c intl: [SELECT Id, Name FROM EDM_REF_PRODUCT_TYPE__c WHERE Id IN: intlIds]) {
                        intlIdName.put(intl.Id, intl.Name);
                    }
                }
                
                for(Integer i = 0; i < triggerNew.size(); i++) {
                    
                    String bodyContent = '';
                    
                    EAW_Title__c oldTitle;
                    
                    if(triggerOld != null && triggerOld.size() > 0) {
                        oldTitle = triggerOld.get(i);
                    }
                    
                    EAW_Title__c newTitle = triggerNew.get(i);
                    
                    if(ns.TACategoryNotification__c == true) {
                        
                        //NA-12
                        if(oldTitle == null
                            && String.isNotBlank(newTitle.PROD_TYP_CD_INTL__c)
                            && ns.TACreateNotification__c == true) {
                            
                            bodyContent += newTitle.Name__c +' is added to Foxipedia.'+'\n';
                        }
                        
                        //NA-19
                        if(oldTitle != null
                            && intlIdName.containsKey(newTitle.PROD_TYP_CD_INTL__c)
                            && intlIdName.get(newTitle.PROD_TYP_CD_INTL__c) != null
                            && String.isNotBlank(newTitle.PROD_TYP_CD_INTL__c)
                            && newTitle.PROD_TYP_CD_INTL__c <> oldTitle.PROD_TYP_CD_INTL__c 
                            && ns.TAProductTypeNotification__c == true) {
                            
                            bodyContent += newTitle.Name__c +' - International Product Type has been changed to '+intlIdName.get(newTitle.PROD_TYP_CD_INTL__c)+'.'+'\n';
                        }
                        
                        //NA-20
                        if(divisionIdName != null
                            && divisionIdName.containsKey(newTitle.FIN_DIV_CD__c)
                            && divisionIdName.get(newTitle.FIN_DIV_CD__c) != null
                            && oldTitle != null
                            && String.isNotBlank(newTitle.PROD_TYP_CD_INTL__c)
                            && String.isNotBlank(newTitle.FIN_DIV_CD__c)
                            && newTitle.FIN_DIV_CD__c <> oldTitle.FIN_DIV_CD__c
                            && ns.TADivisionNotification__c == true) {
                            
                            bodyContent += newTitle.Name__c +' - Division has been changed to '+divisionIdName.get(newTitle.FIN_DIV_CD__c)+'.'+'\n';
                        }    
                        
                        //NA-41
                        if(oldTitle != null
                            && String.isNotBlank(newTitle.PROD_TYP_CD_INTL__c)
                            && String.isNotBlank(newTitle.LIFE_CYCL_STAT_GRP_CD__c)
                            && oldTitle.LIFE_CYCL_STAT_GRP_CD__c == 'PUBLC'
                            && (newTitle.LIFE_CYCL_STAT_GRP_CD__c == 'CNFDL' || newTitle.LIFE_CYCL_STAT_GRP_CD__c == 'SCNFDL') 
                            && ns.TAStatusNotification__c == true) {
                            
                            if(newTitle.LIFE_CYCL_STAT_GRP_CD__c == 'CNFDL') {
                                bodyContent += newTitle.Name__c +' - Confidential status has been changed from '+oldTitle.LIFE_CYCL_STAT_GRP_CD__c+' to '+newTitle.LIFE_CYCL_STAT_GRP_CD__c+'.'+'\n';
                            }
                            else {
                                bodyContent += newTitle.Name__c +' - Semi-confidential status has been changed from '+oldTitle.LIFE_CYCL_STAT_GRP_CD__c+' to '+newTitle.LIFE_CYCL_STAT_GRP_CD__c+'.'+'\n';
                            }
                        }
                    }
                    
                    //NA-15
                    if(ns.TVDCategoryNotification__c && ns.TVDNumberOfScreensNotification__c == true && String.isNotBlank(newTitle.PROD_TYP_CD_INTL__c)) {
                        
                        if((oldTitle == null || oldTitle.US_Admissions__c == null || oldTitle.US_Admissions__c == 0) && (newTitle.US_Admissions__c != null && newTitle.US_Admissions__c != 0)) {
                        
                            bodyContent += newTitle.Name__c +' - US Admissions has been added '+newTitle.US_Admissions__c+'.'+'\n';     
                        }
                        else if(oldTitle != null 
                                && newTitle.US_Admissions__c != null
                                && newTitle.US_Admissions__c != oldTitle.US_Admissions__c) {
                            
                            bodyContent += newTitle.Name__c +' - US Admissions has been changed from '+oldTitle.US_Admissions__c+' to '+newTitle.US_Admissions__c+'.'+'\n';     
                        }
                        else if(oldTitle != null 
                                && oldTitle.US_Admissions__c != null
                                && newTitle.US_Admissions__c == null) {
                            
                            bodyContent += newTitle.Name__c +' - US Admissions has been deleted.'+'\n';     
                        }
                    }
                    
                    if(String.isNotBlank(bodyContent)) {
                        feedItems.add(
                            new FeedItem(
                                body = bodyContent+'\n'+newTitle.SFDC_Base_Instance__c+'/'+newTitle.Id,
                                ParentId = lstChatterGroup[0].Id
                            )
                        );
                    }
                }
                
                if(feedItems != null && feedItems.size() > 0) {
                    insert feedItems;
                }
            }
        }
        catch(Exception e) {}  
    }
}