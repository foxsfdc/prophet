public class EAW_ReleaseDateTag_Handler {
    public static void reQualificationOfRules(List<EAW_Release_Date_Tag_Junction__c> oldReleaseDateTags,List<EAW_Release_Date_Tag_Junction__c> newReleaseDateTags){
        try{
            Set<String> tagSet = new Set<String>();
            Set<String> condtionalOperandSet = new Set<String>();
            for(Integer i=0;i<oldReleaseDateTags.size();i++){
                if(oldReleaseDateTags[i].Tag__c != NULL && newReleaseDateTags[i].Tag__c != NULL){
                    if(oldReleaseDateTags[i].Tag__c != newReleaseDateTags[i].Tag__c){
                        tagSet.add(oldReleaseDateTags[i].Tag__r.Name);
                        tagSet.add(oldReleaseDateTags[i].Tag__r.Name);
                        condtionalOperandSet.add(oldReleaseDateTags[i].Release_Date__r.Release_Date_Guideline__c);
                    }
                }
            }
            system.debug('tagSet:::'+tagSet);
            
            Id releaseDateRuleRecordTypeId = Schema.SObjectType.EAW_Rule__c.getRecordTypeInfosByName().get('Release Date Calculation').getRecordTypeId();
            Id widowDateRuleRecordTypeId = Schema.SObjectType.EAW_Rule__c.getRecordTypeInfosByName().get('Window Date Calculation').getRecordTypeId();
            
            List<EAW_Rule_Detail__c> releaseDateRuleDetailList = [SELECT Id,Rule_No__c,Conditional_Operand_Id__c FROM EAW_Rule_Detail__c WHERE (Conditional_Operand_Id__c IN : condtionalOperandSet OR Rule_No__r.Conditional_Operand_Id__c IN :condtionalOperandSet ) AND (Condition_Field__c = 'Release Date Tag' OR Rule_No__r.Condition_Field__c = 'Release Date Tag') AND (Rule_No__r.Criteria__c 
                                                                                                                                                                                                                                                                                                                                                                                IN :tagSet OR Condition_Criteria__c IN : tagSet) AND (Rule_No__r.RecordTypeId = :releaseDateRuleRecordTypeId) AND (Rule_No__r.Release_Date_Guideline__r.Active__c = true) AND Rule_No__r.Release_Date_Guideline__c != null];
            List<EAW_Rule_Detail__c> windowDateRuleDetailList = [SELECT Id,Rule_No__c,Conditional_Operand_Id__c FROM EAW_Rule_Detail__c WHERE (Conditional_Operand_Id__c IN : condtionalOperandSet OR Rule_No__r.Conditional_Operand_Id__c IN :condtionalOperandSet ) AND (Condition_Field__c = 'Release Date Tag' OR Rule_No__r.Condition_Field__c = 'Release Date Tag') AND (Rule_No__r.Criteria__c 
                                                                                                                                                                                                                                                                                                                                                                                IN :tagSet OR Condition_Criteria__c IN : tagSet) AND (Rule_No__r.RecordTypeId = :widowDateRuleRecordTypeId) AND (Rule_No__r.Window_Guideline__r.Status__c = 'Active') AND Rule_No__r.Window_Guideline__c != null];
            
            System.debug(':ReQualifiedRuleJunction::::'+releaseDateRuleDetailList);
            if(releaseDateRuleDetailList.size() > 0){
                for(EAW_Rule_Detail__c  ruleDetail : releaseDateRuleDetailList){
                    if(checkRecursiveData.isRecursiveData(ruleDetail.Rule_No__c)){
                        system.debug('reQualify:::'+ruleDetail.Rule_No__c);
                        EAW_RuleBuilderCalculationController.processTitlesWithNewRule(ruleDetail.Rule_No__c);
                    }
                }
            }
            Set<Id> windowRuleSet = new Set<Id>();
            if(windowDateRuleDetailList.size() > 0){
                for(EAW_Rule_Detail__c  ruleDetail : windowDateRuleDetailList){
                    if(checkRecursiveData.isRecursiveData(ruleDetail.Rule_No__c)){
                        windowRuleSet.add(ruleDetail.Rule_No__c);
                    }
                }
            }
            if(windowRuleSet.size() > 0){
                EAW_WGRuleBuilderCalculationController.processWindowGuidelineWithEachRule(windowRuleSet,null);
            }
        } catch (Exception e){
            
        }
    }
}