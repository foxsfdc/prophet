public with sharing class EAW_PlanHandler {
    
    public void populatePlanName(List<EAW_Plan__c> plans) {
                 
        for(EAW_Plan__c p: plans) {
        
            system.debug(':::Insert plan::::;'+p);
            if(p.EAW_Title__c != null && p.Plan_Guideline__c != null) {
                
                if(p.TITLE_FIN_PROD_ID__c != null) {
                    p.Name__c = p.Title_Name__c + '/' + p.Plan_Guideline_Name__c + '/' +p.TITLE_FIN_PROD_ID__c;
                }
                else {
                    p.Name__c = p.Title_Name__c + '/' + p.Plan_Guideline_Name__c;
                }
                
                String temp = p.Title_Name__c + '/' + p.Plan_Guideline_Name__c;
                if(temp.length() < 80) {
                    p.Name = temp.substring(0,temp.length());
                } else {
                    p.Name = temp.substring(0,80);    
                }
            }
        }    
    }
    //LRCC-739
    public void populatePlanFieldValues(List<EAW_Plan__c> plans) {
        
        Set<Id> planGuidelineIds = new Set<Id>();
        
        for(EAW_Plan__c plan : plans) {
            if(plan.Plan_Guideline__c != null) {
                planGuidelineIds.add(plan.Plan_Guideline__c);
            }
        }
        
        Map<Id, EAW_Plan_Guideline__c> planGuidelineMap = new Map<Id, EAW_Plan_Guideline__c> ([ Select Id, Name, Sales_Region__c, Territory__c, Language__c From EAW_Plan_Guideline__c Where Id IN : planGuidelineIds ]);
        
        for(EAW_Plan__c plan : plans) {
            if(plan.Plan_Guideline__c != null) {
            
                plan.Sales_Region__c = planGuidelineMap.get(plan.Plan_Guideline__c).Sales_Region__c;
                plan.Territory__c = planGuidelineMap.get(plan.Plan_Guideline__c).Territory__c;
                plan.Language__c = planGuidelineMap.get(plan.Plan_Guideline__c).Language__c;    
            }        
        }
    }
    
    public void validateProductTypes(List<EAW_Plan__c> plans) {
        //LRCC-1078 Foxipedia integration
        if(!checkRecursiveData.bypassDateCalculation){
        
            Set<Id> titleProductTypeIds = new Set<Id> ();
            Map<Id, String> planGuidelineIdProductTypes = new Map<Id, String> ();
            Set<String> productTypeName = new Set<String> ();
            Boolean isSuccess = false;
            
            for(EAW_Plan__c p: [SELECT Id, EAW_Title__c, EAW_Title__r.PROD_TYP_CD_INTL__c, Plan_Guideline__c, Plan_Guideline__r.Product_Type__c FROM EAW_Plan__c WHERE Id IN: plans]) {
                
                if(String.isNotBlank(p.EAW_Title__r.PROD_TYP_CD_INTL__c) && String.isNotBlank(p.Plan_Guideline__r.Product_Type__c)) {
                    
                    titleProductTypeIds.add(p.EAW_Title__r.PROD_TYP_CD_INTL__c);
                    planGuidelineIdProductTypes.put(p.Plan_Guideline__c, p.Plan_Guideline__r.Product_Type__c);    
                }    
            }
            system.debug('plans: '+system.JSON.serialize(plans));
            system.debug('titleProductTypeIds: '+titleProductTypeIds);
            system.debug('planGuidelineIdProductTypes: '+planGuidelineIdProductTypes);
            
            for(EDM_REF_PRODUCT_TYPE__c pt: [SELECT Id, Name FROM EDM_REF_PRODUCT_TYPE__c WHERE Id IN: titleProductTypeIds]) {
                productTypeName.add(pt.Name);
            }
            system.debug('productTypeName: '+productTypeName);
            if(planGuidelineIdProductTypes != null && planGuidelineIdProductTypes.size() > 0 && productTypeName.size() > 0) {
                
                for(EAW_Plan__c plan: plans) {
                    
                    if(String.isNotBlank(plan.Plan_Guideline__c) && planGuidelineIdProductTypes.containsKey(plan.Plan_Guideline__c)) {
                    
                        List<String> pgProductTypes = planGuidelineIdProductTypes.get(plan.Plan_Guideline__c).split(';');
                        system.debug('pgProductTypes: '+pgProductTypes);
                        if(pgProductTypes != null && pgProductTypes.size() > 0) {
                            
                            for(String productType: pgProductTypes) {
                                
                                if(productTypeName.contains(productType)) {
                                    isSuccess = true;
                                    break;
                                }
                            }
                            
                            if(isSuccess == false) {
                                plan.addError(System.Label.EAW_PGTitleAttributeProductTypeMatching);
                            }
                        }
                    }
                }
            }
        }
    }
    
    public void createWindow(List<EAW_Plan__c> plans) {
        //LRCC-1078 Foxipedia integration
        if(!checkRecursiveData.bypassDateCalculation){
            List<EAW_Window__c> windowsToInsert = new List<EAW_Window__c> ();
                
            try {
                
                Set<Id> pgIds = new Set<Id> ();
                Set<Id> titleIds = new Set<Id> ();
                Map<Id, List<EAW_Window_Guideline__c>> pgWithWindowGuidelineMap = new Map<Id, List<EAW_Window_Guideline__c>> ();
                Map<Id,String> titleProductTypeMap = new Map<Id,String>();
                
                for(EAW_Plan__c p: [SELECT Id, EAW_Title__c, EAW_Title__r.PROD_TYP_CD_INTL__r.Name, Plan_Guideline__c, isCustom__c FROM EAW_Plan__c WHERE Plan_Guideline__c != null AND isCustom__c = false AND Id IN: plans]) {
                    pgIds.add(p.Plan_Guideline__c);
                    titleIds.add(p.EAW_Title__c);    
                }
                
                if(pgIds.size() > 0) {
                    
                    for(EAW_Title__c title: [SELECT Id, Name, PROD_TYP_CD_INTL__r.Name FROM EAW_Title__c WHERE Id IN :titleIds]) {
                        titleProductTypeMap.put(title.Id, title.PROD_TYP_CD_INTL__r.Name);
                    }
                    
                    String windowGuidelineQuery = 'SELECT Window_Guideline__r.Name, Window_Guideline__r.Window_Type__c, Window_Guideline__r.Product_Type__c, Window_Guideline__r.Status__c, Window_Guideline__r.Start_Date__c, Window_Guideline__r.End_Date__c, ' +
                                'Window_Guideline__r.AutoCreate__c, Window_Guideline__r.Tracking_Date__c, Window_Guideline__r.Outside_Date__c, Window_Guideline__r.Media__c, Window_Guideline__r.Description__c, Window_Guideline__r.Customers__c, Plan__c ' +
                                'FROM EAW_Plan_Window_Guideline_Junction__c WHERE Plan__c IN: pgIds';
                    
                    List<EAW_Plan_Window_Guideline_Junction__c> planWindowGuidelineJunctions = Database.query(windowGuidelineQuery);
                    
                    if(planWindowGuidelineJunctions != null && planWindowGuidelineJunctions.size() > 0) {
                        
                        for(EAW_Plan_Window_Guideline_Junction__c windowGuidelineJunction : planWindowGuidelineJunctions) {
                            
                            EAW_Window_Guideline__c windowGuideline = new EAW_Window_Guideline__c();
                            windowGuideline.Id = windowGuidelineJunction.Window_Guideline__r.Id;
                            windowGuideline.Name = windowGuidelineJunction.Window_Guideline__r.Name;
                            windowGuideline.Window_Type__c = windowGuidelineJunction.Window_Guideline__r.Window_Type__c;
                            windowGuideline.Product_Type__c = windowGuidelineJunction.Window_Guideline__r.Product_Type__c;
                            windowGuideline.Status__c = windowGuidelineJunction.Window_Guideline__r.Status__c;
                            windowGuideline.Start_Date__c = windowGuidelineJunction.Window_Guideline__r.Start_Date__c;
                            windowGuideline.End_Date__c = windowGuidelineJunction.Window_Guideline__r.End_Date__c;
                            windowGuideline.Tracking_Date__c = windowGuidelineJunction.Window_Guideline__r.Tracking_Date__c;
                            windowGuideline.Outside_Date__c = windowGuidelineJunction.Window_Guideline__r.Outside_Date__c;
                            windowGuideline.Media__c = windowGuidelineJunction.Window_Guideline__r.Media__c;
                            windowGuideline.Description__c = windowGuidelineJunction.Window_Guideline__r.Description__c;
                            windowGuideline.Customers__c = windowGuidelineJunction.Window_Guideline__r.Customers__c;
                            windowGuideline.AutoCreate__c = windowGuidelineJunction.Window_Guideline__r.AutoCreate__c;
                            
                            List<EAW_Window_Guideline__c> wgList = new List<EAW_Window_Guideline__c> ();
                            
                            if(pgWithWindowGuidelineMap.containsKey(windowGuidelineJunction.Plan__c) && pgWithWindowGuidelineMap.get(windowGuidelineJunction.Plan__c).size() > 0) {
                                wgList = pgWithWindowGuidelineMap.get(windowGuidelineJunction.Plan__c);
                            }
                            wgList.add(windowGuideline);
                            pgWithWindowGuidelineMap.put(windowGuidelineJunction.Plan__c, wgList);
                        }
                    }
                    
                    if(pgWithWindowGuidelineMap != null && pgWithWindowGuidelineMap.size() > 0) {
                         
                        for(EAW_Plan__c p: plans) {
                            
                            if(p.Plan_Guideline__c != null && pgWithWindowGuidelineMap.containsKey(p.Plan_Guideline__c)) {
                            
                                List<EAW_Window_Guideline__c> wgs = pgWithWindowGuidelineMap.get(p.Plan_Guideline__c);
                                
                                if(wgs != null && wgs.size() > 0) {
                                
                                    for(EAW_Window_Guideline__c wg: wgs) {
                                        
                                        if(titleProductTypeMap.containsKey(p.EAW_Title__c) 
                                            && titleProductTypeMap.get(p.EAW_Title__c) != null 
                                            && (titleProductTypeMap.get(p.EAW_Title__c)).toUpperCase() == (wg.Product_Type__c).toUpperCase()) {
                                         
                                            EAW_Window__c newWindow = new EAW_Window__c();
                                            newWindow.Name = wg.Name;
                                            newWindow.Window_Type__c = wg.Window_Type__c;
                                            newWindow.Status__c = 'Estimated';
                                            newWindow.Start_Date__c = wg.Start_Date__c;
                                            newWindow.End_Date__c = wg.End_Date__c;
                                            newWindow.Tracking_Date__c = wg.Tracking_Date__c;
                                            newWindow.Outside_Date__c = wg.Outside_Date__c;
                                            newWindow.Media__c = wg.Media__c;
                                            newWindow.Customers__c = wg.Customers__c;
                                            newWindow.EAW_Window_Guideline__c = wg.Id;
                                            newWindow.EAW_Plan__c = p.Id;
                                            newWindow.EAW_Title_Attribute__c = p.EAW_Title__c;
                                            
                                            windowsToInsert.add(newWindow);
                                        }
                                    }
                                }
                            }
                        }
                        
                        if(windowsToInsert.size() > 0) {
                            insert windowsToInsert;
                        }
                    }
                }
            }
            catch (DmlException e) {
                for(EAW_Window__c window: windowsToInsert) {
                    window.addError(e.getDmlMessage(0));
                }
            }
        }
    }
}