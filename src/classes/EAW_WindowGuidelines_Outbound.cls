public with sharing class EAW_WindowGuidelines_Outbound {
    private static List<EAW_Error_Log__c> repoErrorLogs  = new List<EAW_Error_Log__c>();
    private static set<id> repoSuccessIds= new set<id>();
    private static Set<id> repoIgnoredIds = new Set<id>();
    
	String repoUrl = '';
	
	public EAW_WindowGuidelines_Outbound(){
		checkOuboundRecursiveData.bypassTriggerFire=true;
		Rest_Endpoint_Settings__c repoInstance = Rest_Endpoint_Settings__c.getInstance('Outbound-WG-to-Repo');
		if(repoInstance!=null){
			repoUrl=repoInstance.End_Point__c;
		}		
	}
	
    public class WindowGuidelineJson{
        public String windowGuidelineId;
        public String windowGuidelineName;
        public String windowGuidelineAlias;
        public String description;
        public boolean autoCreate;
        public String deleted;
        public String lastModifiedDate;
        public String lastModifiedBy;
        public String customers;
        public String productTypeId;
        public String productType;
        public String windowGuidelineTypeId;
        public String windowGuidelineType;
        public String versionNo;
        public String versionNotes;
        public String previousVersionWGId;
        public String nextVersionWGId;
        public String startDateRuleText;
        public String endDateRuleText;
        public String outsideDateRuleText;
        public String trackingDateRuleText;
        public String windowGuidelineStatus;
        public String operation;
        
        public WindowGuidelineJson(String windowGuidelineId,String windowGuidelineName,String windowGuidelineAlias,
        							String description,boolean autoCreate,String deleted,String lastModifiedDate,
									String lastModifiedBy,String customers,String productTypeId,String productType,
									String windowGuidelineTypeId,String windowGuidelineType,String versionNo,
									String versionNotes,String previousVersionWGId,String nextVersionWGId,
									String startDateRuleText,String endDateRuleText,String outsideDateRuleText,
									String trackingDateRuleText,String windowGuidelineStatus,String operation){
                                        
            this.windowGuidelineId = windowGuidelineId;
			this.windowGuidelineName = windowGuidelineName;
			this.windowGuidelineAlias = windowGuidelineAlias;
			this.description = description;
			this.autoCreate = autoCreate;
			this.deleted = deleted;
			this.lastModifiedDate = lastModifiedDate;
			this.lastModifiedBy = lastModifiedBy;
			this.customers = customers;
			this.productTypeId = productTypeId;
			this.productType = productType;
			this.windowGuidelineTypeId = windowGuidelineTypeId;
			this.windowGuidelineType = windowGuidelineType;
			this.versionNo = versionNo;
			this.versionNotes = versionNotes;
			this.previousVersionWGId = previousVersionWGId;
			this.nextVersionWGId = nextVersionWGId;
			this.startDateRuleText = startDateRuleText;
			this.endDateRuleText = endDateRuleText;
			this.outsideDateRuleText = outsideDateRuleText;
			this.trackingDateRuleText = trackingDateRuleText;
			this.windowGuidelineStatus= windowGuidelineStatus;
			this.operation = operation;
        }
        
        
    }
    
    private void getJson(EAW_Outbound_Utility.OutboundSystem os, List<EAW_Window_Guideline__c> recordList){
        for(EAW_Window_Guideline__c wg:recordList){
        	List<id> idList = new List<id>();
        	idList.add(wg.id);
            String operation = '';            
            
            EAW_Outbound_Utility eouRepo = new EAW_Outbound_Utility(EAW_Outbound_Utility.OutboundDataType.WindowGuideline,os,idList);
            if(os==EAW_Outbound_Utility.OutboundSystem.Repo){
            	operation = wg.Repo_DML_Type__c;
            	WindowGuidelineJson wgJson = new WindowGuidelineJson(wg.id+'',wg.Name,wg.Window_Guideline_Alias__c,wg.Description__c,wg.AutoCreate__c,wg.Soft_Deleted__c,
        			EAW_Outbound_Utility.getFormattedDateTime(wg.LastModifiedDate),wg.LastModifiedBy.name,wg.Customers__c,null,
        			wg.Product_Type__c,null,wg.Window_Type__c,''+wg.Version__c,wg.Version_Notes__c,''+wg.Previous_Version__c,wg.Next_Version__c+'',
        			wg.Start_Date_Rule__c,wg.End_Date_Rule__c,wg.Outside_Date_Rule__c,wg.Tracking_Date_Rule__c,wg.Status__c,operation);
                eouRepo.sendDataThroughWebAPI( system.json.serialize(wgJson),repoUrl,repoErrorLogs,repoSuccessIds);
            }            
        }
    }
    
    private void aggregateAllWGs(List<EAW_Window_Guideline__c> records,String operation){
        List<EAW_Window_Guideline__c> repoRecords = new List<EAW_Window_Guideline__c>();
        
        List<EAW_Window_Guideline__c> updateList = new List<EAW_Window_Guideline__c>();
        List<EAW_Window_Guideline__c> deleteList = new List<EAW_Window_Guideline__c>();
        List<EAW_Error_Log__c> insertErrorList = new List<EAW_Error_Log__c>();
        
        if(records!=null && !records.isempty()){
            for(EAW_Window_Guideline__c record:records){
            	
    			if('update'.equalsignorecase(operation) || 'Insert'.equalsignorecase(operation)){
    				if(record.Sent_To_Repo__c){
    					record.Repo_DML_Type__c='Update';
    				}else{
    					record.Repo_DML_Type__c='Insert';
    				}
    			}else if('delete'.equalsignorecase(operation)){
    				record.Repo_DML_Type__c='Delete';
    			}
    			
                boolean isSoftDeleted = record.Soft_Deleted__c=='TRUE';//To do after handling soft delete please use the active flag
                if(record.Sent_To_Repo__c || !isSoftDeleted){
                    repoRecords.add(record);
                }else if(!record.Sent_To_Repo__c && isSoftDeleted){
                    repoIgnoredIds.add(record.id);
                }            
            }
            if(!repoRecords.isEmpty()){
                getJson(EAW_Outbound_Utility.OutboundSystem.Repo,repoRecords);
            }
                        
            for(EAW_Window_Guideline__c record:records){
                    boolean canBeDeleted = false;
                    boolean isSoftDeleted = record.Soft_Deleted__c=='TRUE';//To do after handling soft delete please use the active flag  
                    if((repoSuccessIds.contains(record.id)) //Both are success
                        || (repoIgnoredIds.contains(record.id))//If record is inactive and didn't send to both api then we can delete without sending them
                        ){
                        record.Send_To_Third_Party__c=false;
                        canBeDeleted = true; // Can be deleted
                    }
                    if(repoSuccessIds.contains(record.id)){//Repo call is success then repo DML type is blank and sent to repo should be true
                        record.Sent_To_Repo__c=true;
                        record.Repo_DML_Type__c='';
                    }
                    
                    if(isSoftDeleted && canBeDeleted){
                        deleteList.add(record);
                    }else{
                        updateList.add(record);
                    }
            }
            insertErrorList.addAll(repoErrorLogs);            
        }
        if(updateList!=null && !updateList.isEmpty()){
            update updateList;
        }
        if(deleteList!=null && !deleteList.isEmpty()){
            delete deleteList;
        }
        if(insertErrorList!=null && !insertErrorList.isEmpty()){
            insert insertErrorList;
        }
        //To do Skip downstream calculation of Release dates and Windows
        //if response 200 then Send_To_Third_Party - False, Dml_Type as blank
        //Sent_To_Repo-true
        //Sent_To_Galileo-true
    }

    
    private static List<EAW_Window_Guideline__c> getWGs(Set<id> WGIds){
        List<EAW_Window_Guideline__c> WGlist = [select id,Name,Window_Guideline_Alias__c,Description__c,AutoCreate__c,Soft_Deleted__c,LastModifiedDate,LastModifiedBy.name,Customers__c,
        										Product_Type__c,Window_Type__c,Version__c,Version_Notes__c,Previous_Version__c,Next_Version__c,Start_Date_Rule__c,End_Date_Rule__c,
        										Outside_Date_Rule__c,Tracking_Date_Rule__c,Status__c,Repo_DML_Type__c, Sent_To_Repo__c,Send_To_Third_Party__c 
        										from EAW_Window_Guideline__c where id in :WGIds];                                      
        return WGlist;
    }
    
    private static void sendData(Set<id> windowGuidelineIds,String operation){
    	EAW_WindowGuidelines_Outbound ewgo = new EAW_WindowGuidelines_Outbound();
    	ewgo.aggregateAllWGs(getWGs(windowGuidelineIds), operation);
    }
    
    @future(callout=true)
    public static void sendDataAsync(Set<id> WGIds,String operation){
        sendData(WGIds, operation);
    }
    
    public static void sendDataSync(Set<id> WGIds,String operation){
       sendData(WGIds, operation);
    }
    
   
}