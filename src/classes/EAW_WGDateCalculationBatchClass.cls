public class EAW_WGDateCalculationBatchClass implements Database.Batchable<sObject>{

    public Set<Id> releaseDateGuidelineIdSet = new Set<Id>();
    public Set<Id> windowGuidelineIdSet = new Set<Id>();
    public Set<Id> titleIdSet = new Set<Id>();
    //LRCC-1863
    public Boolean allowWindowDateChange = false;
    
    //LRCC-1881
    public Boolean restrictWindowStatusChange = False;
    
    public EAW_WGDateCalculationBatchClass( Set<Id> wgIdSetToProcess ){
        
        this.windowGuidelineIdSet = wgIdSetToProcess;
        
        System.debug('::::; windowGuidelineIdSet :::::'+windowGuidelineIdSet);
        
        
        //The following code was not working expected have to build new method to find the parent WG for the child WG
        
        //The following logic is defined to remove the child RDG for the date calculation if the parent RDG is also present
        // Redefined the following method logic by LRCC - 1461 to return proper downstream reference map.
        EAW_DownstreamRuleGuidelinesFindUtil util = new EAW_DownstreamRuleGuidelinesFindUtil();
        Map<Id, Set<Id>> referrencedWGs = new Map<Id, Set<Id>>();
        referrencedWGs = util.findReferencedWindowGuidelineRelatedToReleaseDateGuideline(wgIdSetToProcess);
        for( Id wgId : referrencedWGs.keySet()){
            Set<Id> wgIdSet = referrencedWGs.get(wgId);
            if( wgIdSet != NULL ) {
                wgIdSet.remove(wgId);
                windowGuidelineIdSet.removeAll(wgIdSet);
            }
        }
        
        System.debug('::::; windowGuidelineIdSet-2 :::::'+windowGuidelineIdSet);
    }
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
    
        System.debug(':::releaseDateGuidelineIdSet:::' + releaseDateGuidelineIdSet);
        system.debug('::titleIdSet::'+titleIdSet);
        System.debug(':::::::: windowGuidelineIdSet :::::::::'+windowGuidelineIdSet);
        
        String query = '';
        
        if( titleIdSet != NULL && titleIdSet.size() > 0 ){
            query = 'Select Id, Name, Status__c,  Start_Date__c, Start_Date_Text__c, End_Date__c, End_Date_Text__c, Outside_Date__c, Tracking_Date__c, EAW_Window_Guideline__c, EAW_Plan__c, EAW_Title_Attribute__c, Projected_Start_Date__c, Projected_End_Date__c, Projected_Outside_Date__c, Projected_Tracking_Date__c, Manual_Tracking_Date_Overridden__c, Manual_Start_Date_Overridden__c, Manual_Outside_Date_Overridden__c, Manual_End_Date_Overridden__c  From EAW_Window__c Where EAW_Window_Guideline__c IN : windowGuidelineIdSet AND EAW_Title_Attribute__c IN : titleIdSet AND Status__c != \'Inactive\'';
        } else {
            query = 'Select Id, Name, Status__c, Start_Date__c, Start_Date_Text__c,  End_Date__c, End_Date_Text__c, Outside_Date__c, Tracking_Date__c, EAW_Window_Guideline__c, EAW_Plan__c, EAW_Title_Attribute__c, Projected_Start_Date__c, Projected_End_Date__c, Projected_Outside_Date__c, Projected_Tracking_Date__c, Manual_Tracking_Date_Overridden__c, Manual_Start_Date_Overridden__c, Manual_Outside_Date_Overridden__c, Manual_End_Date_Overridden__c  From EAW_Window__c Where EAW_Window_Guideline__c IN : windowGuidelineIdSet AND EAW_Title_Attribute__c != NULL AND Status__c != \'Inactive\'';
        }
        return Database.getQueryLocator(query);
    }

    
    public void execute(Database.BatchableContext BC, list<sObject> windowRecList){
        
        System.debug(':::::::: windowRecList :::::::::'+windowRecList.size() );
        
        Set<Id> wgIdSet = new Set<Id>();
        Set<Id> titleIdSetTemp = new Set<Id>();
        
        if( windowRecList != NULL && windowRecList.size() > 0 ) {
        
            for( EAW_Window__c window : (List<EAW_Window__c>) windowRecList ) {
                if( window.EAW_Window_Guideline__c != NULL ) wgIdSet.add(window.EAW_Window_Guideline__c);
                if( window.EAW_Title_Attribute__c != NULL ) titleIdSetTemp.add(window.EAW_Title_Attribute__c);
            }
            
            if( titleIdSetTemp != NULL ) {
                if( titleIdSet == NULL ) titleIdSet = new Set<Id>();
                titleIdSet.addAll(titleIdSetTemp);
            }
            
            System.debug('::::::: wgIdSet ::::::::::'+wgIdSet.size());
            System.debug('::::::: titleIdSetTemp ::::::::::'+titleIdSetTemp.size());
            
            System.debug('::::::: Heap sizze in batch::::::::::'+LIMITS.getHeapSize()); 
            
            if( wgIdSet != NULL && wgIdSet.size() > 0 ) {
                
                EAW_NWGRuleBuilderCalculationController windowDateCalcLogic = new EAW_NWGRuleBuilderCalculationController();
                
                //LRCC-1863 - add allowWindowDateChange
                System.debug('::::::: allowWindowDateChange::::::::::'+allowWindowDateChange);
                
                windowDateCalcLogic.executeWGDateCalculationsFromBatchExecution(wgIdSet, titleIdSetTemp,allowWindowDateChange, restrictWindowStatusChange);
                
                System.debug('::::::: Heap sizze in batch::::::::::'+LIMITS.getHeapSize()); 
                System.debug('::::::: calculation initiated::::::::::');

            }
        
        }
        
    }
    
    
    public void finish(Database.BatchableContext BC){
        
        
        EAW_DownstreamRuleGuidelinesFindUtil dependentGuidelineFindUtil = new EAW_DownstreamRuleGuidelinesFindUtil();
        
        Set<Id> releaseDateGuidelineIdSetToProcess = new Set<Id>();
        Set<Id> windowGuidelinesIdToProcess = new Set<Id>();
        
        if( releaseDateGuidelineIdSet != NULL && releaseDateGuidelineIdSet.size() > 0 ) {
            releaseDateGuidelineIdSetToProcess = dependentGuidelineFindUtil.findDependentReleaseDateGuidelineToProcess(releaseDateGuidelineIdSet);
        }
        
        if( windowGuidelineIdSet != NULL && windowGuidelineIdSet.size() > 0 ) {
            windowGuidelinesIdToProcess = dependentGuidelineFindUtil.findDependentWindowGuidelineToProcess(windowGuidelineIdSet);
            windowGuidelinesIdToProcess.removeAll(windowGuidelineIdSet); // This logic is to remove the same window guideline process again if it was self referneced in the rules
        }
        
        System.debug('::::::: releaseDateGuidelineIdSetToProcess ::::::::::'+releaseDateGuidelineIdSetToProcess);
        System.debug('::::::: windowGuidelinesIdToProcess  ::::::::::'+windowGuidelinesIdToProcess);
        System.debug('::::::: windowGuidelineIdSet ::::::::::'+windowGuidelineIdSet);
        System.debug('::::::: titleIdSet ::::::::::'+titleIdSet);
        
        
        if( releaseDateGuidelineIdSetToProcess != NULL && releaseDateGuidelineIdSetToProcess.size() > 0 ){
            
            EAW_RDGDateCalculationBatchClass releaseDateBatch = new EAW_RDGDateCalculationBatchClass(releaseDateGuidelineIdSetToProcess);
            releaseDateBatch.titleIdSet = titleIdSet;
            releaseDateBatch.windowGuidelineIdSet = windowGuidelinesIdToProcess;
            ID batchprocessid = Database.executeBatch(releaseDateBatch, 200);
            
        } else if( windowGuidelinesIdToProcess != NULL && windowGuidelinesIdToProcess.size() > 0 ){
            EAW_WGDateCalculationBatchClass windowDateBatch = new EAW_WGDateCalculationBatchClass(windowGuidelinesIdToProcess);
            windowDateBatch.releaseDateGuidelineIdSet = NULL;
            windowDateBatch.titleIdSet = titleIdSet;
            //LRCC-1863
            windowDateBatch.allowWindowDateChange = allowWindowDateChange ;
            //windowDateBatch.windowGuidelineIdSet = windowGuidelinesIdToProcess;
            ID batchprocessid = Database.executeBatch(windowDateBatch, 200);
        } else {
            EAW_ReleaseDate_API_BatchProcess rdBatch = new EAW_ReleaseDate_API_BatchProcess();
            ID batchprocessid = Database.executeBatch(rdBatch, 100);
        }
    } 



}