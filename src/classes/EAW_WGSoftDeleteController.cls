public class EAW_WGSoftDeleteController{
   
    public EAW_Window_Guideline__c wg {get;set;}
    public List <EAW_Window_Guideline__c > wgList {get;set;}
    public String wgListString {get;set;}
    public String wgId {get;set;}
   
    public EAW_WGSoftDeleteController(ApexPages.StandardController controller) {
    
        wgList = new List <EAW_Window_Guideline__c > ();     
        wg = (EAW_Window_Guideline__c )controller.getRecord();
       
        if(wg != NULL){
            wgId =wg.Id;
            wgList = [SELECT Soft_Deleted__c,Name,Id
                     FROM EAW_Window_Guideline__c  
                     WHERE Id =: wg.Id];  
            System.debug('wgList ::::::::'+wgList );
            //wgList.add(wg);
            wgListString  = JSON.serialize(wgList);
        }
    }
}