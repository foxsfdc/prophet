@RestResource(urlMapping='/SFConsumerForFoxipediaChangeEvent/Overview')
global with sharing class EAW_FoxipediaChangeEventController {
   
    @HttpPost
    global static String doPost() {
        
        String message = 'Success';
        RestResponse res = RestContext.response;
        String requestBody = RestContext.request.requestBody.toString(); 
        System.debug('requestBody ::::'+requestBody);       
        String decoded = EncodingUtil.urlDecode(RestContext.request.requestBody.toString(), 'UTF-8');
        EAW_Create_OverviewAndDetail_Wrapper.Wrapper wrpRes = (EAW_Create_OverviewAndDetail_Wrapper.Wrapper) System.JSON.deserialize(decoded,EAW_Create_OverviewAndDetail_Wrapper.Wrapper.class);
       
        System.debug('reqBody::::'+wrpRes );
        String changedEntity = '';
        String foxId = '';
        if(wrpRes != NULL && wrpRes.Data != NULL &&  wrpRes.Data.Payload.Change != NULL){
        	changedEntity = wrpRes.Data.Payload.ChangedEntity;
            System.debug('changedEntity: '+changedEntity);
            if(wrpRes.Data.Payload.Change != NULL && wrpRes.Data.Payload.Change.foxId != NULL){
	            foxId =wrpRes.Data.Payload.Change.foxId ;
	            System.debug('foxId: '+foxId);
            }
        }
		if(!String.isblank(changedEntity) && !String.isblank(foxId)){
			List<String> statusToProcess = new List<String>{'Failed','Need to Process'};
			List<EAW_Inbound_Notifications__c> eawInList = [select FOX_ID__c,Message_Id__c from EAW_Inbound_Notifications__c where (FOX_ID__c =:foxId) and Status__c IN :statusToProcess];
			//EAW_Inbound_Notifications__c eawIN = EAW_Inbound_Notifications__c.getInstance(wrpRes.Data.Payload.MessageID);
			if(eawInList==null || eawInList.size()==0){
				EAW_Inbound_Notifications__c eawIN = new EAW_Inbound_Notifications__c(Message_Id__c=wrpRes.Data.Payload.MessageID,Source_System__c='Foxipedia',FOX_ID__c=foxId,/*Created_Date__c=Datetime.now(),*/Changed_Entity__c=changedEntity);
				insert eawIN;
				/*
	            EAW_FoxipediaRestAPIReqController efarcJob = new EAW_FoxipediaRestAPIReqController(changedEntity,foxId,eawIN);
				//efarcJob.getAllTitleDetails();
				ID jobID = System.enqueueJob(efarcJob);
				*/
	            message = 'Success, Overview MessageID: '+wrpRes.Data.Payload.MessageID+' changedEntity: '+changedEntity+' foxId: '+foxId;
	            system.debug(message);
	            eawIN.Message__c=message;
	            //eawIN.Job_Id__c=''+jobID;
	            update eawIN;
			}else{
				message = 'Currently '+foxId+' Title is in Queue for processing,  MessageID: '+wrpRes.Data.Payload.MessageID;
			}
		}else {
            message = 'No change entity:::';
            System.debug('No change entity:::');
        }
		return message;
    } 
}