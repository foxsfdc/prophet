public without sharing class EAW_GenericTypeaheadMultiselect {
    
    @AuraEnabled
    public static List<EAW_LookupSearchResult> search(String genericType, String genericName, String searchTerm, Boolean isWGstrandNewOverride) {//LRCC-1592 isWGstrandNewOverride is added
    
        List<SObject> searchResults = new List<SObject>();
        List<EAW_LookupSearchResult> lookupSearchResults = new List<EAW_LookupSearchResult>();
        
        try {
            //LRCC-1617
            if(!String.isblank(searchTerm)){
                searchTerm = String.escapeSingleQuotes(searchTerm);//Add escape character to single quotation
            }
            String query;
            
            Schema.SObjectType SObjectTypeObj = Schema.getGlobalDescribe().get(genericType);
            
            if(SObjectTypeObj == null) {
                throw new AuraHandledException(genericType + ' Custom Settings not found');    
            } 
            else if(genericType == 'EAW_Tag__c' && genericName != null) {
                query = 'SELECT Id, Name FROM ' + SObjectTypeObj + ' WHERE Tag_Type__c = \'' + genericName + '\'';
                if(String.isNotBlank(searchTerm)) {
                    query += ' AND Name LIKE \'%' + searchTerm + '%\'';
                }
                query += ' ORDER BY Name ASC LIMIT 2000';
                searchResults = Database.query(query); 
            }
            else if(genericType == 'EAW_Customer__c') {
                query = 'SELECT Id, Name, Customer_Id__c FROM ' + SObjectTypeObj;
                if(String.isNotBlank(searchTerm)) {
                    query += ' WHERE Name LIKE \'%' + searchTerm + '%\'';
                }
                query += ' ORDER BY Name ASC LIMIT 2000';
                searchResults = Database.query(query);
            } 
            else if(genericType == 'EDM_GLOBAL_TITLE__c') {
                query = 'SELECT Id, Name, FIN_PROD_ID__c FROM ' + SObjectTypeObj;
                if(String.isNotBlank(searchTerm)) {
                    query += ' WHERE Name LIKE \'%' + searchTerm + '%\'';
                }
                query += ' ORDER BY Name ASC LIMIT 2000';
                searchResults = Database.query(query);
            } 
            else if(genericType == 'EAW_Plan_Guideline__c') {
                
                //LRCC-1280
                query = 'SELECT Id, Name, Version__c, Status__c FROM ' + SObjectTypeObj;
                
                if(genericName != null && genericName == 'planWithoutAnyCondition') {
                    if(String.isNotBlank(searchTerm)) {
                        query += ' WHERE Next_Version__c  = null AND Name LIKE \'%' + searchTerm + '%\'';
                    } else{
                        query += ' WHERE Next_Version__c  = null'; // LRCC - 1483 
                    }
                } else {
                    query += ' WHERE Status__c = \'Active\''; 
                    if(String.isNotBlank(searchTerm)) {
                        query += ' AND Name LIKE \'%' + searchTerm + '%\'';
                    }
                }
                query += ' ORDER BY Name ASC LIMIT 2000';
                searchResults = Database.query(query);
            } 
            else if(genericType == 'EAW_Window_Guideline__c') {
                
                List<EAW_Window_Guideline__c> tempList = new List<EAW_Window_Guideline__c> ();
                Set<String> tempSet = new Set<String> ();
                
                if(genericName != null && genericName == 'windowWithoutAnyCondition') {   // LRCC - 1484 
                    query = 'SELECT Id, Name, Window_Guideline_Alias__c, Status__c FROM ' + SObjectTypeObj;
                    query += ' WHERE Next_Version__c  = null AND Window_Guideline_Alias__c != null'; 
                    if(String.isNotBlank(searchTerm)) {
                        query += ' AND Name LIKE \'%' + searchTerm + '%\'';
                    } 
                } else if(isWGstrandNewOverride == TRUE) { //LRCC-1592
                    query = 'SELECT Id, Name FROM ' + SObjectTypeObj + ' WHERE Status__c = \'Draft\''; 
                    if(String.isNotBlank(searchTerm)) {
                        query += ' AND Name LIKE \'%' + searchTerm + '%\'';
                    }
                } else if(isWGstrandNewOverride == false) { //LRCC-1747
                    query = 'SELECT Id, Window_Guideline_Alias__c, Status__c FROM ' + SObjectTypeObj + ' WHERE Status__c = \'Active\'';
                    query += ' AND Next_Version__c  = null AND Window_Guideline_Alias__c != null'; 
                    if(String.isNotBlank(searchTerm)) {
                        query += ' AND Name LIKE \'%' + searchTerm + '%\'';
                    }
                } else {
                    query = 'SELECT Id, Name, Window_Guideline_Alias__c, Status__c FROM ' + SObjectTypeObj + ' WHERE Status__c = \'Active\'';
                    query += ' AND Next_Version__c  = null AND Window_Guideline_Alias__c != null'; 
                    if(String.isNotBlank(searchTerm)) {
                        query += ' AND Name LIKE \'%' + searchTerm + '%\'';
                    }
                }
                query += ' ORDER BY Name ASC LIMIT 2000';
                
                tempList = Database.query(query); 
                
                //LRCC-1592
                if(isWGstrandNewOverride == TRUE) {
                    searchResults = tempList;
                } else {
                    for(EAW_Window_Guideline__c wg: tempList) {
                        
                        if(!tempSet.contains(wg.Window_Guideline_Alias__c)) {
                            
                            tempSet.add(wg.Window_Guideline_Alias__c);
                            searchResults.add(wg);
                        }
                    }
                }
            } 
            else if(genericType == 'EAW_Title__c') {
                //1095 & 1250-Replace Title EDM with Title Attribute
                //query = 'SELECT Id, Name, Title_EDM__c, FIN_PROD_ID__c FROM ' + SObjectTypeObj + ' WHERE Title_EDM__c != NULL';
                query = 'SELECT Id, Name, FIN_PROD_ID__c FROM ' + SObjectTypeObj;
                
                if(String.isNotBlank(searchTerm)) {
                    //LRCC-1617
                    query += ' WHERE (Name LIKE \'%' + searchTerm + '%\'  OR FIN_PROD_ID__c LIKE \'%' + searchTerm + '%\')'; // LRCC - 1521
                }
                
                query += ' ORDER BY Name ASC LIMIT 2000'; 
                System.debug('query ::::title:::'+query );
                searchResults = Database.query(query); 
            } 
            else if(genericType == 'EAW_Window__c') {
                query = 'SELECT Id, Name FROM ' + SObjectTypeObj + ' WHERE Status__c != \'Inactive\'';
                if(String.isNotBlank(searchTerm)) {
                    query += ' AND Name LIKE \'%' + searchTerm + '%\'';
                }
                query += ' ORDER BY Name ASC LIMIT 2000';
                searchResults = Database.query(query);
            }
            else if(genericType == 'EDM_REF_DIVISION__c') {
                query = 'SELECT Id, Name FROM ' + SObjectTypeObj;
                if(String.isNotBlank(searchTerm)) {        
                    query += ' WHERE Name LIKE \'%' + searchTerm + '%\'';
                }
                query += ' ORDER BY Name ASC LIMIT 2000';
                searchResults = Database.query(query);
            }
            else if(genericType == 'EDM_REF_PRODUCT_TYPE__c') {
                query = 'SELECT Id, Name FROM ' + SObjectTypeObj;
                if(String.isNotBlank(searchTerm)) {        
                    query += ' WHERE Name LIKE \'%' + searchTerm + '%\'';
                }
                query += ' ORDER BY Name ASC LIMIT 2000';
                searchResults = Database.query(query);
            }
            else if(genericType == 'EAW_Territories__c' || genericType  == 'EAW_Languages__c') {
                // LRCC:1232 start
                query = 'SELECT Id, Name__c FROM ' + SObjectTypeObj;
                if(String.isNotBlank(searchTerm)) {    
                    query += ' WHERE Name__c LIKE \'%' + searchTerm + '%\'';
                }
                query += ' ORDER BY Name ASC LIMIT 2000';
                searchResults = Database.query(query);
                // LRCC:1232 end
            } else if( genericType  == 'EAW_Release_Date_Type__c' ) {
                query = 'SELECT Id, Name FROM ' + SObjectTypeObj;
                if(String.isNotBlank(searchTerm)) {    
                    query += ' WHERE Name LIKE \'%' + searchTerm + '%\'';
                }
                query += ' ORDER BY Name ASC LIMIT 2000';
                searchResults = Database.query(query);
            } else {
                // LRCC:1232 start
                query = 'SELECT Id, Name FROM ' + SObjectTypeObj;
                if(String.isNotBlank(searchTerm)) {    
                    query += ' WHERE Name LIKE \'%' + searchTerm + '%\'';
                }
                query += ' ORDER BY Name ASC LIMIT 2000';
                searchResults = Database.query(query);
                // LRCC:1232 end
            }
            if(searchResults != NULL && searchResults.size() > 0) {
                
                for(SObject result : (List<SObject>)searchResults) {
                
                    if(genericType == 'EAW_Customer__c') {
                        lookupSearchResults.add(new EAW_LookupSearchResult(String.valueOf(result.get('Id')), genericType, 'standard:account', String.valueOf(result.get('Name')), String.valueOf(result.get('Customer_Id__c'))));
                    } 
                    else if(genericType == 'EDM_GLOBAL_TITLE__c') {
                        lookupSearchResults.add(new EAW_LookupSearchResult(String.valueOf(result.get('Id')), genericType, 'standard:account', String.valueOf(result.get('Name')), String.valueOf(result.get('FIN_PROD_ID__c'))));
                    } 
                    else if(genericType == 'EAW_Title__c') {
                        //1095 & 1250-Replace Title EDM with Title Attribute
                        //lookupSearchResults.add(new EAW_LookupSearchResult(String.valueOf(result.get('Id')), genericType, 'standard:account', String.valueOf(result.get('Name')), String.valueOf(result.getSObject('Title_EDM__r').get('FIN_PROD_ID__c'))));
                        lookupSearchResults.add(new EAW_LookupSearchResult(String.valueOf(result.get('Id')), genericType, 'standard:account', String.valueOf(result.get('Name')), String.valueOf(result.get('FIN_PROD_ID__c'))));
                    } 
                    else if(genericType == 'EAW_Plan_Guideline__c') {
                        //LRCC-1588
                        String pgNameAlias = String.valueOf(result.get('Name')); //LRCC-1483+'-'+ String.valueOf(result.get('Version__c')) +'-'+String.valueOf(result.get('Status__c'));
                        if(genericName != null && genericName == 'planWithoutAnyCondition') {
                            lookupSearchResults.add(new EAW_LookupSearchResult(String.valueOf(result.get('Id')), genericType, 'standard:account', String.valueOf(result.get('Name')), null));
                        } else{
                            lookupSearchResults.add(new EAW_LookupSearchResult(String.valueOf(result.get('Id')), genericType, 'standard:account', pgNameAlias, null)); // LRCC - 1483
                        }                    
                    } 
                    else if(genericType == 'EAW_Territories__c' || genericType  == 'EAW_Languages__c') {
                        lookupSearchResults.add(new EAW_LookupSearchResult(String.valueOf(result.get('Id')), genericType, 'standard:account', String.valueOf(result.get('Name__c')), null));
                    } 
                    else if(genericType  == 'EAW_Window_Guideline__c') { //LRCC-1616
                        
                        //LRCC-1592
                        if(isWGstrandNewOverride == TRUE) {
                            lookupSearchResults.add(new EAW_LookupSearchResult(String.valueOf(result.get('Id')), genericType, 'standard:account', String.valueOf(result.get('Name')), null));                            
                        } else {
                            lookupSearchResults.add(new EAW_LookupSearchResult(String.valueOf(result.get('Id')), genericType, 'standard:account', String.valueOf(result.get('Window_Guideline_Alias__c')), null));
                        }
                    }
                    else {
                        lookupSearchResults.add(new EAW_LookupSearchResult(String.valueOf(result.get('Id')), genericType, 'standard:account', String.valueOf(result.get('Name')), null));
                    }
                }
            }
        } catch(Exception e) {
            throw new AuraHandledException('Exception:' + e.getMessage());    
        }
        return lookupSearchResults; 
    }
}