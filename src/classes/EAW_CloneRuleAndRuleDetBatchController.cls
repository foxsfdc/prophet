global class EAW_CloneRuleAndRuleDetBatchController implements Database.Batchable < sObject > , Database.stateful {

    global List < EAW_Window__c > newWindows = new List < EAW_Window__c > ();

    global EAW_CloneRuleAndRuleDetBatchController(List < EAW_Window__c > windows) {
        System.debug('::::::wIdSet::clone rule ::' + windows);
        newWindows = windows;
        System.debug('::::::newWindows ::clone rule::' + newWindows);
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {

        String query = 'SELECT Id, EAW_Window_Guideline__c FROM EAW_Window__c WHERE Id IN: newWindows';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List < sObject > scope) {

        System.debug(':::scope:clone ruleh::::::::' + scope);
        System.debug(':::scope::lenght:' + scope.size());

        Map < Id, Set < String >> windowIdDatesToModify = new Map < Id, Set < String >> ();
        List < EAW_Rule__c > rulesToDelete = new List < EAW_Rule__c > ();
        List < EAW_Window__c > windowsToProcess = new List < EAW_Window__c > ();

        windowsToProcess = scope;

        for (EAW_Rule__c r: [SELECT Id, Rule_Overridden__c, Date_To_Modify__c, Window__c FROM EAW_Rule__c WHERE Window__c IN: windowsToProcess]) {

            if (r.Rule_Overridden__c == false) {
                rulesToDelete.add(r);
            } 
            else {
                if (!windowIdDatesToModify.containsKey(r.Window__c)) {
                    windowIdDatesToModify.put(r.Window__c, new Set < String > ());
                }
                windowIdDatesToModify.get(r.Window__c).add(r.Date_To_Modify__c);
            }
        }

        if (rulesToDelete != null && rulesToDelete.size() > 0) {
            delete rulesToDelete;
        }

        // Commented by LRCC - 1259 since we don't need any Rules at window level.
        //EAW_Window_Handler reHandler = new EAW_Window_Handler();
       // reHandler.cloneRuleAndRuleDetailsFromWGL(windowsToProcess, windowIdDatesToModify);
    }

    global void finish(Database.BatchableContext BC) {

        //LRCC-1653 - -In this ticket once passed QA it with remove the commented code.
        /*System.debug('deleteWindowStarndWindowIds:::window finish:::' + newWindows);
        EAW_WindowStrandDeleteBatchController ewsdbc = new EAW_WindowStrandDeleteBatchController(newWindows);
        Integer bcount = Integer.valueOf(Label.windowStrandDeleteBatchCount);
        System.debug('::::::bcount ::::' + bcount);
        database.executeBatch(ewsdbc, bcount);*/
        
    }
}