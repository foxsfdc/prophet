public with sharing class EAW_FutureToInvokeQueueableWGCalculation {
    
    @future 
    public static void invokeWindowGuidelineCalcQueueableLogic( Set<Id> windowGuielineRecordIdSet, Set<Id> filteredTitleIdSet ) {
  
        EAW_QWGRuleBuilderCalculationController wg =  new EAW_QWGRuleBuilderCalculationController();
        wg.windowGuielineRecordIdSet = windowGuielineRecordIdSet;
        wg.filteredTitleIdSet = filteredTitleIdSet;
        //String jobId = System.enqueueJob(wg);
    }
}