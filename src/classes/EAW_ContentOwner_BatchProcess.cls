global class EAW_ContentOwner_BatchProcess implements Database.Batchable<sObject>, Database.Stateful,Schedulable {
    
    // instance member to retain state across transactions
	global Set<id> RdgIds = new Set<id>();
	global Set<id> wgIds = new Set<id>();
	global Set<id> titleIdSet = new Set<id>();
	 
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(
            'select Content_Owner_Codes__c,Content_Owner_Descriptions__c,FOX_ID__c,LastModifiedDate,Source_System__c,Status__c from EAW_Inbound_Notifications__c where Source_System__c=\'Content Owner\' and Status__c not in (\'Success\',\'Processing\') ORDER BY CreatedDate ASC NULLS FIRST'
            //'select Content_Owner_Codes__c,Content_Owner_Descriptions__c,FOX_ID__c,LastModifiedDate,Source_System__c,Status__c from EAW_Inbound_Notifications__c where Source_System__c=\'Content Owner\' and Status__c not in (\'Success\',\'Processing\') ORDER BY CreatedDate ASC NULLS FIRST'
        );
    }
    global void execute(Database.BatchableContext bc, List<EAW_Inbound_Notifications__c> scope){
        // process each batch of records
        try{
        	for (EAW_Inbound_Notifications__c eawIN : scope) {
	            eawIN.status__c='Processing';
	        }
	        update scope;
	        checkRecursiveData.bypassDateCalculation=true;//To bypass recursive calculation of downstream release dates
	        EAW_ContentOwnerDataController cod = new EAW_ContentOwnerDataController();
	        cod.processContentOwners(scope);
	        if(cod.wgIds!=null && !cod.wgIds.isempty()){
				wgIds.addAll(cod.wgIds);
			}
			if(cod.rdgIds!=null && !cod.rdgIds.isempty()){
				rdgIds.addAll(cod.rdgIds);
			}
			if(cod.titleIdSet!=null && !cod.titleIdSet.isempty()){
				titleIdSet.addAll(cod.titleIdSet);
			}
	        for (EAW_Inbound_Notifications__c eawIN : scope) {
	            eawIN.status__c='Success';
	        }
	        update scope;
	        //System.debug('EAW_ContentOwner_BatchProcess titleIdSet: '+System.JSON.serialize(titleIdSet));
	        //System.debug('EAW_ContentOwner_BatchProcess rdgIds: '+System.JSON.serialize(rdgIds));
	        //System.debug('EAW_ContentOwner_BatchProcess wgIds: '+System.JSON.serialize(wgIds));
	    }catch(exception e){
			system.debug('Exception in EAW_ContentOwner_BatchProcess: '+e.getMessage());
			String message = e.getMessage();
    		if(message!=null && message.length()>10000)message=message.substring(0, 10000);
    		for (EAW_Inbound_Notifications__c eawIN : scope) {
	    		eawIN.Message__c=message;
	    		eawIN.Status__c='Failed';
    		}
    		upsert scope;
		}
    }    
    global void finish(Database.BatchableContext bc){
        //System.debug('EAW_ContentOwner_BatchProcess finish titleIdSet: '+System.JSON.serialize(titleIdSet));
        //System.debug('EAW_ContentOwner_BatchProcess finish rdgIds: '+System.JSON.serialize(rdgIds));
        //System.debug('EAW_ContentOwner_BatchProcess finish wgIds: '+System.JSON.serialize(wgIds));
        if(titleIdSet!=null && !titleIdSet.isEmpty()){
        	if(rdgIds!=null && !rdgIds.isEmpty() ){
	    		EAW_RDGDateCalculationBatchClass releaseDateBatch = new EAW_RDGDateCalculationBatchClass(rdgIds);
	            releaseDateBatch.titleIdSet = titleIdSet;
	            if(wgIds!=null && !wgIds.isempty()){
	            	releaseDateBatch.windowGuidelineIdSet = wgIds;
	            }else{
	            	releaseDateBatch.windowGuidelineIdSet = null;
	            }
	            ID batchprocessid = Database.executeBatch(releaseDateBatch, 200);
	    		system.debug('EAW_ContentOwner_BatchProcess EAW_RDGDateCalculationBatchClass batchprocessid: '+batchprocessid);
        	}else if(wgIds!=null && !wgIds.isEmpty()){
        		EAW_WGDateCalculationBatchClass windowDateBatch = new EAW_WGDateCalculationBatchClass(wgIds);
                windowDateBatch.releaseDateGuidelineIdSet = NULL;
                windowDateBatch.titleIdSet = titleIdSet;
                ID batchprocessid = Database.executeBatch(windowDateBatch, 200);
                system.debug('EAW_ContentOwner_BatchProcess EAW_WGDateCalculationBatchClass batchprocessid: '+batchprocessid);
        	}
        }
    }
    
    public void execute(SchedulableContext sc) {
        
        EAW_ContentOwner_BatchProcess coBatch = new EAW_ContentOwner_BatchProcess();
        Database.executeBatch(coBatch,10);//size should be ten, since we may hit too many dml rows exception,apex heap size
    }    
}