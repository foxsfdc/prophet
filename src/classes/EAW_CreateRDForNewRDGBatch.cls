public class EAW_CreateRDForNewRDGBatch implements Database.Stateful,Database.Batchable<sObject>{

    public Set<Id> releaseDateGuidelineIdSet = new Set<Id>();
    public Set<Id> windowGuidelineIdSet = new Set<Id>();
    public Set<Id> titleIdSet = new Set<Id>();
    Set<Id> releaseDateIdSet = new Set<Id>();
    Set<Id> rdgSetToCreateReleaseDateRecord = new Set<Id>();
    Map<Id, Set<Id>> rdgAndParentRDGMap = new Map<Id, Set<Id>>();
    Set<Id> parentIdSet = new Set<Id>();
    
    Map<Id, Set<Id>> rdgTitleIdMap;
    
    
    public EAW_CreateRDForNewRDGBatch( Set<Id> rdgIdSet, Set<Id> titleIds ){
        
        System.debug(':::::::::: rdgIdSet :::::::::::::'+rdgIdSet);
        
        rdgTitleIdMap = new Map<Id, Set<Id>>();
        
        if( titleIds != NULL && titleIds.size() == 1 ) {
            
            //This logic used to create the Release date record for the downstream guideline when it was manually added to the upstream guideline
            
            List<EAW_Release_Date_Guideline__c> rdgList = [ Select Id, ( Select Id, Release_Date_Guideline__c,Title__c FROM Release_Dates__r Where Title__c IN : titleIds ) From EAW_Release_Date_Guideline__c Where Id IN : rdgIdSet ];
            
            for( EAW_Release_Date_Guideline__c rdgRec : rdgList ) {
                Set<Id> tempIdSet = new Set<Id>();
                tempIdSet.addAll(titleIds);
                for( EAW_Release_Date__c rdRec : rdgRec.Release_Dates__r ) {
                    tempIdSet.remove(rdRec.Title__c);
                }
                rdgTitleIdMap.put(rdgRec.Id, tempIdSet);
            }
            
            for( Id rdgId : rdgTitleIdMap.keySet() ) {
                if( rdgTitleIdMap.containsKey(rdgId) && rdgTitleIdMap.get(rdgId).size() > 0 ) {
                    rdgSetToCreateReleaseDateRecord.add(rdgId);
                }
            }
            
        } else {
            
            List<EAW_Release_Date_Guideline__c> rdgList = [ Select Id, ( Select Id, Release_Date_Guideline__c FROM Release_Dates__r Limit 1 ) From EAW_Release_Date_Guideline__c Where Id IN : rdgIdSet ];
            
            System.debug(':::::::::: rdgList :::::::::::::'+rdgList);
            
            rdgSetToCreateReleaseDateRecord.addAll(rdgIdSet);
            
            if( rdgList != NULL && rdgList.size() > 0 ) {
                
                for( EAW_Release_Date_Guideline__c rdgRec : rdgList ) {
                
                    for( EAW_Release_Date__c rdRec : rdgRec.Release_Dates__r ) {
                        rdgSetToCreateReleaseDateRecord.remove(rdRec.Release_Date_Guideline__c);
                    }
                }
            }
        }
        
        System.debug(':::::::::: rdgSetToCreateReleaseDateRecord :::::::::::::'+rdgSetToCreateReleaseDateRecord);
            
        if( rdgSetToCreateReleaseDateRecord != NULL && rdgSetToCreateReleaseDateRecord.size() > 0 ) {
            
            List<EAW_Rule__c> ruleRecordList = [ Select Id, Release_Date_Guideline__c, Conditional_Operand_Id__c, ( Select Id, Conditional_Operand_Id__c From Rule_Orders__r ) From EAW_Rule__c Where Release_Date_Guideline__c IN : rdgSetToCreateReleaseDateRecord ];            
            
            if( ruleRecordList != NULL && ruleRecordList.size() > 0 ) {
            
                for( EAW_Rule__c ruleRecord : ruleRecordList ){
                    
                    Set<Id> parentRDGIdSet = new Set<Id>();
                    
                    if( String.isNotBlank(ruleRecord.Conditional_Operand_Id__c) ) {
                        parentRDGIdSet.addAll((List<Id>)ruleRecord.Conditional_Operand_Id__c.split('\\;'));
                    }
                    
                    for( EAW_Rule_Detail__c ruleDetail : ruleRecord.Rule_Orders__r ) {
                        if( String.isNotBlank(ruleDetail.Conditional_Operand_Id__c) ) {
                            parentRDGIdSet.addAll((List<Id>)ruleDetail.Conditional_Operand_Id__c.split('\\;'));
                        }
                    }
                    if( parentRDGIdSet != NULL && parentRDGIdSet.size() > 0 ) {
                        rdgAndParentRDGMap.put(ruleRecord.Release_Date_Guideline__c, parentRDGIdSet);
                        parentIdSet.addAll(parentRDGIdSet);
                    }
                }
            
            }
            
        }
        
        System.debug(':::rdgAndParentRDGMap:::' + rdgAndParentRDGMap);
        System.debug(':::parentIdSet:::' + parentIdSet);
    }
    
    
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
    
        System.debug(':::releaseDateGuidelineIdSet:::' + releaseDateGuidelineIdSet);
        system.debug('::titleIdSet::'+titleIdSet);
        System.debug(':::::::::: rdgSetToCreateReleaseDateRecord :::::::::::::'+rdgSetToCreateReleaseDateRecord);
        System.debug(':::::::::: parentIdSet:::::::::::::'+parentIdSet);
        
        String query = '';
        
        Boolean activeFlag = TRUE;
            
        if( parentIdSet != NULL && parentIdSet.size() > 0 ) {
            if( titleIdSet != NULL && titleIdSet.size() == 1 ) {
                query = 'Select Id, Title__c, Status__c, Active__c, Projected_Date__c, Release_Date__c, Feed_Date__c, Manual_Date__c, Release_Date_Guideline__c,Title__r.PROD_TYP_CD_INTL__c,Title__r.PROD_TYP_CD_INTL__r.Name, Send_To_Third_Party__c, Repo_DML_Type__c, Galileo_DML_Type__c FROM EAW_Release_Date__c Where Release_Date_Guideline__c IN : parentIdSet AND Title__c IN : titleIdSet AND Active__c =: activeFlag ORDER BY Release_Date_Guideline__c, Title__c';
            } else {
                query = 'Select Id, Title__c, Status__c, Active__c, Projected_Date__c, Release_Date__c, Feed_Date__c, Manual_Date__c, Release_Date_Guideline__c,Title__r.PROD_TYP_CD_INTL__c,Title__r.PROD_TYP_CD_INTL__r.Name, Send_To_Third_Party__c, Repo_DML_Type__c, Galileo_DML_Type__c FROM EAW_Release_Date__c Where Release_Date_Guideline__c IN : parentIdSet AND Active__c =: activeFlag ORDER BY Release_Date_Guideline__c, Title__c';
            }
        } else {
            query = 'Select Id FROM EAW_Release_Date__c limit 0'; // To skip the execute logic if all the RDG has release date recors under it
        }
        
        System.debug(':::::::::: query :::::::::::::'+query);
        
        if( titleIdSet == NULL ) titleIdSet = new Set<Id>();
        
        System.debug(':::::::::: query :::::::::::::'+Database.Query(query));
        
        return Database.getQueryLocator(query);
    }

    
    public void execute(Database.BatchableContext BC, list<sObject> rdRecList){
        
        System.debug(':::::::::: rdRecList:::::::::::::'+rdRecList);
        
        System.debug(':::::::::: rdgSetToCreateReleaseDateRecord :::::::::::::'+rdgSetToCreateReleaseDateRecord);
        
        List<EAW_Release_Date__c> releaseDateToInsert = new List<EAW_Release_Date__c>();
        
        Set<Id> tempTitleIdSet = new Set<Id>();
        
        if( rdRecList != NULL ){
        
            for(EAW_Release_Date__c rdRec : (List<EAW_Release_Date__c>)rdRecList ) {
                if( rdRec.Title__c != NULL ) {
                    titleIdSet.add(rdRec.Title__c);
                    tempTitleIdSet.add(rdRec.Title__c);
                }
            }
        }
        
        Map<Id, Set<Id>> rdgTitleIdSetMap = new Map<Id, Set<Id>>();
        
        if( rdgSetToCreateReleaseDateRecord != NULL ) {
        
            for(EAW_Release_Date__c rd : [ Select Id, Title__c, Release_Date_Guideline__c FROM EAW_Release_Date__c Where Release_Date_Guideline__c IN : rdgSetToCreateReleaseDateRecord AND Title__c IN : tempTitleIdSet ] ) {
                if( rd.Title__c != NULL ) {
                    Set<Id> tempSet = new Set<Id>();
                    if( rdgTitleIdSetMap.containsKey(rd.Release_Date_Guideline__c) ) tempSet = rdgTitleIdSetMap.get(rd.Release_Date_Guideline__c);
                    tempSet.add(rd.Title__c);
                    rdgTitleIdSetMap.put(rd.Release_Date_Guideline__c, tempSet);
                }
            }
            
            for( Id rdgRecId : rdgSetToCreateReleaseDateRecord ) {
                
                Set<Id> checkRecursiveTitleIdSet = new Set<Id>();
                
                for(EAW_Release_Date__c rdRec : (List<EAW_Release_Date__c>)rdRecList ) {
                    
                    if( rdgAndParentRDGMap.containsKey(rdgRecId) && rdgAndParentRDGMap.get(rdgRecId).contains(rdRec.Release_Date_Guideline__c) && !checkRecursiveTitleIdSet.contains(rdRec.Title__c) ){
                        EAW_Release_Date__c newReleaseDateRec = new EAW_Release_Date__c();
                        newReleaseDateRec.Release_Date_Guideline__c = rdgRecId;
                        newReleaseDateRec.Title__c = rdRec.Title__c;
                        newReleaseDateRec.Active__c = False;
                        releaseDateToInsert.add(newReleaseDateRec);
                        checkRecursiveTitleIdSet.add(rdRec.Title__c);
                    }
                    
                }
            
            }
        }
        System.debug('::::: releaseDateToInsert :::::'+releaseDateToInsert);
        System.debug('::::: releaseDateToInsert Size :::::'+releaseDateToInsert.size());
        if( releaseDateToInsert.size() > 0 ) {
            insert releaseDateToInsert;
        }
    }
    
    
    public void finish(Database.BatchableContext BC){
        
        System.debug('::::: titleIdSet :::::'+titleIdSet);
        System.debug('::::: releaseDateGuidelineIdSet :::::'+releaseDateGuidelineIdSet);
        
        EAW_RDGDateCalculationBatchClass releaseDateBatch = new EAW_RDGDateCalculationBatchClass(releaseDateGuidelineIdSet);
        releaseDateBatch.titleIdSet = titleIdSet;
        releaseDateBatch.windowGuidelineIdSet = windowGuidelineIdSet;
        ID batchprocessid = Database.executeBatch(releaseDateBatch, 200);
        
    }



}