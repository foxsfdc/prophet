global class EAW_WgBatchProcessAfterDataLoad implements Database.Batchable<sObject> {
    
    //This methode is used to find and update the Conditional Opernad Id with SF record Id based on the RDG Name value updated in the conditional operand details
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        // collect the batches of records or objects to be passed to execute
        return Database.getQueryLocator(
            'SELECT ID From EAW_Window_Guideline__c'
        );
        //Where Convert_Condition_Operand_Details__c = TRUE
    }
    
    
    global void execute(Database.BatchableContext bc, List<EAW_Window_Guideline__c> wgList ){
        // process each batch of records
        
        if( wgList != NULL && wgList.size() > 0 ) {
        
            List<EAW_Rule__c> ruleListToUpdate = new List<EAW_Rule__c>();
            List<EAW_Rule_Detail__c> rdListToUpdate = new List<EAW_Rule_Detail__c>();
            
            EAW_Window_Guideline__c wg = wgList[0];
            
            Set<String> wgNameSet = new Set<String>();
            
            Map<String, Id> wgNameIdMap = new Map<String, Id>();
            Map<String, Id> rdgNameIdMap = new Map<String, Id>();
            
            List<EAW_Rule__c> ruleList = [ Select Id,Condition_Operand_Details__c, Conditional_Operand_Id__c, ( Select Id,Condition_Operand_details__c, Conditional_Operand_Id__c  From Rule_Orders__r ) From EAW_Rule__c Where Window_Guideline__c =: wg.Id ];
            
            for( EAW_Rule__c rule : ruleList ) {
            
               if( !String.isBlank(rule.Condition_Operand_Details__c) ) {
                    wgNameSet.addAll(rule.Condition_Operand_Details__c.split(';'));
                }
                
                for( EAW_Rule_Detail__c rd : rule.Rule_Orders__r ) {
                    
                    if( !String.isBlank(rd.Condition_Operand_Details__c) ) {
                        wgNameSet.addAll(rd.Condition_Operand_Details__c.split(';'));
                    }
                
                }
            
            }
            
            System.debug(':::: wgNameSet :::::'+ wgNameSet );
            
            if( wgNameSet != NULL && wgNameSet.size() > 0 ) {
                
                List<EAW_Window_Guideline__c> tempWgList = [ SELECT ID, Name From EAW_Window_Guideline__c Where Name IN : wgNameSet AND Status__c = 'Active' ];
                
                if( tempWgList != NULL && tempWgList.size() > 0 ){
                    
                    for( EAW_Window_Guideline__c tempWG : tempWgList ) {
                        wgNameIdMap.put(tempWG.Name.toUpperCase(), tempWG.Id);
                    }
                
                }
                
                List<EAW_Release_Date_Guideline__c> tempRdgList = [ SELECT ID, Name From EAW_Release_Date_Guideline__c Where Name IN : wgNameSet AND Active__c = TRUE ];
                
                if( tempRdgList != NULL && tempRdgList.size() > 0 ) {
                    
                    for( EAW_Release_Date_Guideline__c tempRDG : tempRdgList ) {
                        rdgNameIdMap.put(tempRDG.Name.toUpperCase(), tempRDG.Id);
                    }
                
                }
                
            }
            
            if( ( wgNameIdMap != NULL && wgNameIdMap.size() > 0 ) || ( rdgNameIdMap != NULL && rdgNameIdMap.size() > 0 ) ) {
            
                for( EAW_Rule__c rule : ruleList ) {
            
                    if( !String.isBlank(rule.Condition_Operand_Details__c) ) {
                        
                        String tempIdStr = '';
                        
                        for( String tempStr : rule.Condition_Operand_Details__c.toUpperCase().split(';') ){
                            
                            Integer tempIdStrLength = tempIdStr.length()+19;
                            
                            System.debug('::::::::tempIdStrLength:::::::::'+tempIdStrLength);
                            if( tempIdStrLength <= 255 ) {
                                if( wgNameIdMap.containsKey(tempStr) ) {
                                    tempIdStr += wgNameIdMap.get(tempStr)+';';
                                }
                                if( rdgNameIdMap.containsKey(tempStr) ) {
                                    tempIdStr += rdgNameIdMap.get(tempStr)+';';
                                }
                            } else {
                                break;
                            }
                        }
                        System.debug('::::::::tempIdStr:::::::::'+tempIdStr);
                        
                        if( !String.isBlank(tempIdStr) ) {
                            tempIdStr = tempIdStr.subString(0,tempIdStr.length()-1);
                            rule.Conditional_Operand_Id__c = tempIdStr;
                            ruleListToUpdate.add(rule);
                        }
                    }
                    
                    for( EAW_Rule_Detail__c rd : rule.Rule_Orders__r ) {
                        
                        if( !String.isBlank(rd.Condition_Operand_Details__c) ) {
                            String tempIdStr = '';
                            
                            for( String tempStr : rd.Condition_Operand_Details__c.toUpperCase().split(';') ){
                                
                                Integer tempIdStrLength = tempIdStr.length()+19;
                            
                                System.debug('::::::::tempIdStrLength:::::::::'+tempIdStrLength);
                                if( tempIdStrLength <= 255 ) {
                                    if( wgNameIdMap.containsKey(tempStr) ) {
                                        tempIdStr += wgNameIdMap.get(tempStr)+';';
                                    }
                                    if( rdgNameIdMap.containsKey(tempStr) ) {
                                        tempIdStr += rdgNameIdMap.get(tempStr)+';';
                                    }
                                } else {
                                    break;
                                }
                            }
                            System.debug('::::::::tempIdStr:::::::::'+tempIdStr);
                            if( !String.isBlank(tempIdStr) ) {
                                tempIdStr = tempIdStr.subString(0,tempIdStr.length()-1);
                                rd.Conditional_Operand_Id__c = tempIdStr;
                                rdListToUpdate.add(rd);
                            }
                        }
                    
                    }
                
                }
                
                System.debug('::::::::ruleListToUpdate:::::::::'+ruleListToUpdate);
                System.debug('::::::::rdListToUpdate:::::::::'+rdListToUpdate);
                
                if( rdListToUpdate != NULL && rdListToUpdate.size() > 0 ) {
                    update rdListToUpdate;
                }
                
                
                if( ruleListToUpdate != NULL && ruleListToUpdate.size() > 0 ) {
                    update ruleListToUpdate;
                }
            
            }
        }
    }    
    
    
    global void finish(Database.BatchableContext bc){
        // execute any post-processing operations
    } 
    

}