public with sharing class EAW_PlanWindowGuidelineController {

    @AuraEnabled
    public static Map<String, List<EAW_Window_Guideline__c>> removeSelectedJunctions(List<EAW_Window_Guideline__c> windowGuidelines, String planId) {
    
         try {    
            List<String> windowGuidelineIds = new List<String>();
            List<String> invalidWindowGuidelineIds = new List<String>();
            Boolean flag;
            
            List<EAW_Window__c> windowsToBeSoftDelete = new List<EAW_Window__c>();
            Set<Id> softDeletedWindowGuidelineId = new Set<Id>();
            Set<Id> softDeletedWindowTitleId = new Set<Id>();
            
            for(EAW_Window_Guideline__c windowGuideline : windowGuidelines) {
                windowGuidelineIds.add(windowGuideline.Id);
            }
            
            List<EAW_Plan_Window_Guideline_Junction__c> planWindowGuidelineJunctions =
                    [SELECT Plan__c, Window_Guideline__c FROM EAW_Plan_Window_Guideline_Junction__c WHERE Window_Guideline__c IN :windowGuidelineIds /*LRCC-1680: After discussed with kumar comment this logic:*/ AND Plan__c = :planId]; // Added Plan for LRCC - 1848
            
            if(planWindowGuidelineJunctions != null && planWindowGuidelineJunctions.size() > 0) {
                delete planWindowGuidelineJunctions;
            }    
            //LRCC-1821
            //commented out to do Soft Delete
            //List<EAW_Window__c> windowsToBeDelete = 
                    //[SELECT Id, Status__c, EAW_Window_Guideline__c FROM EAW_Window__c WHERE EAW_Window_Guideline__c IN :windowGuidelineIds AND Status__c != 'Firm'];
            
            //if(windowsToBeDelete != null && windowsToBeDelete.size() > 0) {
                //delete windowsToBeDelete;
            //}
            
            for (EAW_Window__c window : [SELECT Id, EAW_Title_Attribute__c, EAW_Window_Guideline__c FROM EAW_Window__c WHERE EAW_Window_Guideline__c IN :windowGuidelineIds AND Status__c != 'Firm']) {
                
                EAW_Window__c tempWindow = new EAW_Window__c(Id=window.Id, Soft_Deleted__c='TRUE');
                tempWindow.Galileo_DML_Type__c = 'Delete';
                tempWindow.Repo_DML_Type__c = 'Delete';
                tempWindow.Send_To_Third_Party__c = True;
                //LRCC-1821
                tempWindow.Galileo_RC_DML_Type__c='Delete';
                
                softDeletedWindowGuidelineId.add(window.EAW_Window_Guideline__c);
                softDeletedWindowTitleId.add(window.EAW_Title_Attribute__c);
                windowsToBeSoftDelete.add(tempWindow);
            }
            
            if(windowsToBeSoftDelete != null && windowsToBeSoftDelete.size() > 0) {
                checkRecursiveData.bypassDateCalculation = True;
                update windowsToBeSoftDelete;
                EAW_WGDateCalculationBatchClass windowDateBatch = new EAW_WGDateCalculationBatchClass(softDeletedWindowGuidelineId);
                windowDateBatch.releaseDateGuidelineIdSet = NULL;
                windowDateBatch.titleIdSet = softDeletedWindowTitleId;
                //LRCC-1863
                windowDateBatch.allowWindowDateChange = true;
                //windowDateBatch.windowGuidelineIdSet = softDeletedWindowGuidelineId;
                ID batchprocessid = Database.executeBatch(windowDateBatch, 200);
            }
            
            /**LRCC-1680
            for(String windowGuidelineId : windowGuidelineIds) {
                List<EAW_Plan_Window_Guideline_Junction__c> junctions =
                        [SELECT Plan__c, Window_Guideline__c FROM EAW_Plan_Window_Guideline_Junction__c WHERE Window_Guideline__c = :windowGuidelineId];
            
                if(junctions.size() == 0) {
                    invalidWindowGuidelineIds.add(windowGuidelineId);
                }
            }
            
            List<EAW_Window_Guideline__c> invalidWindowGuidelines = [SELECT Name, Status__c FROM EAW_Window_Guideline__c WHERE id IN :invalidWindowGuidelineIds];
            **/
            Map<String, List<EAW_Window_Guideline__c>> windowGuidelineIdsAndUpdatedWindowGuidelineList = new Map<String, List<EAW_Window_Guideline__c>>();
            windowGuidelineIdsAndUpdatedWindowGuidelineList.put('windowGuidelines', getWindowGuidelines(planId));
            /**LRCC-1680
            windowGuidelineIdsAndUpdatedWindowGuidelineList.put('invalidWindowGuidelines', invalidWindowGuidelines);
            **/
            return windowGuidelineIdsAndUpdatedWindowGuidelineList;
        }
        catch(Exception e) {
            throw new AuraHandledException('Error : ' + e.getMessage());
        }
    }

    @AuraEnabled
    public static List<EAW_Window_Guideline__c> invalidateWindowGuidelines(List<EAW_Window_Guideline__c> windowGuidelines) {
        for(EAW_Window_Guideline__c windowGuideline : windowGuidelines) {
            windowGuideline.Status__c = 'Inactive';
        }

        update windowGuidelines;

        return windowGuidelines;
    }

    @AuraEnabled
    public static EAW_Window_Guideline__c cloneSelectedWindowGuidelines(EAW_Window_Guideline__c oldWindowGuideline, String planId) {
        
        try {
        
            EAW_Window_Guideline__c newWindowGuideline = new EAW_Window_Guideline__c();
            
            newWindowGuideline.Name = oldWindowGuideline.Name + ' clone';
            
            //LRCC-510
            if(oldWindowGuideline.Clone_WG_Name__c != null) {
                newWindowGuideline.Clone_WG_Name__c = oldWindowGuideline.Clone_WG_Name__c + ' clone';
            } 
            else {
                newWindowGuideline.Clone_WG_Name__c = 'clone';
            }
            
            if(oldWindowGuideline.Window_Guideline_Alias__c != null) {
                newWindowGuideline.Window_Guideline_Alias__c = oldWindowGuideline.Window_Guideline_Alias__c;
                newWindowGuideline.Name = oldWindowGuideline.Window_Guideline_Alias__c + '-' + oldWindowGuideline.Product_Type__c + ' ' + newWindowGuideline.Clone_WG_Name__c;
            }
            
            newWindowGuideline.Customers__c = oldWindowGuideline.Customers__c;
            newWindowGuideline.Description__c = oldWindowGuideline.Description__c;
            newWindowGuideline.End_Date__c = oldWindowGuideline.End_Date__c;
            newWindowGuideline.Media__c = oldWindowGuideline.Media__c;
            newWindowGuideline.Outside_Date__c = oldWindowGuideline.Outside_Date__c; 
            newWindowGuideline.Start_Date__c = oldWindowGuideline.Start_Date__c;
            newWindowGuideline.Tracking_Date__c = oldWindowGuideline.Tracking_Date__c;
            newWindowGuideline.Version__c = oldWindowGuideline.Version__c;
            newWindowGuideline.Version_Notes__c = oldWindowGuideline.Version_Notes__c;
            newWindowGuideline.Status__c = 'Draft';
            newWindowGuideline.Window_Type__c = oldWindowGuideline.Window_Type__c;
            newWindowGuideline.Product_Type__c = oldWindowGuideline.Product_Type__c; // LRCC-1154
            
            //LRCC-1429
            newWindowGuideline.Start_Date_Rule__c = oldWindowGuideline.Start_Date_Rule__c;
            newWindowGuideline.End_Date_Rule__c = oldWindowGuideline.End_Date_Rule__c;
            newWindowGuideline.Outside_Date_Rule__c = oldWindowGuideline.Outside_Date_Rule__c;
            newWindowGuideline.Tracking_Date_Rule__c = oldWindowGuideline.Tracking_Date_Rule__c;
            
            insert newWindowGuideline;
            
            /* LRCC-1612: When click clone only create orphan window guidelines.
            --------------------------------------------------------------------
            EAW_Plan_Window_Guideline_Junction__c planWindowGuideline = new EAW_Plan_Window_Guideline_Junction__c();
            planWindowGuideline.Plan__c = planId;
            planWindowGuideline.Window_Guideline__c = newWindowGuideline.Id;
            
            insert planWindowGuideline;
            */
            
            //LRCC-1429 & LRCC-1612.
            EAW_WindowGuidelineCloneController.cloneRuleAndRuleDetailUtil(oldWindowGuideline.Id, newWindowGuideline.Id);
        
            return newWindowGuideline;
        }
        catch(Exception e) {
            throw new AuraHandledException('Error : ' + e.getMessage());
        }
    }

    @AuraEnabled
    public static List<EAW_Window_Guideline__c> addSelectedExistingWindowGuidelines(List<String> windowGuidelineIds, String planId) {
        List<EAW_Plan_Window_Guideline_Junction__c> newPlanWindowGuidelineJunctions = new List<EAW_Plan_Window_Guideline_Junction__c>();

        for(String windowGuidelineId : windowGuidelineIds) {
            EAW_Plan_Window_Guideline_Junction__c planWindowGuideline = new EAW_Plan_Window_Guideline_Junction__c();
            planWindowGuideline.Plan__c = planId;
            planWindowGuideline.Window_Guideline__c = windowGuidelineId;

            newPlanWindowGuidelineJunctions.add(planWindowGuideline);
        }

        insert newPlanWindowGuidelineJunctions;

        return getWindowGuidelines(planId);
    }

    @AuraEnabled
    public static List<EAW_Window_Guideline__c> getWindowGuidelines(String recordId) {
        String junctionQuery = 'SELECT Window_Guideline__c FROM EAW_Plan_Window_Guideline_Junction__c WHERE Plan__c = \'' + recordId + '\'';
        List<EAW_Plan_Window_Guideline_Junction__c> planWindowGuidelineJunctions = Database.query(junctionQuery);
        List<String> windowGuidelineIds = new List<String>();

        for(EAW_Plan_Window_Guideline_Junction__c planWindowGuidelineJunction : planWindowGuidelineJunctions) {
            windowGuidelineIds.add(planWindowGuidelineJunction.Window_Guideline__c);
        }
        // LRCC-1493
        String windowGuidelineQuery = 'SELECT Name, Status__c, Media__c, Customers__c, Start_Date__c, End_Date__c, Outside_Date__c, Tracking_Date__c, Start_Date_Rule__c, End_Date_Rule__c, Tracking_Date_Rule__c, Outside_Date_Rule__c, Window_Guideline_Alias__c, Window_Type__c, Product_Type__c ' +
                'FROM EAW_Window_Guideline__c WHERE Id IN :windowGuidelineIds ORDER BY Name';
        List<EAW_Window_Guideline__c> windowGuidelines = Database.query(windowGuidelineQuery);

        return windowGuidelines;
    }

    @AuraEnabled
    public static List<String> getExhibitionData(String recordId) {

        // Data we return
        List<String> exhibData = new List<String>();

        // Search for Window Guidelines linked to plan
        string query = 'select Window_Guideline__r.Id from EAW_Plan_Window_Guideline_Junction__c where Plan__r.id =\'' + recordId + '\'';
        List<EAW_Plan_Window_Guideline_Junction__c> windowsJunction = Database.query(query);


        // Build String for Window Guideline Ids to search for
        List<String> windowIds = new List<String>();
        for(EAW_Plan_Window_Guideline_Junction__c junc : windowsJunction) {
            windowIds.add('\'' + junc.Window_Guideline__r.Id + '\'');
        }
        String windowIdsStr = '(' + String.join(windowIds,',') + ')';

        // Search for all window strands
        String query2 = 'select Territory__c, Language__c from EAW_Window_Guideline_Strand__c where EAW_Window_Guideline__r.Id in ' + windowIdsStr;
        List<EAW_Window_Guideline_Strand__c> windowStrands = Database.query(query2);

        // string together results .. use a set to avoid dups
        Set<String> territories = new Set<String>();
        Set<String> languages = new Set<String>();
        for(EAW_Window_Guideline_Strand__c strand : windowStrands) {
            territories.add(strand.Territory__c);
            languages.add(strand.Language__c);
        }

        exhibData.add(territories.size() > 0 ? String.join(New List<String>(territories),'; ') : '');
        exhibData.add(languages.size() > 0 ? String.join(New List<String>(languages),'; ') : '');
        return exhibData;
    }

    @AuraEnabled
    public static List<EAW_Window_Guideline__c> searchForIds(String searchText) {
        
        List<List<SObject>> results = [FIND :searchText IN ALL FIELDS RETURNING EAW_Window_Guideline__c];
        Set<Id> ids = new Set<Id> ();

        for (List<SObject> sobjs : results) {
            for (SObject sobj : sobjs) {
                ids.add(sobj.Id);
            }
        }
        
        //LRCC:1215 - Window Guidelines - A single window guideline should not be associated to multiple active plan guidelines
        for(EAW_Plan_Window_Guideline_Junction__c pwg: [SELECT Id, Plan__c, Window_Guideline__c FROM EAW_Plan_Window_Guideline_Junction__c WHERE Window_Guideline__c IN: ids]) {
            if(ids.contains(pwg.Window_Guideline__c)) {
                ids.remove(pwg.Window_Guideline__c);
            } 
        }
        
        List<EAW_Window_Guideline__c> windowGuidelines = [select Name, Customers__c, Description__c,Product_Type__c, End_Date__c, Media__c, Outside_Date__c, Start_Date__c, Tracking_Date__c, Version__c, Version_Notes__c, Status__c, Window_Type__c
                                                          from EAW_Window_Guideline__c where Id in:ids /*LRCC-1214: and Next_Version__c = null*/ and (Status__c = 'Active'/*LRCC-1679: OR Status__c = 'Draft'*/)];
        return windowGuidelines;
    }

    @AuraEnabled
    public static List<EAW_Window_Guideline__c> createPlanWindowGuidelineJunction(String planId, String windowGuidelineId) {
        EAW_Plan_Window_Guideline_Junction__c planWindowGuideline = new EAW_Plan_Window_Guideline_Junction__c();
        planWindowGuideline.Plan__c = planId;
        planWindowGuideline.Window_Guideline__c = windowGuidelineId;

        insert planWindowGuideline;

        List<String> windowGuidelineIds = new List<String>();
        windowGuidelineIds.add(windowGuidelineId);
        String windowGuidelineQuery = 'SELECT Name, Window_Guideline_Alias__c,  Status__c, Media__c, Customers__c, Start_Date__c, End_Date__c, Outside_Date__c, Tracking_Date__c, Window_Type__c, Product_Type__c ' + // LRCC-1493
                'FROM EAW_Window_Guideline__c WHERE Id IN :windowGuidelineIds ORDER BY Name';
        List<EAW_Window_Guideline__c> windowGuidelines = Database.query(windowGuidelineQuery);

        return windowGuidelines;
    }

    @AuraEnabled
    public static statusAndType getPlanStatusFromRecordId(String recordId) {
        statusAndType newstatusAndType = new statusAndType();
        EAW_Plan_Guideline__c plan = new EAW_Plan_Guideline__c();
        String planQuery = 'SELECT Status__c,Product_Type__c FROM EAW_Plan_Guideline__c WHERE Id=\'' + recordId + '\'';
        plan = Database.query(planQuery);
        newstatusAndType.status = plan.Status__c;
        if(plan.Product_Type__c != NULL){
            newstatusAndType.productType = plan.Product_Type__c.split(';');
        }
        return newstatusAndType;
    }
    
    public class statusAndType {
        @AuraEnabled
        public String status;
        @AuraEnabled
        public List<String> productType; 
    }

}