public with sharing class EAW_TitlePlanWindowController {
    
    @AuraEnabled
    public static Map<String, List<SObject>> createPlan(String titleId, String planGuidelineId, String title, String body, 
                                                        Boolean flag,Integer pagesPerRecord,Integer currentPageNumber,
                                                        String pageIdMapUI,String selectedFilterMap) 
    {
        try 
        {
            String planGuidelineQuery = 'SELECT Name, Territory__c, Language__c, Product_Type__c, Release_Type__c, Sales_Region__c ' +
                                        'FROM EAW_Plan_Guideline__c WHERE id = :planGuidelineId';
            EAW_Plan_Guideline__c planGuideline = Database.query(planGuidelineQuery);
            
            String planQuery = 'SELECT Name, Territory__c, Language__c, Product_Type__c, Release_Type__c, Sales_Region__c, EAW_Title__r.Title_EDM__c ' +
                               'FROM EAW_Plan__c WHERE EAW_Title__c = :titleId';
            List<EAW_Plan__c> plans = Database.query(planQuery);
            
            Boolean isDuplicate = false;
            String edmTitleId = '';
            
            if (plans != null && plans.size() > 0) 
            {
                List<String> planIds = new List<String>();
                //edmTitleId = plans.get(0).EAW_Title__r.Title_EDM__c;
                edmTitleId = plans.get(0).EAW_Title__c;
                
                for (EAW_Plan__c plan : plans) 
                {
                    isDuplicate = checkPlanForDuplicates(plan, planGuideline);
                    planIds.add(plan.Id);
                    
                    if (isDuplicate) 
                    {
                        break;
                    }
                }
            } 
            else {
                String titleQuery = 'SELECT Id, Title_EDM__c FROM EAW_Title__c WHERE Id = :titleId ';
                List<EAW_Title__c> titles = Database.query(titleQuery);
            
                edmTitleId = titles.get(0).Id;
            }
            
            if (!isDuplicate) 
            {
                EAW_Plan__c newPlan = new EAW_Plan__c();
                newPlan.Name = planGuideline.Name;
                newPlan.Territory__c = planGuideline.Territory__c;
                newPlan.Language__c = planGuideline.Language__c;
                newPlan.Product_Type__c = planGuideline.Product_Type__c;
                newPlan.Release_Type__c = planGuideline.Release_Type__c;
                newPlan.Sales_Region__c = planGuideline.Sales_Region__c;
                newPlan.Plan_Guideline__c = planGuideline.Id;
                newPlan.EAW_Title__c = titleId;
                newPlan.isCustom__c = true; //LRCC-1390
                
                insert newPlan;
                
                //LRCC-810: When create plan and also create notes for this plan
                if (String.isNotBlank(title)) 
                {
                    Note n = new Note(Title = title, Body = body, ParentId = newPlan.Id);
                    insert n;
                }
                
                //LRCC-1515
                Boolean autoCreateFlag = TRUE;
                String windowGuidelineQuery = 'SELECT Window_Guideline__r.Name, Window_Guideline__r.Window_Type__c, Window_Guideline__r.Product_Type__c, Window_Guideline__r.Status__c, Window_Guideline__r.Start_Date__c, Window_Guideline__r.End_Date__c, ' +
                        'Window_Guideline__r.AutoCreate__c, Window_Guideline__r.Tracking_Date__c, Window_Guideline__r.Outside_Date__c, Window_Guideline__r.Media__c, Window_Guideline__r.Description__c, Window_Guideline__r.Customers__c, Plan__c ' +
                        'FROM EAW_Plan_Window_Guideline_Junction__c WHERE Plan__c = :planGuidelineId AND Window_Guideline__r.AutoCreate__c =: autoCreateFlag';
                List<EAW_Plan_Window_Guideline_Junction__c> planWindowGuidelineJunctions = Database.query(windowGuidelineQuery);
                List<EAW_Window__c> windows = new List<EAW_Window__c>();
                                
                //LRCC-760: Remove DML's inside the for loops
                List<EAW_Plan__c> plansToDelete = new List<EAW_Plan__c>();
                List<EAW_Window_Guideline__c> windowGuidelineList = new List<EAW_Window_Guideline__c> ();
                
                Map<String, List<String>> newWindowMap = new Map<String, List<String>>();
                newWindowMap.put('titles', new List<String>());
                newWindowMap.put('newWindows', new List<String>());
                
                Integer planWindowSize = 0;
                Boolean lastWindow = false;
                
                if (planWindowGuidelineJunctions.size() > 0) 
                {
                    for (EAW_Plan_Window_Guideline_Junction__c windowGuidelineJunction : planWindowGuidelineJunctions) 
                    {
                        EAW_Window_Guideline__c windowGuideline = new EAW_Window_Guideline__c();
                        windowGuideline.Id = windowGuidelineJunction.Window_Guideline__r.Id;
                        windowGuideline.Name = windowGuidelineJunction.Window_Guideline__r.Name;
                        windowGuideline.Window_Type__c = windowGuidelineJunction.Window_Guideline__r.Window_Type__c;
                        windowGuideline.Product_Type__c = windowGuidelineJunction.Window_Guideline__r.Product_Type__c;
                        windowGuideline.Status__c = windowGuidelineJunction.Window_Guideline__r.Status__c;
                        windowGuideline.Start_Date__c = windowGuidelineJunction.Window_Guideline__r.Start_Date__c;
                        windowGuideline.End_Date__c = windowGuidelineJunction.Window_Guideline__r.End_Date__c;
                        windowGuideline.Tracking_Date__c = windowGuidelineJunction.Window_Guideline__r.Tracking_Date__c;
                        windowGuideline.Outside_Date__c = windowGuidelineJunction.Window_Guideline__r.Outside_Date__c;
                        windowGuideline.Media__c = windowGuidelineJunction.Window_Guideline__r.Media__c;
                        windowGuideline.Description__c = windowGuidelineJunction.Window_Guideline__r.Description__c;
                        //LRCC-1560 - Replace Customer lookup field with Customer text field
                        //windowGuideline.Customer__c = windowGuidelineJunction.Window_Guideline__r.Customer__c;
                        windowGuideline.Customers__c = windowGuidelineJunction.Window_Guideline__r.Customers__c;
                        windowGuideline.AutoCreate__c = windowGuidelineJunction.Window_Guideline__r.AutoCreate__c;
                        
                        planWindowSize++;
                        if (planWindowSize == planWindowGuidelineJunctions.size())
                            lastWindow = true;
                        
                        windowGuidelineList.add(windowGuideline);
                    }
                    
                    //LRCC-760: Refactor & Remove DML's inside the for loops
                    Map<String, List<String>> tempWindowMap;
                    if(windowGuidelineList.size() > 0) 
                    {
                        tempWindowMap = createWindow(edmTitleId, newPlan.Id,  null, windows, null, lastWindow, null, null, windowGuidelineList);
                    }
                    if (newWindowMap == null) 
                    {
                        plansToDelete.add(newPlan);
                        return null;
                    } 
                    else {
                        List<String> titleIds = new List<String>();
                        titleIds.add(edmTitleId);
                        newWindowMap.put('titles', titleIds);
                        
                        List<String> finalWindowIds = newWindowMap.get('newWindows');
                        
                        if (tempWindowMap != null) 
                        {
                            finalWindowIds.addAll(tempWindowMap.get('newWindows'));
                        }
                        newWindowMap.put('newWindows', finalWindowIds);
                    }
                        
                    if (plansToDelete.size() > 0) 
                    {
                        delete plans;
                    }
                } 
                else {
                    List<String> titleIds = new List<String>();
                    titleIds.add(edmTitleId);
                    newWindowMap.put('titles', titleIds);
                }
                
                if (newWindowMap.get('titles').size() > 0) 
                {
                    Map<String, List<SObject>> titlePlanWindows = EAW_TitlePlanDateManagementController.searchTitlePlanWindows(newWindowMap.get('titles'), newWindowMap.get('newWindows'), pagesPerRecord, currentPageNumber, pageIdMapUI, selectedFilterMap);
                    return titlePlanWindows;
                } 
                else {
                    return null;
                }
            }
        } 
        catch (Exception e) 
        {
            System.debug(':: Exception ::'+e);
            throw new AuraHandledException('Error : ' + e.getMessage());
        }
        return null;
    }
    
    @AuraEnabled
    public static Map<String, List<SObject>> createWindow(String titleId, String planId, String windowGuidelineId, String title, String body, Integer pagesPerRecord,Integer currentPageNumber,String pageIdMapUI,String selectedFilterMap) {
        
        try 
        {   //LRCC-1560 - Replace Customer lookup field with Customer text field
            /*String windowGuidelineQuery = 'SELECT Name, Window_Type__c, Product_Type__c, Status__c, Start_Date__c, End_Date__c, ' +
                    'Tracking_Date__c, Outside_Date__c, Media__c, Description__c, Customer__c, AutoCreate__c ' +
                    'FROM EAW_Window_Guideline__c WHERE id = :windowGuidelineId';*/
            String windowGuidelineQuery = 'SELECT Name, Window_Type__c, Product_Type__c, Status__c, Start_Date__c, End_Date__c, ' +
                    'Tracking_Date__c, Outside_Date__c, Media__c, Description__c, Customers__c, AutoCreate__c ' +
                    'FROM EAW_Window_Guideline__c WHERE id = :windowGuidelineId';
            EAW_Window_Guideline__c windowGuideline = Database.query(windowGuidelineQuery);
            
            String planQuery = 'SELECT Id, EAW_Title__c FROM EAW_Plan__c WHERE EAW_Title__c = :titleId';
            List<EAW_Plan__c> plans = Database.query(planQuery);
            titleId = plans.get(0).EAW_Title__c;
            
            List<String> planIds = new List<String>();
            
            for(EAW_Plan__c plan : plans) 
            {
                planIds.add(plan.Id);
            }
            
            if (windowGuideline != null) 
            {   //LRCC-1560 - Replace Customer lookup field with Customer text field
                /*String windowQuery = 'SELECT Name, Window_Type__c, Status__c, Start_Date__c, End_Date__c, Tracking_Date__c, Outside_Date__c, Media__c, Customer__c, EAW_Window_Guideline__c, EAW_Plan__c ' +
                        'FROM EAW_Window__c WHERE EAW_Plan__c = :planIds';*/
                String windowQuery = 'SELECT Name, Window_Type__c, Status__c, Start_Date__c, End_Date__c, Tracking_Date__c, Outside_Date__c, Media__c, Customers__c, EAW_Window_Guideline__c, EAW_Plan__c ' +
                        'FROM EAW_Window__c WHERE EAW_Plan__c = :planIds';
                List<EAW_Window__c> windows = Database.query(windowQuery);
                Map<String, List<String>> newWindowMap = createWindow(titleId, planId, windowGuidelineId, windows, windowGuideline, false, title, body, null);
                Map<String, List<SObject>> titlePlanWindows;
                
                if (newWindowMap != null) 
                {
                    //LRCC-1459
                    titlePlanWindows = EAW_TitlePlanDateManagementController.searchTitlePlanWindows(newWindowMap.get('titles'), newWindowMap.get('newWindows'), pagesPerRecord, currentPageNumber, pageIdMapUI, selectedFilterMap);
                }
                    
                if (titlePlanWindows != null) 
                {
                    return titlePlanWindows;
                }
            }
        } 
        catch (Exception e) 
        {
            throw new AuraHandledException('Error : ' + e.getMessage());
        }
        return null;
    }
    
    private static Map<String, List<String>> createWindow(String titleId, String planId, String windowGuidelineId, List<EAW_Window__c> windows, EAW_Window_Guideline__c windowGuideline, Boolean lastWindow, String title, String body, List<EAW_Window_Guideline__c> windowGuidelines) 
    {
        Boolean isDuplicate = false;
        Set<Id> windowGuidelineIdSet = new Set<Id> ();
        List<String> windowNames = new List<String>();
        Map<Id, Id> wgIdAndWindowIdMap = new Map<Id, Id> (); 
        Set<Id> newWindowIds = new Set<Id> ();   
        if (windows.size() > 0) 
        {    
            for (EAW_Window__c window : windows) 
            {
                isDuplicate = checkWindowForDuplicates(window, windowGuideline);
                
                if (isDuplicate) 
                {
                    break;
                }
            }
        }
        
        if (!isDuplicate) 
        {   
            List<EAW_Window__c> windowsToInsert = new List<EAW_Window__c> ();
            
            //LRCC-760: Refactor & Remove DML's inside the for loops
            if (windowGuidelines != null && windowGuidelines.size() > 0) 
            {
                for (EAW_Window_Guideline__c wg: windowGuidelines) 
                {
                    System.debug('wg:::::'+wg);
                    EAW_Window__c newWindow = new EAW_Window__c();
                    newWindow.Name = wg.Name;
                    newWindow.Window_Type__c = wg.Window_Type__c;
                    newWindow.Status__c = 'Estimated';
                    newWindow.Start_Date__c = wg.Start_Date__c;
                    newWindow.End_Date__c = wg.End_Date__c;
                    newWindow.Tracking_Date__c = wg.Tracking_Date__c;
                    newWindow.Outside_Date__c = wg.Outside_Date__c;
                    newWindow.Media__c = wg.Media__c;
                    //LRCC-1560 - Replace Customer lookup field with Customer text field
                    //newWindow.Customer__c = wg.Customer__c;
                    newWindow.Customers__c = wg.Customers__c;
                    newWindow.EAW_Window_Guideline__c = wg.Id;
                    newWindow.EAW_Plan__c = planId;
                    newWindow.EAW_Title_Attribute__c = titleId;
                    
                    windowGuidelineIdSet.add(wg.Id);
                    windowsToInsert.add(newWindow);
                }
            }
            else if(windowGuideline != null) {
            
                EAW_Window__c newWindow = new EAW_Window__c();
                newWindow.Name = windowGuideline.Name;
                newWindow.Window_Type__c = windowGuideline.Window_Type__c;
                newWindow.Status__c = 'Estimated';
                newWindow.Start_Date__c = windowGuideline.Start_Date__c;
                newWindow.End_Date__c = windowGuideline.End_Date__c;
                newWindow.Tracking_Date__c = windowGuideline.Tracking_Date__c;
                newWindow.Outside_Date__c = windowGuideline.Outside_Date__c;
                newWindow.Media__c = windowGuideline.Media__c;
                //LRCC-1560 - Replace Customer lookup field with Customer text field
                //newWindow.Customer__c = windowGuideline.Customer__c;
                newWindow.Customers__c = windowGuideline.Customers__c;
                newWindow.EAW_Window_Guideline__c = windowGuideline.Id;
                newWindow.EAW_Plan__c = planId;
                newWindow.EAW_Title_Attribute__c = titleId;
                
                windowGuidelineIdSet.add(windowGuideline.Id);
                windowsToInsert.add(newWindow);
            } 
            
            if (windowsToInsert.size() > 0) 
            {
                insert windowsToInsert;
                
                for (EAW_Window__c window : windowsToInsert) 
                {
                    wgIdAndWindowIdMap.put(window.EAW_Window_Guideline__c, window.Id);
                    windowNames.add(window.EAW_Window_Guideline__c);
                    newWindowIds.add(window.Id);
                }
                
                //LRCC-810: When create plan and also create notes for this plan
                if (String.isNotBlank(title)) 
                {
                    Note n = new Note(Title = title, Body = body, ParentId = windowsToInsert[0].Id);
                    insert n;
                }
            }
             
            if (lastWindow)
            {
                List<EAW_Window__c> windowList = [Select Id,Name,EAW_Window_Guideline__c,Start_Date__c,Outside_Date__c,End_Date__c,Tracking_Date__c from EAW_Window__c where EAW_Plan__c=:planId];
                EAW_RulesEngineHandler reHandler = new EAW_RulesEngineHandler();
                reHandler.createWindowBody(windowList);
            }
            
            /*String windowGuidelineStrandQuery = 'SELECT End_Date_Days_Offset__c, End_Date_From__c, End_Date_Months_Offset__c, Start_Date_Days_Offset__c, Start_Date_From__c, Start_Date_Months_Offset__c, ' +
                    'Territory__c, Language__c, Media__c, License_Type__c, EAW_Window_Guideline__c, Name FROM EAW_Window_Guideline_Strand__c WHERE EAW_Window_Guideline__c IN: windowGuidelineIdSet';
            
            List<EAW_Window_Guideline_Strand__c> windowGuidelineStrands = Database.query(windowGuidelineStrandQuery);
             //LRCC-1653 - already commented here .
            List<EAW_Window_Strand__c> windowStrands = createWindowStrand(wgIdAndWindowIdMap, windowGuidelineStrands);
            
            if (windowStrands != null && windowStrands.size() > 0) 
            {
                insert windowStrands;
            }*/ // Since the Window strand insert logic has been handled via trigger
            
            //LRCC:631
            if(newWindowIds.size() > 0) {
                EAW_RightsCheckCalloutController.sentWindowstoGalilio(newWindowIds,'insert');
            }
            
            List<String> titles = new List<String>();
            titles.add(titleId);
            
            Map<String, List<String>> newWindowMap = new Map<String, List<String>>();
            newWindowMap.put('titles', titles);
            newWindowMap.put('newWindows', windowNames);
            
            return newWindowMap;
        } 
        
        return null;
    }
    
    //LRCC-1653 - -In this ticket once passed QA it with remove the commented code.
    /*private static List<EAW_Window_Strand__c> createWindowStrand(Map<Id, Id> wgIdAndWindowIdMap, List<EAW_Window_Guideline_Strand__c> windowGuidelineStrands) 
    {
        List<EAW_Window_Strand__c> windowStrands = new List<EAW_Window_Strand__c>();
        
        for (EAW_Window_Guideline_Strand__c windowGuidelineStrand : windowGuidelineStrands) 
        {
            EAW_Window_Strand__c windowStrand = new EAW_Window_Strand__c();
            windowStrand.Name = windowGuidelineStrand.Name;
            windowStrand.End_Date_Days_After__c = windowGuidelineStrand.End_Date_Days_Offset__c;
            windowStrand.End_Date_From__c = windowGuidelineStrand.End_Date_From__c;
            windowStrand.End_Date_Months_After__c = windowGuidelineStrand.End_Date_Months_Offset__c;
            windowStrand.Start_Date_Days_After__c = windowGuidelineStrand.Start_Date_Days_Offset__c;
            windowStrand.Start_Date_From__c = windowGuidelineStrand.Start_Date_From__c;
            windowStrand.Start_Date_Months_After__c = windowGuidelineStrand.Start_Date_Months_Offset__c;
            windowStrand.Territory__c = windowGuidelineStrand.Territory__c;
            windowStrand.Language__c = windowGuidelineStrand.Language__c;
            windowStrand.Media__c = windowGuidelineStrand.Media__c;
            windowStrand.License_Type__c = windowGuidelineStrand.License_Type__c;
            windowStrand.Window_Guideline_Strand__c = windowGuidelineStrand.Id;
            
            //LRCC-760: Refactor & Remove DML's inside the for loops
            if (wgIdAndWindowIdMap != null && wgIdAndWindowIdMap.get(windowGuidelineStrand.EAW_Window_Guideline__c) != null)
            {
                windowStrand.Window__c = wgIdAndWindowIdMap.get(windowGuidelineStrand.EAW_Window_Guideline__c);
            }
            
            windowStrands.add(windowStrand);
        }

        return windowStrands;
    }*/
    
    private static Boolean checkPlanForDuplicates(EAW_Plan__c plan, EAW_Plan_Guideline__c planGuideline) 
    {
        if (plan.Name == planGuideline.Name &&
            plan.Territory__c == planGuideline.Territory__c &&
            plan.Language__c == planGuideline.Language__c &&
            plan.Product_Type__c == planGuideline.Product_Type__c &&
            plan.Release_Type__c == planGuideline.Release_Type__c &&
            plan.Sales_Region__c == planGuideline.Sales_Region__c) 
        {
            return true;
        }
            
        return false;
    }
    
    private static Boolean checkWindowForDuplicates(EAW_Window__c window, EAW_Window_Guideline__c windowGuideline) 
    {
        if (window.Name == windowGuideline.Name &&
            window.Window_Type__c == windowGuideline.Window_Type__c &&
            window.Start_Date__c == windowGuideline.Start_Date__c &&
            window.End_Date__c == windowGuideline.End_Date__c &&
            window.Tracking_Date__c == windowGuideline.Tracking_Date__c &&
            window.Outside_Date__c == windowGuideline.Outside_Date__c &&
            window.Media__c == windowGuideline.Media__c &&
            //LRCC-1560 - Replace Customer lookup field with Customer text field
            //window.Customer__c == windowGuideline.Customer__c
            window.Customers__c == windowGuideline.Customers__c) 
        {
            return true;
        }
        return false;
    }
}