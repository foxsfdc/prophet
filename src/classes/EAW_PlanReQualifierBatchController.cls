global class EAW_PlanReQualifierBatchController implements Database.Stateful,Database.Batchable<sObject>{
    
    global Set<String> productTypeSet = new Set<String>();
    global Boolean isTVDBoxOffice;
    global Boolean isTitleAttribute;
    global Boolean isTitleTag;
    global Boolean isReleaseDate;
    global String planGuidelineId;
    global Set<Id> qualifiedEAWTitleIdSet ;
    global Set<Id> isTVDBoxOfficeWithPGIds;
    global Set<Id> isTitleAttributeWithPGIds ;
    global Set<Id> isTitleTagWithPGIds;
    global Set<Id> isReleaseDateWithPGIds;
    
    global Set<Id> qualifiedTitleIdSet;
    global Set<Id> planGuidelineIdSet; // LRCC - 1457
    
    global EAW_PlanReQualifierBatchController(Set<String> ptSet,String planGuidelineIds,Set<Id> qEAWTitleIdSet,
                                              Set<Id> isTVDBoxOfficePGIds,Set<Id> isTitleAttributePGIds,Set<Id> isTitleTagPGIds,
                                              Set<Id> isReleaseDatePGIds) {
                                                  
                                                  
                                                  System.debug('::::::ptSet::::'+ptSet);
                                                  System.debug('::::::isTVDBoxOfficePGIds::::'+isTVDBoxOfficePGIds);
                                                  System.debug('::::::isTitleAttributePGIds::::'+isTitleAttributePGIds);
                                                  System.debug('::::::isTitleTagPGIds::::'+isTitleTagPGIds);
                                                  System.debug('::::::isReleaseDatePGIds::::'+isReleaseDatePGIds); 
                                                  
                                                  
                                                  productTypeSet = ptSet;
                                                  planGuidelineId = planGuidelineIds;
                                                  qualifiedEAWTitleIdSet  = qEAWTitleIdSet;
                                                  isTVDBoxOfficeWithPGIds = isTVDBoxOfficePGIds;
                                                  isTitleAttributeWithPGIds = isTitleAttributePGIds;
                                                  isTitleTagWithPGIds = isTitleTagPGIds;
                                                  isReleaseDateWithPGIds = isReleaseDatePGIds;
                                                  
                                                  qualifiedTitleIdSet = new Set<Id>();
                                                  planGuidelineIdSet = new Set<Id>();
                                                  
                                                  System.debug('::::::isTVDBoxOfficeWithPGIds ::::'+isTVDBoxOfficeWithPGIds );
                                                  System.debug('::::::isTitleAttributeWithPGIds ::::'+isTitleAttributeWithPGIds );
                                                  System.debug('::::::isTitleTagWithPGIds ::::'+isTitleTagWithPGIds );
                                                  System.debug('::::::isReleaseDateWithPGIds ::::'+isReleaseDateWithPGIds );
                                                  System.debug('::::::productTypeSet ::::'+productTypeSet+':::::::::isTitleTag :::::'+isTitleTag); 
                                                  System.debug('::::::isTVDBoxOffice::::'+isTVDBoxOffice+':::::isTitleAttribute::::'+isTitleAttribute);
                                                  System.debug('::::::qualifiedEAWTitleIdSet  ::::'+qualifiedEAWTitleIdSet  +':::::isReleaseDate::::'+isReleaseDate);
                                              }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        
        
        
        System.debug(':::productTypeSet :::' + productTypeSet);
        system.debug('::isTitleAttribute::'+isTitleAttribute);
        
        String query = 'SELECT Id,Title_EDM__c,PROD_TYP_CD_INTL__c,PROD_TYP_CD_INTL__r.Name FROM EAW_Title__c WHERE PROD_TYP_CD_INTL__r.Name IN :productTypeSet';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        
        System.debug(':::scope:::' + scope);
        System.debug(':::scope::lenght:' + scope.size());
        
        List<EAW_Title__c >titleAttributeList = scope;
        Map<Id, Id> edmTitleToEawTitleIdMap = new Map<Id, Id>();
        Set<Id> titleSet = new Set<Id>();
        
        
        if( titleAttributeList.size() > 0 ){
            
            for(EAW_Title__c title : titleAttributeList){
                
                titleSet.add(title.Id);
                edmTitleToEawTitleIdMap.put(title.Title_EDM__c, title.Id);
            }
        }
        
        
        System.debug(':::::titleSet:::::'+titleSet);          
        System.debug(':::::edmTitleToEawTitleIdMap:::::'+edmTitleToEawTitleIdMap);
        
        checkRecursiveData.bypassDateCalculation  = true;
        
        if(isTVDBoxOfficeWithPGIds.size() > 0 && titleSet.size() > 0 ){
            
            planGuidelineIdSet.addAll(isTVDBoxOfficeWithPGIds);        
            EAW_BoxOfficeHandler boxOfficeHanlder = new EAW_BoxOfficeHandler();
            List<EAW_Box_Office__c> boxOfficeList = [ SELECT Id, Name, Territory__c, TVD_US_Box_Office__c,Title_Attribute__c,Title_Attribute__r.PROD_TYP_CD_INTL__c,Title_Attribute__r.PROD_TYP_CD_INTL__r.Name,TVD_Local_Currency_Value__c, TVD_Local_Admissions_Count__c,TVD_Number_of_Screens__c FROM EAW_Box_Office__c WHERE Title_Attribute__c IN : titleSet ];
            qualifiedEAWTitleIdSet = boxOfficeHanlder.processTVDBoxOfficeToQualifyTitleForInsert(boxOfficeList,isTVDBoxOfficeWithPGIds);//boxOfficeHanlder.processTVDBoxOfficeToQualifyTitleForInsert(boxOfficeList, new Set<Id>{planGuidelineId});
            
        }
        System.debug(':::::: TVD Box Office Qualified Title Ids Set ::::::'+qualifiedEAWTitleIdSet);
        if( qualifiedEAWTitleIdSet != NULL && qualifiedEAWTitleIdSet.size() > 0 ) {
            qualifiedTitleIdSet.addAll(qualifiedEAWTitleIdSet);
            titleSet.removeAll(qualifiedEAWTitleIdSet);
        }
        
        //Condition check related to Release date Object records
        if(isReleaseDateWithPGIds.size() > 0 && titleSet.size() > 0) {
            
            planGuidelineIdSet.addAll(isReleaseDateWithPGIds);
            EAW_ReleaseDate_Handler releaseDateHandler = new EAW_ReleaseDate_Handler();
            qualifiedEAWTitleIdSet = releaseDateHandler.checkPlanQualificationRules(null,isReleaseDateWithPGIds,titleSet);//releaseDateHandler.checkPlanQualificationRules(null,new Set<Id>{planGuidelineId},titleSet);
        }
        
        if( qualifiedEAWTitleIdSet != NULL && qualifiedEAWTitleIdSet.size() > 0 ) {
            qualifiedTitleIdSet.addAll(qualifiedEAWTitleIdSet);
            titleSet.removeAll(qualifiedEAWTitleIdSet);
        }
        
        //Condition check related to Title attribute Object records
        if( isTitleAttributeWithPGIds.size() > 0  && titleSet.size() > 0 ) {
            
            planGuidelineIdSet.addAll(isTitleAttributeWithPGIds);
            EAW_Title_Handler titleHandler = new EAW_Title_Handler();
            List<EAW_Title__c> titleList = [SELECT Id,Name, RLSE_CAL_YR__c, FIN_DIV_CD__c,FIN_DIV_CD__r.Name, PROD_TYP_CD_INTL__c,PROD_TYP_CD_INTL__r.Name FROM EAW_Title__c WHERE Id IN : titleSet ];
            if(titleList.size() > 0) {
                    
                qualifiedEAWTitleIdSet = titleHandler.processTitleToCheckForPlanQualification(titleList, isTitleAttributeWithPGIds, false);//titleHandler.processTitleToCheckForPlanQualification(titleList,new Set<Id>{planGuidelineId});
            }
        }
        if(qualifiedEAWTitleIdSet != NULL && qualifiedEAWTitleIdSet.size() > 0) {
            qualifiedTitleIdSet.addAll(qualifiedEAWTitleIdSet);
            titleSet.removeAll(qualifiedEAWTitleIdSet);
        }
        //Condition check related to Title Tag Object records
        if( isTitleTagWithPGIds.size() > 0 && titleSet.size() > 0 ) {
            
            planGuidelineIdSet.addAll(isTitleTagWithPGIds);
            EAW_TitleTagJunctionTriggerHandler tagHandler = new EAW_TitleTagJunctionTriggerHandler();
            qualifiedEAWTitleIdSet = tagHandler.processTitleTagToCheckForPlanQualification(null,isTitleTagWithPGIds,titleSet,true);//processTitleTagToCheckForPlanQualification(null,new Set<Id>{planGuidelineId},titleSet);
        }
        if( qualifiedEAWTitleIdSet != NULL && qualifiedEAWTitleIdSet.size() > 0 ) {
            qualifiedTitleIdSet.addAll(qualifiedEAWTitleIdSet);
            titleSet.removeAll(qualifiedEAWTitleIdSet);
        }
        system.debug('::planGuidelineIdSet:::'+planGuidelineIdSet);
        system.debug('::titleSet:::'+titleSet);
        if(planGuidelineIdSet.size() > 0 && titleSet != null && titleSet.size() != 0 && scope.size() == titleSet.size()){
            Map<Id,Id> previousVersionMap = new Map<Id,Id>();
            for(EAW_Plan_Guideline__c previousPG : [SELECT Id,Name,Next_Version__c FROM EAW_Plan_Guideline__c WHERE Next_Version__c != null AND Next_Version__c IN :planGuidelineIdSet]){
                previousVersionMap.put(previousPG.Id,previousPG.Next_Version__c);
            }
            if(previousVersionMap.keySet() != null && previousVersionMap.keySet().size() > 0){
                List<EAW_Plan__c> plansToUpdate = new List<EAW_Plan__c>();
                for(EAW_Plan__c plan : [SELECT Id,Plan_Guideline__c,EAW_Title__c,isCustom__c FROM EAW_Plan__c WHERE Plan_Guideline__c IN :previousVersionMap.keySet() AND EAW_Title__c IN :titleSet]){
                    if(previousVersionMap.get(plan.Plan_Guideline__c) != null){
                        plan.Plan_Guideline__c = previousVersionMap.get(plan.Plan_Guideline__c);
                        plan.isCustom__c = true; //LRCC-1390
                        plansToUpdate.add(plan);
                    }
                }
                system.debug('::::::plansToUpdate::::::'+plansToUpdate);
                if(plansToUpdate.size() > 0){
                    update plansToUpdate;
                }
            }
        }
    }  
    
    global void finish(Database.BatchableContext BC){
        
        Set<Id> windowGuidelineIdSet = new Set<Id>();
        for(EAW_Plan_Window_Guideline_Junction__c   junction : [SELECT Plan__c,Window_Guideline__c FROM EAW_Plan_Window_Guideline_Junction__c WHERE Plan__c IN :planGuidelineIdSet]){
            if(junction.Window_Guideline__c != null)windowGuidelineIdSet.add(junction.Window_Guideline__c);
        }
        if( windowGuidelineIdSet.size() > 0 ){
            EAW_WGDateCalculationBatchClass windowDateBatch = new EAW_WGDateCalculationBatchClass(windowGuidelineIdSet);
            windowDateBatch.releaseDateGuidelineIdSet = NULL;
            windowDateBatch.titleIdSet = qualifiedTitleIdSet;
            //windowDateBatch.windowGuidelineIdSet = windowGuidelineIdSet;
            ID batchprocessid = Database.executeBatch(windowDateBatch, 200);
        }
        //LRCC-1745
        EAW_Plan_Guideline__c planGuideline = new EAW_Plan_Guideline__c();
        planGuideline.Id = planGuidelineId;
        planGuideline.isPlanQualifyBatchProcessing__c = false;
        update planGuideline;
    }
}