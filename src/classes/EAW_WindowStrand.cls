public with sharing class EAW_WindowStrand {
	
    @AuraEnabled
    public static list<list<String>> collectWindowStrandPicklists() {
        list<list<String>> picklistsValues = new list<list<String>>();
        
        list<String> licenseTypes = EAW_CollectPicklistValues.getGenericObject('EAW_Window_Guideline_Strand__c', 'License_Type__c');
        list<String> medias = EAW_CollectPicklistValues.getGenericObject('EAW_Window_Guideline_Strand__c', 'Media__c');
        list<String> territories = EAW_CollectPicklistValues.getGenericObject('EAW_Window_Guideline_Strand__c', 'Territory__c');
        list<String> languages = EAW_CollectPicklistValues.getGenericObject('EAW_Window_Guideline_Strand__c', 'Language__c');
        picklistsValues.add(licenseTypes);
        picklistsValues.add(medias);
        picklistsValues.add(territories);
        picklistsValues.add(languages);

        return picklistsValues;
    }
}