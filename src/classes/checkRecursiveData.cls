public class checkRecursiveData {
    
    private static boolean firstRun = true;
    private static Set<String> idSet = new Set<String>();
    
    public static boolean bypassDateCalculation = FALSE;
    
    public static boolean isRecursiveData(string idValue){
        
        if(firstRun){
            idSet.add(idValue);
            firstRun = false;
            return true;
        }else{
            if(idSet.contains(idValue)){
                return false;
            } else{
                idSet.add(idValue);
                return true;
            }
        }
    }
}