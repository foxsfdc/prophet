<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>update_release_date_guideline_name_field</fullName>
        <field>Name</field>
        <formula>If( NOT(ISPICKVAL(Language__c , &apos;&apos;)), TEXT( EAW_Release_Date_Type__c ) &amp; &quot; / &quot; &amp; 
TEXT( Territory__c ) &amp; &quot; / &quot; &amp; 
TEXT( Language__c ) &amp; &quot; / &quot; &amp; 
TEXT(Product_Type__c), TEXT( EAW_Release_Date_Type__c ) &amp; &quot; / &quot; &amp; 
TEXT( Territory__c ) &amp; &quot; / &quot; &amp; 
TEXT(Product_Type__c))</formula>
        <name>update release date guideline name field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Auto populate Release Date Guideline Name</fullName>
        <actions>
            <name>update_release_date_guideline_name_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND (  OR (( Active__c ),NOT( CONTAINS( Name , &apos;clone&apos;) )),  NOT( ISPICKVAL( EAW_Release_Date_Type__c , &apos;&apos;) ),  NOT( ISPICKVAL( Territory__c , &apos;&apos;) ),  NOT( ISPICKVAL( Product_Type__c, &apos;&apos;) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
