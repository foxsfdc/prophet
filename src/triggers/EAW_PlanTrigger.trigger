trigger EAW_PlanTrigger on EAW_Plan__c (before insert, before update, after insert, after update) {
    
    EAW_PlanHandler reHandler = new EAW_PlanHandler();
    
    if(Trigger.isBefore) {
        if(Trigger.isInsert) {
            reHandler.populatePlanName(Trigger.new); //LRCC-1462
            reHandler.populatePlanFieldValues(Trigger.new); // LRCC-739 auto populate field values for sales region, territory and language
        }
        else if(trigger.isUpdate) {
            reHandler.populatePlanName(Trigger.new); //LRCC-1462
        }
    } 
    else {
        if(trigger.isInsert) {
            //TODO Foxipedia test as part of LRCC-1736 
            reHandler.validateProductTypes(Trigger.new); //LRCC-1322: 3rd Point-We have't get parent field values throw before trigger so only we are go for after trigger.
            reHandler.createWindow(Trigger.new); //LRCC-1390
        }
        else {
            //TODO Foxipedia test as part of LRCC-1736 
            reHandler.validateProductTypes(Trigger.new); //LRCC-1322: 3rd Point-We have't get parent field values throw before trigger so only we are go for after trigger
            //reHandler.createWindow(Trigger.new); //LRCC-1390
        }
    }
}