trigger EAW_ReleaseDateTrigger on EAW_Release_Date__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
        
    EAW_ReleaseDate_Handler callHandler = new EAW_ReleaseDate_Handler();
            
    if(Trigger.isBefore) {
        
        if(Trigger.isInsert) {
            //RD-Duplicate Check
            callHandler.populatecustomersValue(Trigger.new); //LRCC-1236
            callHandler.populateValues(Trigger.new, NULL); // This method needs Release date type value get populted by populatecustomersValue method.  
            //LRCC-611
            callHandler.calculateAutoFirm(Trigger.new,Trigger.old);
        } else if(Trigger.isUpdate) {
            //RD-Duplicate Check
            callHandler.populateValues(Trigger.new, Trigger.oldMap);
            //LRCC-611
            callHandler.calculateAutoFirm(Trigger.new,Trigger.old);
             //LRCC-1527
            callHandler.releaseDateFieldCheck(Trigger.new);
            callHandler.checkForNullDateValues(Trigger.new); // LRCC - 1010
            //This method call should be last in the sequence. If you add any new method please add it before this method
            callHandler.checkForReQualification(Trigger.old,Trigger.new); // LRCC - 1082 (both Plan and Release date calculations)
            
        }
        
    } else if(Trigger.isAfter) {
        if(Trigger.isInsert) {
            callHandler.checkForReQualification(Trigger.new); // LRCC-282, LRCC - 1082 (both Plan and Release date calculations)
            callHandler.sentNotifcationsToRDReviewGroup(null, Trigger.new); //LRCC-1049
            //callHandler.sendDataToOtherSystems(Trigger.new,'insert');//LRCC-1709 - Will handled in batch class
        } 
        else if(Trigger.isUpdate) {
            callHandler.sentNotifcationsToRDReviewGroup(Trigger.old, Trigger.new); //LRCC-1049
            //callHandler.sendDataToOtherSystems(Trigger.new,'update');//LRCC-1709
        }
        else if(trigger.isDelete) {
            callHandler.testDeleteRecords(trigger.old);
            //callHandler.deleteDownstreamReleaseDates(trigger.old); //LRCC-1228 - No needed since we handled it as a soft delete
            //callHandler.sendDataToOtherSystems(Trigger.new,'delete');//LRCC-1709
        }
    }
}