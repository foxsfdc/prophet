trigger EAW_WindowGuidelineStrandTrigger on EAW_Window_Guideline_Strand__c (before insert, after insert, before update, after update, before delete) {

    EAW_WindowGuidelineStrandTrigger_Handler handler = new EAW_WindowGuidelineStrandTrigger_Handler();
    
    if(Trigger.isBefore) {
    
        if(Trigger.isInsert) {
            //This method was implemented to share only specific records to Window User in SFINTEG for successful UAT testing. This was commented here and need to be removed in future
            //handler.setWindowGuidelineTemp(Trigger.new);
            handler.preventRecordChangeWhenActiveStatus(Trigger.new); //LRCC-1009, LRCC-1358
            //LRCC-1399 & LRCC-1487
            handler.limitStrandWithNoSpecificeLanguage(Trigger.new);
        }
            
        if(Trigger.isUpdate) {
            //This method was implemented to share only specific records to Window User in SFINTEG for successful UAT testing. This was commented here and need to be removed in future
            //handler.setWindowGuidelineTemp(Trigger.new);
            handler.preventRecordChangeWhenActiveStatus(Trigger.new); //LRCC-1009, LRCC-1358
            //LRCC-1399 & LRCC-1487
            handler.limitStrandWithNoSpecificeLanguage(Trigger.new);
        }
        
        //LRCC-1432
        if(Trigger.isDelete) {
            handler.preventDeletionofStrandsRelatedToActiveGuideline(Trigger.Old);
        }
    }
    
    if(Trigger.isAfter) {
    	
    	if(Trigger.isInsert){
    		handler.sendData(Trigger.new,'Insert'); //LRCC-1780
    	}
    	
        if(Trigger.isUpdate) {
        
            //LRCC-1653 - -In this ticket once passed QA it with remove the commented code.
            //handler.windowStrandOffsetUpdate(Trigger.oldMap,Trigger.newMap); // LRCC-1330
            handler.sentNotificationToRReviewGroup(Trigger.old, Trigger.new); //LRCC-1050
            handler.sendData(Trigger.new,'Update');//LRCC-1780
        }
    }
}