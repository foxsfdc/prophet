trigger EAW_TitleTagJunctionTrigger on EAW_Title_Tag_Junction__c ( after insert, after update, after delete ) {
    
    EAW_TitleTagJunctionTriggerHandler handler = new EAW_TitleTagJunctionTriggerHandler();
     
    if(Trigger.isBefore) {
    
    } else if(Trigger.isAfter) {
        
        if(Trigger.isInsert) {
            handler.reQualificationCheckForAllRules(trigger.new, false ); //For Release Date Guideline, Window Guideline and Plan Guideline Re-Qualification
            handler.sentNotificationTAReviewGroup(trigger.new, true); //LRCC-1405
        } 
        else if(Trigger.isUpdate) {
            handler.reQualificationCheckForAllRules(trigger.new, trigger.old); //For Release Date Guideline, Window Guideline and Plan Guideline Re-Qualification
        } 
        else if(Trigger.isDelete) {
            handler.reQualificationCheckForAllRules(trigger.old, true ); //For Release Date Guideline and Window Guideline Re-Qualification
            handler.sentNotificationTAReviewGroup(trigger.old, false); //LRCC-1405
        }
    }
}