trigger EAW_Notes_Trigger on Note (before insert, before update, before delete, after insert, after update, after delete) {
    
    EAW_NotesTrigger_Handler handler = new EAW_NotesTrigger_Handler();
    
     //LRCC-1787
     if( Trigger.isBefore ) {
    
        if( trigger.isInsert) {
            handler.updateOldNotes(trigger.new);
        } else if( trigger.isUpdate ){
            handler.updateOldNotes(trigger.old);
        } else if( trigger.isDelete ) {
            handler.updateOldNotes(trigger.old);
        }
        
     } else if( trigger.isAfter ){
        
        if( trigger.isInsert ){
            handler.updateHasNotesInReleaseDateObjectInsert(trigger.new);
        } else if( trigger.isDelete ){
            handler.updateHasNotesInReleaseDateObjectDelete(trigger.old);
        }
    }
}