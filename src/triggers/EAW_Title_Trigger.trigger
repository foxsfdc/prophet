trigger EAW_Title_Trigger on EAW_Title__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) 
{
    EAW_RulesEngineHandler eRlEng = new EAW_RulesEngineHandler();
    if(Trigger.isAfter)
    {
        if(Trigger.isInsert)
            eRlEng.createBody(Trigger.new);
    }
    if(Trigger.isBefore)
    { 
        
    }
}