trigger EAW_TagTrigger on EAW_Tag__c (before delete) {
    if(trigger.isBefore) {
        if(trigger.isDelete) {
            // Delete tags only if they are not attached to any Windows
            for(EAW_Tag__c tag : trigger.old) {
                
                if(tag.Tag_Type__c == 'window tag') {
                    string query = 'select Id from EAW_Window_Tag_Junction__c where Tag__r.Id=\'' + tag.Id + '\'';
                    List<EAW_Window_Tag_Junction__c > windowJuncs = Database.query(query);
                    if(windowJuncs.size() > 0) {
                        tag.addError('Cannot delete a tag currently used by a Window Schedules');
                    }
                } else if(tag.Tag_Type__c == 'release date tag') {
                    string query = 'select Id from EAW_Release_Date_Tag_Junction__c where Tag__r.Id=\'' + tag.Id + '\'';
                    List<EAW_Release_Date_Tag_Junction__c> releaseDateJuncs = Database.query(query);
                    if(releaseDateJuncs.size() > 0) {
                        tag.addError('Cannot delete a tag used by existing Release Dates');
                    }
                } else if(tag.Tag_Type__c == 'title tag') {
                    string query = 'select Id from EAW_Title_Tag_Junction__c where Tag__r.Id=\'' + tag.Id + '\'';
                    List<EAW_Title_Tag_Junction__c> titleJuncs = Database.query(query);
                    if(titleJuncs.size() > 0) {
                        tag.addError('Cannot delete a tag used by existing Titles');
                    }
                }
            }
        }   
    }
}