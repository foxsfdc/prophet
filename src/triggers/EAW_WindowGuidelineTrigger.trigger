trigger EAW_WindowGuidelineTrigger on EAW_Window_Guideline__c (before insert, before update, before delete, after insert, after update) {
    
    EAW_WindowGuidelineTrigger_Handler handler = new EAW_WindowGuidelineTrigger_Handler();
    
    if(trigger.isBefore) {
    
        //Code below is executed BEFORE a window is updated/Changed
        if(trigger.isDelete) {
            //handler.deleteAssociatedWindows(trigger.old); commented for LRCC-716
            handler.preventDeletionofWDG(trigger.old); //LRCC-716
        } else if(trigger.isUpdate) {
            handler.updateNewWGVersionIdToReferencedRuleAndRuleDetails(Trigger.new, Trigger.oldMap); //LRCC-1259 
            handler.ensureValidationIsNotBypassed(trigger.old, trigger.new);
            handler.validateWindowStrands(trigger.old, trigger.new);
            handler.preventRecordChangeWhenActiveStatus(Trigger.new, Trigger.oldMap); //LRCC-1009 
            handler.updateRuleConditionalOperands(trigger.old,trigger.new); // LRCC - 1624  
        } else if(trigger.isInsert) {
            handler.validateWindowGuidelineStatus(trigger.new);
        }
    } else {
        
        //Code below is executed AFTER a window is updated/changed
        if(trigger.isInsert) {
            //I have comment out method in trigger level the logic moved to Window Guideline New Version controller related to LRCC-1255.
            //handler.reconfigureJunctionsWithPlanGuidelines(trigger.new);
            //handler.sendData(Trigger.new,'Insert');//LRCC-1779
        } else if(trigger.isUpdate) {
            handler.attachWindowGuidelineToAssociatedPlans(trigger.old, trigger.new);
            handler.moveWindowsToCurrentWGs(trigger.old, trigger.new); //LRCC-623
            //handler.sendAssociateWindowsToRulesEngine(trigger.old, trigger.new);
            //handler.updateNewWGVersionIdToReferencedRuleAndRuleDetails(Trigger.new, Trigger.oldMap); //LRCC-1259 - Regarding LRCC-1785 this method moved to before update trigger.
            handler.checkWGRuleDependency(trigger.new, trigger.oldMap); //LRCC-1575
            handler.reCalculateWindowDates(Trigger.new, Trigger.old);
            handler.sendData(Trigger.new,'Update');//LRCC-1779
        }
    }
}