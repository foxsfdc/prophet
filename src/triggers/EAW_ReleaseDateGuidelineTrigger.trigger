trigger EAW_ReleaseDateGuidelineTrigger on EAW_Release_Date_Guideline__c (before insert, after insert, before update, after update, before delete) {
    
    EAW_ReleaseDateGuidelineHandler rdHandler = new EAW_ReleaseDateGuidelineHandler();
    
    if(Trigger.isBefore) {
        
        if(trigger.isUpdate || trigger.isInsert) {
            
            rdhandler.setDefaultLanguage(Trigger.new);
            rdhandler.populateReleaseDateGuidelineName(Trigger.new);    //LRCC-1462
            //LRCC-1015, LRCC-1130
            rdhandler.preventDuplicate(Trigger.new);
            
            //rdhandler.preventUncheckingallowManualCheckBox(trigger.oldMap, trigger.new); //LRCC-1540
        }
       //LRCC-1586      
        if(trigger.isUpdate) {
            rdHandler.preventUpdateOfRDG(trigger.new, trigger.oldMap);
            rdHandler.updateRuleConditionalOperands(trigger.old,trigger.new,trigger.newMap); // LRCC - 1624
        }
        //LRCC:1136
        if(trigger.isDelete) {
            rdHandler.preventDeletionofRDG(trigger.old);
        }
    } 
    else if(Trigger.isAfter) {
        
        if(Trigger.isInsert){
        	rdhandler.sendData(Trigger.new,'Insert'); //LRCC-1776
        }
        
        if(Trigger.isUpdate) {
            //LRCC:1145
            rdHandler.validateRDGAndReleaseDates(Trigger.new, Trigger.old);
            //rdHandler.reActivateRDGAndReleaseDates(Trigger.new, Trigger.old); - This logic might not needed since the date calculation logic take care of the release date Active
            //LRCC:1137
            rdHandler.reCalculateReleaseDates(Trigger.new, Trigger.old);
            rdHandler.ruleTextFieldUpdate(Trigger.old, Trigger.new); //LRCC-1298
            rdhandler.updateUnFirmDates(Trigger.old,Trigger.new);//LRCC-1559
            rdhandler.sendData(Trigger.new,'Update'); //LRCC-1776
            
        }
    }
}